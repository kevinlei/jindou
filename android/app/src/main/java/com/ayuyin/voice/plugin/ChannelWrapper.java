package com.ayuyin.voice.plugin;
import com.ayuyin.voice.BuildConfig;
import com.ayuyin.voice.MainActivity;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.Toast;
import androidx.annotation.NonNull;

import com.ayuyin.voice.R;
import com.ayuyin.voice.elogin.MyELoginActivityFullscreen;
import com.g.gysdk.GYManager;
import com.g.gysdk.GYResponse;
import com.g.gysdk.GyCallBack;
import com.g.gysdk.GyConfig;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;

public class ChannelWrapper implements MethodChannel.MethodCallHandler {
    private Activity activity;
    private final Context context;
    private final ExecutorService cachedThreadPool = Executors.newCachedThreadPool();
    public static MethodChannel.Result result;

    public ChannelWrapper(FlutterPlugin.FlutterPluginBinding pluginBinding) {
        this.context = pluginBinding.getApplicationContext();
    }

    public void post(Runnable runnable) {
        PluginWrapper.handler.post(runnable);
    }

    public void asyncRunnable(Runnable runnable) {
        cachedThreadPool.execute(runnable);
    }

    public void onSuccess(MethodChannel.Result result, Object object) {
        post(()-> result.success(object));
    }

    public void onError(MethodChannel.Result result, String desc) {
        post(()-> result.error("0", "PluginError", desc));
    }
    
    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public void onDestroy() {
        this.activity = null;
    }

    @Override
    public void onMethodCall(@NonNull MethodCall call, @NonNull MethodChannel.Result result) {
        try {
            ChannelWrapper.result = result;
            Map<String, Object> param = (Map)call.arguments;
            if (ChannelMethod.initLogin.equals(call.method)) {
                initLogin(param, result);
            } else if (ChannelMethod.prefetchPhone.equals(call.method)) {
                prefetchPhone(param, result);
            } else if (ChannelMethod.loginPass.equals(call.method)) {
                loginPass(param, result);
            } else if (ChannelMethod.systemSettings.equals(call.method)) {
                systemSettings(param, result);
            } else if (ChannelMethod.sleepSettings.equals(call.method)) {
                sleepSettings(param, result);
            } else {
                Map<String, Object> data = new HashMap<>();
                data.put("success", false);
                onSuccess(result, data);
            }
        } catch (Exception ignored) {
            android.util.Log.d("ChannelError", ignored + "");
            Map<String, Object> data = new HashMap<>();
            data.put("success", false);
            onSuccess(result, data);
        }
    }



    // 初始化一键登录
    private void initLogin(Map<String, Object> param, @NonNull MethodChannel.Result result) {
        GYManager.getInstance().init(GyConfig.with(this.context)
        //预取号使用缓存，可以提高预取号的成功率，建议设置为true
        .preLoginUseCache(true)
        //个验debug调试模式
        .debug(BuildConfig.DEBUG)
        //运营商debug调试模式
        .eLoginDebug(BuildConfig.DEBUG)
        //应用渠道
        .channel("kdpd")
        //成功失败回调
        .callBack(new GyCallBack() {
            @Override
            public void onSuccess(GYResponse response) {
                android.util.Log.d("GYManager初始化成功", "" + response);
            }

            @Override
            public void onFailed(GYResponse response) {
                android.util.Log.d("GYManager初始化失败", "" + response);
            }
        }).build());

        Map<String, Object> data = new HashMap<>();
        data.put("success", true);
        onSuccess(result, data);
    }

    // 预取号
    private void prefetchPhone(Map<String, Object> param, @NonNull MethodChannel.Result result) {
        PluginCallBack callBack = new PluginCallBack(result);
        GYManager.getInstance().ePreLogin(8000, new GyCallBack() {
            @Override
            public void onSuccess(GYResponse response) {
                android.util.Log.d("GYManager预登录", "初始化时 提前预登录成功 prelogin:" + response);
                Map<String, Object> data = new HashMap<>();
                data.put("success", true);
                callBack.onSuccess(data);
            }

            @Override
            public void onFailed(GYResponse response) {
                android.util.Log.d("GYManager预登录", "初始化时 提前预登录失败 prelogin:" + response);
                Map<String, Object> data = new HashMap<>();
                data.put("success", false);
                callBack.onSuccess(data);
            }
        });
    }

    // 一键登录
    private void loginPass(Map<String, Object> param, @NonNull MethodChannel.Result result) {
        new Handler(Looper.getMainLooper()).post(() -> {
            // 开始执行新的 Activity
            Intent intent = new Intent(context, MyELoginActivityFullscreen.class);
            intent.putExtra("testkey","testValue");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
            context.startActivity(intent);
        });
    }

    // 睡眠模式
    private void sleepSettings(Map<String, Object> param, @NonNull MethodChannel.Result result) {
        String mobileCode = (String) param.get("mobileCode");
        if ("xiaomi".equals(mobileCode)) {
            setXiaomiSleep(activity);
        } else {
            goIntentAppSetting(activity);
        }

        Map<String, Object> data = new HashMap<>();
        data.put("success", true);
        onSuccess(result, data);
    }

    // 保活设置
    private void systemSettings(Map<String, Object> param, @NonNull MethodChannel.Result result) {
        String mobileCode = (String) param.get("mobileCode");
        if ("xiaomi".equals(mobileCode)) {
            setXiaomiPower(activity);
        } else {
            goIntentAppDetailsSetting(activity);
        }
        
        Map<String, Object> data = new HashMap<>();
        data.put("success", true);
        onSuccess(result, data);
    }

    /**
     * 小米睡眠模式
     */
    private static void setXiaomiSleep(Activity pActivity) {
        try {
            Intent intent = new Intent("miui.intent.action.APP_PERM_EDITOR");
            ComponentName componentName = new ComponentName("com.miui.powerkeeper", "com.miui.powerkeeper.ui.ScenarioPowerSavingActivity");
            intent.setComponent(componentName);
            intent.putExtra("package_name", pActivity.getPackageName());
            intent.putExtra("package_label", pActivity.getResources().getString(R.string.app_name));
            pActivity.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
            goIntentAppSetting(pActivity);
        }
    }
    
    /**
     * 小米智能省电
     */
    private static void setXiaomiPower(Activity pActivity) {
        try {
            Intent intent = new Intent("miui.intent.action.APP_PERM_EDITOR");
            ComponentName componentName = new ComponentName("com.miui.powerkeeper", "com.miui.powerkeeper.ui.HiddenAppsConfigActivity");
            intent.setComponent(componentName);
            intent.putExtra("package_name", pActivity.getPackageName());
            intent.putExtra("package_label", pActivity.getResources().getString(R.string.app_name));
            pActivity.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
            goIntentAppDetailsSetting(pActivity);
        }
    }

    /**
     * 默认打开系统设置
     */
    private static void goIntentAppSetting(Activity pActivity) {
        try {
            Intent intent = new Intent();
            ComponentName componentName = new ComponentName("com.android.settings","com.android.settings.Settings");
            intent.setComponent(componentName);
            pActivity.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 默认打开应用详细页
     */
    private static void goIntentAppDetailsSetting(Activity pActivity) {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", pActivity.getPackageName(), null);
        intent.setData(uri);
        try {
            pActivity.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
