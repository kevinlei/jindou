package com.ayuyin.voice.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.g.gysdk.GYManager;
import com.g.gysdk.GYResponse;
import com.g.gysdk.GyCallBack;
import com.ayuyin.voice.R;
import com.ayuyin.voice.util.ViewUtil;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * author: fengchaoqun.
 * date: 2019/5/21 11:35 AM.
 */
public class PhoneNumberVerifyActivity extends BaseActivity {

    @BindView(R.id.et_input)
    EditText etInput;
    @BindView(R.id.ll_verify)
    View verifyView;

    @BindView(R.id.verify_result_pn)
    TextView verifyResultPn;
    @BindView(R.id.ll_result)
    View resultView;

    public static void start(Context context, String accessCode) {
        Intent intent = new Intent(context, PhoneNumberVerifyActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ViewUtil.setStatusBarTransparent(Color.TRANSPARENT, Color.TRANSPARENT, this);
        ViewUtil.setStatusBarLightMode(true, this);

        setContentView(R.layout.activity_cloud_verify);
        ButterKnife.bind(this);

        toolbarMoveDownward();

        etInput.setText("");
        verifyResultPn.setText("手机号码为：" + "");

        verifyView.setVisibility(View.VISIBLE);
        resultView.setVisibility(View.GONE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //关闭本机号码校验
//        GYManager.getInstance().cancelGetVerifyToken();
    }

    @OnClick(R.id.btn_next)
    public void onBtnNextClicked() {
        final String phone = etInput.getText().toString();
        if (TextUtils.isEmpty(phone) || phone.length() != 11) {
            showToast("请输入完整的待检测号码");
            return;
        }
        showLoadingDialog();
        //先获取本机号码校验通行证
        GYManager.getInstance().getVerifyToken(phone, 5000, new GyCallBack() {
            @Override
            public void onSuccess(final GYResponse response) {
                Log.d("getVerifyToken", "校验token成功 response:" + response);
                try {
                    JSONObject jsonObject = new JSONObject(response.getMsg());
                    String accessCode = jsonObject.getString("accessCode");
                    String processId = jsonObject.getString("process_id");
                    int operatorType = jsonObject.getInt("operatorType");
                    String phone = jsonObject.getString("phone");
                    //本机号码校验
                    verifyPhone(accessCode, processId, phone, operatorType);
                } catch (Throwable t) {
                    t.printStackTrace();
                    hideLoadingDialog();
                }
            }

            @Override
            public void onFailed(GYResponse response) {
                Log.e("getVerifyToken", "校验token失败 response:" + response);
                hideLoadingDialog();
                showFailDialog(response.getCode(), response.toString());
            }
        });
    }

    @OnClick(R.id.btn_back)
    public void onBtnBackClicked() {
        finish();
    }

    private void verifyResult(final boolean success, final String phone, final GYResponse response) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (success) {
                    showToast("校验成功");
                    hideLoadingDialog();

                    verifyView.setVisibility(View.GONE);
                    resultView.setVisibility(View.VISIBLE);
                    verifyResultPn.setText("手机号码为：" + phone);
                } else {
                    showToast("校验失败:" + response.getMsg());
                    hideLoadingDialog();

                    verifyResultPn.setText("手机号码为：" + "");
                }
            }
        });
    }

    private void verifyPhone(String accessCode, String processId, final String phone, int type) {
        GYManager.getInstance().verifyPhoneNumber(accessCode, processId, phone, type, new GyCallBack() {
            @Override
            public void onSuccess(GYResponse response) {
                Log.d("PhoneNumberVerifyActivi", "校验成功 response:" + response);
                verifyResult(true, phone, response);
            }

            @Override
            public void onFailed(GYResponse response) {
                Log.d("PhoneNumberVerifyActivi", "校验失败 response:" + response);
                verifyResult(false, phone, response);
            }
        });
    }

}
