package com.ayuyin.voice.elogin;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.ayuyin.voice.plugin.ChannelWrapper;
import com.g.gysdk.EloginActivityParam;
import com.g.gysdk.GYManager;
import com.g.gysdk.GYResponse;
import com.g.gysdk.GyCallBack;
import com.g.gysdk.GyPreloginResult;
import com.ayuyin.voice.R;
import com.ayuyin.voice.activity.BaseActivity;
import com.ayuyin.voice.util.ViewUtil;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * 一键登录授权页面
 */
public class MyELoginActivityFullscreen extends BaseActivity {
    private static String TAG =  MyELoginActivityFullscreen.class.getSimpleName();

    // 隐私协议的打勾
    private CheckBox mCheckBox;
    // 一键登录按钮
    private View mLoginBtn;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // 沉浸式设置
        ViewUtil.setStatusBarTransparent(Color.TRANSPARENT, Color.TRANSPARENT, this);
        ViewUtil.setStatusBarLightMode(true, this);
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP) {
            findViewById(android.R.id.content)
                    .setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
        }

        setContentView(R.layout.activity_login_fullscreen);

        // 沉浸式activitu，toolbar需要向下移动一个状态栏的高度
        toolbarMoveDownward();

        TextView numberTv = findViewById(R.id.number_textview);
        TextView sloganTv = findViewById(R.id.slogan_textview);
        View loginBtn = findViewById(R.id.login_button);
        CheckBox checkBox = findViewById(R.id.privacy_checkbox);
        TextView privacyTv = findViewById(R.id.privacy_textview);

        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "点击了隐私协议checkBox，当前状态：" + ((CheckBox)v).isChecked());
            }
        });

        mCheckBox = checkBox;
        // gysdk需要对{@mLoginBtn}设置 setOnClickListener，请不要自行设置；app的逻辑请写在EloginActivityParam.setLoginOnClickListener中
        mLoginBtn = loginBtn;
        initPrivacyTv(privacyTv);

        EloginActivityParam eloginActivityParam = new EloginActivityParam()
                .setActivity(this)
                .setNumberTextview(numberTv)
                .setSloganTextview(sloganTv)
                .setLoginButton(loginBtn)
                .setPrivacyCheckbox(checkBox)
                .setPrivacyTextview(privacyTv)
                .setUiErrorListener(new EloginActivityParam.UIErrorListener() {
                    @Override
                    public void onError(String msg) {
                        //隐私协议未打勾、界面不合规、setLoginOnClickListener抛出异常等情况下的回调
                        Toast.makeText(MyELoginActivityFullscreen.this, "请先同意隐私协议", Toast.LENGTH_SHORT).show();
                        Log.e(TAG, "UIErrorListener.onError:" + msg);
                        hideLoadingDialog();
                    }
                })
                .setLoginOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d(TAG, "一键登录按钮 onLoginClick:");
                        if (!mCheckBox.isChecked()) {
                            showToast("请先仔细阅读协议并勾选，然后再点击登录");
                            // 抛出异常，避免sdk进行后续登录动作（否则eAccountLogin会回调onFailed错误）
                            throw new IllegalStateException("请先仔细阅读协议并勾选，然后再点击登录");
                        }
                        //启动登录时候的转圈圈
                        showLoadingDialog();
                    }
                });

        // 请在预登录成功之后、设置好privacyTv的内容之后再调用eAccountLogin
        GYManager.getInstance().eAccountLogin(eloginActivityParam, 5000, new GyCallBack() {
            @Override
            public void onSuccess(GYResponse response) {
                Log.d(TAG, "登录成功 response:" + response);
                Map<String, Object> res = new HashMap<>();

                //停止登录时候的转圈圈
                hideLoadingDialog();
                //关闭一键登录界面
                finish();

                try {
                    JSONObject jsonObject = new JSONObject(response.getMsg());
                    JSONObject data = jsonObject.getJSONObject("data");
                    String token = data.getString("token");
                    String gyuid = response.getGyuid();
                    res.put("success", true);
                    res.put("token", token);
                    res.put("gyuid", gyuid);
                    ChannelWrapper.result.success(res);
                } catch (Exception e) {
                    e.printStackTrace();
                    res.put("success", false);
                    ChannelWrapper.result.success(res);
                }
            }

            @Override
            public void onFailed(GYResponse response) {
                Log.e(TAG, "登录失败 response:" + response);
                try {
                    JSONObject jsonObject = new JSONObject(response.getMsg());
                    int errorCode = jsonObject.getInt("errorCode");
                    //...
                } catch (Exception e) {
                    e.printStackTrace();
                }

                //停止登录时候的转圈圈
                hideLoadingDialog();
                //关闭一键登录界面
                finish();

                Map<String, Object> res = new HashMap<>();
                res.put("success", false);
                ChannelWrapper.result.success(res);
            }
        });
    }

    /**
     * 初始化隐私协议，请在onCreate/预登录成功之后&调用eAccountLogin之前就设置好，不允许动态改变
     * @param textView
     */
    private void initPrivacyTv(TextView textView) {
        textView.setLineSpacing(8.0F, 1.0F);
        textView.setMovementMethod(LinkMovementMethod.getInstance());

        GyPreloginResult preLoginResult = GYManager.getInstance().getPreLoginResult();

        textView.setText("");
        textView.append("登录即认可");
        textView.append(generateSpan(preLoginResult.getPrivacyName(), preLoginResult.getPrivacyUrl()));
        textView.append("并使用本机号码登录");
    }

    /**
     * 创建隐私协议的声明文本
     * @param name
     * @param url
     * @return
     */
    private SpannableString generateSpan(final String name, final String url) {
        SpannableString spannableString = new SpannableString(name);
        spannableString.setSpan(new ClickableSpan() {
            public void onClick(View view) {
                Log.d(TAG, "点击了隐私协议：" + name + "  " + url);
                MyELoginActivityPrivacyWeb.start(MyELoginActivityFullscreen.this, url, name);
            }

            public void updateDrawState(TextPaint ds) {
                try {
                    ds.setColor(0xFF3973FF);
                    ds.setUnderlineText(false);
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        }, 0, name.length(), 33);
        return spannableString;
    }

    /**
     * onDestroy
     */
    @Override
    public void onDestroy() {
        hideLoadingDialog();
        super.onDestroy();
    }
}
