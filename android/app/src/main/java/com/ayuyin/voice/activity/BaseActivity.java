package com.ayuyin.voice.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.ayuyin.voice.MyApplication;
import com.ayuyin.voice.R;
import com.ayuyin.voice.dialog.ErrorDialog;
import com.xiasuhuei321.loadingdialog.view.LoadingDialog;

/**
 * Created by wang on 17/4/11.
 */

public class BaseActivity extends AppCompatActivity {
    private FrameLayout frameLayout = null;
    protected Toolbar toolbar;
    private LoadingDialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_base);

        frameLayout = (FrameLayout) findViewById(R.id.layout_content);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("返回");
        toolbar.setTitleTextColor(Color.BLACK);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        dialog = new com.xiasuhuei321.loadingdialog.view.LoadingDialog(this);
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        frameLayout.removeAllViews();
        View.inflate(this, layoutResID, frameLayout);
        onContentChanged();
    }

    @Override
    public void setContentView(View view) {
        frameLayout.removeAllViews();
        frameLayout.addView(view);
        onContentChanged();
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        frameLayout.removeAllViews();
        frameLayout.addView(view, params);
        onContentChanged();
    }

    /**
     * 获取状态栏高度
     * @return
     */
    private int getStatusBarHeight() {
        int result = 0;
        //获取状态栏高度的资源id
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    /**
     * 沉浸式activitu，toolbar需要向下移动一个状态栏的高度
     */
    protected void toolbarMoveDownward() {
        int px = getStatusBarHeight();
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams)toolbar.getLayoutParams();
        layoutParams.topMargin = layoutParams.topMargin + px;
        toolbar.setLayoutParams(layoutParams);
    }

    public void showLoadingDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (dialog == null) {
                    dialog = new com.xiasuhuei321.loadingdialog.view.LoadingDialog(BaseActivity.this);
                }
                dialog.show();
            }
        });
    }

    public void hideLoadingDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideLoadingDialogInternal();
            }
        });
    }

    private void hideLoadingDialogInternal() {
        if (dialog != null) {
            dialog.close();
            dialog = null;
        }
    }

    public void showFailDialog(final int code, final String msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideLoadingDialogInternal();
                if (TextUtils.isEmpty(msg)) {
                    ErrorDialog.getInstance(BaseActivity.this).show(String.valueOf(code));
                } else {
                    ErrorDialog.getInstance(BaseActivity.this).show(msg);
                }
            }
        });
    }

    public void showToast(final String msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MyApplication.getMyApplicationContext(), msg, Toast.LENGTH_LONG).show();
            }
        });
    }

}
