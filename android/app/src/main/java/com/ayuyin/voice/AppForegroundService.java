package com.ayuyin.voice;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;

import androidx.annotation.Nullable;

public class AppForegroundService extends Service {
    private static final String NOTIFICATION_ID = "AppForegroundService";
    private static final String NOTIFICATION_NAME = "AppForegroundService";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        NotificationManager notificationManager = (NotificationManager)
                getSystemService(Context.NOTIFICATION_SERVICE);
        //创建NotificationChannel
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(NOTIFICATION_ID,
                    NOTIFICATION_NAME, NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(channel);
        }
        //Android9.0会报Caused by: java.lang.SecurityException: Permission Denial:
        //android.permission.FOREGROUND_SERVICE---AndroidManifest.xml中需要申请此权限
        //<uses-permission android:name="android.permission.FOREGROUND_SERVICE" />
        startForeground(1, getNotification());
    }

    //这个必须加，不能设置为null
    private Notification getNotification() {
        Notification.Builder builder = new Notification.Builder(this);
//                .setSmallIcon(R.drawable.ic_default)
//                .setContentTitle("测试服务")
//                .setContentText("我正在运行");
        //设置Notification的ChannelID,否则不能正常显示
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setChannelId(NOTIFICATION_ID);
        }
        return builder.build();
    }
}
