package com.ayuyin.voice.plugin;

import io.flutter.plugin.common.MethodChannel;

public interface WrapperCallBack {

    void onSuccess(Object object);

    void onError(String desc);
}

class PluginCallBack implements WrapperCallBack {

    PluginCallBack(MethodChannel.Result result) {
        this.result = result;
    }

    MethodChannel.Result result;

    void post(Runnable runnable) {
        PluginWrapper.handler.post(runnable);
    }

    public void onSuccess(Object object) {
        post(() -> result.success(object));
    }

    public void onError(String desc) {
        post(() -> result.error("0", "PluginError", desc));
    }


}
