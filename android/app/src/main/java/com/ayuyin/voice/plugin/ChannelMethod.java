package com.ayuyin.voice.plugin;

public class ChannelMethod {
    // 初始化登录
    static final String initLogin = "initLogin";
    // 预取号
    static final String prefetchPhone = "prefetchPhone";
    // 快速登录
    static final String loginPass = "loginPass";
    // 保活设置
    static final String systemSettings = "systemSettings";
    // 睡眠模式
    static final String sleepSettings = "sleepSettings";
}
