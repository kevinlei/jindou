package com.ayuyin.voice.view;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ayuyin.voice.R;

import java.util.List;

public class ChooseAdapter extends RecyclerView.Adapter<ChooseAdapter.ViewHolder> {
    private  List<Choose> mChooseList;
    static class ViewHolder extends RecyclerView.ViewHolder{
        ImageView ChooseImage;
        TextView ChooseName;
        TextView ChooseTip;

        public ViewHolder (View view)
        {
            super(view);
            ChooseImage = (ImageView) view.findViewById(R.id.choose_image);
            ChooseName = (TextView) view.findViewById(R.id.choose_name);
            ChooseTip = (TextView) view.findViewById(R.id.choose_tip);
        }

    }

    public  ChooseAdapter (List <Choose> ChooseList){
        mChooseList = ChooseList;
    }

    @Override

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.choose_item,parent,false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position){
        Choose Choose = mChooseList.get(position);
        holder.ChooseImage.setImageResource(Choose.getImageId());
        holder.ChooseName.setText(Choose.getName());
        holder.ChooseTip.setText(Choose.getTip());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mItemClickListener.onItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount(){
        return mChooseList.size();
    }

    private ItemClickListener mItemClickListener ;
    public interface ItemClickListener{
        public void onItemClick(int position) ;
    }
    public void setOnItemClickListener(ItemClickListener itemClickListener){
        this.mItemClickListener = itemClickListener ;

    }
}
