package com.ayuyin.voice.view;

public class Choose {

    private String name;
    private String tip;
    private int imageId;

    public Choose(String name, String tip, int imageId){
        this.name = name;
        this.tip = tip;
        this.imageId = imageId;

    }

    public String getName() {
        return name;
    }

    public int getImageId() {
        return imageId;
    }

    public String getTip() {
        return tip;
    }
}