package com.ayuyin.voice.plugin;

import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;
import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;
import io.flutter.plugin.common.MethodChannel;


public class PluginWrapper implements FlutterPlugin, ActivityAware {

    static final Handler handler = new Handler(Looper.getMainLooper());
    public static MethodChannel channel;
    private ChannelWrapper wrapper;

    @Override
    public void onAttachedToEngine(@NonNull FlutterPluginBinding binding) {
        channel = new MethodChannel(binding.getBinaryMessenger(), "ub.plugin.wrapper");
        wrapper = new ChannelWrapper(binding);
        channel.setMethodCallHandler(wrapper);
    }

    @Override
    public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
        if (channel != null) {
            channel.setMethodCallHandler(null);
            wrapper = null;
            channel = null;
        }
    }

    @Override
    public void onAttachedToActivity(@NonNull ActivityPluginBinding binding) {
        if (wrapper != null) {
            wrapper.setActivity(binding.getActivity());
        }
    }

    @Override
    public void onDetachedFromActivityForConfigChanges() {
        onDetachedFromActivity();
    }

    @Override
    public void onReattachedToActivityForConfigChanges(@NonNull ActivityPluginBinding binding) {
        onAttachedToActivity(binding);
    }

    @Override
    public void onDetachedFromActivity() {
        if (wrapper != null) {
            wrapper.onDestroy();
        }
    }

}




