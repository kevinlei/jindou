#import "SmFraudPlugin.h"
#import "SmAntiFraud.h"

@implementation SmFraudPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  FlutterMethodChannel* channel = [FlutterMethodChannel
      methodChannelWithName:@"sm_fraud"
            binaryMessenger:[registrar messenger]];
  SmFraudPlugin* instance = [[SmFraudPlugin alloc] init];
  [registrar addMethodCallDelegate:instance channel:channel];
}

-(NSString *) safe:(NSString*) str {
    if (str == nil || str.length == 0) {
        return @"";
    }
    return str;
}

- (void)handleMethodCall:(FlutterMethodCall*)call result:(FlutterResult)result {
  if ([@"getPlatformVersion" isEqualToString:call.method]) {
    result([@"iOS " stringByAppendingString:[[UIDevice currentDevice] systemVersion]]);
  } else if ([@"create" isEqualToString:call.method]) {
      NSDictionary* optionDic = call.arguments;
      SmOption *option = [[SmOption alloc] init];
      
      NSString *org = [self safe:optionDic[@"organization"]];
      [option setOrganization:org];
      
      NSString *channel = [self safe:optionDic[@"channel"]];
      [option setChannel:channel];
      
      NSString *url = [self safe:optionDic[@"url"]];
      [option setUrl:url];
      
      NSString *confurl = [self safe:optionDic[@"confUrl"]];
      [option setConfUrl:confurl];
      
      NSString *appid = [self safe:optionDic[@"appId"]];
      [option setAppId:appid];
      
      NSString *publicKey = [self safe:optionDic[@"publicKeyIOS"]];
      [option setPublicKey:publicKey];
      
      NSArray *notCollect = optionDic[@"notCollect"];
      [option setNotCollect:notCollect];

      NSString *area = optionDic[@"area"];
      if ([area isEqualToString:@"xjp"]) {
          [option setArea:AREA_XJP];
      } else if ([area isEqualToString:@"fjny"]) {
          [option setArea:AREA_FJNY];
      }
      
      if([optionDic objectForKey:@"encryptVer"]){
          NSString *sdkver = [[SmAntiFraud getSDKVersion] stringByReplacingOccurrencesOfString:@"." withString:@"0"];
          if ([sdkver intValue] >= 30008) {
              NSInteger encryptVer = [optionDic[@"encryptVer"] intValue];
              //[option setEncryptVer:encryptVer];
              [option performSelector:@selector(setEncryptVer:) withObject:@(encryptVer)];
          }
      }
      
      if([optionDic objectForKey:@"transport"]){
          BOOL transport= [optionDic[@"transport"] boolValue];
          [option setTransport:transport];
      }
      
      if([optionDic objectForKey:@"cloudConf"]){
          BOOL cloudConf = [optionDic[@"cloudConf"] boolValue];
          [option setCloudConf:cloudConf];
      }
      
      if([optionDic objectForKey:@"useHttps"]){
          BOOL useHttps = [optionDic[@"useHttps"] boolValue];
          [option setUseHttps:useHttps];
      }
      
      [[SmAntiFraud shareInstance] create:option];
  } else if ([@"getSDKVersion" isEqualToString:call.method]) {
      NSString *sdkver = [SmAntiFraud getSDKVersion];
      result(sdkver);
  } else if ([@"getDeviceId" isEqualToString:call.method]) {
      NSString *did = [[SmAntiFraud shareInstance] getDeviceId];
      result(did);
  }else {
    result(FlutterMethodNotImplemented);
  }
}

@end
