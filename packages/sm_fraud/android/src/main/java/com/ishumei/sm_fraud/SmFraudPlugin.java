package com.ishumei.sm_fraud;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;

import com.ishumei.smantifraud.SmAntiFraud;

import java.util.HashSet;
import java.util.List;
import java.util.Map;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;

public class SmFraudPlugin implements FlutterPlugin, MethodCallHandler {
    private static final String TAG = "SmFltLog";

    private MethodChannel channel;
    private Context mCtx;

    private static final String Method_ID_SmAntiFraud_create = "create";
    private static final String Method_ID_SmAntiFraud_getDeviceId = "getDeviceId";
    private static final String Method_ID_SmAntiFraud_getSDKVersion = "getSDKVersion";

    private static final String OPTION_ORG = "organization";
    private static final String OPTION_CHANNEL = "channel";
    private static final String OPTION_URL = "url";
    private static final String OPTION_CONFURL = "confUrl";
    private static final String OPTION_APPID = "appId";
    private static final String OPTION_PUBLICKEY = "publicKeyAndroid";
    private static final String OPTION_AINFO_KEY = "aInfoKeyAndroid";
    private static final String OPTION_NOTCOLLECT = "notCollect";
    private static final String OPTION_AREA = "area";
    private static final String OPTION_TRANSPORT = "transport";
    private static final String OPTION_CLOUDCONF = "cloudConf";
    private static final String OPTION_USE_HTTPS = "useHttps";

    // 3.0.6 版本无此值
    private static final String OPTION_AINFOKEY = "ainfoKey";
    // 3.0.6 版本无此值
    private static final String OPTION_ENCRYPTVER = "encryptVer";

    @Override
    public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
        mCtx = flutterPluginBinding.getApplicationContext();
        channel = new MethodChannel(flutterPluginBinding.getBinaryMessenger(), "sm_fraud");
        channel.setMethodCallHandler(this);
    }

    @Override
    public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
        if (mCtx == null) {
            Log.e(TAG, "MethodeId: " + call.method + ", ctx is null.");
        }

        switch (call.method) {
            case "getPlatformVersion":
                result.success("Android " + android.os.Build.VERSION.RELEASE);
                break;
            case Method_ID_SmAntiFraud_create:
                try {
                    @SuppressWarnings("unchecked")
                    Map<String, Object> args = (Map<String, Object>) call.arguments;
                    initSmSDK(args);
                    result.success(null);
                } catch (Exception e) {
                    Log.e(TAG, "Method_ID_SmAntiFraud_create: ", e);
                }
                break;
            case Method_ID_SmAntiFraud_getDeviceId:
                String deviceId = SmAntiFraud.getDeviceId();
                if (TextUtils.isEmpty(deviceId)) {
                    Log.e(TAG, "Method_ID_SmAntiFraud_getDeviceId: deviceId is null.");
                }
                result.success(deviceId);
                break;
            case Method_ID_SmAntiFraud_getSDKVersion:
                result.success(SmAntiFraud.getSDKVersion());
                break;
            default:
                result.notImplemented();
                break;
        }
    }

    private void initSmSDK(Map<String, Object> params) {
        SmAntiFraud.SmOption option = new SmAntiFraud.SmOption();
        if (isNotEmptyStr(params, OPTION_ORG)) {
            option.setOrganization((String) params.get(OPTION_ORG));
        }
        if (isNotEmptyStr(params, OPTION_APPID)) {
            option.setAppId((String) params.get(OPTION_APPID));
        }
        if (isNotEmptyStr(params, OPTION_PUBLICKEY)) {
            option.setPublicKey((String) params.get(OPTION_PUBLICKEY));
        }
        if (isNotEmptyStr(params, OPTION_AREA)) {
            option.setArea((String) params.get(OPTION_AREA));
        }
        if (isNotEmptyStr(params, OPTION_CHANNEL)) {
            option.setChannel((String) params.get(OPTION_CHANNEL));
        }
        if (isNotEmptyStr(params, OPTION_URL)) {
            option.setUrl((String) params.get(OPTION_URL));
        }
        if (isNotEmptyStr(params, OPTION_CONFURL)) {
            option.setConfUrl((String) params.get(OPTION_CONFURL));
        }
        if (params.containsKey(OPTION_CLOUDCONF)) {
            //noinspection ConstantConditions
            option.setCloudConf((Boolean) params.get(OPTION_CLOUDCONF));
        }
        if (params.containsKey(OPTION_TRANSPORT)) {
            //noinspection ConstantConditions
            option.setTransport((Boolean) params.get(OPTION_TRANSPORT));
        }
        if (params.containsKey(OPTION_USE_HTTPS)) {
            //noinspection ConstantConditions
            option.setUsingHttps((Boolean) params.get(OPTION_USE_HTTPS));
        }
        if (params.containsKey(OPTION_NOTCOLLECT)) {
            @SuppressWarnings("unchecked")
            List<String> notCollect = (List<String>) params.get(OPTION_NOTCOLLECT);
            if (notCollect != null && !notCollect.isEmpty()) {
                option.setNotCollect(new HashSet<>(notCollect));
            }
        }
        SmAntiFraud.create(mCtx, option);
    }

    private boolean isNotEmptyStr(Map<String, Object> map, String key) {
        Object v = map.get(key);
        if (v == null) {
            return false;
        } else if (v instanceof String) {
            return !((String) v).isEmpty();
        } else {
            return false;
        }
    }

    @Override
    public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
        channel.setMethodCallHandler(null);
    }
}
