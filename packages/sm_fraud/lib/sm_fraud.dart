
import 'dart:async';

import 'package:flutter/services.dart';

class SmFraud {
  static const MethodChannel _channel =
      const MethodChannel('sm_fraud');

static const OPTION_ORG = "organization";
static const OPTION_CHANNEL = "channel";
static const OPTION_URL = "url";
static const OPTION_CONFURL = "confUrl";
static const OPTION_APPID = "appId";
static const OPTION_AINFOKEY_ANDROID = "aInfoKeyAndroid";
static const OPTION_PUBLICKEY_ANDROID = "publicKeyAndroid";
static const OPTION_PUBLICKEY_IOS = "publicKeyIOS";
static const OPTION_NOTCOLLECT = "notCollect";
static const OPTION_AREA = "area";
static const OPTION_ENCRYPTVER = "encryptVer";
static const OPTION_TRANSPORT = "transport";
static const OPTION_CLOUDCONF = "cloudConf";
static const OPTION_USE_HTTPS = "useHttps";
static const OPTION_AINFOKEY = "ainfoKey";

static const ENCRYPT_V2 = 0;
static const ENCRYPT_V3 = 1;

static const SM_AREA_BJ = "bj";
static const SM_AREA_FJNY = "xjp";
static const SM_AREA_XJP = "fjny";

  static Future<String?> get platformVersion async {
    final String? version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Future<String?> getSDKVersion() async {
    final String? sdkver = await _channel.invokeMethod('getSDKVersion');
    return sdkver;
  }

  static Future<void> create({required Map optionDic}) async {
    return await _channel.invokeMethod('create',optionDic);
  }

  static Future<String?> getDeviceId() async {
    final String? deviceId = await _channel.invokeMethod('getDeviceId');
    return deviceId;
  }

}
