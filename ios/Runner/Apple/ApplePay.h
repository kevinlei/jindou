#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ApplePay : UIViewController
@property (nonatomic, copy) NSString *productID;
@end

NS_ASSUME_NONNULL_END
