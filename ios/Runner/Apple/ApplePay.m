#import "ApplePay.h"

@interface ApplePay()<SKProductsRequestDelegate,SKPaymentTransactionObserver>

@end


@implementation ApplePay


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColor.clearColor;
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    if ([SKPaymentQueue canMakePayments]) {
        NSArray *productArray = [[NSArray alloc] initWithObjects:self.productID,nil];
        NSSet *nsset = [NSSet setWithArray:productArray];
        SKProductsRequest *request = [[SKProductsRequest alloc] initWithProductIdentifiers:nsset];
        request.delegate = self;
        [request start];
    } else {
        [[PluginResult defaultManager] responesResult:@{@"code": @(-1), @"msg": @"当前环境不支持支付"}];
        // SKPaymentQueue can not make payments
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}


- (void)dealloc
{
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
}

- (void)productsRequest:(nonnull SKProductsRequest *)request didReceiveResponse:(nonnull SKProductsResponse *)response {
    NSArray *productArray = response.products;
    if([productArray count] == 0){
        // 没有商品信息
        dispatch_async(dispatch_get_main_queue(), ^{
            [[PluginResult defaultManager] responesResult:@{@"code": @(-1), @"msg": @"没有该商品信息"}];
            [self dismissViewControllerAnimated:NO completion:nil];
        });
        return;
    }
    SKProduct *payProduct;
    for (SKProduct *product in productArray) {
        if([product.productIdentifier isEqualToString:self.productID]){
            payProduct = product;
            break;
        }
    }
    if (payProduct == NULL) {
        // 没有商品信息
        dispatch_async(dispatch_get_main_queue(), ^{
            [[PluginResult defaultManager] responesResult:@{@"code": @(-1), @"msg": @"没有该商品信息"}];
            [self dismissViewControllerAnimated:NO completion:nil];
        });
        return;
    }
    [self requestBuyProduct:payProduct];
}


#pragma mark- 发起购买
-(void)requestBuyProduct:(SKProduct *) product {
    SKMutablePayment *payment = [SKMutablePayment paymentWithProduct:product];
    payment.quantity = 1;
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

- (void)paymentQueue:(nonnull SKPaymentQueue *)queue updatedTransactions:(nonnull NSArray<SKPaymentTransaction *> *)transactions {
    for(SKPaymentTransaction *tran in transactions) {
        switch (tran.transactionState) {
            case SKPaymentTransactionStatePurchased:
                {
                    NSData *data = [NSData dataWithContentsOfURL:[[NSBundle mainBundle] appStoreReceiptURL]];
                    NSString *receipt = [data base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
                    NSLog(@"苹果支付回调:%@", receipt);
                    [queue finishTransaction:tran];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[PluginResult defaultManager] responesResult:@{@"code": @(0), @"msg": receipt}];
                        [self dismissViewControllerAnimated:NO completion:nil];
                    });
                }
                break;
            case SKPaymentTransactionStatePurchasing:
                
                break;
            case SKPaymentTransactionStateRestored:
                
                break;
            case SKPaymentTransactionStateFailed:
                {
                    // 支付错误
                    [queue finishTransaction:tran];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[PluginResult defaultManager] responesResult:@{@"code": @(-1), @"msg": @"支付错误"}];
                        [self dismissViewControllerAnimated:NO completion:nil];
                    });
                }
                break;
            default:
                break;
        }
    }
}


- (void)request:(SKRequest *)request didFailWithError:(NSError *)error {
    // 请求错误
    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissViewControllerAnimated:NO completion:nil];
    });
}

@end
