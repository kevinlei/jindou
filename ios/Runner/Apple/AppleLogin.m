#import "AppleLogin.h"
#import <AuthenticationServices/AuthenticationServices.h>


@interface AppleLogin()<ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding>

@end

@implementation AppleLogin

- (void)viewDidLoad {
    [super viewDidLoad];
    [self startAppleLogin];
}

-(void)startAppleLogin {
    if (@available(iOS 13.0, *)) {
        ASAuthorizationAppleIDProvider *appleIDProvider = [[ASAuthorizationAppleIDProvider alloc]init];
        ASAuthorizationAppleIDRequest *request = [appleIDProvider createRequest];
        request.requestedScopes = @[ASAuthorizationScopeFullName,ASAuthorizationScopeEmail];
        ASAuthorizationController *auth = [[ASAuthorizationController alloc]initWithAuthorizationRequests:@[request]];
        auth.delegate = (id<ASAuthorizationControllerDelegate>)self;
        auth.presentationContextProvider = (id<ASAuthorizationControllerPresentationContextProviding>)self;
        [auth performRequests];
    }else {
        // 提示用户 暂不支持iOS 13以下机型使用苹果登录
        [[PluginResult defaultManager] responesResult:@{@"success":@(NO), @"tips":@"暂不支持iOS13以下机型"}];
    }
}



- (nonnull ASPresentationAnchor)presentationAnchorForAuthorizationController:(nonnull ASAuthorizationController *)controller  API_AVAILABLE(ios(13.0)){
    return [UIApplication sharedApplication].windows.firstObject;
}

- (void)authorizationController:(ASAuthorizationController *)controller didCompleteWithError:(NSError *)error  API_AVAILABLE(ios(13.0)){
    [[PluginResult defaultManager] responesResult:@{@"success":@(NO), @"tips":@"授权错误"}];
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)authorizationController:(ASAuthorizationController *)controller didCompleteWithAuthorization:(ASAuthorization *)authorization  API_AVAILABLE(ios(13.0)){
    if([authorization.credential isKindOfClass:[ASAuthorizationAppleIDCredential class]]){
        ASAuthorizationAppleIDCredential *apple = authorization.credential;
        NSString *token = apple.user;
        [[PluginResult defaultManager] responesResult:@{@"success":@(YES), @"tips":token}];
    }else {
        [[PluginResult defaultManager] responesResult:@{@"success":@(NO), @"tips":@"暂不支持iOS 13以下机型"}];
    }
    [self dismissViewControllerAnimated:NO completion:nil];
}

@end
