#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <Flutter/Flutter.h>



NS_ASSUME_NONNULL_BEGIN

@interface PluginVerifyView : NSObject<FlutterPlatformView>

-(void)createWithFrame: (CGRect)frame viewIdentifier:(int64_t)viewId arguments:(id _Nullable)args;

-(void)startVerifyCheck;

-(void)stopVerifyCheck;

@end

NS_ASSUME_NONNULL_END
