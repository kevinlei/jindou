
#import "PluginVerifyView.h"

@interface PluginVerifyView()
{
    UIView              *verifyView;
    UIImageView         *imageView;     ///< 扫描背景
}

@property (nonatomic, strong) NTESLiveDetectManager     *manager;       ///< 活体检测管理器
@property (nonatomic, strong) NSString                  *verifyID;      ///< 活体检测ID
@property (nonatomic, assign) NSUInteger                actionCount;    ///< 已完成的动作个数
@property (nonatomic, copy) NSString                    *actions;       ///< 动作序列
@property (nonatomic, copy) NSArray                     *actionAudios;  ///< 动作音效
@property (nonatomic, strong) AVAudioPlayer             *audioPlayer;
@end


@implementation PluginVerifyView


- (void)createWithFrame:(CGRect)frame viewIdentifier:(int64_t)viewId arguments:(id)args {
    
    NSDictionary * param = args;
    self.verifyID = [param valueForKey:@"verifyID"];
    
    verifyView = [[UIView alloc] initWithFrame:frame];
    [verifyView setBackgroundColor: UIColor.clearColor];
    
    imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"verifyCheck"]];
    imageView.layer.cornerRadius = 110;
    imageView.layer.masksToBounds = YES;
    [verifyView addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(verifyView.mas_centerX);
        make.width.mas_equalTo(220);
        make.height.mas_equalTo(220);
    }];
    
    self.actionCount = 0;
    self.actionAudios = @[@"", @"turn-right", @"turn-left", @"open-mouth", @"open-eyes"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onEnterBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onVerifyStatusChange:) name:@"NTESLDNotificationStatusChange" object:nil];
}


#pragma mark- 开始扫描
- (void) startVerifyCheck {
    self.manager = [[NTESLiveDetectManager alloc] initWithImageView:imageView withDetectSensit:NTESSensitNormal];
    [self.manager setTimeoutInterval:30];
    __weak __typeof(self)weakSelf = self;
    [self.manager startLiveDetectWithBusinessID:self.verifyID actionsHandler:^(NSDictionary * _Nonnull params) {
        weakSelf.actions = [params objectForKey:@"actions"];
    } completionHandler:^(NTESLDStatus status, NSDictionary * _Nullable params) {
        switch (status) {
            case NTESLDCheckPass:
                {
                    [weakSelf returnVerifyResult:[params objectForKey:@"token"] success:YES];
                }
                break;
            case NTESLDCheckNotPass:
                {
                    [weakSelf returnVerifyResult:@"活体检测不通过" success:NO];
                }
                break;
            case NTESLDNonGateway:
                {
                    [weakSelf returnVerifyResult:@"网络未连接" success:NO];
                }
                break;
            case NTESLDCameraNotAvailable:
                {
                    [weakSelf returnVerifyResult:@"未获取到相机权限" success:NO];
                }
                break;
            case NTESLDOperationTimeout:
                {
                    [weakSelf returnVerifyResult:@"检测超时" success:NO];
                }
            default:
                {
                    [weakSelf returnVerifyResult:@"检测错误" success:NO];
                }
                break;
        }
    }];
}


#pragma mark- 停止检测
-(void) stopVerifyCheck {
    if (self.manager) {
        [self.manager stopLiveDetect];
    }
}


#pragma mark- 检测状态改变
- (void) onVerifyStatusChange:(NSNotification *)notification {
    NSDictionary *actionInfo = notification.userInfo;
    actionInfo = [actionInfo objectForKey:@"action"];
    NSNumber *actionKey = [[actionInfo allKeys] firstObject];
    BOOL actionStatus = [[actionInfo objectForKey:actionKey] boolValue];
    
    if (actionStatus) {
        self.actionCount++;
        if (self.actionCount < self.actions.length) {
            NSString *action = [self.actions substringWithRange:NSMakeRange(self.actionCount, 1)];
            [self playVerifyAudio:[self.actionAudios objectAtIndex:[action integerValue]]];
        }
    }
    switch ([actionKey intValue]) {
        case 0:
            //{self.tipsLabel.text = @"请正对手机屏幕";}
            break;
        case 1:
            [self returnVerifyResult:@"请缓慢向右转头" success:NO];
            break;
        case 2:
            [self returnVerifyResult:@"请缓慢向左转头" success:NO];
            break;
        case 3:
            [self returnVerifyResult:@"请张张嘴" success:NO];
            break;
        case 4:
            [self returnVerifyResult:@"请眨眨眼" success:NO];
            break;
        default:
       
            break;
    }
}

#pragma mark- 进入后台
- (void) onEnterBackground {
    if (self.manager) {
        [self.manager stopLiveDetect];
    }
}


#pragma mark- 返回认证结果
- (void) returnVerifyResult:(NSString *)tips success:(BOOL) success {
    /// 判断当前线程是否在主线程,如果在主线程直接执行
    if (strcmp(dispatch_queue_get_label(DISPATCH_CURRENT_QUEUE_LABEL), dispatch_queue_get_label(dispatch_get_main_queue())) == 0) {
        if (success) {
            [[PluginResult defaultManager] callFlutterMethod:@"onVerifySuccess" arguments:tips];
        } else {
            [[PluginResult defaultManager] callFlutterMethod:@"onVerifyAction" arguments:tips];
        }
    } else {
        /// 如果当前不再主线程,将进程添加到主线程后执行
        dispatch_async(dispatch_get_main_queue(), ^{
            if (success) {
                [[PluginResult defaultManager] callFlutterMethod:@"onVerifySuccess" arguments:tips];
            } else {
                [[PluginResult defaultManager] callFlutterMethod:@"onVerifyAction" arguments:tips];
            }
        });
    }
}


#pragma mark- 播放音效
- (void) playVerifyAudio: (NSString *)audioName {
    NSURL *audioPath = [[NSBundle mainBundle] URLForResource:audioName withExtension:@"wav"];
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:audioPath error:nil];
    if (self.audioPlayer) {
        [self.audioPlayer prepareToPlay];
    }
    [self.audioPlayer play];
}


- (nonnull UIView *)view {
    return verifyView;
}

- (void)dealloc {
    NSLog(@"释放-------------");
    [self.manager stopLiveDetect];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
