
#import "PluginVerifyFactory.h"

@interface PluginVerifyFactory()
@property (nonatomic, strong) PluginVerifyView                   *view;
@end

@implementation PluginVerifyFactory

- (instancetype)initWith:(NSObject<FlutterPluginRegistrar> *)registrar {
    self = [super init];
    return self;
}

- (nonnull NSObject<FlutterPlatformView> *)createWithFrame:(CGRect)frame viewIdentifier:(int64_t)viewId arguments:(id _Nullable)args {
    self.view = [[PluginVerifyView alloc] init];
    [self.view createWithFrame:frame viewIdentifier:viewId arguments:args];
    return self.view;
}

- (PluginVerifyView *)getView {
    return self.view;
}


- (NSObject<FlutterMessageCodec> *)createArgsCodec {
    return [FlutterStandardMessageCodec sharedInstance];
}

@end
