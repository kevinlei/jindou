#import <Foundation/Foundation.h>
#import <Flutter/Flutter.h>
#import "PluginVerifyView.h"

NS_ASSUME_NONNULL_BEGIN

@interface PluginVerifyFactory : NSObject<FlutterPlatformViewFactory>

-(instancetype)initWith: (NSObject<FlutterPluginRegistrar> *)registrar;

-(PluginVerifyView *)getView;

@end

NS_ASSUME_NONNULL_END
