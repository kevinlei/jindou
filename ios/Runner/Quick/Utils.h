//
//  Utils.h
//  GyDemo
//
//  Created by admin on 2017/4/21.
//  Copyright © 2017年 getui. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


#define iOS7 ([[UIDevice currentDevice].systemVersion doubleValue] >= 7.0)
#define RGBCOLOR(r, g, b) [UIColor colorWithRed:(r) / 255.0 green:(g) / 255.0 blue:(b) / 255.0 alpha:1]

//获取屏幕 宽度、高度
#define kScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define kScreenHeight ([UIScreen mainScreen].bounds.size.height)
#define kScreenFrame ([UIScreen mainScreen].bounds)
#define kScreenScale ([UIScreen mainScreen].bounds.size.height / 812.0)

#define kMAXScreenSide MAX([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)

#define kIsiPhoneX (kScreenHeight>=812.0)
#define kNavigationHeight (kIsiPhoneX? 88: 64)      // 导航条高度

#pragma mark - 颜色
// rgb颜色转换（16进制->10进制）
#define kColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16)) / 255.0 green:((float)((rgbValue & 0xFF00) >> 8)) / 255.0 blue:((float)(rgbValue & 0xFF)) / 255.0 alpha:1.0f]
#define kColorFromRGBA(rgbValue, alphaValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16)) / 255.0 green:((float)((rgbValue & 0xFF00) >> 8)) / 255.0 blue:((float)(rgbValue & 0xFF)) / 255.0 alpha:alphaValue]

// 获取RGB颜色
#define kRGBA(r, g, b, a) [UIColor colorWithRed:r / 255.0f green:g / 255.0f blue:b / 255.0f alpha:a]
#define kRGB(r, g, b) kRGBA(r, g, b, 1.0f)


// 有效且有值的字符串
#define F_IsStringValue_GyValid(msg) ([msg isKindOfClass:[NSString class]] && msg.length > 0 ? YES : NO)


@interface Utils : NSObject

/** 设置视图圆角 */
+ (void)setViewRadius:(CGFloat)radius toView:(UIView *)view;

/**
 设置视图圆角、边框、边框颜色
 view:需要添加圆角和边框的视图，必须有
 radius:圆角半径，必须有，0：没有半径
 width:边框半径，必须有，0：没有边框
 color:边框颜色，可以nil，默认黑色
 */
+ (void)setViewRadius:(CGFloat)radius borderWidth:(CGFloat)width borderColor:(UIColor *)color toView:(UIView *)view;

+ (NSString *)toMD5String:(NSString *)source;

+ (NSString *)getDemoMode;
+ (NSString *)getInfoInPlistWithName:(NSString *)infoName;

+ (UIView *)gradientColorImage;

+ (UIImage *)imageWithColor:(UIColor *)color;


+(void)backsen;
/**
 * @interfaceOrientation 输入要强制转屏的方向
 */
+ (void)switchNewOrientation:(UIInterfaceOrientationMask)mask;

+ (NSString *)dic2Str:(NSDictionary *)dic;

+ (NSDictionary*)jsonData2Dic:(NSData*)data;

+ (NSData *)dic2JsonData:(NSDictionary *)dictionary;

+ (CAGradientLayer*)addGradient:(UIView*)view;
+ (CAGradientLayer*)addGradient:(UIView*)view frame:(CGRect)frame;

OLRect GYOLRectMake(CGFloat y,
                        CGFloat centerX,
                        CGFloat x,
                        CGFloat yLandscape,
                        CGFloat centerXLandscape,
                        CGFloat xLandscape,
                    CGSize size);
@end

@interface UIView(GYSDK)
- (void)addBorder:(CGFloat)width color:(UIColor*)color;
- (void)addCorner:(CGFloat)corner;



@end
