#import "UIColor+HexColor.h"

@implementation UIColor (HexColor)



+ (UIColor *)colorHexString:(NSString *)hexColor {
    return [self colorHexString:hexColor alpha:1];
}

+ (UIColor *)colorHexString:(NSString *)hexColor alpha:(float)alpha {
    
    NSString *colorStr = [[hexColor stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // 如果色值小于6位返回透明色
    if ([colorStr length] < 6) {
        return [UIColor clearColor];
    }
    // 判断字符串前缀是以什么开始的
    if ([colorStr hasPrefix:@"0X"]) {
        colorStr = [colorStr substringFromIndex:2]; // 截取范围是2--结尾
    }
    if ([colorStr hasPrefix:@"0x"]) {
        colorStr = [colorStr substringFromIndex:2]; // 截取范围是2--结尾
    }
    if ([colorStr hasPrefix:@"#"]) {
        colorStr = [colorStr substringFromIndex:1]; // 截取范围是1--结束
    }
    // 再次判断色值是否是6位数，如果不是返回透明色
    if ([colorStr length] == 8 && [colorStr hasPrefix:@"FF"]) {
        colorStr = [colorStr substringFromIndex:2];
    }
    
    if ([colorStr length] != 6) {
        return [UIColor clearColor];
    }
    
    // 从六位数值中找到RGB对应的位数并转换
    NSRange range;
    range.length = 2;
    
    // R,G,B
    range.location = 0;
    NSString *rStr = [colorStr substringWithRange:range];
    range.location = 2;
    NSString *gStr = [colorStr substringWithRange:range];
    range.location = 4;
    NSString *bStr = [colorStr substringWithRange:range];
    
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rStr] scanHexInt:&r];
    [[NSScanner scannerWithString:gStr] scanHexInt:&g];
    [[NSScanner scannerWithString:bStr] scanHexInt:&b];
    
    // 返回UIColor
    return [UIColor colorWithRed:((float) r / 255.0f) green:((float) g / 255.0f) blue:((float) b / 255.0f) alpha:alpha];
}

@end

