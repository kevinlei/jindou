//
//  Utils.m
//  GyDemo
//
//  Created by admin on 2017/4/21.
//  Copyright © 2017年 getui. All rights reserved.
//

#import <CommonCrypto/CommonDigest.h> // 系统加密库

@implementation Utils : NSObject

#pragma mark - 关于绘图方法

/** 设置视图圆角 */
+ (void)setViewRadius:(CGFloat)radius toView:(UIView *)view {
    [self setViewRadius:radius borderWidth:0 borderColor:nil toView:view];
}

/**
 设置视图圆角、边框、边框颜色
 view:需要添加圆角和边框的视图，必须有
 radius:圆角半径，必须有，0：没有半径
 width:边框半径，必须有，0：没有边框
 color:边框颜色，可以nil，默认黑色
 */
+ (void)setViewRadius:(CGFloat)radius borderWidth:(CGFloat)width borderColor:(UIColor *)color toView:(UIView *)view {
    if (!view || ![view isKindOfClass:[UIView class]])
        return; // 视图判断
    
    // 半径判断
    if (radius > 0) {
        [view.layer setMasksToBounds:YES];
        [view.layer setCornerRadius:radius]; // 设置圆角半径
    } else {
        [view.layer setMasksToBounds:NO];
        [view.layer setCornerRadius:0];
    }
    
    // 宽度判断
    if (width > 0) {                       // 边框宽度和颜色
        [view.layer setBorderWidth:width]; // 设置边框宽度
        
        UIColor *borderColor = color; // 边框颜色
        borderColor = color ? color : [UIColor blackColor];
        
        [view.layer setBorderColor:[borderColor CGColor]]; // 设置边框颜色
    } else {
        [view.layer setBorderWidth:0];   // 设置边框宽度
        [view.layer setBorderColor:nil]; // 设置边框颜色
    }
}

+ (NSString *)toMD5String:(NSString *)source {
    @try {
        if (!source || ![source isKindOfClass:[NSString class]]) {
            return nil;
        }
        
        const char *cStr = [source UTF8String];
        unsigned char result[16];
        CC_MD5(cStr, (CC_LONG)strlen(cStr), result);
        return [NSString stringWithFormat:
                @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
                result[0], result[1], result[2], result[3],
                result[4], result[5], result[6], result[7],
                result[8], result[9], result[10], result[11],
                result[12], result[13], result[14], result[15]];
    }
    @catch (NSException *exception) {
        NSLog(@"%@", exception);
        return nil;
    }
}

+ (NSString *)getDemoMode {
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"gysdkconfig" ofType:@"plist"];
    NSDictionary *dictionary = [[NSDictionary alloc] initWithContentsOfFile:plistPath];
    NSString *model = dictionary[@"model"];
    return model;
}

+ (NSString *)getInfoInPlistWithName:(NSString *)infoName {
    NSString *info = nil;
    @try {
        NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"gysdkconfig" ofType:@"plist"];
        NSDictionary *dictionary = [[NSDictionary alloc] initWithContentsOfFile:plistPath];
        NSString *model = dictionary[@"model"];
        NSDictionary *config = dictionary[model];
        if (config && config[infoName]) {
            info = config[infoName];
        }
    } @catch (NSException *exception) {
        NSLog(@"%@:%@", NSStringFromSelector(_cmd), exception.description);
    }
    return info;
}

+ (void)switchNewOrientation:(UIInterfaceOrientationMask)mask {
    UIInterfaceOrientation interfaceOrientation;
    if (mask & UIInterfaceOrientationMaskPortrait) {
        interfaceOrientation = UIInterfaceOrientationPortrait;
    } else if (mask & UIInterfaceOrientationMaskPortraitUpsideDown) {
        interfaceOrientation = UIInterfaceOrientationPortraitUpsideDown;
    } else if (mask & UIInterfaceOrientationMaskLandscapeRight) {
        interfaceOrientation = UIInterfaceOrientationLandscapeRight;
    } else if (mask & UIInterfaceOrientationMaskLandscapeLeft) {
        interfaceOrientation = UIInterfaceOrientationLandscapeLeft;
    } else {
        interfaceOrientation = UIInterfaceOrientationPortrait;
    }
    
    [[UIDevice currentDevice] setValue:@(UIInterfaceOrientationUnknown) forKey:@"orientation"];
    [[UIDevice currentDevice] setValue:@(interfaceOrientation) forKey:@"orientation"];
}

+ (NSString *)dic2Str:(NSDictionary *)dic {
    NSData *data = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:nil];
    NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    return str;
}

+ (NSDictionary*)jsonData2Dic:(NSData*)data {
   NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    return dic;
}

+ (NSData *)dic2JsonData:(NSDictionary *)dictionary {
    if (!dictionary || ![dictionary isKindOfClass:[NSDictionary class]]) {
        return nil;
    }
    
    @try {
        NSError *error = nil;
        // iOS5中，苹果引入了一个解析JSON串的NSJSONSerialization类
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:kNilOptions error:&error];
        if (!jsonData || error) {
            NSLog(@"JSONSerialization Error:%@", error);
            return nil;
        }
        
        return jsonData;
    }
    @catch (NSException *exception) {
        NSLog(@"%@", exception);
    }
    
    return nil;
}

+ (CAGradientLayer*)addGradient:(UIView*)view {
    return [self addGradient:view frame:view.bounds];
}



+ (UIImage *)gradientColorImage
{
    // 创建CGContextRef
    UIGraphicsBeginImageContext(kScreenFrame.size);
    CGContextRef gc = UIGraphicsGetCurrentContext();
    
    // 创建CGMutablePathRef
    CGMutablePathRef path = CGPathCreateMutable();
    
    // 绘制Path
    CGRect rect = CGRectMake(0.f, 0.f, [UIScreen mainScreen].bounds.size.width, [[UIApplication sharedApplication] statusBarFrame].size.height + kNavigationHeight);
    CGPathMoveToPoint(path, NULL, CGRectGetMinX(rect), CGRectGetMinY(rect));
    CGPathAddLineToPoint(path, NULL, CGRectGetMinX(rect), CGRectGetMaxY(rect));
    CGPathAddLineToPoint(path, NULL, CGRectGetMaxX(rect), CGRectGetMaxY(rect));
    CGPathAddLineToPoint(path, NULL, CGRectGetMaxX(rect), CGRectGetMinY(rect));
    CGPathCloseSubpath(path);
    
    // 绘制渐变
    [self drawLinearGradient:gc path:path startColor:[UIColor colorHexString:@"0xFFF46F5F6"].CGColor endColor:[UIColor colorHexString:@"0xFFFFF82B2"].CGColor];
    
    // 注意释放CGMutablePathRef
    CGPathRelease(path);
    
    // 从Context中获取图像
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return img;
}
+ (void)drawLinearGradient:(CGContextRef)context
                      path:(CGPathRef)path
                startColor:(CGColorRef)startColor
                  endColor:(CGColorRef)endColor
{
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGFloat locations[] = { 0.0, 1.0 };
    
    NSArray *colors = @[(__bridge id) startColor, (__bridge id) endColor];
    
    CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef) colors, locations);
    
    CGRect pathRect = CGPathGetBoundingBox(path);
    
    // 渐变方向可修改
    CGPoint startPoint = CGPointMake(CGRectGetMinX(pathRect), CGRectGetMidY(pathRect));
    CGPoint endPoint = CGPointMake(CGRectGetMaxX(pathRect), CGRectGetMidY(pathRect));
    
    CGContextSaveGState(context);
    CGContextAddPath(context, path);
    CGContextClip(context);
    CGContextDrawLinearGradient(context, gradient, startPoint, endPoint, 0);
    CGContextRestoreGState(context);
    
    CGGradientRelease(gradient);
    CGColorSpaceRelease(colorSpace);
}
+(void)backsen{
    NSLog(@"123");
}
+ (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();

    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);

    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return image;
}


+ (CAGradientLayer*)addGradient:(UIView*)view frame:(CGRect)frame {
   
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = frame;
    gradient.colors = @[(id)[UIColor colorWithRed:49/255.0 green:169/255.0 blue:255/255.0 alpha:1].CGColor,(id)[UIColor colorWithRed:6/255.0 green:112/255.0 blue:222/255.0 alpha:1].CGColor];
    gradient.startPoint = CGPointMake(0, 0);
    gradient.endPoint = CGPointMake(1, 0);
    [view.layer addSublayer:gradient];
    [view.layer insertSublayer:gradient atIndex:0];
    return gradient;
}

OLRect GYOLRectMake(CGFloat y,
                        CGFloat centerX,
                        CGFloat x,
                        CGFloat yLandscape,
                        CGFloat centerXLandscape,
                        CGFloat xLandscape,
                        CGSize size) {
    OLRect rect;
    rect.portraitTopYOffset = y;
    rect.portraitCenterXOffset = centerX;
    rect.portraitLeftXOffset = x;
    rect.landscapeTopYOffset = yLandscape;
    rect.landscapeCenterXOffset = centerXLandscape;
    rect.landscapeLeftXOffset = xLandscape;
    rect.size = size;
    return rect;
}
@end

@implementation UIView(GYSDK)

- (void)addBorder:(CGFloat)width color:(UIColor*)color {
    self.layer.borderColor = color.CGColor;
    self.layer.borderWidth = width;
}

- (void)addCorner:(CGFloat)corner {
    self.layer.cornerRadius = corner;
    self.layer.masksToBounds = YES;
}

@end
