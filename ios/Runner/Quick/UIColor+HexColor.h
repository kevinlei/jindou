#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIColor (HexColor)


/// 将十六进制颜色转换成UIColor
/// @param hexColor 十六进制颜色,如果字符串小于6位返回透明色,#可有可无,不区分大小写
+ (UIColor *)colorHexString:(NSString *)hexColor;


/// 将十六进制颜色转换成UIColor
/// @param hexColor 十六进制颜色,如果字符串小于6位返回透明色,#可有可无,不区分大小写
/// @param alpha 颜色透明度
+ (UIColor *)colorHexString:(NSString *)hexColor alpha:(float)alpha;

@end

NS_ASSUME_NONNULL_END
