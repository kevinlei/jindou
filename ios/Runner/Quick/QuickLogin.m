#import "QuickLogin.h"
#import <GeYanSdk/GeYanSdk.h>

@implementation QuickLogin


+ (void)checkLoginEnable:(FlutterMethodCall *)call {
    NSString * businessID = [call.arguments objectForKey:@"businessId"];
    [GeYanSdk startWithAppId:businessID
                    withCallback:^(BOOL isSuccess, NSError *error, NSString *gtcid) {
            NSLog(@"GeYanSdk startWithAppId gtcid:%@", error);
        NSLog(@"GeYanSdk startWithAppId gtcid:%@", gtcid);
            // GeYanSdk startWithAppId gtcid:gtc_dbf8471ad1ef4681bf9928a446784aa535
    }];
    BOOL shouldQuickLogin;
    if([[GeYanSdk currentNetworkInfo]objectForKey:@"network"]!=0){
        
        shouldQuickLogin= true;
    }else{
        shouldQuickLogin= false;
    }
    [[PluginResult defaultManager] responesResult:@{@"success":@(shouldQuickLogin)}];
}
+ (void)getPrefetchPhone {
     [GeYanSdk setDebug:YES];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [GeYanSdk preGetToken:^(NSDictionary * _Nonnull resultDic) {
            
            NSLog(@"%@==%@",resultDic,[resultDic objectForKey:@"msg"]);
            NSString* token = [NSString stringWithFormat:@"%@", [resultDic objectForKey:@"gyuid"]];
            NSString* message = [NSString stringWithFormat:@"%@", [resultDic objectForKey:@"msg"]];
            int type = [[NSString stringWithFormat:@"%@", [resultDic objectForKey:@"operatorType"]] intValue];
            NSString* number = [NSString stringWithFormat:@"%@", [resultDic objectForKey:@"number"]];
            
            int code = [[NSString stringWithFormat:@"%@", [resultDic objectForKey:@"code"]] intValue];
            //0：未知 1：移动  2：联通  3：电信
            if(code==30000){
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSLog(@"%@==%@==%@",token,[resultDic objectForKey:@"number"],@(type));
                    [[PluginResult defaultManager] callFlutterMethod:@"onQuickLoginSuccess" arguments:@{@"success":@(YES),@"token":token,@"result":resultDic,@"type":@(type)}];
                });
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[PluginResult defaultManager] callFlutterMethod:@"onQuickLoginSuccess" arguments:@{@"success":@(NO), @"result":message}];
                });
            }
        }];

      });

    [[PluginResult defaultManager] responesResult:@{@"success":@(NO), @"result":@"等待一键登录回调"}];
}


+ (void)getLoginToken {
    
    
    CGFloat yPercent = kScreenHeight / 896;//以iPhone11 为适配标准
    
    UIWindow *keyWindow = [UIApplication sharedApplication].windows.firstObject;
    // 自定义授权页面

    GyAuthViewModel *authViewModel = GyAuthViewModel.new;
    authViewModel.statusBarStyle = UIStatusBarStyleDefault;
    // Navigation/导航
//    authViewModel.naviTitle = [[NSAttributedString alloc] initWithString:@"登录"];
//    authViewModel.naviHidden=YES;
    authViewModel.naviBgColor= [UIColor colorWithPatternImage:[Utils  gradientColorImage]];
    
    authViewModel.naviBackImage = [UIImage imageNamed:@"backs.png"];
    //Logo/图标
    authViewModel.appLogo = [UIImage imageNamed:@"Logins"];
    CGFloat y = 100 * yPercent;
    authViewModel.logoRect =  GYOLRectMake(y, 0, 0, 0, 0, 0, CGSizeMake(250, 250));

    UIView *mups=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 100, 50)];
//    UIButton *send=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 100, 50)];
//    send.font=[UIFont systemFontOfSize:14];
//    [send setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [send setTitle:@"登录遇到问题？" forState:UIControlStateNormal];
//
//    [mups addSubview:send];
    
    authViewModel.naviRightControl = mups;
    
    
    authViewModel.logoHidden = NO;
    authViewModel.logoCornerRadius = 0;
    
    authViewModel.phoneNumColor= [UIColor whiteColor];
    //Phone Number Preview/手机号预览
    authViewModel.phoneNumFont = [UIFont boldSystemFontOfSize:24];


   

    //Authorization Button/认证按钮
        authViewModel.authButtonImages = @[
            //UIControlStateNormal
//            [UIImage imageNamed:@"chekbtn"],
//            [UIImage imageNamed:@"bg2"],
            [Utils imageWithColor:[UIColor whiteColor]],
            [Utils imageWithColor:[UIColor whiteColor]],
            [Utils imageWithColor:[UIColor whiteColor]],
        ];
    
    
    //    authViewModel.authButtonImages = @[
    //        //UIControlStateNormal
    //        [self imageWithColor:[UIColor blueColor]],
    //        //UIControlStateDisabled
    //        [self imageWithColor:[UIColor grayColor]],
    //        //UIControlStateHighlighted
    //        [self imageWithColor:[UIColor redColor]]
    //    ];
    authViewModel.webBackBtnHidden=NO;
    
    //Switch Button/切换按钮
    
    authViewModel.switchButtonText=@"";
    authViewModel.switchButtonHidden=YES;
    authViewModel.switchButtonColor=[UIColor whiteColor];
    authViewModel.switchButtonFont = [UIFont systemFontOfSize:15];
    CGFloat gans = 600 * yPercent;
    authViewModel.switchButtonRect=GYOLRectMake(gans, 0, 0, 0, 0, 0, CGSizeZero);
    
    
    
    authViewModel.webBackBtnImg =  [UIImage imageNamed:@"back_13x21_.png"];
    
    authViewModel.authButtonTitle = nil;
    authViewModel.authButtonTitle = [[NSAttributedString alloc] initWithString:@"一键登录/注册"
                                                       attributes:@{NSForegroundColorAttributeName: [UIColor colorHexString:@"0xFFFF753F"],
                                                                    NSFontAttributeName:[UIFont boldSystemFontOfSize:18]}];
    
    authViewModel.authButtonCornerRadius=20;
    //Slogan/口号标语

    CGFloat gan = 450 * yPercent;
    authViewModel.sloganRect =  GYOLRectMake(gan, 0, 0, 0, 0, 0, CGSizeZero);
    
    authViewModel.sloganTextColor = [UIColor whiteColor];
    authViewModel.sloganTextFont = [UIFont systemFontOfSize:12];

    // CheckBox & Privacy Terms/隐私条款勾选框及隐私条款
    CGFloat terms = 750 * yPercent;
    authViewModel.termsRect =  GYOLRectMake(terms, 0, 0, 0, 0, 0, CGSizeZero);
    
    
    authViewModel.defaultCheckBoxState = YES;
    authViewModel.checkBoxSize = CGSizeMake(20, 20);
    authViewModel.uncheckedImage = [UIImage imageNamed:@"chekbtn.png"];
    authViewModel.checkedImage = [UIImage imageNamed:@"chekbanbtn.png"];

    //Custom Area/自定义区域

    // Background Image/授权页面背景图片
    authViewModel.backgroundImage = [UIImage imageNamed:@"bg2.png"];
    //Orientation
    authViewModel.supportedInterfaceOrientations = UIInterfaceOrientationMaskPortrait;

    //present授权页面时的样式
    authViewModel.modalPresentationStyle = UIModalPresentationFullScreen;
    //进入授权页面的方式
    authViewModel.pullAuthVCStyle = OLPullAuthVCStyleModal;
    //暗黑模式
    authViewModel.userInterfaceStyle = @(0);


    //----------隐私条款----------------
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 7;
    paragraphStyle.alignment = NSTextAlignmentLeft;
    paragraphStyle.paragraphSpacing = 0.0;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.firstLineHeadIndent = 0.0;
    authViewModel.privacyTermsAttributes = @{
        NSForegroundColorAttributeName: UIColor.whiteColor,
        NSParagraphStyleAttributeName: paragraphStyle,
        NSFontAttributeName: [UIFont systemFontOfSize:12]
    };
    authViewModel.termTextColor = [UIColor grayColor];
    // 额外自定义服务条款，注意index属性，默认的index为0，SDK会根据index对多条服务条款升序排列，假如想设置服务条款顺序为 自定义服务条款1 默认服务条款
    GyAuthPrivacyItem *item1 = [[GyAuthPrivacyItem alloc] initWithTitle:@"《用户协议》"
                                                                linkURL:[NSURL URLWithString:@"https://api.338fk.com/data-api/api/v1/un-token/article/info?id=11"]
                                                                  index:1];
    // 加载本地的html
    NSURL *URL = [[NSBundle mainBundle] URLForResource:@"test.html" withExtension:nil];
    NSURLRequest *URLRequest = [NSURLRequest requestWithURL:URL];
    GyAuthPrivacyItem *item2 = [[GyAuthPrivacyItem alloc] initWithTitle:@"《隐私协议》"
                                                                linkURL:[NSURL URLWithString:@"https://api.338fk.com/data-api/api/v1/un-token/article/info?id=10"]
                                                                  index:2];
    
    GyAuthPrivacyItem *item3 = [[GyAuthPrivacyItem alloc] initWithTitle:@"《个人信息保护政策》"
                                                             urlRequest:URLRequest
                                                                  index:3
                                                                  block:nil];
    authViewModel.additionalPrivacyTerms = @[item1, item2];
    //----------------各种回调------------------
    authViewModel.tapAuthBackgroundBlock = ^() {
        NSLog(@"点击弹窗授权页面背景 回调");
    
    };
    authViewModel.clickAuthButtonBlock = ^() {
//        _startTime = NSDate.new.timeIntervalSince1970;
        NSLog(@"点击登录按钮 回调");
        return YES; // 3.0+增加了返回值
    };
    

    authViewModel.clickCheckboxBlock = ^(BOOL isChecked) {
        NSLog(@"点击checkbox 回调 %@", @(isChecked));
    };

    authViewModel.viewLifeCycleBlock = ^(NSString *viewLifeCycle, BOOL animated) {
        NSLog(@"页面生命周期 回调: %@, %@", viewLifeCycle, @(animated));
    };

    authViewModel.loadingViewBlock = ^(UIView *containerView) {
        NSLog(@"授权页面，加载进度条");
    };
    authViewModel.stopLoadingViewBlock = ^(UIView *containerView) {
        NSLog(@"授权页面，停止自定义加载进度条，调用[GeyanSDK stopLoading]之后的回调");
    };

    authViewModel.authVCTransitionBlock = ^(CGSize size, id <UIViewControllerTransitionCoordinator> coordinator, UIView *customAreaView) {
        NSLog(@"页面生命周期 回调: 自定义VIEW");
    };

    if ([GeYanSdk isPreGettedTokenValidate]) {
        // 一键登录预取号还在有效期，可直接调用oneTapLogin方法进行登录
        [GeYanSdk oneTapLogin:keyWindow.rootViewController withViewModel:authViewModel andCallback:^(NSDictionary *dic) {
            
            NSString *accessToken = [NSString stringWithFormat:@"%@", [dic objectForKey:@"token"]];
            NSString* message = [NSString stringWithFormat:@"%@", [dic objectForKey:@"msg"]];
            NSLog(@"666666666666");
            NSLog(@"%@",dic);
            [GeYanSdk stopLoading];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC * 1), dispatch_get_main_queue(), ^{

            });
            if ([dic[@"errorCode"] isEqualToNumber:@(-20302)]) {
                //退出登录页
                return;
            }
            if (![dic[@"code"] isEqualToNumber:@30000]) {

                return;
            }
            if (![dic[@"code"] isEqualToNumber:@30000]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                                   [[PluginResult defaultManager] callFlutterMethod:@"onLoginPassSuccess" arguments:@{@"success":@(NO), @"message":message}];
                               });
                return;
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                                [[PluginResult defaultManager] callFlutterMethod:@"onLoginPassSuccess" arguments:@{@"success":@(YES), @"message":accessToken}];
                            });
//            [self queryPhoneNumber:dic];
            [GeYanSdk closeAuthVC:YES completion:^{
                NSLog(@"关闭授权页面");

            }];

        }];
//        return;
    }
    [GeYanSdk preGetToken:^(NSDictionary *preDic) {
        NSLog(@"preGetToken: %@ %@", preDic, preDic[@"msg"]);
        if (![GeYanSdk isPreGettedTokenValidate]) {
            [[PluginResult defaultManager] responesResult:@{@"success":@(NO), @"message":preDic[@"msg"]}];
            return;
        }
        [GeYanSdk oneTapLogin:keyWindow.rootViewController withViewModel:authViewModel andCallback:^(NSDictionary * dic) {
            NSString *accessToken = [NSString stringWithFormat:@"%@", [dic objectForKey:@"accessToken"]];
            NSString* message = [NSString stringWithFormat:@"%@", [dic objectForKey:@"desc"]];
            [GeYanSdk stopLoading];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC * 1), dispatch_get_main_queue(), ^{

            });
//            if ([dic[@"errorCode"] isEqualToNumber:@(-20302)]) {
//                //退出登录页
//                return;
//            }
//            if (![dic[@"code"] isEqualToNumber:@30000]) {
//                dispatch_async(dispatch_get_main_queue(), ^{
//                                   [[PluginResult defaultManager] callFlutterMethod:@"onLoginPassSuccess" arguments:@{@"success":@(NO), @"message":message}];
//                               });
//                return;
//            }
//                            dispatch_async(dispatch_get_main_queue(), ^{
//                                [[PluginResult defaultManager] callFlutterMethod:@"onLoginPassSuccess" arguments:@{@"success":@(YES), @"message":accessToken}];
//                            });
////            [self queryPhoneNumber:dic];
//            [GeYanSdk closeAuthVC:YES completion:^{
//                NSLog(@"关闭授权页面");
//
//            }];
        }];
    }];
//
//
////    [[NTESQuickLoginManager sharedInstance] setupModel:model];
////
////    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
////
////        [[NTESQuickLoginManager sharedInstance] CUCMCTAuthorizeLoginCompletion:^(NSDictionary * _Nonnull resultDic) {
////            NSString *accessToken = [NSString stringWithFormat:@"%@", [resultDic objectForKey:@"accessToken"]];
////            NSString* message = [NSString stringWithFormat:@"%@", [resultDic objectForKey:@"desc"]];
////            BOOL success = [[resultDic objectForKey:@"success"] boolValue];
////            if (success) {
////
////                dispatch_async(dispatch_get_main_queue(), ^{
////                    [[PluginResult defaultManager] callFlutterMethod:@"onLoginPassSuccess" arguments:@{@"success":@(success), @"message":accessToken}];
////                });
////
////            } else {
////                dispatch_async(dispatch_get_main_queue(), ^{
////                    [[PluginResult defaultManager] callFlutterMethod:@"onLoginPassSuccess" arguments:@{@"success":@(success), @"message":message}];
////                });
////            }
////            [[NTESQuickLoginManager sharedInstance] closeAuthController: nil];
////        }];
////
////      });
//
//
//    [[PluginResult defaultManager] responesResult:@{@"success":@(NO), @"message":@"等待一键登录回调"}];

}

@end
