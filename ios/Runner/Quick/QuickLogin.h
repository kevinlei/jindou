#import <Foundation/Foundation.h>
#import <Flutter/Flutter.h>

NS_ASSUME_NONNULL_BEGIN

@interface QuickLogin : NSObject

+ (void)checkLoginEnable:(FlutterMethodCall *) call;

+ (void)getPrefetchPhone;

+ (void)getLoginToken;

@end

NS_ASSUME_NONNULL_END
