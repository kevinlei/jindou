#import "PluginResult.h"

@interface PluginResult () {
    NSObject<FlutterPluginRegistrar> *_registrar;
    FlutterMethodChannel *_channel;
    FlutterResult _result;
}
@end

@implementation PluginResult


static PluginResult *_instance;


+(instancetype)defaultManager {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (_instance == nil) {
            _instance = [[self alloc] init];
        }
    });
    return _instance;
}

-(void)setFlutterRegistrar:(NSObject<FlutterPluginRegistrar> *)registrar {
    _registrar = registrar;
}

-(NSObject<FlutterPluginRegistrar> *)getRegistrar {
    return _registrar;
}

-(void)setMethodChannel:(FlutterMethodChannel *)channel {
    _channel = channel;
}

- (void)callFlutterMethod:(NSString *)method arguments:(id)arguments {
    if (_channel) {
        [_channel invokeMethod:method arguments:arguments];
    }
}

-(void)setFlutterResult:(FlutterResult)result {
    _result = result;
}

-(void)responesResult: (id _Nullable)data {
    if (_result) {
        _result(data);
    }
}

@end
