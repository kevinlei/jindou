#import <Foundation/Foundation.h>
#import <Flutter/Flutter.h>

NS_ASSUME_NONNULL_BEGIN

//@interface PluginUtils : NSObject<FlutterPlugin,FUPayDelegate>
@interface PluginUtils : NSObject<FlutterPlugin>

- (void)goFuYouPay:(FlutterMethodCall *)call result:(FlutterResult)result;

@end

NS_ASSUME_NONNULL_END
