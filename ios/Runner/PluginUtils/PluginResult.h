#import <Foundation/Foundation.h>
#import <Flutter/Flutter.h>


NS_ASSUME_NONNULL_BEGIN

@interface PluginResult : NSObject

+(instancetype)defaultManager;

-(void)setFlutterRegistrar:(NSObject<FlutterPluginRegistrar> *)registrar;

-(NSObject<FlutterPluginRegistrar> *)getRegistrar;

-(void)setMethodChannel:(FlutterMethodChannel *)channel;

-(void)callFlutterMethod:(nonnull NSString *)method arguments:(id _Nullable)arguments;

-(void)setFlutterResult:(FlutterResult)result;

-(void)responesResult:(id _Nullable)data;

@end

NS_ASSUME_NONNULL_END
