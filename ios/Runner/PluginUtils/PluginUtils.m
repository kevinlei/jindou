#import "PluginUtils.h"

@interface PluginUtils()<AMapSearchDelegate,CLLocationManagerDelegate,AMapLocationManagerDelegate>
@property (nonatomic, strong) PluginVerifyFactory   *verifyFactory;     ///< 实名认证
@property (nonatomic, strong) AMapSearchAPI   *search;
@property (nonatomic, strong) NSMutableArray   *positAaaray;
@property (nonatomic, strong) AMapLocationManager   *locationManager;


@end

@implementation PluginUtils


+ (void)registerWithRegistrar:(nonnull NSObject<FlutterPluginRegistrar> *)registrar {
    FlutterMethodChannel* channel = [FlutterMethodChannel methodChannelWithName:@"ub.plugin.wrapper" binaryMessenger:[registrar messenger]];
    
    PluginUtils* instance = [[PluginUtils alloc] initWithRegistrar:registrar];
    [registrar addMethodCallDelegate:instance channel:channel];
    [registrar addApplicationDelegate:instance];
    [[PluginResult defaultManager] setFlutterRegistrar:registrar];
    [[PluginResult defaultManager] setMethodChannel:channel];
}

- (instancetype) initWithRegistrar:(nonnull NSObject<FlutterPluginRegistrar> *)registrar {
    self = [super init];
    if (self) {
        self.verifyFactory = [[PluginVerifyFactory alloc] initWith:registrar];
       [registrar registerViewFactory:self.verifyFactory withId:@"com.echo.voice/verify"];
     
    }
    return self;
}

- (void)handleMethodCall:(FlutterMethodCall *)call result:(FlutterResult)result {
    [[PluginResult defaultManager] setFlutterResult:result];
    self.positAaaray=[[NSMutableArray alloc]init];
    if([@"getMachineToken" isEqualToString:call.method]){
        NSString *appid = [call.arguments objectForKey:@"appId"];
        
        if([appid isEqualToString:@""]) {
            result(@"");
        }else {
            //初始化
            [GeYanSdk startWithAppId:appid
                withCallback:^(BOOL isSuccess, NSError *error, NSString *gtcid) {
                NSLog(@"GeYanSdk startWithAppId gtcid:%@", gtcid);
            }];
            //提前进行预登录，以便节省后续用户的等待时间;
            [GeYanSdk preGetToken:^(NSDictionary *preDic) {
                NSLog(@"preGetToken:%@", preDic);
            }];
        }
    } else if([@"enableWakeLock" isEqualToString:call.method]) {
        bool enable = [[call.arguments objectForKey:@"enable"] boolValue];
        [[UIApplication sharedApplication] setIdleTimerDisabled: enable];
    } else if ([@"initLocation" isEqualToString:call.method]) {
        [AMapSearchAPI updatePrivacyShow:AMapPrivacyShowStatusDidShow privacyInfo:AMapPrivacyInfoStatusDidContain];
        [AMapSearchAPI updatePrivacyAgree:AMapPrivacyAgreeStatusDidAgree];
        NSLog(@"******%@",call.arguments);
        [[AMapServices sharedServices] setEnableHTTPS:YES];
        [AMapServices sharedServices].apiKey =@"aa1b762e341ec1657b64482af7029969";
        
        
        self.locationManager = [[AMapLocationManager alloc] init];
        [self.locationManager setDelegate:self];
        [self.locationManager setDesiredAccuracy:kCLLocationAccuracyHundredMeters];
        //单次定位超时时间
        [self.locationManager setLocationTimeout:6];
        [self.locationManager setReGeocodeTimeout:3];
        //带逆地理的单次定位
        [self.locationManager requestLocationWithReGeocode:YES completionBlock:^(CLLocation *location, AMapLocationReGeocode *regeocode, NSError *error) {
            if (error) {
    
                NSLog(@"locError:{%ld - %@};",(long)error.code,error.localizedDescription);
                if (error.code == AMapLocationErrorLocateFailed) {
                    return ;
                }
            }
            //定位信息
            NSLog(@"location:%@", location);
            NSLog(@"location:%f==%f", location.coordinate.latitude,location.coordinate.longitude);
            if (regeocode)
            {
                
                self.search = [[AMapSearchAPI alloc] init];
                self.search.delegate = self;
                AMapPOIAroundSearchRequest *request = [[AMapPOIAroundSearchRequest alloc] init];
                request.location            = [AMapGeoPoint locationWithLatitude:location.coordinate.latitude longitude:location.coordinate.longitude];
                request.keywords            = @"公司企业|政府机构及社会团体|道路附属设施|地名地址信息|公司企业|道路附属设施|风景名胜|购物服务|餐饮服务|汽车维修|公共设施|政府机构及社会团体|科教文化服务|商务住宅|交通设施服务";
                /* 按照距离排序. */
                request.sortrule            = 0;
                [self.search AMapPOIAroundSearch:request];
                
            }
        }];
        
    } else if ([@"startLocation" isEqualToString:call.method]) {
        NSLog(@"******%@",[call.arguments objectForKey:@"clientKey"]);
        
        AMapPOIAroundSearchRequest *request = [[AMapPOIAroundSearchRequest alloc] init];
 
        request.location            = [AMapGeoPoint locationWithLatitude:106.308360 longitude:29.600783];
        request.keywords            = [call.arguments objectForKey:@"clientKey"];
        /* 按照距离排序. */
        request.sortrule            = 0;
        [self.search AMapPOIAroundSearch:request];
    } else if ([@"stopLocation" isEqualToString:call.method]) {
        
    } else if ([@"destroyLocation" isEqualToString:call.method]) {
        
    } else if ([@"searchAround" isEqualToString:call.method]) {
        
    } else if ([@"setApiKey" isEqualToString:call.method]) {
        
    } else if ([@"quitQuickLogin" isEqualToString:call.method]) {
        NSLog(@"555555551");
        result(@{@"success":@(true)});
    } else if ([@"initLogin" isEqualToString:call.method]) {
        [QuickLogin checkLoginEnable:call];
        result(@{@"success":@(true)});
    } else if ([@"getAppInfo" isEqualToString:call.method]) {
        [AppClipData getPackageInfo];
    } else if ([@"getClipData" isEqualToString:call.method]) {
        [AppClipData getClipData];
    } else if ([@"copyToClipData" isEqualToString:call.method]) {
        [AppClipData copyToClipData:call];
    } else if ([@"registerApple" isEqualToString:call.method]) {
        AppleLogin *appleLogin = [[AppleLogin alloc] init];
        UIWindow *keyWindow = [UIApplication sharedApplication].windows.firstObject;
        appleLogin.modalPresentationStyle = UIModalPresentationOverFullScreen;
        appleLogin.edgesForExtendedLayout = UIRectEdgeTop;
        [keyWindow.rootViewController presentViewController:appleLogin animated:YES completion:nil];
    } else if ([@"payByApple" isEqualToString:call.method]) {
        ApplePay *applePay = [[ApplePay alloc] init];
        applePay.productID = [call.arguments objectForKey:@"productId"];
        UIWindow *keyWindow = [UIApplication sharedApplication].windows.firstObject;
        applePay.modalPresentationStyle = UIModalPresentationOverFullScreen;
        applePay.edgesForExtendedLayout = UIRectEdgeTop;
        [keyWindow.rootViewController presentViewController:applePay animated:YES completion:nil];
    } else if ([@"prefetchPhone" isEqualToString:call.method]) {
        [QuickLogin getPrefetchPhone];
    } else if ([@"loginPass" isEqualToString:call.method]) {
        NSLog(@"555555553");
        [QuickLogin getLoginToken];
    }else if ([@"shareWXMsg" isEqualToString:call.method]) {
        //  [SharePlugin sendShare:call shareType:@"wx"];
    } else if([@"shareQQMsg" isEqualToString:call.method]) {
        //  [SharePlugin sendShare:call shareType:@"qq"];
    } else if ([@"hasInstallWeChat" isEqualToString:call.method]){
        //    [SharePlugin isInstall:call appType:@"wx"];
    } else if ([@"startVerifyCheck" isEqualToString:call.method]) {
        if (self.verifyFactory) {
            PluginVerifyView *view = [self.verifyFactory getView];
            [view startVerifyCheck];
        }
    } else if ([@"stopVerifyCheck" isEqualToString:call.method]) {
        if (self.verifyFactory) {
            PluginVerifyView *view = [self.verifyFactory getView];
            [view stopVerifyCheck];
        }
    } else if ([@"hasInstallAli" isEqualToString:call.method]){
        NSURL * url = [NSURL URLWithString:@"alipay://"];
        if (![[UIApplication sharedApplication]canOpenURL:url]) {//未安装
            result(@(NO));
        }else {
            result(@(YES));
        }
    } if([@"payAli" isEqualToString:call.method]) {
        NSString *orderInfo = [call.arguments objectForKey:@"orderInfo"];
//        [[AlipaySDK defaultService] payOrder:orderInfo fromScheme:@"com.beeps.voice" callback:^(NSDictionary *resultDic) {
//            NSLog(@"reslut = %@",resultDic);
//            result(@{@"success":@(YES),@"message":@""});
//        }];
    }else if([@"payFuYou" isEqualToString: call.method]){
        [self goFuYouPay:call result:result];
    }else if([@"sdPay" isEqualToString: call.method]){
        [self goSandPay:call result:result];
    }else {
        result(FlutterMethodNotImplemented);
    }
}


- (void)goFuYouPay:(FlutterMethodCall *)call result:(FlutterResult)result {
    
    NSString *payType = [call.arguments objectForKey:@"payType"];
    
//    FUPayParamModel *paramModel = [FUPayParamModel new];
//    paramModel.backNotifyUrl = [call.arguments objectForKey:@"backNotifyUrl"];
//    paramModel.mchntCd =  [call.arguments objectForKey:@"mchntCd"];
//    paramModel.goodsName =  [call.arguments objectForKey:@"goodsName"];
//    paramModel.goodsDesc =  [call.arguments objectForKey:@"goodsDesc"];
//    paramModel.orderId =  [call.arguments objectForKey:@"orderId"];
//    paramModel.orderAmt =  [call.arguments objectForKey:@"orderAmt"];
//    paramModel.ver = @"1.0.0";
//    paramModel.orderDate = [call.arguments objectForKey:@"orderDate"];
//    paramModel.orderTmStart = [call.arguments objectForKey:@"orderTmStart"];
//    paramModel.orderTmEnd = [call.arguments objectForKey:@"orderTmEnd"];
//    paramModel.appScheme = [call.arguments objectForKey:@"appScheme"];
//
//    [FUPaySDK shareInstance].keyConfig.aggpayDesKey = [call.arguments objectForKey:@"orderToken"];
//    [[FUPaySDK shareInstance] setPayEnvType:EnvTypePro];
//
//    if([payType isEqualToString:@"open"]) {
//        UIWindow *keyWindow = [UIApplication sharedApplication].windows.firstObject;
//        [[FUPaySDK shareInstance] openPayList:paramModel viewController:keyWindow.rootViewController];
//    }else if([payType isEqualToString:@"wechat"]) {
//        [self usePayType:kFUPayTypeWxPayApp paramModel:paramModel];
//    }else if([payType isEqualToString:@"wechatmini"]) {
//        [self usePayType:kFUPayTypeWxPayMini paramModel:paramModel];
//    }else if([payType isEqualToString:@"alipay"]) {
//        [self usePayType:kFUPayTypeAlipayLl paramModel:paramModel];
//    }
}

// 富有支付回调
//- (void)payResultCallBack:(FUPayResult)payResult msg:(NSString *)msg{
//    NSLog(@"富有支付回调:%@",msg);
//    [[PluginResult defaultManager] responesResult:@{@"success":@(YES),@"message":msg}];
//}
//
//- (void)usePayType:(FUPayType)payType paramModel:(FUPayParamModel *)paramModel {
//    [FUPaySDK shareInstance].payDelegate = self;
//    UIWindow *keyWindow = [UIApplication sharedApplication].windows.firstObject;
//    [[FUPaySDK shareInstance] startPayType:payType paramModel:paramModel viewController:keyWindow.rootViewController];
//}


- (void)goSandPay:(FlutterMethodCall *)call result:(FlutterResult)result {
    
    NSString *data = [call.arguments objectForKey:@"data"];
    NSDictionary *dict = data.mj_JSONObject;
    NSString *pay_extra = [dict objectForKey:@"pay_extra"];
    
    NSLog(@"杉德支付---data:%@",dict);
    
    ///  所有 参数
    XHPayParameterModel *model = [XHPayParameterModel new];
    model.sign_type = @"RSA";
    model.jump_scheme = [dict objectForKey:@"jump_scheme"];
    model.order_amt = [dict objectForKey:@"order_amt"];
    model.clear_cycle = [dict objectForKey:@"clear_cycle"];
    /// 支付操作完成回跳地址
    model.return_url = [dict objectForKey:@"return_url"];
    /// 分账标识 NO无分账，YES有分账 必填
    model.accsplit_flag = [dict objectForKey:@"accsplit_flag"];
    model.notify_url = [dict objectForKey:@"notify_url"];
    /// 不可为空
    model.create_time = [dict objectForKey:@"create_time"];
    model.expire_time = [dict objectForKey:@"expire_time"];
    model.goods_name = [dict objectForKey:@"goods_name"];
    model.store_id = [dict objectForKey:@"store_id"];
    model.create_ip = [dict objectForKey:@"create_ip"];
    // 单号
    model.mer_order_no =  [dict objectForKey:@"mer_order_no"];
    ///  商户号
    model.mer_no = [dict objectForKey:@"mer_no"];
    model.version = [dict objectForKey:@"version"];
    model.product_code = [dict objectForKey:@"product_code"];
    
    // 不参与签名 微信传1屏蔽所有信用卡。支付宝传1屏蔽部分信用卡    传4屏蔽花呗    传5屏蔽花呗和部分信用卡
    //    model.limit_pay = @"1";
    
    model.pay_extra = pay_extra;
    
    ///  开始签名
    model.sign = [dict objectForKey:@"sign"];
    NSLog(@"支付传参: %@",model.mj_keyValues);
    
    [[SDPTController shared] singletonPaymentMethodWith:model];
    
    // 支付成功 回调
    [SDPTController shared].paySucTypeBlock = ^(ProductPyType type, id  _Nonnull responseObj) {
        
        [self accordingToSpecPayTypeDealWith:type tokenidOrTn:(NSString*)responseObj];
        
        if (type ==PyTypeUnionQuick) {
            NSLog(@"支付类型快捷充值 支付 成功 -  -- - 商户自己定义的标记字符串 : %@",(NSString*)responseObj);
        }
        if (type ==PyTypeUnionPHFive) {
            NSLog(@"支付类型银联WAP 支付 成功 -  -- - 商户自己定义的标记字符串 : %@",(NSString*)responseObj);
        }
        if (type == PyTypeUnionPFast) {
            NSLog(@"支付类型银联快捷 支付 成功 -  -- - 商户自己定义的标记字符串 : %@",(NSString*)responseObj);
            NSLog(@"商户根据自己定义的标记字符串 跳转不同的页面");
        }
        
    };
    
    ///  用户点击左边返回按钮 取消回调
    [SDPTController shared].backBtnActionPayPageDismissBlock = ^{
        NSLog(@"银联快捷 用户点击 支付页面消失");
    };
    
    /// 失败回调
    [self addFailSDPTBlock];
}
/* POI 搜索回调. */
- (void)onPOISearchDone:(AMapPOISearchBaseRequest *)request response:(AMapPOISearchResponse *)response
{
    
    NSLog(@"========%@",response);
 
    if (response.pois.count == 0)
    {
        return;
    }else{
        for (int i=0; i<response.pois.count; i++) {
            AMapPOI *main=[response.pois objectAtIndex:i];
            NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
            [dic setObject:main.name forKey:@"name"];
            [dic setObject:main.address forKey:@"address"];
            [dic setObject:[NSString stringWithFormat:@"%f",main.location.latitude] forKey:@"latitude"];
            [dic setObject:[NSString stringWithFormat:@"%f",main.location.longitude] forKey:@"longitude"];
            
            NSLog(@"%@",main.address);
            [self.positAaaray addObject:dic];
        }
        [[PluginResult defaultManager] callFlutterMethod:@"PbLocDialog" arguments:@{@"message":self.positAaaray}];
      
    }
    
    //解析response获取POI信息，具体解析见 Demo
}

/// 根据返回的类型 处理不同的支付方式 实际吊起支付
- (void)accordingToSpecPayTypeDealWith:(ProductPyType ) type tokenidOrTn:(NSString*) tokenid {
    
    switch (type) {
        case PyTypeWeiXP:
        {
            NSLog(@"调取微信官方SDK 支付，吊起小程序支付。 tokenid参数 %@",tokenid);
            [self selectWxPay:tokenid];
        }
            break;
            
        case  PyTypeLinkP:
        {
            NSLog(@"打印调取统一 链接支付  返回好友支付链接 - -- - - %@",tokenid);
            // 可以 分享到支付宝或者 微信中支付 或生成二维码 云闪付扫码支付
            //[self shard:tokenid];
        }
            break;
        case  PyTypeZFBP:
        {
            NSLog(@"调取统一支付宝支付 参数 %@",tokenid);
            /// 监听 当前app进入前台 刷新token等操作 离开app后一切回来就会触发的操作 查看 appdelegate里面的- (void)applicationWillEnterForeground:(UIApplication *)application 获取点击左上角返回事件
            //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(endBackgroundFormOutAlipay) name:UIApplicationWillEnterForegroundNotification object:nil];
            
            //            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(endBackgroundFormOutAlipay) name:AppLeftBackString object:nil];
        }
            break;
        case  PyTypeZFBSCANP:
        {
            NSLog(@"调取统一支付宝支付 参数 %@",tokenid);
            //            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(endBackgroundFormOutAlipay) name:AppLeftBackString object:nil];
            
            /// 监听支付宝手动返回 app后台进入前台
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(endBackgroundFormOutAlipay) name:UIApplicationWillEnterForegroundNotification object:nil];
        }
            break;
            
        case PyTypeSandBP:
        {
            NSLog(@"调取统一 杉德宝 支付 返回参数 %@",tokenid);
            NSLog(@"sandbao - - -Tn - - - %@",tokenid);
            /// 可吊起杉德宝APP，在杉德宝APP内支付。 杉德宝SDK只有真机 没有模拟器-固注释
            //                [[SpsDock sharedInstance] callSps:tokenid];
        }
            break;
        case PyCloudWeiXAppletP:
        {
            /// h5 云函数 链接后拼接返回的tokenid 然后使用系统的Safari浏览器打开 一定要导入系统的 SafariServices.framework
            NSLog(@"- h5 吊起-- -小程序 -返回数据  - - -tokenid - -%@",tokenid);
            NSString *h5YunLink = @"https://wx6edc693abbd5031e-5dkqn9f591fa6-1302106789.tcloudbaseapp.com/jump_mp.html";
            NSString  *strUrl = [NSString stringWithFormat:@"%@?token_id=%@",h5YunLink,tokenid];
            
            NSLog(@" - -  - -安全浏览器 -打开 url - - - %@",strUrl);
            // 这里实际上有2种 方式 一是使用系统的SFSafariViewController 加载 二是使用UIApplication跳系统浏览器在跳微信
            SFSafariViewController *sacVC =  [[SFSafariViewController alloc]initWithURL:[NSURL URLWithString:strUrl]];
            //            sacVC.delegate = self ;
            //[self.navigationController presentViewController:sacVC animated:YES completion:^{}];
            
            //  第二种 页面需要跳出当前app  当前app--浏览器---微信
            //            [self openURLsystemBrowserWith:strUrl];
            
        }
            break;
        case PyTypeUnionP:
        {
            
            // 00 生产  拿到银联的Tn 吊起云闪付支付 最新版本银联SDK取消了快捷H5卡号支付，但是可以用很多的银行APP直接支付，具体参考银联官方文档 这里的UPPayDemoSDPT 应该可以换成自己的建议用自己app的bundleid 因为使用同一个可能会造成 回跳是跳到别的app去了
            //[[UPPaymentControl defaultControl]startPay:tokenid fromScheme:@"UPPayDemoSDPT://" mode:@"00" viewController:self];
            
        }
            break;
            
        default:
            break;
    }
    
}

- (void)addFailSDPTBlock {
    
    [SDPTController shared].payFailureBlock = ^(ProductPyType type, NSString * _Nonnull errorMsg) {
        NSLog(@"调取单个 支付  错误 信息 -- 错误类型 - type - %ld -  - - errorMsg- - -%@",type,errorMsg);
        /// SDK 返回失败回调中 做用户友好交互处理
//        [self.view makeToast:errorMsg
//                    duration:3
//                    position:CSToastPositionCenter
//                       style:[[CSToastStyle alloc] initWithDefaultStyle]];
    };
    
}


/// 微信支付 吊起  小程序支付
/// @param tokenID  微信tokenId
- (void)selectWxPay:(NSString *)tokenID {
    
    // 缓存 tokenid，后续检查该笔订单是否支付等可以查询。 拼接小程序链接
    
    
//    NSString * allPath = [NSString stringWithFormat:@"pages/zf/index?token_id=%@",tokenID];
//    WXLaunchMiniProgramReq *launchMiniProgramReq = [WXLaunchMiniProgramReq object];
//    launchMiniProgramReq.userName = @"gh_39d783541785";
//    launchMiniProgramReq.path = allPath;
//    /// 0 正式版
//    launchMiniProgramReq.miniProgramType = 0;
//
//    [WXApi sendReq:launchMiniProgramReq completion:^(BOOL success) {
//    }];
#warning  注意 在小程序中点击返回APP，返回使用 的是移动应用id做url-type跳回的scheme 写错的话 在微信中支付完成跳不回去。
    
}




- (void)detachFromEngineForRegistrar:(NSObject<FlutterPluginRegistrar> *)registrar {

}

@end
