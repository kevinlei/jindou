#import "AppClipData.h"

@implementation AppClipData

#pragma mark- app信息
+(void) getPackageInfo {
    NSString *appName = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"];
    NSString *packageName = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"];
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString *buildNumber = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    UIDevice *uiDevice = [[UIDevice alloc] init];
    NSString *uuid = uiDevice.uuid;
    [[PluginResult defaultManager] responesResult:@{@"appName":appName, @"packageName":packageName, @"version": version, @"buildNumber":@([buildNumber intValue]), @"uuid":uuid}];
}

#pragma mark- 获取剪贴板数据
+(void) getClipData {
    NSString *content = [[UIPasteboard generalPasteboard] string];
    [[PluginResult defaultManager] responesResult:content];
}

#pragma mark- 复制到剪贴板
+(void) copyToClipData:(FlutterMethodCall *)call {
    NSString *content = call.arguments[@"content"];
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    [pasteboard setString:content];
    [[PluginResult defaultManager] responesResult:[NSNumber numberWithBool:YES]];
}


@end
