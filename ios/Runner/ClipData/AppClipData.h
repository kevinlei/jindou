#import <Foundation/Foundation.h>
#import <Flutter/Flutter.h>
#import <FCUUID/UIDevice+FCUUID.h>

NS_ASSUME_NONNULL_BEGIN

@interface AppClipData : NSObject

+(void) getPackageInfo;

+(void) getClipData;

+(void) copyToClipData:(FlutterMethodCall *)call;

@end

NS_ASSUME_NONNULL_END
