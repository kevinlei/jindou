//
//  FUKeyConfig.h
//  FUPaySDK
//
//  Created by springsky on 2020/5/25.
//  Copyright © 2020 Spring sky. All rights reserved.
//

#import <Foundation/Foundation.h>


/***
 *银行卡类型 0 所有类型  1 借记卡  2 信用卡
 */
typedef NS_ENUM(NSInteger,FUBankCardType){
    FUCardTypeAll = 0,
    FUCardTypeDebit = 1,
    FUCardTypeCredit = 2
};


NS_ASSUME_NONNULL_BEGIN

/***
 秘钥配置
 */
@interface FUKeyConfig : NSObject

/*** DES key，必传字段 */
@property(nonatomic, strong) NSString* aggpayDesKey;

/*** 苹果支付公钥，使用苹果支付必传字段  */
@property (strong,nonatomic) NSString* applePayPubkey;
//
//
///*** 小程序支付的 小程序AppId  */
//@property (strong,nonatomic) NSString* wxAppletId;
///*** 小程序支付的app路径  */
//@property (strong,nonatomic) NSString* wxAppletPath; ;
 

@end

NS_ASSUME_NONNULL_END
