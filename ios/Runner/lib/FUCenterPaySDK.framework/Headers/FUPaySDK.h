//
//  FUPaySDK.h
//  FUPaySDK
//
//  Created by springsky on 2020/2/13.
//  Copyright © 2020年 Spring sky. All rights reserved.
//  V 20220520
//

#import <UIKit/UIKit.h>
#import "FUPayParamModel.h"
#import "FUKeyConfig.h"

//! Project version number for FUPaySDK.
FOUNDATION_EXPORT double FUPaySDKVersionNumber;

//! Project version string for FUPaySDK.
FOUNDATION_EXPORT const unsigned char FUPaySDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <FUPaySDK/PublicHeader.h>

typedef NS_ENUM(NSInteger,FUPayType) {
    kFUPayTypeCMB = 1,//招行银行
    kFUPayTypeICBC,//工商银行
    kFUPayTypeABC,//农业银行
    kFUPayTypeCCB,//建行银行
    kFUPayTypeBOC,//中行银行
    kFUPayTypeUNPAY, //银联支付
    kFUPayTypeApplePay, //苹果支付
    kFUPayTypeAlipay, //支付宝支付
    kFUPayTypeAlipayLl, //支付宝扫码支付
    kFUPayTypeWxPayApp, //微信支付
    kFUPayTypeWxPayMini, //微信小程序支付
    kFUPayTypeBankPay, //银行卡快捷支付
    kFUPayTypeBankCredit //信用卡分期
};

typedef NS_ENUM(NSInteger,EnvType) {
    EnvTypePro = 0,
    EnvTypeDev = 1,
    EnvTypeUat = 2
};

/***
 支付结果
 */
typedef NS_ENUM(NSInteger,FUPayResult) {
    FUPayResultSuccess = 8000, //成功
    FUPayResultFail = 1, //支付失败
    FUPayResultCancel = 2, //取消支付
    FUPayResultUnkown = 3 //请查询支付结果
};


@protocol FUPayDelegate <NSObject>
/***
 *支付成功后回调
 */
@required
-(void) payResultCallBack:(FUPayResult)payResult
                      msg:(NSString*) msg;

@end

@interface FUPaySDK : NSObject

@property (strong,nonatomic) UIViewController* viewController;
+(instancetype) shareInstance;

/***
 当前是否使用自带的支付方式列表界面
 */
+(BOOL) isSDKPayListUI;

//支付回调
@property (assign,nonatomic) id<FUPayDelegate> payDelegate;
/**秘钥配置*/
@property (strong,nonatomic) FUKeyConfig* keyConfig; 

//设置支付环境
-(void) setPayEnvType:(EnvType) envType;

-(void) applicationDidBecomeActive;

-(void) applicationWillEnterForeground;

/**
 * 支付界面退出时，释放相关引用，防止程序崩溃
 */
-(void) sdkRelease;

//加载微信的配置信息
-(BOOL) initWXApi:(NSString*) wxAppId universalLink:(NSString*) universalLink;

//处理Appdelegate的数据
-(void) handleUserActivity:(NSUserActivity *)userActivity;

//苹果支付加载证书
-(void) loadRSA:(NSString*) p12Path passwd:(NSString*) passwd;

//处理所有的url
-(void) fuHandleCallBack:(NSURL*)url;

/**
 * 打开支付方式列表进行支付
 */
-(void) openPayList:(FUPayParamModel*) paramModel
      viewController:(UIViewController*) viewController;

/***
 * 从支付列表选择一个支付方式进行支付
 */
- (void)startPayTypeFromPayList:(FUPayType)payType
                     paramModel:(FUPayParamModel *)paramModel
                 viewController:(UIViewController *)viewController;

/**
 * 指定某一个支付方式进行支付
 */
-(void) startPayType:(FUPayType) payType
          paramModel:(FUPayParamModel*) paramModel
      viewController:(UIViewController*) viewController;

/***
 * 获取SDK的版本
 */
+(NSString*) sdkVersion;
@end
