//
//  FUPayParamModel.h
//  FUPaySDK
//
//  Created by springsky on 2020/2/13.
//  Copyright © 2020年 Spring sky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FUKeyConfig.h"

NS_ASSUME_NONNULL_BEGIN
 
@interface FUPayParamModel : NSObject
/**
* 银行卡支付 支持的类型 ，默认支持信用卡和借记卡
*/
@property (assign,nonatomic) FUBankCardType supportBankCardType;
/***商户号，必传*/
@property (strong,nonatomic) NSString* mchntCd;
/***下单日期，必传（格式：YYYYMMDD）*/
@property (strong,nonatomic) NSString* orderDate;
/***订单有效期开始时间，必传（格式：YYYYMMDDHHmmss）*/
@property (strong,nonatomic) NSString* orderTmStart;
/***下单有效期结束时间，必传（格式：YYYYMMDDHHmmss）*/
@property (strong,nonatomic) NSString* orderTmEnd;
/*** 商品名称，支付订单时必传 */
@property (strong,nonatomic) NSString* goodsName;
/*** 商品描述，可选 */
@property (strong,nonatomic) NSString* goodsDesc;
/*** 订单金额（分），支付订单时必传 */
@property (strong,nonatomic) NSString* orderAmt;
/*** 返回到当前App的标识，可为空，为空则为Build Id */
@property (strong,nonatomic) NSString* appScheme;
/*** 回调支付结果url */
@property (strong,nonatomic) NSString* backNotifyUrl;
/*** 暂时不需要传递 */
@property (strong,nonatomic) NSString* payTypeBackUrl;
/*** 暂时不需要传递 */
@property (strong,nonatomic) NSString* mchntName;
/*** 商户订单号，查询和支付订单必传 */
@property (strong,nonatomic) NSString* orderId;

@property (strong,nonatomic) NSString* rem1; //备注1
@property (strong,nonatomic) NSString* rem2; //备注2
@property (strong,nonatomic) NSString* rem3; //备注3
/*** 版本，默认1.0.0 */
@property (strong,nonatomic) NSString* ver;

/*** 苹果支付时，为必传字段 */
@property (strong,nonatomic) NSString* merchantId;


@end

NS_ASSUME_NONNULL_END
