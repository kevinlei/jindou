//
//  SDPTController.h
//  PayFramework
//
//  Created by WGPawn on 3/8/21.
//  Copyright © 2021 ". All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SafariServices/SafariServices.h>
#import <WebKit/WebKit.h>


@class YAXHPayParameterModel;
@class YASDPTController;

/* 支付方式枚举*/
/// 8 - 杉德云账户
/// 12 -电子账户（会员钱包）消费（C2C）
/// 13 -电子账户 会员开户
/// 15 -云账户消费（C2C）PRO

///100- 未知类型

typedef enum : NSUInteger {
    PySandYunAccP = 8,
    PyTypeSandYunC2C = 12,
    PyTypeSandYunOpen = 13,
    PyTypeSandYunC2CPRO = 15,

    YANoOne = 100
} YAProductPyType;



NS_ASSUME_NONNULL_BEGIN
  


@interface YASDPTController : NSObject


/// 初始化
+(instancetype)shared;


 

/// 设置打印日志
///  isLogEnable YES 会打印日志
@property (nonatomic, assign) BOOL isLogEnable;

/// 当前SDK版本号
@property (nonatomic, copy) NSString *sdkVersion;
 
 

///  支付类型type 失败回调，errorMsg为错误消息。
@property (nonatomic, copy) void(^payFailureBlock)(YAProductPyType type,NSString *errorMsg);
 

/// 根据type  区分各种支付的类型 获取相应的支付参数返回
/// 对应关系如下 type
///
///
/// 8  --- 杉德云账户
/// 根据type不同，responseObj对象的含义不同。包含吊起支付宝，微信官方SDK用拼接tokenid,银联TN，杉德宝Tn，好友代付链接。
@property (nonatomic, copy) void(^paySucTypeBlock)(YAProductPyType type,  id responseObj);
  

/// 用户点击左上角 收银台页面消失 取消支付
@property(nonatomic,copy)void (^backBtnActionPayPageDismissBlock)(void);


/// 云账户h5页面吊起原生银联SDK回调方法，回传银联Tn号和当前吊起银联的UIViewController，传入银联SDK方法即可
@property (nonatomic, copy) void(^yunAccWalletPassTnBlock)(NSString *tnStr, UIViewController* currentTopVc);

 
/*
  
 
 杉德云账户-04010001-账户侧--开户并扣款，04010002-云账户支付-受理侧，支付成功SDK-void(^paySucTypeBlock)实时返回支付结果。
 @param model 参数 model.product_code = @"04010001"
  
 */
 
 


/// 单个吊起支付方式 不出现收银台页面直接吊起支付 不需要传入当前VC页面,根据product_code区分后,根据官方SDK做后续操作，传入具体TN或tokenId.
/// @param parameterModel 商户传入参数模型
/// 注意⚠️： product_code必须传入单个
-(void)singletonPaymentMethodWith:(YAXHPayParameterModel*) parameterModel;


/// 云账户银联原生SDK充值 支付结果回调实现方法 需要商户在APPDelegate中的- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options中的银联回调中调用传入支付结果的code
/// @param codeStr 银联SDK回调的支付的Code
-(void)handleYunUnionPaymentResultCode:(NSString*)codeStr;


@end




NS_ASSUME_NONNULL_END

