//
//  SDPTController.h
//  PayFramework
//
//  Created by WGPawn on 3/8/21.
//  Copyright © 2021 ". All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SafariServices/SafariServices.h>
#import <WebKit/WebKit.h>


@class XHPayParameterModel;
@class SDPTController;

/* 支付方式枚举*/
/// 0 - 微信
/// 1 - 支付宝（支付宝生活号）
/// 2 - 链接支付
/// 3 - 杉德宝
/// 4 - 银联
/// 5 - h5云吊起微信小程序
/// 9 - 支付宝（扫码）
/// 10 - 银联wap支付（h5）
/// 11 - 银联一键快捷下单
/// 14 -银联快捷充值

///100- 未知类型

typedef enum : NSUInteger {
    PyTypeWeiXP = 0,
    PyTypeZFBP = 1,
    PyTypeLinkP = 2,
    PyTypeSandBP = 3,
    PyTypeUnionP = 4,
    PyCloudWeiXAppletP = 5,
    PyDCEPP = 6,
    PyBOCEWalletP = 7,
    
    PyTypeZFBSCANP = 9,
    PyTypeUnionPHFive = 10,
    PyTypeUnionPFast = 11,
    
    
    PyTypeUnionQuick = 14,
    
    
    NoOne = 100
} ProductPyType;


NS_ASSUME_NONNULL_BEGIN
  


@interface SDPTController : NSObject


+(instancetype)shared;




/// 设置打印日志
///  isLogEnable YES 会打印日志
@property (nonatomic, assign) BOOL isLogEnable;

/// 当前SDK版本号
@property (nonatomic, copy) NSString *sdkVersion;
 
 

///  支付类型type 失败回调，errorMsg为错误消息。
@property (nonatomic, copy) void(^payFailureBlock)(ProductPyType type,NSString *errorMsg);
 

/// 根据type  区分各种支付的类型 获取相应的支付参数返回
/// 对应关系如下 type
/// 11 - 银联一键快捷
/// 10 - 银联手机网页支付（WAP支付）
/// 9  --- 支付宝扫码（支付宝h5）

/// 6  ---
/// 5  --- 微信 小程序支付云函数起吊
/// 4  --- 银联SDK - - -TN             传给银联SDK
/// 3 --- 杉德宝 - - -TN             传给杉德宝SDK
/// 2--- 链接支付 - - -link string  直接在支付宝微信等好友发送打开
/// 1  --- 支付宝（生活号） - - - - tokenId             用来查询支付结果 必要参数
/// 0 ---  微信 - - -tokenId                  用来拼接在小程序路径后面
/// 根据type不同，responseObj对象的含义不同。包含吊起支付宝，微信官方SDK用拼接tokenid,银联TN，杉德宝Tn，好友代付链接。
@property (nonatomic, copy) void(^paySucTypeBlock)(ProductPyType type,  id responseObj);
  

/// 用户点击左上角 收银台页面消失 取消支付
@property(nonatomic,copy)void (^backBtnActionPayPageDismissBlock)(void);


/// 吊起支付用控件方法
/// @param showVc 需要出现的ViewController
/// @param parameterModel 参数模型
/// 注意⚠️：
/// 可以根据业务多合一，包含银联支付，支付宝支付，微信支付，链接好友支付，杉德宝支付等。如果接入多合一支付  需要全部参数都要传。必要参数不可传""，例如接入微信支付但是没有传微信的参数,支付页面会显示微信支付方式但是无法吊起微信支付。
 
-(void)showSDPTDesktopIn:(UIViewController*)showVc model:(XHPayParameterModel*)parameterModel;
 
 

/*
 粘贴板链接好友支付--不需要接入任何SDK，可在银联微信支付宝中打开即可支付
 @param model 参数模型 model.product_code = @"02000002"
 model.linkTips 粘贴成功提示语 不填默认 弹出 复制成功
 
 杉德宝支付 -- 需要接入杉德宝SDK。 注意⚠️需要把自己的app的bundle id 加在在url type中
 @param model 参数 model.product_code = @"02040001"

 银联支付--需要接入银联SDK，传入TN等
 @param model 参数 model.product_code = @"02030001"
 
 银联手机网页支付（WAP支付）
 @param model 参数 model.product_code = @"06030001"
 
 银联一键快捷
 @param model 参数 model.product_code = @"05030001"
 
 微信支付--需要接入微信SDK，并且配置universeLink等
 @param model 参数 model.product_code = @"02010005"
 
 支付宝支付--支付宝生活号--支持带页面多种支付方式，也支持单个不带页面支付方式，不需要接入支付宝SDK即可支付
 @param model 参数 model.product_code = @"02020004"

 支付宝扫码支付--仅支持不带页面单个支付方式，不需要接入支付宝SDK即可支付
 @param model 参数 model.product_code = @"02020006"
  
 h5云函数-吊起微信小程序支付-不需要接入微信SDK即可支付
 @param model 参数 model.product_code = @"02010007"
 
  
 银联- 快捷充值  银联wap快捷和后台绑卡快捷的组合体 仅支持showSDPTDesktopIn方法调用
 @param model 参数 model.product_code = @"06030003"
 */
 
 


/// 单个吊起支付方式 不出现收银台页面直接吊起支付 不需要传入当前VC页面,根据product_code区分后,根据官方SDK做后续操作，传入具体TN或tokenId.
/// @param parameterModel 商户传入参数模型
/// 注意⚠️：1.product_code必须传入单个。2.微信/银联云闪付需要接入对应的官方SDK,支付宝不需要。3.其余项目配置以官方文档为准。 4.单个调起支付下单支持：[ 0 - 微信 。 1 - 支付宝 。 2 - 链接支付。 3 - 杉德宝 。 4 - 银联 。 5 - h5云吊起微信小程序 。 8 - 杉德云账户 04010001 。 9 - 支付宝扫码支付 02020006 。 10 - 银联手机网页支付（WAP支付）  06030001。 11 - 银联一键快捷下单 05030001 ]  5.调取单个下单仅支持单个产品编码每次，效率可能较高。
-(void)singletonPaymentMethodWith:(XHPayParameterModel*) parameterModel;

 

@end




NS_ASSUME_NONNULL_END

