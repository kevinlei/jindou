//
//  UnionPayFastHFiveWebVC.h
//  SDPTLib
//
//  Created by WGPawn on 2022/5/7.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>
#import "SDPTController.h"


@class  SDPTController;

NS_ASSUME_NONNULL_BEGIN


// void(^UnionPyaDismissBlock)(void);

@interface UnionPayFastHFiveWebVC : UIViewController

@property (nonatomic,strong)  NSString *wkWebHtmlStr;

@property (nonatomic,strong)  NSString *wkUrlStr;

 
@end

NS_ASSUME_NONNULL_END
