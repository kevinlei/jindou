import 'dart:convert';
import 'dart:io';
import 'package:dart_ping_ios/dart_ping_ios.dart';
import 'package:echo/import.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';

class InitPage extends StatefulWidget {
  const InitPage({super.key});

  @override
  State<InitPage> createState() => _InitPageState();
}

class _InitPageState extends State<InitPage> {
  Map? startupActiv;

  @override
  void initState() {
    super.initState();
    initAppContent();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return startupPage();
  }

  Widget startupPage() {
    String img = startupActiv?['ad_picture'] ?? "";
    Widget bgImg = img.isNotEmpty
        ? LoadNetImage(
            NetConstants.ossPath + img,
            width: double.infinity,
            height: double.infinity,
            bgc: true,
            bgcImg: "assets/login/bgret.png",
            fit: BoxFit.fill,
          )
        : FContainer(
            imageFit: BoxFit.fill,
            imageAlign: Alignment.topCenter,
            image: ImageUtils().getAssetsImage("login/bgret"),
            width: double.infinity,
            height: double.infinity,
          );

    return GestureDetector(
      onTap: onTapBanner,
      child: bgImg,
    );
  }

  void onTapBanner() async {
    AppManager().homeActivity.title = startupActiv?["ad_name"] ?? "";
    AppManager().homeActivity.linkImg = startupActiv?["ad_picture"] ?? "";
    AppManager().homeActivity.linkUrl = startupActiv?["ad_link"] ?? "";
  }

  void initAppContent() async {
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitDown, DeviceOrientation.portraitUp]);
    SystemUiOverlayStyle systemUiOverlayStyle = const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        systemNavigationBarColor: Color(0xFF1F1F1F));
    SystemChrome.setSystemUIOverlayStyle(systemUiOverlayStyle);
    initWebView();

    Future.delayed(const Duration(seconds: 0), () async {
      AppManager().initAppEngines();
      PluginUtils().initLogin();
      AppSDK().initWXLogin();
      // 初始化ios网络插件
      if (Platform.isIOS) {
        DartPingIOS.register();
      }
    });

    await AppConstants.initAppInfo();

    getStartupActiv();
  }

  void initWebView() async {
    if (Platform.isIOS) return;
    await AndroidInAppWebViewController.setWebContentsDebuggingEnabled(true);
    var swAvailable = await AndroidWebViewFeature.isFeatureSupported(
        AndroidWebViewFeature.SERVICE_WORKER_BASIC_USAGE);
    var swInterceptAvailable = await AndroidWebViewFeature.isFeatureSupported(
        AndroidWebViewFeature.SERVICE_WORKER_SHOULD_INTERCEPT_REQUEST);
    if (swAvailable && swInterceptAvailable) {
      AndroidServiceWorkerController serviceWorkerController =
          AndroidServiceWorkerController.instance();
      serviceWorkerController.setServiceWorkerClient(AndroidServiceWorkerClient(
        shouldInterceptRequest: (request) async => null,
      ));
    }
  }

  void getStartupActiv() async {
    int seconds = 2;
    await DioUtils().httpRequest(
      NetConstants.openScreenAdvertising,
      hastoast: false,
      loading: false,
      onSuccess: (res) {
        try {
          if (res != null && res["ad_picture"] != "") {
            startupActiv = res;
            if (!mounted) return;
            setState(() {});
            seconds = 6;
          }
        } catch (e) {
          seconds = 2;
        }
      },
    );
    initPage(seconds);
  }

  void initPage(int seconds) async {
    Future.delayed(Duration(seconds: seconds), () async {
      await getUserInfo();
      if (!mounted) return;
      setState(() {});
    });
  }

  Future<void> getUserInfo() async {
    try {
      if (AppConstants.accessToken.isEmpty) {
        WidgetUtils().pushReplace(const LoginPage());
      } else {
        await DioUtils().httpRequest(
          NetConstants.getUserInfo,
          loading: false,
          onSuccess: (response) async {
            if (response == null) {
              return WidgetUtils().pushPage(const LoginPage());
            }
            UserConstants.userginInfo = LoginUserModel.fromJson(response);
            // AppManager().connectMq();
            AppManager().loginSDK();
            WidgetUtils().pushRemove(const NavigatorPage());
          },
          onError: (code, msg) {
            WidgetUtils().pushReplace(const LoginPage());
          },
        );
      }
    } catch (err) {
      WidgetUtils().pushReplace(const LoginPage());
    }
    Future.delayed(const Duration(milliseconds: 300), () async {
      versionUpdate();
    });
  }

  // 版本更新
  void versionUpdate() {
    DioUtils().asyncHttpRequest(
      NetConstants.noneConfig,
      loading: false,
      hastoast: false,
      params: {
        "conf_key": Platform.isIOS ? "ios_version" : "android_version",
      },
      onSuccess: onGetVersion,
    );
  }

  void onGetVersion(res) async {
    Map response = jsonDecode(res);
    String serVersion = response["version_num"];
    List locList = AppConstants.version.split('.');
    List serList = serVersion.split('.');

    bool first = int.parse(serList[0]) > int.parse(locList[0]);
    bool next = int.parse(serList[1]) > int.parse(locList[1]) &&
        int.parse(serList[0]) == int.parse(locList[0]);
    bool last = int.parse(serList[2]) > int.parse(locList[2]) &&
        int.parse(serList[1]) == int.parse(locList[1]);

    bool needUpdate = false;
    if (AppConstants.version == serVersion) {
      needUpdate = false;
    } else {
      // 本地版本小于服务版本
      if (first) {
        needUpdate = true;
      } else if (next) {
        needUpdate = true;
      } else if (last) {
        needUpdate = true;
      }
    }

    int forceUpdate = response["is_force"];
    String updateContent = response["update_content"];
    String updateUrl = response["update_address"];

    if (needUpdate && !AppConstants.hasShowUpdate) {
      AppConstants.hasShowUpdate = true;
      await WidgetUtils().showFDialog(
        WillPopScope(
          onWillPop: () => Future.value(false),
          child: UpdateDialog(
            force: forceUpdate,
            version: serVersion,
            upload: updateUrl,
            tips: updateContent,
          ),
        ),
      );
    }
  }
}
