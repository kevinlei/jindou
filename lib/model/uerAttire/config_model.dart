import 'package:json_annotation/json_annotation.dart';

part 'config_model.g.dart';

@JsonSerializable()
class ConfigGlobal extends Object {
  @JsonKey(name: 'Emojis')
  List<ConfigEmo>? emo;

  @JsonKey(name: 'gift')
  List<ConfigGift>? gift;

  @JsonKey(name: 'grade_conf')
  List<ConfigLevel>? level;

  @JsonKey(name: 'nobility')
  List<ConfigTitle>? title;

  ConfigGlobal();

  factory ConfigGlobal.fromJson(Map<String, dynamic> srcJson) =>
      _$ConfigGlobalFromJson(srcJson);

  Map<String, dynamic> toJson() => _$ConfigGlobalToJson(this);
}

/// 表情
@JsonSerializable()
class ConfigEmo extends Object {
  @JsonKey(name: 'id')
  late int id;
  @JsonKey(name: 'name')
  late String name;
  @JsonKey(name: 'picture')
  late String picture;
  @JsonKey(name: 'emojis_vfx')
  late String emojisvfx;
  @JsonKey(name: 'category_id')
  late int categoryid;
  @JsonKey(name: 'sort')
  late int sort;

  ConfigEmo();

  factory ConfigEmo.fromJson(Map<String, dynamic> srcJson) =>
      _$ConfigEmoFromJson(srcJson);

  Map<String, dynamic> toJson() => _$ConfigEmoToJson(this);
}

@JsonSerializable()
class ConfigGift extends Object {
  @JsonKey(name: 'cardiac')
  late int cardiac;

  @JsonKey(name: 'id')
  late int id;

  @JsonKey(name: 'name')
  late String name;

  @JsonKey(name: 'picture')
  late String picture;

  @JsonKey(name: 'price')
  late int price;

  @JsonKey(name: 'sold')
  int? sold;

  @JsonKey(name: 'sort')
  int? sort;

  @JsonKey(name: 'is_sell')
  int? isSell;

  @JsonKey(name: 'special')
  String? special;

  @JsonKey(name: 'count')
  int? count;

  @JsonKey(name: 'isLock')
  bool? isLock;

  ConfigGift();

  factory ConfigGift.fromJson(Map<String, dynamic> srcJson) =>
      _$ConfigGiftFromJson(srcJson);

  Map<String, dynamic> toJson() => _$ConfigGiftToJson(this);
}

/// 等级
@JsonSerializable()
class ConfigLevel extends Object {
  @JsonKey(name: 'charm_badge')
  late String charmBadge;

  @JsonKey(name: 'charm_picture')
  late String charmPicture;

  @JsonKey(name: 'experience')
  late String experience;

  @JsonKey(name: 'grade')
  late String grade;

  @JsonKey(name: 'id')
  late int id;

  @JsonKey(name: 'wealth_badge')
  late String wealthBadge;

  @JsonKey(name: 'wealth_picture')
  late String wealthPicture;

  ConfigLevel();

  factory ConfigLevel.fromJson(Map<String, dynamic> srcJson) =>
      _$ConfigLevelFromJson(srcJson);

  Map<String, dynamic> toJson() => _$ConfigLevelToJson(this);
}

/// 爵位
@JsonSerializable()
class ConfigTitle extends Object {
  @JsonKey(name: 'head_picture')
  late String headPicture;

  @JsonKey(name: 'id')
  late int id;

  @JsonKey(name: 'image')
  late String image;

  @JsonKey(name: 'moods')
  late int moods;

  @JsonKey(name: 'name')
  late String name;

  @JsonKey(name: 'price')
  late int price;

  @JsonKey(name: 'ratio')
  late String ratio;

  @JsonKey(name: 'rebate')
  late int rebate;

  @JsonKey(name: 'renew')
  late int renew;

  ConfigTitle();

  factory ConfigTitle.fromJson(Map<String, dynamic> srcJson) =>
      _$ConfigTitleFromJson(srcJson);

  Map<String, dynamic> toJson() => _$ConfigTitleToJson(this);
}

/// 商品
@JsonSerializable()
class ConfigGoods extends Object {
  @JsonKey(name: 'id')
  late int id;

  @JsonKey(name: 'category_id')
  late int categoryId;

  @JsonKey(name: 'title')
  late String title;

  @JsonKey(name: 'price')
  late String price;

  @JsonKey(name: 'picture')
  late String picture;

  @JsonKey(name: 'special')
  late String special;

  @JsonKey(name: 'show_type')
  late int showType;

  @JsonKey(name: 'good_type')
  late int goodType;

  @JsonKey(name: 'extend')
  late String extend;

  @JsonKey(name: 'label_icon')
  String? labelIcon;

  @JsonKey(name: 'label_name')
  String? labelName;

  ConfigGoods();

  factory ConfigGoods.fromJson(Map<String, dynamic> srcJson) =>
      _$ConfigGoodsFromJson(srcJson);

  Map<String, dynamic> toJson() => _$ConfigGoodsToJson(this);
}
