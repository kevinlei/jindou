// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'config_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ConfigGlobal _$ConfigGlobalFromJson(Map<String, dynamic> json) => ConfigGlobal()
  ..emo = (json['Emojis'] as List<dynamic>?)
      ?.map((e) => ConfigEmo.fromJson(e as Map<String, dynamic>))
      .toList()
  ..gift = (json['gift'] as List<dynamic>?)
      ?.map((e) => ConfigGift.fromJson(e as Map<String, dynamic>))
      .toList()
  ..level = (json['grade_conf'] as List<dynamic>?)
      ?.map((e) => ConfigLevel.fromJson(e as Map<String, dynamic>))
      .toList()
  ..title = (json['nobility'] as List<dynamic>?)
      ?.map((e) => ConfigTitle.fromJson(e as Map<String, dynamic>))
      .toList();

Map<String, dynamic> _$ConfigGlobalToJson(ConfigGlobal instance) =>
    <String, dynamic>{
      'Emojis': instance.emo,
      'gift': instance.gift,
      'grade_conf': instance.level,
      'nobility': instance.title,
    };

ConfigEmo _$ConfigEmoFromJson(Map<String, dynamic> json) => ConfigEmo()
  ..id = json['id'] as int
  ..name = json['name'] as String
  ..picture = json['picture'] as String
  ..emojisvfx = json['emojis_vfx'] as String
  ..categoryid = json['category_id'] as int
  ..sort = json['sort'] as int;

Map<String, dynamic> _$ConfigEmoToJson(ConfigEmo instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'picture': instance.picture,
      'emojis_vfx': instance.emojisvfx,
      'category_id': instance.categoryid,
      'sort': instance.sort,
    };

ConfigGift _$ConfigGiftFromJson(Map<String, dynamic> json) => ConfigGift()
  ..cardiac = json['cardiac'] as int
  ..id = json['id'] as int
  ..name = json['name'] as String
  ..picture = json['picture'] as String
  ..price = json['price'] as int
  ..sold = json['sold'] as int?
  ..sort = json['sort'] as int?
  ..isSell = json['is_sell'] as int?
  ..special = json['special'] as String?
  ..count = json['count'] as int?
  ..isLock = json['isLock'] as bool?;

Map<String, dynamic> _$ConfigGiftToJson(ConfigGift instance) =>
    <String, dynamic>{
      'cardiac': instance.cardiac,
      'id': instance.id,
      'name': instance.name,
      'picture': instance.picture,
      'price': instance.price,
      'sold': instance.sold,
      'sort': instance.sort,
      'is_sell': instance.isSell,
      'special': instance.special,
      'count': instance.count,
      'isLock': instance.isLock,
    };

ConfigLevel _$ConfigLevelFromJson(Map<String, dynamic> json) => ConfigLevel()
  ..charmBadge = json['charm_badge'] as String
  ..charmPicture = json['charm_picture'] as String
  ..experience = json['experience'] as String
  ..grade = json['grade'] as String
  ..id = json['id'] as int
  ..wealthBadge = json['wealth_badge'] as String
  ..wealthPicture = json['wealth_picture'] as String;

Map<String, dynamic> _$ConfigLevelToJson(ConfigLevel instance) =>
    <String, dynamic>{
      'charm_badge': instance.charmBadge,
      'charm_picture': instance.charmPicture,
      'experience': instance.experience,
      'grade': instance.grade,
      'id': instance.id,
      'wealth_badge': instance.wealthBadge,
      'wealth_picture': instance.wealthPicture,
    };

ConfigTitle _$ConfigTitleFromJson(Map<String, dynamic> json) => ConfigTitle()
  ..headPicture = json['head_picture'] as String
  ..id = json['id'] as int
  ..image = json['image'] as String
  ..moods = json['moods'] as int
  ..name = json['name'] as String
  ..price = json['price'] as int
  ..ratio = json['ratio'] as String
  ..rebate = json['rebate'] as int
  ..renew = json['renew'] as int;

Map<String, dynamic> _$ConfigTitleToJson(ConfigTitle instance) =>
    <String, dynamic>{
      'head_picture': instance.headPicture,
      'id': instance.id,
      'image': instance.image,
      'moods': instance.moods,
      'name': instance.name,
      'price': instance.price,
      'ratio': instance.ratio,
      'rebate': instance.rebate,
      'renew': instance.renew,
    };

ConfigGoods _$ConfigGoodsFromJson(Map<String, dynamic> json) => ConfigGoods()
  ..id = json['id'] as int
  ..categoryId = json['category_id'] as int
  ..title = json['title'] as String
  ..price = json['price'] as String
  ..picture = json['picture'] as String
  ..special = json['special'] as String
  ..showType = json['show_type'] as int
  ..goodType = json['good_type'] as int
  ..extend = json['extend'] as String
  ..labelIcon = json['label_icon'] as String?
  ..labelName = json['label_name'] as String?;

Map<String, dynamic> _$ConfigGoodsToJson(ConfigGoods instance) =>
    <String, dynamic>{
      'id': instance.id,
      'category_id': instance.categoryId,
      'title': instance.title,
      'price': instance.price,
      'picture': instance.picture,
      'special': instance.special,
      'show_type': instance.showType,
      'good_type': instance.goodType,
      'extend': instance.extend,
      'label_icon': instance.labelIcon,
      'label_name': instance.labelName,
    };
