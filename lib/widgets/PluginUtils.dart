import 'package:echo/import.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:fluwx/fluwx.dart' as fluwx;

import '../utils/AppInfo.dart';

class PluginUtils {
  PluginUtils._();
  static final PluginUtils _instance = PluginUtils._();
  factory PluginUtils() => _instance;

  final MethodChannel _methodChannel = const MethodChannel("ub.plugin.wrapper");

  final Map<String, Function(String, String, dynamic)?> listenerMap = {};

  Future<AppInfo> getAppInfo() async {
    final Map map = await _methodChannel.invokeMethod("getAppInfo");
    return AppInfo(map);
  }

  //微信分享
  void weChatShareWebPage(fluwx.WeChatScene type) async {
    fluwx.shareToWeChat(fluwx.WeChatShareImageModel(
      fluwx.WeChatImage.network(
          'https://img0.baidu.com/it/u=836989700,3834794217&fm=253&fmt=auto&app=138&f=PNG?w=499&h=166'),
      title: '我的世界',
      mediaTagName: '我的世界mediaTagName',
      messageAction: '我的世界messageAction',
      messageExt: '我的世界messageExt',
      msgSignature: '我的世界msgSignature',
      description: '电商时代科技手段',
      scene: type,
    ));
  }

  Future<String> readClipBroad() async {
    return await _methodChannel.invokeMethod("readClipBroad");
  }

  Future<void> writeClipBroad(String content) async {
    await _methodChannel.invokeMethod("writeClipBroad", {"content": content});
  }

  Future<void> initLogin() async {
    await _methodChannel.invokeMethod("initLogin", {
      "businessId": NetConstants.quickConfig,
    });
  }

  Future<String> initLocation(String longitude, String latitude) async {
    return await _methodChannel.invokeMethod(
        "initLocation", {"longitude": longitude, "latitude": latitude});
  }

  Future<bool> startLocation(String clientKey) async {
    return await _methodChannel
        .invokeMethod("startLocation", {"clientKey": clientKey});
  }

  Future<bool> stopLocation(String clientKey) async {
    return await _methodChannel
        .invokeMethod("stopLocation", {"clientKey": clientKey});
  }

  Future<Map?> prefetchPhone() async {
    return await _methodChannel.invokeMethod("prefetchPhone");
  }

  Future<Map?> loginPass() async {
    return await _methodChannel.invokeMethod("loginPass");
  }

  Future systemSettings(String mobileCode) async {
    return await _methodChannel
        .invokeMethod("systemSettings", {"mobileCode": mobileCode});
  }

  Future sleepSettings(String mobileCode) async {
    return await _methodChannel
        .invokeMethod("sleepSettings", {"mobileCode": mobileCode});
  }

  void setListener(String key, Function(String, String, dynamic)? callback) {
    if (listenerMap.containsKey(key)) {
      if (callback != null) {
        listenerMap[key] = callback;
      } else {
        listenerMap.remove(key);
      }
    } else {
      if (callback != null) {
        listenerMap.addEntries({key: callback}.entries);
      }
    }
    _methodChannel.setMethodCallHandler((call) {
      for (String listenerKey in listenerMap.keys) {
        Function(String, String, dynamic)? listenerCall =
            listenerMap[listenerKey];
        if (listenerCall == null) continue;
        listenerCall(listenerKey, call.method, call.arguments);
      }
      return Future.value(null);
    });
  }
}
