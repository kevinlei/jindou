import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class FLoading extends StatelessWidget {
  final String? loadText;
  final Color? textColor;

  const FLoading({Key? key, this.loadText, this.textColor}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FContainer(
      width: 260.r,
      height: 260.r,
      radius: BorderRadius.circular(10.r),
      color: Colors.black,
      child:
          Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
        SizedBox(
            width: 100.r,
            height: 100.r,
            child: const LoadAssetImage(
              "loading",
              format: ImageFormat.GIF,
              fit: BoxFit.cover,
            )),
        FText(loadText ?? "加载中...", color: textColor, size: 24.sp)
      ]),
    );
  }
}
