import 'dart:async';

import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class ScrollText extends StatefulWidget {
  /// style必填
  final TextSpan richText;
  final double maxWidth;

  const ScrollText(this.richText, {Key? key, required this.maxWidth})
      : super(key: key);

  @override
  _ScrollTextState createState() => _ScrollTextState();
}

class _ScrollTextState extends State<ScrollText> {
  ScrollController controller = ScrollController();

  double textWidth = 0;
  double spaceWidth = 0;
  int animTime = 5;
  int animSpeed = 40;
  bool hasListener = false;
  String space = "    ";
  TextStyle defaultStyle = TextStyle(fontSize: 30.sp);

  @override
  void initState() {
    super.initState();
    getSpaceWidth();
    getTextWidth();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      if (textWidth > widget.maxWidth && controller.hasClients) {
        startTextScroll();
      }
    });
  }

  void getSpaceWidth() {
    final TextSpan spaceSpan =
        TextSpan(text: space, style: widget.richText.style ?? defaultStyle);
    final TextPainter spacePainter = TextPainter(
      text: spaceSpan,
      maxLines: 1,
      textDirection: TextDirection.ltr,
    );
    spacePainter.layout();
    spaceWidth = spacePainter.width;
  }

  void getTextWidth() {
    final TextPainter textPainter = TextPainter(
      text: widget.richText,
      maxLines: 1,
      textDirection: TextDirection.ltr,
    );
    textPainter.layout();
    textWidth = textPainter.width;
  }

  void startTextScroll() {
    animTime = (textWidth + spaceWidth) ~/ animSpeed;
    if (!hasListener) {
      controller.addListener(onScrollListen);
      hasListener = true;
    }
    controller.jumpTo(0);
    controller.animateTo(textWidth + spaceWidth,
        duration: Duration(seconds: animTime), curve: Curves.linear);
  }

  void onScrollListen() async {
    if (controller.hasClients && controller.offset >= textWidth + spaceWidth) {
      controller.jumpTo(0);
      await Future.delayed(const Duration(seconds: 1));
      if (!controller.hasClients) return;
      controller.animateTo(textWidth + spaceWidth,
          duration: Duration(seconds: animTime), curve: Curves.linear);
    }
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  void didUpdateWidget(covariant ScrollText oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.richText == oldWidget.richText &&
        widget.maxWidth == oldWidget.maxWidth) return;
    getTextWidth();
    if (textWidth > widget.maxWidth && controller.hasClients) {
      startTextScroll();
    } else if (controller.hasClients) {
      controller.removeListener(onScrollListen);
      hasListener = false;
      controller.jumpTo(0);
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget child;
    if (textWidth > widget.maxWidth) {
      child = SizedBox(
          width: textWidth + widget.maxWidth,
          child: Text.rich(
              TextSpan(children: [
                widget.richText,
                TextSpan(
                    text: space, style: widget.richText.style ?? defaultStyle),
                widget.richText,
              ]),
              maxLines: 1));
    } else {
      child = Text.rich(widget.richText, maxLines: 1);
    }
    return SizedBox(
      width: textWidth > widget.maxWidth ? widget.maxWidth : textWidth,
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        physics: NeverScrollableScrollPhysics(),
        controller: controller,
        child: child,
      ),
    );
  }
}
