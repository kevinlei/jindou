import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../utils/ImageUtils.dart';
import '../utils/string_util.dart';

const double APP_BAR_HEIGHT = 52;
const double MATCH_PARENT = double.infinity;


Widget myPageWidget({
  Key? key,
  required Widget? appBar,
  required Widget contentView,
  bool isContentViewWithScrollView = true,
  bool isBgWithAppBar = true,
  bool isContentWithAppBar = false,
  Color? pageBackgroundColor,
  Decoration? pageBackgroundDecoration,
  EdgeInsetsGeometry? padding,
  ScrollController? scrollController,
  Widget? backgroundWidget,
  List<Widget>? topStaticWidgets,
}) {
  List<Widget> topWidgets = [];
  if (null != appBar) {
    topWidgets.add(appBar);
  }

  if (null != topStaticWidgets) {
    topWidgets.addAll(topStaticWidgets);
  }

  return Scaffold(
    key: key,
    appBar: null,
    body: Stack(
      children: [
        Padding(
          padding: EdgeInsets.only(
              top: isBgWithAppBar
                  ? 0
                  : ScreenUtil().statusBarHeight + APP_BAR_HEIGHT),
          child: backgroundWidget ??
              Container(
                width: double.infinity,
                height: double.infinity,
                color: null != pageBackgroundDecoration
                    ? null
                    : (pageBackgroundColor ?? Color(0xFF1C1E1D)),
                decoration: pageBackgroundDecoration,
              ),
        ),
        Align(
          widthFactor: 1.0,
          alignment: Alignment.topLeft,
          child: topWidgets.length > 0
              ? myColumn(
                  children: topWidgets,
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start)
              : myEmptyWidget(),
        ),
        Container(
          width: double.infinity,
          height: double.infinity,
          padding: EdgeInsets.only(
              top: null == appBar
                  ? 0
                  : ScreenUtil().statusBarHeight + APP_BAR_HEIGHT),
          child: isContentViewWithScrollView
              ? SingleChildScrollView(
                  controller: scrollController,
                  child: contentView,
                )
              : contentView,
        ),
      ],
    ),
  );
}

Widget myGestureDetector({
  required Widget child,
  required GestureTapCallback? onTap,
  EdgeInsetsGeometry? padding,
}) {
  if (null != onTap) {
    Widget childWidget = null == padding
        ? child
        : Container(
            padding: padding,
            child: child,
          );
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: onTap,
      child: childWidget,
    );
  } else {
    return child;
  }
}

Text myText(
  String? text, {
  Key? key,
  TextStyle? style,
  StrutStyle? strutStyle,
  TextAlign? textAlign,
  TextDirection? textDirection,
  Locale? locale,
  bool? softWrap,
  TextOverflow? overflow,
  // TextScaler? textScaler,
  int? maxLines,
  String? semanticsLabel,
  TextWidthBasis? textWidthBasis,
  String? defaultText,
}) {
  return Text(
    text ?? (defaultText ?? ""),
    key: key,
    style: style,
    strutStyle: strutStyle,
    textAlign: textAlign,
    textDirection: textDirection,
    locale: locale,
    softWrap: softWrap,
    overflow: overflow,
    // textScaler: textScaler,
    maxLines: maxLines,
    semanticsLabel: semanticsLabel,
    textWidthBasis: textWidthBasis,
  );
}

Text myLeftText(
  String? text, {
  Key? key,
  Color? color,
  double? fontSize,
  FontWeight? fontWeight,
  bool? softWrap,
  TextOverflow? overflow,
  // TextScaler? textScaler,
  int? maxLines,
  String? defaultText,
}) {
  return Text(
    text ?? (defaultText ?? ''),
    key: key,
    style: TextStyle(fontWeight: fontWeight, fontSize: fontSize, color: color),
    textAlign: TextAlign.left,
    softWrap: softWrap,
    overflow: overflow,
    // textScaler: textScaler,
    maxLines: maxLines,
  );
}

Text myRightText(
  String? text, {
  Key? key,
  Color? color,
  double? fontSize,
  FontWeight? fontWeight,
  bool? softWrap,
  TextOverflow? overflow,
  // TextScaler? textScaler,
  int? maxLines,
  String? defaultText,
}) {
  return Text(
    text ?? (defaultText ?? ''),
    key: key,
    style: TextStyle(fontWeight: fontWeight, fontSize: fontSize, color: color),
    textAlign: TextAlign.right,
    softWrap: softWrap,
    overflow: overflow,
    // textScaler: textScaler,
    maxLines: maxLines,
  );
}

Text myCenterText(
  String? text, {
  Key? key,
  Color? color,
  double? fontSize,
  FontWeight? fontWeight,
  bool? softWrap,
  TextOverflow? overflow,
  // TextScaler? textScaler,
  int? maxLines,
  String? defaultText,
}) {
  return Text(
    text ?? (defaultText ?? ''),
    key: key,
    style: TextStyle(fontWeight: fontWeight, fontSize: fontSize, color: color),
    textAlign: TextAlign.center,
    softWrap: softWrap,
    overflow: overflow,
    // textScaler: textScaler,
    maxLines: maxLines,
  );
}

Divider myDivider(
    {Key? key,
    double? height = 0.5,
    Color? color = Colors.white,
    double? indent,
    double? endIndent}) {
  return Divider(
    key: key,
    height: height,
    thickness: height,
    color: color,
    indent: indent,
    endIndent: endIndent,
  );
}

Widget mySizeIcon(
  String? iconPath, {
  required double? size,
  bool showIconEmptySpace = false,
  EdgeInsetsGeometry? padding,
  GestureTapCallback? onTap,
}) {
  return myIcon(
      iconPath: iconPath,
      iconHeight: size,
      iconWidth: size,
      showIconEmptySpace: showIconEmptySpace,
      padding: padding,
      onTap: onTap);
}

Widget myIcon({
  required String? iconPath,
  double? iconWidth,
  double? iconHeight,
  bool showIconEmptySpace = false,
  EdgeInsetsGeometry? padding,
  GestureTapCallback? onTap,
}) {
  if (null == iconPath || iconPath.trim().length <= 0) {
    return showIconEmptySpace
        ? SizedBox(
            width: iconWidth,
            height: iconHeight,
          )
        : myEmptyWidget();
  } else {
    Widget childWidget = null == padding
        ? Image.asset(
            ImageUtils().getImgPath(
              iconPath,
            ),
            width: iconWidth,
            height: iconHeight,
          )
        : Container(
            padding: padding,
            child: Image.asset(
              iconPath,
              width: iconWidth,
              height: iconHeight,
            ),
          );
    return myGestureDetector(padding: null, child: childWidget, onTap: onTap);
  }
}

Widget myColumn({
  Key? key,
  AlignmentGeometry? alignment,
  Color? color,
  double? width,
  double? height,
  Decoration? decoration,
  EdgeInsetsGeometry? padding,
  EdgeInsetsGeometry? margin,
  Decoration? foregroundDecoration,
  BoxConstraints? constraints,
  MainAxisAlignment mainAxisAlignment = MainAxisAlignment.start,
  MainAxisSize mainAxisSize = MainAxisSize.max,
  CrossAxisAlignment crossAxisAlignment = CrossAxisAlignment.start,
  required List<Widget> children,
}) {
  return Container(
    key: key,
    alignment: alignment,
    color: color,
    width: width,
    height: height,
    padding: padding,
    margin: margin,
    decoration: decoration,
    foregroundDecoration: foregroundDecoration,
    constraints: constraints,
    child: Column(
      mainAxisAlignment: mainAxisAlignment,
      crossAxisAlignment: crossAxisAlignment,
      mainAxisSize: mainAxisSize,
      children: children,
    ),
  );
}

Widget myRow({
  Key? key,
  AlignmentGeometry? alignment,
  Color? color,
  double? width,
  double? height,
  Decoration? decoration,
  EdgeInsetsGeometry? padding,
  EdgeInsetsGeometry? margin,
  Decoration? foregroundDecoration,
  BoxConstraints? constraints,
  MainAxisAlignment mainAxisAlignment = MainAxisAlignment.start,
  MainAxisSize mainAxisSize = MainAxisSize.max,
  CrossAxisAlignment crossAxisAlignment = CrossAxisAlignment.center,
  required List<Widget> children,
}) {
  return Container(
    key: key,
    alignment: alignment,
    color: color,
    width: width,
    height: height,
    padding: padding,
    margin: margin,
    decoration: decoration,
    foregroundDecoration: foregroundDecoration,
    constraints: constraints,
    child: Row(
      mainAxisAlignment: mainAxisAlignment,
      crossAxisAlignment: crossAxisAlignment,
      mainAxisSize: mainAxisSize,
      children: children,
    ),
  );
}

Widget myNetImage(String? src,
    {double? width,
    double? height,
    BoxFit? fit,
    PlaceholderWidgetBuilder? placeholder,
    LoadingErrorWidgetBuilder? errorWidget}) {
  if (StringUtil.isEmpty(src)) {
    return SizedBox(
      width: width,
      height: height,
    );
  }
  return CachedNetworkImage(
    width: width,
    height: height,
    imageUrl: src!,
    fit: fit,
    placeholder: placeholder,
    errorWidget: errorWidget,
  );
}

ImageProvider myImageProvider(String? url,
    {int? maxHeight, int? maxWidth, ValueSetter? errorListener}) {
  return CachedNetworkImageProvider(url ?? '',
      maxHeight: maxHeight, maxWidth: maxWidth, errorListener: errorListener);
}

Widget myEmptyWidget() {
  return const SizedBox.shrink();
}

Widget mySpaceHeight(double v) {
  return SizedBox(
    height: v,
  );
}

Widget mySpaceWidth(double v) {
  return SizedBox(
    width: v,
  );
}

BoxDecoration myRoundCornerBoxDecoration(
    {required double radius, Color? color, BoxBorder? border}) {
  return BoxDecoration(
      borderRadius: BorderRadius.all(Radius.circular(radius)),
      color: color,
      border: border);
}
