import 'dart:convert';
import 'dart:math';

import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class EmotionFooter extends StatefulWidget {
  final TextEditingController controller;
  final Color color;
  const EmotionFooter(this.controller, {super.key, this.color = Colors.white});

  @override
  _EmotionFooterState createState() => _EmotionFooterState();
}

class _EmotionFooterState extends State<EmotionFooter> {
  List emotionData = [];

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      decodeEmotion();
    });
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      color: widget.color,
      child: Stack(
        children: [emotionContent(), btnDelete()],
      ),
    );
  }

  Widget emotionContent() {
    return GridView.builder(
      padding: EdgeInsets.fromLTRB(20.w, 20.h, 20.w, 20.w),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 6,
        mainAxisSpacing: 10.w,
        crossAxisSpacing: 10.w,
      ),
      itemBuilder: (_, index) => emotion(index),
      itemCount: emotionData.length,
    );
  }

  Widget btnDelete() {
    Widget btn = GestureDetector(
      onTap: onPressDelete,
      child: FContainer(
        width: 120.w,
        height: 80.w,
        radius: BorderRadius.circular(10.w),
        color: const Color(0xFF6E6E6E),
        align: Alignment.center,
        child: LoadAssetImage(
          "msgPrivateChat/msgChat/del2",
          width: 50.w,
          color: Colors.white,
        ),
      ),
    );
    return Positioned(
        right: 20.w, bottom: ScreenUtil().bottomBarHeight + 40.h, child: btn);
  }

  Widget emotion(int index) {
    String em = String.fromCharCode(emotionData[index]["unicode"]);
    return GestureDetector(
      onTap: () => onPressEmotion(em),
      child: FContainer(
          align: Alignment.center, child: Text(em, textScaleFactor: 1.5)),
    );
  }

  void decodeEmotion() async {
    try {
      String source =
          await DefaultAssetBundle.of(context).loadString("assets/emo.json");
      emotionData = jsonDecode(source);
      if (!mounted) return;
      setState(() {});
    } catch (e) {}
  }

  void onPressEmotion(String em) {
    String text = widget.controller.text;
    int start = widget.controller.selection.baseOffset != -1
        ? widget.controller.selection.baseOffset
        : 0;
    int end = widget.controller.selection.extentOffset != -1
        ? widget.controller.selection.extentOffset
        : 0;
    String f = text.substring(0, start);
    String e = text.substring(end, text.length);
    widget.controller.value = widget.controller.value.copyWith(
      text: f + em + e,
      selection: TextSelection(
          baseOffset: start + em.length, extentOffset: start + em.length),
    );
  }

  void onPressDelete() {
    String text = widget.controller.text;
    int start = widget.controller.selection.baseOffset;
    int end = widget.controller.selection.extentOffset;

    String f, e;
    if (start == end) {
      if (start == 0) return;
      List<String> characters = text.characters.toList();
      int remove = getStartCharacter(characters, start);
      if (remove == -1) return;
      int length = characters[remove].length;
      f = characters.getRange(0, remove).join();
      e = characters.getRange(remove + 1, characters.length).join();
      widget.controller.value = widget.controller.value.copyWith(
        text: f + e,
        selection: TextSelection(
            baseOffset: start - length, extentOffset: start - length),
      );
    } else {
      f = text.substring(0, start);
      e = text.substring(end, text.length);
      widget.controller.value = widget.controller.value.copyWith(
        text: f + e,
        selection: TextSelection(baseOffset: start, extentOffset: start),
      );
    }
  }

  int getStartCharacter(List<String> characters, int start) {
    int offset = 0;
    for (int i = 0; i < characters.length; i++) {
      offset += characters[i].length;
      if (offset == start) {
        return i;
      }
    }
    return -1;
  }
}
