import 'package:echo/widgets/my_widgets.dart';
import 'package:echo/widgets/myfont_weight.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:flutter/services.dart';

class FText extends StatelessWidget {
  final String text;
  final double? size;
  final Color? color;
  final int? maxLines;
  final int? nullLength;
  final FontWeight? weight;
  final TextOverflow? overflow;
  final TextAlign? align;
  final double? height;
  final double? wordSpacing;

  const FText(
    this.text, {
    Key? key,
    this.size,
    this.color,
    this.maxLines,
    this.overflow,
    this.nullLength,
    this.weight,
    this.align,
    this.height,
    this.wordSpacing,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    int maxLength = nullLength ?? 20;
    String content = text;
    content = text.isEmpty ? (" " * maxLength) : content;
    return Text(
      content,
      style: TextStyle(
        fontSize: size ?? 24.sp,
        color: color ?? Colors.white,
        fontWeight: weight,
        backgroundColor: text.isEmpty ? Colors.grey.withOpacity(0.2) : null,
        height: height,
        letterSpacing: wordSpacing,
        wordSpacing: wordSpacing,
      ),
      maxLines: maxLines ?? 1,
      overflow: overflow ?? TextOverflow.visible,
      textScaleFactor: 1,
      softWrap: true,
      textAlign: align,
    );
  }
}

class FProAccept extends StatefulWidget {
  final List<ProtocolType> proList;
  final Function(bool)? onAccept;

  const FProAccept(this.proList, {Key? key, this.onAccept}) : super(key: key);

  @override
  _FProAcceptState createState() => _FProAcceptState();
}

class _FProAcceptState extends State<FProAccept> {
  bool hasAccept = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
        Column(
          children: [selectButton()],
        ),
        ConstrainedBox(
            constraints: BoxConstraints(maxWidth: 600.w),
            child: richTextContent())
      ]),
    );
  }

  Widget selectButton() {
    Widget btn = IconButton(
        onPressed: onTapSelect,
        padding: EdgeInsets.zero,
        icon: LoadAssetImage(hasAccept ? "login/chekbanbtn" : "login/chekbtn",
            width: 28.w, height: 28.w));

    return SizedBox(width: 50.w, height: 50.w, child: btn);
  }

  Widget richTextContent() {
    List<InlineSpan> spans = [
      TextSpan(
          text: "我已详细阅读并同意",
          style: TextStyle(color: const Color(0xFF992222), fontSize: 21.sp))
    ];

    for (int index = 0; index < widget.proList.length; index++) {
      spans.add(TextSpan(
          text: "《${widget.proList[index].text}》",
          style: TextStyle(color: const Color(0xFF222222), fontSize: 21.sp),
          recognizer: TapGestureRecognizer()
            ..onTap = () => onTapProto(widget.proList[index])));
    }

    return Text.rich(TextSpan(children: spans), textScaleFactor: 1);
  }

  void onTapSelect() {
    hasAccept = !hasAccept;
    if (widget.onAccept != null) {
      widget.onAccept!(hasAccept);
    }
    if (!mounted) return;
    setState(() {});
  }

  void onTapProto(ProtocolType type) {
    WidgetUtils().pushPage(ProtocolPage(type: type));
  }
}

class FProSAccept extends StatefulWidget {
  final List<ProtocolType> proList;
  final Function(bool)? onAccept;

  const FProSAccept(this.proList, {Key? key, this.onAccept}) : super(key: key);

  @override
  _FProSAcceptState createState() => _FProSAcceptState();
}

class _FProSAcceptState extends State<FProSAccept> {
  bool hasAccept = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            children: [
              selectButton(),
              // SizedBox(height: 30.w),
            ],
          ),
          ConstrainedBox(
            constraints: BoxConstraints(maxWidth: 600.w),
            child: richTextContent(),
          )
        ],
      ),
    );
  }

  Widget selectButton() {
    Widget btn = IconButton(
      onPressed: onTapSelect,
      padding: EdgeInsets.zero,
      icon: LoadAssetImage(
        hasAccept ? "login/chekbanbtn" : "login/chekhuibtn",
        width: 28.w,
        height: 28.w,
      ),
    );

    return SizedBox(width: 50.w, height: 50.w, child: btn);
  }

  Widget richTextContent() {
    List<InlineSpan> spans = [
      TextSpan(
          text: "我已详细阅读并同意",
          style: TextStyle(color: const Color(0xFF992222), fontSize: 21.sp))
    ];

    for (int index = 0; index < widget.proList.length; index++) {
      spans.add(
        TextSpan(
          text: "《${widget.proList[index].text}》",
          style: TextStyle(color: const Color(0xFF222222), fontSize: 21.sp),
          recognizer: TapGestureRecognizer()
            ..onTap = () => onTapProto(
                  widget.proList[index],
                ),
        ),
      );
    }

    return Text.rich(TextSpan(children: spans), textScaleFactor: 1);
  }

  void onTapSelect() {
    hasAccept = !hasAccept;
    if (widget.onAccept != null) {
      widget.onAccept!(hasAccept);
    }
    if (!mounted) return;
    setState(() {});
  }

  void onTapProto(ProtocolType type) {
    WidgetUtils().pushPage(ProtocolPage(type: type));
  }
}

class FLoginButton extends StatefulWidget {
  final String text;
  final bool isActivate;
  final bool iponeBg;

  /// 普通状态
  final Function()? onPress;

  /// 带有网络请求状态
  final Future Function()? onRequest;

  final double? padding;
  final double? radius;
  final Color? bgColor;
  final Color? txtColor;
  final Color? borderColor;
  final double? txtSize;

  final FontWeight? fontWeight;

  const FLoginButton(this.text,
      {Key? key,
      this.onPress,
      this.padding,
      this.onRequest,
      this.radius,
      this.bgColor,
      this.txtColor,
      this.borderColor,
      this.txtSize,
      this.isActivate = false,
      this.iponeBg = false,
      this.fontWeight})
      : super(key: key);

  @override
  _FLoginButtonState createState() => _FLoginButtonState();
}

class _FLoginButtonState extends State<FLoginButton> {
  bool isRequest = false;

  @override
  Widget build(BuildContext context) {
    List<Widget> children = [
      LoadAssetImage(widget.iponeBg ? "login/iponeFrame" : "login/iponeFrame",
          width: widget.iponeBg ? 40.w : 0),
      SizedBox(width: 5.h),
      FText(
        widget.text,
        size: widget.txtSize ?? 32.sp,
        weight: widget.fontWeight ?? FontWeight.bold,
        color: widget.isActivate ? Color(0xFFFF753F) : Color(0xFFFF753F),
      )
    ];
    if (isRequest) children.insert(0, CupertinoActivityIndicator(radius: 20.w));
    return GestureDetector(
      onTap: onPress,
      child: FContainer(
        gradient: widget.isActivate
            ? [const Color(0xFFFFFFFF), const Color(0xFFFFFFFF)]
            : [const Color(0xFFFFFFFF), const Color(0xFFFFFFFF)],
        gradientBegin: Alignment.centerLeft,
        gradientEnd: Alignment.centerRight,
        height: 88.w,
        radius: BorderRadius.circular(widget.radius ?? 44.w),
        margin: EdgeInsets.symmetric(
            horizontal: widget.padding ?? 25.w, vertical: 20.h),
        align: Alignment.center,
        child: Row(
            mainAxisAlignment: MainAxisAlignment.center, children: children),
      ),
    );
  }

  void onPress() async {
    if (isRequest) return;
    if (widget.onPress != null) {
      widget.onPress!();
    } else if (widget.onRequest != null) {
      isRequest = true;
      if (!mounted) return;
      setState(() {});
      await widget.onRequest!();
      isRequest = false;
      if (!mounted) return;
      setState(() {});
    }
  }
}

class FcationButton extends StatefulWidget {
  final String text;
  final bool isActivate;

  /// 普通状态
  final Function()? onPress;

  /// 带有网络请求状态
  final Future Function()? onRequest;

  final double? padding;
  final double? radius;
  final Color? bgColor;
  final Color? txtColor;
  final Color? borderColor;
  final double? txtSize;

  final FontWeight? fontWeight;

  const FcationButton(this.text,
      {Key? key,
      this.onPress,
      this.padding,
      this.onRequest,
      this.radius,
      this.bgColor,
      this.txtColor,
      this.borderColor,
      this.txtSize,
      this.isActivate = false,
      this.fontWeight})
      : super(key: key);

  @override
  _FcationButtonState createState() => _FcationButtonState();
}

class _FcationButtonState extends State<FcationButton> {
  bool isRequest = false;

  @override
  Widget build(BuildContext context) {
    List<Widget> children = [
      FText(
        widget.text,
        size: widget.txtSize ?? 32.sp,
        weight: widget.fontWeight ?? FontWeight.bold,
        color: widget.isActivate
            ? Colors.white
            : const Color(0xFF222222).withOpacity(0.05),
      )
    ];
    if (isRequest) children.insert(0, CupertinoActivityIndicator(radius: 20.w));
    return GestureDetector(
      onTap: onPresss,
      child: FContainer(
        gradient: widget.isActivate
            ? [Color(0xFFFF753F), Color(0xFFFF753F), Color(0xFFFF753F)]
            : [
                const Color(0xFF222222).withOpacity(0.05),
                const Color(0xFF222222).withOpacity(0.05)
              ],
        gradientBegin: Alignment.topLeft,
        gradientEnd: Alignment.bottomRight,
        height: 100.w,
        radius: BorderRadius.circular(widget.radius ?? 50.w),
        margin: EdgeInsets.symmetric(
            horizontal: widget.padding ?? 25.w, vertical: 20.h),
        align: Alignment.center,
        child: Row(
            mainAxisAlignment: MainAxisAlignment.center, children: children),
      ),
    );
  }

  void onPresss() async {
    if (isRequest) return;
    if (widget.onPress != null) {
      widget.onPress!();
    } else if (widget.onRequest != null) {
      isRequest = true;
      if (!mounted) return;
      setState(() {});
      await widget.onRequest!();
      isRequest = false;
      if (!mounted) return;
      setState(() {});
    }
  }
}

//性别按钮
class SixButton extends StatefulWidget {
  final String text;
  final bool isActivate;

  /// 普通状态
  final Function()? onPress;

  /// 带有网络请求状态
  final Future Function()? onRequest;

  final double? padding;
  final double? radius;
  final Color? bgColor;
  final Color? txtColor;
  final Color? borderColor;
  final double? txtSize;

  final FontWeight? fontWeight;

  const SixButton(this.text,
      {Key? key,
      this.onPress,
      this.padding,
      this.onRequest,
      this.radius,
      this.bgColor,
      this.txtColor,
      this.borderColor,
      this.txtSize,
      this.isActivate = false,
      this.fontWeight})
      : super(key: key);

  @override
  _SixButtonState createState() => _SixButtonState();
}

class _SixButtonState extends State<SixButton> {
  bool isRequest = false;

  @override
  Widget build(BuildContext context) {
    List<Widget> children = [
      LoadAssetImage(widget.text == '男' ? "login/sextant" : "login/shutout",
          width: 30.w),
      FText(
        widget.text,
        size: widget.txtSize ?? 32.sp,
        weight: widget.fontWeight ?? FontWeight.bold,
        color: widget.isActivate
            ? Colors.white
            : const Color(0xFF222222).withOpacity(0.05),
      )
    ];
    if (isRequest) children.insert(0, CupertinoActivityIndicator(radius: 20.w));
    return GestureDetector(
      onTap: onPresss,
      child: FContainer(
        gradient: widget.isActivate
            ? [const Color(0xFFFF753F), const Color(0xFFFF753F)]
            : [
                const Color(0xFF222222).withOpacity(0.05),
                const Color(0xFF222222).withOpacity(0.05)
              ],
        gradientBegin: Alignment.centerLeft,
        gradientEnd: Alignment.centerRight,
        height: 100.w,
        width: 215.w,
        radius: BorderRadius.circular(50.w),
        margin: EdgeInsets.symmetric(
            horizontal: widget.padding ?? 25.w, vertical: 20.h),
        align: Alignment.center,
        child: Row(
            mainAxisAlignment: MainAxisAlignment.center, children: children),
      ),
    );
  }

  void onPresss() async {
    if (isRequest) return;
    if (widget.onPress != null) {
      widget.onPress!();
    } else if (widget.onRequest != null) {
      isRequest = true;
      if (!mounted) return;
      setState(() {});
      await widget.onRequest!();
      isRequest = false;
      if (!mounted) return;
      setState(() {});
    }
  }
}

class FRichText extends StatelessWidget {
  final double? fontSize;
  final int? lines;
  final Color? color;
  final List<FRichTextItem> texts;
  const FRichText(this.texts, {Key? key, this.fontSize, this.lines, this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RichText(
        textScaleFactor: 1,
        text: TextSpan(
            children: List.generate(
          texts.length,
          (index) => TextSpan(
            text: texts[index].text,
            style: TextStyle(
                color: texts[index].color ?? color ?? Colors.white,
                fontWeight: texts[index].weight ?? FontWeight.normal,
                fontSize: texts[index].size ?? fontSize ?? 30.sp),
            recognizer: (texts[index].onPress == null)
                ? null
                : (TapGestureRecognizer()..onTap = texts[index].onPress),
          ),
        )),
        softWrap: true,
        maxLines: lines);
  }
}

class FRichTextItem {
  String text = "";
  Color? color;
  double? size;
  FontWeight? weight;
  Function()? onPress;
}
// -----------------------------------------------------------

class FEmptyWidget extends StatelessWidget {
  final String? text;

  const FEmptyWidget({Key? key, this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        LoadAssetImage("public/empty", width: 160.w, height: 160.w),
        SizedBox(height: 10.w),
        FText(text ?? "暂无数据", color: const Color(0xFF999999))
      ]),
    );
  }
}

// -----------------------------------------------------------

// -----------------------------------------------------------

class FSizeButton extends StatefulWidget {
  final String text;
  final bool isActivate;

  /// 普通状态
  final Function()? onPress;

  /// 带有网络请求状态
  final Future Function()? onRequest;

  final double? padding;
  final double? radius;
  final Color? bgColor;
  final Color? txtColor;
  final bool? percolor;
  final Color? borderColor;
  final double height;
  final double width;
  final double? txtSize;

  final FontWeight? fontWeight;

  const FSizeButton(this.text,
      {Key? key,
      this.onPress,
      this.padding,
      required this.height,
      required this.width,
      this.percolor,
      this.onRequest,
      this.radius,
      this.bgColor,
      this.txtColor,
      this.txtSize,
      this.borderColor,
      this.isActivate = false,
      this.fontWeight})
      : super(key: key);

  @override
  _FSizeButtonState createState() => _FSizeButtonState();
}

class _FSizeButtonState extends State<FSizeButton> {
  bool isRequest = false;

  @override
  Widget build(BuildContext context) {
    List<Widget> children = [
      FText(
        widget.text,
        size: widget.txtSize ?? 32.sp,
        weight: widget.fontWeight ?? FontWeight.bold,
        color: widget.percolor != null
            ? const Color(0xFFFFFFFF)
            : (widget.isActivate
                ? const Color(0xFFFFFFFF)
                : const Color(0xFF58446C)),
      )
    ];
    if (isRequest) children.insert(0, CupertinoActivityIndicator(radius: 20.w));
    return GestureDetector(
      onTap: onPress,
      child: FContainer(
        gradient: widget.percolor != null
            ? [Color(0xFFFF753F), Color(0xFFFF753F)]
            : (widget.isActivate
                ? [const Color(0xFFFF753F), const Color(0xFFFF753F)]
                : [const Color(0xFFFF753F), const Color(0xFFFF753F)]),
        gradientBegin: Alignment.centerLeft,
        gradientEnd: Alignment.centerRight,
        height: widget.height,
        width: widget.width,
        radius: BorderRadius.circular(widget.radius ?? 44.w),
        align: Alignment.center,
        child: Row(
            mainAxisAlignment: MainAxisAlignment.center, children: children),
      ),
    );
  }

  void onPress() async {
    if (isRequest) return;
    if (widget.onPress != null) {
      widget.onPress!();
    } else if (widget.onRequest != null) {
      isRequest = true;
      if (!mounted) return;
      setState(() {});
      await widget.onRequest!();
      isRequest = false;
      if (!mounted) return;
      setState(() {});
    }
  }
}

class FCodeInput extends StatefulWidget {
  final Function(String) onInput;
  final Color? defaultColor;
  const FCodeInput(this.onInput, {Key? key, this.defaultColor})
      : super(key: key);

  @override
  _FCodeInputState createState() => _FCodeInputState();
}

class _FCodeInputState extends State<FCodeInput> {
  String inputText = "";
  FocusNode focusNode = FocusNode();

  @override
  void dispose() {
    super.dispose();
    focusNode.dispose();
  }

  @override
  Widget build(BuildContext context) {
    InputBorder border = const OutlineInputBorder(
        borderSide: BorderSide(color: Colors.transparent));
    return SizedBox(
      height: 88.w,
      child: Stack(children: [
        Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: List.generate(4, (index) => cellItem(index))),
        TextField(
          showCursor: false,
          maxLength: 4,
          focusNode: focusNode,
          onChanged: onTextInput,
          keyboardType: TextInputType.number,
          inputFormatters: [FilteringTextInputFormatter.digitsOnly],
          style: TextStyle(color: Colors.transparent, fontSize: 30.sp),
          decoration: InputDecoration(
            enabledBorder: border,
            focusedBorder: border,
            counterText: "",
            contentPadding: EdgeInsets.zero,
          ),
        )
      ]),
    );
  }

  Widget cellItem(int index) {
    Color borderColor = widget.defaultColor ?? const Color(0xFF26000000);
    String text = "";
    if (focusNode.hasFocus && (inputText.length > index)) {
      borderColor = const Color(0xFFFF753F);
      text = inputText[index];
    }

    return FContainer(
      width: 116.w,
      height: 88.w,
      radius: BorderRadius.circular(16.w),
      color: const Color(0xFFF5F6F7),
      border: Border.all(color: borderColor, width: 2.w),
      align: Alignment.center,
      child: FText(text, nullLength: 0, color: Colors.black),
    );
  }

  void onTextInput(String value) {
    inputText = value;
    widget.onInput(value);
    if (!mounted) return;
    setState(() {});
  }
}

class FSheetLine extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const SizedBox();
    // return FContainer(
    //   width: 100.w,
    //   height: 8.w,
    //   color: Colors.grey,
    //   margin: EdgeInsets.only(bottom: 10.w),
    //   radius: BorderRadius.circular(5.w),
    // );
  }
}
