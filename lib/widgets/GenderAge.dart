import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class GenderAge extends StatelessWidget {
  // 是否男性
  final bool isMale;
  final int birthday;
  final double? width;
  final double? height;
  final double? size;
  const GenderAge(this.isMale, this.birthday,
      {super.key, this.width, this.height, this.size});

  @override
  Widget build(BuildContext context) {
    String img = isMale ? "public/male" : "public/female";
    return Stack(
      alignment: AlignmentDirectional.center,
      children: [
        LoadAssetImage(img, width: width ?? 50.w, height: height ?? 25.w),
        Padding(
          padding: EdgeInsets.only(top: 1.5.w, left: 10.w),
          child: FText(
            UserConstants.getUserAge(birthday).toString(),
            size: size ?? 16.sp,
            color: isMale ? const Color(0xFF648FFF) : const Color(0xFFFF2FC0),
            weight: FontWeight.bold,
          ),
        )
      ],
    );
  }

  String getAge() {
    if (birthday.toString().length != 13) {
      return "0";
    }
    DateTime brt = DateTime.fromMillisecondsSinceEpoch(birthday);
    int age = 0;
    DateTime dateTime = DateTime.now();

    if (dateTime.isBefore(brt)) {
      return '0';
    }
    int yearNow = dateTime.year;
    int monthNow = dateTime.month;
    int dayOfMonthNow = dateTime.day;

    int yearBirth = brt.year;
    int monthBirth = brt.month;
    int dayOfMonthBirth = brt.day;

    age = yearNow - yearBirth;

    if (monthNow <= monthBirth) {
      if (monthNow == monthBirth) {
        if (dayOfMonthNow < dayOfMonthBirth) age--;
      } else {
        age--;
      }
    }
    return age.toString();
  }
}
