import 'package:echo/import.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CommonAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String? title;
  final Widget? titleChild;
  final double? elevation;
  final Color? backgroundColor;
  final Color? textColor;
  final List<Widget>? actions;
  final Function? onPressBack;
  final PreferredSizeWidget? bottom;

  const CommonAppBar(
      {Key? key,
      this.title,
      this.titleChild,
      this.actions,
      this.backgroundColor,
      this.elevation,
      this.onPressBack,
      this.bottom,
      this.textColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: elevation ?? 0,
      backgroundColor: backgroundColor ?? Colors.transparent,
      centerTitle: true,
      title: titleChild ??
          FText(
            title ?? "",
            size: 36.sp,
            color: textColor ?? Colors.black,
            weight: FontWeight.bold,
          ),
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back_ios,
          color: textColor ?? Colors.black,
        ),
        onPressed: _onPressBack,
      ),
      actions: actions,
      bottom: bottom,
    );
  }

  void _onPressBack() {
    if (onPressBack != null) {
      onPressBack!();
      return;
    }
    SystemChannels.textInput.invokeMethod('TextInput.hide');
    WidgetUtils().popPage();
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}

class CardAppBar extends StatelessWidget {
  const CardAppBar(
      {Key? key, required this.title, this.onPressBack, this.actions})
      : super(key: key);

  final String title;

  final List<Widget>? actions;

  final Function? onPressBack;

  @override
  Widget build(BuildContext context) {
    return Row(children: [
      Expanded(
          child: Align(
        alignment: Alignment.centerLeft,
        child: IconButton(
            onPressed: _onPressBack,
            icon: const Icon(Icons.arrow_back_ios, color: Colors.black)),
      )),
      Expanded(
        child: Text(title,
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Colors.black,
                fontSize: 36.sp,
                fontWeight: FontWeight.w600)),
      ),
      Expanded(
          child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [if (actions != null) ...actions!]))
    ]);
  }

  void _onPressBack() {
    if (onPressBack != null) {
      onPressBack!();
      return;
    }
    SystemChannels.textInput.invokeMethod('TextInput.hide');
    WidgetUtils().popPage();
  }
}
