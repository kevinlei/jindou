import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class AdolescentMode extends StatefulWidget {
  const AdolescentMode({super.key});

  @override
  State<AdolescentMode> createState() => _AdolescentModeState();
}

class _AdolescentModeState extends State<AdolescentMode> {
  @override
  Widget build(BuildContext context) {
    return FContainer(
      imageFit: BoxFit.fill,
      imageAlign: Alignment.topCenter,
      image: ImageUtils().getAssetsImage("home/bg"),
      child: Column(
        children: [
          SizedBox(height: 440.h),
          LoadAssetImage("person/youngBg", width: 200.w, height: 200.w),
          SizedBox(height: 40.h),
          Center(
              child: FText("青少年模式已开启",
                  size: 36.sp, color: Colors.black, weight: FontWeight.bold)),
          SizedBox(height: 80.h),
          FcationButton("关闭青少年模式",
              isActivate: true, radius: 90.w, onPress: onTapNext),
        ],
      ),
    );
  }

  void onTapNext() async {
    await WidgetUtils().pushPage(const YoungPage(), fun: onCare);
  }

  void onCare() async {
    UserConstants.userginInfo?.info.teenmode = 0;
    EventUtils().emit(EventConstants.E_MQ_ADOMODE, null);
    setState(() {});
  }
}
