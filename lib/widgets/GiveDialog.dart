import 'dart:convert';

import 'package:echo/import.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

// 送礼操作框
class GiveDialog extends StatefulWidget {
  final bool isRoom;
  final bool isSeat;
  final String? userId;
  const GiveDialog(this.isRoom, {super.key, this.userId, this.isSeat = true});

  @override
  State<GiveDialog> createState() => _GiveDialogState();
}

class _GiveDialogState extends State<GiveDialog>
    with SingleTickerProviderStateMixin {
  late TabController tabController;

  // 操作栏
  List<String> tabList = [];

  // 当前选择礼物id
  int giftId = -1;

  // 当前选择赠送数量
  int giftNumber = 1;

  // 是否打开数量器
  bool isNumbers = false;

  // 是否全选麦位
  bool isAllSeat = false;

  // 收礼人
  List<Mic> seatGiftList = [];

  // 麦位列表
  List<Mic> seatLists = [];
  // 麦位列表备份
  List<Mic> seatListsbackups = [];

  // 全部礼物列表
  List<GiftInfo> allGiftList = [];

  // 背包礼物列表
  List<GiftInfo> myGiftList = [];
  // 背包礼物总价值
  int bagListPrice = 0;

  // 盲盒礼物列表
  List<MagicBox> magicBoxList = [];

  // 是否正在赠送礼物中
  bool isSendGift = false;

  // 是否盲盒
  int isMagic = -1;

  @override
  void initState() {
    super.initState();
    allGiftList = AppManager().roomInfo.gifts;
    seatListsbackups = [...AppManager().roomInfo.seatLists];

    requestDynamicInfo();

    if (widget.userId != null &&
        widget.userId == UserConstants.userginInfo!.info.userId) {
      tabList.add("背包");
    } else {
      tabList.add("礼物");
      tabList.add("背包");
    }

    tabController =
        TabController(length: tabList.length, vsync: this, initialIndex: 0);

    if (widget.isRoom) {
      initSeatList(true, isInit: true);
    }

    if (!(widget.userId != null &&
        widget.userId == UserConstants.userginInfo!.info.userId)) {
      getGiftList("礼物");
    }

    getGiftList("背包");
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  // 更新用户财富信息
  void requestDynamicInfo() {
    AppManager().requestDynamicInfo();
    if (!mounted) return;
    setState(() {});
  }

  // 初始化麦位列表
  void initSeatList(bool isAll, {bool isInit = false}) {
    List<Mic> list = [];
    for (var i = 0; i < seatListsbackups.length; i++) {
      if (seatListsbackups[i].micUserInfo.exUserId != 0 &&
          (seatListsbackups[i].micUserInfo.userId !=
                  UserConstants.userginInfo!.info.userId ||
              !isAll)) {
        String seatInfo = jsonEncode(seatListsbackups[i]);
        list.add(Mic.fromJson(jsonDecode(seatInfo)));
      }

      if (isInit) {
        if (widget.userId != null &&
            widget.userId == seatListsbackups[i].micUserInfo.userId) {
          String seatInfo = jsonEncode(seatListsbackups[i]);
          seatGiftList.add(Mic.fromJson(jsonDecode(seatInfo)));
        }
      }
    }
    seatLists = [...list];
    if (!mounted) return;
    setState(() {});
  }

  // 初始化礼物列表
  void getGiftList(String text) async {
    String http = "";
    Map<String, dynamic>? params;

    switch (text) {
      case "礼物":
        http = NetConstants.allGift;
        params = {"extra_param": "magic_box"};
        break;
      default:
        http = NetConstants.knapsackGift;
    }

    await DioUtils().httpRequest(
      http,
      loading: false,
      params: params,
      onSuccess: (res) {
        if (res == null) return;
        if (text == "礼物") {
          var data = jsonDecode(res);
          List<GiftInfo> gifts = [];
          for (var i = 0; i < data.length; i++) {
            GiftInfo info = GiftInfo.fromJson(data[i]);
            gifts.add(info);
          }
          allGiftList = gifts;
        } else if (text == "背包") {
          List<GiftInfo> gifts = [];
          for (var i = 0; i < res["gift_bag_infos"].length; i++) {
            GiftInfo info = GiftInfo.fromJson(res["gift_bag_infos"][i]);
            gifts.add(info);
          }
          myGiftList = gifts;
          bagListPrice = res["total_value"];
        }
        setState(() {});
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> childList = [];
    for (var i = 0; i < tabList.length; i++) {
      if (tabList[i] == "礼物") {
        childList.add(giftListWdg(true, allGiftList));
      } else if (tabList[i] == "背包") {
        childList.add(giftListWdg(false, myGiftList));
      }
    }

    return FContainer(
      padding: EdgeInsets.only(top: 20.w),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Visibility(visible: isMagic == 3, child: giftTips()),
          Visibility(
              visible: widget.isRoom && widget.isSeat,
              child: seatListContent()),
          FContainer(
            padding:
                EdgeInsets.only(bottom: ScreenUtil().bottomBarHeight + 50.w),
            color: widget.isRoom ? Colors.black : Colors.white,
            radius: widget.isRoom ? null : BorderRadius.circular(20.w),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                tabber(),
                SizedBox(height: 20.w),
                FContainer(
                  padding: EdgeInsets.symmetric(horizontal: 20.w),
                  height: 500.w,
                  child: TabBarView(
                    physics: const NeverScrollableScrollPhysics(),
                    controller: tabController,
                    children: childList,
                  ),
                ),
                SizedBox(height: 30.w),
                givePrice(),
              ],
            ),
          )
        ],
      ),
    );
  }

  // 礼物tips
  Widget giftTips() {
    List gifts = [];
    for (var i = 0; i < AppManager().roomInfo.magicBoxGiftList.length; i++) {
      if (AppManager().roomInfo.magicBoxGiftList[i]["game_id"] == giftId) {
        gifts.add(AppManager().roomInfo.magicBoxGiftList[i]);
      }
    }

    List<Widget> list = [];
    for (var i = 0; i < (gifts.length > 6 ? 6 : gifts.length); i++) {
      list.add(FContainer(
        width: 60.w,
        height: 60.w,
        padding: EdgeInsets.all(5.w),
        child: FContainer(
          width: double.infinity,
          height: double.infinity,
          color: Colors.black.withOpacity(0.2),
          radius: BorderRadius.circular(10.w),
          image: ImageUtils().getImageProvider(
              NetConstants.ossPath + gifts[i]["gift_picture"]),
          imageFit: BoxFit.fill,
        ),
      ));
    }

    return FContainer(
      margin: EdgeInsets.symmetric(horizontal: 15.w),
      padding: EdgeInsets.symmetric(vertical: 15.w),
      radius: BorderRadius.circular(20.w),
      height: 108.w,
      image: ImageUtils().getAssetsImage("room/zhouxingbang"),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          FContainer(
            margin: EdgeInsets.only(left: 30.w, right: 10.w),
            child: FText(
              "有机会送出",
              size: 24.sp,
            ),
          ),
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: list,
            ),
          ),
          GestureDetector(
            onTap: lockMagicGiftList,
            child: FContainer(
              margin: EdgeInsets.only(left: 10.w),
              width: 130.w,
              height: double.infinity,
              child: Visibility(
                visible: AppManager().roomInfo.magicBoxUrl.isNotEmpty,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(width: 5.w),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        FText(
                          "查看",
                          size: 22.sp,
                          wordSpacing: 1.2,
                        ),
                        FText(
                          "详情",
                          size: 22.sp,
                          wordSpacing: 1.2,
                        ),
                      ],
                    ),
                    SizedBox(width: 5.w),
                    LoadAssetImage(
                      "public/RightNavigation",
                      width: 18.w,
                      color: Colors.white,
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  // 选择赠送礼物人
  Widget seatListContent() {
    Widget list = ListView.separated(
      padding: const EdgeInsets.all(0),
      scrollDirection: Axis.horizontal,
      separatorBuilder: (_, __) => SizedBox(width: 30.w),
      itemBuilder: (_, index) => seatCellContent(seatLists[index]),
      itemCount: seatLists.length,
    );

    return FContainer(
      margin: EdgeInsets.all(15.w),
      padding: EdgeInsets.symmetric(vertical: 15.w),
      radius: BorderRadius.circular(20.w),
      height: 108.w,
      color: Colors.black,
      child: Row(children: [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 20.w),
          child:
              FText("送给", size: 26.sp, color: Colors.white.withOpacity(0.35)),
        ),
        Expanded(child: list),
        btnAllSeat()
      ]),
    );
  }

  Widget seatCellContent(Mic micInfo) {
    String text = "";
    switch (micInfo.micNum) {
      case 1:
        text = "房主";
        break;
      case 2:
        text = "主持";
        break;
      default:
        text = "${micInfo.micNum - 1}";
    }

    return GestureDetector(
      onTap: () => onTapSeat(micInfo),
      child: SizedBox(
        height: 80.w,
        child: Stack(alignment: Alignment.topCenter, children: [
          FContainer(
            width: 70.w,
            height: 70.w,
            radius: BorderRadius.circular(50),
            imageFit: BoxFit.cover,
            image: ImageUtils().getImageProvider(
                NetConstants.ossPath + micInfo.micUserInfo.userImage),
            border: isSelectUser(micInfo)
                ? Border.all(color: const Color(0xFFFF753F))
                : null,
          ),
          Positioned(
            bottom: 0,
            child: FContainer(
              padding: EdgeInsets.all(5.w),
              width: 50.w,
              align: Alignment.center,
              radius: BorderRadius.circular(50),
              color: isSelectUser(micInfo)
                  ? const Color(0xFFFF753F)
                  : Colors.white,
              child: FText(
                text,
                size: 18.sp,
                weight: FontWeight.w500,
                color: isSelectUser(micInfo) ? Colors.white : Colors.black,
              ),
            ),
          ),
        ]),
      ),
    );
  }

  Widget btnAllSeat() {
    return GestureDetector(
      onTap: () {
        isAllSeat = !isAllSeat;
        !isAllSeat ? seatGiftList = [] : seatGiftList = [...seatLists];
        setState(() {});
      },
      child: FContainer(
        height: 130.w,
        padding: EdgeInsets.symmetric(horizontal: 20.w),
        align: Alignment.center,
        child: FContainer(
          width: 103.w,
          height: 53.w,
          radius: BorderRadius.circular(50),
          color: Color(isAllSeat ? 0xFFFF753F : 0xFF807E7F),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              FContainer(
                width: 78.w,
                height: 52.w,
                radius: BorderRadius.circular(50),
                align: Alignment.center,
                color: Colors.white,
                child: FText("全选",
                    color: isAllSeat ? const Color(0xFFFF753F) : Colors.black),
              ),
              Visibility(visible: !isAllSeat, child: const Spacer()),
            ],
          ),
        ),
      ),
    );
  }

  Widget tabber() {
    List<Widget> childList = [];
    for (var i = 0; i < tabList.length; i++) {
      childList.add(tabberItem(i, tabList[i]));
    }
    childList.addAll([
      const Spacer(),
      Visibility(
        visible: (seatGiftList.length == 1 || widget.userId != null) &&
            tabList[tabController.index] == "背包",
        child: GestureDetector(
          onTap: backAllSend,
          child: FText(
            "一键全送",
            color: widget.isRoom ? Colors.white : Colors.black,
          ),
        ),
      )
    ]);

    return FContainer(
      padding: EdgeInsets.symmetric(vertical: 10.w, horizontal: 40.w),
      border: Border(
          bottom: BorderSide(
              width: 1.w,
              color: Color(widget.isRoom ? 0xFFFFFFFF : 0xFF222222)
                  .withOpacity(0.2))),
      child: Row(
        children: childList,
      ),
    );
  }

  Widget tabberItem(int index, String text) {
    return GestureDetector(
      onTap: () {
        if (tabController.index != index) {
          tabController.animateTo(index);
          giftId = -1;
          isMagic = -1;
          giftNumber = 1;
          seatGiftList = [];
          isAllSeat = false;
          setState(() {});
          initSeatList(text != "背包");
          getGiftList(text);
        }
      },
      child: FContainer(
        margin: EdgeInsets.only(right: 30.w),
        padding: EdgeInsets.symmetric(horizontal: 10.w),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            FText(
              text,
              size: 34.sp,
              color: tabController.index == index
                  ? widget.isRoom
                      ? Colors.white
                      : Colors.black
                  : Color(widget.isRoom ? 0xFFFFFFFF : 0xFF222222)
                      .withOpacity(0.35),
              weight: FontWeight.bold,
              wordSpacing: 1.1,
            ),
            SizedBox(height: 10.w),
            FContainer(
              width: 34.w,
              height: 8.w,
              radius: BorderRadius.circular(50),
              gradientBegin: Alignment.centerLeft,
              gradientEnd: Alignment.centerRight,
              gradient: tabController.index == index
                  ? const [
                      Color(0xFFFF8057),
                      Color(0xFFFF5883),
                      Color(0xFFFD96FF)
                    ]
                  : widget.isRoom
                      ? [
                          Colors.black,
                          Colors.black,
                          Colors.black,
                        ]
                      : [
                          Colors.white,
                          Colors.white,
                          Colors.white,
                        ],
            )
          ],
        ),
      ),
    );
  }

  // 礼物列表
  Widget giftListWdg(bool isGift, List<GiftInfo> list) {
    return GridView.builder(
      padding: const EdgeInsets.all(0),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 4,
        mainAxisSpacing: 10.w,
        crossAxisSpacing: 10.w,
        childAspectRatio: 0.75,
      ), // 数据数量
      itemCount: list.length,
      // 所有数据
      itemBuilder: (_, index) => giftItem(list[index], isGift),
    );
  }

  // 礼物
  Widget giftItem(GiftInfo giftInfo, bool isGift) {
    return GestureDetector(
      onTap: () {
        if (giftId == giftInfo.id) {
          isMagic = -1;
          giftId = -1;
        } else {
          if (tabList[tabController.index] != "背包") {
            if (giftInfo.isActivity == 3) {
              initSeatList(false);
            } else {
              initSeatList(true);
              for (var i = 0; i < seatGiftList.length; i++) {
                if (seatGiftList[i].micUserInfo.userId ==
                    UserConstants.userginInfo!.info.userId) {
                  seatGiftList.remove(seatGiftList[i]);
                  break;
                }
              }
            }
          }
          isMagic = giftInfo.isActivity;
          giftId = giftInfo.id;
        }
        setState(() {});
      },
      child: FContainer(
        padding: EdgeInsets.all(10.w),
        radius: BorderRadius.circular(20.w),
        border: Border.all(
            color: giftId == giftInfo.id && isMagic == giftInfo.isActivity
                ? const Color(0xFF8C8C8C)
                : Colors.transparent),
        child: Column(
          children: [
            Expanded(
              child: FContainer(
                radius: BorderRadius.circular(20.w),
                imageFit: BoxFit.cover,
                image: ImageUtils().getImageProvider(
                    NetConstants.ossPath + giftInfo.giftPicture),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Visibility(
                      visible: (!isGift)&&giftInfo.label.isNotEmpty,
                      child: FContainer(
                        radius: BorderRadius.circular(50),
                        padding: EdgeInsets.symmetric(
                            vertical: 2.w, horizontal: 10.w),
                        gradient: const [
                          Color(0xFF4DF1E5),
                          Color(0xFFB12AFF),
                        ],
                        gradientBegin: Alignment.centerLeft,
                        gradientEnd: Alignment.centerRight,
                        child: FText(
                          giftInfo.label,
                          size: 18.h,
                        ),
                      ),
                    ),
                    const Spacer(),
                    Visibility(
                      visible: !isGift,
                      child: FContainer(
                        radius: BorderRadius.circular(50),
                        padding: EdgeInsets.symmetric(
                            vertical: 2.w, horizontal: 10.w),
                        color: Colors.red,
                        child: FText(
                          "x${giftInfo.giftNum}",
                          size: 18.h,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            SizedBox(height: 5.w),
            FText(
              giftInfo.giftName,
              color: widget.isRoom ? Colors.white : Colors.black,
            ),
            SizedBox(height: 5.w),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                LoadAssetImage("person/myGold", width: 20.w),
                SizedBox(width: 3.w),
                FText("${giftInfo.giftPrice} ",
                    size: 20.sp, color: const Color(0xFFFF753F)),
              ],
            )
          ],
        ),
      ),
    );
  }

  // 盲盒列表
  Widget magicBoxWdgList() {
    return GridView.builder(
      padding: const EdgeInsets.all(0),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
        mainAxisSpacing: 10.w,
        crossAxisSpacing: 50.w,
        childAspectRatio: 0.85,
      ), // 数据数量
      itemCount: magicBoxList.length,
      // 所有数据
      itemBuilder: (_, index) => magicBox(magicBoxList[index]),
    );
  }

  // 盲盒
  Widget magicBox(MagicBox info) {
    return GestureDetector(
      onTap: () {
        giftId == info.id ? giftId = -1 : giftId = info.gameId;
        setState(() {});
      },
      child: FContainer(
        padding: EdgeInsets.all(10.w),
        radius: BorderRadius.circular(20.w),
        border: Border.all(
            color: giftId == info.gameId
                ? const Color(0xFF8C8C8C)
                : Colors.transparent),
        child: Column(
          children: [
            Expanded(
                child: FContainer(
              image: ImageUtils()
                  .getImageProvider(NetConstants.ossPath + info.gameLogo),
              imageFit: BoxFit.fill,
            )),
            SizedBox(height: 10.w),
            FText(
              info.gameName,
              size: 22.sp,
              color: widget.isRoom ? Colors.white : Colors.black,
            ),
            FText(
              info.gameScale.toString(),
              size: 20.sp,
              color: widget.isRoom
                  ? Colors.white.withOpacity(0.7)
                  : Colors.black.withOpacity(0.7),
            ),
          ],
        ),
      ),
    );
  }

  Widget givePrice() {
    return FContainer(
      height: 65.w,
      padding: EdgeInsets.symmetric(horizontal: 20.w),
      child: Row(
        children: [
          tabList[tabController.index] != "背包"
              ? Row(
                  children: [
                    LoadAssetImage("person/myGold", width: 28.w),
                    SizedBox(width: 10.w),
                    FText(
                        "${FuncUtils().formatNum(UserConstants.userginInfo!.goldInfo.gold, 2)} ",
                        size: 30.sp,
                        color: const Color(0xFFFF753F)),
                    GestureDetector(
                      onTap: () async {
                        await WidgetUtils().pushPage(ChargePage());
                        setState(() {});
                      },
                      child: FContainer(
                        color: Colors.transparent,
                        child: FText(
                          "| 充值",
                          size: 30.sp,
                          color: Color(widget.isRoom ? 0xFFFFFFFF : 0xFF222222)
                              .withOpacity(0.5),
                          weight: FontWeight.w300,
                        ),
                      ),
                    )
                  ],
                )
              : FContainer(
                  color: Colors.transparent,
                  child: Row(
                    children: [
                      FText(
                        "礼物总值：",
                        size: 30.sp,
                        color: Color(widget.isRoom ? 0xFFFFFFFF : 0xFF222222)
                            .withOpacity(0.5),
                        weight: FontWeight.w300,
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 8.w),
                        child: FText(
                          "$bagListPrice",
                          size: 30.sp,
                          color: const Color(0xFFFF753F),
                          weight: FontWeight.w300,
                        ),
                      ),
                    ],
                  ),
                ),
          const Spacer(),
          countSelect(),
        ],
      ),
    );
  }

  Widget countSelect() {
    return FContainer(
      width: 240.w,
      height: 65.w,
      radius: BorderRadius.circular(50),
      border: Border.all(
        color: const Color(0xFFFF753F),
      ),
      child: Row(children: [Flexible(child: btnCount()), btnSend()]),
    );
  }

  Widget btnCount() {
    Widget btn = FContainer(
      padding: EdgeInsets.symmetric(horizontal: 10.w),
      align: Alignment.center,
      child: Row(children: [
        Expanded(
            child: Center(
                child: FText("$giftNumber",
                    color: widget.isRoom ? Colors.white : Colors.black,
                    size: 24.sp))),
        Visibility(
            visible: giftId != -1,
            child: Icon(
              isNumbers ? Icons.keyboard_arrow_up : Icons.keyboard_arrow_down,
              size: 40.w,
              color: widget.isRoom ? Colors.white : Colors.black,
            ))
      ]),
    );
    return GestureDetector(onTap: onTapCount, child: btn);
  }

  Widget btnSend() {
    Widget btn = FContainer(
      width: 110.w,
      height: 65.w,
      gradientBegin: Alignment.topLeft,
      gradientEnd: Alignment.bottomRight,
      gradient: const [
        Color(0xFF96FCFC),
        Color(0xFFFFA7FB),
        Color(0xFFFF82B2),
      ],
      radius: const BorderRadius.only(
          topRight: Radius.circular(50), bottomRight: Radius.circular(50)),
      align: Alignment.center,
      child: FText("赠送", size: 28.sp, weight: FontWeight.w500),
    );
    return GestureDetector(onTap: onTapSend, child: btn);
  }

  void onTapCount() async {
    if (giftId == -1) return;
    setState(() {
      isNumbers = true;
    });
    int? num = await WidgetUtils()
        .showFDialog(CountSelect(isMagic == 3), barrier: Colors.transparent);
    setState(() {
      giftNumber = num is int && num > 0 ? num : 1;
      isNumbers = false;
    });
  }

  // 是否已选择该用户
  bool isSelectUser(Mic user) {
    for (var i = 0; i < seatGiftList.length; i++) {
      if (seatGiftList[i].micNum == user.micNum) {
        return true;
      }
    }
    return false;
  }

  // 点击收礼人
  void onTapSeat(Mic user) {
    if (isSelectUser(user)) {
      seatGiftList.remove(user);
    } else {
      seatGiftList.add(user);
    }
    if (seatGiftList.length == seatLists.length) {
      isAllSeat = true;
    } else {
      isAllSeat = false;
    }
    setState(() {});
  }

  // 背包礼物一键全送
  void backAllSend() async {
    bool? accept = await WidgetUtils().showAlert("是否将背包礼物一键全送?");
    if (accept != true) return;
    onTapSend(isBackAll: true);
  }

  // 赠送礼物
  void onTapSend({bool isBackAll = false}) async {
    if (isSendGift) return;
    if ((widget.isRoom && seatGiftList.isEmpty) &&
        !(widget.isRoom && !widget.isSeat && widget.userId != null)) {
      return WidgetUtils().showToast("请选择收礼人");
    }
    if (giftId == -1 && !isBackAll) {
      return WidgetUtils().showToast("请选择要赠送的礼物");
    }
    if (isMagic == 3 && giftNumber > 188) {
      return WidgetUtils().showToast("盲盒礼物最大赠送188个");
    }
    isSendGift = true;

    // 被赠送人
    List users = [];
    if (widget.isRoom && widget.isSeat) {
      for (var i = 0; i < seatGiftList.length; i++) {
        var user = {
          "mic_id": seatGiftList[i].micNum,
          "obtain_user_id": seatGiftList[i].micUserInfo.userId,
        };
        users.add(user);
      }
    } else {
      int micId = -1;
      for (var i = 0; i < seatLists.length; i++) {
        if (seatLists[i].micUserInfo.userId == widget.userId &&
            widget.userId != null) {
          micId = seatLists[i].micNum;
          break;
        }
      }
      users.add({
        "mic_id": micId,
        "obtain_user_id": widget.userId,
      });
    }
    debugPrint("被赠送人$users, 赠送礼物id$giftId, 赠送礼物数量$giftNumber");

    if (isMagic == 3) {
      sendMagicBox(users);
    } else {
      sendGift(users, isBackAll);
    }
  }

  // 赠送礼物
  void sendGift(List users, bool isBackAll) async {
    await DioUtils().httpRequest(
      NetConstants.sendGift,
      method: DioMethod.POST,
      loading: false,
      params: {
        "obtain_users": users,
        "send_type": tabList[tabController.index] == "背包" ? 1 : 2,
        "send_gift_info": [
          {
            "send_gift_id": giftId,
            "send_gift_num": giftNumber,
          }
        ],
        "src": widget.isRoom ? "room" : "notice",
        "bag_send_type": isBackAll ? 1 : 0,
        "reason":
            "${UserConstants.userginInfo!.info.name}用户${widget.isRoom ? "在${AppManager().roomInfo.roomMode!.roomName}" : ""}赠送给$users用户$giftId礼物",
        "room_id": widget.isRoom ? AppManager().roomInfo.roomMode!.roomId : -1,
      },
      onSuccess: (res) => sendRetrun(res, users),
      onError: (code, msg) => isSendGift = false,
    );
  }

  // 赠送盲盒
  void sendMagicBox(List users) async {
    List uids = [];
    for (var i = 0; i < users.length; i++) {
      uids.add({
        "uid": users[i]["obtain_user_id"],
        "pit_id": users[i]["mic_id"],
      });
    }
    await DioUtils().httpRequest(
      NetConstants.sendBlindBoxGift,
      method: DioMethod.POST,
      loading: false,
      params: {
        "send_detail": uids,
        "box_game_id": giftId,
        "box_num": giftNumber,
        "room_id": widget.isRoom ? AppManager().roomInfo.roomMode!.roomId : -1,
      },
      onSuccess: (res) => sendRetrun(res, uids),
      onError: (code, msg) => isSendGift = false,
    );
  }

  void sendRetrun(res, List uids) {
    Future.delayed(const Duration(seconds: 0), () async {
      AppManager().xinstallFlutterPlugin.reportEventWhenOpenDetailInfo(
          widget.isRoom ? "private" : "Gifts",
          1,
          "${UserConstants.userginInfo!.info.userId}-$uids-${widget.isRoom ? AppManager().roomInfo.roomMode!.roomName : "私聊"})");
    });

    if (res == null) return WidgetUtils().popPage();

    if (widget.isRoom ||
        widget.userId == UserConstants.userginInfo!.info.userId) {
      WidgetUtils().popPage();
    } else {
      List<HXGiftMsg> giftList = [];
      List<SendGiftInfo> sendGiftList =
          (res["user_get_gift_infos"] as List<dynamic>)
              .map((e) => SendGiftInfo.fromJson(e as Map<String, dynamic>))
              .toList();
      for (var info in sendGiftList[0].sendGiftInfos) {
        giftList.add(HXGiftMsg(
            info.giftPicture, info.giftNum, info.giftPrice, info.giftName));
      }
      WidgetUtils().popPage(giftList);
    }

    if (res["operator_user_gold"]["gold"] != null) {
      UserConstants.userginInfo!.goldInfo.gold =
          res["operator_user_gold"]["gold"];
      UserConstants.userginInfo!.gradeInfo.userGrade.wealth.grade =
          res["operator_user_gold"]["wealth"]["grade"];
      UserConstants.userginInfo!.gradeInfo.userGrade.wealth.experience =
          res["operator_user_gold"]["wealth"]["experience"];
      UserConstants.userginInfo!.gradeInfo.userGrade.wealth.nowExperience =
          res["operator_user_gold"]["wealth"]["now_experience"];
      UserConstants.userginInfo!.gradeInfo.userGrade.wealth.badge =
          res["operator_user_gold"]["wealth"]["badge"];
    }
  }

  // 查看魔盒礼物列表
  void lockMagicGiftList() async {
    if (AppManager().time >= 999) return WidgetUtils().showToast("网络异常");
    if (AppManager().roomInfo.magicBoxUrl.isEmpty) return;
    await showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      isScrollControlled: true,
      isDismissible: false,
      enableDrag: false,
      builder: (_) => RoomGame(
        url: AppManager().roomInfo.magicBoxUrl,
        magicBoxId: giftId,
      ),
    );
  }
}

// 选择赠送礼物数量
class CountSelect extends StatelessWidget {
  final bool isMagic;
  const CountSelect(this.isMagic, {super.key});

  @override
  Widget build(BuildContext context) {
    double bottom = ScreenUtil().bottomBarHeight + 150.w;
    return Stack(alignment: Alignment.bottomRight, children: [
      Positioned(
        bottom: bottom,
        right: 20.w,
        child: FContainer(
          padding: EdgeInsets.all(12.w),
          border: Border.all(color: const Color(0xFF000000).withOpacity(0.2)),
          radius: BorderRadius.circular(20.w),
          color: const Color(0xFFF6F6F6),
          width: 360.w,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              isMagic
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        header("50", 50, 0xFFFF6F42),
                        header("100", 100, 0xFFFF5454),
                        header("188", 188, 0xFF7D4EFF),
                      ],
                    )
                  : Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        header("520", 520, 0xFFFF6F42),
                        header("1314", 1314, 0xFFFF5454),
                        header("自定义", 0, 0xFF7D4EFF),
                      ],
                    ),
              SizedBox(height: 20.w),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  isMagic
                      ? body("30", 30, 0xFF8CC14B)
                      : body("50", 50, 0xFF8CC14B),
                  body("10", 10, 0xFF03A9F5),
                ],
              ),
              SizedBox(height: 20.w),
              footer()
            ],
          ),
        ),
      )
    ]);
  }

  Widget header(String text, int num, int color) {
    return GestureDetector(
      onTap: () => onTapNumber(num),
      child: FContainer(
        width: 106.w,
        height: 106.w,
        color: Color(color),
        radius: BorderRadius.circular(50),
        align: Alignment.center,
        child: FText(
          text,
          size: 26.sp,
        ),
      ),
    );
  }

  Widget body(String text, int num, int color) {
    return GestureDetector(
      onTap: () => onTapNumber(num),
      child: FContainer(
        width: 150.w,
        padding: EdgeInsets.symmetric(vertical: 10.w),
        radius: BorderRadius.circular(50),
        color: Color(color),
        align: Alignment.center,
        child: FText(
          text,
          size: 26.sp,
        ),
      ),
    );
  }

  Widget footer() {
    List<Widget> list = [];
    for (var i = 9; i > 0; i--) {
      list.add(footerItem("$i", i, 0xFFFF753F));
    }
    return GridView.count(
      crossAxisCount: 3,
      mainAxisSpacing: 12.w,
      crossAxisSpacing: 12.w,
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      childAspectRatio: 1.8,
      children: list,
    );
  }

  Widget footerItem(String text, int num, int color) {
    return GestureDetector(
      onTap: () => onTapNumber(num),
      child: FContainer(
        color: Color(color),
        radius: BorderRadius.circular(50),
        align: Alignment.center,
        child: FText(
          text,
          size: 26.sp,
        ),
      ),
    );
  }

  void onTapNumber(int num) async {
    int number = num;
    if (number == 0) {
      int? zdyNum = await WidgetUtils()
          .showBottomSheet(const GiftNumberInput(), enableDrag: false);
      if (zdyNum is int && zdyNum > 0) {
        number = zdyNum;
      }
    }
    WidgetUtils().popPage(number);
  }
}

// 自定义赠送礼物数量
class GiftNumberInput extends StatefulWidget {
  const GiftNumberInput({Key? key}) : super(key: key);

  @override
  _GiftNumberInputState createState() => _GiftNumberInputState();
}

class _GiftNumberInputState extends State<GiftNumberInput> {
  // 输入值
  int? num;

  @override
  Widget build(BuildContext context) {
    return RawKeyboardListener(
      focusNode: FocusNode(),
      onKey: (event) {
        if (event.runtimeType == RawKeyDownEvent) {
          if (event.logicalKey.keyId == 4294967309) {
            WidgetUtils().popPage();
          }
        }
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        mainAxisSize: MainAxisSize.min,
        children: [
          FContainer(
            color: const Color(0xFF0B0C17),
            padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 10.w),
            child: Row(children: [
              Expanded(child: inputField()),
              SizedBox(width: 10.w),
            ]),
          ),
          SizedBox(height: MediaQuery.of(context).viewInsets.bottom)
        ],
      ),
    );
  }

  Widget inputField() {
    InputBorder border = OutlineInputBorder(
        borderSide: const BorderSide(color: Colors.transparent),
        borderRadius: BorderRadius.circular(20.w));
    Widget child = TextField(
      style: TextStyle(color: Colors.white, fontSize: 28.sp),
      maxLines: 2,
      minLines: 1,
      autofocus: true,
      maxLength: 4,
      keyboardType: TextInputType.number,
      inputFormatters: [FilteringTextInputFormatter.allow(RegExp(r"[0-9]"))],
      onChanged: (value) {
        num = value.isNotEmpty ? int.parse(value) : null;
      },
      decoration: InputDecoration(
          focusedBorder: border,
          enabledBorder: border,
          filled: true,
          fillColor: const Color(0xFFFFFFFF).withOpacity(0.05),
          contentPadding: EdgeInsets.symmetric(horizontal: 10.w),
          counterText: "",
          hintText: "",
          hintStyle: TextStyle(color: const Color(0xFFBBBBBB), fontSize: 28.sp),
          suffix: buttonSend()),
    );
    return SizedBox(height: 70.w, child: child);
  }

  Widget buttonSend() {
    Widget btn = FButton(
      width: 105.w,
      height: 55.w,
      align: Alignment.center,
      radius: BorderRadius.circular(50),
      isTheme: true,
      child: FText(
        "完成",
        size: 26.sp,
      ),
    );
    return GestureDetector(onTap: onTapSend, child: btn);
  }

  void onTapSend() async {
    WidgetUtils().popPage(num);
  }
}
