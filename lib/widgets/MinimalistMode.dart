import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class MinimalistMode extends StatelessWidget {
  final String text;
  final String span;
  final bool miniMode;
  final Function(bool) onTap;
  final String confirmText;
  final String cancelText;
  const MinimalistMode(this.text, this.span, this.onTap,
      {Key? key,
      this.miniMode = false,
      this.confirmText = "开启",
      this.cancelText = "取消"})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: FContainer(
        width: 570.w,
        radius: BorderRadius.circular(20.w),
        color: Color(0xFF222222).withOpacity(0.5),
        padding: EdgeInsets.fromLTRB(25.w, 40.h, 25.w, 0),
        child: Column(mainAxisSize: MainAxisSize.min, children: [
          FText(text, size: 36.sp, color: Color(0xFFE2E2E2)),
          SizedBox(height: 20.h),
          Text.rich(TextSpan(
              text: span,
              style: TextStyle(
                  color: Color(0xFFE2E2E2),
                  fontSize: 28.sp,
                  height: 1.5,
                  letterSpacing: 1.2))),
          Row(
            children: [
              Visibility(
                  visible: !miniMode,
                  child: Expanded(
                      child: FLoginButton(confirmText,
                          isActivate: true,
                          radius: 90.w,
                          padding: 0, onPress: () {
                    onTap(true);
                    WidgetUtils().popPage();
                  }))),
              Visibility(
                  visible: !miniMode,
                  child: SizedBox(
                    width: 10.w,
                  )),
              Expanded(
                  child: TextButton(
                      child: FText(cancelText,
                          size: 28.sp, color: Color(0xFFB6B6B6)),
                      onPressed: () => WidgetUtils().popPage())),
              Visibility(
                  visible: miniMode,
                  child: SizedBox(
                    width: 10.w,
                  )),
              Visibility(
                  visible: miniMode,
                  child: Expanded(
                      child: TextButton(
                          child: FText("关闭",
                              size: 28.sp, color: Color(0xFFB6B6B6)),
                          onPressed: () {
                            onTap(false);
                            WidgetUtils().popPage();
                          }))),
            ],
          ),
          SizedBox(height: 20.h),
        ]),
      ),
    );
  }
}
