import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'my_widgets.dart';

Widget myLRItem({
  Key? key,
  required Widget leftWidget,
  required Widget rightWidget,
  GestureTapCallback? onTap,
  AlignmentGeometry? alignment,
  Color? color,
  double? height,
  double? width = double.infinity,
  Decoration? decoration,
  EdgeInsetsGeometry? padding,
  EdgeInsetsGeometry? margin,
  Decoration? foregroundDecoration,
  BoxConstraints? constraints,
  MainAxisSize mainAxisSize = MainAxisSize.max,
  MainAxisAlignment mainAxisAlignment = MainAxisAlignment.spaceBetween,
  CrossAxisAlignment crossAxisAlignment = CrossAxisAlignment.center,
}) {
  return myGestureDetector(
      child: myRow(
          width: width,
          alignment: alignment,
          color: color,
          height: height,
          padding: padding,
          margin: margin,
          decoration: decoration,
          foregroundDecoration: foregroundDecoration,
          constraints: constraints,
          children: [leftWidget, rightWidget],
          mainAxisSize: mainAxisSize,
          mainAxisAlignment: mainAxisAlignment,
          crossAxisAlignment: crossAxisAlignment),
      onTap: onTap);
}

Widget myLRRItem({
  Key? key,
  required Widget leftWidget,
  required Widget rightWidget1,
  required Widget rightWidget2,
  GestureTapCallback? onTap,
  AlignmentGeometry? alignment,
  Color? color,
  double? height,
  Decoration? decoration,
  EdgeInsetsGeometry? padding,
  EdgeInsetsGeometry? margin,
  Decoration? foregroundDecoration,
  BoxConstraints? constraints,
  MainAxisAlignment mainAxisAlignment = MainAxisAlignment.start,
  CrossAxisAlignment crossAxisAlignment = CrossAxisAlignment.center,
}) {
  return myGestureDetector(
      child: myRow(
          width: double.infinity,
          alignment: alignment,
          color: color,
          height: height,
          padding: padding,
          margin: margin,
          decoration: decoration,
          foregroundDecoration: foregroundDecoration,
          constraints: constraints,
          children: [Expanded(child: leftWidget), rightWidget1, rightWidget2],
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: mainAxisAlignment,
          crossAxisAlignment: crossAxisAlignment),
      onTap: onTap);
}

Widget myInputText({
  Key? key,
  String initValue = '',
  @required String? hintText,
  TextStyle? inputTextStyle,
  TextStyle? hintTextStyle,
  int minLines = 1,
  int? maxLines,
  bool editable = true,
  TextAlign textAlign = TextAlign.start,
  EdgeInsetsGeometry contentPadding = const EdgeInsets.all(0),
  Color? cursorColor,
  TextInputType? keyboardType,
  List<TextInputFormatter>? inputFormatters,
  TextEditingController? controller,
  @required ValueChanged<String>? inputTextOnChanged,
  FocusNode? focusNode,
}) {
  return TextField(
    focusNode: focusNode,
    key: key,
    keyboardType: keyboardType,
    inputFormatters: inputFormatters,
    cursorColor: cursorColor,
    enabled: editable,
    style: inputTextStyle,
    controller: controller ??
        TextEditingController.fromValue(TextEditingValue(
            text: initValue,
            selection: TextSelection.fromPosition(TextPosition(
                affinity: TextAffinity.downstream,
                offset: (initValue).length)))),
    scrollPadding: EdgeInsets.zero,
    decoration: InputDecoration(
      contentPadding: contentPadding,
      isDense: true,
      border: InputBorder.none,
      hintText: hintText ?? '',
      hintStyle: hintTextStyle,
    ),
    textAlign: textAlign,
    maxLines: maxLines,
    minLines: minLines,
    onChanged: inputTextOnChanged,
  );
}
