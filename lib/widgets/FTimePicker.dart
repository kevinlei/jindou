import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class FTimePicker extends StatefulWidget {
  final DateTime initTime;

  const FTimePicker({Key? key, required this.initTime}) : super(key: key);

  @override
  _FTimePickerState createState() => _FTimePickerState();
}

class _FTimePickerState extends State<FTimePicker> {
  int maxDays = 31;
  Map<String, int> monthDays = {
    "1": 31,
    "2": 28,
    "3": 31,
    "4": 30,
    "5": 31,
    "6": 30,
    "7": 31,
    "8": 31,
    "9": 30,
    "10": 31,
    "11": 30,
    "12": 31
  };

  late FixedExtentScrollController monthCtr;
  late FixedExtentScrollController dayCtr;
  late FixedExtentScrollController hourCtr;
  late FixedExtentScrollController minuteCtr;
  late FixedExtentScrollController secondCtr;

  @override
  void initState() {
    super.initState();
    int initMonth = widget.initTime.month - 1;
    int initDay = widget.initTime.day - 1;
    int initHour = widget.initTime.hour;
    int initMinute = widget.initTime.minute;
    int initSecond = widget.initTime.second;

    monthCtr = FixedExtentScrollController(initialItem: initMonth);
    dayCtr = FixedExtentScrollController(initialItem: initDay);
    hourCtr = FixedExtentScrollController(initialItem: initHour);
    minuteCtr = FixedExtentScrollController(initialItem: initMinute);
    secondCtr = FixedExtentScrollController(initialItem: initSecond);
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      padding: EdgeInsets.only(
          left: 20.w,
          right: 20.w,
          top: 20.w,
          bottom: ScreenUtil().bottomBarHeight + 20.w),
      color: Colors.black,
      child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [FSheetLine(), titleContent(), pickerContent()]),
    );
  }

  @override
  void dispose() {
    monthCtr.dispose();
    dayCtr.dispose();
    hourCtr.dispose();
    minuteCtr.dispose();
    secondCtr.dispose();
    super.dispose();
  }

  Widget titleContent() {
    return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
      const TextButton(onPressed: null, child: SizedBox()),
      FText("时间选择", size: 32.sp, weight: FontWeight.bold),
      TextButton(
          onPressed: onConfirm,
          child: FText("确定", size: 28.sp, color: const Color(0xFFFF753F)))
    ]);
  }

  Widget pickerContent() {
    Widget content = Row(children: [
      PickerWheel(pickerItem(1, 12, "月", monthCtr), onSelect: onMonthChange),
      PickerWheel(pickerItem(1, maxDays, "日", dayCtr)),
      PickerWheel(pickerItem(0, 23, "时", hourCtr)),
      PickerWheel(pickerItem(0, 59, "分", minuteCtr)),
      PickerWheel(pickerItem(0, 59, "秒", secondCtr)),
    ]);
    return SizedBox(height: 360.h, child: content);
  }

  Widget pickerItem(
      int min, int max, String suffix, FixedExtentScrollController controller) {
    BorderSide borderSide =
        BorderSide(color: const Color(0xFFEDEEF3), width: 2.w);
    return CupertinoPicker.builder(
        childCount: max - min + 1,
        itemExtent: 80.h,
        selectionOverlay:
            FContainer(border: Border(top: borderSide, bottom: borderSide)),
        scrollController: controller,
        onSelectedItemChanged: null,
        itemBuilder: (_, index) => timeItem("${index + min}$suffix"));
  }

  Widget timeItem(String text) {
    return Center(
        child: FText(
      text,
      size: 32.sp,
    ));
  }

  void onMonthChange() {
    int month = monthCtr.selectedItem + 1;
    int days = monthDays[month.toString()]!;
    if (maxDays != days) {
      maxDays = days;
      if (!mounted) return;
      setState(() {});
      dayCtr.jumpTo(0);
    }
  }

  void onConfirm() {
    DateTime time = DateTime(
      widget.initTime.year,
      monthCtr.selectedItem + 1,
      dayCtr.selectedItem + 1,
      hourCtr.selectedItem,
      minuteCtr.selectedItem,
      secondCtr.selectedItem,
    );
    WidgetUtils().popPage(time);
  }
}
