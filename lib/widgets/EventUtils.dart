import 'dart:collection';

import '../constants/AppManager.dart';

class EventUtils {
  EventUtils._();
  static final EventUtils _instance = EventUtils._();
  factory EventUtils() => _instance;

  HashMap<String, _EventItem> _mEventItemsMap = HashMap();

  void emit(String stream, var value) {
    _setValue(stream, value);
  }

  void on(String stream, EventCall eventCall) {
    _setCallback(stream, eventCall);
  }

  void off(String stream, EventCall eventCall) {
    _delCallback(stream, eventCall);
  }

  dynamic getValue(String stream) {
    return _getValue(stream);
  }

  void _setValue(String key, var value) {
    _EventItem? item = _mEventItemsMap[key];
    item ??= _EventItem();
    item.value = value;
    _mEventItemsMap[key] = item;
    item.callbacks?.forEach((callback) {
      if (callback.callback != null) callback.callback!(value);
    });
  }

  void _setCallback(String key, EventCall eventCall) {
    if (eventCall.key.isEmpty) return;
    if (eventCall.callback == null) return;
    _EventItem? item = _mEventItemsMap[key];
    item ??= _EventItem();
    List<EventCall>? callbacks = item.callbacks;
    if (callbacks == null) {
      item.callbacks = [];
      _mEventItemsMap[key] = item;
    }
    item.callbacks?.add(eventCall);
  }

  void _delCallback(String key, EventCall eventCall) {
    if (eventCall.key.isEmpty) return;
    if (eventCall.callback == null) return;
    _EventItem? item = _mEventItemsMap[key];
    List<EventCall>? callbacks = item?.callbacks;
    if (callbacks == null) return;
    callbacks.removeWhere((element) => element.key == eventCall.key);
    if (callbacks.isEmpty) {
      _mEventItemsMap.remove(key);
    }
  }

  dynamic _getValue(String key) {
    return _mEventItemsMap[key]?.value;
  }
}

class _EventItem {
  var value;
  List<EventCall>? callbacks;
}

class EventCall {
  /// 监听所在的类名
  late String key;

  /// 监听的回调
  Function(dynamic)? callback;
  EventCall(String k, Function(dynamic)? call) {
    key = k;
    callback = call;
  }
}
