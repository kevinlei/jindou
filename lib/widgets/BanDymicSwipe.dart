import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class BanDymicSwipe extends StatefulWidget {
  final int type;
  const BanDymicSwipe(this.type, {Key? key}) : super(key: key);

  @override
  _BanDymicSwipeState createState() => _BanDymicSwipeState();
}

class _BanDymicSwipeState extends State<BanDymicSwipe> {
  List bannerList = [];

  @override
  void initState() {
    super.initState();
    requestBanner();
  }

  void requestBanner() {
    DioUtils().asyncHttpRequest(
      NetConstants.banner,
      loading: false,
      params: {"type": widget.type},
      onSuccess: onBannerList,
    );
  }

  void onBannerList(response) {
    bannerList.clear();
    bannerList = response ?? [];
    if (!mounted) return;
    setState(() {});
  }


  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: bannerList.isNotEmpty,
      child: FContainer(
        height: 220.h,
        child: Swiper(
          layout: SwiperLayout.CUSTOM,
          customLayoutOption: CustomLayoutOption(startIndex: -1, stateCount: 3)
              .addRotate([0.0, 0.0, 0.0]).addTranslate([
            const Offset(-320.0, 0.0),
            const Offset(0.0, 0.0),
            const Offset(320.0, 0.0)
          ]),
          itemCount: bannerList.length,
          itemWidth: 310.0,
          itemHeight: 140.0,
          scale: 0.6,

          //轮播图之间的间距
          viewportFraction: 0.8,
          onTap: onTapBanner,
          //当前视窗比例，小于1时就会在屏幕内，可以看见旁边的轮播图
          indicatorLayout: PageIndicatorLayout.COLOR,
          //轮播图之间的间距
          autoplay: bannerList.length > 1,
          control: const SwiperControl(
            color: Colors.transparent,
          ),
          itemBuilder: (BuildContext context, int index) {
            return Container(
              //超出部分，可裁剪
              clipBehavior: Clip.hardEdge,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
              ),
              child: Image.network(
                NetConstants.ossPath + bannerList[index]["img_url"],
                fit: BoxFit.fill,
              ),
            );
          },

          pagination: const SwiperPagination(
              builder: RectSwiperPaginationBuilder(
                color: Colors.black54,
                activeColor: Colors.white,
              )),
        ),
      ),
    );
  }

  Widget bannerCell(int index) {
    return FContainer(
      radius: BorderRadius.circular(20.w),
      image: ImageUtils().getImageProvider(
          NetConstants.ossPath + bannerList[index]["img_url"]),
      imageFit: BoxFit.cover,
    );
  }

  void onTapBanner(int index) async {
    String title = bannerList[index]["title"] ?? "";
    String linkImg = bannerList[index]["explain_img_url"] ?? "";
    String linkUrl = bannerList[index]["link_url"] ?? "";

    if (linkUrl.isNotEmpty) {
      showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        isDismissible: false,
        enableDrag: false,
        builder: (_) => RoomGame(url: linkUrl),
      );
    }
    else if (linkImg.isNotEmpty) {
      WidgetUtils().pushPage(BannerPage(title: title, image: linkImg,));
    }
  }
}
