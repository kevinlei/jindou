import 'dart:async';
import 'dart:io';
import 'package:logger/logger.dart' show Level;
import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:path_provider/path_provider.dart';

class AudioFooter extends StatefulWidget {
  final Function(String, int) onSend;

  const AudioFooter({Key? key, required this.onSend}) : super(key: key);

  @override
  _AudioFooterState createState() => _AudioFooterState();
}

class _AudioFooterState extends State<AudioFooter> {
  OverlayEntry? audioMask;

  String recordPath = "", recordTime = "", playTime = "";

  SoundRecordStatus recordStatus = SoundRecordStatus.DEF;
  int maxTime = 10, audioTime = 0;

  @override
  void initState() {
    super.initState();
    initPlayer();
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      color: Colors.transparent,
      padding: EdgeInsets.only(bottom: ScreenUtil().bottomBarHeight),
      align: Alignment.center,
      child: SingleChildScrollView(
        child: Column(mainAxisSize: MainAxisSize.min, children: [
          FText(
              recordStatus == SoundRecordStatus.PLAYING ? playTime : recordTime,
              nullLength: 0,
              size: 36.sp),
          SizedBox(height: 40.h),
          GestureDetector(
              onLongPress: onLongPress,
              onLongPressEnd: onLongPressEnd,
              onLongPressCancel: onLongPressCancel,
              onTap: onTapAudio,
              child: getAudioImg()),
          SizedBox(height: 20.h),
          FText(getAudioTxt(), size: 26.sp, color: const Color(0xFF666666))
        ]),
      ),
    );
  }

  @override
  void dispose() {
    removeMask();
    AppManager().cancelStream();
    disposePlayer();
    super.dispose();
  }

  Widget mask() {
    double top = ScreenUtil().statusBarHeight;
    double bottom = ScreenUtil().bottomBarHeight + 580.h;
    return GestureDetector(
      onTap: () {},
      child: FContainer(
        margin: EdgeInsets.fromLTRB(0, kToolbarHeight + top, 0, bottom),
        color: Colors.white.withOpacity(0),
      ),
    );
  }

  Widget buttonDelete() {
    return GestureDetector(
      onTap: onTapDelete,
      child: LoadAssetImage("dynamic/a2", width: 76.w, height: 48.w),
    );
  }

  Widget buttonConfirm() {
    return GestureDetector(
      onTap: onTapConfirm,
      child: LoadAssetImage("dynamic/a1", width: 76.w, height: 48.w),
    );
  }

  Widget getAudioImg() {
    switch (recordStatus) {
      case SoundRecordStatus.DEF:
        return LoadAssetImage("dynamic/audio1", width: 150.w, height: 150.w);
      case SoundRecordStatus.RECORDING:
        return LoadAssetImage("dynamic/audio4", width: 150.w, height: 150.w);
      case SoundRecordStatus.READY:
        return Row(children: [
          buttonDelete(),
          SizedBox(width: 60.w),
          LoadAssetImage("dynamic/audio2", width: 150.w, height: 150.w),
          SizedBox(width: 60.w),
          buttonConfirm(),
        ], mainAxisAlignment: MainAxisAlignment.center);
      case SoundRecordStatus.PLAYING:
        return LoadAssetImage("dynamic/audio3", width: 150.w, height: 150.w);
    }
  }

  String getAudioTxt() {
    switch (recordStatus) {
      case SoundRecordStatus.DEF:
        return "按住说话";
      case SoundRecordStatus.RECORDING:
        return "松手停止录制";
      case SoundRecordStatus.READY:
        return "点击播放";
      case SoundRecordStatus.PLAYING:
        return "点击停止播放";
    }
  }

  void initPlayer() async {
    await AppManager().soundPlayer.closePlayer();
    await AppManager().soundPlayer.openPlayer();
    await AppManager()
        .soundPlayer
        .setSubscriptionDuration(const Duration(milliseconds: 50));
    await AppManager().soundRecorder.openRecorder();
    await AppManager()
        .soundRecorder
        .setSubscriptionDuration(const Duration(milliseconds: 50));
  }

  void disposePlayer() async {
    try {
      await AppManager().soundPlayer.closePlayer();
      await AppManager().soundRecorder.closeRecorder();
    } catch (err) {
      debugPrint("释放FlutterSound出错：$err");
    }
  }

  void startRecorder() async {
    try {
      Directory tempDir = await getTemporaryDirectory();
      int timeStamp = DateTime.now().millisecondsSinceEpoch ~/ 1000;
      recordPath =
          '${tempDir.path}/audioRecord-$timeStamp${ext[Platform.isAndroid ? Codec.aacADTS.index : Codec.pcm16WAV.index]}';
      debugPrint("录音文件路径：$recordPath");
      await AppManager().soundRecorder.startRecorder(
          toFile: recordPath,
          codec: Platform.isAndroid ? Codec.aacADTS : Codec.pcm16WAV,
          bitRate: 8000,
          numChannels: 1,
          sampleRate: 44100);
      showMask();
      onRecorderListener();
    } on Exception catch (err) {
      debugPrint("录音出错：${err.toString()}");
      stopRecorder();
    }
  }

  void onRecorderListener() {
    AppManager().cancelStream();
    AppManager().recorderStream =
        AppManager().soundRecorder.onProgress!.listen((e) {
      int time = e.duration.inMilliseconds;
      DateTime date = DateTime.fromMillisecondsSinceEpoch(time, isUtc: true);
      recordTime = date.toString().substring(14, 19);
      audioTime = date.second;
      debugPrint("录音时长：${date.toString().substring(14, 19)}");
      recordStatus = SoundRecordStatus.RECORDING;
      if (date.second >= maxTime) {
        return stopRecorder();
      }
      if (!mounted) return;
      setState(() {});
    });
  }

  void stopRecorder() async {

    try {
      await AppManager().soundRecorder.stopRecorder();
    } on Exception catch (err) {
      debugPrint("停止录音错误：$err");
    } finally {
      AppManager().cancelStream();
      recordStatus = SoundRecordStatus.READY;
    }
    if (!mounted) return;
    setState(() {});
  }

  void startPlayer() async {
    try {
      bool exit = await recordPathExit();
      if (!exit) return WidgetUtils().showToast("播放路径出错");
      await AppManager().soundPlayer.startPlayer(
          fromURI: recordPath,
          codec: Platform.isAndroid ? Codec.aacADTS : Codec.pcm16WAV,
          sampleRate: 44100,
          whenFinished: onPlayFinish);
      onPlayerListener();
    } on Exception catch (err) {
      debugPrint("播放录音错误：$err");
      stopPlayer();
    }
  }

  void stopPlayer() async {
    try {
      await AppManager().soundPlayer.stopPlayer();
    } on Exception catch (err) {
      debugPrint("停止播放出错：$err");
    } finally {
      AppManager().cancelStream();
      recordStatus = SoundRecordStatus.READY;
    }
    if (!mounted) return;
    setState(() {});
  }

  void onPlayerListener() {
    AppManager().cancelStream();
    AppManager().playerStream =
        AppManager().soundPlayer.onProgress!.listen((event) {
      int time = event.position.inMilliseconds;
      DateTime date = DateTime.fromMillisecondsSinceEpoch(time, isUtc: true);
      playTime = date.toString().substring(14, 19);
      debugPrint("播放时长：${date.toString().substring(14, 19)}");
      recordStatus = SoundRecordStatus.PLAYING;
      if (!mounted) return;
      setState(() {});
    });
  }

  void onPlayFinish() {
    recordStatus = SoundRecordStatus.READY;
    if (!mounted) return;
    setState(() {});
  }

  void onLongPress() {
    // if (recordStatus != SoundRecordStatus.DEF) return;
    startRecorder();
  }

  void onLongPressEnd(de) {
    switch (recordStatus) {
      case SoundRecordStatus.DEF:
        break;
      case SoundRecordStatus.RECORDING:
        stopRecorder();
        break;
      case SoundRecordStatus.READY:
        break;
      case SoundRecordStatus.PLAYING:
        break;
    }
  }

  void onLongPressCancel() {
    switch (recordStatus) {
      case SoundRecordStatus.DEF:
        break;
      case SoundRecordStatus.RECORDING:
        stopRecorder();
        removeMask();
        break;
      case SoundRecordStatus.READY:
        break;
      case SoundRecordStatus.PLAYING:
        break;
    }
  }

  void onTapAudio() {
    switch (recordStatus) {
      case SoundRecordStatus.DEF:
        break;
      case SoundRecordStatus.RECORDING:
        stopRecorder();
        break;
      case SoundRecordStatus.READY:
        startPlayer();
        break;
      case SoundRecordStatus.PLAYING:
        stopPlayer();
        break;
    }
  }

  void onTapDelete() {
    recordStatus = SoundRecordStatus.DEF;
    recordTime = "";
    recordPath = "";
    audioTime = 0;
    removeMask();
    if (!mounted) return;
    setState(() {});
  }

  void onTapConfirm() async {
    bool exit = await recordPathExit();
    if (!exit) {
      return WidgetUtils().showToast("录音路径错误");
    }
    if (audioTime > maxTime) return WidgetUtils().showToast("录音时间过长");
    widget.onSend(recordPath, audioTime);
    onTapDelete();
  }

  void showMask() {
    if (audioMask != null) return;
    audioMask = OverlayEntry(builder: (_) => mask());
    Overlay.of(context)?.insert(audioMask!);
  }

  void removeMask() {
    audioMask?.remove();
    audioMask = null;
  }

  Future<bool> recordPathExit() async {
    if (recordPath.isEmpty) return false;
    bool exit = await File(recordPath).exists();
    return exit;
  }
}
