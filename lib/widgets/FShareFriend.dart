import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class FShareFriend extends StatefulWidget {
  final String? btnText;

  const FShareFriend({Key? key, this.btnText}) : super(key: key);

  @override
  _FShareFriendState createState() => _FShareFriendState();
}

class _FShareFriendState extends State<FShareFriend>
    with SingleTickerProviderStateMixin {
  late TabController tabCtr;

  bool isInputting = false;
  bool isSearching = false;
  String inputText = "";

  @override
  void initState() {
    super.initState();
    tabCtr = TabController(length: 3, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    EdgeInsets insets = MediaQuery.of(context).viewInsets;
    return FContainer(
      padding: EdgeInsets.fromLTRB(
          25.w, 20.h, 20.w, 20.h + ScreenUtil().bottomBarHeight),
      child: SingleChildScrollView(
        child: Column(mainAxisSize: MainAxisSize.min, children: [
          FSheetLine(),
          friendTable(),
          SizedBox(height: 30.h),
          searchContent(),
          SizedBox(height: 30.h),
          SizedBox(
              height: (insets.bottom + 100.h) > 600.h
                  ? (insets.bottom + 100.h)
                  : 600.h,
              child: listContent())
        ]),
      ),
    );
  }

  @override
  void dispose() {
    tabCtr.dispose();
    super.dispose();
  }

  Widget friendTable() {
    return TabBar(
      labelStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 32.sp),
      labelColor: Colors.black,
      unselectedLabelStyle:
          TextStyle(fontWeight: FontWeight.normal, fontSize: 32.sp),
      unselectedLabelColor: const Color(0xFF999999),
      tabs: const [
        Text("好友", style: TextStyle(color: Colors.white), textScaleFactor: 1.0),
        Text("关注", style: TextStyle(color: Colors.white), textScaleFactor: 1.0),
        Text("粉丝", style: TextStyle(color: Colors.white), textScaleFactor: 1.0),
      ],
      controller: tabCtr,
      indicatorColor: const Color(0xFFA852FF),
      indicatorSize: TabBarIndicatorSize.label,
    );
  }

  Widget searchContent() {
    return Row(children: [
      Expanded(child: textField()),
      Visibility(visible: isInputting, child: buttonSearch())
    ]);
  }

  Widget textField() {
    InputBorder border = const OutlineInputBorder(
        borderSide: BorderSide(color: Colors.transparent));
    return FContainer(
      radius: BorderRadius.circular(30.w),
      height: 60.w,
      color: const Color(0xFFF6F7FA),
      padding: EdgeInsets.symmetric(horizontal: 20.w),
      child: TextField(
        onChanged: onTextInput,
        style: TextStyle(color: Colors.black, fontSize: 24.sp),
        decoration: InputDecoration(
          enabledBorder: border,
          focusedBorder: border,
          contentPadding: EdgeInsets.zero,
          hintStyle: TextStyle(color: const Color(0xFFBBBBBB), fontSize: 24.sp),
          hintText: "输入用户昵称或ID",
        ),
      ),
    );
  }

  Widget buttonSearch() {
    Widget btn = FContainer(
      height: 60.w,
      radius: BorderRadius.circular(60.w),
      width: 140.w,
      color: const Color(0xFFA852FF),
      align: Alignment.center,
      margin: EdgeInsets.only(left: 20.w),
      child: FText("搜索", size: 28.sp),
    );
    return GestureDetector(onTap: onTapSearch, child: btn);
  }

  Widget listContent() {
    if (isSearching) {
      return _FriendList(searchText: inputText, btnText: widget.btnText);
    }
    return TabBarView(controller: tabCtr, children: [
      _FriendList(listType: CountListType.FRIENDS, btnText: widget.btnText),
      _FriendList(listType: CountListType.FOLLOW, btnText: widget.btnText),
      _FriendList(listType: CountListType.FANS, btnText: widget.btnText),
    ]);
  }

  void onTextInput(String value) {
    inputText = value;
    if ((isInputting && value.isEmpty) || (!isInputting && value.isNotEmpty)) {
      isInputting = value.isNotEmpty;
      if (value.isEmpty) {
        isSearching = false;
      }
      if (!mounted) return;
      setState(() {});
    }
  }

  void onTapSearch() {
    FocusManager.instance.primaryFocus?.unfocus();
    isSearching = true;
    if (!mounted) return;
    setState(() {});
  }
}

class _FriendList extends StatefulWidget {
  final String? btnText;
  final CountListType? listType;
  final String? searchText;

  const _FriendList({Key? key, this.listType, this.btnText, this.searchText})
      : super(key: key);

  @override
  __FriendListState createState() => __FriendListState();
}

class __FriendListState extends State<_FriendList> {
  int curPage = 1;
  List userList = [];

  @override
  void initState() {
    super.initState();
    callRefresh();
  }

  @override
  Widget build(BuildContext context) {
    return FRefreshLayout(
      onRefresh: callRefresh,
      onLoad: callLoad,
      firstRefresh: false,
      emptyWidget: userList.isEmpty ? const FEmptyWidget() : null,
      child: ListView.builder(
        itemBuilder: (_, index) => userCell(userList[index]),
        itemCount: userList.length,
        itemExtent: 120.w,
      ),
    );
  }

  Widget userCell(data) {
    return FContainer(
      border: const Border(bottom: BorderSide(color: Color(0xFFEEEEEE))),
      child: Row(children: [
        CircleAvatar(
          radius: 40.w,
          backgroundImage: ImageUtils()
              .getImageProvider(NetConstants.ossPath + data["headImageUrl"]),
          backgroundColor: const Color(0xFFBBBBBB),
        ),
        SizedBox(width: 20.w),
        Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(children: [
                ConstrainedBox(
                  constraints: BoxConstraints(maxWidth: 260.w),
                  child: FText(data["nickname"]),
                ),
                userAge(data["sex"], data["birthday"] ?? 0)
              ]),
              FText("ID：${data["displayId"]}",
                  size: 24.sp, color: const Color(0xFF999999))
            ]),
        const Spacer(),
        buttonGive(data)
      ]),
    );
  }

  Widget userAge(int sex, int bir) {
    if (bir == 0) return const SizedBox();
    return FContainer(
      width: 70.w,
      height: 30.w,
      margin: EdgeInsets.only(left: 10.w),
      padding: EdgeInsets.symmetric(horizontal: 10.w),
      color: sex == 0 ? const Color(0xFFFFE5E5) : const Color(0xFFE5EFFF),
      radius: BorderRadius.circular(30.w),
      child: Row(children: [
        LoadAssetImage(sex == 0 ? "person/sex2" : "person/sex1",
            width: 20.w, height: 20.w),
        FText("${UserConstants.getUserAge(bir)}",
            size: 24.sp,
            color: sex == 0 ? const Color(0xFFFF617F) : const Color(0xFF43A0FF))
      ]),
    );
  }

  Widget buttonGive(data) {
    if (data["uId"] == UserConstants.userginInfo?.info.userId)
      return const SizedBox();
    Widget btn = FContainer(
      width: 125.w,
      height: 50.w,
      align: Alignment.center,
      color: const Color(0xFFA852FF),
      radius: BorderRadius.circular(50.w),
      child: FText(widget.btnText ?? "分享", size: 26.sp),
    );
    return GestureDetector(onTap: () => onTapSend(data), child: btn);
  }

  void onTapSend(data) {
    WidgetUtils().popPage({
      "conId": data["hxId"],
      "user": data["uId"],
      "head": data["headImageUrl"],
      "name": data["nickname"],
    });
  }

  Future<void> callRefresh() async {
    curPage = 1;
    if (widget.searchText != null) {
      requestSearch(true);
    } else {
      requestList(true);
    }
  }

  Future<void> callLoad() async {
    curPage += 1;
    if (widget.searchText != null) {
      requestSearch(false);
    } else {
      requestList(false);
    }
  }

  void requestList(bool isRefresh) {
    String netUrl = "";
    DioMethod method = DioMethod.GET;
    switch (widget.listType) {
      case CountListType.FOLLOW:
        netUrl = NetConstants.followList;
        break;
      case CountListType.FANS:
        netUrl = NetConstants.fansList;
        method = DioMethod.POST;
        break;
      case CountListType.FRIENDS:
        netUrl = NetConstants.friendList;
        break;
      default:
    }
    if (netUrl.isEmpty) return;
    DioUtils().asyncHttpRequest(
      netUrl,
      method: method,
      params: {"PageIndex": curPage, "PageSize": AppConstants.pageSize},
      onSuccess: isRefresh ? onRefresh : onLoad,
    );
  }

  void requestSearch(bool isRefresh) {
    DioUtils().asyncHttpRequest(
      NetConstants.searchUser,
      params: {
        "key": widget.searchText?.trimRight(),
        "pageIndex": curPage,
        "pageSize": AppConstants.pageSize
      },
      onSuccess: isRefresh ? onRefresh : onLoad,
    );
  }

  void onRefresh(response) {
    userList.clear();
    userList = response?["data"] ?? [];
    if (!mounted) return;
    setState(() {});
  }

  void onLoad(response) {
    userList.addAll(response?["data"] ?? []);
    if (!mounted) return;
    setState(() {});
  }
}
