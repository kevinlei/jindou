import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class BannerSwipe extends StatefulWidget {//房间顶部广告
  final int type;
  const BannerSwipe(this.type, {Key? key}) : super(key: key);

  @override
  _BannerSwipeState createState() => _BannerSwipeState();
}

class _BannerSwipeState extends State<BannerSwipe> {
  List bannerList = [];

  @override
  void initState() {
    super.initState();
    requestBanner();
  }

  void requestBanner() {
    DioUtils().asyncHttpRequest(
      NetConstants.banner,
      loading: false,
      params: {"type": widget.type},
      onSuccess: onBannerList,
    );
  }

  void onBannerList(response) {
    bannerList.clear();
    bannerList = response ?? [];
    if (!mounted) return;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: bannerList.isNotEmpty,
      child: FContainer(
        height: 180.h,
        child: Swiper(
          pagination: SwiperPagination(
              alignment: Alignment.bottomRight,
              margin: EdgeInsets.symmetric(vertical: 0, horizontal: 15.w),
              builder: DotSwiperPaginationBuilder(
                color: Colors.white.withOpacity(0.4),
                activeColor: const Color(0xFFFF6E60),
                size: 6,
                activeSize: 6,
                space: 2,
              )),
          itemBuilder: (_, index) => bannerCell(index),
          itemCount: bannerList.length,
          autoplay: bannerList.length > 1,
          onTap: onTapBanner,
        ),
      ),
    );
  }

  Widget bannerCell(int index) {
    return FContainer(
      radius: BorderRadius.circular(20.w),
      image: ImageUtils().getImageProvider(
          NetConstants.ossPath + bannerList[index]["img_url"]),
      imageFit: BoxFit.cover,
    );
  }

  void onTapBanner(int index) async {
    String title = bannerList[index]["title"] ?? "";
    String linkImg = bannerList[index]["explain_img_url"] ?? "";
    String linkUrl = bannerList[index]["link_url"] ?? "";
    if (linkUrl.isNotEmpty) {
      showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        isDismissible: false,
        enableDrag: false,
        builder: (_) => RoomGame(url: linkUrl),
      );
    } else if (linkImg.isNotEmpty) {
      WidgetUtils().pushPage(BannerPage(title: title, image: linkImg));
    }
  }
}
