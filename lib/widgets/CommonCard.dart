// TODO Implement this library.import 'package:echo/import.dart';
import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class CommonCard extends StatelessWidget {
  const CommonCard({Key? key, this.child, this.padding}) : super(key: key);

  final Widget? child;

  final EdgeInsets? padding;

  @override
  Widget build(BuildContext context) {
    return FContainer(
      padding: padding ?? EdgeInsets.only(top: ScreenUtil().statusBarHeight),
      image: ImageUtils().getAssetsImage("public/huibg"),
      imageFit: BoxFit.fitWidth,
      imageAlign: Alignment.topCenter,
      child: child,
    );
  }
}
