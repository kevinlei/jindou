import 'dart:convert';

import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class ReportType extends StatefulWidget {
  final String userId;
  final bool isRoom;
  const ReportType(this.userId, {super.key, this.isRoom = false});

  @override
  State<ReportType> createState() => _ReportTypeState();
}

class _ReportTypeState extends State<ReportType> {
  List types = [];

  @override
  void initState() {
    super.initState();
    if (!mounted) return;
    initTypes();
  }

  void initTypes() async {
    await DioUtils().httpRequest(
      NetConstants.config,
      params: {"conf_key": "inform_user"},
      loading: false,
      onSuccess: (res) {
        if (res == null) return;
        var data = jsonDecode(res);
        types = data;
        setState(() {});
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      padding: EdgeInsets.only(left: 30.w, right: 30.w, bottom: 20.w),
      imageFit: BoxFit.fill,
      imageAlign: Alignment.topCenter,
      image: ImageUtils().getAssetsImage("public/huibg"),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: const CommonAppBar(
          title: "举报理由",
          textColor: Colors.black,
        ),
        body: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(height: 40.w),
            ...List.generate(types.length, (index) => reportItem(types[index]))
          ],
        ),
      ),
    );
  }

  Widget reportItem(data) {
    Widget child = FContainer(
      padding: EdgeInsets.symmetric(vertical: 15.w),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          FText(
            data["name"],
            size: 34.sp,
            color: Colors.black,
          ),
          LoadAssetImage("public/RightNavigation", width: 20.w),
        ],
      ),
    );
    return GestureDetector(
      onTap: () {
        WidgetUtils().pushPage(FeedsetbackPage(
            data["name"], "详细描述，有利帮助管理员进行判定", data["type"],
            setBack: (d, p, i) => report(d, p, i, data["type"])));
      },
      child: child,
    );
  }

  // 举报用户
  void report(
      String describe, String phone, List<String> imgs, int type) async {
    await DioUtils().httpRequest(
      NetConstants.msgReport,
      method: DioMethod.POST,
      params: {
        "user_id": widget.userId,
        "type": type,
        "phone": phone,
        "reason": describe,
        "img_info": imgs,
        "sub_category": widget.isRoom ? 3 : 1,
      },
      onSuccess: (data) async {
        WidgetUtils().popPage();
        WidgetUtils().popPage();
      },
    );
  }
}
