import 'dart:io';

import 'package:echo/import.dart';
import 'package:flutter_native_image/flutter_native_image.dart';
import 'package:permission_handler/permission_handler.dart';

class FuncUtils {
  static final FuncUtils _instance = FuncUtils._();
  factory FuncUtils() => _instance;
  FuncUtils._();

  /// 手机号
  final String regexMobile =
      '^((13[0-9])|(14[579])|(15[0-35-9])|(16[2567])|(17[01235-8])|(18[0-9])|(19[13589]))\\d{8}\$';

  /// 验证码
  final String regexPhoneCode = '\\d{6}\$';

  /// 邮件
  final String regexEmail =
      '^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*\$';

  /// 必须包含字母和数字
  final String regexPwd = '^(?![0-9]+\$)(?![a-zA-Z]+\$)[0-9A-Za-z]{6,15}\$';

  bool isMobile(String input) {
    return _matches(regexMobile, input);
  }
///保留2位小数点处理
  formatNum(double? nums, int postion) {
    double num;
    if (nums is double) {
      num = nums;
    } else {
      num = double.parse(nums.toString());
    }
    if ((num.toString().length -
        num.toString().lastIndexOf(".") -
        1) <
        postion) {
      return (num.toStringAsFixed(postion)
          .substring(0,
          num.toString().lastIndexOf(".") + postion + 1)
          .toString());
    } else {
      return (num.toString()
          .substring(0,
          num.toString().lastIndexOf(".") + postion + 1)
          .toString());
    }

  }

  bool isMobileCode(String input) {
    return _matches(regexPhoneCode, input);
  }

  bool isIdCard(String cardId) {
    if (cardId.length != 18) {
      return false;
    }
    RegExp postalCode = RegExp(
        r'^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|[Xx])$');
    if (!postalCode.hasMatch(cardId)) {
      return false;
    }
    final List idCardList = [
      "7",
      "9",
      "10",
      "5",
      "8",
      "4",
      "2",
      "1",
      "6",
      "3",
      "7",
      "9",
      "10",
      "5",
      "8",
      "4",
      "2"
    ];
    final List idCardYArray = [
      '1',
      '0',
      '10',
      '9',
      '8',
      '7',
      '6',
      '5',
      '4',
      '3',
      '2'
    ];
    int idCardWiSum = 0;

    for (int i = 0; i < 17; i++) {
      int subStrIndex = int.parse(cardId.substring(i, i + 1));
      int idCardWiIndex = int.parse(idCardList[i]);
      idCardWiSum += subStrIndex * idCardWiIndex;
    }
    int idCardMod = idCardWiSum % 11;
    String idCardLast = cardId.substring(17, 18);
    if (idCardMod == 2) {
      if (idCardLast != 'x' && idCardLast != 'X') {
        return false;
      }
    } else {
      if (idCardLast != idCardYArray[idCardMod]) {
        return false;
      }
    }
    return true;
  }

  bool _matches(String regex, String input) {
    if (input.isEmpty) return false;
    return RegExp(regex).hasMatch(input);
  }

  // 上传图片/视频/音频
  Future<String?> requestUpload(String imagePath, UploadType ossType) async {
    if (imagePath.isEmpty) return "";
    if (!checkFileSuffix(imagePath)) {
      WidgetUtils().showToast("文件格式不支持");
      return null;
    }
    File compressedFile = await FlutterNativeImage.compressImage(
      imagePath,
      quality: 70,
    );
    MultipartFile multipartFile =
        await MultipartFile.fromFile(compressedFile.path);
    FormData params =
        FormData.fromMap({"file": multipartFile, "oss_type": ossType.value});
    String image = "";
    await DioUtils().httpRequest(
      NetConstants.uploadFile,
      method: DioMethod.POST,
      loading: false,
      contentType: "application/x-www-form-urlencoded",
      params: params,
      onSuccess: (response) => image = response["file_path"],
    );
    return image;
  }

  // 上传音频
  Future<String?> requestUploadAudio(
      String audioPath, UploadType ossType) async {
    if (audioPath.isEmpty) return "";
    if (!checkFileSuffix(audioPath)) {
      WidgetUtils().showToast("文件格式不支持");
      return null;
    }
    MultipartFile multipartFile = await MultipartFile.fromFile(audioPath);
    FormData params =
        FormData.fromMap({"file": multipartFile, "oss_type": ossType.value});
    String audio = "";
    await DioUtils().httpRequest(
      NetConstants.uploadFile,
      method: DioMethod.POST,
      loading: false,
      contentType: "application/x-www-form-urlencoded",
      params: params,
      onSuccess: (response) => audio = response["file_path"],
    );
    return audio;
  }

  bool checkFileSuffix(String path) {
    List suffixList = [
      "jpg",
      "jpeg",
      "png",
      "webp",
      "gif",
      "wav",
      "tiff",
      "tif",
      "heif",
      "mp3",
      "aac",
      "pcm",
      "mp4",
      "avi",
      "mov",
      "wmv",
      "rmvb",
      "mkv",
    ];
    for (String suffix in suffixList) {
      if (path.toLowerCase().endsWith(suffix)) return true;
    }
    return false;
  }

  // 检测文本是否合法
  Future<bool> checkTextLawful(String content, CheckTxtType type) async {
    bool isLawful = true;
    await DioUtils().httpRequest(
      NetConstants.checkText,
      method: DioMethod.POST,
      loading: false,
      params: {
        "content": content,
        "event_id": type.value,
        "user_id": UserConstants.userginInfo!.info.userId,
      },
      onSuccess: (response) {
        if (response["risk_level"] == "REJECT") {
          isLawful = false;
          WidgetUtils().showToast(response["description"]);
        }
      },
      onError: (_, __) => isLawful = false,
    );
    return isLawful;
  }

  // 检测图片是否合法
  Future<bool> checkImageLawful(String content, CheckImgType type,
      {bool isReg = true,
      String? receiveUserId,
      String? guildId,
      int? roomId}) async {
    bool isLawful = true;

    await DioUtils().httpRequest(
      NetConstants.checkImage,
      method: DioMethod.POST,
      loading: false,
      params: {
        "img_url": NetConstants.ossPath + content,
        "event_id": type.value,
        "user_id": receiveUserId ?? UserConstants.userginInfo!.info.userId,
      },
      onSuccess: (response) {
        if (response["risk_level"] == "REJECT") {
          isLawful = false;
          WidgetUtils().showToast(response["description"]);
        }
      },
      onError: (_, __) => isLawful = false,
    );
    return isLawful;
  }

  // 检测权限
  Future<bool> requestPermission(Permission permission) async {
    // 相机
    if (permission == Permission.camera) {
      bool granted = await Permission.camera.isGranted;
      if (granted) return true;
      WidgetUtils()
          .showNotification("相机权限使用说明", "用于拍摄一段视频或一张照片，发布您的动态或者上传到您的个人主页");
      PermissionStatus state = await Permission.camera.request();
      bool accept = state == PermissionStatus.granted;
      if (!accept) {
        bool? open = await WidgetUtils().showAlert("如果需要打开摄像机，可以前往设置打开权限");
        if (open != null && open) openAppSettings();
      }
      return accept;
    } else if (permission == Permission.locationWhenInUse) {
      PermissionStatus state = await Permission.locationWhenInUse.status;
      if (state == PermissionStatus.granted) return true;
      WidgetUtils().showNotification("位置信息使用说明", "访问位置信息是为了更好的向你提供附近交友和动态推荐");
      state = await Permission.locationWhenInUse.request();
      bool accept = state == PermissionStatus.granted;
      if (!accept) {
        bool? open = await WidgetUtils().showAlert("如果需要定位信息，可以前往设置打开权限");
        if (open != null && open) openAppSettings();
      }
      return accept;
    } else if (permission == Permission.microphone) {
      PermissionStatus state = await Permission.microphone.status;
      if (state == PermissionStatus.granted) return true;
      WidgetUtils().showNotification("麦克风权限使用说明", "用于房间语音或录制音频上传到您的个人主页");
      state = await Permission.microphone.request();
      bool accept = state == PermissionStatus.granted;
      if (!accept) {
        bool? open = await WidgetUtils().showAlert("如果需要麦克风，可以前往设置打开权限");
        if (open != null && open) openAppSettings();
      }
      return accept;
    } else if (permission == Permission.photos) {
      if (Platform.isAndroid) {
        // 安卓为适配photo_manager插件, photo权限改为storage权限
        bool granted = await Permission.storage.isGranted;
        if (!granted) granted = await Permission.photos.isGranted;
        if (granted) return true;
        WidgetUtils()
            .showNotification("存储读写权限使用说明", "用于读取手机的照片或视频，发布您的动态或者上传到您的个人主页");
        PermissionStatus state = await Permission.storage.request();
        bool accept = state == PermissionStatus.granted;
        if (!accept) {
          bool? open = await WidgetUtils().showAlert("如果需要打开存储读写权限，可以前往设置打开权限");
          if (open != null && open) {
            await openAppSettings();
            accept = true;
          }
        }
        return accept;
      } else {
        PermissionStatus state = await Permission.photos.status;
        if (state == PermissionStatus.granted ||
            state == PermissionStatus.limited) return true;
        WidgetUtils()
            .showNotification("相册权限使用说明", "用于选取照片或视频，发布您的动态或者上传到您的个人主页");
        state = await Permission.photos.request();
        bool accept = state == PermissionStatus.granted ||
            state == PermissionStatus.limited;
        if (!accept) {
          bool? open = await WidgetUtils().showAlert("如果需要打开相册，可以前往设置打开权限");
          if (open != null && open) openAppSettings();
        }
        return accept;
      }
    } else if (permission == Permission.notification) {
    } else if (permission == Permission.requestInstallPackages) {
      bool granted = await Permission.requestInstallPackages.isGranted;
      if (granted) return true;
      WidgetUtils().showNotification("安装软件权限使用说明", "用于更新升级该软件");
      PermissionStatus state =
          await Permission.requestInstallPackages.request();
      bool accept = state == PermissionStatus.granted;
      if (!accept) {
        bool? open = await WidgetUtils().showAlert("如果需要安装软件权限，可以前往设置打开权限");
        if (open != null && open) openAppSettings();
      }
      return accept;
    } else if (permission == Permission.storage) {
      bool granted = await Permission.storage.isGranted;
      if (granted) return true;
      WidgetUtils()
          .showNotification("存储读写权限使用说明", "用于读取手机的照片或视频，发布您的动态或者上传到您的个人主页");
      PermissionStatus state = await Permission.storage.request();
      bool accept = state == PermissionStatus.granted;
      if (!accept) {
        bool? open = await WidgetUtils().showAlert("如果需要打开存储读写权限，可以前往设置打开权限");
        if (open != null && open) openAppSettings();
      }
      return accept;
    }
    return false;
  }
}
