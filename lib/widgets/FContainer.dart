import 'package:echo/import.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FContainer extends StatelessWidget {
  final Color? color;
  final Color? foreColor;
  final double? width;
  final double? height;
  final BorderRadius? radius;
  final BorderRadius? foreRadius;
  final Border? border;
  final Border? foreBorder;
  final EdgeInsets? padding;
  final BoxConstraints? constraints;
  final EdgeInsets? margin;
  final ImageProvider? image;
  final ImageProvider? foreImage;
  final BoxFit? imageFit;
  final BoxFit? foreImageFit;
  final Alignment? align;
  final Alignment? imageAlign;
  final Alignment? foreImageAlign;
  final double? imageOpacity;
  final double? foreImageOpacity;
  final Rect? imageSlice;
  final Rect? foreImageSlice;
  final double? imgScale;
  final double? foreImgScale;
  final Widget? child;
  final List<Color>? gradient;
  final List<Color>? foreGradient;
  final Alignment? gradientBegin;
  final Alignment? foreGradientBegin;
  final Alignment? gradientEnd;
  final Alignment? foreGradientEnd;
  final List<double>? stops;
  final List<double>? foreStops;
  final List<BoxShadow>? shadow;
  final List<BoxShadow>? foreShadow;
  final Decoration? decoration;
  const FContainer({
    Key? key,
    this.color,
    this.foreColor,
    this.width,
    this.height,
    this.radius,
    this.foreRadius,
    this.border,
    this.foreBorder,
    this.padding,
    this.constraints,
    this.margin,
    this.image,
    this.foreImage,
    this.imageFit,
    this.foreImageFit,
    this.imageAlign,
    this.foreImageAlign,
    this.imageOpacity,
    this.foreImageOpacity,
    this.imageSlice,
    this.foreImageSlice,
    this.imgScale,
    this.foreImgScale,
    this.align,
    this.child,
    this.gradient,
    this.foreGradient,
    this.gradientBegin,
    this.foreGradientBegin,
    this.gradientEnd,
    this.foreGradientEnd,
    this.stops,
    this.foreStops,
    this.shadow,
    this.foreShadow,
    this.decoration,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    DecorationImage? backImg, foreImg;
    if (image != null) {
      backImg = DecorationImage(
          image: image!,
          scale: imgScale ?? 1.0,
          opacity: imageOpacity ?? 1.0,
          centerSlice: imageSlice,
          fit: imageFit,
          alignment: imageAlign ?? Alignment.center);
    }
    if (foreImage != null) {
      foreImg = DecorationImage(
          image: foreImage!,
          scale: foreImgScale ?? 1.0,
          opacity: foreImageOpacity ?? 1.0,
          centerSlice: foreImageSlice,
          fit: foreImageFit,
          alignment: foreImageAlign ?? Alignment.center);
    }

    return Container(
      width: width,
      height: height,
      margin: margin,
      padding: padding,
      constraints: constraints,
      alignment: align,
      foregroundDecoration: BoxDecoration(
        color: foreColor,
        image: foreImg,
        borderRadius: foreRadius,
        border: foreBorder,
        boxShadow: foreShadow,
        gradient: (foreGradient == null || foreGradient!.isEmpty)
            ? null
            : LinearGradient(
                colors: foreGradient!,
                begin: foreGradientBegin ?? Alignment.centerLeft,
                end: foreGradientEnd ?? Alignment.centerRight,
                stops: foreStops,
              ),
      ),
      decoration: decoration ??
          BoxDecoration(
            color: color,
            image: backImg,
            borderRadius: radius,
            border: border,
            boxShadow: shadow,
            gradient: (gradient == null || gradient!.isEmpty)
                ? null
                : LinearGradient(
                    colors: gradient!,
                    begin: gradientBegin ?? Alignment.topCenter,
                    end: gradientEnd ?? Alignment.bottomCenter,
                    stops: stops,
                  ),
          ),
      child: child,
    );
  }
}

class FsetButton extends StatefulWidget {
  final String text;

  /// 普通状态
  final Function()? onPress;

  /// 带有网络请求状态
  final Future Function()? onRequest;

  final double? padding;

  final double? radius;

  final Color? bgColor;

  final double? textSize;

  final Color? txtColor;

  final Color? borderColor;

  final FontWeight? fontWeight;

  const FsetButton(this.text,
      {Key? key,
      this.onPress,
      this.padding,
      this.onRequest,
      this.radius,
      this.bgColor,
      this.textSize,
      this.txtColor,
      this.borderColor,
      this.fontWeight})
      : super(key: key);

  @override
  _FsetButtonState createState() => _FsetButtonState();
}

class _FsetButtonState extends State<FsetButton> {
  bool isRequest = false;

  @override
  Widget build(BuildContext context) {
    List<Widget> children = [
      FText(
        widget.text,
        size: widget.textSize ?? 32.sp,
        weight: widget.fontWeight ?? FontWeight.bold,
        color: widget.txtColor ?? Colors.black,
      )
    ];
    if (isRequest) children.insert(0, CupertinoActivityIndicator(radius: 20.w));
    return GestureDetector(
      onTap: onPress,
      child: FContainer(
        border: Border.all(
            color: widget.borderColor ?? Colors.transparent,
            width: widget.borderColor == null ? 0 : 1),
        height: 90.w,
        color: widget.bgColor ?? Color(0xFFFF753F),
        radius: BorderRadius.circular(widget.radius ?? 30.w),
        margin: EdgeInsets.symmetric(
            horizontal: widget.padding ?? 25.w, vertical: 20.h),
        align: Alignment.center,
        child: Row(
            children: children, mainAxisAlignment: MainAxisAlignment.center),
      ),
    );
  }

  void onPress() async {
    if (isRequest) return;
    if (widget.onPress != null) {
      widget.onPress!();
    } else if (widget.onRequest != null) {
      isRequest = true;
      if (!mounted) return;
      setState(() {});
      await widget.onRequest!();
      isRequest = false;
      if (!mounted) return;
      setState(() {});
    }
  }
}

class FButton extends StatelessWidget {
  final Widget? child;
  final double? width;
  final double? height;
  final Color? bgColor;
  final EdgeInsets? padding;
  final BorderRadius? radius;
  final List<Color>? gradient;
  final ImageProvider? image;
  final String? text;
  final void Function()? onTap;
  final bool isTheme;
  final Alignment? align;
  final Border? border;
  const FButton({
    super.key,
    this.child,
    this.width,
    this.height,
    this.bgColor,
    this.padding,
    this.radius,
    this.gradient,
    this.image,
    this.text,
    this.onTap,
    this.isTheme = false,
    this.align,
    this.border,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: FContainer(
        color: (gradient != null || image != null) ? null : bgColor,
        gradient: image != null
            ? null
            : isTheme
                ? const [
                    Color(0xFFFF753F),
                    Color(0xFFFF753F),
                    Color(0xFFFF753F),
                  ]
                : gradient,
        gradientBegin: isTheme ? Alignment.topLeft : Alignment.centerLeft,
        gradientEnd: isTheme ? Alignment.bottomRight : Alignment.centerRight,
        stops: isTheme ? const [0, 0.4, 0.8] : null,
        width: width,
        height: height,
        imageFit: BoxFit.cover,
        imageAlign: Alignment.center,
        image: image,
        align: align,
        padding:
            padding ?? EdgeInsets.symmetric(vertical: 10.w, horizontal: 20.w),
        radius: radius ?? BorderRadius.circular(50),
        border: border,
        child: child ?? FText(text ?? ""),
      ),
    );
  }
}

class FBtnOpen extends StatefulWidget {
  final bool initOpen;
  final void Function() onTap;
  final double? width;
  final double? height;
  const FBtnOpen(this.onTap,
      {super.key, this.initOpen = false, this.width, this.height});

  @override
  State<FBtnOpen> createState() => _FBtnOpenState();
}

class _FBtnOpenState extends State<FBtnOpen> {
  bool open = false;

  @override
  void initState() {
    super.initState();
    if (!mounted) return;
    setState(() {
      open = widget.initOpen;
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.onTap();
        setState(() {
          open = !open;
        });
      },
      child: LoadAssetImage(
        open ? "person/on3" : "person/off3",
        width: widget.width ?? 90.w,
        height: widget.height,
      ),
    );
  }
}
