/*
 * <p> 字体加粗样式</p>
 */
import 'dart:ui';

class MyFontWeight {
  MyFontWeight._();

  //正常
  static const FontWeight normal = FontWeight.w400;

  //中粗
  static const FontWeight medium = FontWeight.w700;

  //加粗
  static const FontWeight bold = FontWeight.w800;
}
