import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class FRefreshLayout extends StatelessWidget {
  final Widget? child;
  final EasyRefreshChildBuilder? builder;
  final Widget? firstWidget;
  final bool firstRefresh;
  final Widget? emptyWidget;
  final List<Widget>? slivers;
  final Axis direction;
  final bool reverse;
  final bool shrinkWrap;
  final int headerIndex;
  final ScrollController? scroll;
  final EasyRefreshController? controller;

  final Future<void> Function()? onRefresh;
  final Future<void> Function()? onLoad;

  const FRefreshLayout(
      {Key? key,
      this.child,
      this.onRefresh,
      this.onLoad,
      this.firstRefresh = true,
      this.firstWidget,
      this.controller,
      this.headerIndex = 0,
      this.emptyWidget,
      this.scroll})
      : this.builder = null,
        this.slivers = null,
        this.reverse = false,
        this.shrinkWrap = false,
        this.direction = Axis.vertical,
        super(key: key);

  const FRefreshLayout.builder(
      {Key? key,
      this.controller,
      this.builder,
      this.onRefresh,
      this.onLoad,
      this.scroll,
      this.firstRefresh = false})
      : this.child = null,
        this.slivers = null,
        this.reverse = false,
        this.shrinkWrap = false,
        this.firstWidget = null,
        this.emptyWidget = null,
        this.headerIndex = 0,
        this.direction = Axis.vertical,
        super(key: key);

  const FRefreshLayout.custom({
    Key? key,
    this.controller,
    this.slivers,
    this.onRefresh,
    this.onLoad,
    this.firstRefresh = true,
    this.firstWidget,
    this.emptyWidget,
    this.reverse = false,
    this.shrinkWrap = false,
    this.headerIndex = 0,
    this.scroll,
    this.direction = Axis.vertical,
  })  : this.child = null,
        this.builder = null,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    if (slivers != null) {
      return EasyRefresh.custom(
        onRefresh: onRefresh,
        onLoad: onLoad,
        firstRefresh: firstRefresh,
        reverse: reverse,
        controller: controller,
        shrinkWrap: shrinkWrap,
        scrollDirection: direction,
        firstRefreshWidget: firstWidget,
        emptyWidget: emptyWidget,
        headerIndex: headerIndex,
        header: FRefreshHeader(),
        footer: FRefreshFooter(),
        scrollController: scroll,
        slivers: slivers,
      );
    }
    if (builder != null) {
      return EasyRefresh.builder(
        onRefresh: onRefresh,
        onLoad: onLoad,
        controller: controller,
        firstRefresh: firstRefresh,
        header: FRefreshHeader(),
        footer: FRefreshFooter(),
        scrollController: scroll,
        builder: builder,
      );
    }
    return EasyRefresh(
      onRefresh: onRefresh,
      onLoad: onLoad,
      firstRefreshWidget: firstWidget,
      firstRefresh: firstRefresh,
      controller: controller,
      header: FRefreshHeader(),
      footer: FRefreshFooter(),
      headerIndex: headerIndex,
      emptyWidget: emptyWidget,
      scrollController: scroll,
      child: child,
    );
  }
}
