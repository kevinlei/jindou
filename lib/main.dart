import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'import.dart';

final GlobalKey<NavigatorState> navigatorKey = GlobalKey();

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final toastBuilder = BotToastInit();
  bool goNext = true;
  String splashAd = "";
  String splashLink = "";
  bool showSplash = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      navigatorKey: navigatorKey,
      title: '金豆派对',
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      navigatorObservers: [BotToastNavigatorObserver()],
      supportedLocales: const [Locale('zh', 'CN'), Locale('en', 'US')],
      theme: getAppThemeData(),
      builder: appBuilder,
      home: const LoginResume(),
    );
  }

  ThemeData getAppThemeData() {
    return ThemeData(
      platform: TargetPlatform.iOS,
      brightness: Brightness.light,
    );
  }

  Widget appBuilder(BuildContext context, Widget? widget) {
    ScreenUtil.init(context, designSize: const Size(750, 1624));
    widget = toastBuilder(context, widget);
    Widget widgets = Scaffold(
      resizeToAvoidBottomInset: false,
      body: widget,
      backgroundColor: Colors.black,
    );
    return widgets;
  }
}
