import 'package:echo/import.dart';

class LoggerInterceptor extends Interceptor {
  late DateTime _startTime;
  late DateTime _endTime;

  @override
  onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    _startTime = DateTime.now();
    super.onRequest(options, handler);
  }

  @override
  onResponse(Response response, ResponseInterceptorHandler handler) {
    _endTime = DateTime.now();
    int duration = _endTime.difference(_startTime).inMilliseconds;
    print('请求响应时间 $duration 毫秒');
    super.onResponse(response, handler);
  }
}
