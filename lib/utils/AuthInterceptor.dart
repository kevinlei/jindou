import 'dart:convert';
import 'package:echo/import.dart';
import 'package:flutter/foundation.dart';

class AuthInterceptor extends Interceptor {
  @override
  void onResponse(
      Response<dynamic> response, ResponseInterceptorHandler handler) async {
    if (kDebugMode) {
      print("请求地址${response.requestOptions.uri}");
      print("请求体${response.requestOptions.data}");
      print("请求返回信息$response");
    }
    super.onResponse(response, handler);
  }

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) async {
    DioException exception = DioException.create(err);
    err.error = exception;
    if (kDebugMode) {
      print("请求错误地址${err.requestOptions.uri}");
      print("请求体${err.requestOptions.data}");
      print("请求错误信息$err");
    }
    super.onError(err, handler);
  }
}
