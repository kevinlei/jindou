import 'package:flutter/material.dart';

/*
 * <p> 路由管理辅助工具
 * 说明：（1）、以出栈和入栈的思想来看待路由管理；
 *      （2）、出栈和入栈都在同一个栈；
 *      （3）、出栈：对应页面会销毁；
 *      （4）、入栈：对应页面会创建；</p>
 */
typedef OnRouteResultCallBack = void Function(
    dynamic returnResult); //push进栈的路由页面销毁时返回结果回调

class RouteUtil {
  RouteUtil._();

  /// 获取传递参数
  /// 注：（1)、在Widget build(BuildContext context)方法里可以拿到，其它位置会报错；
  ///    （2)、可以作为静态路由参数；
  static Object? getArguments(BuildContext context) {
    if (null != context) {
      return ModalRoute.of(context)?.settings?.arguments;
    }
    return null;
  }

  /// 出栈 当前页面销毁
  /// [returnResult]返回数据给push当前页面的路由
  static void pop(BuildContext context, [dynamic returnResult]) {
    if (null != context) {
      Navigator.of(context).pop(returnResult);
    }
  }

  /// 一直出栈至到遇到路由名为[utilRouteName]
  /// 名为[utilRouteName]路由页面以上的路由都出栈
  static void popUntil(BuildContext context, String utilRouteName) {
    if (null != context && !_isRouteNameEmpty(utilRouteName)) {
      Navigator.popUntil(context, ModalRoute.withName(utilRouteName.trim()));
    }
  }

  /// 当前页面出栈 同时 入栈[newRouteName]    类似功能[pushReplacement]、 [pushReplacementNamed]
  /// [newRouteName]入栈静态路由路由名 注：此处不能用动态路由路由名
  /// [arguments]传递给路由页面[routePage]的参数  可以通过[getArguments]获取
  /// [returnResult]返回数据给push当前页面的路由
  static void popAndPushNamed(
    BuildContext context,
    String newRouteName, {
    Object? arguments,
    dynamic returnResult,
  }) {
    if (null != context && !_isRouteNameEmpty(newRouteName)) {
      Navigator.popAndPushNamed(
        context,
        newRouteName,
        result: returnResult,
        arguments: arguments,
      );
    }
  }

  /// 入栈    类似功能[pushNamed]
  /// [routePage]入栈路由页面
  /// [routeName]设置入栈路由页面[routePage]路由名  注：此为动态路由路由名，不能像静态路由用于push，只能用于pop
  /// [arguments]传递给路由页面[routePage]的参数  可以通过[getArguments]获取
  /// [resultCallBack]路由页面[routePage]出栈[pop]返回结果回调
  static void push(
    BuildContext context,
    Widget routePage, {
    String? routeName,
    Object? arguments,
    OnRouteResultCallBack? resultCallBack,
    bool maintainState = true,
    bool fullscreenDialog = false,
  }) {
    if (null != context && null != routePage) {
      Future futureResultCallBack = Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) {
            return routePage;
          },
          settings: RouteSettings(
              name: routeName?.trim() ?? "", arguments: arguments),
          maintainState: maintainState,
          fullscreenDialog: fullscreenDialog,
        ),
      );
      if (null != resultCallBack) {
        futureResultCallBack?.then(resultCallBack);
      }
    }
  }

  /// 入栈    类似功能[push]
  /// [newRouteName]入栈静态路由路由名 注：此处不能用动态路由路由名
  /// [arguments]传递给路由页面[routePage]的参数  可以通过[getArguments]获取
  /// [resultCallBack]路由页面[routePage]出栈[pop]返回结果回调
  static void pushNamed(
    BuildContext context,
    String newRouteName, {
    Object? arguments,
    OnRouteResultCallBack? resultCallBack,
  }) {
    if (null != context && !_isRouteNameEmpty(newRouteName)) {
      Future futureResultCallBack = Navigator.pushNamed(
        context,
        newRouteName.trim(),
        arguments: arguments,
      );
      if (null != resultCallBack) {
        futureResultCallBack?.then(resultCallBack);
      }
    }
  }

  /// 一直出栈至到遇到路由名为[utilRouteName]，同时[routePage]进栈   类似功能[pushNamedAndRemoveUntil]
  /// 名为[utilRouteName]路由页面以上的路由都出栈
  /// [routePage]入栈路由页面
  /// [routeName]设置入栈路由页面[routePage]路由名  注：此为动态路由路由名，不能像静态路由用于push，只能用于pop
  /// [arguments]传递给路由页面[routePage]的参数  可以通过[getArguments]获取
  static void pushAndRemoveUntil(
    BuildContext context,
    Widget routePage,
    String utilRoteName, {
    String? routeName,
    Object? arguments,
    bool maintainState = true,
    bool fullscreenDialog = false,
  }) {
    if (null != context &&
        null != routePage &&
        !_isRouteNameEmpty(utilRoteName)) {
      Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(
          builder: (context) {
            return routePage;
          },
          settings: RouteSettings(
              name: routeName?.trim() ?? "", arguments: arguments),
          maintainState: maintainState,
          fullscreenDialog: fullscreenDialog,
        ),
        ModalRoute.withName(utilRoteName?.trim() ?? ""), //todo
      );
    }
  }

  /// 一直出栈至到遇到路由名为[utilRouteName]，同时[newRouteName]进栈    类似功能[pushAndRemoveUntil]
  /// 名为[utilRouteName]路由页面以上的路由都出栈
  /// [newRouteName]入栈静态路由路由名 注：此处不能用动态路由路由名
  /// [arguments]传递给路由页面[routePage]的参数  可以通过[getArguments]获取
  static void pushNamedAndRemoveUntil(
    BuildContext context,
    String newRouteName,
    String utilRoteName, {
    Object? arguments,
  }) {
    if (null != context &&
        !_isRouteNameEmpty(newRouteName) &&
        !_isRouteNameEmpty(utilRoteName)) {
      Navigator.pushNamedAndRemoveUntil(
        context,
        newRouteName.trim(),
        ModalRoute.withName(utilRoteName.trim()),
        arguments: arguments,
      );
    }
  }

  /// 清空栈 再 入栈  （一般用于跳到登录页面）    类似功能[pushNamedAndRemoveAll]
  /// [routePage]入栈路由页面
  /// [routeName]设置入栈路由页面[routePage]路由名  注：此为动态路由路由名，不能像静态路由用于push，只能用于pop
  /// [arguments]传递给路由页面[routePage]的参数  可以通过[getArguments]获取
  static void pushAndRemoveAll(
    BuildContext context,
    Widget routePage, {
    String? routeName,
    Object? arguments,
    bool maintainState = true,
    bool fullscreenDialog = false,
  }) {
    if (null != context && null != routePage) {
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
            builder: (context) {
              return routePage;
            },
            settings: RouteSettings(
                name: routeName?.trim() ?? "", arguments: arguments),
            maintainState: maintainState,
            fullscreenDialog: fullscreenDialog,
          ),
          (Route<dynamic> route) => false);
    }
  }

  /// 清空栈 再 入栈 （一般用于跳到登录页面）   类似功能[pushAndRemoveAll]
  /// [newRouteName]入栈静态路由路由名 注：此处不能用动态路由路由名
  /// [arguments]传递给路由页面[routePage]的参数  可以通过[getArguments]获取
  static void pushNamedAndRemoveAll(
    BuildContext context,
    String newRouteName, {
    Object? arguments,
  }) {
    if (null != context && !_isRouteNameEmpty(newRouteName)) {
      Navigator.pushNamedAndRemoveUntil(
        context,
        newRouteName.trim(),
        (Route<dynamic> route) => false,
        arguments: arguments,
      );
    }
  }

  /// 当前页面出栈 同时 入栈[routePage]   类似功能[popAndPushNamed]、 [pushReplacementNamed]
  /// [routePage]入栈路由页面
  /// [routeName]设置入栈路由页面[routePage]路由名  注：此为动态路由路由名，不能像静态路由用于push，只能用于pop
  /// [arguments]传递给路由页面[routePage]的参数  可以通过[getArguments]获取
  /// [returnResult]返回数据给push当前页面的路由
  static void pushReplacement(
    BuildContext context,
    Widget routePage, {
    String? routeName,
    Object? arguments,
    dynamic returnResult,
    bool maintainState = true,
    bool fullscreenDialog = false,
  }) {
    if (null != context && null != routePage) {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) {
            return routePage;
          },
          settings: RouteSettings(
              name: routeName?.trim() ?? "", arguments: arguments),
          maintainState: maintainState,
          fullscreenDialog: fullscreenDialog,
        ),
        result: returnResult,
      );
    }
  }

  /// 当前页面出栈 同时 入栈[newRouteName]    类似功能[pushReplacement]、 [popAndPushNamed]
  /// [newRouteName]入栈静态路由路由名 注：此处不能用动态路由路由名
  /// [arguments]传递给路由页面[routePage]的参数  可以通过[getArguments]获取
  /// [returnResult]返回数据给push当前页面的路由
  static void pushReplacementNamed(
    BuildContext context,
    String newRouteName, {
    Object? arguments,
    dynamic returnResult,
  }) {
    if (null != context && !_isRouteNameEmpty(newRouteName)) {
      Navigator.pushReplacementNamed(
        context,
        newRouteName.trim(),
        arguments: arguments,
        result: returnResult,
      );
    }
  }

  static _isRouteNameEmpty(String? routeName) {
    return (null == routeName || routeName.trim().length <= 0);
  }
}
