/*
 * <p> 字符串操作工具</p>
 */
class StringUtil {
  StringUtil._();
  ///转为字符串
  static String toStringValue(dynamic value) {
    if (null == value) {
      return "";
    }
    return '$value';
  }

  ///判断字符串是否相等 都为空不相等
  static bool isEqual(String? A, String? B) {
    if (null != A && null != B) {
      return A.trim().compareTo(B.trim()) == 0;
    }
    return false;
  }

  ///判断字符串是否相等 都为空相等
  static bool isSimpleEqual(String A, String B) {
    if (null == A && null == B) {
      return true;
    }
    return isEqual(A, B);
  }

  ///判断字符串是否为空
  static bool isEmpty(String? value) {
    if (null != value && value.trim().length > 0) {
      return false;
    }
    return true;
  }

  ///不为空字符串
  static String stringNotNull(String value) {
    return value?.trim() ?? "";
  }

  /// 转为double类型
  static double toDouble(String value, {double defaultValue = 0.0}) {
    if (!isEmpty(value)) {
      return double.tryParse(value) ?? defaultValue;
    }
    return defaultValue;
  }

  /// 转为int类型
  static int toInt(String value, {int defaultValue = 0}) {
    if (!isEmpty(value)) {
      return int.tryParse(value) ?? defaultValue;
    }
    return defaultValue;
  }

  static num? toNum(String? value, {num? defaultValue}) {
    if (!isEmpty(value)) {
      return num.tryParse(value!) ?? defaultValue;
    }
    return defaultValue;
  }
}
