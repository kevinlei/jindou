import 'dart:convert';
import 'package:echo/import.dart';

class DioException implements Exception {
  final String message;
  final int code;

  DioException(this.code, this.message);

  @override
  String toString() {
    return "code：$code || msg：$message";
  }

  factory DioException.create(DioError error) {
    void elk(String characteristic) {
      if (error.requestOptions.uri.toString().contains("elk/log")) {
        AppConstants.channelStatistics(characteristic,
            extra: "${{
              "url": error.requestOptions.uri,
            }}");
      }
    }

    switch (error.type) {
      case DioErrorType.cancel:
        return BadRequestException(-2, "请求取消");

      case DioErrorType.connectTimeout:
        elk("connectTimeout");
        return BadRequestException(-2, "连接超时");

      case DioErrorType.sendTimeout:
        elk("sendTimeout");
        return BadRequestException(-2, "请求超时");

      case DioErrorType.receiveTimeout:
        elk("receiveTimeout");
        return BadRequestException(-2, "响应超时");

      case DioErrorType.other:
        if (error.message.contains("SocketException")) {
          return BadRequestException(-2, "网络异常");
        }
        elk("other");
        return BadRequestException(-2, "其他错误");

      case DioErrorType.response:
        {
          int errCode = error.response?.statusCode ?? 0;
          switch (errCode) {
            case 400:
              try {
                Map? response = json.decode(error.response?.data);
                String message = response?["error_description"] ?? "";
                return BadRequestException(
                    errCode, message.isEmpty ? "请求错误400" : message);
              } catch (e) {
                return BadRequestException(errCode, "请求错误400");
              }
            case 401:
              try {
                Map? errorMsg = jsonDecode(error.response?.data);
                String description = errorMsg?["msg"] ?? "登录已过期";
                return BadRequestException(errCode, description);
              } catch (e) {
                return BadRequestException(errCode, "登录已过期");
              }
            case 403:
              return BadRequestException(errCode, "服务器拒绝执行403");
            case 404:
              return BadRequestException(errCode, "无法连接服务器404");
            case 405:
              return BadRequestException(errCode, "请求方法被禁止405");
            case 500:
              return BadRequestException(errCode, "服务器开小差了～");
            case 502:
              return BadRequestException(errCode, "服务器开小差了～");
            case 503:
              return BadRequestException(errCode, "服务器开小差了～");
            case 505:
              return BadRequestException(errCode, "不支持HTTP协议请求505");
            case 601:
              try {
                Map? map = json.decode(error.response?.data);
                String message = map?["msg"] ?? "请求错误601";
                return BadRequestException(errCode, message);
              } catch (e) {
                return BadRequestException(errCode, "请求错误601");
              }
            default:
              return DioException(
                  errCode, error.response?.statusMessage ?? "异常错误");
          }
        }
      default:
        return DioException(-1003, error.message);
    }
  }
}

// 请求错误
class BadRequestException extends DioException {
  BadRequestException(int code, String message) : super(code, message);
}
