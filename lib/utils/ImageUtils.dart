import 'dart:ui' as ui;
import 'package:echo/import.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:transparent_image/transparent_image.dart';

enum ImageFormat {
  PNG,
  JPG,
  GIF,
  WEBP,
}

extension ImageFormatExtension on ImageFormat {
  String get value => ["png", "jpg", "gif", "webp"][index];
}

class ImageUtils {
  ImageUtils._();
  static final ImageUtils _instance = ImageUtils._();
  factory ImageUtils() => _instance;

  ImageProvider getAssetsImage(String name,
      {ImageFormat format = ImageFormat.PNG}) {
    return AssetImage(getImgPath(name, format: format));
  }

  ImageProvider getImageProvider(String imageUrl,
      {String holderImg = 'none', ImageFormat? format = ImageFormat.PNG}) {
    if (imageUrl.isEmpty || imageUrl == NetConstants.ossPath) {
      return AssetImage(getImgPath(holderImg, format: format));
    }
    return NetworkImage(imageUrl);
  }

  String getImgPath(String name, {ImageFormat? format = ImageFormat.PNG}) {
    return "assets/$name.${format?.value}";
  }

  Future<ui.Image> getFrameImage(String name,
      {ImageFormat format = ImageFormat.PNG}) async {
    String asset = "assets/$name.${format.value}";
    ByteData data = await rootBundle.load(asset);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List());
    ui.FrameInfo frameInfo = await codec.getNextFrame();
    return frameInfo.image;
  }
}

class LoadAssetImage extends StatelessWidget {
  const LoadAssetImage(this.image,
      {this.width,
      this.height,
      this.fit,
      this.format = ImageFormat.PNG,
      this.color});

  final String? image;
  final double? width;
  final double? height;
  final BoxFit? fit;
  final ImageFormat? format;
  final Color? color;

  @override
  Widget build(BuildContext context) {
    String path = image ?? "";
    path = path.isEmpty ? "none" : path;
    return Image.asset(
      ImageUtils().getImgPath(path, format: format),
      height: height,
      width: width,
      fit: fit,
      color: color,
      excludeFromSemantics: true,
    );
  }
}

class LoadNetImage extends StatelessWidget {
  const LoadNetImage(
    this.image, {
    this.width,
    this.height,
    this.fit = BoxFit.cover,
    this.format = ImageFormat.PNG,
    this.holderImg = 'none',
    this.showHolder = true,
    this.placeWidget,
    this.bgc = false,
    this.bgcImg,
  });

  final String image;
  final double? width;
  final double? height;
  final BoxFit fit;
  final ImageFormat format;
  final String holderImg;
  final bool showHolder;
  final Widget? placeWidget;
  final bool? bgc;
  final String? bgcImg;

  @override
  Widget build(BuildContext context) {
    Widget _image = LoadAssetImage(holderImg,
        height: height, width: width, fit: fit, format: ImageFormat.PNG);
    if (image.isEmpty || image == NetConstants.ossPath) {
      return _image;
    }
    if (image.startsWith('http')) {
      if (bgc == true) {
        if (bgcImg != null) {
          return FadeInImage.assetNetwork(
            image: image,
            placeholder: bgcImg!,
            width: width,
            height: height,
            fit: fit,
            fadeOutDuration: const Duration(milliseconds: 80),
            fadeInDuration: const Duration(milliseconds: 80),
          );
        } else {
          return FadeInImage.memoryNetwork(
            image: image,
            placeholder: kTransparentImage,
            width: width,
            height: height,
            fit: fit,
            fadeOutDuration: const Duration(milliseconds: 80),
            fadeInDuration: const Duration(milliseconds: 80),
          );
        }
      }
      return FadeInImage.memoryNetwork(
        image: image,
        placeholder: kTransparentImage,
        width: width,
        height: height,
        fit: fit,
        fadeOutDuration: const Duration(milliseconds: 80),
        fadeInDuration: const Duration(milliseconds: 80),
      );
    }
    return LoadAssetImage(
      image,
      height: height,
      width: width,
      fit: fit,
      format: format,
    );
  }
}
