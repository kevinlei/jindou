import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:dio/adapter.dart';
import 'package:echo/import.dart';
import 'package:flutter/cupertino.dart';

typedef NetSuccessCallback = Function(dynamic data);

typedef NetSuccessCodeCallback = Function(dynamic data, int code, String msg);
typedef NetErrorCallback = Function(int code, String msg);

class DioUtils {
  late Dio _dio;
  Dio get dio => _dio;
  static final DioUtils _instance = DioUtils._();
  factory DioUtils() => _instance;

  final CancelToken _cancelToken = CancelToken();

  DioUtils._() {
    BaseOptions options = BaseOptions(
      connectTimeout: 10000,
      receiveTimeout: 10000,
      sendTimeout: 10000,
      responseType: ResponseType.plain,
    );
    _dio = Dio(options);
    _dio.interceptors.addAll([LoggerInterceptor(), AuthInterceptor()]);

     if(false){
       setProxy();
     }
  }

  void setProxy() {
    if (true) {
      //允许开启代理
      //获取代理主机ip
      String? host = '192.168.1.107'; //= '192.168.43.166';
      if (host == null || host.isEmpty) {
        //已设置代理
        return;
      }
      (_dio?.httpClientAdapter as DefaultHttpClientAdapter)
          ?.onHttpClientCreate = (client) {
        client.findProxy = (uri) {
          return 'PROXY $host:8888';
        };
        client.badCertificateCallback =
            (X509Certificate cert, String host, int port) => true;
      };
    }
  }

  /// 同步返回请求
  Future httpRequest(
    String url, {
    DioMethod method = DioMethod.GET,
    params,
    bool needMachine = true,
    bool loading = true,
    bool hastoast = true, //失败是否弹窗
    String contentType = "application/json",
    Map<String, dynamic>? headerEntries,
    CancelToken? cancelToken,
    NetSuccessCallback? onSuccess,
    NetErrorCallback? onError,
  }) {
    if (loading) {
      WidgetUtils().showLoading();
    }
    return _request(
      url,
      method: method,
      params: params,
      contentType: contentType,
      headerEntries: headerEntries,
      cancelToken: cancelToken,
      needMachine: needMachine,
    ).then((result) {
      _onResult(result, hastoast, onSuccess, onError);
      WidgetUtils().cancelLoading();
    }, onError: (dynamic error) {
      WidgetUtils().cancelLoading();
      int code = error.error?.code ?? 0;
      String msg = error.error?.message ?? '网络异常';
      _onError(code, msg, hastoast, onError);
    });
  }

  /// 异步返回请求
  void asyncHttpRequest(
    String url, {
    DioMethod method = DioMethod.GET,
    params,
    bool needMachine = true,
    bool loading = true,
    bool hastoast = true, //失败是否弹窗
    String contentType = "application/json",
    Map<String, dynamic>? headerEntries,
    CancelToken? cancelToken,
    NetSuccessCallback? onSuccess,
    NetErrorCallback? onError,
  }) {
    if (loading) {
      WidgetUtils().showLoading();
    }
    Stream.fromFuture(_request(
      url,
      method: method,
      params: params,
      contentType: contentType,
      headerEntries: headerEntries,
      cancelToken: cancelToken,
      needMachine: needMachine,
    )).asBroadcastStream().listen((result) {
      _onResult(result, hastoast, onSuccess, onError);
      WidgetUtils().cancelLoading();
    }, onError: (dynamic error) {
      WidgetUtils().cancelLoading();
      int code = error.error?.code ?? 0;
      String msg = error.error?.message ?? '网络异常';
      _onError(code, msg, hastoast, onError);
    });
  }

  Future _request(
    String url, {
    DioMethod method = DioMethod.GET,
    params,
    String contentType = "application/json",
    CancelToken? cancelToken,
    Map<String, dynamic>? headerEntries,
    bool? needMachine,
  }) async {
    _initDioHeader(method, contentType, headerEntries,
        needMachine: needMachine);
    final Response response = await _dio.request(
      url,
      queryParameters: method == DioMethod.GET ? params : null,
      data: method == DioMethod.GET ? null : params,
      cancelToken: cancelToken ?? _cancelToken,
    );
    try {
      String data = response.data.toString();
      Map<String, dynamic> map = json.decode(data) as Map<String, dynamic>;
      return map;
    } catch (error) {
      debugPrint("请求返回结果错误$error");
      return {"code": -1001, "msg": "网络繁忙"};
    }
  }

  /// code不只为1同步返回请求
  Future httpcodeRequest(
    String url, {
    DioMethod method = DioMethod.GET,
    params,
    bool isSuccess = false, // 是否成功code不止为1
    bool needMachine = true,
    bool loading = true,
    bool hastoast = true, //失败是否弹窗
    String contentType = "application/json",
    Map<String, dynamic>? headerEntries,
    CancelToken? cancelToken,
    NetSuccessCodeCallback? onSuccess,
    NetErrorCallback? onError,
  }) {
    if (loading) {
      WidgetUtils().showLoading();
    }
    return _requestcode(
      url,
      method: method,
      params: params,
      contentType: contentType,
      headerEntries: headerEntries,
      cancelToken: cancelToken,
      needMachine: needMachine,
    ).then((result) {
      _onResultcode(result, isSuccess, hastoast, onSuccess, onError);
      WidgetUtils().cancelLoading();
    }, onError: (dynamic error) {
      WidgetUtils().cancelLoading();
      int code = error.error?.code ?? 0;
      String msg = error.error?.message ?? '网络异常';
      _onError(code, msg, hastoast, onError);
    });
  }

  /// code不只为1异步返回请求
  void asyncHttpcodeRequest(
    String url, {
    DioMethod method = DioMethod.GET,
    params,
    bool isSuccess = false, // 是否成功code不止为1
    bool needMachine = true,
    bool loading = true,
    bool hastoast = true, //失败是否弹窗
    String contentType = "application/json",
    Map<String, dynamic>? headerEntries,
    CancelToken? cancelToken,
    NetSuccessCodeCallback? onSuccess,
    NetErrorCallback? onError,
  }) {
    if (loading) {
      WidgetUtils().showLoading();
    }
    Stream.fromFuture(_requestcode(
      url,
      method: method,
      params: params,
      contentType: contentType,
      headerEntries: headerEntries,
      cancelToken: cancelToken,
      needMachine: needMachine,
    )).asBroadcastStream().listen((result) {
      _onResultcode(result, isSuccess, hastoast, onSuccess, onError);
      WidgetUtils().cancelLoading();
    }, onError: (dynamic error) {
      WidgetUtils().cancelLoading();
      int code = error.error?.code ?? 0;
      String msg = error.error?.message ?? '网络异常';
      _onError(code, msg, hastoast, onError);
    });
  }

  Future _requestcode(
    String url, {
    DioMethod method = DioMethod.GET,
    params,
    String contentType = "application/json",
    CancelToken? cancelToken,
    Map<String, dynamic>? headerEntries,
    bool? needMachine,
  }) async {
    _initDioHeader(method, contentType, headerEntries,
        needMachine: needMachine);
    final Response response = await _dio.request(url,
        queryParameters: method == DioMethod.GET ? params : null,
        data: method == DioMethod.GET ? null : params,
        cancelToken: cancelToken ?? _cancelToken);
    try {
      final String data = response.data.toString();
      final Map<String, dynamic> map =
          json.decode(data) as Map<String, dynamic>;
      return map;
    } catch (error) {
      debugPrint("请求返回结果错误$error");
      return {"code": -1001, "msg": "网络繁忙"};
    }
  }

  void _initDioHeader(
      DioMethod method, String contentType, Map<String, dynamic>? headerEntries,
      {bool? needMachine = true}) {
    _dio.options.headers.clear();
    _dio.options.method = method.value;
    _dio.options.contentType = contentType;
    if (headerEntries != null && headerEntries.isNotEmpty) {
      _dio.options.headers = headerEntries;
    }
    if (AppConstants.accessToken.isNotEmpty &&
        !_dio.options.headers.containsKey("authorization")) {
      _dio.options.headers.addEntries({
        "authorization": AppConstants.accessToken,
        "Connection": "keep-alive",
        "os": Platform.isAndroid ? "android" : "ios",
      }.entries);
    }
    if (!_dio.options.headers.containsKey("machine_code") &&
        needMachine == true) {
      String machineCode = AppConstants.machineCode;
      _dio.options.headers.addEntries({
        "mid": machineCode,
        "os_ver": utf8.encode(AppConstants.device),
        "app_ver": "${AppConstants.version}_${AppConstants.appBuild}"
      }.entries);
    }
  }

  void _onResult(result, bool hastoast, NetSuccessCallback? onSuccess,
      NetErrorCallback? onError) {
    if (result['code'] == 1) {
      if (onSuccess == null) return;
      onSuccess(result['data']);
    } else {
      int code = result?['code'] ?? result?['Code'] ?? 0;
      String msg = result?['msg'] ?? result?['Msg'] ?? "网络异常";
      _onError(code, msg, hastoast, onError);
    }
  }

  void _onResultcode(result, bool isSuccess, bool hastoast,
      NetSuccessCodeCallback? onSuccess, NetErrorCallback? onError) {
    if (result['code'] == 1 || isSuccess) {
      if (onSuccess == null) return;
      int code = result?['code'] ?? result?['Code'] ?? 0;
      String msg = result?['msg'] ?? result?['Msg'] ?? "网络异常";
      onSuccess(isSuccess ? result['data'] : result['data'] ?? result['Data'],
          code, msg);
    } else {
      int code = result?['code'] ?? result?['Code'] ?? 0;
      String msg = result?['msg'] ?? result?['Msg'] ?? "网络异常";
      _onError(code, msg, hastoast, onError);
    }
  }

  void _onError(
      int code, String msg, bool hastoast, NetErrorCallback? onError) async {
    if (code == -1) {
      AppConstants.accessToken = "";
      AppConstants.refreshToken = "";
      UserConstants.userginInfo = null;
      SharedPreferences preferences = await SharedPreferences.getInstance();
      await preferences.clear();
      preferences.setBool(SharedKey.KEY_UNIQUE.value, false);
      preferences.setString(
          SharedKey.KEY_MACHINE.value, AppConstants.machineCode);

      WidgetUtils().showNotification("温馨提示", msg);
      WidgetUtils().pushRemove(const LoginPage());
    } else {
      if (msg.isNotEmpty && hastoast) {
        WidgetUtils().showToast(msg);
      }
    }
    if (onError == null) return;
    onError(code, msg);
  }
}
