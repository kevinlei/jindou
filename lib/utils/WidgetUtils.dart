import 'package:echo/import.dart';
import 'package:echo/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WidgetUtils {
  static final WidgetUtils _instance = WidgetUtils._();
  factory WidgetUtils() => _instance;

  BuildContext? _context;
  WidgetUtils._() {
    _context = navigatorKey.currentContext;
  }

  Future pushPage(Widget page, {void Function()? fun}) async {
    debugPrint('当前页面$page');
    if (_context == null) return;
    String textReturn = "";
    await Navigator.push(
        _context!,
        CupertinoPageRoute(
            builder: (_) => page,
            settings: RouteSettings(
              name: page.toString(),
            ))).then((value) => {
          value != null ? textReturn = value : null,
          fun != null ? fun() : null
        });
    return textReturn;
  }

  void pushRemove(Widget page) {
    if (_context == null) return;
    Navigator.pushAndRemoveUntil(
        _context!,
        CupertinoPageRoute(
            builder: (_) => page,
            settings: RouteSettings(
              name: page.toString(),
            )), (route) {
      return false;
    });
  }

  void pushReplace(Widget page) {
    if (_context == null) return;
    Future.delayed(Duration.zero, () {
      Navigator.pushReplacement(
        _context!,
        CupertinoPageRoute(
          builder: (_) => page,
          settings: RouteSettings(
            name: page.toString(),
          ),
        ),
      );
    });
  }

  void popPage([Object? result]) {
    if (_context == null || !Navigator.canPop(_context!)) return;
    Navigator.pop(_context!, result);
  }

  Future<T?> showBottomSheet<T>(Widget widget,
      {enableDrag = true,
      Color? color,
      ShapeBorder? shape,
      bool fullScreen = true,
      void Function()? onCallback}) async {
    if (_context == null) return null;
    return await showModalBottomSheet(
        context: _context!,
        backgroundColor: color ?? const Color(0xFF1D1928),
        isScrollControlled: fullScreen,
        enableDrag: enableDrag,
        shape: shape ??
            RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(30.w),
                topLeft: Radius.circular(30.w),
              ),
            ),
        builder: (_) => widget).whenComplete(() {
      onCallback != null ? onCallback() : null;
    });
  }

  void showToast(String msg, {Color? backColor, Color? textColor}) {
    BotToast.showText(
      text: msg.substring(0, msg.length > 200 ? 200 : msg.length),
      contentColor: backColor ?? Colors.black.withOpacity(0.4),
      textStyle: TextStyle(color: textColor ?? Colors.white),
    );
  }

  void showLoading() {
    BotToast.showCustomLoading(toastBuilder: (_) => const FLoading());
  }

  void cancelLoading() {
    BotToast.closeAllLoading();
  }

  void showNotification(String title, String subTitle,
      {int? second, Function()? onClose}) {
    BotToast.showNotification(
      borderRadius: 10.w,
      duration: Duration(seconds: second ?? 4),
      title: (_) => FText(title, color: Colors.black),
      onClose: onClose,
      subtitle: (_) => FText(subTitle,
          size: 24.sp, maxLines: 100, color: const Color(0xFF999999)),
    );
  }

  Future<bool?> showAlert(String content,
      {String cancelText = "",
      String confirmText = "",
      String tirText = ""}) async {
    if (_context == null) return false;
    return await showDialog(
        context: _context!,
        barrierDismissible: true,
        builder: (_) => _alertDialog(content,
            cancelText: cancelText,
            confirmText: confirmText,
            TirText: tirText));
  }

  Future<T?> showFDialog<T>(Widget widget,
      {Color barrier = Colors.black12,
      bool dismiss = true,
      Offset? anchorPoint,
      bool useSafeArea = true}) async {
    if (_context == null) return null;
    return await showDialog(
      context: _context!,
      barrierDismissible: dismiss,
      barrierColor: barrier,
      anchorPoint: anchorPoint,
      useSafeArea: useSafeArea,
      builder: (_) => widget,
    );
  }

  Widget _alertDialog(String content,
      {String cancelText = "", String confirmText = "", String TirText = ""}) {
    Widget contentWidget = Expanded(
      child: Center(
        child: FText(content,
            align: TextAlign.center,
            size: 28.sp,
            color: const Color(0xCC222222),
            overflow: TextOverflow.ellipsis,
            maxLines: 2),
      ),
    );

    Widget cancelButton = GestureDetector(
      onTap: () => popPage(false),
      child: FContainer(
        width: 210.w,
        height: 80.w,
        border: Border.all(color: const Color(0xFFFF753F)),
        radius: BorderRadius.circular(80.r),
        align: Alignment.center,
        child: FText(cancelText.isEmpty ? "取消" : cancelText,
            color: const Color(0xFFFF753F), size: 32.sp),
      ),
    );

    Widget confirmButton = GestureDetector(
      onTap: () => popPage(true),
      child: FContainer(
        width: 210.w,
        height: 80.w,
        gradientBegin: Alignment.centerLeft,
        gradientEnd: Alignment.centerRight,
        gradient: const [Color(0xFFFF753F), Color(0xFFFF753F)],
        radius: BorderRadius.circular(80.w),
        align: Alignment.center,
        child: FText(confirmText.isEmpty ? "确认" : confirmText,
            color: Colors.white, size: 32.sp),
      ),
    );

    return Center(
      child: FContainer(
        width: 540.w,
        height: 300.w,
        color: const Color(0xFFFFFFFF),
        padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 20.w),
        radius: BorderRadius.circular(28.w),
        child: Column(children: [
          FText(TirText.isEmpty ? "温馨提示" : TirText,
              size: 32.sp, color: const Color(0xCC222222)),
          SizedBox(height: 10.w),
          contentWidget,
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [cancelButton, confirmButton])
        ]),
      ),
    );
  }

  static String zeroFill(int i) {
    return i >= 10 ? "$i" : "0$i";
  }

  static String formatSeconds(int seconds) {
    seconds = (seconds / 1000).truncate();
    int minutes = seconds ~/ 60;
    int remainingSeconds = seconds % 60;
    String minutesStr = (minutes < 10) ? "0$minutes" : minutes.toString();
    String secondsStr = (remainingSeconds < 10)
        ? "0$remainingSeconds"
        : remainingSeconds.toString();
    return "$minutesStr:$secondsStr";
  }
}
