/*
 * <p> list操作工具</p>
 */

class ListUtil {
  ListUtil._();

  ///判断list是否不为空
  static bool isListValid(List? list) {
    if (null != list) {
      return true;
    }
    return false;
  }

  ///判断list是否有数据
  static bool isListHasData(List? list) {
    if (null != list && list.length > 0) {
      return true;
    }
    return false;
  }

  ///清空list
  static void clear(List? list) {
    list?.clear();
  }

  ///list item数量
  static int size(List? list) {
    return list?.length ?? 0;
  }

  ///添加[addList]到[list]尾部
  static void addAll(List? list, List? addList) {
    if (null != list && isListHasData(addList)) {
      list.addAll(addList!);
    }
  }

  ///清空[list]，再添加[addList]到[list]，此时[list]和[addList]数据一样
  static void resetAll(List? list, List? addList) {
    clear(list);
    if (null != list && isListHasData(addList)) {
      list.addAll(addList!);
    }
  }

  ///获取[list]指定[position]对应的item
  static T? getItem<T>(List<T>? list, int position) {
    return null == list ? null : list[position];
  }

  static T? getItemStrictly<T>(List<T>? list, int position) {
    if (null == list) {
      return null;
    }

    int lastIndex = list!.length - 1;
    if (position > lastIndex) {
      return null;
    }

    return list[position];
  }
}
