class AppInfo {
  /// The package name. `bundleIdentifier` on iOS, `getPackageName` on Android.
  String packageName = "";

  /// The package version. `CFBundleShortVersionString` on iOS, `versionName` on Android.
  String version = "";

  /// The build number. `CFBundleVersion` on iOS, `versionCode` on Android.
  int buildNumber = 1;

  AppInfo(Map map) {
    this.packageName = map.containsKey("packageName") ? map["packageName"] : "";
    this.version = map.containsKey("version") ? map["version"] : "";
    this.buildNumber = map.containsKey("buildNumber") ? map["buildNumber"] : 1;
  }

  @override
  String toString() {
    return "packageName: $packageName, version: $version, buildNumber: $buildNumber";
  }
}
