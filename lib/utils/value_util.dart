/*
 数值处理工具
 */
class ValueUtil {
  ValueUtil._();

  /// 将[value]四舍五入转为保存小数点[fractionDigits]位的String
  /// [fractionDigits]四舍五入保留小数位数
  static String? toStringAsFixed(num? value, int fractionDigits) {
    return value?.toStringAsFixed(fractionDigits);
  }

  /// 用于布尔判断，没值为false
  static bool boolValue(bool? value) {
    return value ?? false;
  }

  static bool isTrue(bool? value) {
    return value ?? false;
  }

  static String showValue(dynamic? value, {String defValue = '--'}) {
    return null == value ? defValue : value.toString();
  }

  static String showValueWithUnit(dynamic? value,
      {String defValue = '--', required String unit}) {
    return null == value ? defValue : '$value$unit';
  }

  static String showValueWithPrefixUnit(dynamic? value,
      {String defValue = '--', required String unit}) {
    return null == value ? defValue : '$unit$value';
  }

  static num? multi(num? v1,num? v2){
    if(null==v1||null==v2){
      return null;
    }

    return v1*v2;
  }

  static num? add(num? v1,num? v2){
    if(null==v1||null==v2){
      return null;
    }

    return v1+v2;
  }
}
