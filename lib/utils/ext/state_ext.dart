

import 'package:flutter/cupertino.dart';

import '../route_util.dart';

extension StateExtension on State {
  void refresh(VoidCallback? fn) {
    if (mounted) {
      setState(fn ?? () {});
    }
  }

  void exit({dynamic? returnResult}) {
    RouteUtil.pop(context, returnResult);
  }
}
