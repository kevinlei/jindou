import 'dart:core';
import 'dart:ui';

extension StringExtension on String {
  Color toColor() {
    final buffer = StringBuffer();
    if (length == 6) buffer.write('ff');
    if (length == 7) buffer.write('f');

    buffer.write(replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }
}
