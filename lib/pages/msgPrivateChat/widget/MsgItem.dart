import 'package:echo/import.dart';
import 'package:flutter/material.dart';
import 'package:im_flutter_sdk/im_flutter_sdk.dart';

class MsgTextCell extends StatelessWidget {
  final MessageDirection dir;
  final HXTextMsg msg;

  const MsgTextCell(this.dir, this.msg, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FContainer(
      padding: EdgeInsets.symmetric(horizontal: 25.w, vertical: 20.w),
      margin: EdgeInsets.symmetric(horizontal: 20.w),
      constraints: BoxConstraints(maxWidth: 500.w),
      color: dir == MessageDirection.SEND
          ? const Color(0xFFFF753F)
          : const Color(0xFFE5E6E7),
      radius: BorderRadius.only(
        bottomLeft: Radius.circular(20.w),
        bottomRight: Radius.circular(20.w),
        topLeft: dir == MessageDirection.RECEIVE
            ? const Radius.circular(0)
            : Radius.circular(20.w),
        topRight: dir == MessageDirection.SEND
            ? const Radius.circular(0)
            : Radius.circular(20.w),
      ),
      child: SelectableText(
        msg.text,
        textScaleFactor: 1.0,
        style: TextStyle(
          fontSize: 28.sp,
          color: dir == MessageDirection.SEND ? Colors.white : Colors.black,
        ),
      ),
    );
  }
}

class MsgImgCell extends StatelessWidget {
  final HXImageMsg msg;

  const MsgImgCell(this.msg, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20.w),
      child: ConstrainedBox(
        constraints: BoxConstraints(maxWidth: 460.w, maxHeight: 400.w),
        child: GestureDetector(
          onTap: () {
            WidgetUtils().pushPage(PhotoPreview([msg.imagePath], index: 0));
          },
          child: ClipRRect(
            borderRadius: BorderRadius.circular(10.w),
            child: LoadNetImage(NetConstants.ossPath + msg.imagePath,
                width: msg.width, height: msg.height),
          ),
        ),
      ),
    );
  }
}

class MsgAudioCell extends StatefulWidget {
  final MessageDirection dir;
  final HXAudioMsg msg;

  const MsgAudioCell(this.dir, this.msg, {Key? key}) : super(key: key);

  @override
  _MsgAudioCellState createState() => _MsgAudioCellState();
}

class _MsgAudioCellState extends State<MsgAudioCell> {
  bool isPlaying = false;

  @override
  Widget build(BuildContext context) {
    Widget btn = FContainer(
      padding: EdgeInsets.symmetric(horizontal: 25.w, vertical: 20.w),
      margin: EdgeInsets.symmetric(horizontal: 20.w),
      width: 210.w,
      color: const Color(0xFFFF753F),
      radius: BorderRadius.circular(10.w),
      child: cellContent(),
    );
    return GestureDetector(onTap: onTapCell, child: btn);
  }

  Widget cellContent() {
    int duration = widget.msg.duration;
    Widget anim = isPlaying
        ? ScrollText(
            maxWidth: 80.w,
            TextSpan(
              style: const TextStyle(
                color: Colors.white,
                letterSpacing: 3,
                fontWeight: FontWeight.w900,
              ),
              children: textSpanList(50),
            ),
          )
        : Text.rich(
            TextSpan(
              style: const TextStyle(
                color: Colors.white,
                letterSpacing: 3,
                fontWeight: FontWeight.w900,
              ),
              children: [
                ...textSpanList(3),
              ],
            ),
          );

    List<Widget> children = [
      anim,
      SizedBox(width: 10.w),
      FText("$duration ''", size: 28.sp)
    ];
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: widget.dir == MessageDirection.SEND
          ? children
          : children.reversed.toList(),
    );
  }

  List<TextSpan> textSpanList(int length) {
    List<TextSpan> list = [];
    for (var i = 0; i < length; i++) {
      List<TextSpan> list2 = [
        TextSpan(
          text: '|',
          style: TextStyle(
            fontSize: 22.sp,
          ),
        ),
        const TextSpan(
          text: '|',
        ),
      ];
      list.addAll(list2);
    }
    return list;
  }

  void startPlayer() async {
    if (AppManager().soundPlayer.isPlaying) return;
    try {
      String source = widget.msg.audioPath;
      if (source.isEmpty) {
        return WidgetUtils().showToast("播放路径错误");
      }
      update(true);
      source = NetConstants.ossPath + source;
      bool state = source.contains('wav');
      await AppManager().soundPlayer.startPlayer(
            fromURI: source,
            codec: !state ? Codec.aacADTS : Codec.pcm16WAV,
            sampleRate: 44100,
            whenFinished: () => update(false),
          );
    } catch (err) {
      debugPrint("播放录音错误：$err");
      stopPlayer();
    }
  }

  void stopPlayer() async {
    try {
      update(false);
      await AppManager().soundPlayer.stopPlayer();
    } catch (err) {
      debugPrint("停止播放出错：$err");
    }
  }

  void onTapCell() async {
    isPlaying ? stopPlayer() : startPlayer();
  }

  void update(bool open) {
    if (!mounted) return;
    setState(() {
      isPlaying = open;
    });
  }
}

class MsgHbaoCell extends StatefulWidget {
  final MessageDirection dir;
  final HXHbaoMsg msg;

  const MsgHbaoCell(this.dir, this.msg, {Key? key}) : super(key: key);

  @override
  _MsgHbaoCellState createState() => _MsgHbaoCellState();
}

class _MsgHbaoCellState extends State<MsgHbaoCell> {
  bool isPlaying = false;

  @override
  Widget build(BuildContext context) {
    return FContainer(
      padding: EdgeInsets.symmetric(horizontal: 25.w, vertical: 20.w),
      margin: EdgeInsets.symmetric(horizontal: 20.w),
      // width: double.infinity,
      gradient: const [
        Color(0xFFF8452C),
        Color(0xFFEF37C9),
        Color(0xFFB94BFF),
      ],
      gradientBegin: Alignment.topLeft,
      gradientEnd: Alignment.bottomRight,
      radius: BorderRadius.only(
        bottomLeft: Radius.circular(20.w),
        bottomRight: Radius.circular(20.w),
        topLeft: widget.dir == MessageDirection.RECEIVE
            ? const Radius.circular(0)
            : Radius.circular(20.w),
        topRight: widget.dir == MessageDirection.SEND
            ? const Radius.circular(0)
            : Radius.circular(20.w),
      ),
      child: Row(
        children: [
          LoadAssetImage(
            "msgPrivateChat/hbao/msgHbao",
            width: 80.w,
            fit: BoxFit.cover,
          ),
          SizedBox(width: 10.w),
          FText("赠送${widget.msg.hbaoNum}个金豆", size: 28.sp),
        ],
      ),
    );
  }
}

class MsgGiftCell extends StatelessWidget {
  final MessageDirection dir;
  final HXGiftMsg msg;

  const MsgGiftCell(this.dir, this.msg, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FContainer(
      padding: EdgeInsets.fromLTRB(dir == MessageDirection.SEND ? 100.w : 20.w,
          20.w, dir == MessageDirection.SEND ? 20.w : 100.w, 20.w),
      margin: EdgeInsets.symmetric(horizontal: 20.w),
      height: 160.w,
      constraints: BoxConstraints(maxWidth: 500.w),
      color: const Color(0xFF353848),
      radius: BorderRadius.circular(10.w),
      child: Stack(
          alignment: Alignment.center,
          clipBehavior: Clip.none,
          children: [
            giftContent(msg.giftName, msg.giftPrice),
            giftImage(msg.giftImg)
          ]),
    );
  }

  Widget giftContent(String name, int price) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          SizedBox(
            width: 240.w,
            child: FText("送你${msg.giftCount}个$name",
                overflow: TextOverflow.ellipsis, size: 28.sp),
          ),
          Row(mainAxisSize: MainAxisSize.min, children: [
            LoadAssetImage("person/myGold", width: 25.w),
            SizedBox(width: 10.w),
            FText("$price金豆", size: 26.sp, color: const Color(0xFF999999))
          ])
        ]);
  }

  Widget giftImage(String image) {
    return Positioned(
        left: dir == MessageDirection.SEND ? -150.w : null,
        right: dir == MessageDirection.RECEIVE ? -150.w : null,
        child: LoadNetImage(NetConstants.ossPath + image,
            fit: BoxFit.cover, width: 150.w, height: 150.w));
  }
}

class MsgRoomCell extends StatelessWidget {
  final MessageDirection dir;
  final HXRoomMsg msg;

  const MsgRoomCell(this.dir, this.msg, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTapCell,
      child: FContainer(
        height: 243.w,
        color: const Color(0xFFFFE27D),
        width: 500.w,
        margin: EdgeInsets.symmetric(horizontal: 20.w),
        padding: EdgeInsets.symmetric(horizontal: 20.w),
        radius: BorderRadius.circular(10.w),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const FText("分享房间："),
            SizedBox(height: 5.w),
            roomContent()
          ],
        ),
      ),
    );
  }

  Widget roomContent() {
    return FContainer(
      color: Colors.white,
      height: 150.w,
      radius: BorderRadius.circular(10.w),
      padding: EdgeInsets.all(10.w),
      child: Row(children: [
        Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          ConstrainedBox(
            constraints: BoxConstraints(maxWidth: 300.w),
            child: FText(msg.roomName,
                weight: FontWeight.bold,
                overflow: TextOverflow.ellipsis,
                size: 22.sp),
          ),
          ConstrainedBox(
            constraints: BoxConstraints(maxWidth: 300.w),
            child: FText(
                msg.displayId.isEmpty ? "邀请你来一起聊天" : "房间ID：${msg.displayId}",
                maxLines: 10,
                size: 20.sp,
                color: const Color(0xFF666666)),
          )
        ]),
        const Spacer(),
        SizedBox(width: 10.w),
        FContainer(
          width: 125.w,
          radius: BorderRadius.circular(10.w),
          height: 125.w,
          color: const Color(0xFFEEEEEE),
          image: ImageUtils()
              .getImageProvider(NetConstants.ossPath + msg.roomCover),
          imageFit: BoxFit.cover,
        ),
      ]),
    );
  }

  void onTapCell() async {
    if (msg.roomId == 0) return;
    // WidgetUtils().pushPage(RoomPage(msg.roomId, homePageCtr));
    // SharedPreferences preferences = await SharedPreferences.getInstance();
    // preferences.setInt(SharedKey.KEY_ROOM_ID.value, msg.roomId);
    // WidgetUtils().popPage();
    // homePageCtr.nextPage(
    //     duration: const Duration(milliseconds: 200), curve: Curves.linear);
    // homePageCtr.jumpToPage(1);
  }
}

class MsgDynamicCell extends StatelessWidget {
  final MessageDirection dir;
  final HXDynamicMsg msg;

  const MsgDynamicCell(this.dir, this.msg, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTapCell,
      child: FContainer(
        height: 243.w,
        color: const Color(0xFFFFE27D),
        width: 500.w,
        margin: EdgeInsets.symmetric(horizontal: 20.w),
        padding: EdgeInsets.symmetric(horizontal: 20.w),
        radius: BorderRadius.circular(10.w),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const FText("分享动态："),
            SizedBox(height: 5.w),
            dynamicContent()
          ],
        ),
      ),
    );
  }

  Widget dynamicContent() {
    return FContainer(
      color: Colors.white,
      height: 150.w,
      radius: BorderRadius.circular(10.w),
      padding: EdgeInsets.all(10.w),
      child: Row(children: [
        Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Row(children: [
            CircleAvatar(
              radius: 20.w,
              backgroundImage: ImageUtils()
                  .getImageProvider(NetConstants.ossPath + msg.head),
            ),
            SizedBox(width: 10.w),
            ConstrainedBox(
              constraints: BoxConstraints(maxWidth: 240.w),
              child: FText(msg.name,
                  weight: FontWeight.bold,
                  overflow: TextOverflow.ellipsis,
                  size: 22.sp),
            ),
          ]),
          ConstrainedBox(
            constraints: BoxConstraints(maxWidth: 300.w),
            child: FText(msg.describe,
                maxLines: 10, size: 20.sp, color: const Color(0xFF666666)),
          )
        ]),
        const Spacer(),
        SizedBox(width: 10.w),
        getDynamicImg(),
      ]),
    );
  }

  Widget getDynamicImg() {
    int dyType = msg.dyType;
    ImageProvider? image;
    if (dyType == DynamicType.TEXT_IMAGE.index) {
      image =
          ImageUtils().getImageProvider(NetConstants.ossPath + msg.resource);
    } else if (dyType == DynamicType.TEXT_AUDIO.index) {
      image = ImageUtils().getAssetsImage("msg/msg_a");
    } else if (dyType == DynamicType.VOTE_TEXT.index) {
      image = ImageUtils().getAssetsImage("msg/msg_v");
    } else if (dyType == DynamicType.VOTE_IMAGE.index) {
      image = ImageUtils().getAssetsImage("msg/msg_v");
    }

    return FContainer(
      width: 125.w,
      radius: BorderRadius.circular(10.w),
      height: 125.w,
      color: const Color(0xFFEEEEEE),
      image: image,
      imageFit: BoxFit.cover,
    );
  }

  void onTapCell() {
    if (msg.dynamicId == 0) return;
    // WidgetUtils().pushPage(CommentPage(homePageCtr, dynamicId: msg.dynamicId));
  }
}

class MsgClubCell extends StatelessWidget {
  final MessageDirection dir;
  final HXClubMsg msg;

  const MsgClubCell(this.dir, this.msg, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTapCell,
      child: FContainer(
        height: 243.w,
        color: const Color(0xFFFFE27D),
        width: 500.w,
        margin: EdgeInsets.symmetric(horizontal: 20.w),
        padding: EdgeInsets.symmetric(horizontal: 20.w),
        radius: BorderRadius.circular(10.w),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const FText("分享公会："),
            SizedBox(height: 5.w),
            roomContent()
          ],
        ),
      ),
    );
  }

  Widget roomContent() {
    return FContainer(
      color: Colors.white,
      height: 150.w,
      radius: BorderRadius.circular(10.w),
      padding: EdgeInsets.all(10.w),
      child: Row(children: [
        Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          ConstrainedBox(
            constraints: BoxConstraints(maxWidth: 300.w),
            child: FText(msg.clubName,
                weight: FontWeight.bold,
                overflow: TextOverflow.ellipsis,
                size: 22.sp),
          ),
          ConstrainedBox(
            constraints: BoxConstraints(maxWidth: 300.w),
            child: FText("公会ID：${msg.displayId}",
                maxLines: 10, size: 20.sp, color: const Color(0xFF666666)),
          )
        ]),
        const Spacer(),
        SizedBox(width: 10.w),
        FContainer(
          width: 125.w,
          radius: BorderRadius.circular(10.w),
          height: 125.w,
          color: const Color(0xFFEEEEEE),
          image: ImageUtils()
              .getImageProvider(NetConstants.ossPath + msg.clubCover),
          imageFit: BoxFit.cover,
        ),
      ]),
    );
  }

  void onTapCell() {
    // WidgetUtils().pushPage(ClubInfoPage(homePageCtr, clubId: msg.clubId));
  }
}

class MsgGoldCell extends StatelessWidget {
  final MessageDirection dir;
  final HXGoldMsg msg;

  const MsgGoldCell(this.dir, this.msg, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FContainer(
      padding: EdgeInsets.fromLTRB(dir == MessageDirection.SEND ? 100.w : 20.w,
          20.w, dir == MessageDirection.SEND ? 20.w : 100.w, 20.w),
      margin: EdgeInsets.symmetric(horizontal: 20.w),
      height: 160.w,
      constraints: BoxConstraints(maxWidth: 500.w),
      color: const Color(0xFF353848),
      radius: BorderRadius.circular(10.w),
      child: Stack(
          alignment: Alignment.center,
          clipBehavior: Clip.none,
          children: [
            SizedBox(
              width: 240.w,
              child: FText(
                  dir == MessageDirection.SEND
                      ? "赠送${msg.goldNumber}个金豆"
                      : "送你${msg.goldNumber}个金豆",
                  maxLines: 2,
                  size: 28.sp),
            ),
            goldImage()
          ]),
    );
  }

  Widget goldImage() {
    return Positioned(
        left: dir == MessageDirection.SEND ? -150.w : null,
        right: dir == MessageDirection.RECEIVE ? -150.w : null,
        child: LoadAssetImage("dynamic/gold",
            fit: BoxFit.cover, width: 115.w, height: 115.w));
  }
}

class MsgDressCell extends StatelessWidget {
  final MessageDirection dir;
  final HXDressMsg msg;

  const MsgDressCell(this.dir, this.msg, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // ConfigGoods? goods = ConfigManager().getGoods(msg.dressId);
    return FContainer(
      padding: EdgeInsets.fromLTRB(dir == MessageDirection.SEND ? 100.w : 20.w,
          20.w, dir == MessageDirection.SEND ? 20.w : 100.w, 20.w),
      margin: EdgeInsets.symmetric(horizontal: 20.w),
      height: 160.w,
      constraints: BoxConstraints(maxWidth: 500.w),
      color: const Color(0xFF353848),
      radius: BorderRadius.circular(10.w),
      child: Stack(
          alignment: Alignment.center,
          clipBehavior: Clip.none,
          children: [
            SizedBox(
              width: 240.w,
              child: FText("送你1个${msg.day}天的{goods?.title ?? " "}装扮",
                  maxLines: 2, size: 28.sp),
            ),
            // goldImage(goods?.picture ?? "")
          ]),
    );
  }

  Widget goldImage(String image) {
    return Positioned(
        left: dir == MessageDirection.SEND ? -150.w : null,
        right: dir == MessageDirection.RECEIVE ? -150.w : null,
        child: LoadNetImage(NetConstants.ossPath + image,
            fit: BoxFit.cover, width: 115.w, height: 115.w));
  }
}
