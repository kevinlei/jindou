import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class InteractionMsg extends StatefulWidget {
  const InteractionMsg({super.key});

  @override
  State<InteractionMsg> createState() => _InteractionMsgState();
}

class _InteractionMsgState extends State<InteractionMsg> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      padding: EdgeInsets.only(left: 30.w, right: 30.w, bottom: 20.w),
      imageFit: BoxFit.fill,
      imageAlign: Alignment.topCenter,
      image: ImageUtils().getAssetsImage("public/huibg"),
      color: Colors.white,
      child: Column(
        children: const [
          CommonAppBar(
            title: "互动消息",
            textColor: Colors.black,
            backgroundColor: Colors.transparent,
          ),
        ],
      ),
    );
  }
}
