import 'package:echo/import.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SendHbao extends StatefulWidget {
  final String userId;
  final void Function(int gold) onSendHbao;
  const SendHbao(this.userId, this.onSendHbao, {super.key});

  @override
  State<SendHbao> createState() => _SendHbaoState();
}

class _SendHbaoState extends State<SendHbao> {
  int textValue = 0;

  // 节流
  bool isOpen = false;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: FContainer(
        width: 620.w,
        radius: BorderRadius.circular(40.w),
        color: Colors.white,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            FContainer(
              padding: EdgeInsets.symmetric(vertical: 20.w),
              border:
                  const Border(bottom: BorderSide(color: Color(0xFFEBEBEB))),
              align: Alignment.center,
              child: FText(
                "赠送金币",
                size: 32.sp,
                weight: FontWeight.w600,
                color: Colors.black,
              ),
            ),
            SizedBox(height: 30.w),
            FContainer(
              margin: EdgeInsets.symmetric(horizontal: 30.w),
              padding: EdgeInsets.symmetric(vertical: 6.w),
              radius: BorderRadius.circular(20.w),
              color: const Color(0xFF222222).withOpacity(0.05),
              child: Row(
                children: [
                  SizedBox(width: 30.w),
                  Expanded(
                    child: FText(
                      "金币总数",
                      size: 26.sp,
                      color: Colors.black,
                      weight: FontWeight.w600,
                    ),
                  ),
                  Expanded(
                    child: TextField(
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w600,
                        fontSize: 30.h,
                      ),
                      autofocus: true,
                      maxLines: 1,
                      maxLength: 9,
                      textInputAction: TextInputAction.send,
                      keyboardType: TextInputType.number,
                      onChanged: (value) {
                        if (value.isEmpty) {
                          textValue = 0;
                        } else {
                          textValue = int.parse(value);
                        }
                        setState(() {});
                      },
                      inputFormatters: [
                        FilteringTextInputFormatter.allow(RegExp(r'[0-9]'))
                      ],
                      decoration: InputDecoration(
                        isDense: true,
                        contentPadding: EdgeInsets.symmetric(
                            vertical: 18.h, horizontal: 20.w),
                        border: InputBorder.none,
                        hintText: "请输入金币数量",
                        hintStyle: TextStyle(
                          color: const Color(0xFF222222).withOpacity(0.5),
                          fontWeight: FontWeight.w600,
                          fontSize: 30.h,
                        ),
                        counterText: "",
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 15.w),
            FText(
              "我的余额：${UserConstants.userginInfo!.goldInfo.gold}金币",
              size: 24.sp,
              color: const Color(0xFF222222).withOpacity(0.6),
            ),
            SizedBox(height: 30.w),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 30.w),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  FButton(
                    onTap: () => WidgetUtils().popPage(),
                    width: 250.w,
                    height: 80.w,
                    align: Alignment.center,
                    border: Border.all(color: const Color(0xFFFF753F)),
                    child: FText(
                      "取消",
                      color: const Color(0xFFFF753F),
                      size: 30.sp,
                    ),
                  ),
                  FButton(
                    onTap: sendGold,
                    width: 250.w,
                    height: 80.w,
                    align: Alignment.center,
                    isTheme: true,
                    child: FText(
                      "发送",
                      size: 30.sp,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 30.w),
          ],
        ),
      ),
    );
  }

  // 赠送金币
  void sendGold() async {
    if (textValue > (UserConstants.userginInfo!.goldInfo.gold ?? 0)) {
      return WidgetUtils().showToast("您的金币余额不足!");
    }
    if (isOpen) return;
    isOpen = true;
    SystemChannels.textInput.invokeMethod("TextInput.hide");
    await DioUtils().httpRequest(
      NetConstants.sendGold,
      method: DioMethod.POST,
      loading: false,
      params: {
        "obtain_user_id": widget.userId,
        "send_gold_num": textValue,
        "reason": "赠送给${widget.userId}的金币",
        "src": "app"
      },
      onSuccess: (res) {
        UserConstants.userginInfo!.goldInfo.gold =
            UserConstants.userginInfo!.goldInfo.gold! - textValue;
        WidgetUtils().popPage();
        WidgetUtils().showToast("赠送成功");
        widget.onSendHbao(textValue);
      },
      onError: (code, msg) => isOpen = false,
    );
  }
}
