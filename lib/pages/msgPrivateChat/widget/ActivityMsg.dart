import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class ActivityMsg extends StatefulWidget {
  const ActivityMsg({super.key});

  @override
  State<ActivityMsg> createState() => _ActivityMsgState();
}

class _ActivityMsgState extends State<ActivityMsg> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      padding: EdgeInsets.only(left: 30.w, right: 30.w, bottom: 20.w),
      imageFit: BoxFit.fill,
      imageAlign: Alignment.topCenter,
      image: ImageUtils().getAssetsImage("public/huibg"),
      color: Colors.white,
      child: Column(
        children: const [
          CommonAppBar(
            title: "活动公告",
            textColor: Colors.black,
            backgroundColor: Colors.transparent,
          ),
        ],
      ),
    );
  }
}
