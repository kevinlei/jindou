import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class AdminMsg extends StatefulWidget {
  const AdminMsg({super.key});

  @override
  State<AdminMsg> createState() => _AdminMsgState();
}

class _AdminMsgState extends State<AdminMsg> {
  // 系统通知
  List adminMsgList = [];

  @override
  void initState() {
    super.initState();
    init();
  }

  void init() async {
    adminMsgList = await AppManager().adminMessageList();
    AppManager().adminunread = 0;
    AppManager().messageRead(2);
    if (!mounted) return;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      padding: EdgeInsets.only(left: 30.w, right: 30.w, bottom: 20.w),
      imageFit: BoxFit.fill,
      imageAlign: Alignment.topCenter,
      image: ImageUtils().getAssetsImage("public/huibg"),
      color: Colors.white,
      child: Column(
        children: [
          const CommonAppBar(
            title: "官方消息",
            textColor: Colors.black,
            backgroundColor: Colors.transparent,
          ),
          Expanded(child: list())
        ],
      ),
    );
  }

  Widget list() {
    return FRefreshLayout(
      firstRefresh: false,
      onRefresh: onRefresh,
      onLoad: adminMsgList.isNotEmpty &&
              AppManager().adminMsgList.length < AppManager().adminListNum
          ? callLoad
          : null,
      child: ListView.builder(
        padding: EdgeInsets.symmetric(horizontal: 30.w, vertical: 0),
        itemBuilder: (context, index) => item(adminMsgList[index], index),
        itemCount: adminMsgList.length,
      ),
    );
  }

  Widget item(data, int index) {
    int startTimer = index == 0 ? adminMsgList[0]["create_time"] : 0;
    int endTimer = index == 0 ? 0 : adminMsgList[index - 1]["create_time"];

    return Padding(
      padding: EdgeInsets.only(top: 30.w),
      child: Column(
        children: [
          timeWidget(data["create_time"], startTimer, endTimer),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              LoadAssetImage(
                "msgPrivateChat/systemNotifications",
                width: 100.w,
                height: 100.w,
              ),
              SizedBox(width: 16.w),
              Expanded(
                child: FContainer(
                  color: const Color(0xFF222222).withOpacity(0.1),
                  padding:
                      EdgeInsets.symmetric(horizontal: 20.w, vertical: 30.w),
                  radius: BorderRadius.only(
                    bottomLeft: Radius.circular(20.w),
                    bottomRight: Radius.circular(20.w),
                    topRight: Radius.circular(20.w),
                  ),
                  child: FText(
                    data["content"],
                    maxLines: 200,
                    size: 28.sp,
                    color: Colors.black,
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  Widget timeWidget(int msgTime, int startTime, int lastTime) {
    if (msgTime == 0) return const SizedBox();

    DateTime msgDate = DateTime.fromMillisecondsSinceEpoch(msgTime);
    DateTime lastDate = DateTime.now();
    if (lastTime != 0) {
      lastDate = DateTime.fromMillisecondsSinceEpoch(lastTime);
    }

    // 如果消息时间距离上一条消息间隔大于1小时就显示消息时间
    String timeText = "";
    if (msgDate.difference(lastDate).inMinutes > 5) {
      if (DateTime.now().difference(msgDate).inHours < 24 &&
          (DateTime.now().day == msgDate.day)) {
        timeText = (msgDate.hour > 12 ? "下午" : "上午") +
            msgDate.toString().substring(11, 16);
      } else if (DateTime.now().difference(msgDate).inHours < 48 &&
          (DateTime.now().day - msgDate.day == 1)) {
        timeText = "昨天${msgDate.toString().substring(11, 16)}";
      } else {
        timeText = msgDate.toString().substring(0, 16);
      }
    } else if (startTime > 0) {
      if (DateTime.now().difference(msgDate).inHours < 24 &&
          (DateTime.now().day == msgDate.day)) {
        timeText = (msgDate.hour > 12 ? "下午" : "上午") +
            msgDate.toString().substring(11, 16);
      } else if (DateTime.now().difference(msgDate).inHours < 48 &&
          (DateTime.now().day - msgDate.day == 1)) {
        timeText = "昨天${msgDate.toString().substring(11, 16)}";
      } else {
        timeText = msgDate.toString().substring(0, 16);
      }
    }
    return Visibility(
      visible: timeText.isNotEmpty,
      child: Padding(
        padding: EdgeInsets.only(bottom: 15.w),
        child: FText(timeText, size: 26.sp, color: Colors.grey),
      ),
    );
  }

  Future<void> onRefresh() async {
    AppManager().adminPageIndex = 1;
    adminMsgList = await AppManager().adminMessageList(isInit: false);
    if (!mounted) return;
    setState(() {});
  }

  Future<void> callLoad() async {
    AppManager().adminPageIndex += 1;
    adminMsgList = await AppManager().adminMessageList(isLoad: true);
    if (!mounted) return;
    setState(() {});
  }
}
