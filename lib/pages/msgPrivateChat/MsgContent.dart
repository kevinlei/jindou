import 'package:echo/import.dart';
import 'package:flutter/material.dart';
import 'package:im_flutter_sdk/im_flutter_sdk.dart';

class MsgContent extends StatefulWidget {
  final EMMessage msg;
  final int startTime;
  final int lastTime;
  final HxMsgExt msgExt;
  final bool isRead;

  const MsgContent(
      this.msg, this.startTime, this.lastTime, this.msgExt, this.isRead,
      {Key? key})
      : super(key: key);

  @override
  _MsgContentState createState() => _MsgContentState();
}

class _MsgContentState extends State<MsgContent> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void didUpdateWidget(covariant MsgContent oldWidget) {
    if (oldWidget.isRead != widget.isRead && mounted) {
      if (oldWidget.isRead) {
        widget.msg.hasReadAck = true;
      }
      setState(() {});
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    widget.msg.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(mainAxisSize: MainAxisSize.min, children: [
      SizedBox(height: 10.h),
      timeWidget(),
      contentWidget(),
      SizedBox(height: 10.h),
    ]);
  }

  // 当前用户发送的信息接收到回调更改
  Widget markscont() {
    if (widget.msg.direction == MessageDirection.SEND) {
      return tuWidget(widget.msg.hasReadAck);
    }

    return const FText("");
  }

  Widget tuWidget(bool? hasRead) {
    return FContainer(
      height: 65.w,
      child: Column(
        children: [
          const Spacer(),
          LoadAssetImage(
              hasRead == true
                  ? "msgPrivateChat/msgChat/reblstes"
                  : "msgPrivateChat/msgChat/restap",
              width: 20.w),
        ],
      ),
    );
  }

  Widget contentWidget() {
    List<Widget> children = [
      msgHead(),
      msgWidget(),
      markscont(),
      const Spacer()
    ];
    if (widget.msg.direction == MessageDirection.SEND) {
      children = children.reversed.toList();
    } else {
      children = [msgHead(), msgWidget(), const Spacer()];
    }
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: children,
    );
  }

  Widget msgHead() {
    return GestureDetector(
      onTap: onPressHead,
      child: CircleAvatar(
        radius: 40.w,
        backgroundColor: Colors.grey,
        backgroundImage: getMsgHead(),
      ),
    );
  }

  ImageProvider? getMsgHead() {
    try {
      String sendId = widget.msg.attributes!["sendUser"] ?? "";
      if (sendId.isEmpty) return null;
      String head = "";
      if (sendId == UserConstants.userginInfo?.info.userId) {
        head = UserConstants.userginInfo?.info.headIcon ?? "";
      } else {
        head = widget.msgExt.toHead;
      }
      return ImageUtils().getImageProvider(NetConstants.ossPath + head);
    } catch (e) {
      return null;
    }
  }

  Widget msgWidget() {
    try {
      if (widget.msg.body.type == MessageType.TXT) {
        EMTextMessageBody msgBody = widget.msg.body as EMTextMessageBody;
        HxReceiveMsg receiveMsg = HxReceiveMsg(msgBody.content);
        switch (receiveMsg.msgType) {
          case HXMsgType.TEXT:
            HXTextMsg msgBody = receiveMsg.msgBody as HXTextMsg;
            return MsgTextCell(widget.msg.direction, msgBody);
          case HXMsgType.IMAGE:
            HXImageMsg msgBody = receiveMsg.msgBody as HXImageMsg;
            return MsgImgCell(msgBody);
          case HXMsgType.LOC:
            return const SizedBox();
          case HXMsgType.AUDIO:
            HXAudioMsg msgBody = receiveMsg.msgBody as HXAudioMsg;
            return MsgAudioCell(widget.msg.direction, msgBody);
          case HXMsgType.HBAO:
            HXHbaoMsg msgBody = receiveMsg.msgBody as HXHbaoMsg;
            return MsgHbaoCell(widget.msg.direction, msgBody);
          case HXMsgType.VIDEO:
            return const SizedBox();
          case HXMsgType.DYNAMIC:
            HXDynamicMsg msgBody = receiveMsg.msgBody as HXDynamicMsg;
            return MsgDynamicCell(widget.msg.direction, msgBody);
          case HXMsgType.GIFT:
            HXGiftMsg msgBody = receiveMsg.msgBody as HXGiftMsg;
            return MsgGiftCell(widget.msg.direction, msgBody);
          case HXMsgType.ROOM:
            HXRoomMsg msgBody = receiveMsg.msgBody as HXRoomMsg;
            return MsgRoomCell(widget.msg.direction, msgBody);
          case HXMsgType.GOLD:
            HXGoldMsg msgBody = receiveMsg.msgBody as HXGoldMsg;
            return MsgGoldCell(widget.msg.direction, msgBody);
          case HXMsgType.CLUB:
            HXClubMsg msgBody = receiveMsg.msgBody as HXClubMsg;
            return MsgClubCell(widget.msg.direction, msgBody);
          case HXMsgType.DRESS:
            HXDressMsg msgBody = receiveMsg.msgBody as HXDressMsg;
            return MsgDressCell(widget.msg.direction, msgBody);
        }
      }
      return const FText("");
    } catch (e) {
      debugPrint('解析异常$e');
      return const FText("");
    }
  }

  Widget timeWidget() {
    int msgTime = widget.msg.localTime;
    if (msgTime == 0) return const SizedBox();

    DateTime msgDate = DateTime.fromMillisecondsSinceEpoch(msgTime);
    DateTime lastDate = DateTime.now();

    if (widget.lastTime != 0) {
      lastDate = DateTime.fromMillisecondsSinceEpoch(widget.lastTime);
    }
    // 如果消息时间距离上一条消息间隔大于1小时就显示消息时间
    String timeText = "";
    if (msgDate.difference(lastDate).inMinutes > 5) {
      if (DateTime.now().difference(msgDate).inHours < 24 &&
          (DateTime.now().day == msgDate.day)) {
        timeText = (msgDate.hour > 12 ? "下午" : "上午") +
            msgDate.toString().substring(11, 16);
      } else if (DateTime.now().difference(msgDate).inHours < 48 &&
          (DateTime.now().day - msgDate.day == 1)) {
        timeText = "昨天${msgDate.toString().substring(11, 16)}";
      } else {
        timeText = msgDate.toString().substring(0, 16);
      }
    } else if (widget.startTime > 0) {
      if (DateTime.now().difference(msgDate).inHours < 24 &&
          (DateTime.now().day == msgDate.day)) {
        timeText = (msgDate.hour > 12 ? "下午" : "上午") +
            msgDate.toString().substring(11, 16);
      } else if (DateTime.now().difference(msgDate).inHours < 48 &&
          (DateTime.now().day - msgDate.day == 1)) {
        timeText = "昨天${msgDate.toString().substring(11, 16)}";
      } else {
        timeText = msgDate.toString().substring(0, 16);
      }
    }
    return Visibility(
        visible: timeText.isNotEmpty,
        child: FText(timeText, size: 26.sp, color: Colors.grey));
  }

  void onPressHead() {}
}
