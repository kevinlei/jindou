import 'dart:async';
import 'dart:io';
import 'package:echo/import.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:im_flutter_sdk/im_flutter_sdk.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';

class MsgChat extends StatefulWidget {
  final String hxId;
  final HxMsgExt msgExt;
  final bool isRoom;
  final String msgId;
  const MsgChat(
    this.hxId,
    this.msgExt, {
    super.key,
    this.isRoom = false,
    this.msgId = "",
  });

  @override
  State<MsgChat> createState() => _MsgChatState();
}

class _MsgChatState extends State<MsgChat> {
  ScrollController scroll = ScrollController();
  TextEditingController textController = TextEditingController();
  // 获取卡片组件高度
  GlobalKey cardKey = GlobalKey();
  // 节流
  bool openCellFresh = true;

  // 用户信息
  UserRoomInfo? userInfo;

  // 当前会话
  EMConversation? con;

  // 消息列表
  List<EMMessage> msgList = [];
  // 搜索的起始消息 ID
  String startId = "";

  // 当前打开operationPanel(emo/表情、input/键盘)
  String currentPanel = "";

  // 是否点击语音按钮
  bool isAudio = false;

  // 获取取消发送按钮位置
  GlobalKey anchorKey = GlobalKey();
  Offset offset = const Offset(0, 0);

  // 是否进入取消按钮
  bool isCanAudio = false;
  // 是否开始录入语音
  bool isInputAudio = false;
  // 录音时长
  int maxTime = 10, audioTime = 0;

  // 节流
  Timer? timer;

  // 是否反向
  bool isdown = false;

  // 是否消息已读
  bool isRead = false;

  @override
  void initState() {
    super.initState();
    if (!mounted) return;
    // 添加收消息监听
    EMClient.getInstance.chatManager.addEventHandler(
      'MsgChatPage',
      EMChatEventHandler(
        onMessagesReceived: (List<EMMessage> list) async {
          for (EMMessage e in list) {
            if (e.conversationId != widget.hxId ||
                e.chatType != ChatType.Chat) {
              continue;
            }
            EMClient.getInstance.chatManager
                .sendMessageReadAck(e)
                .then((value) {});
            EMClient.getInstance.chatManager
                .sendConversationReadAck(e.conversationId!)
                .then((value) {});
            msgList.add(e);
          }
          scrollToEnd();
          setState(() {});
          // 異步通知消息已讀
          Future.delayed(const Duration(seconds: 0), () async {
            EMConversation? con = await EMClient.getInstance.chatManager
                .getConversation(widget.hxId);

            if (con == null) return;
            con.markAllMessagesAsRead();
          });
        },
        //消息发送方已读回执
        onConversationRead: (from, to) {
          if (!isRead) {
            isRead = true;
            setState(() {});
          }
        },
      ),
    );

    //  监听消息列表滚动到顶部刷新数据
    scroll.addListener(() async {
      if (msgList.isNotEmpty && msgList.length >= 20) {
        RenderBox renderBox =
            cardKey.currentContext?.findRenderObject() as RenderBox;
        if ((scroll.offset + renderBox.size.height + 20.w) >
            scroll.position.maxScrollExtent) {
          if (!openCellFresh) return;
          openCellFresh = false;
          await loadConMsg();
          Future.delayed(const Duration(milliseconds: 600), () async {
            openCellFresh = true;
          });
        }
      }
    });

    // 初始化
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      startId = widget.msgId;
      con = await EMClient.getInstance.chatManager.getConversation(widget.hxId);
      // 初始化用户信息
      initUser();
      // 初始化消息
      initMsg(widget.msgExt);
    });
  }

  @override
  void dispose() {
    EMClient.getInstance.chatManager.removeEventHandler('MsgChatPage');
    scroll.removeListener(() {});
    scroll.dispose();
    textController.dispose();
    super.dispose();
  }

  // 获取用户信息
  void initUser() async {
    await DioUtils().httpRequest(
      NetConstants.userRoomInfo,
      loading: false,
      params: {"user_id": widget.msgExt.toUser},
      onSuccess: (res) {
        if (res == null) return;
        userInfo = UserRoomInfo.fromJson(res);
        if (!mounted) return;
        setState(() {});
      },
    );
  }

  // 初始化消息列表
  void initMsg(HxMsgExt msgExt) async {
    try {
      if (con == null) return;
      con!.markAllMessagesAsRead();
      List<EMMessage> list = await con!.loadMessagesFromTime(
        startTime: 1682870400000,
        endTime: DateTime.now().millisecondsSinceEpoch,
        count: 20,
      );
      if (list.isNotEmpty) {
        isdown = list.length > 15;
        startId = list.first.msgId;
        for (EMMessage e in list) {
          EMClient.getInstance.chatManager
              .sendMessageReadAck(e)
              .then((value) {});
        }
        msgList.insertAll(0, list);
        scrollToEnd();
        if (!mounted) return;
        setState(() {});
      }
    } catch (err) {
      debugPrint('初始化私聊頁面失敗$err');
    }
  }

  void scrollToEnd() async {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      if (scroll.hasClients) {
        openCellFresh = false;
        double max = isdown
            ? scroll.position.minScrollExtent
            : scroll.position.maxScrollExtent;
        scroll.animateTo(max,
            duration: const Duration(milliseconds: 350), curve: Curves.linear);
        Future.delayed(const Duration(milliseconds: 350), () {
          double max2 = isdown
              ? scroll.position.minScrollExtent
              : scroll.position.maxScrollExtent;
          scroll.animateTo(max2,
              duration: const Duration(milliseconds: 100),
              curve: Curves.linear);
          openCellFresh = true;
        });
      }
    });
  }

  Future<void> loadConMsg() async {
    try {
      if (con == null) return;
      List<EMMessage> list = await con!
          .loadMessages(startMsgId: startId, loadCount: AppConstants.pageSize);
      if (list.isNotEmpty) {
        startId = list.first.msgId;
        msgList.insertAll(0, list);
        setState(() {});
      }
    } on EMError catch (e) {
      debugPrint('获取会话消息失败: $e');
    }
  }

  ///-------------------------------------------------录音相关----------------------------------------------///
  // 开始录入语音
  void onInputAudio(PointerDownEvent d) async {
    try {
      await AppManager().soundRecorder.startRecorder(
          toFile: AppManager().recordPath,
          codec: Platform.isAndroid ? Codec.aacADTS : Codec.pcm16WAV,
          bitRate: 8000,
          numChannels: 1,
          sampleRate: 44100);
      onRecorderListener();
      setState(() {
        isInputAudio = true;
      });
    } on Exception catch (err) {
      onEndInputAudio(null);
      debugPrint("录音出错：${err.toString()}");
    }
  }

  // 录音监听
  void onRecorderListener() {
    AppManager().cancelStream();
    AppManager().recorderStream =
        AppManager().soundRecorder.onProgress!.listen((e) {
      int time = e.duration.inMilliseconds;
      DateTime date = DateTime.fromMillisecondsSinceEpoch(time, isUtc: true);
      audioTime = date.second;
      debugPrint("录音时长：$audioTime");
      // if (date.second >= maxTime) {
      //   return onEndInputAudio(null);
      // }
    });
  }

  // 判断用户是否滑动到取消发送语音按钮
  void onUpdetaInputAudio(PointerMoveEvent d) {
    try {
      RenderBox renderBox =
          anchorKey.currentContext?.findRenderObject() as RenderBox;
      Offset offset = renderBox.localToGlobal(Offset.zero);
      if (d.position.dy >= offset.dy &&
          d.position.dy <= (offset.dy + renderBox.size.height) &&
          d.position.dx >= offset.dx &&
          d.position.dx <= (offset.dx + renderBox.size.width)) {
        if (!isCanAudio) {
          setState(() {
            isCanAudio = true;
          });
        }
      } else if (isCanAudio) {
        setState(() {
          isCanAudio = false;
        });
      }
    } catch (err) {}
  }

  // 录音结束
  void onEndInputAudio(PointerUpEvent? d) async {
    try {
      await AppManager().soundRecorder.stopRecorder();
      if (isCanAudio) {
        setState(() {
          isInputAudio = false;
          isCanAudio = false;
        });
      } else {
        setState(() {
          isInputAudio = false;
        });
      }
      if (audioTime < 1) {
        return WidgetUtils().showToast("录音时长太短，请重新录制");
      }
      if (audioTime > maxTime) {
        return WidgetUtils().showToast("录音时长太长，请重新录制");
      }

      onSendAudio();
    } on Exception catch (err) {
      debugPrint("停止录音错误：$err");
    } finally {
      AppManager().cancelStream();
    }
  }

  ///-------------------------------------------------------------------------------------------

  // 点击表情或者输入框
  void openOperationPanel(String current) {
    currentPanel = current;
    setState(() {});
    if (currentPanel == "" || currentPanel == "hbao") {
      FocusManager.instance.primaryFocus?.unfocus();
    }
    if (current != "input") {
      SystemChannels.textInput.invokeMethod("TextInput.hide");
    }
    scrollToEnd();
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      imageFit: BoxFit.fill,
      imageAlign: Alignment.topCenter,
      image: ImageUtils().getAssetsImage("public/huibg"),
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.transparent,
        appBar: appBar(),
        body: Stack(
          alignment: AlignmentDirectional.bottomCenter,
          children: [
            Column(
              children: [
                Visibility(
                    visible: !widget.isRoom &&
                        userInfo != null &&
                        userInfo!.roomInfo.exteriorRoomId != 0 &&
                        userInfo!.roomInfo.exteriorRoomId !=
                            AppManager().roomInfo.roomMode?.exteriorRoomId,
                    child: stepOnTheRoom()),
                Expanded(
                    child: GestureDetector(
                  onTapDown: (d) => openOperationPanel(""),
                  child: SingleChildScrollView(
                      controller: scroll,
                      reverse: isdown,
                      child: Column(
                        children: [
                          SizedBox(height: 20.w),
                          Padding(
                            key: cardKey,
                            padding: EdgeInsets.all(20.w),
                            child: card(),
                          ),
                          msgListWdg(),
                        ],
                      )),
                )),
                msgChatFooter(),
              ],
            ),
            AnimatedSwitcher(
                duration: const Duration(milliseconds: 300),
                child: isInputAudio ? inputAudio() : const SizedBox())
          ],
        ),
      ),
    );
  }

  CommonAppBar appBar() {
    return CommonAppBar(
      titleChild: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          FText(
            widget.msgExt.toName,
            size: 36.sp,
            color: Colors.black,
            weight: FontWeight.bold,
          ),
          SizedBox(width: 5.w),
          FContainer(
            padding: EdgeInsets.symmetric(vertical: 4.w, horizontal: 8.w),
            color: Colors.white,
            radius: BorderRadius.circular(20.w),
            align: Alignment.center,
            child: FText(
              userInfo?.info.online == 20 ? "在线" : "离线",
              size: 20.sp,
              color: const Color(0xFF00CCB8),
            ),
          )
        ],
      ),
      actions: [
        GestureDetector(
          onTap: () async {
            await WidgetUtils().pushPage(MsgSetUp(widget.msgExt, con));
            initMsg(widget.msgExt);
          },
          child: LoadAssetImage(
            "public/more",
            width: 60.w,
            height: 60.w,
            color: Colors.black,
          ),
        )
      ],
    );
  }

  Widget stepOnTheRoom() {
    return FContainer(
      padding: EdgeInsets.symmetric(vertical: 20.w, horizontal: 30.w),
      color: const Color(0xFFFEE0F0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          FText(
            "我正在房间中,一起来上麦吧！",
            size: 28.sp,
            color: const Color(0xFFFF753F),
          ),
          GestureDetector(
            onTap: () {
              // WidgetUtils()
              //     .pushPage(RoomPage(userInfo!.roomInfo.exteriorRoomId));
              AppManager().joinRoom(userInfo!.roomInfo.exteriorRoomId);
            },
            child: FContainer(
              padding: REdgeInsets.symmetric(vertical: 20.w, horizontal: 50.w),
              radius: BorderRadius.circular(40.w),
              border: Border.all(color: const Color(0xFFFF753F)),
              child: FText(
                "踩房间",
                size: 28.sp,
                color: const Color(0xFFFF753F),
              ),
            ),
          )
        ],
      ),
    );
  }

  ///--------------------------------------------------------卡片--------------------------------------------------///
  Widget card() {
    return FContainer(
      width: double.infinity,
      padding: EdgeInsets.all(30.w),
      radius: BorderRadius.circular(20.w),
      imageFit: BoxFit.cover,
      image: ImageUtils().getAssetsImage("msgPrivateChat/msgChat/cardBg"),
      child: Column(
        children: [
          cardHerder(),
          SizedBox(height: 10.w),
          cardBody(),
          SizedBox(height: 20.w),
          SizedBox(
            height: 130.w,
            child: cardfouter(),
          ),
        ],
      ),
    );
  }

  Widget cardHerder() {
    return SizedBox(
      height: 120.w,
      child: Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
        SizedBox(
          width: 120.w,
          height: 120.w,
          child: CircleAvatar(
            backgroundColor: Colors.white,
            backgroundImage: ImageUtils()
                .getImageProvider(NetConstants.ossPath + widget.msgExt.toHead),
          ),
        ),
        SizedBox(width: 20.w),
        Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            FText(
              widget.msgExt.toName,
              size: 32.sp,
              weight: FontWeight.bold,
            ),
            FContainer(
              padding: EdgeInsets.symmetric(vertical: 5.w, horizontal: 10.w),
              radius: BorderRadius.circular(50),
              color: Colors.white.withOpacity(0.3),
              child:
                  FText("ID: ${userInfo?.info.displayId ?? ""}", size: 20.sp),
            ),
            Row(
              children: [
                GenderAge(
                    userInfo?.info.sex == 1, userInfo?.info.birthday ?? 0),
                SizedBox(width: 5.w),
                ConfigManager().userWealthCharmIcon(
                    userInfo?.gradeInfo.userGrade.wealth.badge ?? "",
                    userInfo?.gradeInfo.userGrade.wealth.grade ?? 0,
                    marginRight: 5.w,
                    isWealth: true),
                ConfigManager().userWealthCharmIcon(
                    userInfo?.gradeInfo.userGrade.charm.badge ?? "",
                    userInfo?.gradeInfo.userGrade.charm.grade ?? 0,
                    marginRight: 5.w),
              ],
            )
          ],
        ),
        const Spacer(),
        GestureDetector(
          onTap: () {
            WidgetUtils().pushPage(
                PersonHomepage(userId: userInfo?.info.userId ?? ""),
                fun: () => PersonProvider().callRefresh());
          },
          child: FContainer(
            padding: EdgeInsets.all(14.w),
            radius: BorderRadius.circular(50),
            color: Colors.white.withOpacity(0.3),
            child: FText(
              "查看主页",
              size: 27.sp,
            ),
          ),
        )
      ]),
    );
  }

  Widget cardBody() {
    return FContainer(
      width: double.infinity,
      image: ImageUtils()
          .getAssetsImage("msgPrivateChat/msgChat/cardIntroduction"),
      imageFit: BoxFit.fill,
      padding:
          EdgeInsets.only(top: 30.w, left: 20.w, right: 20.w, bottom: 16.w),
      margin: EdgeInsets.symmetric(horizontal: 4.w),
      child: FText(
        userInfo?.info.sign ?? "",
        size: 25.sp,
        overflow: TextOverflow.ellipsis,
      ),
    );
  }

  Widget cardfouter() {
    int length = userInfo?.info.photos?.length ?? 0;
    return Row(
      children: List.generate(
        length > 4 ? 4 : length,
        (index) => FContainer(
          width: 130.w,
          height: 130.w,
          margin: EdgeInsets.only(right: 23.w),
          radius: BorderRadius.circular(10.w),
          image: ImageUtils().getImageProvider(
              NetConstants.ossPath + userInfo!.info.photos![index]),
          imageFit: BoxFit.cover,
        ),
      ),
    );
  }

  ///-------------------------------------消息列表---------------------------------------///
  Widget msgListWdg() {
    return FContainer(
      padding: EdgeInsets.symmetric(horizontal: 20.w),
      child: Column(
        children: List.generate(msgList.length, (index) {
          int lastTime = 0;
          int startTime = 0;
          if (index > 0) {
            lastTime = msgList[index - 1].localTime;
          } else {
            startTime = msgList[0].serverTime;
          }
          if (isRead) {
            msgList[index].hasReadAck = true;
          }
          return MsgContent(
              msgList[index], startTime, lastTime, widget.msgExt, isRead);
        }),
      ),
    );
  }

  Widget msgChatFooter() {
    return FContainer(
      padding: EdgeInsets.only(
          top: 20.w,
          left: 40.w,
          right: 40.w,
          bottom: ScreenUtil().bottomBarHeight + 20.w),
      color: Colors.white,
      child: Column(
        children: [
          input(),
          SizedBox(height: 20.w),
          Visibility(
              visible: currentPanel.isEmpty || currentPanel == "input",
              child: ability()),
          operationPanel(),
        ],
      ),
    );
  }

  Widget input() {
    return SizedBox(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          GestureDetector(
            onTap: () async {
              bool auth =
                  await FuncUtils().requestPermission(Permission.microphone);
              if (!auth) return;
              isAudio = !isAudio;
              setState(() {});
            },
            child: LoadAssetImage(
              "msgPrivateChat/msgChat/${isAudio ? "keyWord" : "voiceInput"}",
              width: 80.w,
            ),
          ),
          SizedBox(width: 15.w),
          Expanded(
            child: FContainer(
              padding: EdgeInsets.symmetric(horizontal: 20.w),
              radius: BorderRadius.circular(50),
              color: const Color(0xFFF4F4F4),
              child: !isAudio
                  ? RawKeyboardListener(
                      // 输入框
                      focusNode: FocusNode(),
                      onKey: (event) {
                        if (event.runtimeType == RawKeyDownEvent) {
                          if (event.logicalKey.keyId == 4294967309) {
                            onSendText();
                          }
                        }
                      },
                      child: TextField(
                        onTap: () => openOperationPanel("input"),
                        onSubmitted: (v) => onSendText(),
                        style: TextStyle(color: Colors.black, fontSize: 35.h),
                        maxLines: 1,
                        textInputAction: TextInputAction.send,
                        controller: textController,
                        decoration: InputDecoration(
                          isDense: true,
                          contentPadding: EdgeInsets.symmetric(
                              vertical: 18.h, horizontal: 20.w),
                          border: InputBorder.none,
                          hintText: "请输入消息",
                          hintStyle: TextStyle(
                            color: const Color(0xFF222222).withOpacity(0.4),
                            fontSize: 35.h,
                          ),
                          counterText: "",
                        ),
                      ),
                    )
                  : SizedBox(
                      height: 80.w,
                      child: Listener(
                        // 录音按钮
                        onPointerDown: onInputAudio,
                        onPointerMove: onUpdetaInputAudio,
                        onPointerUp: onEndInputAudio,
                        child: FContainer(
                          width: double.infinity,
                          height: double.infinity,
                          align: Alignment.center,
                          child: FText(
                            "按住 说话",
                            size: 28.sp,
                            color: Colors.black,
                            weight: FontWeight.w500,
                          ),
                        ),
                      ),
                    ),
            ),
          ),
          Visibility(visible: !isAudio, child: SizedBox(width: 10.w)),
          AnimatedSwitcher(
            duration: const Duration(milliseconds: 300),
            child: isAudio
                ? const SizedBox()
                : SizedBox(
                    height: 80.w,
                    child: Row(
                      children: [
                        Visibility(
                          visible: UserConstants
                                  .userginInfo!.info.transferAccountPower ==
                              1,
                          child: GestureDetector(
                            onTap: () {
                              openOperationPanel("hbao");
                              WidgetUtils().showFDialog(
                                  SendHbao(widget.msgExt.toUser, onSendHbao));
                            },
                            child: LoadAssetImage(
                                "msgPrivateChat/hbao/hbaoEmo_2",
                                width: 60.w),
                          ),
                        ),
                        SizedBox(width: 5.w),
                        FButton(
                          onTap: onSendText,
                          padding: EdgeInsets.symmetric(
                              vertical: 10.w, horizontal: 30.w),
                          isTheme: true,
                          child: FText(
                            "发送",
                            size: 28.sp,
                          ),
                        )
                      ],
                    ),
                  ),
          )
        ],
      ),
    );
  }

  Widget ability() {
    List<String> imgs = [];
    List<void Function()> anys = [];
    if (!(UserConstants.userginInfo?.info.teenmode == 1)) {
      imgs = ["photo", "photograph", "gift", "emo"];
      anys = [
        onTapPhoto,
        onTapCamera,
        onTapGift,
        () => openOperationPanel("emo")
      ];
    } else {
      imgs = ["photo", "photograph", "emo"];
      anys = [onTapPhoto, onTapCamera, () => openOperationPanel("emo")];
    }
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: List.generate(
          imgs.length,
          (index) => GestureDetector(
                onTap: anys[index],
                child: LoadAssetImage(
                  "msgPrivateChat/msgChat/${imgs[index]}",
                  width: imgs[index] != "emo" ? 80.w : 65.w,
                ),
              )),
    );
  }

  Widget operationPanel() {
    Widget child;
    double height = MediaQuery.of(context).viewInsets.bottom;
    switch (currentPanel) {
      case "emo":
        height = 500.w;
        child = EmotionFooter(textController);
        break;
      case "hbao":
        height = 0;
        child = const SizedBox();
        break;
      default:
        child = const SizedBox();
    }
    return SizedBox(
      height: height,
      child: child,
    );
  }

  Widget inputAudio() {
    return FContainer(
      width: double.infinity,
      height: 600.w,
      padding: EdgeInsets.all(40.w),
      gradient: [Colors.white.withOpacity(0), Colors.white.withOpacity(1)],
      gradientBegin: Alignment.topCenter,
      gradientEnd: Alignment.bottomCenter,
      child: Column(
        children: [
          FText(
            !isCanAudio ? "松手发送，上滑取消" : "松手取消发送",
            size: 30.sp,
            color: !isCanAudio ? Colors.black : const Color(0xFFF8335E),
          ),
          SizedBox(height: 20.w),
          FContainer(
            key: anchorKey,
            width: 130.w,
            height: 130.w,
            color: Color(!isCanAudio ? 0xFFE7E8E8 : 0xFFF8335E),
            radius: BorderRadius.circular(50),
            child: Icon(Icons.close, size: 48.sp, color: Colors.black),
          ),
          SizedBox(height: 70.w),
          FContainer(
            width: double.infinity,
            height: 90.w,
            radius: BorderRadius.circular(50),
            gradient: const [
              Color(0xFF96FCFC),
              Color(0xFFFFA7FB),
              Color(0xFFFF82B2),
            ],
            gradientBegin: Alignment.topLeft,
            gradientEnd: Alignment.bottomRight,
            align: Alignment.center,
            child: ScrollText(
              maxWidth: 600.w,
              TextSpan(
                style: const TextStyle(
                  color: Colors.white,
                  letterSpacing: 3,
                  fontWeight: FontWeight.w900,
                ),
                children: textSpanList(),
              ),
            ),
          )
        ],
      ),
    );
  }

  List<TextSpan> textSpanList() {
    List<TextSpan> list = [];
    for (var i = 0; i < 50; i++) {
      List<TextSpan> list2 = [
        TextSpan(
          text: '|',
          style: TextStyle(
            fontSize: 22.sp,
          ),
        ),
        const TextSpan(
          text: '|',
        ),
      ];
      list.addAll(list2);
    }
    return list;
  }

  // 判断用户是否可以发送私信消息
  Future<bool> canSendChat({
    required int type,
    String content = "",
    String filePath = "",
  }) async {
    bool isAccou = false;
    await DioUtils().httpRequest(
      NetConstants.sendMsg,
      method: DioMethod.POST,
      loading: false,
      params: {
        "receiver_id": widget.msgExt.toUser,
        "receiver_name": widget.msgExt.toName,
        "content_type": type,
        "content": content,
        "file_path": filePath,
        "longitude": 0,
        "latitude": 0,
        "address": "",
      },
      onSuccess: (response) {
        isAccou = true;
        isRead = false;
      },
    );
    timer?.cancel();
    timer = null;
    return isAccou;
  }

  // 发送文字消息
  void onSendText() async {
    if (timer != null) return;
    timer = Timer(const Duration(seconds: 1), () {
      timer!.cancel();
      timer = null;
    });

    List<EMMessage> list = msgList;
    String text = textController.text.trimRight();
    if (text.isEmpty) return WidgetUtils().showToast("请输入内容!");

    if (!await canSendChat(type: 1, content: text)) return;
    textController.clear();

    HXTextMsg msgBody = HXTextMsg(text);
    EMMessage message =
        await HXManager().sendHxChatMsg(widget.hxId, widget.msgExt, msgBody);
    startId = message.msgId;
    list.add(message);
    msgList = list;

    setState(() {});
    scrollToEnd();
  }

  // 发送语音
  void onSendAudio() async {
    String? audioPath = await FuncUtils()
        .requestUploadAudio(AppManager().recordPath, UploadType.CHAT_AUDIO);
    if (audioPath == null) return;
    if (audioPath.isEmpty) return WidgetUtils().showToast("录音上传失败");

    if (!await canSendChat(type: 3, filePath: audioPath)) return;

    HXAudioMsg audioMsg = HXAudioMsg(audioPath, audioTime);
    EMMessage message =
        await HXManager().sendHxChatMsg(widget.hxId, widget.msgExt, audioMsg);
    startId = message.msgId;
    msgList.add(message);
    setState(() {});
    scrollToEnd();
  }

  // 发送拍照
  void onTapPhoto() async {
    final ImagePicker picker = ImagePicker();
    bool accept = await FuncUtils().requestPermission(Permission.photos);
    if (!accept) return;

    List<XFile> files = await picker.pickMultiImage();
    for (XFile file in files) {
      if (file.path.isEmpty) return;
      String? imagePath =
          await FuncUtils().requestUpload(file.path, UploadType.CHAT_IMAGE);
      if (imagePath == null) return WidgetUtils().cancelLoading();
      if (imagePath.isEmpty) {
        WidgetUtils().showToast("上传图片失败");
        WidgetUtils().cancelLoading();
        return;
      }

      if (!await canSendChat(type: 2, filePath: imagePath)) return;

      HXImageMsg msgBody = HXImageMsg(imagePath, width: 100, height: 100);
      WidgetUtils().cancelLoading();
      EMMessage message =
          await HXManager().sendHxChatMsg(widget.hxId, widget.msgExt, msgBody);
      startId = message.msgId;
      msgList.add(message);
      setState(() {});
      scrollToEnd();
    }
  }

  // 发送照片
  void onTapCamera() async {
    final ImagePicker picker = ImagePicker();
    bool auth = await FuncUtils().requestPermission(Permission.camera);
    if (!auth) return;
    XFile? file = await picker.pickImage(source: ImageSource.camera);
    if (file == null || file.path.isEmpty) return;

    String? imagePath =
        await FuncUtils().requestUpload(file.path, UploadType.CHAT_IMAGE);
    if (imagePath == null) return WidgetUtils().cancelLoading();
    if (imagePath.isEmpty) {
      WidgetUtils().showToast("上传图片失败");
      return;
    }

    if (!await canSendChat(type: 2, filePath: imagePath)) return;

    HXImageMsg msgBody = HXImageMsg(imagePath, width: 100, height: 100);
    EMMessage message =
        await HXManager().sendHxChatMsg(widget.hxId, widget.msgExt, msgBody);
    startId = message.msgId;
    msgList.add(message);
    setState(() {});
    scrollToEnd();
  }

  // 送礼
  void onTapGift() async {
    List<HXGiftMsg>? giftList = await WidgetUtils().showBottomSheet(
      GiveDialog(
        false,
        userId: widget.msgExt.toUser,
      ),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
        topRight: Radius.circular(10.w),
        topLeft: Radius.circular(10.w),
      )),
      color: Colors.transparent,
    );

    if (giftList != null && giftList.isNotEmpty) {
      List<EMMessage> msgs = [];
      for (HXGiftMsg gift in giftList) {
        EMMessage message =
            await HXManager().sendHxChatMsg(widget.hxId, widget.msgExt, gift);
        msgs.add(message);
      }

      startId = msgs.last.msgId;
      msgList.addAll(msgs);
      setState(() {});
      scrollToEnd();
    }
  }

  // 发红包
  void onSendHbao(int gold) async {
    HXHbaoMsg msgBody = HXHbaoMsg(gold);
    EMMessage message =
        await HXManager().sendHxChatMsg(widget.hxId, widget.msgExt, msgBody);
    startId = message.msgId;
    msgList.add(message);
    setState(() {});
    scrollToEnd();
  }

  // 删除/撤回消息
  void onWithdrawMsg() async {
    try {
      await EMClient.getInstance.chatManager.deleteRemoteMessagesWithIds(
        conversationId: widget.hxId,
        type: EMConversationType.Chat,
        msgIds: [startId],
      );
    } on EMError catch (e) {
      print("撤回消息失败：$e");
    }
  }
}
