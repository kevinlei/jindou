import 'package:cached_network_image/cached_network_image.dart';
import 'package:echo/import.dart';
import 'package:flutter/material.dart';
import 'package:im_flutter_sdk/im_flutter_sdk.dart';

class MsgPageType {
  EMMessage? data;
  late int unread;
}

class MsgPrivateChat extends StatefulWidget {
  final bool isRoom;
  const MsgPrivateChat({super.key, this.isRoom = false});

  @override
  State<MsgPrivateChat> createState() => _MsgPrivateChatState();
}

class _MsgPrivateChatState extends State<MsgPrivateChat> {
  // 消息列表
  List<EMConversation> conList = [];

  // 是否开启青少年模式
  bool adolescentMode = false;

  @override
  void initState() {
    super.initState();
    conList = AppManager().conList;
    EventUtils().on(
      EventConstants.E_MSG_UPDATE,
      EventCall("MsgPrivateChat", (v) {
        initMsg();
      }),
    );
    EventUtils().on(
      EventConstants.E_MQ_ADOMODE,
      EventCall("MsgPrivateChat", (v) {
        adolescentMode = UserConstants.userginInfo?.info.teenmode == 1;
        setState(() {});
      }),
    );
    adolescentMode = UserConstants.userginInfo?.info.teenmode == 1;
    initMsg();
  }

  void initMsg() {
    Future.delayed(const Duration(milliseconds: 200), () async {
      conList = AppManager().conList;
      setState(() {});
    });
  }

  @override
  void dispose() {
    EventUtils()
        .off(EventConstants.E_MSG_UPDATE, EventCall("MsgPrivateChat", (v) {}));
    EventUtils()
        .off(EventConstants.E_MQ_ADOMODE, EventCall("MsgPrivateChat", (v) {}));
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return !adolescentMode
        ? FContainer(
            padding: EdgeInsets.only(
                top: !widget.isRoom ? ScreenUtil().statusBarHeight : 0,
                left: 30.w,
                right: 30.w,
                bottom: 20.w),
            imageFit: BoxFit.fill,
            radius: widget.isRoom ? BorderRadius.circular(20.w) : null,
            imageAlign: Alignment.topCenter,
            image: ImageUtils().getAssetsImage("public/huibg"),
            color: Colors.white,
            height: widget.isRoom ? 1000.w : null,
            child: Column(
              children: [
                SizedBox(height: 40.w),
                msgHerder(),
                SizedBox(height: 50.w),
                Expanded(
                  child: FRefreshLayout(
                    firstWidget: const SizedBox(),
                    firstRefresh: false,
                    child: msgListView(conList),
                  ),
                ),
              ],
            ),
          )
        : const AdolescentMode();
  }

  Widget msgHerder() {
    return Row(
      children: [
        FContainer(
          width: 95.w,
          height: 70.w,
          child: Stack(alignment: AlignmentDirectional.topCenter, children: [
            Positioned(
                bottom: 0,
                child: LoadAssetImage(
                  "public/pageViewBot",
                  width: 95.w,
                )),
            FText(
              "消息",
              size: 46.sp,
              color: Colors.black,
              weight: FontWeight.bold,
            )
          ]),
        ),
        const Spacer(),
        GestureDetector(
          onTap: () => WidgetUtils().pushPage(const searchPage()),
          child: LoadAssetImage(
            "msgPrivateChat/search",
            width: 60.w,
          ),
        ),
        SizedBox(width: 18.w),
        GestureDetector(
          onTap: () async {
            bool? accept = await WidgetUtils().showAlert("是否忽略全部私信未读消息？");
            if (accept == null || !accept) return;
            AppManager().markAllConversation();
            initMsg();
          },
          child: LoadAssetImage(
            "msgPrivateChat/clear",
            width: 60.w,
          ),
        ),
        SizedBox(width: 18.w),
        GestureDetector(
          onTap: () {
            WidgetUtils().pushPage(const RelationPage(CountListType.FRIENDS));
          },
          child: LoadAssetImage(
            "msgPrivateChat/friends",
            width: 120.w,
          ),
        ),
      ],
    );
  }

  Widget functionality() {
    return FContainer(
      margin: EdgeInsets.only(bottom: 40.w),
      height: 150.w,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          functionalityItem("msgPrivateChat/officialSources", "官方消息",
              AppManager().adminunread, () async {
            await WidgetUtils().pushPage(const AdminMsg());
            EventUtils().emit(EventConstants.E_MSG_UPDATE, null);
            if (!mounted) return;
            setState(() {});
          }),
          functionalityItem("msgPrivateChat/systemNotifications", "系统通知",
              AppManager().sysunread, () async {
            await WidgetUtils().pushPage(const SysMsg());
            EventUtils().emit(EventConstants.E_MSG_UPDATE, null);
            if (!mounted) return;
            setState(() {});
          }),
          functionalityItem("msgPrivateChat/interactiveMessages", "互动消息", 0,
              () async {
            // await WidgetUtils().pushPage(const InteractionMsg());
            // if (!mounted) return;
            // setState(() {});
          }),
          functionalityItem("msgPrivateChat/eventAnnouncement", "活动公告", 0,
              () async {
            // await WidgetUtils().pushPage(const ActivityMsg());
            // if (!mounted) return;
            // setState(() {});
          }),
        ],
      ),
    );
  }

  Widget functionalityItem(
      String img, String text, int unread, void Function() onTap) {
    return Stack(
      // fit: StackFit.expand,
      clipBehavior: Clip.none,
      children: [
        GestureDetector(
          onTap: onTap,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              LoadAssetImage(
                img,
                width: 115.w,
                height: 115.w,
              ),
              FText(
                text,
                size: 22.sp,
                color: const Color(0xFFA5A5A7),
              )
            ],
          ),
        ),
        Positioned(
          top: 0,
          left: 80.w,
          child: msgUnread(unread, isAdmin: text == "官方消息"),
        ),
      ],
    );
  }

  Widget hotChatSquare() {
    Widget child = FContainer(
      margin: EdgeInsets.only(bottom: 20.w),
      color: Colors.transparent,
      height: 115.w,
      child: Row(
        children: [
          LoadAssetImage(
            "msgPrivateChat/hotChatSquare",
            width: 115.w,
            height: 115.w,
          ),
          SizedBox(width: 10.w),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              FText(
                "热聊广场",
                size: 30.sp,
                color: Colors.black,
                weight: FontWeight.bold,
              ),
              // Text.rich(
              //   TextSpan(
              //     style: TextStyle(
              //       color: const Color(0xFFA5A5A7),
              //       fontSize: 22.sp,
              //     ),
              //     children: const [
              //       TextSpan(
              //         text: "已有",
              //       ),
              //       TextSpan(
              //           text: "477",
              //           style: TextStyle(color: Color(0xFFFF753F))),
              //       TextSpan(
              //         text: "人在这里嗨聊",
              //       ),
              //     ],
              //   ),
              // ),
            ],
          ),
          const Spacer(),
          LoadAssetImage("public/RightNavigation", width: 20.w),
        ],
      ),
    );

    return GestureDetector(
      onTap: () {
        WidgetUtils().pushPage(const MsgChatHall());
      },
      child: child,
    );
  }

  Widget msgListView(List<EMConversation> list) {
    return ListView.builder(
      padding: const EdgeInsets.all(0),
      addAutomaticKeepAlives: false,
      addRepaintBoundaries: false,
      physics: const NeverScrollableScrollPhysics(),
      itemBuilder: (_, index) {
        if (index == 0) {
          return functionality();
        }
        if (index == 1) {
          return hotChatSquare();
        } else {
          return FutureBuilder<MsgPageType>(
            key: Key("${list[index - 2].pinnedTime}"),
            future: _getData(list[index - 2]),
            builder:
                (BuildContext context, AsyncSnapshot<MsgPageType> snapshot) =>
                    _createListView(list[index - 2], snapshot),
          );
        }
      },
      itemCount: list.length + 2,
    );
  }

  Future<MsgPageType> _getData(EMConversation con) async {
    MsgPageType data = MsgPageType();
    data.data = await con.latestMessage();
    data.unread = await con.unreadCount();
    return data;
  }

  Widget _createListView(EMConversation con, AsyncSnapshot snapshot) {
    EMMessage? msgdata = snapshot.data?.data;
    int unread = snapshot.data?.unread ?? 0;
    return msgListViewItem(con, msgdata, unread);
  }

  Widget msgListViewItem(EMConversation con, EMMessage? data, int unread) {
    String sendId = "", toUserId = "", toName = "", toHead = "";

    if (data != null) {
      sendId = (data.attributes?["sendUser"] ?? "").toString();
      if (sendId == UserConstants.userginInfo!.info.userId) {
        toUserId = (data.attributes?["toUser"] ?? "").toString();
        toName = (data.attributes?["toName"] ?? "").toString();
        toHead = (data.attributes?["toHead"] ?? "").toString();
      } else {
        toUserId = (data.attributes?["sendUser"] ?? "").toString();
        toName = (data.attributes?["sendName"] ?? "").toString();
        toHead = (data.attributes?["sendHead"] ?? "").toString();
      }
    } else {
      sendId = (con.ext?["sendUser"] ?? "").toString();
      if (sendId == UserConstants.userginInfo!.info.userId) {
        toUserId = (con.ext?["toUser"] ?? "").toString();
        toName = (con.ext?["toName"] ?? "").toString();
        toHead = (con.ext?["toHead"] ?? "").toString();
      } else {
        toUserId = (con.ext?["sendUser"] ?? "").toString();
        toName = (con.ext?["sendName"] ?? "").toString();
        toHead = (con.ext?["sendHead"] ?? "").toString();
      }
    }
    if (toHead.isEmpty) return const SizedBox();

    Widget child = FContainer(
      margin: EdgeInsets.only(bottom: 20.w),
      height: 115.w,
      child: Row(
        children: [
          FContainer(
            width: 115.w,
            height: 115.w,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(50),
              child: CachedNetworkImage(
                imageUrl: NetConstants.ossPath + toHead,
                fit: BoxFit.cover,
                placeholder: (context, url) =>
                    const CircularProgressIndicator(),
                errorWidget: (context, url, error) => const Icon(Icons.error),
              ),
            ),
          ),
          SizedBox(width: 10.w),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  FText(
                    toName,
                    size: 30.sp,
                    color: Colors.black,
                    weight: FontWeight.bold,
                  ),
                  SizedBox(width: 5.w),
                ],
              ),
              FContainer(
                constraints: BoxConstraints(maxWidth: 350.w),
                child: FText(
                  getMsgText(data),
                  color: const Color(0xFFA5A5A7),
                  size: 22.sp,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ],
          ),
          const Spacer(),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              FContainer(
                child: FText(
                  getTimeText(data),
                  size: 22.sp,
                  color: const Color(0xFFA5A5A7),
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              msgUnread(unread),
            ],
          )
        ],
      ),
    );

    return GestureDetector(
      onTap: () async {
        await WidgetUtils().pushPage(MsgChat(
            con.id, HxMsgExt(toUserId, toHead, toName),
            msgId: data?.msgId ?? ""));
        AppManager().refreshUnread();
        initMsg();
      },
      child: Slidable(
        endActionPane: ActionPane(
          extentRatio: 0.25,
          motion: const ScrollMotion(),
          children: [slidAction(con)],
        ),
        child: child,
      ),
    );
  }

  CustomSlidableAction slidAction(EMConversation con) {
    return CustomSlidableAction(
      onPressed: (_) {
        AppManager().deleteConversation(con);
        initMsg();
      },
      padding: EdgeInsets.only(left: 20.w),
      backgroundColor: Colors.transparent,
      child: FContainer(
        width: double.infinity,
        height: double.infinity,
        color: const Color(0xFFFF753F),
        align: Alignment.center,
        child: FText(
          "删除",
          color: Colors.white,
          size: 28.sp,
        ),
      ),
    );
  }

  Widget msgUnread(int unread, {isAdmin = false}) {
    if (unread > 0) {
      return FContainer(
          padding: EdgeInsets.symmetric(vertical: 5.w, horizontal: 10.w),
          radius: BorderRadius.circular(50),
          color: const Color(0xFFFF753F),
          align: Alignment.center,
          child: FText(isAdmin ? "new" : "$unread", size: 22.sp));
    } else {
      return const SizedBox();
    }
  }

  String getMsgText(EMMessage? data) {
    try {
      if (data?.body.type == MessageType.TXT) {
        EMTextMessageBody msgBody = data?.body as EMTextMessageBody;
        HxReceiveMsg receiveMsg = HxReceiveMsg(msgBody.content);
        switch (receiveMsg.msgType) {
          case HXMsgType.TEXT:
            HXTextMsg msg = receiveMsg.msgBody as HXTextMsg;
            return msg.text;
          case HXMsgType.IMAGE:
            return "[图片]";
          case HXMsgType.LOC:
            return "[位置分享]";
          case HXMsgType.AUDIO:
            return "[语音]";
          case HXMsgType.VIDEO:
            return "[视频]";
          case HXMsgType.DYNAMIC:
            return "[动态分享]";
          case HXMsgType.GIFT:
            return "[礼物赠送]";
          case HXMsgType.ROOM:
            return "[房间分享]";
          case HXMsgType.GOLD:
            return "[金币赠送]";
          case HXMsgType.CLUB:
            return "[公会分享]";
          case HXMsgType.DRESS:
            return "[赠送装扮]";
          case HXMsgType.HBAO:
            return "[赠送红包]";
        }
      }
      return "";
    } catch (e) {
      debugPrint('解析异常msgconcontent$e');
      return "";
    }
  }

  String getTimeText(EMMessage? data) {
    String timeText = "";
    int localTime = 0;
    if (data?.localTime != null && data?.localTime is int) {
      localTime = data!.localTime;
    }
    if (localTime == 0) return timeText;
    DateTime time = DateTime.fromMillisecondsSinceEpoch(localTime);
    if (DateTime.now().difference(time).inHours < 24 &&
        (DateTime.now().day == time.day)) {
      if (time.hour > 12) {
        timeText = "下午 ${time.toString().substring(11, 16)}";
      } else {
        timeText = "上午 ${time.toString().substring(11, 16)}";
      }
    } else if (time.difference(DateTime.now()).inHours < 48 &&
        (DateTime.now().day - time.day) == 1) {
      timeText = "昨天 ${time.toString().substring(11, 16)}";
    } else {
      timeText = time.toString().substring(0, 16);
    }
    return timeText;
  }
}
