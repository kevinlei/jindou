import 'dart:async';
import 'dart:convert';

import 'package:echo/import.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:im_flutter_sdk/im_flutter_sdk.dart';

class MsgChatHall extends StatefulWidget {
  const MsgChatHall({super.key});

  @override
  State<MsgChatHall> createState() => _MsgChatHallState();
}

class _MsgChatHallState extends State<MsgChatHall> {
  TextEditingController textController = TextEditingController();
  ScrollController scroll = ScrollController();

  // 消息列表
  List<EMMessage> worldChatList = [];

  // 是否打开表情
  bool isEmo = false;

  // 节流
  Timer? timer;
  bool sendOpen = false;

  // 是否反向
  bool isdown = false;

  @override
  void initState() {
    super.initState();
    if (!mounted) return;
    EventUtils().on(
        EventConstants.E_ROOM_MSG_UPDATE,
        EventCall("MsgChatHall", (v) {
          worldChatList = AppManager().roomInfo.wordRoomMsg;
          if (isdown) {
            worldChatList = worldChatList.reversed.toList();
          }
          if (!mounted) return;
          setState(() {});
          scrollToEnd();
        }));
    worldChatList = AppManager().roomInfo.wordRoomMsg;
    isdown = worldChatList.length > 15;
    if (isdown) {
      worldChatList = worldChatList.reversed.toList();
    }
    scrollToEnd();
  }

  @override
  void dispose() {
    EventUtils().off(
        EventConstants.E_ROOM_MSG_UPDATE, EventCall("MsgChatHall", (v) {}));
    textController.dispose();
    scroll.dispose();
    super.dispose();
  }

  void scrollToEnd() async {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      if (scroll.hasClients) {
        double max = isdown
            ? scroll.position.minScrollExtent
            : scroll.position.maxScrollExtent;
        scroll.animateTo(max,
            duration: const Duration(milliseconds: 100), curve: Curves.linear);
        Future.delayed(const Duration(milliseconds: 100), () {
          double max2 = isdown
              ? scroll.position.minScrollExtent
              : scroll.position.maxScrollExtent;
          scroll.animateTo(max2,
              duration: const Duration(milliseconds: 100),
              curve: Curves.linear);
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        SystemChannels.textInput.invokeMethod("TextInput.hide");
        setState(() {
          isEmo = false;
        });
      },
      child: FContainer(
        imageFit: BoxFit.fill,
        imageAlign: Alignment.topCenter,
        image: ImageUtils().getAssetsImage("public/huibg"),
        child: FContainer(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const CommonAppBar(
                title: "聊天大厅",
                textColor: Colors.black,
              ),
              Expanded(
                child: ListView.builder(
                  controller: scroll,
                  reverse: isdown,
                  padding: EdgeInsets.symmetric(vertical: 0, horizontal: 30.w),
                  itemBuilder: (context, index) =>
                      msgItem(worldChatList[index]),
                  itemCount: worldChatList.length,
                ),
              ),
              keyWord(),
            ],
          ),
        ),
      ),
    );
  }

  Widget msgItem(EMMessage msgInfo) {
    Radius radius = Radius.circular(20.w);

    EMTextMessageBody msg = msgInfo.body as EMTextMessageBody;
    var data = json.decode(msg.content);
    ExtUser user = ExtUser.fromJson(data["info"]);

    bool isMy = user.id == UserConstants.userginInfo!.info.userId;

    return FContainer(
      margin: EdgeInsets.only(bottom: 20.w),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Visibility(
            visible: !isMy,
            child: GestureDetector(
              onTap: () {
                WidgetUtils().pushPage(PersonHomepage(userId: user.id));
              },
              child: FContainer(
                margin: EdgeInsets.only(right: 10.w),
                width: 80.w,
                height: 80.w,
                radius: BorderRadius.circular(50),
                image: ImageUtils()
                    .getImageProvider(NetConstants.ossPath + user.head),
                imageFit: BoxFit.cover,
              ),
            ),
          ),
          Expanded(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment:
                  isMy ? CrossAxisAlignment.end : CrossAxisAlignment.start,
              children: [
                headers(isMy, user),
                SizedBox(height: 10.w),
                FContainer(
                  padding: EdgeInsets.all(15.w),
                  radius: BorderRadius.only(
                    topLeft: isMy ? radius : Radius.zero,
                    topRight: !isMy ? radius : Radius.zero,
                    bottomLeft: radius,
                    bottomRight: radius,
                  ),
                  color:
                      isMy ? const Color(0xFFFF753F) : const Color(0xFFF5F6F7),
                  child: FText(
                    data["content"],
                    color: isMy ? Colors.white : Colors.black,
                    maxLines: 100,
                  ),
                )
              ],
            ),
          ),
          Visibility(
            visible: isMy,
            child: GestureDetector(
              onTap: () {
                WidgetUtils().pushPage(PersonHomepage(userId: user.id));
              },
              child: FContainer(
                margin: EdgeInsets.only(left: 10.w),
                width: 80.w,
                height: 80.w,
                image: ImageUtils()
                    .getImageProvider(NetConstants.ossPath + user.head),
                imageFit: BoxFit.cover,
                radius: BorderRadius.circular(50),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget headers(bool isMy, ExtUser user) {
    if (isMy) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          FText(
            user.name,
            color: Colors.black,
          ),
          ConfigManager().userWealthCharmIcon(
              user.wealthBadge, user.wealthGrade,
              marginRight: 5.w, isWealth: true),
          ConfigManager().userWealthCharmIcon(user.charmBadge, user.charmGrade,
              marginRight: 5.w),
        ],
      );
    }
    return Row(
      children: [
        ConfigManager().userWealthCharmIcon(user.wealthBadge, user.wealthGrade,
            marginRight: 5.w, isWealth: true),
        ConfigManager().userWealthCharmIcon(user.charmBadge, user.charmGrade,
            marginRight: 5.w),
        FText(
          user.name,
          color: Colors.black,
        ),
      ],
    );
  }

  Widget keyWord() {
    return FContainer(
      padding: EdgeInsets.only(
        top: 15.w,
        left: 40.w,
        right: 40.w,
        bottom: 50.w,
      ),
      color: Colors.white,
      child: Column(
        children: [
          Row(
            children: [
              Expanded(child: textInput()),
              SizedBox(width: 5.w),
              Row(
                children: [
                  GestureDetector(
                    onTap: () {
                      SystemChannels.textInput.invokeMethod("TextInput.hide");
                      Future.delayed(const Duration(milliseconds: 130), () {
                        isEmo = true;
                        setState(() {});
                      });
                    },
                    child: LoadAssetImage("msgPrivateChat/msgChat/emo",
                        width: 60.w),
                  ),
                  SizedBox(width: 5.w),
                  FButton(
                    onTap: onSendText,
                    padding:
                        EdgeInsets.symmetric(vertical: 10.w, horizontal: 30.w),
                    isTheme: true,
                    child: FText(
                      "发送",
                      size: 28.sp,
                    ),
                  )
                ],
              )
            ],
          ),
          SizedBox(
            height: isEmo ? 500.w : MediaQuery.of(context).viewInsets.bottom,
            child: Visibility(
              visible: isEmo,
              child: EmotionFooter(textController),
            ),
          ),
        ],
      ),
    );
  }

  Widget textInput() {
    InputBorder customBorder = OutlineInputBorder(
        borderRadius: BorderRadius.circular(50),
        borderSide: const BorderSide(style: BorderStyle.none, width: 0));

    return RawKeyboardListener(
      // 输入框
      focusNode: FocusNode(),
      onKey: (event) {
        if (event.runtimeType == RawKeyDownEvent) {
          if (event.logicalKey.keyId == 4294967309) {
            onSendText();
          }
        }
      },
      child: TextField(
        onSubmitted: (v) => onSendText(),
        onTap: () {
          isEmo = false;
          setState(() {});
          Future.delayed(const Duration(milliseconds: 200), () {
            scrollToEnd();
          });
        },
        controller: textController,
        textInputAction: TextInputAction.send,
        style: TextStyle(color: Colors.black, fontSize: 33.h),
        maxLines: 1,
        decoration: InputDecoration(
          filled: true,
          fillColor: const Color(0xFF222222).withOpacity(0.05),
          border: customBorder,
          enabledBorder: customBorder,
          focusedBorder: customBorder,
          isDense: true,
          contentPadding:
              EdgeInsets.symmetric(vertical: 18.h, horizontal: 20.w),
          hintText: "一起来聊聊吧...",
          hintStyle: TextStyle(
            color: const Color(0xFF222222).withOpacity(0.6),
            fontSize: 33.h,
          ),
          counterText: "",
        ),
      ),
    );
  }

  void onSendText() async {
    if (sendOpen || timer != null) return;
    sendOpen = true;
    timer = Timer(const Duration(seconds: 1), () {
      sendOpen = false;
      timer = null;
    });

    String text = textController.text.trimRight();
    textController.clear();

    if (text.isEmpty) {
      return WidgetUtils().showToast("输入内容不能为空");
    }

    if (text.isEmpty) return WidgetUtils().showToast("请输入要发送的内容");

    scrollToEnd();
    AppManager().sendRoomMsg(text, isWord: true);
  }
}
