import 'package:echo/import.dart';
import 'package:flutter/material.dart';
import 'package:im_flutter_sdk/im_flutter_sdk.dart';

class MsgSetUp extends StatefulWidget {
  final HxMsgExt msgExt;
  final EMConversation? con;
  const MsgSetUp(this.msgExt, this.con, {super.key});

  @override
  State<MsgSetUp> createState() => _MsgSetUpState();
}

class _MsgSetUpState extends State<MsgSetUp> {
  BorderSide border =
      BorderSide(color: Colors.black.withOpacity(0.1), width: 1.w);

  // 用户是否被拉黑
  bool isBlock = false;

  @override
  void initState() {
    super.initState();
    if (!mounted) return;
    initBlock();
  }

  void initBlock() async {
    isBlock = await AppManager().blockList(widget.con!.id);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      imageFit: BoxFit.fill,
      imageAlign: Alignment.topCenter,
      image: ImageUtils().getAssetsImage("public/huibg"),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: const CommonAppBar(
          title: "聊天设置",
        ),
        body: Column(
          children: [
            header(),
            // setUp(),
            abilityItem("清空聊天记录", clearChatLog),
            abilityItem("举报", report),
            abilityItem("拉黑", block),
          ],
        ),
      ),
    );
  }

  Widget header() {
    Widget child = FContainer(
      border: Border(top: border, bottom: border),
      padding: EdgeInsets.all(38.w),
      child: Row(
        children: [
          FContainer(
            radius: BorderRadius.circular(50),
            height: 110.w,
            width: 110.w,
            image: ImageUtils()
                .getImageProvider(NetConstants.ossPath + widget.msgExt.toHead),
          ),
          SizedBox(width: 20.w),
          FText(
            widget.msgExt.toName,
            size: 32.sp,
            weight: FontWeight.w500,
            color: Colors.black,
          )
        ],
      ),
    );

    return GestureDetector(
      onTap: () {
        WidgetUtils().pushPage(PersonHomepage(userId: widget.msgExt.toUser));
      },
      child: child,
    );
  }

  Widget setUp() {
    return FContainer(
      border: Border(bottom: border),
      padding: EdgeInsets.all(38.w),
      child: Column(
        children: [
          setUpItem("置顶聊天"),
          SizedBox(height: 20.w),
          setUpItem("消息免打扰"),
        ],
      ),
    );
  }

  Widget setUpItem(String text) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        FText(
          text,
          size: 32.sp,
          color: Colors.black,
        ),
        FBtnOpen(
          () {},
          width: 105.w,
        )
      ],
    );
  }

  Widget abilityItem(String text, void Function() onTap) {
    Widget child = FContainer(
      border: Border(bottom: border),
      padding: EdgeInsets.all(38.w),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          FText(
            text,
            size: 32.sp,
            color: Colors.black,
          ),
          LoadAssetImage("public/RightNavigation", width: 20.w),
        ],
      ),
    );
    return GestureDetector(
      onTap: onTap,
      child: child,
    );
  }

  // 清空聊天记录
  void clearChatLog() {
    AppManager().cleanConMsg(widget.con!);
  }

  // 举报
  void report() {
    WidgetUtils().pushPage(ReportType(widget.msgExt.toUser));
  }

  // 拉黑
  void block() {
    if (isBlock) {
      AppManager().userFromBlock(widget.con!.id);
    } else {
      AppManager().userToBlock(widget.con!.id);
    }
    initBlock();
  }
}
