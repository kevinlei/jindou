import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class TheCharts extends StatefulWidget {
  const TheCharts({super.key});

  @override
  State<TheCharts> createState() => _TheChartsState();
}

class _TheChartsState extends State<TheCharts> {
  // 切换榜单类型
  bool isWealth = true;
  // 是否今日/今周/今月
  bool isToday = true;
  // 榜单范围
  List<int> types = [0, 1, 2];
  // 当前选择什么榜
  int typeId = 0;

  // 今日/今周/今月总流水
  int allBarley = 0;
  // 财富榜
  List wealthList = [];
  // 魅力榜
  List charmList = [];
  // 自己的信息
  var wealtCharmhUser;

  @override
  void initState() {
    super.initState();
    wealthCharmList(0);
  }

  // 获取房间财富/魅力榜
  void wealthCharmList(int type) async {
    int timeType = typeId;
    if (!isToday) {
      switch (typeId) {
        case 0:
          timeType = 3;
          break;
        case 1:
          timeType = 4;
          break;
        default:
          timeType = 5;
      }
    }

    await DioUtils().httpRequest(
      NetConstants.wealthCharmList,
      method: DioMethod.POST,
      loading: false,
      params: {
        "room_id": -1,
        "billboard_type": type,
        "time_type": timeType,
        "user_id": UserConstants.userginInfo!.info.userId,
      },
      onSuccess: (res) {
        if (res == null || res["billboard_user_infos"] == null) {
          if (type == 0) {
            wealthList = [];
          } else {
            charmList = [];
          }
          wealtCharmhUser = null;
          allBarley = 0;
        } else {
          if (type == 0) {
            wealthList = res["billboard_user_infos"];
          } else {
            charmList = res["billboard_user_infos"];
          }
          wealtCharmhUser = res["user_info"];
          allBarley = res["experience_count"];
        }
        setState(() {});
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      imageFit: BoxFit.fill,
      imageAlign: Alignment.topCenter,
      image: ImageUtils().getAssetsImage(
          isWealth ? "theCharts/wealthBillboard" : "theCharts/charmBillboard"),
      child: Scaffold(
          backgroundColor: Colors.transparent,
          body: Column(
            children: [
              CommonAppBar(
                textColor: Colors.white,
                titleChild: header(),
                actions: [
                  GestureDetector(
                    onTap: () {
                      isToday = !isToday;
                      wealthCharmList(isWealth ? 0 : 1);
                      setState(() {});
                    },
                    child: FContainer(
                      padding: EdgeInsets.symmetric(horizontal: 30.w),
                      align: Alignment.center,
                      child: FText(
                        "${isToday ? "今" : "昨"}${dayText()}榜单",
                        size: 28.sp,
                        color: const Color(0xFFDADADA),
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(height: 20.w),
              chartsType(),
              SizedBox(height: 20.w),
              userHeader(isWealth ? wealthList : charmList),
              SizedBox(height: 80.w),
              Expanded(child: userList(isWealth ? wealthList : charmList))
            ],
          ),
          bottomNavigationBar: footer()),
    );
  }

  Widget header() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        GestureDetector(
          onTap: () {
            isWealth = true;
            wealthCharmList(0);
            setState(() {});
          },
          child: FContainer(
            child: Column(
              children: [
                FText(
                  "财富榜",
                  size: 36.sp,
                  color: isWealth ? Colors.white : const Color(0xFFDADADA),
                  weight: FontWeight.w500,
                ),
                SizedBox(height: 5.w),
                Visibility(
                  visible: isWealth,
                  child: FContainer(
                    width: 30.w,
                    height: 8.w,
                    radius: BorderRadius.circular(50),
                    color: Colors.white,
                  ),
                )
              ],
            ),
          ),
        ),
        SizedBox(width: 60.w),
        GestureDetector(
          onTap: () {
            isWealth = false;
            wealthCharmList(1);
            setState(() {});
          },
          child: FContainer(
            child: Column(
              children: [
                FText(
                  "魅力榜",
                  size: 36.sp,
                  color: isWealth ? const Color(0xFFDADADA) : Colors.white,
                  weight: FontWeight.w500,
                ),
                SizedBox(height: 5.w),
                Visibility(
                  visible: !isWealth,
                  child: FContainer(
                    width: 30.w,
                    height: 8.w,
                    radius: BorderRadius.circular(50),
                    color: Colors.white,
                  ),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget chartsType() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 30.w),
      child: Stack(
        alignment: AlignmentDirectional.centerEnd,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 40.w),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                chartsTypeItem("日榜", 0),
                chartsTypeItem("周榜", 1),
                chartsTypeItem("月榜", 2),
              ],
            ),
          ),
          LoadAssetImage("theCharts/illustrate", width: 30.w)
        ],
      ),
    );
  }

  Widget chartsTypeItem(String text, int index) {
    return GestureDetector(
      onTap: () {
        typeId = index;
        wealthCharmList(isWealth ? 0 : 1);
        setState(() {});
      },
      child: FContainer(
        radius: BorderRadius.circular(50),
        padding: EdgeInsets.symmetric(vertical: 10.w, horizontal: 22.w),
        color: index == typeId ? Colors.white.withOpacity(0.4) : null,
        child: FText(
          text,
          size: 28.sp,
          color: index == typeId ? Colors.white : const Color(0xFFDADADA),
        ),
      ),
    );
  }

  Widget userHeader(List list) {
    return FContainer(
      padding: EdgeInsets.symmetric(horizontal: 60.w),
      height: 360.w,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          userHeaderItem(1, list.length > 1 ? list[1] : null),
          userHeaderItem(2, list.isNotEmpty ? list[0] : null),
          userHeaderItem(3, list.length > 2 ? list[2] : null),
        ],
      ),
    );
  }

  Widget userHeaderItem(int index, data) {
    String img = "one";
    switch (index) {
      case 1:
        img = "two";
        break;
      case 3:
        img = "three";
        break;
    }
    double marginTop = 40.w;
    switch (index) {
      case 1:
        marginTop = 110.w;
        break;
      case 3:
        marginTop = 120.w;
        break;
    }
    return FContainer(
      width: 180.w,
      margin: EdgeInsets.only(top: marginTop, right: index == 3 ? 10.w : 0),
      child: Column(
        children: [
          Stack(
            children: [
              Positioned(
                  top: index == 2 ? 30.w : 20.w,
                  left: 6.w,
                  child: FContainer(
                    width: 111.w,
                    height: 111.w,
                    radius: BorderRadius.circular(50),
                    imageFit: BoxFit.cover,
                    image: data?["user_image"] != null &&
                            data["user_image"] != ""
                        ? ImageUtils().getImageProvider(
                            NetConstants.ossPath + (data?["user_image"] ?? ""))
                        : null,
                  )),
              LoadAssetImage(
                "theCharts/$img",
                width: 120.w,
              )
            ],
          ),
          SizedBox(height: 8.w),
          FText(
            data?["user_name"] ?? "",
            size: 28.sp,
            color: Colors.black,
            weight: FontWeight.w500,
            overflow: TextOverflow.ellipsis,
          ),
          SizedBox(height: 4.w),
          // Expanded(
          //   child: FText(
          //     "${data?["experience"] ?? ""}",
          //     overflow: TextOverflow.ellipsis,
          //     color: Colors.black,
          //   ),
          // )
        ],
      ),
    );
  }

  Widget userList(List list) {
    return list.length > 3
        ? FContainer(
            margin: EdgeInsets.symmetric(horizontal: 40.w),
            border: Border.all(color: Colors.white),
            color: Colors.white.withOpacity(0.6),
            radius: const BorderRadius.only(
                topLeft: Radius.circular(50), topRight: Radius.circular(50)),
            child: ListView.builder(
              addAutomaticKeepAlives: false,
              addRepaintBoundaries: false,
              padding: EdgeInsets.only(top: 20.w),
              itemBuilder: (context, index) =>
                  userItem(index + 3, list[index + 3]),
              itemCount: list.length - 3,
            ),
          )
        : const SizedBox();
  }

  Widget userItem(int index, data) {
    int num = index + 1;
    return FContainer(
      margin: EdgeInsets.only(bottom: 20.w),
      padding: EdgeInsets.symmetric(horizontal: 22.w),
      height: 120.w,
      child: Row(
        children: [
          FText("${num > 9 ? num : "0$num"}",
              size: 26.sp, color: const Color(0xFFBBBABA)),
          SizedBox(width: 20.w),
          FContainer(
            width: 90.w,
            height: 90.w,
            radius: BorderRadius.circular(50),
            imageFit: BoxFit.cover,
            image: ImageUtils().getImageProvider(
              NetConstants.ossPath + data["user_image"],
            ),
          ),
          SizedBox(width: 10.w),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              FText(
                data["user_name"],
                size: 28.sp,
                color: Colors.black,
                weight: FontWeight.w500,
              ),
              GenderAge(index % 2 == 0, 24),
            ],
          ),
          // Expanded(
          //   child: FText(
          //     "${data["experience"]}",
          //     align: TextAlign.right,
          //     size: 26.sp,
          //     color: const Color(0xFFBBBABA),
          //     maxLines: 3,
          //   ),
          // )
        ],
      ),
    );
  }

  Widget footer() {
    return FContainer(
      height: 180.w,
      padding: EdgeInsets.only(
          bottom: ScreenUtil().bottomBarHeight + 20.w, left: 30.w, right: 30.w),
      color: const Color(0xFFD972FF),
      child: Row(
        children: [
          FContainer(
            width: 90.w,
            height: 90.w,
            radius: BorderRadius.circular(50),
            color: Colors.transparent,
            imageFit: BoxFit.cover,
            image: wealtCharmhUser?["user_image"] != null &&
                    wealtCharmhUser["user_image"] != ""
                ? ImageUtils().getImageProvider(
                    NetConstants.ossPath + wealtCharmhUser["user_image"])
                : null,
          ),
          SizedBox(width: 20.w),
          FText(
            wealtCharmhUser?["user_name"] ?? "",
            size: 28.sp,
            color: Colors.white,
            weight: FontWeight.w500,
          ),
          Expanded(
            child: FText(
              "${wealtCharmhUser?["experience"] ?? 0}",
              align: TextAlign.right,
              size: 26.sp,
              color: const Color(0xFFBBBABA),
              overflow: TextOverflow.ellipsis,
            ),
          )
        ],
      ),
    );
  }

  String dayText() {
    switch (typeId) {
      case 0:
        return "日";
      case 1:
        return "周";
      default:
        return "月";
    }
  }
}
