import 'package:echo/import.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class UserPage extends StatefulWidget {
  const UserPage({Key? key}) : super(key: key);

  @override
  _UserPageState createState() => _UserPageState();
}

class _UserPageState extends State<UserPage> {
  List<String> urlList = [];
  String audioPath = "";
  CreateProvider1 provider = CreateProvider1();

  @override
  void initState() {
    super.initState();
    urlList = UserConstants.userginInfo?.info.photos ?? [];
    audioPath = UserConstants.userginInfo?.info.voice ?? "";
  }

  @override
  Widget build(BuildContext context) {
    int sex = UserConstants.userginInfo?.info.sex ?? 1;
    int bir = UserConstants.userginInfo?.info.birthday ?? 0;
    String time =
    DateTime.fromMillisecondsSinceEpoch(bir).toString().substring(0, 10);

    return Scaffold(
      backgroundColor: Color(0xFFF5F6F7),
      appBar: CommonAppBar(
        title: "编辑资料",
        textColor: Colors.black,
        elevation: 1,
        backgroundColor: Color(0xFFF5F6F7),
      ),
      body: ListView(
        padding: EdgeInsets.fromLTRB(15.w, 0, 15.w, 20.w),
        children: [
          SizedBox(height: 80.w),
          HeadContent(),
          SizedBox(height: 80.w),
          NameContent(),
          XdContent(),
          // SetListItem(
          //   "生日",
          //   onPress: onTapBir,
          //   suffix: FText(time, size: 26.sp, color: Color(0xFF666666)),
          // ),
          AgreContent(),
          ZhiContent(),
          CityContent(),
          DescribeContent(),
          AudioContent(audioPath: audioPath),
          SetListItem("精选照片", showArr: false),
          ImgprsonContent(urlList: urlList)
        ],
      ),
    );
  }

  void onTapBir() => {
    WidgetUtils().showBottomSheet(AgePicker()),


  };
}


