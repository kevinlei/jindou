import 'package:json_annotation/json_annotation.dart';

part 'augs_model.g.dart';

@JsonSerializable()
class AugsModel {
  final int? id;
  final String? content;
  final String? picture;
  final String? name;

  const AugsModel({
    this.id,
    this.content,
    this.picture,
    this.name,
  });

  factory AugsModel.fromJson(Map<String, dynamic> json) =>
      _$AugsModelFromJson(json);

  Map<String, dynamic> toJson() => _$AugsModelToJson(this);
}
