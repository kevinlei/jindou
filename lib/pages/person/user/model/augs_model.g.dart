// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'augs_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AugsModel _$AugsModelFromJson(Map<String, dynamic> json) => AugsModel(
      id: json['id'] as int?,
      content: json['content'] as String?,
      picture: json['picture'] as String?,
      name: json['name'] as String?,
    );

Map<String, dynamic> _$AugsModelToJson(AugsModel instance) => <String, dynamic>{
      'id': instance.id,
      'content': instance.content,
      'picture': instance.picture,
      'name': instance.name,
    };
