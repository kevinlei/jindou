import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'dart:convert';

class AudioSwipe extends StatefulWidget {
  @override
  _AudioSwipeState createState() => _AudioSwipeState();
}

class _AudioSwipeState extends State<AudioSwipe> {
  List<AugsModel> audioConfig = [];
  @override
  void initState() {
    super.initState();

    requestAudioConfig();
  }

  @override
  Widget build(BuildContext context) {
    return  SizedBox(
      height: 800.w,
      child: Swiper(
        itemCount: audioConfig.length,
        itemWidth: 310.0,
        scale: 0.65,
        //轮播图之间的间距
        viewportFraction: 0.6,
        //当前视窗比例，小于1时就会在屏幕内，可以看见旁边的轮播图
        indicatorLayout: PageIndicatorLayout.COLOR,
        //轮播图之间的间距
        autoplay: audioConfig.length > 1,
        control: const SwiperControl(
          color: Colors.transparent,
        ),
        itemBuilder: (_, index) => swipeCell(index),

        pagination: const SwiperPagination(
            builder: RectSwiperPaginationBuilder(
              color: Colors.black54,
              activeColor: Colors.white,
            )),
      ),
    );
  }

  Widget swipeCell(int index) {
    BoxShadow shadow = BoxShadow(
      color: Color(0xFF000000).withOpacity(0.1),
      spreadRadius: 5,
      blurRadius: 10,
    );

    String image = audioConfig[index].picture ?? "";
    String content = audioConfig[index].content ?? "";
    return FContainer(
      color: Colors.white,
      margin: EdgeInsets.symmetric(vertical: 30.h),
      radius: BorderRadius.circular(12.w),
      imageFit: BoxFit.cover,
      padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 20.h),
      image: ImageUtils().getImageProvider(NetConstants.ossPath + image),
      shadow: [shadow],
      align: Alignment.center,
      child: SingleChildScrollView(
        child: FText(content,
            align: TextAlign.center, color: Colors.white, maxLines: 100,weight: FontWeight.w900,size: 32.sp,),
      ),
    );
  }

  void requestAudioConfig() {
    DioUtils().asyncHttpRequest(
      NetConstants.recordAudio,
      params: {"conf_key":'voice_sign_example'},
      method: DioMethod.GET,
      onSuccess: onAudioConfig,
    );
  }
  // {"id":2,"content":"看，这么大的世界\n\n我遇到了唯一的你\n\n你也遇到了唯一的我\n\n真好~",
  // "picture":"admin-tapeSampleImg/Fpg5CAxizT7b3rNe68BQFE0sHxSU.png"}
  void onAudioConfig(response) {
    var jsonStr = jsonDecode(response);
    jsonStr ??= [];
    audioConfig.clear();
    for (var data in jsonStr) {
      audioConfig.add(AugsModel.fromJson(data));
    }
    if (!mounted) return;
    setState(() {});
  }
}
