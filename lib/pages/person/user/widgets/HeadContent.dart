import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';

class HeadContent extends StatefulWidget {
  @override
  _HeadContentState createState() => _HeadContentState();
}

class _HeadContentState extends State<HeadContent> {
  int headStatus = 0;
  late String headUrl;

  @override
  void initState() {
    super.initState();
    headUrl = UserConstants.userginInfo?.info.headIcon ?? "";
    requestHeadCheck();
  }

  @override
  Widget build(BuildContext context) {
    bool enable = headStatus != 1;
    return Center(
      child: GestureDetector(
        onTap: enable ? onTapHead : onTapCancel,
        child: headWidget(),
      ),
    );
  }

  Widget headWidget() {
    bool enable = headStatus != 1;

    return FContainer(
      width: 172.w,
      height: 172.w,
      imageFit: BoxFit.cover,
      image: ImageUtils().getImageProvider(NetConstants.ossPath + headUrl),
      color: Colors.black12,
      radius: BorderRadius.circular(172.w),
      child: Stack(children: [
        headMask(),
        Visibility(
            child: LoadAssetImage("person/restblck", width: 48.w),
            visible: enable),
      ], clipBehavior: Clip.none, alignment: Alignment.bottomRight),
    );
  }

  Widget headMask() {
    return Visibility(
        visible: headStatus == 1,
        child: CircleAvatar(
          radius: 172.w / 2,
          backgroundColor: Colors.black12,
          child: FText(headStatus == 1 ? "审核中..." : "",
              nullLength: 0, color: Colors.white, size: 24.sp),
        ));
  }

  void requestHeadCheck() {
    DioUtils().asyncHttpRequest(NetConstants.modicoviewnUser,
        method: DioMethod.GET, onSuccess: onHeadStatus);
  }

  void requestCancel() {
    DioUtils().asyncHttpRequest(NetConstants.modireviewUser,
        params: {"user_id": UserConstants.userginInfo?.info.userId},
        method: DioMethod.POST,
        onSuccess: onCancel);
  }

  Future<void> requestModify(String head) async {
    await DioUtils().httpRequest(
      NetConstants.updatHeadUser,
      method: DioMethod.POST,
      params: {"head_icon": head},
      onSuccess: (res) => onModify(res, head),
    );
  }

  void onModify(response, String head) {
    if (response == null) return;
    if (response?["status"] == 'REVIEW') {
      WidgetUtils().showToast("等待头像审核");
      headUrl = head;
      headStatus = 1;
      if (!mounted) return;
      setState(() {});
    } else if (response?["REJECT"] == 'REVIEW') {
      requestHeadCheck();
    } else {
      UserConstants.userginInfo?.info.headIcon = head;
      headUrl = head;
      if (!mounted) return;
      setState(() {});
    }
  }

  void requestCheck(String url) {
    // DioUtils().asyncHttpRequest(NetConstants.checkHead,
    //     method: DioMethod.POST,
    //     params: {"url": url},
    //     onSuccess: (_) => onCheck(url));
  }

  // 检测头像审核状态{"Url":"head-image/Fk6WXS67I0oLuHtgSW7a4yTS3WT2.png","Status":0,"Reason":"","CreateTime":1635344150967}
  // 状态 1有头像审核中 2无头像审核中
  void onHeadStatus(response) {
    String head = response?["img_url"] ?? "";
    headStatus = response?["status"] ?? 0;
    if (headStatus == 1) {
      headUrl = head;
    } else {
      headUrl = head;
    }
    if (!mounted) return;
    setState(() {});
  }

  void onCancel(response) {
    headStatus = 2;
    headUrl = UserConstants.userginInfo?.info.headIcon ?? "";
    if (!mounted) return;
    setState(() {});
  }

  void onCheck(response) {
    WidgetUtils().showToast("等待头像审核");
    headUrl = response;
    headStatus = 1;
    if (!mounted) return;
    setState(() {});
  }

  final ImagePicker picker = ImagePicker();

  void onTapHead() async {
    bool accept = await FuncUtils().requestPermission(Permission.photos);
    if (!accept) return;

    XFile? file = await picker.pickImage(source: ImageSource.gallery);
    if (file == null || file.path.isEmpty) return;
    CroppedFile? crop = await ImageCropper().cropImage(
      sourcePath: file.path,
      compressQuality: 100,
      cropStyle: CropStyle.circle,
      uiSettings: [
        AndroidUiSettings(toolbarTitle: '裁剪', statusBarColor: Colors.white),
        IOSUiSettings(title: '裁剪')
      ],
    );
    if (crop == null || crop.path.isEmpty) return;

    WidgetUtils().showLoading();
    String? url =
        await FuncUtils().requestUpload(crop.path, UploadType.USER_IMAGE);
    if (url == null) return WidgetUtils().cancelLoading();
    if (url.isEmpty) {
      WidgetUtils().showToast("头像上传失败");
      WidgetUtils().cancelLoading();
      return;
    }
    await requestModify(url);
    WidgetUtils().cancelLoading();
  }

  void onTapCancel() async {
    bool? accept = await WidgetUtils().showAlert("是否取消审核？");
    if (accept == null || !accept) return;
    requestCancel();
  }
}
