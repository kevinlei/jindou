import 'dart:io';

import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class AudioContent extends StatefulWidget {
  final String audioPath;

  const AudioContent({Key? key, required this.audioPath}) : super(key: key);

  @override
  _AudioContentState createState() => _AudioContentState();
}

class _AudioContentState extends State<AudioContent> {
  String audioPath = "";

  @override
  void initState() {
    super.initState();
    audioPath = widget.audioPath;
  }

  @override
  Widget build(BuildContext context) {
    return SetListItem("语音介绍",
        suffix: Visibility(
          visible: audioPath.isNotEmpty,
          child: Row(children: [
            LoadAssetImage("person/audios", width: 120.w),
            SizedBox(width: 10.w),
            GestureDetector(
              onTap: onTapDelete,
              child: FContainer(
                width: 80.w,
                height: 80.w,
                child: Icon(Icons.cancel, color: Color(0xFFBBBBBB)),
              ),
            )
          ]),
        ),
        onPress: onTapAudio);
  }

  void onTapAudio() async {
    String? audio =
        await WidgetUtils().pushPage(UserAudioPage(audioUrl: audioPath));
    if (audio == null || audio.isEmpty) return;
    if (!await File(audio).exists()) return;
    requestAddAudio(audio);
  }

  void onTapDelete() async {
    bool? accept = await WidgetUtils().showAlert("确定删除音频？");
    if (accept == null || !accept) return;
    requestDelete(audioPath);
  }

  void requestAddAudio(String audio) async {
    String? path = await FuncUtils().requestUploadAudio(audio, UploadType.DYNAMIC_SQUARE);
    if (path == null) return;
    if (path.isEmpty) return WidgetUtils().showToast("添加音频失败");

    DioUtils().asyncHttpRequest(
      NetConstants.modifyUser,
      method: DioMethod.POST,
      params: {"voice": path},
      onSuccess: (_) => onAddAudio(path),
    );
  }

  void onAddAudio(String url) {
    audioPath = url;
    if (!mounted) return;
    setState(() {});
  }

  void requestDelete(String url) {
    if (url.isEmpty) return;
    DioUtils().asyncHttpRequest(
      NetConstants.modifyUser,
      method: DioMethod.POST,
      params: {
        "voice": ""
      },
      onSuccess: (_) => onDelPhoto(url),
    );
  }

  void onDelPhoto(String url) {
    audioPath = "";
    if (!mounted) return;
    setState(() {});
  }
}
