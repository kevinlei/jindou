import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class XdContent extends StatefulWidget {
  @override
  _XdContentState createState() => _XdContentState();
}

class _XdContentState extends State<XdContent> {
  @override
  Widget build(BuildContext context) {
    int sex = UserConstants.userginInfo?.info.sex ?? 0;
    return SetListItem(
      "性别",
      onPress: onTapCity,
      suffix: FText(sex == 1 ? "男" : "女",
          size: 26.sp, color: const Color(0xFF666666)),
    );
  }

  void onTapCity() async {
    int sex = (UserConstants.userginInfo?.info.sex ?? 0);
    String? data =
        await WidgetUtils().showBottomSheet(XdentPicker(), color: Colors.white);
    if (data == "男") {
      sex = 1;
    } else {
      sex = 0;
    }
    requestModify(sex);
  }

  void requestModify(int job) {
    DioUtils().asyncHttpRequest(
      NetConstants.modifyUser,
      method: DioMethod.POST,
      params: {"sex": job},
      onSuccess: (_) => onModify(job),
    );
  }

  void onModify(int job) {
    UserConstants.userginInfo?.info.sex = job;
    if (!mounted) return;
    setState(() {});
  }
}
