import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class CityContent extends StatefulWidget {
  @override
  _CityContentState createState() => _CityContentState();
}

class _CityContentState extends State<CityContent> {
  @override
  Widget build(BuildContext context) {
    String city = UserConstants.userginInfo?.info.city ?? "";
    return SetListItem(
      "城市",
      onPress: onTapCity,
      suffix: FText(city, size: 26.sp, color: const Color(0xFF666666)),
    );
  }

  void onTapCity() async {
    String city = UserConstants.userginInfo?.info.city ?? "";
    String? data =
        await WidgetUtils().showBottomSheet(CityPicker(), color: Colors.white);
    if (data == null || data.isEmpty || data == city) return;
    requestModify(data);
  }

  void requestModify(String city) {
    DioUtils().asyncHttpRequest(
      NetConstants.modifyUser,
      method: DioMethod.POST,
      params: {"city": city},
      onSuccess: (_) => onModify(city),
    );
  }


  void onModify(String city) {
    UserConstants.userginInfo?.info.city = city;
    if (!mounted) return;
    setState(() {});
  }
}
