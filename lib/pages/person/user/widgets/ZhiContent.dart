import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class ZhiContent extends StatefulWidget {
  @override
  _ZhiContentState createState() => _ZhiContentState();
}

class _ZhiContentState extends State<ZhiContent> {
  @override
  Widget build(BuildContext context) {
    String city = UserConstants.userginInfo?.info.job ?? "";
    return SetListItem(
      "职业",
      onPress: onTapCity,
      suffix: FText(city, size: 26.sp, color: const Color(0xFF666666)),
    );
  }

  void onTapCity() async {
    String job = UserConstants.userginInfo?.info.job ?? "";
    String? data =
        await WidgetUtils().showBottomSheet(ZhiPicker(), color: Colors.white);
    if (data == null || data.isEmpty || data == job) return;
    requestModify(data);
  }

  void requestModify(String job) {
    DioUtils().asyncHttpRequest(
      NetConstants.modifyUser,
      method: DioMethod.POST,
      params: {"job": job},
      onSuccess: (_) => onModify(job),
    );
  }

  void onModify(String job) {
    UserConstants.userginInfo?.info.job = job;
    if (!mounted) return;
    setState(() {});
  }
}
