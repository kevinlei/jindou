import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class DescribeContent extends StatefulWidget {
  @override
  _DescribeContentState createState() => _DescribeContentState();
}

class _DescribeContentState extends State<DescribeContent> {
  @override
  Widget build(BuildContext context) {
    String des = UserConstants.userginInfo?.info.sign ?? "";
    return SetListItem("个性签名",
        onPress: onTapDes,
        suffix:  SizedBox(width: 300.w,child: FText(des,size: 26.sp,overflow: TextOverflow.ellipsis, color: Color(0xFF666666)),),);
  }

  void onTapDes() async {
    String des = UserConstants.userginInfo?.info.sign ?? "";
    String? text = await WidgetUtils().pushPage(UserInput(false, des, "个性签名"));
    if (text == null || text.isEmpty || text == des) return;
    requestModify(text);
  }

  void requestModify(String des) {
    DioUtils().asyncHttpRequest(
      NetConstants.modifyUser,
      method: DioMethod.POST,
      params: {"sign": des},
      onSuccess: (_) => onModify(des),
    );
  }

  void onModify(String des) {
    UserConstants.userginInfo?.info.sign = des;
    if (!mounted) return;
    setState(() {});
  }
}
