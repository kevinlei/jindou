import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class AgreContent extends StatefulWidget {
  @override
  _AgreContentState createState() => _AgreContentState();
}

class _AgreContentState extends State<AgreContent> {
  @override
  Widget build(BuildContext context) {
    int bir = UserConstants.userginInfo?.info.birthday ?? 0;
    String time =
        DateTime.fromMillisecondsSinceEpoch(bir).toString().substring(0, 10);
    return SetListItem(
      "生日",
      onPress: onTapCity,
      suffix: FText(time, size: 26.sp, color: const Color(0xFF666666)),
    );
  }

  void onTapCity() async {
    int? time = await WidgetUtils().showBottomSheet(AgePicker());
    if (time == null) return;
    requestModify(time);
  }

  void requestModify(int job) {
    DioUtils().asyncHttpRequest(
      NetConstants.modifyUser,
      method: DioMethod.POST,
      params: {"birthday": job},
      onSuccess: (_) => onModify(job),
    );
  }

  void onModify(int job) {
    UserConstants.userginInfo?.info.birthday = job;
    if (!mounted) return;
    setState(() {});
  }
}
