import 'dart:async';
import 'dart:io';
import 'package:audioplayers/audioplayers.dart';
import 'package:echo/import.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart' show Level;
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import '../../../../widgets/CommonAppBar.dart';

class UserAudioPage extends StatefulWidget {
  final String audioUrl;

  const UserAudioPage({Key? key, required this.audioUrl}) : super(key: key);

  @override
  _UserAudioPageState createState() => _UserAudioPageState();
}

class _UserAudioPageState extends State<UserAudioPage> {
  FlutterSoundPlayer soundPlayer = FlutterSoundPlayer(logLevel: Level.error);
  FlutterSoundRecorder soundRecorder =
      FlutterSoundRecorder(logLevel: Level.error);

  StreamSubscription? recorderStream, playerStream;
  String recordPath = "", recordTime = "", playTime = "";

  SoundRecordStatus recordStatus = SoundRecordStatus.DEF;
  int maxTime = 10, audioTime = 0;

  @override
  void initState() {
    super.initState();
    initPlayer();
    startPlayer();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: CommonAppBar(title: "语音介绍"),
      body: Column(children: [
        SizedBox(height: 40.h),
        AudioSwipe(),
        SizedBox(height: 20.h),
        Row(children: [Spacer(), buttonTest(), SizedBox(width: 25.w)]),
        Spacer(),
        FText(recordStatus == SoundRecordStatus.PLAYING ? playTime : recordTime,
            nullLength: 0, size: 36.sp),
        SizedBox(width: double.infinity, height: 40.h),
        GestureDetector(
            onLongPress: onLongPress,
            onLongPressEnd: onLongPressEnd,
            onLongPressCancel: onLongPressCancel,
            onTap: onTapAudio,
            child: getAudioImg()),
        SizedBox(height: 20.h),
        FText(getAudioTxt(),
            size: 26.sp, nullLength: 0, color: Color(0xFF666666)),
        SizedBox(height: ScreenUtil().bottomBarHeight + 20.h)
      ], crossAxisAlignment: CrossAxisAlignment.center),
    );
  }

  @override
  void dispose() {
    cancelStream();
    disposePlayer();
    super.dispose();
  }

  Widget buttonDelete() {
    return GestureDetector(
      onTap: onTapDelete,
      child: LoadAssetImage("person/a2", width: 80.w, height: 80.w),
    );
  }

  Widget buttonConfirm() {
    return GestureDetector(
      onTap: onTapConfirm,
      child: LoadAssetImage("person/a1", width: 80.w, height: 80.w),
    );
  }

  Widget getAudioImg() {
    switch (recordStatus) {
      case SoundRecordStatus.DEF:
        return LoadAssetImage("person/audio1", width: 210.w, height: 210.w);
      case SoundRecordStatus.RECORDING:
        return LoadAssetImage("person/audio1", width: 210.w, height: 210.w);
      case SoundRecordStatus.READY:
        return Row(children: [
          buttonDelete(),
          SizedBox(width: 60.w),
          LoadAssetImage("person/audio2", width: 210.w, height: 210.w),
          SizedBox(width: 60.w),
          buttonConfirm(),
        ], mainAxisAlignment: MainAxisAlignment.center);
      case SoundRecordStatus.PLAYING:
        return LoadAssetImage("person/audio3", width: 210.w, height: 210.w);
    }
  }

  Widget buttonTest() {
    Widget btn = recordStatus == SoundRecordStatus.READY
        ? LoadAssetImage("person/test1", width: 100.w)
        : LoadAssetImage("person/test2", width: 120.w);

    return Visibility(
      child: GestureDetector(onTap: onTapTest, child: btn),
      visible: (widget.audioUrl.isNotEmpty || recordPath.isNotEmpty) &&
          recordStatus != SoundRecordStatus.RECORDING,
    );
  }

  String getAudioTxt() {
    switch (recordStatus) {
      case SoundRecordStatus.DEF:
        return "按住说话";
      case SoundRecordStatus.RECORDING:
        return "松手停止录制";
      case SoundRecordStatus.READY:
        return "";
      case SoundRecordStatus.PLAYING:
        return "";
    }
  }

  void initPlayer() async {
    await soundPlayer.closePlayer();
    await soundPlayer.openPlayer();
    await soundPlayer.setSubscriptionDuration(Duration(milliseconds: 50));
    await soundRecorder.openRecorder();
    await soundRecorder.setSubscriptionDuration(Duration(milliseconds: 50));
  }

  void cancelStream() {
    playerStream?.cancel();
    playerStream = null;
    recorderStream?.cancel();
    recorderStream = null;
  }

  void disposePlayer() async {
    try {
      await soundPlayer.closePlayer();
      await soundRecorder.closeRecorder();
    } catch (err) {
      debugPrint("释放FlutterSound出错：$err");
    }
  }

  void startRecorder() async {
    bool auth = await FuncUtils().requestPermission(Permission.microphone);
    if (!auth) return;
    try {
      Directory tempDir = await getTemporaryDirectory();
      int timeStamp = DateTime.now().millisecondsSinceEpoch ~/ 1000;
      recordPath =
          '${tempDir.path}/audioRecord-$timeStamp${ext[Platform.isAndroid ? Codec.aacADTS.index : Codec.pcm16WAV.index]}';
      print("录音文件路径：$recordPath");
      await soundRecorder.startRecorder(
          toFile: recordPath,
          codec: Platform.isAndroid ? Codec.aacADTS : Codec.pcm16WAV,
          bitRate: 8000,
          numChannels: 1,
          sampleRate: 8000);
      onRecorderListener();
    } on Exception catch (err) {
      debugPrint("录音出错：$err");
      stopRecorder();
    }
  }

  void onRecorderListener() {
    cancelStream();
    recorderStream = soundRecorder.onProgress!.listen((e) {
      int time = e.duration.inMilliseconds;
      DateTime date = DateTime.fromMillisecondsSinceEpoch(time, isUtc: true);
      recordTime = date.toString().substring(14, 19);
      audioTime = date.second;
      debugPrint("录音时长：${date.toString().substring(14, 19)}");
      recordStatus = SoundRecordStatus.RECORDING;
      if (date.second >= maxTime) {
        return stopRecorder();
      }
      if (!mounted) return;
      setState(() {});
    });
  }

  void stopRecorder() async {
    try {
      await soundRecorder.stopRecorder();
    } on Exception catch (err) {
      debugPrint("停止录音错误：$err");
    } finally {
      cancelStream();
      recordStatus = SoundRecordStatus.READY;
    }
    if (!mounted) return;
    setState(() {});
  }

  void startPlayer() async {
    debugPrint('usreAudio的播放语音');
    try {
      if (recordPath.isNotEmpty) {
        bool exit = await recordPathExit();
        if (!exit) return WidgetUtils().showToast("播放路径出错");
      } else {
        if (widget.audioUrl.isEmpty) return WidgetUtils().showToast("无试听内容");
      }

      String path = recordPath.isNotEmpty
          ? recordPath
          : (NetConstants.ossPath + widget.audioUrl);


      bool state = path.contains('wav');

      // final player = AudioPlayer();
      // await player.play(UrlSource(path));


      await soundPlayer.startPlayer(
          fromURI:path,
          codec: !state ? Codec.aacADTS : Codec.pcm16WAV,
          sampleRate: 8000,
          whenFinished: onPlayFinish);
      onPlayerListener();
    } on Exception catch (err) {
      debugPrint("播放录音错误：$err");
      stopPlayer();
    }



  }

  void stopPlayer() async {
    try {
      await soundPlayer.stopPlayer();
    } on Exception catch (err) {
      debugPrint("停止播放出错：$err");
    } finally {
      cancelStream();
      recordStatus = SoundRecordStatus.READY;
    }
    if (!mounted) return;
    setState(() {});
  }

  void onPlayerListener() {
    cancelStream();
    playerStream = soundPlayer.onProgress!.listen((event) {
      int time = event.position.inMilliseconds;
      DateTime date = DateTime.fromMillisecondsSinceEpoch(time, isUtc: true);
      playTime = date.toString().substring(14, 19);
      debugPrint("播放时长：${date.toString().substring(14, 19)}");
      recordStatus = SoundRecordStatus.PLAYING;
      if (!mounted) return;
      setState(() {});
    });
  }

  void onPlayFinish() {
    recordStatus = SoundRecordStatus.READY;
    if (!mounted) return;
    setState(() {});
  }

  void onLongPress() {
    if (recordStatus != SoundRecordStatus.DEF) return;
    startRecorder();
  }

  void onLongPressEnd(de) {
    switch (recordStatus) {
      case SoundRecordStatus.DEF:
        break;
      case SoundRecordStatus.RECORDING:
        stopRecorder();
        break;
      case SoundRecordStatus.READY:
        break;
      case SoundRecordStatus.PLAYING:
        break;
    }
  }

  void onLongPressCancel() {
    switch (recordStatus) {
      case SoundRecordStatus.DEF:
        break;
      case SoundRecordStatus.RECORDING:
        stopRecorder();
        break;
      case SoundRecordStatus.READY:
        break;
      case SoundRecordStatus.PLAYING:
        break;
    }
  }

  void onTapAudio() {
    switch (recordStatus) {
      case SoundRecordStatus.DEF:
        break;
      case SoundRecordStatus.RECORDING:
        stopRecorder();
        break;
      case SoundRecordStatus.READY:
        break;
      case SoundRecordStatus.PLAYING:
        break;
    }
  }

  void onTapTest() {
    switch (recordStatus) {
      case SoundRecordStatus.DEF:
        break;
      case SoundRecordStatus.RECORDING:
        break;
      case SoundRecordStatus.READY:
        startPlayer();
        break;
      case SoundRecordStatus.PLAYING:
        stopPlayer();
        break;
    }
  }

  void onTapDelete() {
    recordStatus = SoundRecordStatus.DEF;
    recordTime = "";
    recordPath = "";
    audioTime = 0;
    if (!mounted) return;
    setState(() {});
  }

  void onTapConfirm() async {
    bool exit = await recordPathExit();
    if (!exit) {
      return WidgetUtils().showToast("录音路径错误");
    }
    if (audioTime > maxTime) return WidgetUtils().showToast("录音时间过长");
    WidgetUtils().popPage(recordPath);

  }

  Future<bool> recordPathExit() async {
    if (recordPath.isEmpty) return false;
    bool exit = await File(recordPath).exists();
    return exit;
  }
}
