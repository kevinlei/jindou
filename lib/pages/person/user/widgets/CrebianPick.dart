import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class CrebianPick extends StatefulWidget {

  const CrebianPick({Key? key}) : super(key: key);
  @override
  _CreateAgeState createState() => _CreateAgeState();
}

class _CreateAgeState extends State<CreateAge> {
  int timeStamp =0;

  @override
  void initState() {
    super.initState();
    timeStamp=widget.timeStamps??0;
  }
  @override
  Widget build(BuildContext context) {
    List<Widget> children = [

      Expanded(child: ageContent()),
      SizedBox(width: 20.w),
      LoadAssetImage("login/huitou", width: 30.w),
    ];

    BoxShadow shadow = BoxShadow(
        offset: Offset(3, 5),
        color: Color(0xFF3C3C3C).withOpacity(0.06),
        blurRadius: 10.0);
    Widget btn = FContainer(
      height: 94.w,
      color: Color(0xFF222222).withOpacity(0.05),
      shadow: [shadow],
      radius: BorderRadius.circular(47.w),
      padding: EdgeInsets.only(left: 40.w, right: 20.w),
      margin: EdgeInsets.symmetric(vertical: 20.h),
      child: Row(children: children),
    );
    return GestureDetector(onTap: onPress, child: btn);
  }

  Widget ageContent() {
    if (timeStamp == 0) {
      return FText("请选择生日", color:Color(0xFF222222), size: 32.sp);
    }
    DateTime dateTime = DateTime.fromMillisecondsSinceEpoch(timeStamp);
    return FText("${dateTime.year}-${dateTime.month}-${dateTime.day}",
        size: 32.sp,color: Colors.black);
  }

  void onPress() async {
    FocusManager.instance.primaryFocus?.unfocus();
    int? time = await WidgetUtils().showBottomSheet(AgePicker());
    if (time == null) return;
    timeStamp = time;
    widget.onChange(timeStamp);
    if (!mounted) return;
    setState(() {});
  }
}
