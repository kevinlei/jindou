import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';

class ImgprsonContent extends StatefulWidget {
  final List<String> urlList;

  const ImgprsonContent({Key? key, required this.urlList}) : super(key: key);

  @override
  _ImgprsonContentState createState() => _ImgprsonContentState();
}

class _ImgprsonContentState extends State<ImgprsonContent> {
  List<String> urlList = [];
  LoginProvider provider = LoginProvider();
  @override
  void initState() {
    super.initState();
    urlList = widget.urlList;
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> children =
        List.generate(urlList.length, (index) => imgCell(index));
    if (urlList.length < 4) {
      children.add(btnAdd());
    }
    return Padding(
      padding: EdgeInsets.fromLTRB(5.w, 0, 5.w, 0),
      child: Wrap(children: children, spacing: 10.w, runSpacing: 10.w),
    );
  }

  Widget btnAdd() {
    Widget btn = FContainer(
      width: 150.w,
      height: 150.w,
      radius: BorderRadius.circular(10.w),
      color: Color(0xFFEDEEF3),
      child: Icon(Icons.add, color: Color(0xFFBBBBBB)),
    );
    return GestureDetector(onTap: onTapAdd, child: btn);
  }

  Widget imgCell(int index) {
    Widget btn = FContainer(
      width: 150.w,
      height: 150.w,
      image:
          ImageUtils().getImageProvider(NetConstants.ossPath + urlList[index]),
      imageFit: BoxFit.cover,
      radius: BorderRadius.circular(10.w),
      color: Color(0xFFEDEEF3),
      align: Alignment.topRight,
      child: btnDelete(index),
    );
    return GestureDetector(onTap: () => onTapCell(index), child: btn);
  }

  Widget btnDelete(int index) {
    return SizedBox(
      width: 50.w,
      height: 40.w,
      child: IconButton(
        padding: EdgeInsets.zero,
        color: Color(0xFF1E1C2D),
        icon: Icon(Icons.cancel),
        onPressed: () => onTapDelete(index),
      ),
    );
  }

  void onTapDelete(int index) async {
    bool? accept = await WidgetUtils().showAlert("确定删除照片？");
    if (accept == null || !accept) return;
    requestDelete(urlList[index]);
  }

  final ImagePicker picker = ImagePicker();

  void onTapAdd() async {
    bool accept = await FuncUtils().requestPermission(Permission.photos);
    if (!accept) return;
    XFile? file = await picker.pickImage(source: ImageSource.gallery);
    if (file == null || file.path.isEmpty) return;
    requestAddPhoto(file.path);
  }

  void onTapCell(int index) {
    WidgetUtils().pushPage(PhotoPreview(urlList, index: index));
  }

  void requestAddPhoto(String photo) async {
    WidgetUtils().showLoading();
    String? path =
        await FuncUtils().requestUpload(photo, UploadType.DYNAMIC_AUIDO);
    if (path == null) return WidgetUtils().cancelLoading();
    if (path.isEmpty) {
      WidgetUtils().showToast("上传照片失败");
      WidgetUtils().cancelLoading();
      return;
    }
    // bool lawful =
    //     await FuncUtils().checkImageLawful([path], CheckImgType.IMG_BGC);
    // if (!lawful) return WidgetUtils().cancelLoading();
    // WidgetUtils().cancelLoading();
    DioUtils().asyncHttpRequest(
      NetConstants.photoWall,
      method: DioMethod.POST,
      params: {"photo_url": path, "update_type": 1},
      onSuccess: (_) => onAddPhoto(path),
    );
  }

  void onAddPhoto(String url) {
    urlList.add(url);
    provider.urlList = urlList;
    if (!mounted) return;
    setState(() {});
  }

  void requestDelete(String url) {
    if (url.isEmpty) return;
    DioUtils().asyncHttpRequest(
      NetConstants.photoWall,
      method: DioMethod.POST,
      params: {"photo_url": url, "update_type": 2},
      onSuccess: (_) => onDelPhoto(url),
    );
  }

  void onDelPhoto(String url) {
    urlList.remove(url);
    provider.urlList = urlList;
    if (!mounted) return;
    setState(() {});
  }
}
