import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class XdentPicker extends StatefulWidget {
  @override
  _XdentPickerState createState() => _XdentPickerState();
}

class _XdentPickerState extends State<XdentPicker> {
  late FixedExtentScrollController proCtr;
  String curProCode = "";

  Map<String, String> get proList => {
    "0": "男",
    "1": "女",
  };

  @override
  void initState() {
    super.initState();
    initWheelContent();
  }

  @override
  void dispose() {
    proCtr.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      color: Color(0xFFFFFFFF),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color:  Color(0xFFFFFFFF),
      ),
      padding: EdgeInsets.only(
          top: 10.w, bottom: ScreenUtil().bottomBarHeight + 20.w),
      child: Column(children: [
        SizedBox(height: 30.w),
        FText("请选择性别", size: 32.sp, weight: FontWeight.bold, color: Color(0xFF222222)),
        FSheetLine(),
        pickerContent(),
        Row(children: [btnCancel(), btnConfirm()])
      ], mainAxisSize: MainAxisSize.min),
    );
  }

  Widget pickerContent() {
    Widget content = Row(children: [
      PickerWheel(pickerItem(proList, proCtr), onSelect: onProvinceChange),
    ]);
    return SizedBox(height: 360.h, child: content);
  }

  Widget pickerItem(Map list, FixedExtentScrollController controller) {
    BorderSide borderSide = BorderSide(color: Color(0xFFEDEEF3), width: 2.w);
    return CupertinoPicker.builder(
        childCount: list.length,
        itemExtent: 100.h,
        // selectionOverlay: FContainer(border: Border(top: borderSide, bottom: borderSide)),
        scrollController: controller,
        onSelectedItemChanged: null,
        itemBuilder: (_, index) => cityItem(list.values.toList()[index]));
  }

  Widget cityItem(String text) {
    return Center(child: FText(text,color: Colors.black,size: 32.sp));
  }

  Widget btnCancel() {
    Widget btn = FContainer(
      height: 90.w,
      margin: EdgeInsets.symmetric(horizontal: 50.w),
      radius: BorderRadius.circular(90.w),
      border: Border.all(color: const Color(0xFFFF753F)),
      align: Alignment.center,
      child: FText("取消", size: 32.sp, weight: FontWeight.bold, color: Color(0xFFFF753F)),
    );
    return Flexible(child: GestureDetector(child: btn, onTap: onCancel));
  }

  Widget btnConfirm() {
    Widget btn = FContainer(
      height: 90.w,
      gradientBegin: Alignment.topLeft,
      gradientEnd: Alignment.bottomRight,
      gradient: [Color(0xFFFF753F),Color(0xFFFF753F),Color(0xFFFF753F)],
      margin: EdgeInsets.symmetric(horizontal: 50.w),
      radius: BorderRadius.circular(90.w),
      align: Alignment.center,
      child: FText("确定", size: 32.sp,color: Colors.white, weight: FontWeight.bold),
    );
    return Flexible(child: GestureDetector(child: btn, onTap: onConfirm));
  }

  void initWheelContent() {
    curProCode = proList.keys.first;
    proCtr = FixedExtentScrollController(initialItem: 0);
  }

  void onProvinceChange() {
    curProCode = proList.keys.toList()[proCtr.selectedItem];
    if (!mounted) return;
    setState(() {});
  }

  void onCancel() {
    WidgetUtils().popPage();
  }

  void onConfirm() {
    String proCode = proList.keys.toList()[proCtr.selectedItem];
    String pro = proList[proCode] ?? "";
    WidgetUtils().popPage("$pro");
  }
}
