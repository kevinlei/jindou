import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class ZhiPicker extends StatefulWidget {
  @override
  _ZhiPickerState createState() => _ZhiPickerState();
}

class _ZhiPickerState extends State<ZhiPicker> {
  late FixedExtentScrollController proCtr;
  String curProCode = "";
  List<AugsModel> audioConfig = [];

  @override
  void initState() {
    super.initState();
    requestAudioConfig();
    initWheelContent();
  }

  @override
  void dispose() {
    proCtr.dispose();
    super.dispose();
  }

  void requestAudioConfig() {
    DioUtils().asyncHttpRequest(
      NetConstants.recordAudio,
      params: {"conf_key": 'job_id_list'},
      method: DioMethod.GET,
      onSuccess: onAudioConfig,
    );
  }

  // {"id":2,"content":"看，这么大的世界\n\n我遇到了唯一的你\n\n你也遇到了唯一的我\n\n真好~",
  // "picture":"admin-tapeSampleImg/Fpg5CAxizT7b3rNe68BQFE0sHxSU.png"}
  void onAudioConfig(response) {
    var jsonStr = jsonDecode(response);
    jsonStr ??= [];
    audioConfig.clear();
    for (var data in jsonStr) {
      audioConfig.add(AugsModel.fromJson(data));
    }
    if (!mounted) return;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      color: Color(0xFFFFFFFF),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: Color(0xFFFFFFFF),
      ),
      padding: EdgeInsets.only(
          top: 10.w, bottom: ScreenUtil().bottomBarHeight + 20.w),
      child: Column(mainAxisSize: MainAxisSize.min, children: [
        SizedBox(height: 30.w),
        FText("选择职业",
            size: 32.sp, weight: FontWeight.bold, color: Color(0xFF222222)),
        FSheetLine(),
        pickerContent(),
        Row(children: [btnCancel(), btnConfirm()])
      ]),
    );
  }

  Widget pickerContent() {
    Widget content = Row(children: [
      PickerWheel(pickerItem(audioConfig, proCtr), onSelect: onProvinceChange),
      // PickerWheel(pickerItem(cityList[curProCode], cityCtr)),
    ]);
    return SizedBox(height: 360.h, child: content);
  }

  Widget pickerItem(
      List<AugsModel> audioConfig, FixedExtentScrollController controller) {
    BorderSide borderSide = BorderSide(color: Color(0xFFEDEEF3), width: 2.w);
    return CupertinoPicker.builder(
        childCount: audioConfig.length,
        itemExtent: 100.h,
        // selectionOverlay: FContainer(border: Border(top: borderSide, bottom: borderSide)),
        scrollController: controller,
        onSelectedItemChanged: null,
        itemBuilder: (_, index) =>
            cityItem(audioConfig[index].name.toString()));
  }

  Widget cityItem(String text) {
    return Center(child: FText(text, color: Colors.black, size: 32.sp));
  }

  Widget btnCancel() {
    Widget btn = FContainer(
      height: 90.w,
      margin: EdgeInsets.symmetric(horizontal: 50.w),
      radius: BorderRadius.circular(90.w),
      border: Border.all(color: const Color(0xFFFF753F)),
      align: Alignment.center,
      child: FText("取消",
          size: 32.sp, weight: FontWeight.bold, color: Color(0xFFFF753F)),
    );
    return Flexible(child: GestureDetector(onTap: onCancel, child: btn));
  }

  Widget btnConfirm() {
    Widget btn = FContainer(
      height: 90.w,
      gradientBegin: Alignment.topLeft,
      gradientEnd: Alignment.bottomRight,
      gradient: const [Color(0xFFFF753F), Color(0xFFFF753F), Color(0xFFFF753F)],
      margin: EdgeInsets.symmetric(horizontal: 50.w),
      radius: BorderRadius.circular(90.w),
      align: Alignment.center,
      child: FText("确定",
          size: 32.sp, color: Colors.white, weight: FontWeight.bold),
    );
    return Flexible(child: GestureDetector(onTap: onConfirm, child: btn));
  }

  void initWheelContent() {
    proCtr = FixedExtentScrollController(initialItem: 0);
  }

  void onProvinceChange() {
    curProCode = audioConfig[proCtr.selectedItem].id.toString();
    if (!mounted) return;
    setState(() {});
  }

  void onCancel() {
    WidgetUtils().popPage();
  }

  void onConfirm() {
    String proCode = audioConfig[proCtr.selectedItem].id.toString();
    String pro = audioConfig[proCtr.selectedItem].name.toString();
    debugPrint(proCode);
    debugPrint(pro);
    WidgetUtils().popPage("$pro");
  }
}
