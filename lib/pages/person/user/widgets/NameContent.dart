import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class NameContent extends StatefulWidget {
  @override
  _NameContentState createState() => _NameContentState();
}

class _NameContentState extends State<NameContent> {
  @override
  Widget build(BuildContext context) {
    String name = UserConstants.userginInfo?.info.name ?? "";
    return SetListItem("昵称",
        onPress: onTapName,
        suffix: FText(name, size: 26.sp, color: const Color(0xFF666666)));
  }

  void onTapName() async {
    String name = UserConstants.userginInfo?.info.name ?? "";
    String? text = await WidgetUtils().pushPage(UserInput(true, name, "昵称修改"));
    if (text == null || text.isEmpty || text == name) return;
    requestModify(text);
  }

  void requestModify(String name) {
    DioUtils().asyncHttpRequest(
      NetConstants.modifyUser,
      method: DioMethod.POST,
      params: {"name": name},
      onSuccess: (_) => onModify(name),
    );
  }

  void onModify(String name) {
    UserConstants.userginInfo?.info.name = name;
    if (!mounted) return;
    setState(() {});
  }
}
