import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class DressHome extends StatefulWidget {
  @override
  _DressHomeState createState() => _DressHomeState();
}

class _DressHomeState extends State<DressHome>
    with SingleTickerProviderStateMixin {
  // {"id":1,"title":"头像框"},{"id":2,"title":"坐骑"},{"id":3,"title":"入场特效"},{"id":5,"title":"全服广播"},{"id":7,"title":"聊天气泡"},
  // {"id":8,"title":"套装"},{"id":9,"title":"声波"},{"id":6,"title":"公屏气泡"},{"id":4,"title":"昵称挂件"}
  late PageController controller;

  List tabList = [];
  TabController? tabCtr;

  List bannerList = [];
  int curIndex = -1;

  @override
  void initState() {
    super.initState();
    requestMenu();
    requestBanner();
    controller = PageController(initialPage: 0);
  }

  void requestBanner() {
    DioUtils().asyncHttpRequest(
      NetConstants.banner,
      loading: false,
      params: {"type": 4},
      onSuccess: onBannerList,
    );
  }

  void onBannerList(response) {
    bannerList.clear();
    bannerList = response ?? [];
    if (!mounted) return;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CommonCard(
        child: Column(children: [
          CardAppBar(title: "装扮商城", actions: [
            TextButton(
                onPressed: onTapSelf,
                child: FText("我的装扮", color: Colors.black.withOpacity(0.6)))
          ]),
          SizedBox(height: 40.h),
          PustList(),
          const BannerSwipe(4),
          PriceVider(),
        ]),
      ),
    );
  }

  @override
  void dispose() {
    tabCtr?.dispose();
    tabCtr = null;
    super.dispose();
  }

  Widget PustList() {
    return Column(
      children: [
        SizedBox(
          width: double.infinity,
          height: 150.w,
          child: ListView.separated(
            scrollDirection: Axis.horizontal,
            padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
            // physics: BouncingScrollPhysics(),
            separatorBuilder: (BuildContext context, int index) =>
                const VerticalDivider(
              width: 5.0,
              color: Colors.transparent,
            ),
            itemBuilder: (context, index) => dressIconCell(index),
            itemCount: tabList.length,
          ),
        )
      ],
    );
  }

  Widget dressIconCell(int index) {
    // ConfigGoods goods = tabList[index];
    String labelImg = tabList[index]["category_icon"] ?? "";
    Widget btn = FContainer(
      width: 120.w,
      color: Colors.transparent,
      radius: BorderRadius.circular(20.w),
      align: Alignment.center,
      padding: EdgeInsets.only(bottom: 10.w),
      child: Column(children: [
        Expanded(
          child: ClipRRect(
            //是ClipRRect，不是ClipRect
            borderRadius: BorderRadius.circular(23),
            child: Visibility(
              visible: labelImg.isNotEmpty,
              child: Image.network(
                NetConstants.ossPath + labelImg,
                width: 46,
                height: 46,
                fit: BoxFit.cover,
              ),
            ),

            // child: LoadNetImage( default )
          ),
        ),
        FText(tabList[index]["category_name"],
            weight: FontWeight.w300,
            color: const Color(0xFF9F9FB9),
            overflow: TextOverflow.ellipsis,
            size: 24.sp),
      ]),
    );

    btn = Stack(children: [
      btn,
      // Visibility(
      // visible: labelImg.isNotEmpty,
      // child: LoadNetImage(NetConstants.ossPath + labelImg,
      //     width: 64.w, height: 32.w)
      // ),
    ]);

    return GestureDetector(child: btn, onTap: () => onTapsRestDress());
  }

  //点击顶部图标按钮
  void onTapsRestDress() {
    WidgetUtils().pushPage(DressStore());
  }

  Widget PriceVider() {
    return Column(
      children: [
        SizedBox(
          width: double.infinity,
          height: 1200.h,
          child: ListView.separated(
            scrollDirection: Axis.vertical,
            padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
            // physics: BouncingScrollPhysics(),
            separatorBuilder: (BuildContext context, int index) =>
                const VerticalDivider(
              width: 5.0,
              color: Colors.transparent,
            ),
            itemBuilder: (context, index) => guessYouLikIt(),
            itemCount: 11,
          ),
        )
      ],
    );
  }

  Widget guessYouLikIt() {
    return Column(
      children: [
        Row(
          children: [
            SizedBox(width: 32.w),
            FText(
              "聊天气泡",
              color: Colors.black,
              size: 30.sp,
              weight: FontWeight.w600,
            ),
          ],
        ),
        SizedBox(height: 20.w),
        SizedBox(
          width: double.infinity,
          height: 240.h,
          child: ListView.separated(
            scrollDirection: Axis.horizontal,
            padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
            // physics: BouncingScrollPhysics(),
            separatorBuilder: (BuildContext context, int index) =>
                const VerticalDivider(
              width: 8.0,
              color: Colors.transparent,
            ),
            itemBuilder: (context, index) => dressCell(),
            itemCount: 11,
          ),
        ),
        SizedBox(height: 20.w),
      ],
    );
  }

  Widget dressCell() {
    // ConfigGoods goods = dressList[index];
    // String labelImg = goods.labelIcon ?? "";
    Widget btn = FContainer(
      width: 210.w,
      color: const Color(0xFFA5A5A5).withOpacity(0.1),
      radius: BorderRadius.circular(20.w),
      align: Alignment.center,
      padding: EdgeInsets.only(bottom: 20.w),
      child: Column(children: [
        Expanded(child: LoadAssetImage("login/default", width: 80.w)
            // child: LoadNetImage( default )
            ),
        FText('头像框名称',
            weight: FontWeight.w300,
            color: Colors.black,
            overflow: TextOverflow.ellipsis,
            size: 26.sp),
      ]),
    );

    btn = Stack(children: [
      btn,
      // Visibility(
      // visible: labelImg.isNotEmpty,
      // child: LoadNetImage(NetConstants.ossPath + labelImg,
      //     width: 64.w, height: 32.w)
      // ),
    ]);

    return GestureDetector(child: btn, onTap: () => onTapsDress());
  }

  //点击头像按钮
  void onTapsDress() {
    // WidgetUtils().showBottomSheet(
    //     DressBuyDialog(goods: "", tabName: ""));
  }

  void onTapSelf() {
    WidgetUtils().pushPage(DressPage());
  }

  void requestMenu() {
    DioUtils().asyncHttpRequest(
      NetConstants.dressMenu,
      method: DioMethod.GET,
      params: {"category_type": 1},
      onSuccess: onMenuSuccess,
    );
  }

  void onMenuSuccess(response) {
    tabList = response["CategoryInfo"] ?? [];
    tabCtr = TabController(length: tabList.length, vsync: this);

    if (!mounted) return;
    setState(() {});
  }
}
