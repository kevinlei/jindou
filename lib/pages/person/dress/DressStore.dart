import 'package:flutter/material.dart';
import 'package:echo/import.dart';

import 'DressPriceTable.dart';

class DressStore extends StatefulWidget {
  @override
  _DressStoreState createState() => _DressStoreState();
}

class _DressStoreState extends State<DressStore>
    with SingleTickerProviderStateMixin {
  // {"id":1,"title":"头像框"},{"id":2,"title":"坐骑"},{"id":3,"title":"入场特效"},{"id":5,"title":"全服广播"},{"id":7,"title":"聊天气泡"},
  // {"id":8,"title":"套装"},{"id":9,"title":"声波"},{"id":6,"title":"公屏气泡"},{"id":4,"title":"昵称挂件"}
  late PageController controller;

  List classificationList = ["关注", "管理", "创建"];
  List tabList = [];
  TabController? tabCtr;

  List bannerList = [];
  int curIndex = -1;

  @override
  void initState() {
    super.initState();
    requestMenu();
    //暂时屏蔽
    requestBanner();
    controller = PageController(initialPage: 0);
  }

  void requestBanner() {
    DioUtils().asyncHttpRequest(
      NetConstants.banner,
      loading: false,
      params: {"type": 4},
      onSuccess: onBannerList,
    );
  }

  void onBannerList(response) {
    bannerList.clear();
    bannerList = response ?? [];
    if (!mounted) return;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CommonCard(
        child: Stack(
          children: [
            Column(children: [
              CardAppBar(title: "装扮商城", actions: [
                TextButton(
                    onPressed: onTapSelf,
                    child: FText("我的装扮", color: Colors.black.withOpacity(0.6)))
              ]),
              if (tabCtr != null)
                DressTable(tabCtr: tabCtr!, tabList: tabList, tablest: "1"),
              SizedBox(height: 20.h),
              if (tabCtr != null) Expanded(child: dressList()),
            ]),
            btnPublish(),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    tabCtr?.dispose();
    tabCtr = null;
    super.dispose();
  }

  Widget btnPublish() {
    double bottom = ScreenUtil().bottomBarHeight + 20.w;
    return Positioned(
        right: 20.h,
        left: 20.h,
        bottom: bottom,
        child: GestureDetector(
          onTap: onPressPublish,
          child: FcationButton('立即购买',
              isActivate: true, onPress: onTapQuite, radius: 90.w),
        ));
  }

  void onTapQuite() async {}
  void onPressPublish() {
    // WidgetUtils().pushPage(PublishPage(widget.homePageCtr));
  }
  Widget dressList() {
    return FContainer(
      child: TabBarView(
        controller: tabCtr!,
        children: [
          ...List.generate(tabList.length, (index) {
            return FContainer(
              child: Column(
                children: [
                  dressImage(tabList[index]),
                  SizedBox(height: 20.h),
                  DressPriceTable(
                      titles: ["全部", "新品", "限定", "活动", "专属"],
                      menuData: tabList[index]),
                ],
              ),
              // StoreList(menuData: tabList[index])
            );
          }),
        ],
      ),
    );
  }

  Widget dressImage(tabLists) {
    return FContainer(
      height: 260.h,
      color: Colors.black.withOpacity(0.05),
      margin: EdgeInsets.symmetric(horizontal: 30.w),
      radius: BorderRadius.circular(30.w),
      align: Alignment.center,
      child: Row(
        children: [
          Expanded(
            child: SizedBox.expand(),
          ),
          LoadNetImage(NetConstants.ossPath + tabLists["category_icon"]),
          Expanded(
            child: Column(
              children: [
                SizedBox(height: 20.w),
                GestureDetector(
                  onTap: onTapCharge,
                  child: Row(children: [
                    Expanded(child: Container()),
                    LoadAssetImage("person/presmut", width: 25.w),
                    SizedBox(width: 10.w),
                    FText("说明",
                        size: 20.sp, color: Color(0xFF222222).withOpacity(0.5)),
                    SizedBox(width: 20.w),
                  ]),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void onTapCharge() {
    // WidgetUtils().showBottomSheet(ChargeDialog());
  }
  void onTapSelf() {
    WidgetUtils().pushPage(DressPage());
  }

  void requestMenu() {
    DioUtils().asyncHttpRequest(
      NetConstants.dressMenu,
      method: DioMethod.GET,
      params: {"category_type": 1},
      onSuccess: onMenuSuccess,
    );
  }

  void onMenuSuccess(response) {
    tabList = response["CategoryInfo"] ?? [];
    tabCtr = TabController(length: tabList.length, vsync: this);

    if (!mounted) return;
    setState(() {});
  }
}
