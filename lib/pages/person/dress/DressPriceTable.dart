import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class DressPriceTable extends StatefulWidget {
  final List<String> titles;
  final dynamic menuData;

  const DressPriceTable({Key? key, required this.menuData,required this.titles}) : super(key: key);
  @override
  State<DressPriceTable> createState() => _DressPriceTableState();
}

class _DressPriceTableState extends State<DressPriceTable>
    with TickerProviderStateMixin {
  late TabController tabController;
  final GlobalKey globalKey = GlobalKey();

  // 当前选择
  int page = 0;

  // 获取组件高度高度
  double gloKeyHeigth = 0;
  double height = 0;


  @override
  void initState() {
    super.initState();

    tabController =
        TabController(length: widget.titles.length, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      constraints: BoxConstraints(
        minHeight: 600.w,
      ),
      child: Column(
        children: [
          SizedBox(
            height: 50.w,
            child: items(),
          ),
          SizedBox(
            height: 1000.h,
            child:  tabView(),
          ),


        ],
      ),
    );
  }

  Widget items() {
    List<Widget> list = [];
    for (var i = 0; i < widget.titles.length; i++) {
      list.add(item(widget.titles[i], i));
    }
    return ListView(
      scrollDirection: Axis.horizontal,
      padding: EdgeInsets.fromLTRB(15.h, 0, 0, 0),
      children: list,
    );
  }

  Widget item(String text, int index) {
    Widget child = FContainer(
      radius: BorderRadius.circular(40.w),
      padding: EdgeInsets.symmetric(horizontal: 16.w),
      margin: EdgeInsets.symmetric(horizontal: 5.w),
      align: Alignment.center,
      color: page == index ? null : null,
      child: FText(
        text,
        size: page == index ? 32.sp : 28.sp,
        color: page == index ? Colors.black : Colors.black.withOpacity(0.6),
      ),
    );

    return GestureDetector(
      onTap: () {
        tabController.animateTo(index);
        page = index;
        setState(() {});
      },
      child: child,
    );
  }

  Widget tabView() {
    List<Widget> listWdt = [];
    for (var i = 0; i < widget.titles.length; i++) {
      listWdt.add(StoreList(menuData: widget.menuData));
    }
    return TabBarView(
      controller: tabController,
      physics: const NeverScrollableScrollPhysics(),
      children: listWdt,
    );
  }
}
