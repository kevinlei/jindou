import 'package:json_annotation/json_annotation.dart';
part 'dress_model.g.dart';

@JsonSerializable()
class DressModel extends Object {
  @JsonKey(name: 'id')
  late int id;

  @JsonKey(name: 'count')
  late int count;

  @JsonKey(name: 'locked')
  late bool locked;

  @JsonKey(name: 'availableTime')
  late int availableTime;

  @JsonKey(name: 'inUsed')
  late bool inUsed;

  @JsonKey(name: 'itemType')
  late int itemType;

  @JsonKey(name: 'days')
  late int days;

  DressModel();

  factory DressModel.fromJson(Map<String, dynamic> srcJson) =>
      _$DressModelFromJson(srcJson);

  Map<String, dynamic> toJson() => _$DressModelToJson(this);
}
