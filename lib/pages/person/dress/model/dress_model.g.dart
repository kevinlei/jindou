// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dress_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DressModel _$DressModelFromJson(Map<String, dynamic> json) => DressModel()
  ..id = json['id'] as int
  ..count = json['count'] as int
  ..locked = json['locked'] as bool
  ..availableTime = json['availableTime'] as int
  ..inUsed = json['inUsed'] as bool
  ..itemType = json['itemType'] as int
  ..days = json['days'] as int;

Map<String, dynamic> _$DressModelToJson(DressModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'count': instance.count,
      'locked': instance.locked,
      'availableTime': instance.availableTime,
      'inUsed': instance.inUsed,
      'itemType': instance.itemType,
      'days': instance.days,
    };
