import 'package:flutter/material.dart';
import 'package:echo/import.dart';


class DressPage extends StatefulWidget {
  @override
  _DressPageState createState() => _DressPageState();
}

class _DressPageState extends State<DressPage>
    with SingleTickerProviderStateMixin {
  List tabList = [];
  TabController? tabCtr;

  @override
  void initState() {
    super.initState();
    requestMenu();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CommonCard(
        child: Column(children: [
          CardAppBar(title: "我的装扮", actions: [
            TextButton(
                onPressed: onTapDress,
                child: FText("装扮商城", color: Colors.black.withOpacity(0.6)))
          ]),
          SizedBox(height: 20.h),
          if (tabCtr != null) DressTable(tabCtr: tabCtr!, tabList: tabList,tablest:""),
          if (tabCtr != null) Expanded(child: dressList()),
        ]),
      ),
    );
  }

  @override
  void dispose() {
    tabCtr?.dispose();
    tabCtr = null;
    super.dispose();
  }

  AppBar dressAppbar() {
    return AppBar(
      title: FText("我的装扮", weight: FontWeight.bold, size: 36.sp),
      actions: [
        IconButton(
            onPressed: onTapDress,
            icon: LoadAssetImage("person/dress2", width: 35.w))
      ],
    );
  }

  Widget dressList() {
    return TabBarView(
      controller: tabCtr!,
      children: List.generate(
          tabList.length, (index) => DressList(menuData: tabList[index])),
    );
  }

  void onTapDress() {
    WidgetUtils().popPage();
  }

  void requestMenu() {
    DioUtils().asyncHttpRequest(
      NetConstants.dressMenu,
      params: {"category_type":1},
      method: DioMethod.GET,
      onSuccess: onMenuSuccess,
    );
  }

  void onMenuSuccess(response) {
    tabList = response["CategoryInfo"] ?? [];
    tabCtr = TabController(length: tabList.length, vsync: this);

    if (!mounted) return;
    setState(() {});
  }
}

