import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class DressSuitDialog extends StatefulWidget {
  final ConfigGoods goods;
  final String tabName;

  const DressSuitDialog({Key? key, required this.goods, required this.tabName})
      : super(key: key);

  @override
  _DressSuitDialogState createState() => _DressSuitDialogState();
}

class _DressSuitDialogState extends State<DressSuitDialog> {
  Map dressPrice = {};
  int curDay = 0;
  double gold = 0.0;

  @override
  void initState() {
    super.initState();
    if (widget.goods.price.isEmpty) return;
    try {
      dressPrice = json.decode(widget.goods.price);
      if (dressPrice.isEmpty) return;
      curDay = int.parse(dressPrice.entries.first.key);
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      color: Colors.white,
      padding: EdgeInsets.fromLTRB(
          20.w, 20.w, 20.w, ScreenUtil().bottomBarHeight + 20.w),
      child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(child: FSheetLine()),
            Column(children: [
              FText(widget.goods.title, size: 32.sp, weight: FontWeight.w600),
              FText("${widget.tabName}",
                  maxLines: 2,
                  size: 24.sp,
                  color: Colors.white.withOpacity(0.5)),
              FText("商品有效期为$curDay天，$curDay天内未佩戴该装扮，装扮到期自动消失",
                  maxLines: 2,
                  size: 24.sp,
                  color: Colors.white.withOpacity(0.5)),
            ], crossAxisAlignment: CrossAxisAlignment.start),
            SizedBox(height: 30.h),
            FContainer(
              height: 165.w,
              radius: BorderRadius.circular(20.w),
              color: Colors.white.withOpacity(0.05),
            ),
            SizedBox(height: 30.h),
            priceContent(),
            SizedBox(height: 70.h),
            Row(children: [
              buttonGive(),
              SizedBox(width: 20.w),
              buttonBuy(),
            ], mainAxisSize: MainAxisSize.max),
            SizedBox(height: 30.h),
            DressBuyBottom(leftPadding: 0, updateGold: updateGold)
          ]),
    );
  }

  Widget priceContent() {
    return SizedBox(
      height: 136.w,
      child: ListView.separated(
        scrollDirection: Axis.horizontal,
        itemCount: dressPrice.length,
        itemBuilder: (_, index) => priceCell(index),
        separatorBuilder: (_, __) => SizedBox(width: 20.w),
      ),
    );
  }

  Widget priceCell(int index) {
    int day = int.parse(dressPrice.keys.toList()[index]);
    Widget btn = FContainer(
      width: 218.w,
      color: curDay == day
          ? Color(0xFFCE7CFF).withOpacity(0.1)
          : Color(0xFFFFFFFF).withOpacity(0.05),
      border: Border.all(
          width: 2,
          color: curDay == day ? Color(0xFFCE7CFF) : Colors.transparent),
      radius: BorderRadius.circular(20.w),
      align: Alignment.center,
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            FRichText([
              FRichTextItem()
                ..text = "$day"
                ..weight = FontWeight.bold
                ..size = 44.sp,
              FRichTextItem()
                ..text = " 天"
                ..size = 24.sp,
            ]),
            SizedBox(height: 10.w),
            Row(children: [
              LoadAssetImage("person/about", width: 25.w),
              SizedBox(width: 10.w),
              FText("${dressPrice["$day"]}",
                  color: Colors.white.withOpacity(0.6), size: 24.sp)
            ], mainAxisSize: MainAxisSize.min)
          ]),
    );
    return GestureDetector(onTap: () => onTapCell(day), child: btn);
  }

  Widget buttonGive() {
    Widget btn = FContainer(
      height: 88.w,
      radius: BorderRadius.circular(88.w),
      child: FText("赠送", size: 32.sp, weight: FontWeight.w500),
      align: Alignment.center,
      color: Colors.white.withOpacity(0.2),
    );
    return Flexible(
        flex: 1, child: GestureDetector(onTap: onTapGive, child: btn));
  }

  Widget buttonBuy() {
    Widget btn = FContainer(
      height: 88.w,
      radius: BorderRadius.circular(88.w),
      child: FText("购买", size: 32.sp, weight: FontWeight.w500),
      align: Alignment.center,
      gradient: [Color(0xFFF3A6FF), Color(0xFF8F36FF)],
    );
    return Flexible(
        flex: 2, child: GestureDetector(onTap: onTapBuy, child: btn));
  }

  void updateGold(value) => gold = value;

  void onTapCell(int day) {
    if (curDay == day) return;
    curDay = day;
    if (!mounted) return;
    setState(() {});
  }

  void onTapGive() {}

  void onTapBuy() {}
}
