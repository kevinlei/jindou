import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class DressList extends StatefulWidget {
  final dynamic menuData;

  const DressList({Key? key, required this.menuData}) : super(key: key);

  @override
  _DressListState createState() => _DressListState();
}

class _DressListState extends State<DressList> {
  List<DressModel> dressList = [];
  DateTime timeNow = DateTime.now();

  @override
  void initState() {
    super.initState();
    callRefresh();
  }

  @override
  Widget build(BuildContext context) {
    // {"id":1,"title":"头像框"},{"id":2,"title":"坐骑"},{"id":3,"title":"入场特效"},{"id":5,"title":"全服广播"},{"id":7,"title":"聊天气泡"},
    // {"id":8,"title":"套装"},{"id":9,"title":"声波"},{"id":6,"title":"公屏气泡"},{"id":4,"title":"昵称挂件"}
    return FRefreshLayout(
      firstRefresh: false,
      onRefresh: callRefresh,
      emptyWidget: dressList.isEmpty ? FEmptyWidget() : null,
      child: ListView.separated(
        padding: EdgeInsets.fromLTRB(25.w, 20.h, 25.w, 100.h),
        separatorBuilder: (_, __) => SizedBox(height: 20.h),
        itemBuilder: (_, index) => dressCell(index),
        itemCount: dressList.length,
      ),
    );
  }

  Widget dressCell(int index) {
    bool isSpecial = false;
    Widget? dressContent;
    if (widget.menuData["id"] == 3 || widget.menuData["id"] == 5) {
      isSpecial = true;
      dressContent = specialDressCell(index);
    } else {
      isSpecial = false;
      dressContent = normalDressCell(index);
    }

    return FContainer(
      color: Color(0xFF8E36FF).withOpacity(0.1),
      height: isSpecial ? 404.w : 216.w,
      padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 15.w),
      radius: BorderRadius.circular(20.w),
      child: dressContent,
    );
  }

  Widget normalDressCell(int index) {
    int id = dressList[index].id;
    bool inUse = dressList[index].inUsed;
    // ConfigGoods? goods = ConfigManager().getGoods(id);
    // String name = goods?.title ?? "";
    String name = "";
    String menu = widget.menuData["title"] ?? "";
    // String tag = goods?.labelName ?? "";
    String tag = "";

    Widget expand = Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          dressName(index, name, menu, tag),
          dressTime(
              dressList[index].availableTime, dressList[index].days, inUse),
          Visibility(
              visible: tag == "活动",
              child: FText("此装扮为活动获取 暂不支持续费",
                  size: 22.sp, color: Color(0xFFBBBBBB))),
        ]);

    return Row(children: [
      // dressImage(false, goods?.picture ?? "", inUse),
      dressImage(false, "", inUse),
      SizedBox(width: 20.w),
      Expanded(child: expand),
      Column(mainAxisAlignment: MainAxisAlignment.end, children: [
        if (!inUse) ...[
          // buttonUse(goods),
        ] else ...[
          // buttonRemove(goods),
        ],
        SizedBox(height: 10.w),
        // Visibility(child: buttonBuy(goods), visible: tag != "活动"),
      ])
    ]);
  }

  Widget specialDressCell(int index) {
    int id = dressList[index].id;
    bool inUse = dressList[index].inUsed;
    // ConfigGoods? goods = ConfigManager().getGoods(id);
    // String name = goods?.title ?? "";
    String name = "";
    String menu = widget.menuData["title"] ?? "";
    // String tag = goods?.labelName ?? "";
    String tag = "";

    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      // dressImage(true, goods?.picture ?? "", inUse),
      dressImage(true, "", inUse),
      SizedBox(height: 10.w),
      dressName(index, name, menu, tag),
      dressTime(dressList[index].availableTime, dressList[index].days, inUse),
      Visibility(
        visible: tag == "活动",
        child: FText("此装扮为活动获取 暂不支持续费",
            size: 22.sp, color: Color(0xFFFFFFFF).withOpacity(0.5)),
      ),
      const Spacer(),
      Row(mainAxisAlignment: MainAxisAlignment.end, children: [
        if (!inUse) ...[
          // buttonUse(goods),
        ] else ...[
          // buttonRemove(goods),
        ],
        SizedBox(width: 10.w),
        // Visibility(child: buttonBuy(goods), visible: tag != "活动"),
      ])
    ]);
  }

  Widget dressImage(bool isSpecial, String img, bool inUse) {
    return Stack(children: [
      FContainer(
        width: isSpecial ? null : 170.w,
        color: Color(0xFFFFFFFF).withOpacity(0.1),
        height: 170.w,
        radius: BorderRadius.circular(20.w),
        align: Alignment.center,
        child: LoadNetImage(NetConstants.ossPath + img),
      ),
      Visibility(child: inUseTag(), visible: inUse)
    ]);
  }

  Widget dressName(int index, String name, String menu, String tag) {
    return Row(children: [
      ConstrainedBox(
          constraints: BoxConstraints(maxWidth: 210.w),
          child: FText(name, overflow: TextOverflow.ellipsis)),
      SizedBox(width: 10.w),
      FContainer(
        gradient: [Color(0xFF7046FF), Color(0xFFC900D5)],
        gradientBegin: Alignment.centerLeft,
        gradientEnd: Alignment.centerRight,
        padding: EdgeInsets.symmetric(horizontal: 15.w),
        radius: BorderRadius.circular(5.w),
        constraints: BoxConstraints(maxWidth: 220.w),
        child: FText(tag.isEmpty ? menu : "$menu | $tag",
            size: 20.sp, overflow: TextOverflow.ellipsis),
      )
    ]);
  }

  Widget dressTime(int time, int left, bool inUse) {
    String timeText = DateTime.fromMillisecondsSinceEpoch(
            time * 1000 + timeNow.millisecondsSinceEpoch)
        .toString()
        .substring(0, 10);

    return ConstrainedBox(
      constraints: BoxConstraints(maxWidth: 450.w),
      child: FText("有效期至：$timeText" + (inUse ? " (剩余$left天)" : ""),
          size: 24.sp,
          maxLines: 2,
          color: Color(0xFFFFFFFF).withOpacity(0.5),
          overflow: TextOverflow.ellipsis),
    );
  }

  Widget dressButton(ConfigGoods? goods, bool inUse, bool isActivity) {
    return Row(children: [
      Spacer(),
      Visibility(child: buttonUse(goods), visible: !inUse),
      Visibility(child: buttonRemove(goods), visible: inUse),
      Visibility(child: buttonBuy(goods), visible: !isActivity),
    ]);
  }

  Widget inUseTag() {
    return FContainer(
      width: 95.w,
      align: Alignment.center,
      height: 34.w,
      color: Color(0xFFA852FF),
      child: FText("使用中", size: 22.sp),
      radius: BorderRadius.only(
        topLeft: Radius.circular(20.w),
        bottomRight: Radius.circular(20.w),
      ),
    );
  }

  Widget buttonUse(ConfigGoods? goods) {
    Widget btn = FContainer(
      width: 112.w,
      radius: BorderRadius.circular(60.w),
      height: 60.w,
      gradient: [Color(0xFF46F5F6), Color(0xFFFFA7FB), Color(0xFFFF82B2)],
      gradientBegin: Alignment.topLeft,
      gradientEnd: Alignment.bottomRight,
      align: Alignment.center,
      child: FText("使用", size: 24.sp, weight: FontWeight.w500),
    );
    return GestureDetector(onTap: () => onTapUse(goods), child: btn);
  }

  Widget buttonRemove(ConfigGoods? goods) {
    Widget btn = FContainer(
      width: 112.w,
      radius: BorderRadius.circular(60.w),
      height: 60.w,
      color: Colors.transparent,
      //设置四周边框
      border: new Border.all(width: 1, color: Color(0xFFFF753F)),
      align: Alignment.center,
      child: FText("卸下", size: 24.sp, weight: FontWeight.w500),
    );
    return GestureDetector(onTap: () => onTapRemove(goods), child: btn);
  }

  Widget buttonBuy(ConfigGoods? goods) {
    Widget btn = FContainer(
      width: 112.w,
      radius: BorderRadius.circular(60.w),
      height: 60.w,
      gradient: [Color(0xFF46F5F6), Color(0xFFFFA7FB), Color(0xFFFF82B2)],
      gradientBegin: Alignment.topLeft,
      gradientEnd: Alignment.bottomRight,
      align: Alignment.center,
      child: FText("续费", size: 24.sp, weight: FontWeight.w500),
    );
    return GestureDetector(onTap: () => onTapBuy(goods), child: btn);
  }

  void onTapUse(ConfigGoods? goods) {
    if (goods == null) return;
    int dressId = goods.id;
    DioUtils().asyncHttpRequest(
      NetConstants.useDress,
      method: DioMethod.POST,
      params: {"id": dressId},
      onSuccess: onUseDress,
    );
  }

  void onTapRemove(ConfigGoods? goods) async {
    if (goods == null) return;
    bool? accept = await WidgetUtils().showAlert("确认取消装扮？");
    if (accept == null || !accept) return;
    int dressId = goods.id;
    DioUtils().asyncHttpRequest(
      NetConstants.removeDress,
      method: DioMethod.POST,
      params: {"id": dressId},
      onSuccess: onRemoveDress,
    );
  }

  void onTapBuy(ConfigGoods? goods) async {
    if (goods == null) return;
    await WidgetUtils().showBottomSheet(DressBuyDialog(
        goods: goods, tabName: widget.menuData["title"], isRenew: true));
    callRefresh();
  }

  Future<void> callRefresh() async {
    if (widget.menuData["id"] == 8) {
    } else {
      requestSelfDress();
    }
  }

  void requestSelfDress() {
    DioUtils().asyncHttpRequest(
      NetConstants.selfDress,
      params: {"type": widget.menuData["id"]},
      onSuccess: onDressList,
    );
  }

  void onDressList(response) {
    dressList.clear();
    response ??= [];
    response.forEach((element) {
      dressList.add(DressModel.fromJson(element));
    });
    if (!mounted) return;
    setState(() {});
  }

  void onUseDress(response) {
    WidgetUtils().showToast("装扮成功");
    callRefresh();
  }

  void onRemoveDress(response) {
    WidgetUtils().showToast("取下装扮成功");
    callRefresh();
  }
}
