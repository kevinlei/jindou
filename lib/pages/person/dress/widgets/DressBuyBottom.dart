import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class DressBuyBottom extends StatefulWidget {
  final Function(double) updateGold;
  final double? leftPadding;
  const DressBuyBottom({Key? key, required this.updateGold, this.leftPadding})
      : super(key: key);

  @override
  _DressBuyBottomState createState() => _DressBuyBottomState();
}

class _DressBuyBottomState extends State<DressBuyBottom> {
  double gold = 0.0;

  @override
  void initState() {
    super.initState();
    requestBagList();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTapCharge,
      child: Row(children: [
        SizedBox(width: widget.leftPadding ?? 30.w),
        LoadAssetImage("person/myGold", width: 28.w),
        SizedBox(width: 10.w),
        FText('${FuncUtils().formatNum(gold, 2)}', size: 30.sp, color: Color(0xFFFF753F)),
        FText("| 充值", size: 30.sp, color: Color(0xFF222222).withOpacity(0.5)),
      ]),
    );
  }
  //充值
  void onTapCharge() {
    // WidgetUtils().showBottomSheet(ChargeDialog());
  }

  void requestBagList() {
    DioUtils().asyncHttpRequest(
      NetConstants.getBag,
      loading: false,
      params: {"needDetail": 1},
      onSuccess: updateBagList,
    );
  }

  void updateBagList(response) {
    gold = double.parse(response?["gold"] ?? "0.0");
    widget.updateGold(gold);
    if (!mounted) return;
    setState(() {});
  }
}
