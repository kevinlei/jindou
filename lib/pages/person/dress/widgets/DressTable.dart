import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class DressTable extends StatelessWidget {
  final TabController tabCtr;
  final List tabList;
  final String tablest;
  const DressTable({Key? key, required this.tabCtr, required this.tabList,required this.tablest})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return tablest.length != 0 ? TabBar(
      isScrollable: true,
      indicator: BoxDecoration(
          color: Color(0xFFFF753F),
        borderRadius: BorderRadius.all(Radius.circular(30.0.h)),

      ),
      unselectedLabelColor: Colors.black,
      labelStyle: TextStyle(fontWeight: FontWeight.w400, fontSize: 32.sp),
      unselectedLabelStyle:
          TextStyle(fontWeight: FontWeight.w400, fontSize: 32.sp),
      tabs: getTabTexts(),
      controller: tabCtr,
      // indicatorColor: Color(0xFFF3A6FF),
      // indicator: const BoxDecoration(),
      indicatorSize: TabBarIndicatorSize.label,
    ):TabBar(
       isScrollable: true,
      unselectedLabelColor: Colors.black,
        labelColor:Colors.black,
       labelStyle: TextStyle(fontWeight: FontWeight.w600, fontSize: 36.sp),
        unselectedLabelStyle:
        TextStyle(fontWeight: FontWeight.w400, fontSize: 28.sp),
        tabs: getTabTexts(),
        controller: tabCtr,
        indicatorColor: Color(0xFFF3A6FF),
    // indicator: const BoxDecoration(),
        indicatorSize: TabBarIndicatorSize.label,
    );
  }

  List<Widget> getTabTexts() {
    return  List.generate(tabList.length,
        (index) => Tab(
            child: Container(
              child:Row(
                children: [
                  SizedBox(width: 15.w),
                  Text( tabList[index]["category_name"] ?? ""),
                  SizedBox(width: 15.w),
                ],
              ),
            ),
            // text: tabList[index]["title"] ?? "",
            height: 50.h));


    // return List.generate(tabList.length,
    //         (index) => Tab( child:(Column(
    //           children: [
    //             ClipRRect(//是ClipRRect，不是ClipRect
    //               borderRadius: BorderRadius.circular(23),
    //               child: Image.network(
    //                 NetConstants.ossPath + tabList[index]["icon"] ?? "",
    //                 width: 46,
    //                 height: 46,
    //                 fit: BoxFit.cover,
    //               ),
    //             ),
    //             Expanded(child: Container()),
    //             Text( tabList[index]["title"] ?? "")
    //           ],
    //
    //         )
    //         ),
    //             height: 130.h,
    //         )
    // );
              // icon: tabList[index]["title"] ?? "", child: Text( tabList[index]["title"] ?? ""), ));
  }
}
