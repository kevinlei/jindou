import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class StoreList extends StatefulWidget {
  final dynamic menuData;

  const StoreList({Key? key, required this.menuData}) : super(key: key);

  @override
  _StoreListState createState() => _StoreListState();
}

class _StoreListState extends State<StoreList> {
  List<ConfigGoods> dressList = [];
  List bannerList = [];

  @override
  void initState() {
    super.initState();
    callRefresh();
    requestBanner();
  }

  void requestBanner() {
    DioUtils().asyncHttpRequest(
      NetConstants.banner,
      loading: false,
      method: DioMethod.POST,
      params: {"type": 4},
      onSuccess: onBannerList,
    );
  }

  void onBannerList(response) {
    bannerList.clear();
    bannerList = response ?? [];
    if (!mounted) return;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    // '入场特效','全服广播','聊天气泡','公屏气泡' '昵称挂件' 两列 其他 三列
    // {"id":1,"title":"头像框"},{"id":2,"title":"坐骑"},{"id":3,"title":"入场特效"},{"id":5,"title":"全服广播"},{"id":7,"title":"聊天气泡"},
    // {"id":8,"title":"套装"},{"id":9,"title":"声波"},{"id":6,"title":"公屏气泡"},{"id":4,"title":"昵称挂件"}
    int crossLen = 3;
    double extent = 280.w;
    if (widget.menuData["id"] == 5 ||
        widget.menuData["id"] == 6 ||
        widget.menuData["id"] == 7) {
      crossLen = 2;
      extent = 224.w;
    }
    if (widget.menuData["id"] == 3) {
      crossLen = 1;
      extent = 280.w;
    }

    return Expanded(
        child: FRefreshLayout(
      onRefresh: callRefresh,
      firstRefresh: false,
      emptyWidget: dressList.isEmpty ? FEmptyWidget() : null,
      child: GridView.builder(
        padding: EdgeInsets.fromLTRB(25.w, 20.h, 25.w, 100.h),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: crossLen,
          mainAxisExtent: extent,
          crossAxisSpacing: 20.w,
          mainAxisSpacing: 20.w,
        ),
        itemBuilder: (_, index) => dressCell(index),
        itemCount: dressList.length,
      ),
    ));
  }

  Widget dressCell(int index) {
    ConfigGoods goods = dressList[index];
    String labelImg = goods.labelIcon ?? "";
    Widget btn = FContainer(
      color: Color(0xFFA5A5A5).withOpacity(0.1),
      radius: BorderRadius.circular(20.w),
      align: Alignment.center,
      padding: EdgeInsets.only(bottom: 20.w),
      child: Column(children: [
        Expanded(child: LoadNetImage(NetConstants.ossPath + goods.picture)),
        FText(goods.title,
            weight: FontWeight.w300,
            color: Colors.black,
            overflow: TextOverflow.ellipsis,
            size: 26.sp),
        Row(children: [
          FText(getDressPrice(goods), color: Color(0xFF80222222), size: 24.sp),
          FText('金豆', color: Color(0xFF80222222), size: 24.sp)
        ], mainAxisAlignment: MainAxisAlignment.center)
      ]),
    );

    btn = Stack(children: [
      btn,
      Visibility(
          visible: labelImg.isNotEmpty,
          child: LoadNetImage(NetConstants.ossPath + labelImg,
              width: 64.w, height: 32.w)),
    ]);

    return GestureDetector(child: btn, onTap: () => onTapDress(goods));
  }

  String getDressPrice(ConfigGoods goods) {
    String price = goods.price;
    if (price.isEmpty) return "";
    try {
      Map data = json.decode(price);
      if (data.length > 0) {
        return "${data.entries.first.value}";
      }
    } catch (e) {}
    return "";
  }

  void onTapDress(ConfigGoods goods) {
    if (widget.menuData["id"] == 8) {
      WidgetUtils().showBottomSheet(
          DressSuitDialog(goods: goods, tabName: widget.menuData["title"]));
    } else if (goods.labelName == "活动") {
      WidgetUtils().showBottomSheet(DressActivityDialog(
          goods: goods,
          tabName: widget.menuData["title"],
          bannerList: bannerList));
    } else {
      WidgetUtils().showBottomSheet(
          DressBuyDialog(goods: goods, tabName: widget.menuData["title"]));
    }
  }

  //
  Future<void> callRefresh() async {
    if (widget.menuData["id"] == 8) {
      requestSuitList();
    } else {
      requestGoodsList();
    }
  }

  // 请求装扮列表
  void requestGoodsList() {
    // DioUtils().httpRequest(
    //   NetConstants.goodsConfig,
    //   method: DioMethod.POST,
    //   params: {"category_id": widget.menuData["id"], "on_sale": 1},
    //   onSuccess: onGoodsList,
    // );
  }

  // 请求套装列表
  void requestSuitList() {}

  void onGoodsList(response) {
    dressList.clear();
    response ??= [];
    response.forEach((element) {
      dressList.add(ConfigGoods.fromJson(element));
    });
    if (!mounted) return;
    setState(() {});
  }
}
