import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class DressActivityDialog extends StatelessWidget {
  final ConfigGoods goods;
  final String tabName;
  final List bannerList;

  const DressActivityDialog(
      {Key? key,
      required this.goods,
      required this.tabName,
      required this.bannerList})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    String day = getDressTime(goods);

    return FContainer(
      color: Colors.white,
      padding: EdgeInsets.only(
          top: 20.w, bottom: ScreenUtil().bottomBarHeight + 20.w),
      child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(child: FSheetLine()),
            dressImage(),
            SizedBox(height: 20.h),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20.w),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    FText(goods.title, size: 32.sp, weight: FontWeight.bold),
                    FText("$tabName | 活动的推荐话语，表现装扮的稀有性",
                        maxLines: 2,
                        size: 24.sp,
                        color: Colors.white.withOpacity(0.5)),
                    FText("商品有效期为$day天，$day天内未佩戴该装扮，装扮到期自动消失",
                        maxLines: 2,
                        size: 24.sp,
                        color: Colors.white.withOpacity(0.5)),
                  ]),
            ),
            SizedBox(height: 70.h),
            const BannerSwipe(4),
            SizedBox(height: 70.h),
            FContainer(
              height: 90.w,
              align: Alignment.center,
              radius: BorderRadius.circular(90.w),
              margin: EdgeInsets.symmetric(horizontal: 32.w, vertical: 10.h),
              gradientBegin: Alignment.centerLeft,
              gradientEnd: Alignment.centerRight,
              gradient: const [Color(0xFFFF753F), Color(0xFFFF753F)],
              child: FText("活动获取条件", size: 32.sp),
            )
          ]),
    );
  }

  Widget dressImage() {
    return FContainer(
      height: 260.h,
      color: Colors.white.withOpacity(0.05),
      radius: BorderRadius.only(
        topRight: Radius.circular(30.w),
        topLeft: Radius.circular(30.w),
      ),
      align: Alignment.center,
      child: LoadNetImage(NetConstants.ossPath + goods.picture),
    );
  }

  String getDressTime(ConfigGoods goods) {
    String price = goods.price;
    if (price.isEmpty) return "";
    try {
      Map data = json.decode(price);
      if (data.length > 0) {
        return "${data.entries.first.key}";
      }
    } catch (e) {}
    return "";
  }
}
