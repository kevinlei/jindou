import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class DressBuyDialog extends StatefulWidget {
  final ConfigGoods goods;
  final String tabName;
  final bool? isRenew;

  const DressBuyDialog(
      {Key? key,
      required this.goods,
      required this.tabName,
      this.isRenew = false})
      : super(key: key);

  @override
  _DressBuyDialogState createState() => _DressBuyDialogState();
}

class _DressBuyDialogState extends State<DressBuyDialog> {
  Map dressPrice = {};
  int curDay = 0;
  double gold = 0.0;
  bool hasOpen = false;

  @override
  void initState() {
    super.initState();
    openHas();
    if (widget.goods.price.isEmpty) return;
    try {
      dressPrice = json.decode(widget.goods.price);
      if (dressPrice.isEmpty) return;
      curDay = int.parse(dressPrice.entries.first.key);
    } catch (e) {}
  }

  void openHas() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    bool? youthState = preferences.getBool(SharedKey.KEY_ADOLESCENT.value);
    (youthState != null && youthState) ? hasOpen = true : hasOpen = false;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      padding: EdgeInsets.only(
          top: 20.w, bottom: ScreenUtil().bottomBarHeight + 20.w),
      child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(child: FSheetLine()),
            dressImage(),
            SizedBox(height: 20.h),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 30.w),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    FText(
                      widget.goods.title,
                      size: 32.sp,
                      weight: FontWeight.w600,
                      color: Colors.black,
                    ),
                    FText(widget.tabName,
                        maxLines: 2,
                        size: 28.sp,
                        color: Colors.black.withOpacity(0.5)),
                    FText("商品有效期为$curDay天，$curDay天内未佩戴该装扮，装扮到期自动消失",
                        maxLines: 2,
                        size: 24.sp,
                        color: Colors.black.withOpacity(0.5)),
                  ]),
            ),
            SizedBox(height: 50.h),
            Visibility(
                visible: !hasOpen,
                child: Column(
                  children: [
                    priceContent(),
                    SizedBox(height: 30.h),
                    Row(mainAxisSize: MainAxisSize.max, children: [
                      SizedBox(width: 30.w),
                      buttonGive(),
                      SizedBox(width: 20.w),
                      buttonBuy(),
                      SizedBox(width: 30.w),
                    ]),
                    SizedBox(height: 30.h),
                    DressBuyBottom(updateGold: updateGold),
                  ],
                ))
          ]),
    );
  }

  Widget dressImage() {
    return FContainer(
      height: 260.h,
      color: Colors.black.withOpacity(0.05),
      margin: EdgeInsets.symmetric(horizontal: 30.w),
      radius: BorderRadius.circular(30.w),
      align: Alignment.center,
      child: LoadNetImage(NetConstants.ossPath + widget.goods.picture),
    );
  }

  Widget priceContent() {
    return SizedBox(
      height: 136.w,
      child: ListView.separated(
        scrollDirection: Axis.horizontal,
        padding: EdgeInsets.symmetric(horizontal: 20.w),
        itemCount: dressPrice.length,
        itemBuilder: (_, index) => priceCell(index),
        separatorBuilder: (_, __) => SizedBox(width: 20.w),
      ),
    );
  }

  Widget priceCell(int index) {
    int day = int.parse(dressPrice.keys.toList()[index]);
    Widget btn = FContainer(
      width: 218.w,
      color: curDay == day
          ? const Color(0xFFCE7CFF).withOpacity(0.1)
          : const Color(0xFF222222).withOpacity(0.05),
      border: Border.all(
          width: 2,
          color: curDay == day ? const Color(0xFFFF753F) : Colors.transparent),
      radius: BorderRadius.circular(10.w),
      align: Alignment.center,
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            FRichText([
              FRichTextItem()
                ..text = "$day"
                ..weight = FontWeight.bold
                ..color = Colors.black
                ..size = 44.sp,
              FRichTextItem()
                ..text = " 天"
                ..color = Colors.black
                ..size = 24.sp,
            ]),
            SizedBox(height: 10.w),
            Row(mainAxisSize: MainAxisSize.min, children: [
              // LoadAssetImage("person/about", width: 25.w),
              // SizedBox(width: 10.w),
              FText("${dressPrice["$day"]}金豆",
                  color: Colors.black.withOpacity(0.6), size: 24.sp)
            ])
          ]),
    );
    return GestureDetector(onTap: () => onTapCell(day), child: btn);
  }

  Widget buttonGive() {
    Widget btn = FContainer(
      height: 88.w,
      radius: BorderRadius.circular(88.w),
      border: Border.all(color: const Color(0xFFFF753F)),
      align: Alignment.center,
      child: FText(
        "赠送",
        size: 32.sp,
        weight: FontWeight.w500,
        color: const Color(0xFFFF753F),
      ),
    );
    return Flexible(
        flex: 1, child: GestureDetector(onTap: onTapGive, child: btn));
  }

  Widget buttonBuy() {
    Widget btn = FContainer(
      height: 88.w,
      radius: BorderRadius.circular(88.w),
      align: Alignment.center,
      gradient: [const Color(0xFFFF753F), const Color(0xFFFF753F)],
      child: FText(widget.isRenew == true ? "续费购买" : "购买",
          size: 32.sp, weight: FontWeight.w500),
    );
    return Flexible(
        flex: 2, child: GestureDetector(onTap: onTapBuy, child: btn));
  }

  void onTapCell(int day) {
    if (curDay == day) return;
    curDay = day;
    if (!mounted) return;
    setState(() {});
  }

  void updateGold(value) => gold = value;

  void onTapGive() async {
    if (curDay == 0) return WidgetUtils().showToast("请选择购买天数");
    int curPrice = dressPrice["$curDay"] ?? 999999999999;
    if (gold < curPrice) return WidgetUtils().showToast("金币余额不足请前往充值");
    Map? userData =
        await WidgetUtils().showBottomSheet(const FShareFriend(btnText: "赠送"));
    if (userData == null) return;
    String userId = userData["user"];
    String conId = userData["conId"];
    String head = userData["head"];
    String name = userData["name"];
    bool success = await requestBuy(true, target: userId);
    if (!success) return;
    if (userId == UserConstants.userginInfo?.info.userId) return;
    HxMsgExt msgExt = HxMsgExt(userId, head, name);
    // AppManager()
    //     .sendHxChatMsg(conId, msgExt, HXDressMsg(widget.goods.id, curDay));
    WidgetUtils().popPage();
  }

  void onTapBuy() async {
    if (curDay == 0) return WidgetUtils().showToast("请选择购买天数");
    int curPrice = dressPrice["$curDay"] ?? 999999999999;
    if (gold < curPrice) return WidgetUtils().showToast("金币余额不足请前往充值");
    bool success = await requestBuy(false);
    if (!success) return;
    WidgetUtils().popPage();
  }

  Future<bool> requestBuy(bool give, {String? target}) async {
    int dressId = widget.goods.id;
    bool success = false;
    await DioUtils().httpRequest(NetConstants.buyDress,
        method: DioMethod.POST,
        params: {
          "id": dressId,
          "day": curDay,
          "give": give,
          "targetUserId": target ?? ""
        }, onSuccess: (_) {
      WidgetUtils().showToast(give ? "赠送成功" : "购买成功");
      success = true;
    });
    return success;
  }
}
