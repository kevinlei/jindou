import 'dart:io';

import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:flutter/services.dart';

class BindPhone1 extends StatefulWidget {
  const BindPhone1({Key? key}) : super(key: key);

  @override
  _BindPhone1State createState() => _BindPhone1State();
}

class _BindPhone1State extends State<BindPhone1> {
  String code = "";

  @override
  Widget build(BuildContext context) {
    String phone = UserConstants.userginInfo?.info.phone ?? "";
    return Scaffold(
      backgroundColor: const Color(0xFFF5F6F7),
      appBar: const CommonAppBar(
        title: "换绑手机",
        textColor: Colors.black,
        elevation: 1,
        backgroundColor: Color(0xFFF5F6F7),
      ),
      body: Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
        SizedBox(height: 40.h),
        // FText("当前绑定手机号：${phone.replaceRange(3, 7, "****")}",
        //     size: 34.sp, weight: FontWeight.bold),
        FText(
          "当前绑定手机号：${phone.replaceRange(3, 7, "****")}",
          size: 34.sp,
          weight: FontWeight.bold,
          color: Colors.black,
        ),
        SizedBox(height: 40.h),
        inputField(),
      ]),
      bottomNavigationBar: Padding(
        padding: EdgeInsets.only(
            bottom: Platform.isIOS ? ScreenUtil().bottomBarHeight
                : 0),
        child: FcationButton("下一步",
            isActivate: true, radius: 90.w, onPress: onTapNext),
      ),
    );
  }

  Widget inputField() {
    return FContainer(
      height: 88.w,
      radius: BorderRadius.circular(88.w),
      margin: EdgeInsets.symmetric(horizontal: 25.w),
      color: const Color(0xFF222222).withOpacity(0.05),
      padding: EdgeInsets.symmetric(horizontal: 25.w),
      child: Row(
          children: [Expanded(child: textField()), CodeButton(requestCode)]),
    );
  }

  Widget textField() {
    InputBorder border = const OutlineInputBorder(
        borderSide: BorderSide(color: Colors.transparent));

    return TextField(
      maxLength: 6,
      style: TextStyle(color: Colors.black, fontSize: 30.sp),
      inputFormatters: [FilteringTextInputFormatter.digitsOnly],
      onChanged: onTextInput,
      decoration: InputDecoration(
        focusedBorder: border,
        enabledBorder: border,
        counterText: "",
        contentPadding: EdgeInsets.zero,
        hintText: "请输入短信验证码",
        hintStyle: TextStyle(color: const Color(0xFFBBBBBB), fontSize: 30.sp),
      ),
    );
  }

  void onTextInput(value) => code = value;

  Future<bool> requestCode() async {
    bool success = false;
    await DioUtils().httpRequest(
      NetConstants.loginCode,
      params: {"phone":UserConstants.userginInfo?.info.phone,"platform": Platform.isAndroid ? "android" : "ios","send_type":3, "temp":SMSType.SMS_TEMPLATE_BIND.value},
      method: DioMethod.POST,
      onSuccess: (_) => success = true,
    );
    return success;
  }

  void onTapNext() async {
    if (!FuncUtils().isMobileCode(code))
      return WidgetUtils().showToast("请输入正确的验证码");

    await DioUtils().httpRequest(
      NetConstants.checkPhone,
      method: DioMethod.POST,
      params: {"code": code,"phone":UserConstants.userginInfo?.info.phone,"platform": Platform.isAndroid ? "android" : "ios"},
      onSuccess: (response) =>
          WidgetUtils().pushReplace(BindPhone2(orderId: response)),
    );
  }
}
