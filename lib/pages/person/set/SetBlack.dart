import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class SetBlack extends StatefulWidget {
  @override
  _SetBlackState createState() => _SetBlackState();
}

class _SetBlackState extends State<SetBlack> {
  int curPage = 1;
  List blackList = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFF5F6F7),
      appBar: const CommonAppBar(
        title: "黑名单",
        textColor: Colors.black,
        elevation: 1,
        backgroundColor: Color(0xFFF5F6F7),
      ),
      body: FRefreshLayout(
        onRefresh: callRefresh,
        onLoad: callLoad,
        emptyWidget: blackList.isEmpty ? const FEmptyWidget() : null,
        child: ListView.builder(
          padding: EdgeInsets.fromLTRB(25.w, 40.h, 25.w, 0),
          itemBuilder: (_, index) => blackCell(index),
          itemCount: blackList.length,
          itemExtent: 120.w,
        ),
      ),
    );
  }

  Widget blackCell(int index) {
    return Row(children: [
      CircleAvatar(
        radius: 40.w,
        backgroundImage: ImageUtils().getImageProvider(
            NetConstants.ossPath + blackList[index]["head_icon"]),
        backgroundColor: const Color(0xFFBBBBBB),
      ),
      SizedBox(width: 20.w),
      Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ConstrainedBox(
              constraints: BoxConstraints(maxWidth: 260.w),
              child: FText(blackList[index]["name"],color: Colors.black,),
            ),
            FText("ID：${blackList[index]["display_id"]}",
                size: 24.sp, color: const Color(0xFF999999))
          ]),
      const Spacer(),
      buttonRemove(blackList[index]["user_id"])
    ]);
  }

  Widget buttonRemove(String userId) {
    Widget btn = FContainer(
      width: 125.w,
      height: 50.w,
      radius: BorderRadius.circular(50.w),
      align: Alignment.center,
      gradient:  [Color(0xFF46F5F6), Color(0xFFFFA7FB), Color(0xFFFF82B2)],
      gradientBegin: Alignment.topLeft,
      gradientEnd: Alignment.bottomRight,
      child: FText("移除", size: 24.sp),
    );
    return GestureDetector(onTap: () => onTapRemove(userId), child: btn);
  }

  void onTapRemove(String userId) {
    DioUtils().asyncHttpRequest(
      NetConstants.followUser,
      method: DioMethod.POST,
      params: {"flow_user_id": userId,"flow_action":40},
      onSuccess: (_) => onRemove(userId),
    );
  }

  void onRemove(response) {
    blackList.removeWhere((e) => e["user_id"] == response);
    if (!mounted) return;
    setState(() {});
  }

  Future<void> callRefresh() async {
    curPage = 1;
    requestList(true);
  }

  Future<void> callLoad() async {
    curPage += 1;
    requestList(false);
  }

  void requestList(bool isRefresh) {
    DioUtils().asyncHttpRequest(
      NetConstants.userBlack,
      method: DioMethod.GET,
      params: {"page": curPage, "page_size": AppConstants.pageSize,"mode":40},
      onSuccess: isRefresh ? onRefresh : onLoad,
    );
  }

  void onRefresh(response) {
    blackList = response?["list"] ?? [];
    if (!mounted) return;
    setState(() {});
  }

  void onLoad(response) {
    List list = response?["list"] ?? [];
    if (list.isEmpty) return;
    blackList.addAll(list);
    if (!mounted) return;
    setState(() {});
  }
}
