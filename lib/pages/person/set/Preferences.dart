import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:permission_handler/permission_handler.dart';

class Preferences extends StatefulWidget {
  @override
  _PreferencesState createState() => _PreferencesState();
}

class _PreferencesState extends State<Preferences> with WidgetsBindingObserver {
  bool openPush = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    getNotificationStatus();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    switch (state) {
      case AppLifecycleState.resumed:
        getNotificationStatus();
        break;
      case AppLifecycleState.inactive:
        break;
      case AppLifecycleState.paused:
        break;
      case AppLifecycleState.detached:
        break;
      case AppLifecycleState.hidden:
        break;
    }
  }

  @override
  void deactivate() {
    WidgetsBinding.instance.removeObserver(this);
    super.deactivate();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFF5F6F7),
      appBar: const CommonAppBar(
        title: "隐私设置",
        textColor: Colors.black,
        elevation: 1,
        backgroundColor: Color(0xFFF5F6F7),
      ),
      body: ListView(
        padding: EdgeInsets.fromLTRB(15.w, 40.h, 15.w, 0),
        children: [
          SetListItem("黑名单", onPress: onTapheiPrivate),
          const Divider(
            color: Color(0xFF14222222),
          ),
          SizedBox(height: 20.h),
          SizedBox(
            height: 60.w,
            child: Row(children: [
              SizedBox(width: 25.w),
              FText("展示在线状态", size: 30.sp, color: const Color(0xFF222222)),
              const Spacer(),
              FBtnOpen(
                onTapBroadcast,
                initOpen: UserConstants.openBroad,
              ),
              SizedBox(width: 20.h),
            ]),
          ),
          SizedBox(height: 10.h),
          SetListItem("个性化选项",
              suffix: FText(
                  "",
                  size: 24.sp,
                  color: const Color(0xFF999999)),
              onPress: onTapoptionsate),
          // SetListItem("个人信息管理",
          //     suffix: FText(
          //         "",
          //         size: 24.sp,
          //         color: const Color(0xFF999999)),
          //     onPress: onTapgementate),
          SetListItem("应用权限说明",
              suffix: FText(
                  "",
                  size: 24.sp,
                  color: const Color(0xFF999999)),
              onPress: onTapgementate),
          SetListItem("个人信息收集清单",
              suffix: FText(
                  "",
                  size: 24.sp,
                  color: const Color(0xFF999999)),
              onPress: onTaPesontate),
          SetListItem("系统权限设置",
              suffix: FText(
                  "",
                  size: 24.sp,
                  color: const Color(0xFF999999)),
              onPress: onTapmissionate),
          SetListItem("隐私说明",
              suffix: FText(
                  "",
                  size: 24.sp,
                  color: const Color(0xFF999999)),
              onPress: onTapAuthate),
        ],
      ),
    );
  }

  void onTapheiPrivate() {
    WidgetUtils().pushPage(SetBlack());
  }

  void onTapoptionsate() {
    WidgetUtils().pushPage(SetrestPage());

  }

  void onTapgementate() {

    WidgetUtils().pushPage(ProtocolPage(type: ProtocolType.PROUANXIAN));
  }

  void onTaPesontate() {

    WidgetUtils().pushPage(ProtocolPage(type: ProtocolType.PROPRSON));
  }

  void onTapmissionate() {
    WidgetUtils().pushPage(SetPermission());
  }

  void onTapAuthate() {
    WidgetUtils().pushPage(SetAuth());
  }

  void getNotificationStatus() async {
    openPush = await Permission.notification.isGranted;
    if (!mounted) return;
    setState(() {});
  }

  void onTapBroadcast() async {
    UserConstants.openBroad = !UserConstants.openBroad;
    await UserConstants.setNoticeSet();
    if (!mounted) return;
    setState(() {});
  }

  void onTapAudio() async {
    UserConstants.openAudio = !UserConstants.openAudio;
    await UserConstants.setNoticeSet();
    if (!mounted) return;
    setState(() {});
  }

  void onTapShock() async {
    UserConstants.openShock = !UserConstants.openShock;
    await UserConstants.setNoticeSet();
    if (!mounted) return;
    setState(() {});
  }

  void onTapFans() async {
    UserConstants.openFans = !UserConstants.openFans;
    await UserConstants.setNoticeSet();
    if (!mounted) return;
    setState(() {});
  }

  void onTapPush() async {
    openAppSettings();
  }
}
