import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class AuiponeList extends StatefulWidget {
  @override
  _AuiponeListState createState() => _AuiponeListState();
}

class _AuiponeListState extends State<AuiponeList> {
  int curPage = 1;
  List blackList = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFF5F6F7),
      appBar: const CommonAppBar(
        title: "登录设备",
        textColor: Colors.black,
        elevation: 1,
        backgroundColor: Color(0xFFF5F6F7),
      ),
      body:  Column(
        children: [

          FContainer(
            padding: const EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
            height: 100.w,
            color: Color(0xFF0D291846),
            child:  FText('登录设备管理显示最近登录过您账号的设备情况 若您发现非本人操作的设备，请及时移除，并更换密码以保障您的帐号安全。', color: Color(0xFF99291846),maxLines: 2,),
          ),
      Expanded(child: billList(),),

        ],
      ) ,

    );
  }
  Widget billList() {
      return FRefreshLayout(
        onRefresh: callRefresh,
        // onLoad: callLoad,
        emptyWidget: blackList.isEmpty ? const FEmptyWidget() : null,
        child:  ListView.builder(
          padding: EdgeInsets.fromLTRB(25.w, 40.h, 25.w, 0),
          itemBuilder: (_, index) => blackCell(index),
          itemCount: blackList.length,
          itemExtent: 120.w,
        ),
      );
  }
  Widget blackCell(int index) {
    return Row(children: [
      Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ConstrainedBox(
              constraints: BoxConstraints(maxWidth: 260.w),
              child: FText(blackList[index]["device_name"],color: Colors.black,),
            ),
            FText("${DateTime.fromMillisecondsSinceEpoch(blackList[index]["last_login_time"])
                .toString()
                .substring(0, 19)}",
                size: 24.sp, color: const Color(0xFF999999))
          ]),
      const Spacer(),
      buttonRemove(blackList[index]["device_code"])
    ]);
  }

  Widget buttonRemove(String userId) {
    Widget btn = FContainer(
      width: 50.w,
      height: 50.w,
      align: Alignment.center,
      child:   LoadAssetImage("person/iponesX" ),
    );
    return GestureDetector(onTap: () => onTapRemove(userId), child: btn);
  }

  void onTapRemove(String userId) {
    DioUtils().asyncHttpRequest(
      NetConstants.delete_deviceBlack,
      method: DioMethod.POST,
      params: {"device_code": userId},
      onSuccess: (_) => onRemove(userId),
    );
  }

  void onRemove(response) {
    blackList.removeWhere((e) => e["device_code"] == response);
    if (!mounted) return;
    setState(() {});
  }

  Future<void> callRefresh() async {
    curPage = 1;
    requestList(true);
  }

  Future<void> callLoad() async {
    curPage += 1;
    requestList(false);
  }

  void requestList(bool isRefresh) {
    DioUtils().asyncHttpRequest(
      NetConstants.userdeviceBlack,
      method: DioMethod.GET,
      onSuccess: isRefresh ? onRefresh : onLoad,
    );
  }

  void onRefresh(response) {
    print('666666666');
    print(response);
    blackList = response ?? [];
    if (!mounted) return;
    setState(() {});
  }

  void onLoad(response) {
    List list = response ?? [];
    if (list.isEmpty) return;
    blackList.addAll(list);
    if (!mounted) return;
    setState(() {});
  }
}
