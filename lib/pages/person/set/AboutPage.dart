import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:flutter/services.dart';

class AboutPage extends StatefulWidget {
  const AboutPage({super.key});

  @override
  State<AboutPage> createState() => _AboutPageState();
}

class _AboutPageState extends State<AboutPage> {
  String developS = '';
  String app_name = '';
  String app_url = '';
  String app_wx = '';

  @override
  void initState() {
    super.initState();
    getdevelop();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFF5F6F7),
      appBar: const CommonAppBar(
        title: "关于我们",
        textColor: Colors.black,
        elevation: 1,
        backgroundColor: Color(0xFFF5F6F7),
      ),
      body: Container(
        padding: EdgeInsets.fromLTRB(0.w, 80.h, 0.w, 0.w),
        child: Column(
          children: [
            FContainer(
              width: 140.w,
              height: 140.w,
              radius: BorderRadius.circular(20.w),
              image: ImageUtils().getAssetsImage("logo"),
              imageFit: BoxFit.cover,
            ),
            SizedBox(height: 20.h),
            Center(
              child: FText(app_name.isNotEmpty ? app_name : "金豆派对",
                  size: 32.sp, color: const Color(0xFF222222)),
            ),
            SizedBox(height: 5.h),
            Center(
              child: FText("版本号 V${AppConstants.version}",
                  size: 28.sp, color: const Color(0xFF999999)),
            ),
            SizedBox(height: 5.h),
            Center(
              child: FText(AppConstants.appBuild,
                  size: 22.sp, color: const Color(0xFF999999)),
            ),
            SizedBox(height: 70.h),
            UpdateContent(),
            buttonInfocontCell(app_url),
            buttonInfoCell(),
            const Spacer(),
            Center(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 50.w),
                child: FText(
                  developS,
                  color: const Color(0xFF999999),
                  maxLines: 4,
                ),
              ),
            ),
            SizedBox(
              height:
                  Platform.isIOS ? ScreenUtil().bottomBarHeight + 20.h : 20.h,
            ),
          ],
        ),
      ),
    );
  }

  Widget buttonInfoCell() {
    Widget btn = FContainer(
      height: 120.w,
      color: const Color(0xFFF5F6F7),
      border: Border(
        bottom: BorderSide(
            width: 1, color: const Color(0xFFD9D9D9).withOpacity(0.1)),
      ),
      padding: EdgeInsets.symmetric(horizontal: 5.w),
      child: Row(children: [
        SizedBox(
          width: 15.w,
        ),
        const FText("官方公众号", color: Color(0xFF222222)),
        const Spacer(),
        FText(app_wx.isNotEmpty ? app_wx : "金豆派对", color: Color(0xFF999999)),
        LoadAssetImage("person/rootzi", width: 40.w),
        SizedBox(
          width: 10.w,
        )
      ]),
    );
    return GestureDetector(onTap: () => onTaplescol("官方公众号"), child: btn);
  }

//复制公众号
  Future<void> onTaplescol(String s) async {
    Clipboard.setData(ClipboardData(text: s));
    WidgetUtils().showToast("公众号复制成功");
  }

  Widget buttonInfocontCell(String app_url) {
    Widget btn = FContainer(
      height: 120.w,
      color: const Color(0xFFF5F6F7),
      border: Border(
        bottom: BorderSide(
            width: 1, color: const Color(0xFFD9D9D9).withOpacity(0.1)),
      ),
      padding: EdgeInsets.symmetric(horizontal: 5.w),
      child: Row(children: [
        SizedBox(
          width: 15.w,
        ),
        const FText("官网链接", color: Color(0xFF222222)),
        const Spacer(),
        FText(app_url, color: Color(0xFF999999)),
        LoadAssetImage("person/rootzi", width: 40.w),
        SizedBox(
          width: 10.w,
        )
      ]),
    );
    return GestureDetector(onTap: () => onTapcol(app_url), child: btn);
  }

//复制官网
  void onTapcol(String app_url) {
    Clipboard.setData(ClipboardData(text: app_url));
    WidgetUtils().showToast("官网复制成功");
  }

  void getdevelop() {
    DioUtils().asyncHttpRequest(NetConstants.recordAudio,
        method: DioMethod.GET,
        params: {"conf_key": "about_us"}, onSuccess: (res) {
      debugPrint(res);
      if (res == null) return;
      var jsonStr = jsonDecode(res);
      developS = jsonStr["company_name"].toString();
      app_name = jsonStr["app_name"].toString();
      app_url = jsonStr["app_url"].toString();
      app_wx = jsonStr["app_wx"].toString();
      debugPrint(app_name);
      debugPrint(app_url);
      if (!mounted) return;
      setState(() {});
    });
  }
}
