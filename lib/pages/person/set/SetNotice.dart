import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:permission_handler/permission_handler.dart';


class SetNotice extends StatefulWidget {
  @override
  _SetNoticeState createState() => _SetNoticeState();
}

class _SetNoticeState extends State<SetNotice> with WidgetsBindingObserver {
  bool openPush = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    getNotificationStatus();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    switch (state) {
      case AppLifecycleState.resumed:
        getNotificationStatus();
        break;
      case AppLifecycleState.inactive:
        break;
      case AppLifecycleState.paused:
        break;
      case AppLifecycleState.detached:
        break;
      case AppLifecycleState.hidden:
        break;
    }
  }

  @override
  void deactivate() {
    WidgetsBinding.instance.removeObserver(this);
    super.deactivate();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFF5F6F7),
      appBar: const CommonAppBar(
        title: "消息通知设置",
        textColor: Colors.black,
        elevation: 1,
        backgroundColor: Color(0xFFF5F6F7),
      ),
      body: ListView(
        padding: EdgeInsets.fromLTRB(25.w, 40.h, 25.w, 0),
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: FText("直播通知", size: 24.sp, color: const Color(0xFF222222)),
          ),
          SizedBox(height: 20.h),
          SizedBox(
            height: 100.w,
            child: Row(children: [
              FText("开播通知", size: 32.sp, color: const Color(0xFF222222)),
              const Spacer(),
              FBtnOpen(
                onTapBroadcast,
                initOpen: UserConstants.openBroad,
              ),
            ]),
          ),
          const Divider(color: Color(0xFFEEEEEE)),
          Align(
            alignment: Alignment.centerLeft,
            child: FText("新消息提醒", size: 24.sp, color: const Color(0xFF222222)),
          ),
          SizedBox(
            height: 100.w,
            child: Row(children: [
              FText("声音", size: 32.sp, color: const Color(0xFF222222)),
              const Spacer(),
              GestureDetector(
                onTap: onTapAudio,
                child: LoadAssetImage(
                    UserConstants.openAudio ? "person/on3" : "person/off3",
                    width: 80.w),
              )
            ]),
          ),
          SizedBox(
            height: 100.w,
            child: Row(children: [
              FText("震动", size: 32.sp, color: const Color(0xFF222222)),
              const Spacer(),
              GestureDetector(
                onTap: onTapShock,
                child: LoadAssetImage(
                    UserConstants.openShock ? "person/on3" : "person/off3",
                    width: 80.w),
              )
            ]),
          ),
          const Divider(color: Color(0xFFEEEEEE)),
          Align(
            alignment: Alignment.centerLeft,
            child: FText("其他通知", size: 24.sp, color: const Color(0xFF999999)),
          ),
          SizedBox(
            height: 100.w,
            child: Row(children: [
              FText("新粉丝通知", size: 32.sp, color: const Color(0xFF222222)),
              const Spacer(),
              GestureDetector(
                onTap: onTapFans,
                child: LoadAssetImage(
                    UserConstants.openFans ? "person/on3" : "person/off3",
                    width: 80.w),
              )
            ]),
          ),
          const Divider(color: Color(0xFFEEEEEE)),
          Align(
            alignment: Alignment.centerLeft,
            child: FText("你可能错过重要资讯通知，点击去设置允许通知",
                size: 24.sp, color: const Color(0xFF999999)),
          ),
          SizedBox(
            height: 100.w,
            child: Row(children: [
              FText("推送通知", size: 32.sp, color: const Color(0xFF222222)),
              const Spacer(),
              GestureDetector(
                onTap: onTapPush,
                child: LoadAssetImage(openPush ? "person/on3" : "person/off3",
                    width: 80.w),
              )
            ]),
          ),
        ],
      ),
    );
  }

  void getNotificationStatus() async {
    openPush = await Permission.notification.isGranted;
    if (!mounted) return;
    setState(() {});
  }

  void onTapBroadcast() async {
    UserConstants.openBroad = !UserConstants.openBroad;
    await UserConstants.setNoticeSet();
    if (!mounted) return;
    setState(() {});
  }

  void onTapAudio() async {
    UserConstants.openAudio = !UserConstants.openAudio;
    await UserConstants.setNoticeSet();
    if (!mounted) return;
    setState(() {});
  }

  void onTapShock() async {
    UserConstants.openShock = !UserConstants.openShock;
    await UserConstants.setNoticeSet();
    if (!mounted) return;
    setState(() {});
  }

  void onTapFans() async {
    UserConstants.openFans = !UserConstants.openFans;
    await UserConstants.setNoticeSet();
    if (!mounted) return;
    setState(() {});
  }

  void onTapPush() async {
    openAppSettings();
  }
}
