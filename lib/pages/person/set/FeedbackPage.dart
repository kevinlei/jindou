import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class FeedbackPage extends StatefulWidget {
  final FeedbackType type;
  final String? userId;
  final String? roomId;
  final String? dynamic_id;
  const FeedbackPage(
      {Key? key, required this.type, this.userId, this.roomId, this.dynamic_id})
      : super(key: key);

  @override
  _FeedbackPageState createState() => _FeedbackPageState();
}

class _FeedbackPageState extends State<FeedbackPage>
    with WidgetsBindingObserver {
  String inputText = "";

  LoginProvider provider = LoginProvider();
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => provider,
        builder: (_, __) {
          return Scaffold(
            backgroundColor: const Color(0xFFF5F6F7),
            body: GestureDetector(
              onTap: () {
                FocusScope.of(context).requestFocus(FocusNode());
              },
              child: SafeArea(child: loginContent()),
            ),
          );
        });
  }

  Widget loginContent() {
    return Scaffold(
      backgroundColor: const Color(0xFFF5F6F7),
      appBar: CommonAppBar(
        title: widget.type.title,
        textColor: Colors.black,
        elevation: 1,
        backgroundColor: const Color(0xFFF5F6F7),
      ),
      body: ListView(
        padding: EdgeInsets.fromLTRB(25.w, 40.h, 25.w, 0),
        children: [
          Visibility(
            visible: widget.type == FeedbackType.DYNAMIC_REPORTING_USER,
            child: FText(
              '举报类型',
              color: const Color(0xFF222222),
              size: 32.sp,
            ),
          ),
          Visibility(
            visible: widget.type == FeedbackType.DYNAMIC_REPORTING_USER,
            child: SizedBox(height: 30.h),
          ),
          Visibility(
            visible: widget.type == FeedbackType.DYNAMIC_REPORTING_USER,
            child: DynamicLabel(provider.onChargeSelect),
          ),
          Visibility(
            visible: widget.type == FeedbackType.DYNAMIC_REPORTING_USER,
            child: SizedBox(height: 30.h),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: FText(
              widget.type.hint,
              color: const Color(0xFF222222),
              size: 32.sp,
            ),
          ),
          SizedBox(height: 30.h),
          inputField(),
          SizedBox(height: 200.h),
          Consumer<LoginProvider>(builder: (_, pro, __) {
            return FcationButton("提交",
                isActivate: pro.neirongtext.isNotEmpty,
                txtColor: Colors.white,
                padding: 0,
                radius: 90.w,
                onPress: onTapConfirm);
          }),
        ],
      ),
    );
  }

  Widget inputField() {
    InputBorder border = const OutlineInputBorder(
        borderSide: BorderSide(color: Colors.transparent));
    return FContainer(
      radius: BorderRadius.circular(20.w),
      color: const Color(0xFF222222).withOpacity(0.05),
      height: 350.h,
      padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 10.h),
      child: TextField(
        style: TextStyle(color: const Color(0xFF222222), fontSize: 28.sp),
        maxLines: 9,
        maxLength: 200,
        onChanged: onneirongtextInput,
        decoration: InputDecoration(
          enabledBorder: border,
          focusedBorder: border,
          hintText: "内容描述...",
          contentPadding: EdgeInsets.zero,
          hintStyle: TextStyle(color: const Color(0xFFBBBBBB), fontSize: 28.sp),
          counterStyle:
              TextStyle(color: const Color(0xFFBBBBBB), fontSize: 26.sp),
        ),
      ),
    );
  }

  Future<void> onneirongtextInput(String value) async {
    value = value.replaceAll(" ", "").trim();
    inputText = value;
    provider.onneirongtextInput(value);
  }

  void onTextInput(value) => inputText = value;

  void onTapConfirm() {
    if (inputText.trimRight().isEmpty) return WidgetUtils().showToast("请输入内容");
    if (widget.type == FeedbackType.DYNAMIC_REPORTING_USER) {
      requestReportUser();
    } else if (widget.type == FeedbackType.LOGIN_PROBLEM_FEEDBACK) {
      if (widget.roomId == null) return;
      requestFeedRoom();
    } else if (widget.type == FeedbackType.APP_PROBLEM_FEEDBACK) {
      requestFeedRoom();
    }
  }

  void requestReportUser() {
    if (widget.userId == null || widget.userId!.isEmpty) return;
    DioUtils().asyncHttpRequest(
      NetConstants.reportUser,
      method: DioMethod.POST,
      params: {
        "user_id": widget.userId ?? "",
        "reason": inputText,
        "type": provider.chargeNum,
        "dynamic_id": int.parse(widget.dynamic_id ?? "")
      },
      onSuccess: onReportSuccess,
    );
  }

  void requestFeedRoom() {
    DioUtils().asyncHttpRequest(
      NetConstants.roomadvice,
      method: DioMethod.POST,
      params: {
        "create_id": widget.userId ?? "",
        "advice": inputText,
        "phone": UserConstants.userginInfo?.info.phone
      },
      onSuccess: onFeedSuccess,
    );
  }

  void onReportSuccess(response) {
    WidgetUtils().showToast("举报成功");
    WidgetUtils().popPage();
  }

  void onFeedSuccess(response) {
    WidgetUtils().showToast("反馈成功");
    WidgetUtils().popPage();
  }
}
