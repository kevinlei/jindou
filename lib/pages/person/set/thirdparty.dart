import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class thirdparty extends StatefulWidget {
  final String orderId;

  const thirdparty({Key? key, required this.orderId}) : super(key: key);

  @override
  _thirdpartyState createState() => _thirdpartyState();
}

class _thirdpartyState extends State<thirdparty> {
  String phone = "";
  String code = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: const CommonAppBar(title: "手机绑定"),
      body: Column(children: [
        SizedBox(height: 40.h),
        LoadAssetImage("person/restmot", width: 150.w),
        SizedBox(height: 20.h),
        FText("您已绑定手机号134****1234",
            size: 34.sp,
            weight: FontWeight.bold,
            color: const Color(0xFF222222)),
        // FText("当前手机号：${phone.replaceRange(3, 7, "****")}",
        //     size: 34.sp, weight: FontWeight.bold),
        SizedBox(height: 40.h),
        Row(
          children: [
            SizedBox(width: 40.h),
            FText("注：",
                size: 24.sp, color: const Color(0xFF222222).withOpacity(0.6)),
          ],
        ),
        SizedBox(
          height: 80.h,
          child: Expanded(
            child: Row(
              children: [
                SizedBox(width: 40.h),
                Expanded(
                  child: FText(
                      "1.无法解绑手机号:手机号一旦换绑，则所关联的实名认证手机号、提现手机号将全部更换为新手机号.",
                      maxLines: 100,
                      size: 24.sp,
                      color: const Color(0xFF222222).withOpacity(0.6)),
                ),
                SizedBox(width: 20.h),
              ],
            ),
          ),
        ),
        SizedBox(
          height: 80.h,
          child: Expanded(
            child: Row(
              children: [
                SizedBox(width: 40.h),
                Expanded(
                  child: FText(
                      "2.由于您选择了提现信息作为您的绑定手机号，换绑成功后将会以换绑定的手机作为您新的提现绑定手机号",
                      maxLines: 100,
                      size: 24.sp,
                      color: const Color(0xFF222222).withOpacity(0.6)),
                ),
                SizedBox(width: 20.h),
              ],
            ),
          ),
        ),
        SizedBox(
          height: 80.h,
          child: Expanded(
            child: Row(
              children: [
                SizedBox(width: 40.h),
                Expanded(
                  child: FText("3.若您的帐号使用手机号登录，换绑后，将使用新号码作为登录手机号。",
                      maxLines: 100,
                      size: 24.sp,
                      color: const Color(0xFF222222).withOpacity(0.6)),
                ),
                SizedBox(width: 20.h),
              ],
            ),
          ),
        ),
        SizedBox(height: 100.h),
        FcationButton("更换手机号",
            isActivate: true, radius: 90.w, onPress: onTapConfirm),
      ]),
    );
  }

  void onTapConfirm() async {
    // if (!FuncUtils().isMobile(phone)) {
    //   WidgetUtils().showToast("请输入正确的手机号");
    //   return;
    // }
    // if (!FuncUtils().isMobileCode(code)) {
    //   WidgetUtils().showToast("请输入正确的验证码");
    //   return;
    // }
    // bool success = await requestBind();
    // if (!success) return;
    // UserConstants.userInfo?.phone = phone;
    WidgetUtils().showToast("修改成功");
    WidgetUtils().popPage();
  }
}
