import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class QuitPage4 extends StatefulWidget {
  const QuitPage4({super.key});

  @override
  State<QuitPage4> createState() => _QuitPage4State();
}

class _QuitPage4State extends State<QuitPage4> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF5F6F7),
      appBar: CommonAppBar(
        title: "手机验证",
        textColor: Colors.black,
        elevation: 1,
        backgroundColor: Color(0xFFF5F6F7),
      ),
      body: authContent(),

    );
  }

  Widget authContent() {

    return Column(children: [
      SizedBox(
        height: 50.h,
      ),
      LoadAssetImage("person/yanzheng", width: 340.w, height: 240.w),
      SizedBox(height: 20.h),
      FText('账号处于注销流程中', size: 36.sp, color: Color(0xFF222222)),
      SizedBox(height: 20.h),

     FText('该账号处于注销流程中，注销犹豫期还剩余2天12小时03分钟，继续登录心遇将撤销对该账号的注销。', maxLines:2,align: TextAlign.center,size: 26.sp, color: Color(0xFF222222).withOpacity(0.6)),

      SizedBox(height: 40.h),
      FcationButton("确认登录",
          isActivate: true, radius: 90.w, onPress: quickLogin),
    ]);
  }
  Future<void> quickLogin() async {
    bool? accept = await WidgetUtils().showAlert("系统发现您在本平台加入的有公会哦，是否确认退出公会注销账号",confirmText: '我再想想',cancelText:'确认注销',tirText:'平台权限');

    if (accept == null || !accept) return;

    WidgetUtils().pushPage(thirdparty(orderId: '',));
  }
}
