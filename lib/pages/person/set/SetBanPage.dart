
import 'package:flutter/material.dart';
import 'package:echo/import.dart';

import '../helpcenter/HelpCenterSon.dart';
// import 'HelpCenterSon.dart';


class SetBanPage extends StatefulWidget {
  const SetBanPage({Key? key}) : super(key: key);

  @override
  _SetBanPageState createState() => _SetBanPageState();
}

class _SetBanPageState extends State<SetBanPage> {
  TextEditingController controller = TextEditingController();
  FocusNode focusNode = FocusNode();
  late EasyRefreshController _controller;
  int questPage = 0;
  List helpCenterList = [];
  List helpCenSonList = [];

  @override
  void initState() {
    super.initState();
    _controller = EasyRefreshController();
    gethelpList();
  }

  void gethelpList() async {
    await DioUtils().httpRequest(NetConstants.helpCenter,
        params: {"category_type":2,"p_category":10},
        method: DioMethod.GET, loading: false, onSuccess: (response) async {
          // var jsonStr = jsonDecode(response["CategoryInfo"]);
          helpCenterList = response["CategoryInfo"] ?? [];
          int index = response["CategoryInfo"][0]['id'] ?? 0;
          questPage = index;
          getHelpSon(index, false);
          setState(() {});
        });
  }

  void getHelpSon(int id, bool state) async {
    await DioUtils().httpRequest(NetConstants.helpCenterSon,
        method: DioMethod.GET,
        params: {
          "category_id": 0,
          "is_sub_category":1,
          "sub_category_id": id,
        }, onSuccess: (response) async {
          // if(response?['list'].length == 0) return;
          List list = response?['articles'] ?? [];
          if (state) {
            helpCenSonList.add(list);
          } else {
            helpCenSonList = list;
          }
          setState(() {});
        });
  }

  Future<void> callRefresh() async {
    getHelpSon(questPage, false);
  }

  void onQuestPage(int index) {
    questPage = index;
    getHelpSon(index, false);
    setState(() {});
  }

  void conCusSer() {}


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CommonAppBar(
          title: "帮助中心",
          textColor: Colors.black,
          elevation: 1,
          backgroundColor: Color(0xFFF5F6F7),
        ),
        backgroundColor: Color(0xFFF5F6F7),
        body: Padding(
          padding: EdgeInsets.all(0.w),
          child: Column(
            children: [
              // Row(
              //   children: [
              //     Expanded(child: inputSend()),
              //     SizedBox(width: 10.w),
              //     buttonSend()
              //   ],
              // ),

              // questionList(),
              Expanded(
                child: problemDetails(),
              )
            ],
          ),
        ));
  }





  Widget questionList() {
    return FContainer(
        padding: EdgeInsets.symmetric(horizontal: 25.w),
        radius: BorderRadius.only(
            topLeft: Radius.circular(20.w), topRight: Radius.circular(0.w)),
        height: 90.w,
        color: Color(0xFF505050).withOpacity(0.1),
        child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: helpCenterList.length,
            itemBuilder: (_, index) {
              int id = helpCenterList[index]['id'] ?? 0;
              return GestureDetector(
                onTap: () => onQuestPage(id),
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 15.w),
                  padding: EdgeInsets.only(top: 25.w),
                  decoration: BoxDecoration(
                      border: questPage == id
                          ? Border(
                          bottom: BorderSide(
                              width: 3.w, color: Color(0xFFB965FF)))
                          : null),
                  child: FText(helpCenterList[index]['category_name'],
                      size: 28.sp,
                      align: TextAlign.center,
                      color: questPage == id ? Colors.white : Color(0xFF686A7A),
                      weight: questPage == id ? FontWeight.bold : null),
                ),
              );
            }));
  }

  Widget problemDetails() {
    return FContainer(
        padding: EdgeInsets.symmetric(horizontal: 25.w),
        margin: EdgeInsets.only(top: 3.w),
        radius: BorderRadius.only(
            bottomLeft: Radius.circular(20.w),
            bottomRight: Radius.circular(20.w)),
        color: Color(0xFF505050).withOpacity(0.1),
        child: FRefreshLayout(
            onRefresh: callRefresh,
            controller: _controller,
            firstRefresh: false,
            emptyWidget: helpCenSonList.length == 0 ? FEmptyWidget() : null,
            child: ListView.builder(
                itemCount: helpCenSonList.length,
                itemBuilder: (_, index) {
                  String text = helpCenSonList[index]?['article_name'] ?? '';
                  return GestureDetector(
                    onTap: () {
                      WidgetUtils()
                          .pushPage(HelpCenterSon(data: helpCenSonList[index]));
                    },
                    child: Container(
                        padding: EdgeInsets.symmetric(vertical: 25.w),
                        decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                    width: 1.w, color: Color(0xFF686A7A)))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                                child: FText(
                                  text,
                                  size: 28.sp,
                                  color: Colors.black,
                                )),
                            LoadAssetImage(
                              'set/right',
                              fit: BoxFit.cover,
                              height: 20.w,
                              color: Colors.black,
                            ),
                          ],
                        )),
                  );
                })));
  }

  @override
  void dispose() {
    controller.dispose();
    _controller.dispose();
    focusNode.dispose();
    super.dispose();
  }
}
