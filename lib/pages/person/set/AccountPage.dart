import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class AccountPage extends StatefulWidget {
  const AccountPage({Key? key}) : super(key: key);

  @override
  _AccountPageState createState() => _AccountPageState();
}

class _AccountPageState extends State<AccountPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFF5F6F7),
      appBar: const CommonAppBar(
        title: "账号与安全",
        textColor: Colors.black,
        elevation: 1,
        backgroundColor: Color(0xFFF5F6F7),
      ),
      body: ListView(children: [
        SetListItem(
          "注册方式",
          suffix: FText("手机号", size: 24.sp, color: const Color(0xFF999999)),
        ),
        // SetListItem(
        //   "鸣音ID",
        //   suffix: FText(UserConstants.userInfo?.displayId ?? "",
        //       size: 24.sp, color: Color(0xFF999999)),
        // ),
        SetListItem("登录密码", onPress: onTapPwd),
        SetListItem(
          "绑定手机",
          suffix: FText(UserConstants.userginInfo?.info.phone ?? "",
              size: 24.sp, color: const Color(0xFF999999)),
          onPress: onTapPhone,
        ),
        // SetListItem(
        //   "第三方绑定",
        //   // suffix: FText(UserConstants.userInfo?.phone ?? "",
        //   suffix: FText("手机号", size: 24.sp, color: const Color(0xFF999999)),
        //   onPress: onTapPhone,
        // ),
        AuthContent(),
        SetListItem("登录设备", onPress: onTapPayPwd),
        // SetListItem("二级密码", onPress: onTapPayPwd),
        // SetListItem("注销账号", onPress: onTapQuit),
      ]),
    );
  }

  void onTapPhone() async {
    await WidgetUtils().pushPage(const BindPhone1());
    if (!mounted) return;
    setState(() {});
  }

  void onTapPwd() {
    WidgetUtils().pushPage(const SetPwdPage(isLoginPwd: true));
  }

  void onTapPayPwd() {
    WidgetUtils().pushPage(AuiponeList());
  }

  void onTapQuit() {
    WidgetUtils().pushPage(QuitPage1());
  }
}
