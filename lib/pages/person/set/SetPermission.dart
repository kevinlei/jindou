import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:permission_handler/permission_handler.dart';

class SetPermission extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF5F6F7),
      appBar: CommonAppBar(
        title: "权限设置",
        textColor: Colors.black,
        elevation: 1,
        backgroundColor: Color(0xFFF5F6F7),
      ),
      body: ListView(
        padding: EdgeInsets.fromLTRB(25.w, 40.h, 25.w, 0),
        children: [
          PermissionCell("网络无线数据", "查看无线数据使用目的",
              type: ProtocolType.PER_WIFI, permission: Permission.unknown),
          PermissionCell("通知权限", "查看通知权限使用目的",
              type: ProtocolType.PER_NOTICE,
              permission: Permission.notification),
          PermissionCell("授权电话信息", "查看电话权限使用目的",
              type: ProtocolType.PER_PHONE, permission: Permission.phone),
          PermissionCell("访问位置信息", "查看位置信息使用目的",
              type: ProtocolType.PER_LOC,
              permission: Permission.locationWhenInUse),
          PermissionCell("使用麦克风", "查看麦克风权限使用目的",
              type: ProtocolType.PER_MICRO, permission: Permission.microphone),
          PermissionCell("使用相机权限", "查看相机权限使用目的",
              type: ProtocolType.PER_CAMERA, permission: Permission.camera),
          PermissionCell("使用相册权限", "查看相册使用目的",
              type: ProtocolType.PER_PHOTO,
              permission: Permission.photos,
              showLine: false),
        ],
      ),
    );
  }
}
