import 'dart:io';

import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:flutter/services.dart';

class SetPwdPage extends StatefulWidget {
  final bool isLoginPwd;

  const SetPwdPage({Key? key, required this.isLoginPwd}) : super(key: key);

  @override
  _SetPwdPageState createState() => _SetPwdPageState();
}

class _SetPwdPageState extends State<SetPwdPage> {
  String pwd = "";

  String confirm = "";

  String code = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFF5F6F7),
      appBar: CommonAppBar(
        title: widget.isLoginPwd ? "设置登录密码" : "设置二级密码",
        textColor: Colors.black,
        elevation: 1,
        backgroundColor: const Color(0xFFF5F6F7),
      ),
      body: ListView(
        padding: EdgeInsets.fromLTRB(25.w, 40.h, 25.w, 0),
        keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
        children: [
          // Center(
          FText(
            "手机号：${UserConstants.userginInfo?.info.phone ?? ""}",
            size: 32.sp,
            weight: FontWeight.bold,
            color: const Color(0xFF222222),
          ),
          // ),
          SizedBox(height: 20.h),
          // Center(
          FText("请输入新登录密码",
              size: 32.sp,
              weight: FontWeight.bold,
              color: const Color(0xFF222222)),
          // ),
          SizedBox(height: 20.h),
          // Center(

          FText("密码为6-20位，至少包含字母、数字2种组合",
              size: 24.sp,
              weight: FontWeight.bold,
              color: const Color(0x99222222)),
          // ),
          inputField(1),
          inputField(2),
          inputField(3),
          SizedBox(height: 20.h),
          // Center(
          Row(
            children: [
              SizedBox(width: 60.h),
              FText("通过短信验证可以使用新密码",
                  size: 24.sp, color: const Color(0xFFBBBBBB)),
            ],
          ),

          // ),
          SizedBox(height: 60.h),
          Center(
            child: FcationButton("提交",
                isActivate: true, radius: 90.w, onPress: onTapConfirm),
          ),
        ],
      ),
      // bottomNavigationBar: FContainer(
      //     padding: EdgeInsets.only(
      //         bottom: Platform.isIOS ? ScreenUtil().bottomBarHeight : 0),
      //     child:
      //     color: Color(0xFFF5F6F7)),
    );
  }

  Widget inputField(int index) {
    String title = "", hint = "";
    int maxLen = 0;
    TextInputFormatter formatter;
    Function(String) onTextInput;
    if (index == 1) {
      title = "";
      hint = "请设置您的密码";
      maxLen = widget.isLoginPwd ? 15 : 6;
      formatter = widget.isLoginPwd
          ? FilteringTextInputFormatter.allow(RegExp(r'[0-9A-Za-z]'))
          : FilteringTextInputFormatter.digitsOnly;
      onTextInput = onPwdInput;
    } else if (index == 2) {
      title = "";
      hint = "请再次输入您的密码";
      maxLen = widget.isLoginPwd ? 15 : 6;
      formatter = widget.isLoginPwd
          ? FilteringTextInputFormatter.allow(RegExp(r'[0-9A-Za-z]'))
          : FilteringTextInputFormatter.digitsOnly;
      onTextInput = onConfirmInput;
    } else {
      title = "";
      hint = "请输入验证码";
      maxLen = 6;
      formatter = FilteringTextInputFormatter.digitsOnly;
      onTextInput = onCodeInput;
    }
    InputBorder border = const OutlineInputBorder(
        borderSide: BorderSide(color: Colors.transparent));
    Widget field = TextField(
      maxLength: maxLen,
      inputFormatters: [formatter],
      onChanged: onTextInput,
      obscureText: index != 3,
      obscuringCharacter: "*",
      style: TextStyle(color: Colors.black, fontSize: 28.sp),
      decoration: InputDecoration(
        enabledBorder: border,
        focusedBorder: border,
        contentPadding: EdgeInsets.zero,
        hintText: hint,
        hintStyle: TextStyle(fontSize: 28.sp, color: const Color(0xFFBBBBBB)),
        counterText: "",
      ),
    );

    return FContainer(
      height: 88.w,
      radius: BorderRadius.circular(88.w),
      color: const Color(0xFF0F222222),
      margin: EdgeInsets.only(top: 40.h),
      padding: EdgeInsets.fromLTRB(40.w, 0, 20.w, 0),
      child: Row(children: [
        FText(title, size: 32.sp, nullLength: 0),
        Expanded(child: field),
        if (index == 3) CodeButton(requestCode)
      ]),
    );
  }

  void onPwdInput(value) => pwd = value;

  void onConfirmInput(value) => confirm = value;

  void onCodeInput(value) => code = value;

  Future<bool> requestCode() async {
    String phone = UserConstants.userginInfo?.info.phone ?? "";
    // if (!FuncUtils().isMobile(phone)) {
    //   WidgetUtils().showToast("请输入正确的手机号");
    //   return false;
    // }
    bool success = false;
    await DioUtils().httpRequest(
      NetConstants.loginCode,
      method: DioMethod.POST,
      params: {"phone": phone, "send_type": 0,"platform": Platform.isAndroid ? "android" : "ios","temp":SMSType.SMS_TEMPLATE_PASSWORD.value},
      onSuccess: (_) => success = true,
    );
    return success;
  }

  void onTapConfirm() {
    if (widget.isLoginPwd) {
      if (pwd.length < 6) return WidgetUtils().showToast("登录密码至少6位");
      if (!RegExp(
              '^(?![0-9]+\$)(?![a-zA-Z]+\$)(?![0-9._-]+\$)(?![a-zA-Z._-]+\$)(?!._-)[0-9A-Za-z._-]{6,15}\$')
          .hasMatch(pwd)) return WidgetUtils().showToast("登录密码必须包含数字和字母");
    } else {
      if (!RegExp('\\d{6}\$').hasMatch(pwd)) {
        return WidgetUtils().showToast("密码有6位数字组成");
      }
    }
    if (confirm.isEmpty) return WidgetUtils().showToast("请确认您的密码输入");
    if (confirm != pwd) return WidgetUtils().showToast("请确认您的密码输入是否正确");
    if (!FuncUtils().isMobileCode(code)) {
      return WidgetUtils().showToast("请输入正确的验证码");
    }
    requestChange();
  }

  void requestChange() {
    String phone = UserConstants.userginInfo?.info.phone ?? "";
    DioUtils().asyncHttpRequest(
      widget.isLoginPwd ? NetConstants.resetPwd : NetConstants.resetSecondPwd,
      method: DioMethod.POST,
      params: {
        "phone": phone,
        "zone": "",
        "code": code,
        "pass_word": pwd,
    "platform": Platform.isAndroid ? "android" : "ios"
      },
      onSuccess: onSetSuccess,
    );
  }

  void onSetSuccess(response) {
    WidgetUtils().showToast("设置成功");
    WidgetUtils().popPage();
  }
}
