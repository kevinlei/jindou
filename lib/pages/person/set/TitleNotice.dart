import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class TitleNotice extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double top = ScreenUtil().statusBarHeight;
    double bot = ScreenUtil().bottomBarHeight;
    return FContainer(
      color: Color(0xFF26282B),
      image: ImageUtils().getAssetsImage("set/titleBg"),
      imageFit: BoxFit.cover,
      padding: EdgeInsets.fromLTRB(0, top, 0, bot),
      child: Column(children: [
        titleAppbar(),
        SizedBox(height: 30.w),
        Expanded(
            child: ListView(children: [
          FText("什么是爵位？",
              weight: FontWeight.bold, color: Color(0xFFF9CF88), size: 32.sp),
          SizedBox(height: 30.w),
          FText("爵位等级采用充值制度，充值有效期为30天，开通爵位勋有返利优惠，具体优惠请咨询客服。",
              color: Color(0xFF868686), maxLines: 10, size: 28.sp),
          SizedBox(height: 60.w),
          FText("爵位如何提升？",
              weight: FontWeight.bold, color: Color(0xFFF9CF88), size: 32.sp),
          SizedBox(height: 30.w),
          FText("爵位值通过钱包金币购买提升爵位等级！",
              color: Color(0xFF868686), maxLines: 10, size: 28.sp),
          SizedBox(height: 30.w),
          FText("1金币=1爵位值",
              color: Color(0xFF868686), maxLines: 10, size: 28.sp),
          SizedBox(height: 120.w),
          FText("爵位如何划分？",
              weight: FontWeight.bold, color: Color(0xFFF9CF88), size: 32.sp),
          SizedBox(height: 30.w),
          LoadAssetImage("set/notice"),
          SizedBox(height: 90.w),
          FText("爵位如何展示？",
              weight: FontWeight.bold, color: Color(0xFFF9CF88), size: 32.sp),
          SizedBox(height: 30.w),
          FText("爵位等级将会在个人主页和个人资料卡展示，除此之外还会有以下展示场景：",
              color: Color(0xFF868686), maxLines: 10, size: 28.sp),
          SizedBox(height: 50.w),
          FText("1. 爵位值标识会在房间内的聊天公屏展示您的身份所有人都能看到。",
              color: Color(0xFF868686), maxLines: 10, size: 28.sp),
          SizedBox(height: 50.w),
          FText("2. 升级至侯爵及以上大级时，在房间内展示升级提示，所有人都能看到哦~",
              color: Color(0xFF868686), maxLines: 10, size: 28.sp),
          SizedBox(height: 50.w),
          FText("3. 升级至王爵/以上大级时，进房展示专享入场特效。",
              color: Color(0xFF868686), maxLines: 10, size: 28.sp),
          SizedBox(height: 220.w),
          FText("注：财富等级、魅力等级和爵位等级自2021年1月1日起按规则计算。",
              color: Color(0xFF4B4B4B), maxLines: 10, size: 22.sp),
          SizedBox(height: 50.w),
        ], padding: EdgeInsets.symmetric(horizontal: 25.w)))
      ]),
    );
  }

  Widget titleAppbar() {
    return Row(children: [
      BackButton(color: Colors.white),
      FText("爵位说明", size: 36.sp, color: Colors.white, weight: FontWeight.bold),
      IconButton(onPressed: null, icon: SizedBox())
    ], mainAxisAlignment: MainAxisAlignment.spaceBetween);
  }
}
