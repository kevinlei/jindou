import 'dart:io';

import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:flutter/services.dart';

class BindPhone2 extends StatefulWidget {
  final String orderId;

  const BindPhone2({Key? key, required this.orderId}) : super(key: key);

  @override
  _BindPhone2State createState() => _BindPhone2State();
}

class _BindPhone2State extends State<BindPhone2> {
  String phone = "";
  String code = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: CommonAppBar(title: "新号验证"),
      body: Column(children: [
        inputField(true),
        inputField(false),
      ]),

      bottomNavigationBar: Padding(
        padding: EdgeInsets.only(
            bottom: Platform.isIOS ? ScreenUtil().bottomBarHeight
                : 0),
        child: FcationButton("确定",
            isActivate: true, radius: 90.w, onPress: onTapConfirm),
      ),
      // bottomSheet: Material(
      //   color: Colors.white,
      //   // child: FsetButton("确定", radius: 90.w, onPress: onTapConfirm),
      //   child: FcationButton("确定",
      //       isActivate: true, radius: 90.w, onPress: onTapConfirm),
      // ),
    );
  }

  Widget inputField(bool isPhone) {
    return FContainer(
      height: 88.w,
      radius: BorderRadius.circular(88.w),
      color: Color(0xFFF2F3F8),
      padding: EdgeInsets.symmetric(horizontal: 25.w),
      margin: EdgeInsets.fromLTRB(25.w, 40.h, 25.w, 0),
      child: Row(children: [
        Visibility(
            visible: isPhone,
            child: FText(
              "手机号：",
              size: 32.sp,
        color: Colors.black,
            )),
        Expanded(child: textField(isPhone)),
        Visibility(child: CodeButton(requestCode), visible: !isPhone)
      ]),
    );
  }

  Widget textField(bool isPhone) {
    InputBorder border =
        OutlineInputBorder(borderSide: BorderSide(color: Colors.transparent));

    return TextField(
      maxLength: isPhone ? 11 : 6,
      style: TextStyle(color: Colors.black, fontSize: 30.sp),
      onChanged: (value) => onTextInput(value, isPhone),
      inputFormatters: [FilteringTextInputFormatter.digitsOnly],
      decoration: InputDecoration(
        focusedBorder: border,
        enabledBorder: border,
        counterText: "",
        contentPadding: EdgeInsets.zero,
        hintText: isPhone ? "请输入您的新手机号" : "请输入短信验证码",
        hintStyle: TextStyle(color: Color(0xFFBBBBBB), fontSize: 30.sp),
      ),
    );
  }

  void onTextInput(String value, bool isPhone) {
    if (isPhone) {
      phone = value;
    } else {
      code = value;
    }
  }

  Future<bool> requestCode() async {
    // if (!FuncUtils().isMobile(phone)) {
    //   WidgetUtils().showToast("请输入正确的手机号");
    //   return false;
    // }
    bool isSuccess = false;
    await DioUtils().httpRequest(
      NetConstants.loginCode,
      method: DioMethod.POST,
      params: {"phone": phone, "send_type": 4,"platform": Platform.isAndroid ? "android" : "ios","temp":SMSType.SMS_TEMPLATE_BIND.value},
      onSuccess: (_) => isSuccess = true,
    );
    return isSuccess;
  }

  Future<bool> requestBind() async {
    bool success = false;
    await DioUtils().httpRequest(
      NetConstants.bindPhone,
      method: DioMethod.POST,
      params: {
        "new_phone": phone,
        "code": code,
        "key": widget.orderId,
        "platform": Platform.isAndroid ? "android" : "ios",
      },
      onSuccess: (_) => success = true,
    );
    return success;
  }

  void onTapConfirm() async {
    // if (!FuncUtils().isMobile(phone)) {
    //   WidgetUtils().showToast("请输入正确的手机号");
    //   return;
    // }
    if (!FuncUtils().isMobileCode(code)) {
      WidgetUtils().showToast("请输入正确的验证码");
      return;
    }
    bool success = await requestBind();
    if (!success) return;
    UserConstants.userginInfo?.info.phone = phone;
    WidgetUtils().showToast("修改成功");
    WidgetUtils().popPage();
  }
}
