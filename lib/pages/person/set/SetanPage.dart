import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:echo/import.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

class SetanPage extends StatefulWidget {
  @override
  _SetanPageState createState() => _SetanPageState();
}

class _SetanPageState extends State<SetanPage> {
  String cacheValue = "密码/手机号管理";

  @override
  void initState() {
    super.initState();

  }

  @override
  Widget build(BuildContext context) {
    return SetListItem("安全中心",
        suffix: FText(cacheValue, size: 24.sp, color: Color(0xFF999999)),
        onPress: clearCache);
  }
  void clearCache() async {

    WidgetUtils().pushPage(const AccountPage());
  }

}
