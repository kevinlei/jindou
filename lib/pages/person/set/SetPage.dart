import 'package:echo/import.dart';
import 'package:flutter/material.dart';

import 'Preferences.dart';

class SetPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFF5F6F7),
      appBar: const CommonAppBar(
        title: "设置",
        textColor: Colors.black,
        elevation: 1,
        backgroundColor: Color(0xFFF5F6F7),
      ),
      body: ListView(children: [
        SizedBox(height: 20.h),
        SetanPage(),
        // AuthContent(),
        const Divider(color: Color(0xFF14222222)),
        SetListItem("新消息通知", onPress: onTapNotice),
        SetListItem("隐私设置", onPress: onTapYinsPrivate),
        SetListItem("偏好设置", onPress: onTapPrivate),
        SetListItem("青少年模式", onPress: onTapYoung),
        // SetListItem("黑名单", onPress: onTapBlack),
        const Divider(color: Color(0xFF14222222)),
        CacheContent(),
        // SetListItem("设置手机权限", onPress: onTapPermission),
        SetListItem("常见问题", onPress: onTapMiniMode),
        // SetListItem("极简模式", onPress: onTapMiniMode),

        // UpdateContent(),
        const Divider(color: Color(0xFF14222222)),
        SetListItem("意见反馈", onPress: onTapFeedback),
        SetListItem("关于我们", onPress: onTapAbout),
        SizedBox(height: 40.h),
        FcationButton('退出登录',
            isActivate: true, onPress: onTapQuite, radius: 90.w),
      ]),
    );
  }

  void onTapNotice() {
    WidgetUtils().pushPage(SetNotice());
  }

  void onTapBlack() {
    WidgetUtils().pushPage(SetBlack());
  }

  //隐私设置
  void onTapYinsPrivate() {
    WidgetUtils().pushPage(Preferences());
  }

  //偏好设置
  void onTapPrivate() {
    WidgetUtils().pushPage(AubentPend());
  }

  void onTapPermission() {
    WidgetUtils().pushPage(SetPermission());
  }

  void onTapYoung() {
    WidgetUtils().pushPage(const YoungPage());
  }

  //常见问题
  void onTapMiniMode() async {
    WidgetUtils().pushPage(SetBanPage());
    // SharedPreferences preferences = await SharedPreferences.getInstance();
    // bool miniMode = preferences.getBool(SharedKey.KEY_MINI_MODE.value) ?? false;
    // await WidgetUtils().showFDialog(MinimalistMode(
    //   "极简模式${miniMode ? "已开启" : ""}",
    //   "开启极简模式后app将关闭部分动画与特效，同时提高app的运行速度和稳定性，推荐在模拟器中运行时打开。",
    //   (bool state) async {
    //     preferences.setBool(SharedKey.KEY_MINI_MODE.value, state);
    //   },
    //   miniMode: miniMode,
    // ));
  }

  void onTapFeedback() {
    WidgetUtils().pushPage(FeedbackPage(
      type: FeedbackType.APP_PROBLEM_FEEDBACK,
      userId: UserConstants.userginInfo!.info.userId,
    ));
  }

  void onTapAbout() {
    WidgetUtils().pushPage(const AboutPage());
  }

  void onTapQuite() async {
    bool? accept = await WidgetUtils().showAlert("退出会清理部分缓存信息，确定退出吗？");
    if (accept == null || !accept) return;
    WidgetUtils().showLoading();
    AppConstants.accessToken = "";
    AppConstants.refreshToken = "";
    UserConstants.userginInfo = null;
    SharedPreferences preferences = await SharedPreferences.getInstance();

    await preferences.clear();
    preferences.setBool(SharedKey.KEY_UNIQUE.value, false);
    preferences.setString(
        SharedKey.KEY_MACHINE.value, AppConstants.machineCode);

    AppConstants.initAppInfo();
    Future.delayed(const Duration(seconds: 1), () async {
      WidgetUtils().cancelLoading();
      WidgetUtils().pushRemove(const LoginPage());
    });
  }
}
