import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class AubentPend extends StatefulWidget {
  @override
  _AubentPendState createState() => _AubentPendState();
}

class _AubentPendState extends State<AubentPend> with WidgetsBindingObserver {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  @override
  void deactivate() {
    WidgetsBinding.instance.removeObserver(this);
    super.deactivate();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFF5F6F7),
      appBar: const CommonAppBar(
        title: "偏好设置",
        textColor: Colors.black,
        elevation: 1,
        backgroundColor: Color(0xFFF5F6F7),
      ),
      body: ListView(
        padding: EdgeInsets.fromLTRB(15.w, 20.h, 15.w, 0),
        children: [
          Platform.isAndroid
              ? SizedBox(
                  height: 60.w,
                  child: Row(children: [
                    SizedBox(width: 20.h),
                    FText("后台持续在房",
                        size: 30.sp, color: const Color(0xFF222222)),
                    FText("防闪退", size: 22.sp, color: const Color(0xFFFF753F)),
                    const Spacer(),
                    GestureDetector(
                      onTap: onTapcast,
                      child: LoadAssetImage(
                        UserConstants.openflash ? "person/on3" : "person/off3",
                        width: 90.w,
                      ),
                    ),
                    SizedBox(width: 20.h),
                  ]),
                )
              : Container(),
          SetListItem("防闪退/声音/消息送达攻略", onPress: onTapAbout),
        ],
      ),
    );
  }

//防闪退/声音/消息送达攻略
  void onTapAbout() {
    WidgetUtils().pushPage(const ProtocolPage(type: ProtocolType.PROREST));
  }

//后台持续在房
  Future<void> onTapcast() async {
    if (UserConstants.openflash == true) {
      UserConstants.openflash = !UserConstants.openflash;
    } else {
      if (!AppConstants.device.contains('HONOR')) {
        await WidgetUtils().showFDialog(WillPopScope(
          onWillPop: () => Future.value(false),
          child: const ShowreDialog(force: null),
        ));
      } else {
        await WidgetUtils().showFDialog(WillPopScope(
          onWillPop: () => Future.value(false),
          child: ShowRiltDialog(
            title: "权限开启",
            subotitle: AppConstants().activation(),
          ),
        ));
      }
    }
    if (!mounted) return;
    setState(() {});
  }
}
