import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class SetAuth extends StatefulWidget {
  @override
  _SetAuthState createState() => _SetAuthState();
}

class _SetAuthState extends State<SetAuth> {
  bool showOnline = true;

  bool showRelation = true;

  bool showWall = true;

  @override
  void initState() {
    super.initState();
    // requestUserSet();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF5F6F7),
      appBar: CommonAppBar(
        title: "隐私说明",
        textColor: Colors.black,
        elevation: 1,
        backgroundColor: Color(0xFFF5F6F7),
      ),
      body: ListView(
        padding: EdgeInsets.fromLTRB(25.w, 20.h, 25.w, 0),
        children: [
          // SizedBox(
          //   height: 100.w,
          //   child: Row(children: [
          //     FText("展示在线状态", size: 32.sp,color: Color(0xFF222222)),
          //     Spacer(),
          //     GestureDetector(
          //       onTap: onTapOnline,
          //       child: LoadAssetImage(showOnline ? "person/on3" : "person/off3",
          //           width: 80.w),
          //     )
          //   ]),
          // ),
          // SizedBox(
          //   height: 100.w,
          //   child: Row(children: [
          //     FText("展示我的关注/粉丝/足迹/访客", size: 32.sp,color: Color(0xFF222222)),
          //     Spacer(),
          //     GestureDetector(
          //       onTap: onTapRelation,
          //       child: LoadAssetImage(showRelation ? "person/on3" : "person/off3",
          //           width: 80.w),
          //     )
          //   ]),
          // ),
          // SizedBox(
          //   height: 100.w,
          //   child: Row(children: [
          //     FText("展示我的礼物墙", size: 32.sp,color: Color(0xFF222222)),
          //     Spacer(),
          //     GestureDetector(
          //       onTap: onTapWall,
          //       child: LoadAssetImage(showWall ? "person/on3" : "person/off3",
          //           width: 80.w),
          //     )
          //   ]),
          // ),

          buttonCell(ProtocolType.PRO_USER, 1),
          buttonCell(ProtocolType.PRO_PRIVACY, 2),
          buttonCell(ProtocolType.PRO_CHILD, 3),
          buttonCell(ProtocolType.PRO_SDK_HELP, 17),
          buttonCell(ProtocolType.PRO_SCN_HELP, 18),
        ],
      ),
    );
  }

  Widget buttonCell(ProtocolType type, int index) {
    Widget btn = FContainer(
      height: 100.w,
      color: Color(0xFFF5F6F7),
      border: Border(
        bottom: BorderSide(width: 1, color: Color(0xFFD9D9D9).withOpacity(0.1)),
      ),
      padding: EdgeInsets.symmetric(horizontal: 5.w),
      child: Row(children: [
        FText(type.text, color: Color(0xFF222222)),
        Spacer(),
        Icon(Icons.arrow_forward_ios, size: 30.w, color: Color(0xFF999999))
      ]),
    );
    return GestureDetector(onTap: () => onTapProtocol(type), child: btn);
  }

  void onTapProtocol(ProtocolType type) {
    WidgetUtils().pushPage(ProtocolPage(type: type));
  }

  void onTapOnline() {
    showOnline = !showOnline;
    requestSetUser();
  }

  void onTapRelation() {
    showRelation = !showRelation;
    requestSetUser();
  }

  void onTapWall() {
    showWall = !showWall;
    requestSetUser();
  }

  // "ShowOnlineStatus":true,"ShowRelation":true,"ShowGiftWall":true
  void requestUserSet() {
    String userId = UserConstants.userginInfo?.info.userId ?? "";
    DioUtils().asyncHttpRequest(
      NetConstants.getUserSet + userId,
      onSuccess: onGetStatus,
    );
  }

  void onGetStatus(response) {
    showOnline = response?["showOnlineStatus"] ?? true;
    showRelation = response?["showRelation"] ?? true;
    showWall = response?["showGiftWall"] ?? true;
    if (!mounted) return;
    setState(() {});
  }

  void requestSetUser() {
    DioUtils().asyncHttpRequest(
      NetConstants.setUserSet,
      method: DioMethod.POST,
      params: {
        "showOnlineStatus": showOnline,
        "showRelation": showRelation,
        "showGiftWall": showWall,
      },
      onSuccess: onSetStatus,
    );
  }

  void onSetStatus(response) {
    if (!mounted) return;
    setState(() {});
  }
}
