import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:flutter/services.dart';

class QuitPage2 extends StatefulWidget {
  @override
  _QuitPage2State createState() => _QuitPage2State();
}

class _QuitPage2State extends State<QuitPage2> with WidgetsBindingObserver{
  List<String> reasonList = [];
  TextEditingController controller = TextEditingController();
  LoginProvider provider = LoginProvider();
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => provider,
        builder: (_, __) {
          return Scaffold(
            backgroundColor: Color(0xFFF5F6F7),
            body:GestureDetector(
              onTap: (){
                FocusScope.of(context).requestFocus(FocusNode());
              },
              child: SafeArea(child: loginContent()),
            ),);

        });
  }
  Widget loginContent(){
    return Scaffold(
      backgroundColor: Color(0xFFF5F6F7),
      appBar: CommonAppBar(
        title: "注销账号",
        textColor: Colors.black,
        elevation: 1,
        backgroundColor: Color(0xFFF5F6F7),
      ),
      body: ListView(
          padding: EdgeInsets.fromLTRB(25.w, 40.h, 25.w, 0),
          keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
          children: [
            Row(children: [
              _QuitReason("想要注册新号", onAccept: onAccept),
              _QuitReason("这是多余账号", onAccept: onAccept),
            ]),
            Row(children: [
              _QuitReason("个人原因", onAccept: onAccept),
              _QuitReason("不喜欢本类型APP", onAccept: onAccept),
            ]),
            Row(children: [
              _QuitReason("APP频繁闪退，体验不好", onAccept: onAccept),
              _QuitReason("其他原因", onAccept: onAccept),
            ]),
            SizedBox(height: 10.w),
            Row(children: [
              FText('欢迎留下您的意见和建议',color: Color(0x80222222)),
            ],),

            inputField(),
            SizedBox(height: 20.w),

            TextField(
            // controller: usetNameTextEditController,
            decoration:InputDecoration(
    hintText: "请填写您的联系方式（选填）",
    filled: true,
    fillColor: Colors.grey[100],
              hintStyle: TextStyle(color: Color(0xFFBBBBBB), fontSize: 28.sp),
                enabledBorder: new UnderlineInputBorder( // 不是焦点的时候颜色
                  borderSide: BorderSide(
                      color: Color(0x19000000)
                  ),
                ),
                  focusedBorder: new UnderlineInputBorder( // 焦点集中的时候颜色
                  borderSide: BorderSide(
    color: Color(0x19000000)
    ),
    ),

    ) ,

    ),

            SizedBox(height: 20.w),
            Container(
              child: Row(
                children: [
                  FText("上传图片证据（选填）",color: Color(0x80222222),  ),
                  Spacer(),
                  FText("0/3",color: Color(0x80222222),  size: 32.sp,),
                ],
              ),
            ),
            SizedBox(height: 20.h),

            ImgContent(urlList: []),
            SizedBox(height: 67*2.h),
            FLoginButton("立即注销",isActivate: true, radius: 90.w, onPress: onTapNext),
          ]),

    );

  }
  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  Widget inputField() {
    InputBorder border =
        OutlineInputBorder(borderSide: BorderSide(color: Colors.transparent));
    return FContainer(
      color: Color(0xFF000000).withOpacity(0.05),
      height: 400.w,
      radius: BorderRadius.circular(20.w),
      margin: EdgeInsets.only(top: 20.h),
      padding: EdgeInsets.all(20.w),
      child: TextField(
        maxLines: 16,
        maxLength: 300,
        controller: controller,
        // inputFormatters: [FilteringTextInputFormatter.deny(RegExp(r"[\s]"))],
        style: TextStyle(color: Colors.black, fontSize: 26.sp),
        decoration: InputDecoration(
          enabledBorder: border,
          focusedBorder: border,
          hintText: "请为我们留下宝贵的意见",
          contentPadding: EdgeInsets.zero,
          hintStyle: TextStyle(color: Color(0xFFBBBBBB), fontSize: 28.sp),
          counterStyle: TextStyle(color: Color(0xFFBBBBBB), fontSize: 26.sp),
        ),
      ),
    );
  }



  void onAccept(bool accept, String reason) {
    if (accept) {
      reasonList.add(reason);
    } else {
      reasonList.remove(reason);
    }
  }

  void onTapNext() {
    if (reasonList.isEmpty) return WidgetUtils().showToast("请选择一个注销原因");
    WidgetUtils().pushPage(
        // QuitPage3(reason: reasonList.join(","), proposal: controller.text));
    QuitPage3());
  }
}

class _QuitReason extends StatefulWidget {
  final String reason;
  final Function(bool, String) onAccept;
  const _QuitReason(this.reason, {Key? key, required this.onAccept})
      : super(key: key);

  @override
  __QuitReasonState createState() => __QuitReasonState();
}

class __QuitReasonState extends State<_QuitReason> {
  bool accept = false;

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: GestureDetector(
        onTap: onTapCell,
        child: FContainer(
          height: 80.w,
          align: Alignment.centerLeft,
          child: Row(children: [
            LoadAssetImage(accept ? "person/on1" : "person/off1",
                width: 34.w, height: 34.w),
            SizedBox(width: 10.w),
            Expanded(
                child: FText(widget.reason,
                    size: 26.sp, overflow: TextOverflow.ellipsis,color:accept?  Color(0xFFFF753F):Colors.black,))
          ]),
        ),
      ),
    );
  }

  void onTapCell() {
    accept = !accept;
    widget.onAccept(accept, widget.reason);
    if (!mounted) return;
    setState(() {});
  }
}
