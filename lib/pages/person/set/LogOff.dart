import 'package:echo/import.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';

class LogOff extends StatefulWidget {
  // final String code;
  // final String reason;
  // final String proposal;
  // const LogOff(this.code, this.reason, this.proposal, {super.key});

  @override
  State<LogOff> createState() => _LogOffState();
}

class _LogOffState extends State<LogOff> {
  bool hasAccept = false;
  String systembulletin = '';

  @override
  void initState() {
    super.initState();
    getText();
  }

  void getText() {
    DioUtils().asyncHttpRequest(
      NetConstants.protocol,
      method: DioMethod.POST,
      loading: false,
      params: {"id": 2},
      onSuccess: (res) {
        try {
          setState(() {
            systembulletin = (res?["content"] ?? "");
          });
        } catch (err) {
          print(err);
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const CommonAppBar(
        title: "确认注销重要提醒",
        textColor: Colors.black,
        elevation: 1,
        backgroundColor: Color(0xFFF5F6F7),
      ),
      body: FContainer(
          padding: EdgeInsets.symmetric(vertical: 10.w, horizontal: 20.w),
          color: const Color(0xFFF5F6F7),
          child: ListView(
            children: [
              Row(
                children: [
                  Expanded(
                      child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 40.w),
                      FText(
                        "该账号可直接注销，注销前请认真阅读以下提醒:",
                        maxLines: 2,
                        color: Colors.black,
                        size: 34.sp,
                        wordSpacing: 1.5,
                        weight: FontWeight.bold,
                        align: TextAlign.left,
                        height: 1.5,
                      ),
                      SizedBox(height: 10.w),
                      Html(data: systembulletin, style: {
                        'p': Style(
                            fontSize: FontSize(28.sp),
                            color: const Color(0xFFB4B4B4).withOpacity(0.8),
                            lineHeight: LineHeight.em(1.2),
                            letterSpacing: 1.2,
                            wordSpacing: 2),
                        'span': Style(
                            fontSize: FontSize(28.sp),
                            color: const Color(0xFFB4B4B4).withOpacity(0.8),
                            lineHeight: LineHeight.em(1.2),
                            letterSpacing: 1.2,
                            wordSpacing: 2)
                      }),
                    ],
                  ))
                ],
              ),
            ],
          )),
      bottomNavigationBar: bottom(),
    );
  }

  Widget bottom() {
    return FContainer(
      height: 200.w,
      padding: EdgeInsets.symmetric(vertical: 10.w, horizontal: 20.w),
      color: const Color(0xFF1F2234),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Row(
            children: [
              selectButton(),
              FText(
                "我已阅读并同意 《金豆派对注销须知》",
                size: 24.sp,
                color: Colors.white,
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                  child: GestureDetector(
                onTap: () => WidgetUtils().pushRemove(SetPage()),
                child: FContainer(
                  padding: EdgeInsets.symmetric(vertical: 20.w),
                  radius: BorderRadius.circular(10.w),
                  gradient: [
                    const Color(0xFFF3A6FF).withOpacity(0.2),
                    const Color(0xFF8F36FF).withOpacity(0.2)
                  ],
                  gradientBegin: Alignment.centerLeft,
                  gradientEnd: Alignment.centerRight,
                  align: Alignment.center,
                  child: FText(
                    "放弃",
                    size: 30.sp,
                  ),
                ),
              )),
              SizedBox(width: 20.w),
              Expanded(
                  child: GestureDetector(
                onTap: logOff,
                child: FContainer(
                  padding: EdgeInsets.symmetric(vertical: 20.w),
                  radius: BorderRadius.circular(10.w),
                  gradient: [const Color(0xFFF3A6FF), const Color(0xFF8F36FF)],
                  gradientBegin: Alignment.centerLeft,
                  gradientEnd: Alignment.centerRight,
                  align: Alignment.center,
                  child: FText(
                    "确认注销",
                    size: 30.sp,
                    color: Colors.white,
                  ),
                ),
              )),
            ],
          )
        ],
      ),
    );
  }

  Widget selectButton() {
    Widget btn = IconButton(
        onPressed: onTapSelect,
        padding: EdgeInsets.zero,
        icon: LoadAssetImage(hasAccept ? "login/check2" : "login/check1",
            width: 30.w, height: 30.w));

    return SizedBox(width: 50.w, height: 50.w, child: btn);
  }

  void onTapSelect() {
    setState(() {
      hasAccept = !hasAccept;
    });
  }

  void logOff() async {
    if (!hasAccept) return WidgetUtils().showToast("请确认阅读并同意《金豆派对注销须知》");
    await WidgetUtils().showFDialog(MinimalistMode(
      "确认注销",
      "确认注销后，金豆派对将在7天内处理你的申请并删除帐号信息。手机号、第三方绑定、实名信息将被在7天后被释放，再次登录将会创建一个新的帐号。",
      (bool state) async {
        if (state) {
          await DioUtils().httpRequest(
            NetConstants.quitAccount,
            method: DioMethod.POST,
            params: {
              // "code": widget.code,
              // "proposal": widget.proposal,
              // "reason": widget.reason
            },
            onSuccess: onQuitSuccess,
          );
        }
      },
      confirmText: "确认",
      cancelText: "取消",
    ));
  }

  void onQuitSuccess(response) async {
    WidgetUtils().showLoading();
    AppConstants.accessToken = "";
    AppConstants.refreshToken = "";
    UserConstants.userginInfo = null;
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.clear();
    preferences.setBool(SharedKey.KEY_UNIQUE.value, false);
    Future.delayed(const Duration(seconds: 1), () async {
      WidgetUtils().cancelLoading();
      WidgetUtils().pushRemove(const LoginPage());
    });
  }
}
