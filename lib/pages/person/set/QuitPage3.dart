import 'package:f_verification_box/f_verification_box.dart';
import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:flutter/services.dart';

class QuitPage3 extends StatefulWidget {
  // final String reason;
  // final String proposal;
  // const QuitPage3({Key? key, required this.reason, required this.proposal})
  //     : super(key: key);

  @override
  State<QuitPage3> createState() => _QuitPage3State();
}

class _QuitPage3State extends State<QuitPage3> {
  String code = "";

  @override
  Widget build(BuildContext context) {
    String phone = UserConstants.userginInfo?.info.phone ?? "";
    return Scaffold(
      backgroundColor: const Color(0xFFF5F6F7),
      appBar: const CommonAppBar(
        title: "手机验证",
        textColor: Colors.black,
        elevation: 1,
        backgroundColor: Color(0xFFF5F6F7),
      ),
      body: ListView(
        padding: EdgeInsets.fromLTRB(25.w, 40.h, 25.w, 0),
        children: [
          FText("当前手机号：${phone.replaceRange(3, 7, "****")}",
              size: 34.sp, weight: FontWeight.bold),
          FText(
            "请输入验证码",
            size: 32.sp,
            weight: FontWeight.bold,
            color: Colors.black,
          ),
          SizedBox(height: 20.h),
          Row(
            children: [
              FText(
                "已发送至 +86 $phone",
                size: 30.sp,
                color: Colors.black,
              ),
              const Spacer(),
              CodeButton(requestCode),
            ],
          ),
          SizedBox(
            height: 150.h,
            child: const VerificationBox(
              type: VerificationBoxItemType.underline,
              borderWidth: 2,
              unfocus: false,
              focusBorderColor: Color(0xFFFF753F),
            ),
          ),
          // inputField(),
          // Divider(color: Color(0xFFA852FF)),
          SizedBox(height: 440.h),
          FcationButton("确认注销",
              isActivate: true, radius: 90.w, onPress: onTapNext),
        ],
      ),
    );
  }

  Widget inputField() {
    return SizedBox(
      height: 88.w,
      child: Row(
          children: [Expanded(child: textField()), CodeButton(requestCode)]),
    );
  }

  Widget textField() {
    InputBorder border = const OutlineInputBorder(
        borderSide: BorderSide(color: Colors.transparent));

    return TextField(
      maxLength: 4,
      style: TextStyle(color: Colors.black, fontSize: 30.sp),
      inputFormatters: [FilteringTextInputFormatter.digitsOnly],
      onChanged: onTextInput,
      decoration: InputDecoration(
        focusedBorder: border,
        enabledBorder: border,
        counterText: "",
        contentPadding: EdgeInsets.zero,
        hintText: "请输入短信验证码",
        hintStyle: TextStyle(color: const Color(0xFFBBBBBB), fontSize: 30.sp),
      ),
    );
  }

  Future<bool> requestCode() async {
    String phone = UserConstants.userginInfo?.info.phone ?? "";
    if (phone.isEmpty) return false;
    bool success = false;
    await DioUtils().httpRequest(
      NetConstants.loginCode,
      method: DioMethod.POST,
      params: {"phone": phone, "zone": "", "type": 2,"temp":SMSType.SMS_TEMPLATE_WITHDRAW.value},
      onSuccess: (_) => success = true,
    );
    return success;
  }

  void onTextInput(value) => code = value;

  void onTapNext() async {
    // if (widget.reason.isEmpty) return WidgetUtils().showToast("注销原因不能为空");
    // if (!FuncUtils().isMobileCode(code))
    //   return WidgetUtils().showToast("请输入正确的验证码");
    // WidgetUtils().pushPage(LogOff(code, widget.reason, widget.proposal));
    // WidgetUtils().pushPage(LogOff());
    WidgetUtils().pushPage(const QuitPage4());
  }
}
