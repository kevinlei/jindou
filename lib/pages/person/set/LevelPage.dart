
import 'dart:convert';

import 'package:echo/pages/person/set/widgets/LevelRichPage.dart';
import 'package:flutter/material.dart';
import 'package:echo/import.dart';

import '../romhome/model/aubents_model.dart';

class LevelPage extends StatefulWidget {
  final int index;
  LevelPage({Key? key, required this.index}) : super(key: key);

  @override
  _LevelPageState createState() => _LevelPageState();
}

class _LevelPageState extends State<LevelPage>
    with SingleTickerProviderStateMixin {
  late TabController tabCtr = TabController(length: 2, vsync: this);
  List<AubentsModel> memberList = [];
  List<AubentsModel> memberList1 = [];
  @override
  void dispose() {
    super.dispose();
    tabCtr.dispose();
  }

  ConfigLevel? richLevel;
  ConfigLevel? charmLevel;

  @override
  void initState() {
    super.initState();
    requestSetUser();
    requestSetUser1();
    int richExp = UserConstants.userginInfo?.gradeInfo.userGrade.wealth.grade ?? 0;
    int charmExp = UserConstants.userginInfo?.gradeInfo.userGrade.charm.grade ?? 0;
    richLevel = ConfigManager().getLevel(richExp);
    charmLevel = ConfigManager().getLevel(charmExp);
    tabCtr.animateTo(widget.index);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:Color(0xFF111E1B),
      body:FContainer(
        image: ImageUtils().getAssetsImage("person/bgs1"),
        imageFit: BoxFit.fill,
        imageAlign: Alignment.topCenter,
        child: Stack(children: [
          TabBarView(
            controller: tabCtr,
            children: [
              //   LevelTitlePage(),
              LevelRichPage(isRichPage: true, richLevel: richLevel,memberList: memberList,),
              LevelRichPage(isRichPage: false, charmLevel: charmLevel,memberList: memberList1),
            ],
          ),
          tabAppBar(),
        ]),
      ),


    );
  }

  Widget tabAppBar() {
    return Padding(
      padding: EdgeInsets.only(top: ScreenUtil().statusBarHeight),
      child: Row(children: [
        IconButton(
            onPressed: () => WidgetUtils().popPage(),
            icon: Icon(Icons.arrow_back_ios, color: Colors.white)),
        SizedBox(width: 50.w),
        Expanded(
          child: TabBar(
            labelColor: Color(0xFFEFE0C7),
            unselectedLabelColor: Color(0xFFEFE0C7).withOpacity(0.5),
            controller: tabCtr,
            labelStyle: TextStyle(fontSize: 36.sp, fontWeight: FontWeight.w600),
            unselectedLabelStyle:
                TextStyle(fontSize: 36.sp, fontWeight: FontWeight.w600),
            indicatorColor: Colors.transparent,
            tabs: [Tab(text: "财富"), Tab(text: "魅力")],
          ),
        ),

        SizedBox(width: 90.w),
        // IconButton(
        //     onPressed: onTapTip,
        //     icon: LoadAssetImage("set/tip",
        //         width: 40.w, color: Color(0xFF9F9790))),
      ]),
    );
  }

  void onTapTip() {
    // WidgetUtils().pushPage(TitleNotice());
  }
  void requestSetUser() {
    DioUtils().asyncHttpRequest(
      NetConstants.config,
      method: DioMethod.GET,
      params: {
        "conf_key": 'wealth_exclusive_right',
      },
      onSuccess: onSetStatus,
    );
  }

  void onSetStatus(response) {
    if (response == null) return;
    var jsonStr = jsonDecode(response);
    jsonStr ??= [];
    memberList.clear();
    for (var data in jsonStr) {
      AubentsModel memberModel = AubentsModel.fromJson(data);
      memberList.add(memberModel);
    }
    if (!mounted) return;
    setState(() {
    });
  }
  void requestSetUser1() {
    DioUtils().asyncHttpRequest(
      NetConstants.config,
      method: DioMethod.GET,
      params: {
        "conf_key": 'charm_exclusive_right',
      },
      onSuccess: onSetStatus1,
    );
  }

  void onSetStatus1(response) {
    if (response == null) return;
    var jsonStr = jsonDecode(response);
    jsonStr ??= [];
    memberList1.clear();
    for (var data in jsonStr) {
      AubentsModel memberModel = AubentsModel.fromJson(data);
      memberList1.add(memberModel);
    }
    if (!mounted) return;
    setState(() {
    });
  }


}
