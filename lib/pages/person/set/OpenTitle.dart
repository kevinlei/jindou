import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class OpenTitle extends StatefulWidget {
  final int curSelect;
  final List titleRecord;
  const OpenTitle(this.curSelect, this.titleRecord, {Key? key})
      : super(key: key);

  @override
  _OpenTitleState createState() => _OpenTitleState();
}

class _OpenTitleState extends State<OpenTitle> {
  final List<String> titles = ["子爵", "伯爵", "侯爵", "公爵", "王爵", "帝皇"];
  List recordList = [];
  int curTitle = 0;
  int curSelect = 0;
  int leftDay = 0;

  @override
  void initState() {
    super.initState();
    curSelect = widget.curSelect;
    recordList = widget.titleRecord;
    curTitle = recordList.isEmpty ? 0 : recordList.last["peerage"];
    int time = recordList.isEmpty ? 0 : recordList.last["peerageEndTime"];
    leftDay = DateTime.fromMillisecondsSinceEpoch(time)
        .difference(DateTime.now())
        .inDays;
  }

  @override
  Widget build(BuildContext context) {
    double top = ScreenUtil().statusBarHeight;
    double bot = ScreenUtil().bottomBarHeight;
    return Scaffold(
      backgroundColor: const Color(0xFF26282B),
      body: FContainer(
        image: ImageUtils().getAssetsImage("set/titleBg"),
        imageFit: BoxFit.cover,
        padding: EdgeInsets.fromLTRB(0, top, 0, bot),
        child: Column(children: [
          titleAppbar(),
          SizedBox(height: 30.w),
          titleTop(),
          SizedBox(height: 60.w),
          titleText1(),
          Expanded(child: titleSelect())
        ], crossAxisAlignment: CrossAxisAlignment.start),
      ),
      bottomNavigationBar: btnBuy(),
    );
  }

  Widget titleAppbar() {
    return Row(children: [
      const BackButton(color: Colors.white),
      FText("爵位开通", size: 36.sp, color: Colors.white, weight: FontWeight.bold),
      const IconButton(onPressed: null, icon: SizedBox())
    ], mainAxisAlignment: MainAxisAlignment.spaceBetween);
  }

  Widget titleTop() {
    String title = "暂未开通爵位";
    if (curTitle > 0) {
      DateTime time = DateTime.fromMillisecondsSinceEpoch(
          recordList.last["peerageEndTime"]);
      title = titles[curTitle - 1] + " " + time.toString().substring(0, 10);
    }
    return Row(children: [
      SizedBox(width: 25.w),
      headWidget(),
      SizedBox(width: 10.w),
      Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            FText(UserConstants.userginInfo?.info.name ?? "",
                color: const Color(0xFFF9CF88), size: 28.sp),
            FText(title, color: const Color(0xFF868686), size: 24.sp),
          ]),
      SizedBox(width: 25.w),
    ]);
  }

  Widget headWidget() {
    String head = UserConstants.userginInfo?.info.headIcon ?? "";
    return FContainer(
      width: 85.w,
      height: 85.w,
      border: Border.all(color: const Color(0xFFEBC277), width: 2.w),
      radius: BorderRadius.circular(85.w),
      color: const Color(0xFF282748),
      image: ImageUtils().getImageProvider(NetConstants.ossPath + head),
      imageFit: BoxFit.cover,
    );
  }

  Widget titleText1() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 25.w),
      child: const FText("开通爵位享专属特权",
          weight: FontWeight.bold, color: Colors.white),
    );
  }

  Widget titleSelect() {
    return GridView.builder(
      padding: EdgeInsets.symmetric(horizontal: 25.w, vertical: 30.w),
      physics: const NeverScrollableScrollPhysics(),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
        mainAxisExtent: 250.w,
        mainAxisSpacing: 40.w,
        crossAxisSpacing: 40.w,
      ),
      itemCount: 6,
      itemBuilder: (_, index) => titleCell(index),
    );
  }

  Widget titleCell(int index) {
    String image = '';
    // if (index < ConfigManager().titleConfig.length) {
    //   image =
    //       NetConstants.ossPath + ConfigManager().titleConfig[index].headPicture;
    // }
    Color border =
        curSelect == index ? const Color(0xFFEBC277) : const Color(0xFF3B3B3B);

    Widget btn = FContainer(
      radius: BorderRadius.circular(20.w),
      color: const Color(0xFF202224),
      border: Border.all(color: border, width: 2.w),
      padding: EdgeInsets.only(top: 20.w),
      child: Column(children: [
        Expanded(child: LoadNetImage(image)),
        SizedBox(height: 20.w),
        FText(titles[index], color: const Color(0xFF868686), size: 24.sp),
        SizedBox(height: 20.w),
        cellBottom(index)
      ]),
    );
    return GestureDetector(onTap: () => onTapTitle(index), child: btn);
  }

  Widget cellBottom(int index) {
    Color border =
        curSelect == index ? const Color(0xFFEBC277) : const Color(0xFF3B3B3B);
    int price = 0;
    // if (index < ConfigManager().titleConfig.length) {
    //   price = ConfigManager().titleConfig[index].price;
    // }
    return FContainer(
      height: 60.w,
      color: curSelect == index ? const Color(0xFFEBC277) : Colors.transparent,
      border: curSelect == index
          ? null
          : Border(top: BorderSide(color: border, width: 2.w)),
      radius: curSelect == index
          ? BorderRadius.only(
              bottomRight: Radius.circular(16.w),
              bottomLeft: Radius.circular(16.w),
            )
          : null,
      align: Alignment.center,
      child: FText("$price金币",
          color: curSelect == index
              ? const Color(0xFF333333)
              : const Color(0xFF868686)),
    );
  }

  void onTapTitle(int index) {
    if (index == curSelect) return;
    if (index < curTitle - 1 && leftDay > 0) return;
    curSelect = index;
    if (!mounted) return;
    setState(() {});
  }

  Widget btnBuy() {
    if (curTitle > 0 && leftDay > 0 && curSelect < curTitle - 1)
      return const SizedBox();
    Widget txt;
    int rebate = 0;
    String text = "";
    txt = FRichText([
      FRichTextItem()
        ..text = "续费成功"
        ..color = const Color(0xFF868686),
      FRichTextItem()
        ..text = "奖励${rebate / 100}金币"
        ..color = const Color(0xFFF9CF88),
    ], fontSize: 22.sp);
    // String text = "立即支付${ConfigManager().titleConfig[curSelect].price}金币";
    // if (curTitle > 0 && leftDay > 0 && curSelect + 1 == curTitle) {
    //   rebate = (ConfigManager().titleConfig[curSelect].renew *
    //       ConfigManager().titleConfig[curSelect].rebate);
    //   txt = FRichText([
    //     FRichTextItem()
    //       ..text = "续费成功"
    //       ..color = Color(0xFF868686),
    //     FRichTextItem()
    //       ..text = "奖励${rebate / 100}金币"
    //       ..color = Color(0xFFF9CF88),
    //   ], fontSize: 22.sp);
    //   text = "立即续费${ConfigManager().titleConfig[curSelect].renew}金币";
    // } else {
    //   rebate = (ConfigManager().titleConfig[curSelect].price *
    //       ConfigManager().titleConfig[curSelect].rebate);
    //   txt = FRichText([
    //     FRichTextItem()
    //       ..text = "购买成功"
    //       ..color = Color(0xFF868686),
    //     FRichTextItem()
    //       ..text = "奖励${rebate / 100}金币"
    //       ..color = Color(0xFFF9CF88),
    //   ], fontSize: 22.sp);
    // }
    txt = Align(
        alignment: Alignment.centerLeft,
        child: Padding(padding: EdgeInsets.only(left: 25.w), child: txt));

    Widget btn = FContainer(
      radius: BorderRadius.circular(80.w),
      height: 80.w,
      gradientBegin: Alignment.centerLeft,
      gradientEnd: Alignment.centerRight,
      gradient: [const Color(0xFFFFF1AE), const Color(0xFFE6B86B)],
      margin: EdgeInsets.symmetric(horizontal: 25.w, vertical: 10.w),
      align: Alignment.center,
      child: FText(text, size: 32.sp),
    );
    return SafeArea(
        child: Column(children: [
      Visibility(child: txt, visible: rebate > 0),
      GestureDetector(onTap: onTapBuy, child: btn)
    ], mainAxisSize: MainAxisSize.min));
  }

  void requestRecord() {
    DioUtils()
        .asyncHttpRequest(NetConstants.titleRecord, onSuccess: onTitleRecord);
  }

  // {"Peerage":1,"PeerageEndTime":1638338786634}
  void onTitleRecord(response) {
    recordList = response ?? [];
    if (!mounted) return;
    setState(() {});
  }

  void onTapBuy() {
    // int id = ConfigManager().titleConfig[curSelect].id;
    // DioUtils().asyncHttpRequest(
    //   NetConstants.buyTitle,
    //   method: DioMethod.POST,
    //   params: {"id": id},
    //   onSuccess: onBuySuccess,
    // );
  }

  void onBuySuccess(response) {
    WidgetUtils().showToast("购买成功");
    WidgetUtils().popPage();
  }
}
