import 'dart:io';

import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class QuitPage1 extends StatefulWidget {
  @override
  _QuitPage1State createState() => _QuitPage1State();
}

class _QuitPage1State extends State<QuitPage1> with WidgetsBindingObserver {
  bool hasAccept = false;

  bool safeAccount = true;
  bool safeWallet = true;
  bool safeBag = true;
  bool safeInfo = true;
  bool safeTitle = true;
  bool safeRight = true;

  bool acceptWallet = false;
  bool acceptBag = false;
  bool acceptTitle = false;
  LoginProvider provider = LoginProvider();
  @override
  void initState() {
    super.initState();
    requestQuiteCondition();
    requestBagList();
    requestSelfInfo();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => provider,
        builder: (_, __) {
          return Scaffold(
            backgroundColor: Color(0xFFF5F6F7),
            body: GestureDetector(
              onTap: () {
                FocusScope.of(context).requestFocus(FocusNode());
              },
              child: SafeArea(child: loginContent()),
            ),
          );
        });
  }

  Widget loginContent() {
    return Scaffold(
      backgroundColor: Color(0xFFF5F6F7),
      appBar: CommonAppBar(
        title: "注销账号",
        textColor: Colors.black,
        elevation: 1,
        backgroundColor: Color(0xFFF5F6F7),
      ),
      body: ListView(
        padding: EdgeInsets.fromLTRB(25.w, 40.h, 25.w, 0),
        children: [
          QuitCell(QuitAccountType.ACCOUNT_SAFE, onSafe: safeAccount),
          QuitCell(
            QuitAccountType.WALLET_ASSETS,
            onSafe: safeWallet,
            onConfirm: onAcceptWallet,
          ),
          QuitCell(
            QuitAccountType.VIRTUAL_ASSETS,
            onSafe: safeBag,
            onConfirm: onAcceptBag,
          ),
          QuitCell(QuitAccountType.INFO_ASSETS, onSafe: safeInfo),
          QuitCell(
            QuitAccountType.ACCOUNT_TITLE,
            onSafe: safeTitle,
            onConfirm: onAcceptTitle,
          ),
          QuitCell(QuitAccountType.PLATFORM_RIGHT, onSafe: safeRight),
          SizedBox(height: 200.h)
        ],
      ),
      bottomNavigationBar: buttonBottom(),
    );
  }

  Widget buttonBottom() {
    return Column(children: [
      FProAccept([ProtocolType.PRO_QUIT], onAccept: onAccept),
      FcationButton("申请注销",
          isActivate: true, radius: 90.w, onPress: onTapApply),
      SizedBox(
        height: Platform.isIOS ? ScreenUtil().bottomBarHeight : 0,
      ),
    ], mainAxisSize: MainAxisSize.min);
  }

  void onAccept(bool value) => hasAccept = value;

  void onAcceptWallet(bool value) => acceptWallet = value;

  void onAcceptBag(bool value) => acceptBag = value;

  void onAcceptTitle(bool value) => acceptTitle = value;

  // issSafe对应一个月内没有注销过   wallet代表金币数量   hasRank代表不是主播不是工会会长
  void requestQuiteCondition() {
    DioUtils().asyncHttpRequest(
      NetConstants.quiteCondition,
      onSuccess: onQuiteCondition,
    );
  }

  void onQuiteCondition(response) {
    safeAccount = response?["issSafe"] ?? true;
    double gold = response?["wallet"] ?? 0;
    safeWallet = gold == 0;
    safeRight = response?["hasRank"] ?? true;
    if (!mounted) return;
    setState(() {});
  }

  void requestBagList() {
    DioUtils().asyncHttpRequest(
      NetConstants.getBag,
      loading: false,
      params: {"needDetail": 0},
      onSuccess: updateBagList,
    );
  }

  void updateBagList(response) {
    List bags = response?["gifts"] ?? [];
    if (bags.isNotEmpty) {
      safeBag = false;
    }
    if (!mounted) return;
    setState(() {});
  }

  void requestSelfInfo() {
    DioUtils().asyncHttpRequest(
      NetConstants.getUserInfo,
      method: DioMethod.POST,
      params: {"userId": ""},
      onSuccess: getUserInfoSuccess,
    );
  }

  void getUserInfoSuccess(response) async {
    if (response == null) return;
    UserConstants.userginInfo = LoginUserModel.fromJson(response);
    // if (UserConstants.userInfo!.avatars.isNotEmpty) {
    //   safeBag = false;
    // }
    // if (UserConstants.userInfo!.levelInfo.peerageLv > 0) {
    //   safeTitle = false;
    // }
    if (!mounted) return;
    setState(() {});
  }

  void onTapApply() {
    if (!hasAccept) return WidgetUtils().showToast("请先阅读并同意协议");
    if (!safeAccount) return WidgetUtils().showToast("该手机号一个月内已经注销过一次了哦");
    if (!safeWallet && !acceptWallet)
      return WidgetUtils().showToast("请确认您的账号内有剩余金币或收益");
    if (!safeBag && !acceptBag)
      return WidgetUtils().showToast("请确认您的背包内还有礼物或还有未过期的装扮");
    if (!safeTitle && !acceptTitle)
      return WidgetUtils().showToast("请确认您还有购买的未过期的爵位");
    if (!safeRight) return WidgetUtils().showToast("请先解除平台内的主播，厅主或会长身份");
    WidgetUtils().pushPage(QuitPage2());
  }
}
