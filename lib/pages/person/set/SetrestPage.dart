import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:permission_handler/permission_handler.dart';

class SetrestPage extends StatefulWidget {
  @override
  _SetrestPageState createState() => _SetrestPageState();
}

class _SetrestPageState extends State<SetrestPage> with WidgetsBindingObserver {
  bool openPush = false;

  @override
  void initState() {
    super.initState();

  }

  @override

  @override
  void deactivate() {
    WidgetsBinding.instance.removeObserver(this);
    super.deactivate();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFF5F6F7),
      appBar: const CommonAppBar(
        title: "个性化选项",
        textColor: Colors.black,
        elevation: 1,
        backgroundColor: Color(0xFFF5F6F7),
      ),
      body: ListView(
        padding: EdgeInsets.fromLTRB(30.w, 40.h, 30.w, 0),
        children: [
          FContainer(
            color: Colors.white,
            height: 680.h,
            radius: BorderRadius.circular(20.h),
            padding: EdgeInsets.fromLTRB(15.w, 30.h, 15.w, 0),
            child:Column(
              children: [
                Row(children: [
                  FText("个性化推荐:", size: 30.sp, color: const Color(0xFF222222)),
                ],),
                Row(children: [
                  Expanded(
                    child: FText("为保障您的使用体验，向您推荐您感兴趣的房间、用户和动态内容，在获得您的授权后，我们可能会收集您的性别、年龄以及您的客户端的信息，包括手机型号、操作系统类型、操作系统版本、设备号、以及用户在APP内的历史行为，包括历史进入、停留、关注、加好友、发消息、送礼物等统计数据分析以形成用户画像，然后进行个性化内容推荐，当我们服务提供商与我们联合为您提供服务时，服务提供商可能会将其收集的您的个人信息与我们共享，用于综合统计并通过算法做特征与偏好的分析，向您进行推荐、展示或推送您可能感兴趣的内容或服务。",
                        maxLines: 100,
                        size: 28.sp,
                        color: Colors.black.withOpacity(0.3)),
                  ),
                ]),
                SizedBox(height: 20.h),
                Expanded(
                  child: FText("请注意:我们未针对您个人特征形成用户标签，更不会设置歧视性标签用语。如果您对我们推荐的内容不感兴趣，您可以通过以下按钮关闭个性化推荐服务。",
                      maxLines: 100,
                      size: 28.sp,
                      color: Colors.black.withOpacity(0.3)),
                ),
              ],
            ),
          ),
          SizedBox(height: 20.h),
          FContainer(
            color: Colors.white,
            height: 100.h,
            radius: BorderRadius.circular(20.h),
            child: SizedBox(
              height: 60.w,
              child: Row(children: [
                SizedBox(width: 20.h),
                FText("个性化推荐", size: 30.sp, color: const Color(0xFF222222)),
                const Spacer(),
                FBtnOpen(
                  onTapBroadcast,
                  initOpen: UserConstants.openBsts,
                ),
                SizedBox(width: 20.h),
              ]),
            ),
          ),

        ],
      ),
    );
  }
  Future<void> onTapBroadcast() async {
    UserConstants.openBsts = !UserConstants.openBsts;
    await UserConstants.setNoticeSet();
    if (!mounted) return;
    setState(() {});

  }

}
