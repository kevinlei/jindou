import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class AuthContent extends StatefulWidget {
  @override
  _AuthContentState createState() => _AuthContentState();
}

class _AuthContentState extends State<AuthContent> {
  bool hasAuth = false;

  @override
  void initState() {
    super.initState();
    getAuthStatus();
  }

  @override
  Widget build(BuildContext context) {
    Widget suffix = FText(
      hasAuth ? '已认证' : "未认证",
      color: Color(0xFF999999),
    );

    return SetListItem("实名认证", suffix: suffix, onPress: onTapAuth);
  }

  void getAuthStatus() {
    DioUtils().asyncHttpRequest(NetConstants.authStatus,
        method: DioMethod.GET, onSuccess: onAuthStatus);
  }

  void onAuthStatus(response) {
    if (response == null) return;
    int? status = response["audit_status"];
    debugPrint(
        '当前实名状态$status,${AuthStatus.AUTO_S.index},${AuthStatus.MANUAL_S.index}');
    if (status == 1) {
      hasAuth = true;
      if (!mounted) return;
      setState(() {});
    }
  }

  void onTapAuth() async {
    await WidgetUtils().pushPage(AuthPage1());
    getAuthStatus();
  }
}
