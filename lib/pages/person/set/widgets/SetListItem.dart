import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class SetListItem extends StatelessWidget {
  final String title;
  final Function()? onPress;
  final Widget? suffix;
  final bool? showArr;
  const SetListItem(this.title,
      {Key? key, this.onPress, this.suffix, this.showArr = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Widget> children = [
      Expanded(child: FText(title,color: Colors.black,size: 30.sp,)),
      if (suffix != null) suffix!,
      SizedBox(width: 10.w),
      if (showArr == true)
        Icon(Icons.arrow_forward_ios, size: 30.w, color: Colors.grey)
    ];
    return InkWell(
      onTap: onPress,
      child: FContainer(
        height: 96.w,
        padding: EdgeInsets.fromLTRB(25.w, 0, 25.w, 0),
        child: Row(children: children),
      ),
    );
  }
}
