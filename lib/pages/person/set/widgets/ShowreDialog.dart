import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class ShowreDialog extends StatefulWidget {
  const ShowreDialog({
    Key? key,
    required force,
  }) : super(key: key);

  @override
  _ShowreDialogState createState() => _ShowreDialogState();
}

class _ShowreDialogState extends State<ShowreDialog> {
  bool resmot = false;

  @override
  Widget build(BuildContext context) {
    return FContainer(
      align: Alignment.center,
      child: FContainer(
        padding: EdgeInsets.only(left: 15.w, right: 15.w, top: 20.w),
        width: 600.w,
        height: 480.w,
        color: Colors.white,
        radius: BorderRadius.circular(20.w),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Spacer(),
                GestureDetector(
                  onTap: _onTap,
                  child: LoadAssetImage("person/iponesX", width: 50.w),
                ),
              ],
            ),
            FText(
              "关闭耗电限制",
              color: Colors.black,
              size: 32.sp,
              weight: FontWeight.w600,
            ),
            FContainer(
              padding: EdgeInsets.only(left: 30.w, right: 15.w, top: 20.w),
              child:
                  Row(crossAxisAlignment: CrossAxisAlignment.center, children: [
                Expanded(
                  child: FText("为防止后台运行时被系统截杀，您需要完成以下操作：",
                      maxLines: 10,
                      size: 32.sp,
                      color: Colors.black.withOpacity(0.3)),
                )
              ]),
            ),
            SizedBox(height: 30.w),
            buttontop(),
            SizedBox(height: 20.w),
            buttonboms(),
          ],
        ),
      ),
    );
  }

  Widget buttontop() {
    Widget btn = FContainer(
      width: 500.w,
      height: 80.w,
      color: resmot ? Colors.grey : const Color(0xFFFF753F),
      align: Alignment.center,
      radius: BorderRadius.circular(80.w),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(width: 30.w),
          LoadAssetImage("person/shuirut", width: 40.w),
          SizedBox(width: 90.w),
          FText("关闭睡眠模式", size: 30.sp, color: Colors.white),
        ],
      ),
    );
    return GestureDetector(onTap: onTapCancel, child: btn);
  }

  Widget buttonboms() {
    Widget btn = FContainer(
      width: 500.w,
      border: Border.all(color: const Color(0xFFFF753F)),
      height: 80.w,
      align: Alignment.center,
      color: const Color(0xFFFF753F),
      radius: BorderRadius.circular(80.w),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(width: 30.w),
          LoadAssetImage("person/dian", width: 40.w),
          SizedBox(width: 30.w),
          FText("允许金豆派对后台耗电", size: 30.sp, color: Colors.white),
        ],
      ),
    );
    return GestureDetector(onTap: onTapBot, child: btn);
  }

  //关闭按钮
  void _onTap() {
    WidgetUtils().popPage();
  }

  //关闭睡眠模式
  Future<void> onTapCancel() async {
    await WidgetUtils().showFDialog(WillPopScope(
      onWillPop: () => Future.value(false),
      child: ShowationDialog(
          title: "关闭睡眠模式", subotitle: AppConstants().appSleep(), Flag: 1),
    ));
  }

  //语音后台耗电按钮
  Future<void> onTapBot() async {
    await WidgetUtils().showFDialog(WillPopScope(
      onWillPop: () => Future.value(false),
      child: ShowationDialog(
          title: "允许后台运行", subotitle: AppConstants().activation(), Flag: 2),
    ));
  }
}
