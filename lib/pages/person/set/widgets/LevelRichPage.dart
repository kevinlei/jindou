import 'dart:convert';

import 'package:echo/import.dart';
import 'package:flutter/material.dart';

import '../../romhome/model/aubents_model.dart';

class LevelRichPage extends StatelessWidget {


   LevelRichPage(
      {Key? key, this.richLevel, this.charmLevel, required this.isRichPage, required this.memberList})
      : super(key: key);

  final ConfigLevel? richLevel;
  final ConfigLevel? charmLevel;
   final List<AubentsModel> memberList;
   final bool isRichPage;

  @override
  Widget build(BuildContext context) {

    return FContainer(
      image: ImageUtils().getAssetsImage(isRichPage ? "person/bgs1" : "person/bgs2"),
      imageFit: BoxFit.fill,
      imageAlign: Alignment.topCenter,
      child: Container(
          decoration: BoxDecoration(
              color: Color(0xFF292628).withOpacity(0.8),
              gradient: RadialGradient(
                center: Alignment.topRight,
                radius: 1.8,
                colors: [
                  Color(0xFF21767C).withOpacity(0.2),
                  Color(0xFF292628).withOpacity(0.1),
                ],
                stops: [0.2, 0.5],
              )),
          child: SingleChildScrollView(
            padding: EdgeInsets.fromLTRB(25.w, kToolbarHeight + 60.w, 25.w, 0),
            child: Column(children: [
              SizedBox(height: 40.h,),
              levelHeadTop(),
              SizedBox(height: 170.h,),
              BacksTop(),
              RemustTop(),

              RemsTop(),
              // levelTop(),
              SizedBox(height: 27.h,),
              // LoadAssetImage(isRichPage ? "set/rich" : "set/charm"),
            ]),
          )));
  }
   Widget RemsTop() {
     return FContainer(
       child:LoadAssetImage(isRichPage ? "set/bootm1" : "set/bootm2"),
     );

   }
   Widget RemustTop() {
     return FContainer(
       image: ImageUtils().getAssetsImage(isRichPage ? "set/connet1" : "set/connet2"),
       imageFit: BoxFit.fill,
       imageAlign: Alignment.topCenter,
       child: GridView.builder(
         shrinkWrap: true,
         physics: const NeverScrollableScrollPhysics(),
         padding: EdgeInsets.symmetric(horizontal: 32.w),
         gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
           crossAxisCount:3,
           mainAxisExtent: 200.h,
           crossAxisSpacing: 60.w,
           mainAxisSpacing: 10.h,
         ),
         // itemCount: giftList.length > 3 ? (giftList.length - 3) : 0,
         itemCount: memberList.length,
         // itemBuilder: (_, i) => giftItem(giftList[i + 3]),
         itemBuilder: (_, i) => giftItem(i),
       ),
     );
   }
  Widget BacksTop() {
    return FContainer(
      height: 400.h,
        image: ImageUtils().getAssetsImage(isRichPage ? "set/caidin1" : "set/caidin2"),
        imageFit: BoxFit.fill,
        imageAlign: Alignment.topCenter,
      padding: EdgeInsets.fromLTRB(60.w, 40.w, 60.w, 50.w),
        child: Column(
        children: [
          SizedBox(height: 40.h,),
          PoslevelTop(),
          SizedBox(height: 30.h,),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              LoadAssetImage("set/btn1", width: 30.w,),
            FText('专属权益',color: Color(0xFFA1B5FE),),
              LoadAssetImage("set/btn2", width: 30.w,),
            ],
          ),
        ],
      ),

    );

  }
  Widget giftItem(int i) {
    return FContainer(
      color: Colors.transparent,
      radius: BorderRadius.circular(20.w),
      child: Column(children: [
        Expanded(
          child: AspectRatio(
            aspectRatio: 1,
            // child: LoadNetImage(NetConstants.ossPath + gift.picture),
            child: LoadNetImage(NetConstants.ossPath +
                memberList[i].quityIcon.toString(), fit: BoxFit.fill),
          ),

        ),
        FContainer(
          height: 40.h,
          radius: BorderRadius.only(
            bottomLeft: Radius.circular(20.w),
            bottomRight: Radius.circular(20.w),
          ),
          color: Colors.transparent,
          align: Alignment.center,
          // child: FText(gift.name,
          //     size: 20.sp, color: Colors.white.withOpacity(0.8)),

          child:
          FText(memberList[i].quityName.toString(), size: 20.sp, color: Color(0xFFA1B5FE)),
        ),
        FText('${ memberList[i].quityDetail}', size: 20.sp, color: Color(0xFF4DFFFFFF)),
        SizedBox(height: 10.h),
      ]),
    );
  }

  Widget PoslevelTop() {
    String head = UserConstants.userginInfo?.info.headIcon ?? "";
    double value = 0.0;
    double values = 0;
     int level = UserConstants.userginInfo?.gradeInfo.userGrade.wealth.nowgradeexperience ?? 0;
    int exlevel = UserConstants.userginInfo?.gradeInfo.userGrade.wealth.nowneedexperience ?? 0;

    int levelcharm = UserConstants.userginInfo?.gradeInfo.userGrade.charm.nowgradeexperience ?? 0;
    int exlevelcharm = UserConstants.userginInfo?.gradeInfo.userGrade.charm.nowneedexperience ?? 0;

    if (isRichPage) {
      if(exlevel==0){

        value =  1.0;
      }else
      {
        value =  level / exlevel;
        }
    }else{
      if(exlevelcharm==0){
        value =  1.0;
      }else{
        value =  levelcharm / exlevelcharm;

      }
    }

    values=value * 100;

   return FContainer(
        imageFit: BoxFit.cover,
        height: 200.w,
        color:isRichPage ? Color(0xFF9AB0FF):Color(0xFFF19AFF),
        radius: BorderRadius.circular(18.w),
        padding: EdgeInsets.fromLTRB(32.w, 0, 32.w, 0.w),
     child: Column(
       children: [
         SizedBox(height: 20.h),
         Row(children: [

     FContainer(
     width: 60.w,
       height: 60.w,
       radius: BorderRadius.circular(128.w),
       border: Border.all(
           color: const Color(0xFF6F2EA3).withOpacity(0.5), width: 2.w),
       child: ClipRRect(
         borderRadius: BorderRadius.circular(128.w),
         child: LoadNetImage(NetConstants.ossPath + head),
       ),
     ),
           SizedBox(width: 10.h),
           FText(UserConstants.userginInfo?.info.name.toString() ?? "",color:Color(0xFF1A254F) ,size: 28.sp,),
         ],),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            FText('Lv ${isRichPage ? UserConstants.userginInfo?.gradeInfo.userGrade.wealth.grade :UserConstants.userginInfo?.gradeInfo.userGrade.charm.grade}',
              color:isRichPage ? Color(0xCC1A254F):Color(0xCC371A4F),),
           FText('当前经验： ${isRichPage ? UserConstants.userginInfo?.gradeInfo.userGrade.wealth.nowExperience :UserConstants.userginInfo?.gradeInfo.userGrade.charm.nowExperience}',
             color:isRichPage ? Color(0xCC1A254F):Color(0xCC371A4F),),
            FText('Lv ${isRichPage ? UserConstants.userginInfo?.gradeInfo.userGrade.wealth.nextgrade :UserConstants.userginInfo?.gradeInfo.userGrade.charm.nextgrade}',
              color:isRichPage ? Color(0xCC1A254F):Color(0xCC371A4F),),
        ],),
         SizedBox(height: 10.h),
         LinearProgressIndicator(
           valueColor: isRichPage ? AlwaysStoppedAnimation<Color>(Color(0xFF6E8DFE)): AlwaysStoppedAnimation<Color>(Color(0xFFEA6EFE)),
           backgroundColor: isRichPage ? Color(0xFF2F48A5).withOpacity(0.5) : Color(0xFFA22FA5).withOpacity(0.5),
           minHeight: 10.h,
           value: value,
         ),
         SizedBox(height: 20.h),
         Row(
           mainAxisAlignment: MainAxisAlignment.spaceBetween,
           children: [
             FText('升级需要${isRichPage ? UserConstants.userginInfo?.gradeInfo.userGrade.wealth.nowneedexperience :UserConstants.userginInfo?.gradeInfo.userGrade.charm.nowneedexperience }',color: Color(0xCC1A254F),),
             FText( '${values.toInt()}%',color: Color(0xCC1A254F),),
             FText('1金币=1经验',color: Color(0xCC1A254F),),
           ],),
       ],
     ),
   );
  }

  Widget levelHeadTop() {
    String head = UserConstants.userginInfo?.info.headIcon ?? "";
    return FContainer(
      child: SizedBox(
        width: 200.w,
        height: 200.w,
        child: CircleAvatar(
          backgroundColor: Colors.white,
          backgroundImage: ImageUtils()
              .getImageProvider(NetConstants.ossPath + head),
        ),
      ),
    );
  }

  Widget levelTop() {
    Widget image;
    String lead = "";
    double needExp = 0.0, value = 0.0;
    int exp = 0, level = 0;
    if (isRichPage) {
      image = LoadAssetImage("set/rich1", width: 144.w, height: 144.w);
      if (richLevel != null) {
        image = LoadNetImage(NetConstants.ossPath + richLevel!.wealthBadge,
            width: 144.w, height: 144.w);
        level = int.parse(richLevel!.grade);
      }
      lead = "财富";
      String next = ConfigManager().getLevelByLv(level + 1)?.experience ?? "";
      exp = UserConstants.userginInfo?.gradeInfo.userGrade.charm.grade ?? 0;
      needExp = next.isEmpty ? 0.0 : (double.parse(next) - exp);
      value = next.isEmpty ? 0.0 : exp / double.parse(next);
    } else {
      image = LoadAssetImage("set/charm1", width: 144.w, height: 144.w);
      if (charmLevel != null) {
        image = LoadNetImage(NetConstants.ossPath + charmLevel!.charmBadge,
            width: 144.w, height: 144.w);
        level = int.parse(charmLevel!.grade);
      }
      lead = "魅力";
      String next = ConfigManager().getLevelByLv(level + 1)?.experience ?? "";
      exp = UserConstants.userginInfo?.gradeInfo.userGrade.wealth.grade ?? 0;
      needExp = next.isEmpty ? 0.0 : (double.parse(next) - exp);
      value = next.isEmpty ? 0.0 : exp / double.parse(next);
    }

    return FContainer(
      imageFit: BoxFit.cover,
      height: 500.w,
      radius: BorderRadius.circular(18.w),
      padding: EdgeInsets.fromLTRB(32.w, 0, 32.w, 32.w),
      image: ImageUtils().getAssetsImage(isRichPage ? "set/bg":"set/bg1"),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Row(children: [
          Expanded(
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              FText(isRichPage ? "财富等级" : "魅力等级",
                  size: 28.sp, color: Colors.white, weight: FontWeight.w600),
              SizedBox(height: 10.h,),
              FText("${title(level)}$level级",
                  color: Color(0xFFAAAAAA), size: 24.sp)
            ]),
          ),
          image,
        ], mainAxisAlignment: MainAxisAlignment.spaceBetween),
        const Spacer(),
        FText("当前$lead值：$exp", size: 24.sp, color: Color(0xFFAAAAAA)),
        ClipRRect(
          borderRadius: BorderRadius.circular(12.w),
          child: SizedBox(
              height: 12.w,
              child: LinearProgressIndicator(
                value: value,
                backgroundColor: Colors.black.withOpacity(0.25),
                valueColor: AlwaysStoppedAnimation<Color>(Color(0xFFFFD2A8)),
              )),
        ),
        Row(children: [
          FText("升级所需$lead值：$needExp", size: 24.sp, color: Color(0xFF858585)),
          FText("${title(level + 1)}${level >= 60 ? 60 : level + 1}级",
              size: 24.sp, color: Color(0xFF858585)),
        ], mainAxisAlignment: MainAxisAlignment.spaceBetween),
      ]),
    );
  }

  String title(int level) {
    if (level <= 4) {
      return isRichPage ? "荣耀新贵" : "初见锋芒";
    }
    if (level >= 5 && level <= 10) {
      return isRichPage ? "名门望族" : "人气新星";
    }
    if (level >= 11 && level <= 18) {
      return isRichPage ? "豪门世家" : "魅力四射";
    }
    if (level >= 19 && level <= 30) {
      return isRichPage ? "挥金如土" : "国民偶像";
    }
    if (level >= 31 && level <= 44) {
      return isRichPage ? "家财万贯" : "众星捧月";
    }
    if (level >= 45 && level <= 60) {
      return isRichPage ? "富甲天下" : "风华绝代";
    }
    return isRichPage ? "富甲天下" : "风华绝代";
  }


}
