import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:open_settings/open_settings.dart';

class ShowRiltDialog extends StatefulWidget {
  final String title;
  final String subotitle;

  const ShowRiltDialog({
    Key? key,
    required this.title,
    required this.subotitle,
  }) : super(key: key);

  @override
  _ShowRiltDialogState createState() => _ShowRiltDialogState();
}

class _ShowRiltDialogState extends State<ShowRiltDialog> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: FContainer(
        padding: EdgeInsets.symmetric(vertical: 50.w, horizontal: 28.w),
        width: 560.w,
        color: Colors.white,
        radius: BorderRadius.circular(20.w),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            FText(
              widget.title,
              color: Colors.black,
              size: 32.sp,
              weight: FontWeight.w600,
            ),
            SizedBox(height: 40.w),
            FText(
              widget.subotitle,
              height: 1.6,
              maxLines: 10,
              size: 27.sp,
              color: Colors.black,
              align: TextAlign.center,
              wordSpacing: 1.2,
            ),
            SizedBox(height: 50.w),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                FButton(
                  onTap: () => WidgetUtils().popPage(),
                  bgColor: const Color(0xFF999999).withOpacity(0.12),
                  width: 220.w,
                  height: 80.w,
                  align: Alignment.center,
                  child: FText("取消", size: 28.sp, color: Colors.black),
                ),
                buttontop(),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget buttontop() {
    Widget btn = FContainer(
      border: Border.all(color: const Color(0xFFFF753F)),
      width: 220.w,
      height: 80.w,
      color: const Color(0xFFFF753F),
      align: Alignment.center,
      radius: BorderRadius.circular(80.w),
      child: FText("前往开启", size: 28.sp, color: Colors.white),
    );
    return GestureDetector(onTap: onTapCancel, child: btn);
  }

//前往开启按钮
  void onTapCancel() {
    UserConstants.openflash = true;
    AppConstants().setActivation();
    WidgetUtils().popPage();
  }
}
