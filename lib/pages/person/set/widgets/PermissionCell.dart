import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:permission_handler/permission_handler.dart';

class PermissionCell extends StatefulWidget {
  final String main;
  final String sub;
  final Permission permission;
  final ProtocolType type;
  final bool showLine;
  const PermissionCell(this.main, this.sub,
      {Key? key,
      required this.permission,
      required this.type,
      this.showLine = true})
      : super(key: key);

  @override
  _PermissionCellState createState() => _PermissionCellState();
}

class _PermissionCellState extends State<PermissionCell> {
  bool accept = true;

  @override
  void initState() {
    super.initState();
    getPermission();
  }
  @override
  Widget build(BuildContext context) {
    Widget btn = FContainer(
      height: 120.w,
      border: widget.showLine
          ? Border(bottom: BorderSide(color: Color(0xFFEEEEEE)))
          : null,
      child: Row(children: [
        Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              FText(widget.main,color: Color(0xFF222222)),
              SizedBox(height: 5.w),
              FText(widget.sub, color: Color(0xFFBBBBBB), size: 24.sp)
            ]),
        Spacer(),
        buttonCheck()
      ]),
    );
    return GestureDetector(onTap: onTapPro, child: btn);
  }
  Widget buttonCheck() {
    return GestureDetector(
      onTap: onTapCheck,
      child: FContainer(
        width: 160.w,
        height: 100.w,
        child: Row(children: [
          FText(accept ? "已开启" : "未开启", size: 22.sp, color: Color(0xFF999999)),
          Icon(Icons.arrow_forward_ios, color: Color(0xFF999999), size: 30.w)
        ], mainAxisAlignment: MainAxisAlignment.end),
      ),
    );
  }

  void onTapCheck() {
    openAppSettings();
  }

  void onTapPro() {
    WidgetUtils().pushPage(ProtocolPage(type: widget.type));
  }

  void getPermission() async {
    if (widget.permission != Permission.unknown) {
      accept = await widget.permission.isGranted;
      if (!mounted) return;
      setState(() {});
    }
  }
}
