import 'dart:io';

import 'package:echo/import.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

class CacheContent extends StatefulWidget {
  @override
  _CacheContentState createState() => _CacheContentState();
}

class _CacheContentState extends State<CacheContent> {
  String cacheValue = "";

  @override
  void initState() {
    super.initState();
    loadCache();
  }

  @override
  Widget build(BuildContext context) {
    return SetListItem("清理缓存",
        suffix: FText(cacheValue, size: 24.sp, color: Color(0xFF999999)),
        onPress: clearCache);
  }

  void clearCache() async {
    bool? accept = await WidgetUtils().showAlert("确认清理缓存？");
    if (accept == null || !accept) return;
    Directory tempDir = await getTemporaryDirectory();
    _deleteDir(tempDir);
    await loadCache();
  }

  void _deleteDir(FileSystemEntity file) {
    if (file is Directory) {
      final List<FileSystemEntity> children = file.listSync();
      for (final FileSystemEntity child in children) {
        _deleteDir(child);
      }
    } else {
      file.deleteSync(recursive: true);
    }
  }

  Future<void> loadCache() async {
    Directory tempDir = await getTemporaryDirectory();
    double value = await getTotalSizeOfFilesInDir(tempDir);
    cacheValue = renderSize(value);
    if (!mounted) return;
    setState(() {});
  }

  Future<double> getTotalSizeOfFilesInDir(final FileSystemEntity file) async {
    if (file is File) {
      int length = await file.length();
      return double.parse(length.toString());
    }
    if (file is Directory) {
      final List<FileSystemEntity>? children = file.listSync();
      double total = 0;
      if (children != null)
        for (final FileSystemEntity child in children)
          total += await getTotalSizeOfFilesInDir(child);
      return total;
    }
    return 0;
  }

  String renderSize(double? value) {
    if (null == value) {
      return "0.0";
    }
    List<String> unitArr = []
      ..add('B')
      ..add('K')
      ..add('M')
      ..add('G');
    int index = 0;
    while (value! > 1024) {
      index++;
      value = value / 1024;
    }
    String size = value.toStringAsFixed(2);
    return size + unitArr[index];
  }
}
