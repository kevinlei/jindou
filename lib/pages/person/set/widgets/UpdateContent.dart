import 'dart:io';

import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class UpdateContent extends StatefulWidget {
  @override
  _UpdateContentState createState() => _UpdateContentState();
}

class _UpdateContentState extends State<UpdateContent> {
  String serVersion = AppConstants.version;

  bool needUpdate = false;

  int forceUpdate = -1;

  String updateContent = "";

  String updateUrl = "";

  @override
  void initState() {
    super.initState();
    // requestAppVersion();
  }

  @override
  Widget build(BuildContext context) {
    return SetListItem("检查更新",
        suffix: Row(children: [
          if (needUpdate)
            CircleAvatar(radius: 5.r, backgroundColor: Colors.red),
          FText(needUpdate ? "  新版本：$serVersion" : "  当前版本：$serVersion",
              size: 24.sp, color: const Color(0xFF999999))
        ]),
        onPress: requestUpdate);
  }

  void requestAppVersion() {
    DioUtils().asyncHttpRequest(
      NetConstants.versionConfig,
      loading: false,
      method: DioMethod.POST,
      params: {"inc_type": "basic"},
      onSuccess: onGetVersion,
    );
  }

  void onGetVersion(response) {
    serVersion = response["version_num"];
    List locList = AppConstants.version.split('.');
    List serList = serVersion.split('.');

    bool first = int.parse(serList[0]) > int.parse(locList[0]);
    bool next = int.parse(serList[1]) > int.parse(locList[1]) &&
        int.parse(serList[0]) == int.parse(locList[0]);
    bool last = int.parse(serList[2]) > int.parse(locList[2]) &&
        int.parse(serList[1]) == int.parse(locList[1]);

    needUpdate = false;
    if (AppConstants.version == serVersion) {
      needUpdate = false;
    } else {
      // 本地版本小于服务版本
      if (first) {
        needUpdate = true;
      } else if (next) {
        needUpdate = true;
      } else if (last) {
        needUpdate = true;
      }
    }

    forceUpdate = response["is_force"];
    updateContent = response["update_content"];
    updateUrl = response["update_address"];

    if (!mounted) return;
    setState(() {});
  }

  void requestUpdate() {
    if (!needUpdate) return WidgetUtils().showToast("已是最新版本");
    WidgetUtils().showFDialog(
      UpdateDialog(
        force: forceUpdate,
        version: serVersion,
        upload: updateUrl,
        tips: updateContent,
      ),
    );
  }
}
