import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:open_settings/open_settings.dart';

class ShowationDialog extends StatefulWidget {
  final String title;
  final String subotitle;
  //区分睡眠还是后台运行
  final int Flag;

  const ShowationDialog({
    Key? key,
    required this.title,
    required this.subotitle,
    required this.Flag,
  }) : super(key: key);

  @override
  _ShowationDialogState createState() => _ShowationDialogState();
}

class _ShowationDialogState extends State<ShowationDialog> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: FContainer(
        padding: EdgeInsets.symmetric(vertical: 50.w, horizontal: 28.w),
        width: 560.w,
        color: Colors.white,
        radius: BorderRadius.circular(20.w),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            FText(
              widget.title,
              color: Colors.black,
              size: 32.sp,
              weight: FontWeight.w600,
            ),
            SizedBox(height: 40.w),
            FText(
              widget.subotitle,
              height: 1.6,
              maxLines: 10,
              size: 27.sp,
              color: Colors.black,
              align: TextAlign.center,
              wordSpacing: 1.2,
            ),
            SizedBox(height: 50.w),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                buttonboms(),
                buttontop(),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget buttontop() {
    Widget btn = FContainer(
      width: 200.w,
      border: Border.all(color: const Color(0xFFFF753F)),
      height: 80.w,
      color: const Color(0xFFFF753F),
      align: Alignment.center,
      radius: BorderRadius.circular(80.w),
      child: FText("立即开启", size: 30.sp, color: Colors.white),
    );
    return GestureDetector(onTap: onTapCancel, child: btn);
  }

  Widget buttonboms() {
    Widget btn = FContainer(
      width: 200.w,
      height: 80.w,
      color: const Color(0xFF999999).withOpacity(0.12),
      align: Alignment.center,
      radius: BorderRadius.circular(80.w),
      child: FText("取消", size: 30.sp, color: Colors.black),
    );
    return GestureDetector(onTap: onTapBot, child: btn);
  }

  //开启按钮
  Future<void> onTapCancel() async {
    //1是睡眠2是后台运行
    if (widget.Flag == 1) {
      AppConstants().setAppSleep();
    } else {
      AppConstants().setActivation();
    }
    UserConstants.openflash = true;
    WidgetUtils().popPage();
  }

  void onTapBot() {
    WidgetUtils().popPage();
  }
}
