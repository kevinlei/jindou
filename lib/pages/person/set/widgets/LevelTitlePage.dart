import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class LevelTitlePage extends StatefulWidget {
  @override
  _LevelTitlePageState createState() => _LevelTitlePageState();
}

class _LevelTitlePageState extends State<LevelTitlePage> {
  final List<String> titles = ["子爵", "伯爵", "侯爵", "公爵", "王爵", "帝皇"];

  List recordList = [];
  int curPage = 0;

  @override
  void initState() {
    super.initState();
    requestRecord();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: EdgeInsets.fromLTRB(25.w, 0, 25.w, 0),
      child: Column(children: [
        SizedBox(height: 500.h, child: titleTop()),
        titleText2(),
        SizedBox(height: 20.h),
        LoadAssetImage("set/t${curPage + 1}"),
      ]),
    );
  }

  Widget titleTop() {
    String image = '';
    // if (curPage < ConfigManager().titleConfig.length) {
    //   image = NetConstants.ossPath +
    //       ConfigManager().titleConfig[curPage].headPicture;
    // }
    return Stack(
        alignment: Alignment.bottomCenter,
        clipBehavior: Clip.none,
        children: [
          ImageFiltered(
            imageFilter: ImageFilter.blur(sigmaY: 5, sigmaX: 2),
            child: Image.network(image,
                width: MediaQuery.of(context).size.width,
                opacity: AlwaysStoppedAnimation<double>(0.2)),
          ),
          Positioned(
            bottom: 40.h,
            child: Row(children: [
              GestureDetector(
                  child: LoadAssetImage("set/left${curPage + 1}", width: 100.w),
                  onTap: onTapLeft),
              LoadNetImage(image, width: 456.w, height: 308.w),
              GestureDetector(
                  child:
                      LoadAssetImage("set/right${curPage + 1}", width: 100.w),
                  onTap: onTapRight),
            ], mainAxisAlignment: MainAxisAlignment.spaceBetween),
          ),
        ]);
  }

  Widget titleText1() {
    return FContainer(
      width: 130.w,
      height: 40.w,
      radius: BorderRadius.circular(40.w),
      gradientBegin: Alignment.centerLeft,
      gradientEnd: Alignment.centerRight,
      gradient: [Color(0xFFFFF1AE), Color(0xFFE6B86B)],
      align: Alignment.center,
      child: FText(titles[curPage], size: 28.sp, color: Color(0xFF9B742A)),
    );
  }

  Widget titleText2() {
    return Row(children: [
      LoadAssetImage("set/left", width: 22.w, color: Color(0xFFE6CA9F)),
      SizedBox(width: 10.w),
      FText("${titles[curPage]}权益", color: Color(0xFFE6CA9F)),
      SizedBox(width: 10.w),
      LoadAssetImage("set/right", width: 22.w, color: Color(0xFFE6CA9F)),
    ], mainAxisAlignment: MainAxisAlignment.center);
  }

  Widget btnBuy() {
    String text = "立即开通";
    Function()? onTap = onTapOpen;
    int curTitle = recordList.isEmpty ? 0 : recordList.last["peerage"];
    int time = recordList.isEmpty ? 0 : recordList.last["peerageEndTime"];
    int left = DateTime.fromMillisecondsSinceEpoch(time)
        .difference(DateTime.now())
        .inDays;

    if (recordList.isNotEmpty && left > 0) {
      if (curPage + 1 < curTitle) {
        text = "已有更高爵位";
        onTap = null;
      } else if (curPage + 1 == curTitle) {
        text = "剩余$left天立即续费";
      }
    }

    Widget btn = FContainer(
      height: 80.w,
      align: Alignment.center,
      gradientBegin: Alignment.centerLeft,
      gradientEnd: Alignment.centerRight,
      gradient: [Color(0xFFFFF1AE), Color(0xFFE6B86B)],
      margin: EdgeInsets.symmetric(horizontal: 25.w, vertical: 10.w),
      radius: BorderRadius.circular(80.w),
      child: FText(text, size: 32.sp),
    );
    return SafeArea(child: GestureDetector(onTap: onTap, child: btn));
  }

  void onTapLeft() {
    if (curPage <= 0) return;
    curPage -= 1;
    if (!mounted) return;
    setState(() {});
  }

  void onTapRight() {
    if (curPage >= titles.length - 1) return;
    curPage += 1;
    if (!mounted) return;
    setState(() {});
  }

  void onTapOpen() async {
    await WidgetUtils().pushPage(OpenTitle(curPage, recordList));
    requestUserInfo();
    requestRecord();
  }

  void requestUserInfo() {
    DioUtils().asyncHttpRequest(NetConstants.getUserInfo,
        method: DioMethod.GET, params: {"userId": ""}, onSuccess: (response) {
      if (response == null) return;
      UserConstants.userginInfo = LoginUserModel.fromJson(response);
    });
  }

  void requestRecord() {
    DioUtils()
        .asyncHttpRequest(NetConstants.titleRecord, onSuccess: onTitleRecord);
  }

  // {"Peerage":1,"PeerageEndTime":1638338786634}
  void onTitleRecord(response) {
    recordList = response ?? [];
    if (!mounted) return;
    setState(() {});
  }
}
