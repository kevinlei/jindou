import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class QuitCell extends StatefulWidget {
  final QuitAccountType type;
  final bool onSafe;
  final Function(bool)? onConfirm;
  const QuitCell(this.type, {Key? key, required this.onSafe, this.onConfirm})
      : super(key: key);

  @override
  _QuitCellState createState() => _QuitCellState();
}

class _QuitCellState extends State<QuitCell>
    with SingleTickerProviderStateMixin {
  late AnimationController animCtr;
  bool isExpand = false;
  bool onAccept = false;

  @override
  void initState() {
    super.initState();
    animCtr =
        AnimationController(duration: Duration(milliseconds: 200), vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 20.h),
          Row(children: [
            Expanded(child: FText(widget.type.title, weight: FontWeight.bold,color: Colors.black,)),
            LoadAssetImage(widget.onSafe || onAccept ? "person/on2" : "person/off2",
                width: 34.w, height: 34.w)
          ]),
          Padding(
            padding: EdgeInsets.fromLTRB(40.w, 20.h, 30.w, 0),
            child: FText(widget.type.des,
                maxLines: 100, color: Color(0xFFBBBBBB), size: 24.sp),
          ),
          Align(alignment: Alignment.centerRight, child: buttonCheck()),
          AnimatedBuilder(
            animation: animCtr.view,
            builder: (_, __) => !isExpand ? SizedBox() : expandContent(),
          ),
          SizedBox(height: 20.h),
        ]);
  }

  Widget expandContent() {
    switch (widget.type) {
      case QuitAccountType.ACCOUNT_SAFE:
        return FContainer(
          padding: EdgeInsets.symmetric(horizontal: 40.w),
          child: FText("该手机号一个月内已经注销过一次了哦",
              maxLines: 10, color: Colors.redAccent, size: 24.sp),
        );
      case QuitAccountType.WALLET_ASSETS:
        return FContainer(
          padding: EdgeInsets.symmetric(horizontal: 40.w),
          child: Column(children: [
            FText("您的账号内有剩余金币或收益，是否确认舍弃账号内剩余资产注销账号？",
                maxLines: 10, color: Colors.redAccent, size: 24.sp),
            SizedBox(height: 10.w),
            Row(children: [
              Spacer(),
              buttonConfirm(),
              SizedBox(width: 20.w),
              buttonCancel()
            ])
          ]),
        );
      case QuitAccountType.VIRTUAL_ASSETS:
        return FContainer(
          padding: EdgeInsets.symmetric(horizontal: 40.w),
          child: Column(children: [
            FText("若您在平台内购买虚拟资产，未到服务期内将会作废",
                maxLines: 10, color: Colors.redAccent, size: 24.sp),
            SizedBox(height: 10.w),
            Row(children: [
              Spacer(),
              buttonConfirm(),
              SizedBox(width: 20.w),
              buttonCancel()
            ])
          ]),
        );
      case QuitAccountType.INFO_ASSETS:
        return FContainer(
          padding: EdgeInsets.symmetric(horizontal: 40.w),
          child: FText("您在平台内创作了大量信息资产，是否确认舍弃？",
              maxLines: 10, color: Colors.redAccent, size: 24.sp),
        );
      case QuitAccountType.ACCOUNT_TITLE:
        return FContainer(
          padding: EdgeInsets.symmetric(horizontal: 40.w),
          child: Column(children: [
            FText("购买的爵位以及不同爵位对应的权益将作废",
                maxLines: 10, color: Colors.redAccent, size: 24.sp),
            SizedBox(height: 10.w),
            Row(children: [
              Spacer(),
              buttonConfirm(),
              SizedBox(width: 20.w),
              buttonCancel()
            ])
          ]),
        );
      case QuitAccountType.PLATFORM_RIGHT:
        return FContainer(
          padding: EdgeInsets.symmetric(horizontal: 40.w),
          child: FText("系统发现您在本平台加入的有公会哦，是否确认退出公会注销账号",
              maxLines: 10, color: Colors.redAccent, size: 24.sp),
        );
    }
  }

  @override
  void dispose() {
    animCtr.dispose();
    super.dispose();
  }

  Widget buttonCheck() {
    Widget btn = FContainer(
      width: 120.w,
      height: 60.w,
      align: Alignment.center,
      child: Row(children: [
        FText("查看详情", size: 20.sp, color: Color(0xFF666666)),
        Icon(isExpand ? Icons.keyboard_arrow_down : Icons.keyboard_arrow_right,
            color: Color(0xFF666666), size: 30.w)
      ]),
    );
    return Visibility(
        visible: !widget.onSafe,
        child: GestureDetector(onTap: onTapCheck, child: btn));
  }

  Widget buttonConfirm() {
    Widget btn = FContainer(
      width: 120.w,
      align: Alignment.center,
      height: 60.w,
      radius: BorderRadius.circular(10.w),
      color: Color(0xFFBBBBBB),
      child: FText("确认", size: 28.sp),
    );
    return GestureDetector(onTap: onTapConfirm, child: btn);
  }

  Widget buttonCancel() {
    Widget btn = FContainer(
      width: 120.w,
      align: Alignment.center,
      height: 60.w,
      radius: BorderRadius.circular(10.w),
      color: Colors.blueAccent,
      child: FText("再想想", size: 28.sp),
    );
    return GestureDetector(onTap: onTapCancel, child: btn);
  }

  void onTapConfirm() {
    onAccept = true;
    if (widget.onConfirm != null) {
      widget.onConfirm!(true);
    }
    isExpand = false;
    animCtr.reverse();
    if (!mounted) return;
    setState(() {});
  }

  void onTapCancel() {
    onAccept = false;
    if (widget.onConfirm != null) {
      widget.onConfirm!(false);
    }
    isExpand = false;
    animCtr.reverse();
    if (!mounted) return;
    setState(() {});
  }

  void onTapCheck() {
    isExpand = !isExpand;
    if (isExpand) {
      animCtr.forward();
    } else {
      animCtr.reverse();
    }
  }
}
