import 'dart:io';
import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:url_launcher/url_launcher.dart';

class UpdateDialog extends StatefulWidget {
  final int force;
  final String version;
  final String upload;
  final String tips;

  const UpdateDialog(
      {Key? key,
      required this.force,
      required this.version,
      required this.upload,
      required this.tips})
      : super(key: key);

  @override
  _UpdateDialogState createState() => _UpdateDialogState();
}

class _UpdateDialogState extends State<UpdateDialog> {
  bool isUpdating = false;
  double progress = 0;

  @override
  Widget build(BuildContext context) {
    return FContainer(
      align: Alignment.center,
      child: FContainer(
        width: 540.w,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Stack(
              children: [
                FContainer(
                  margin: EdgeInsets.only(top: 245.w),
                  padding: EdgeInsets.symmetric(horizontal: 20.w),
                  radius: BorderRadius.only(
                      bottomLeft: Radius.circular(40.w),
                      bottomRight: Radius.circular(40.w)),
                  color: Colors.white,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 30.w),
                      Center(
                        child: FText(
                          "发现新版本 ${widget.version}",
                          size: 32.sp,
                          color: Colors.black,
                          weight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(height: 30.w),
                      textContent(),
                      SizedBox(height: 30.w),
                      bottomContent(),
                      SizedBox(height: 50.w),
                    ],
                  ),
                ),
                LoadAssetImage(
                  "home/UpdateDialog",
                  fit: BoxFit.fill,
                  width: double.infinity,
                  height: 250.w,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget textContent() {
    List<String> textList = widget.tips.split("&");
    return FContainer(
      margin: EdgeInsets.symmetric(vertical: 20.w),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: List.generate(
          textList.length,
          (index) => FText(textList[index],
              size: 20.sp, maxLines: 100, color: const Color(0xFF7F7E86)),
        ),
      ),
    );
  }

  Widget bottomContent() {
    return SizedBox(
      height: 90.w,
      child: isUpdating
          ? sliderProgress()
          : Row(
              mainAxisAlignment: Platform.isAndroid
                  ? MainAxisAlignment.spaceEvenly
                  : MainAxisAlignment.center,
              children: [
                if (widget.force == 0) buttonCancel(),
                Visibility(visible: Platform.isAndroid, child: buttonConfirm()),
              ],
            ),
    );
  }

  Widget sliderProgress() {
    return Row(children: [
      FContainer(
        height: 20.w,
        width: 420.w,
        radius: BorderRadius.circular(20.w),
        color: const Color(0xFFCCCCCC),
        align: Alignment.centerLeft,
        child: FContainer(
          height: 20.w,
          width: 420.w * (progress),
          radius: BorderRadius.circular(20.w),
          color: const Color(0xFFF3A6FF),
        ),
      ),
      const Spacer(),
      FText("${(progress * 100).toInt()}%", color: Colors.black, size: 28.sp)
    ]);
  }

  Widget buttonConfirm() {
    Widget btn = FButton(
      onTap: onTapConfirm,
      bgColor: const Color(0xFFFF753F),
      width: 200.w,
      height: 80.w,
      align: Alignment.center,
      child: FText(
        "立即升级",
        size: 30.sp,
      ),
    );
    return btn;
  }

  Widget buttonCancel() {
    Widget btn = FContainer(
      width: 210.w,
      border: Border.all(color: const Color(0xFFFF753F)),
      height: 80.w,
      align: Alignment.center,
      radius: BorderRadius.circular(80.w),
      child: FText("取消", size: 30.sp, color: const Color(0xFFFF753F)),
    );
    return GestureDetector(onTap: onTapCancel, child: btn);
  }

  void onTapConfirm() async {
    if (widget.upload.isEmpty) {
      return WidgetUtils().showToast("更新失败, 请前往微信公众号下载");
    }
    if (Platform.isIOS) {
      bool cans = await canLaunchUrl(Uri.parse(widget.upload));
      if (!cans) return WidgetUtils().showToast("更新失败, 请前往微信公众号下载");
      launchUrl(Uri.parse(widget.upload), mode: LaunchMode.externalApplication);
    } else {
      if (widget.upload.endsWith(".apk")) {
        inAppUpdate();
      } else {
        bool cans = await canLaunchUrl(Uri.parse(widget.upload));
        if (!cans) return WidgetUtils().showToast("更新失败, 请前往微信公众号下载");
        launchUrl(Uri.parse(widget.upload),
            mode: LaunchMode.externalApplication);
      }
    }
  }

  void onTapCancel() {
    WidgetUtils().popPage();
  }

  void inAppUpdate() async {
    bool accept1 =
        await FuncUtils().requestPermission(Permission.requestInstallPackages);
    if (!accept1) return;
    Directory? directory = await getExternalStorageDirectory();
    String path = directory?.path ?? "";
    if (path.isEmpty) return WidgetUtils().showToast("获取路径失败");
    File file = File('$path/echo-app${widget.version}.apk');
    if (!file.existsSync()) {
      file.createSync();
    }
    isUpdating = true;
    if (!mounted) return;
    setState(() {});
    onDownloadApk(file);
  }

  void onDownloadApk(File file) async {
    try {
      Dio dio = Dio();
      await dio.download(widget.upload, file.path,
          options: Options(
            responseType: ResponseType.bytes,
            followRedirects: false,
          ),
          onReceiveProgress: onDownloadPro);
      if (file.path.isEmpty) {
        progress = 0;
        isUpdating = false;
        if (!mounted) return;
        setState(() {});
        return WidgetUtils().showToast("下载失败");
      }
      bool accept2 = await FuncUtils()
          .requestPermission(Permission.requestInstallPackages);
      if (!accept2) return;
      await OpenFile.open(file.path,
          type: "application/vnd.android.package-archive");
    } catch (e) {
      return WidgetUtils().showToast("更新失败, 请前往微信公众号下载");
    }
  }

  void onDownloadPro(int n, int m) {
    progress = double.parse((n / m).toStringAsFixed(2));
    if (!mounted) return;
    setState(() {});
  }
}
