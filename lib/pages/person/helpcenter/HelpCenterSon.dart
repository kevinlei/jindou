import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:flutter_html/flutter_html.dart';

class HelpCenterSon extends StatefulWidget {
  final data;

  const HelpCenterSon({Key? key, required this.data}) : super(key: key);

  @override
  _HelpCenterSonState createState() => _HelpCenterSonState();
}

class _HelpCenterSonState extends State<HelpCenterSon> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF5F6F7),
      appBar: CommonAppBar(
        title: '解决方案',
        textColor: Colors.black,
        elevation: 1,
        backgroundColor: Color(0xFFF5F6F7),
      ),
      body: SingleChildScrollView(
          padding: EdgeInsets.symmetric(horizontal: 23.w),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.only(top: 20.w, bottom: 10.w),
                padding: EdgeInsets.symmetric(vertical: 10.w),
                child: FText(
                  widget.data['article_name'],
                  size: 32.sp,
                  color: Colors.black,
                ),
              ),
              Html(data: widget.data['content'], style: {
                'p': Style(color: Color(0xFFA5A7AE)),
                'span': Style(color: Color(0xFFA5A7AE)),
              }),
              Container(
                padding: EdgeInsets.symmetric(vertical: 20.w),
                child: Row(
                  children: [
                    Expanded(
                      child: Container(
                        color: Colors.black,
                        height: 2.h,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 20.w),
                      child: Row(
                        children: [
                          FText(
                            '问题没有解决？去',
                            size: 28.sp,
                            color: Color(0xFFA5A7AE),
                          ),
                          GestureDetector(
                            onTap: () => WidgetUtils().pushPage(FeedbackPage(
                                type: FeedbackType.APP_PROBLEM_FEEDBACK,
                                userId:UserConstants.userginInfo?.info.userId.toString())),
                            child: FText(
                              '反馈建议',
                              size: 28.sp,
                              color: Color(0xFF008DF5),
                            ),
                          )
                        ],
                      ),
                    ),
                    Expanded(
                      child: Container(
                        color: Colors.black,
                        height: 2.h,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          )),
    );
  }
}
