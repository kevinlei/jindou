import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:echo/import.dart';

import '../set/Preferences.dart';
import 'HelpCenterSon.dart';

import 'package:fluwx/fluwx.dart' as fluwx;

class HelpCenter extends StatefulWidget {
  const HelpCenter({Key? key}) : super(key: key);

  @override
  _HelpCenterState createState() => _HelpCenterState();
}

class _HelpCenterState extends State<HelpCenter> {
  TextEditingController controller = TextEditingController();
  FocusNode focusNode = FocusNode();
  late EasyRefreshController _controller;
  int questPage = 0;
  List helpCenterList = [];
  List helpCenSonList = [];

  @override
  void initState() {
    super.initState();
    _controller = EasyRefreshController();
    gethelpList();
  }

  void gethelpList() async {
    await DioUtils().httpRequest(NetConstants.helpCenter,
        params: {"category_type": 2, "p_category": 10},
        method: DioMethod.GET,
        loading: false, onSuccess: (response) async {
      // var jsonStr = jsonDecode(response["CategoryInfo"]);
      helpCenterList = response["CategoryInfo"] ?? [];
      int index = response["CategoryInfo"][0]['id'] ?? 0;
      questPage = index;
      getHelpSon(index, false);
      setState(() {});
    });
  }

  void getHelpSon(int id, bool state) async {
    await DioUtils().httpRequest(NetConstants.helpCenterSon,
        method: DioMethod.GET,
        params: {
          "category_id": 0,
          "is_sub_category": 1,
          "sub_category_id": id,
        }, onSuccess: (response) async {
      // if(response?['list'].length == 0) return;
      List list = response?['articles'] ?? [];
      if (state) {
        helpCenSonList.add(list);
      } else {
        helpCenSonList = list;
      }
      setState(() {});
    });
  }

  Future<void> callRefresh() async {
    getHelpSon(questPage, false);
  }

  void onTapSend() async {
    String text = controller.text;
    if (text == '') return WidgetUtils().showToast("请输入问题关键词进行搜索!");
    focusNode.unfocus();
    controller.clear();
  }

  void onQuestPage(int index) {
    questPage = index;
    getHelpSon(index, false);
    setState(() {});
  }

  void conCusSer() {}

  void problemJump(String text) {
    switch (text) {
      case '账号绑定':
        // gethelpList();
        WidgetUtils().pushPage(const AccountPage());
        break;
      case '实名认证':
        WidgetUtils().pushPage(AuthPage1());
        break;
      case '隐私设置':
        WidgetUtils().pushPage(Preferences());
        break;
      case '条款协议':
        WidgetUtils().pushPage(const AboutPage());
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: const CommonAppBar(
          title: "帮助中心",
          textColor: Colors.black,
          elevation: 1,
          backgroundColor: Color(0xFFF5F6F7),
        ),
        backgroundColor: const Color(0xFFF5F6F7),
        bottomNavigationBar: botBar(),
        body: Padding(
          padding: EdgeInsets.all(25.w),
          child: Column(
            children: [
              // Row(
              //   children: [
              //     Expanded(child: inputSend()),
              //     SizedBox(width: 10.w),
              //     buttonSend()
              //   ],
              // ),
              helpTerms(),
              questionList(),
              Expanded(
                child: problemDetails(),
              )
            ],
          ),
        ));
  }

  Widget inputSend() {
    InputBorder border = OutlineInputBorder(
        borderRadius: BorderRadius.circular(20.w),
        borderSide: const BorderSide(color: Colors.transparent));

    return TextField(
      maxLines: 1,
      controller: controller,
      focusNode: focusNode,
      style: TextStyle(color: Colors.white, fontSize: 28.sp),
      cursorColor: const Color(0xFFB965FF),
      decoration: InputDecoration(
        fillColor: const Color(0xFF272A41),
        filled: true,
        prefixIconConstraints: BoxConstraints(maxHeight: 60.w, maxWidth: 60.w),
        prefixIcon: Padding(
          child: const LoadAssetImage("square/search"),
          padding: EdgeInsets.only(left: 10.w, right: 10.w),
        ),
        enabledBorder: border,
        focusedBorder: border,
        contentPadding: EdgeInsets.symmetric(vertical: -10.w, horizontal: 20.w),
        hintStyle: TextStyle(color: const Color(0xFF686A7A), fontSize: 28.sp),
        hintText: "请输入问题关键词进行搜索~",
      ),
    );
  }

  Widget buttonSend() {
    Widget btn = FContainer(
      width: 120.w,
      height: 70.w,
      align: Alignment.center,
      radius: BorderRadius.circular(20.w),
      color: const Color(0xFFAF5BFF),
      child: const FText("搜索"),
    );
    return GestureDetector(onTap: onTapSend, child: btn);
  }

  Widget helpTerms() {
    return FContainer(
      margin: EdgeInsets.only(bottom: 25.w),
      padding: EdgeInsets.symmetric(vertical: 25.w, horizontal: 40.w),
      radius: BorderRadius.circular(20.w),
      color: const Color(0xFF505050).withOpacity(0.1),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          helpTermItem("账号绑定", "set/zhhao"),
          helpTermItem("实名认证", "set/shimin"),
          helpTermItem("隐私设置", "set/yinsi"),
          helpTermItem("条款协议", "set/tiaokuan"),
        ],
      ),
    );
  }

  Widget helpTermItem(String text, String img) {
    return GestureDetector(
        onTap: () {
          problemJump(text);
        },
        child: FContainer(
            width: 120.w,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                LoadAssetImage(
                  img,
                  fit: BoxFit.cover,
                  width: 90.w,
                ),
                SizedBox(
                  height: 20.w,
                ),
                FText(
                  text,
                  size: 26.sp,
                  align: TextAlign.center,
                  color: Colors.black,
                )
              ],
            )));
  }

  Widget questionList() {
    return FContainer(
        padding: EdgeInsets.symmetric(horizontal: 25.w),
        radius: BorderRadius.only(
            topLeft: Radius.circular(20.w), topRight: Radius.circular(20.w)),
        height: 90.w,
        color: const Color(0xFF505050).withOpacity(0.1),
        child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: helpCenterList.length,
            itemBuilder: (_, index) {
              int id = helpCenterList[index]['id'] ?? 0;
              return GestureDetector(
                onTap: () => onQuestPage(id),
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 15.w),
                  padding: EdgeInsets.only(top: 25.w),
                  decoration: BoxDecoration(
                      border: questPage == id
                          ? Border(
                              bottom: BorderSide(
                                  width: 3.w, color: const Color(0xFFB965FF)))
                          : null),
                  child: FText(helpCenterList[index]['category_name'],
                      size: 28.sp,
                      align: TextAlign.center,
                      color: questPage == id
                          ? Colors.white
                          : const Color(0xFF686A7A),
                      weight: questPage == id ? FontWeight.bold : null),
                ),
              );
            }));
  }

  Widget problemDetails() {
    return FContainer(
        padding: EdgeInsets.symmetric(horizontal: 25.w),
        margin: EdgeInsets.only(top: 3.w),
        radius: BorderRadius.only(
            bottomLeft: Radius.circular(20.w),
            bottomRight: Radius.circular(20.w)),
        color: const Color(0xFF505050).withOpacity(0.1),
        child: FRefreshLayout(
            onRefresh: callRefresh,
            controller: _controller,
            firstRefresh: false,
            emptyWidget:
                helpCenSonList.length == 0 ? const FEmptyWidget() : null,
            child: ListView.builder(
                itemCount: helpCenSonList.length,
                itemBuilder: (_, index) {
                  String text = helpCenSonList[index]?['article_name'] ?? '';
                  return GestureDetector(
                    onTap: () {
                      WidgetUtils()
                          .pushPage(HelpCenterSon(data: helpCenSonList[index]));
                    },
                    child: Container(
                        padding: EdgeInsets.symmetric(vertical: 25.w),
                        decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                    width: 1.w,
                                    color: const Color(0xFF686A7A)))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                                child: FText(
                              text,
                              size: 28.sp,
                              color: Colors.black,
                            )),
                            LoadAssetImage(
                              'set/right',
                              fit: BoxFit.cover,
                              height: 20.w,
                              color: Colors.black,
                            ),
                          ],
                        )),
                  );
                })));
  }

  Widget botBar() {
    return FContainer(
      color: const Color(0xFF505050).withOpacity(0.1),
      height: 180.w,
      padding: EdgeInsets.only(bottom: 80.w, left: 120.w, right: 120.w),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        // mainAxisAlignment: MainAxisAlignment.center,
        children: [
          GestureDetector(
            onTap: () {
              // fluwx.openWeChatCustomerServiceChat(
              //     corpId: 'wwdef5ce0ec115ae2b',
              //     url: 'https://work.weixin.qq.com/kfid/kfc5e127a02eb7e8381');
              WidgetUtils().pushPage(const BanerWage(
                url:
                    "https://chatbot.weixin.qq.com/webapp/auth/7bZcj7lwQ11RGGNqTtpxdudrkwQBC5",
              ));
            },
            child: Row(
              children: [
                LoadAssetImage(
                  'set/feedback',
                  fit: BoxFit.cover,
                  width: 50.w,
                  color: Colors.black,
                ),
                FText(
                  '人工咨询',
                  size: 30.sp,
                  color: Colors.black,
                )
              ],
            ),
          ),
          Container(color: Colors.black, width: 2.w, height: 30.h),
          GestureDetector(
            onTap: () {
              WidgetUtils().pushPage(FeedbackPage(
                  type: FeedbackType.APP_PROBLEM_FEEDBACK,
                  userId: UserConstants.userginInfo?.info.userId.toString()));
            },
            child: Row(
              children: [
                LoadAssetImage(
                  'set/manualcustomerservice',
                  fit: BoxFit.cover,
                  width: 50.w,
                  color: Colors.black,
                ),
                FText(
                  '意见反馈',
                  size: 30.sp,
                  color: Colors.black,
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    controller.dispose();
    _controller.dispose();
    focusNode.dispose();
    super.dispose();
  }
}
