import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class ChargeType extends StatefulWidget {
  final Function(PayType) onSelect;
  const ChargeType(this.onSelect, {Key? key}) : super(key: key);

  @override
  _ChargeTypeState createState() => _ChargeTypeState();
}

class _ChargeTypeState extends State<ChargeType> {
  List<PayType> typeList = [];
  // List<PayType> typeList = [PayType.PAY_WX, PayType.PAY_ALI, PayType.PAY_SD_KJ];
  PayType payType = PayType.NULL;

  @override
  void initState() {
    super.initState();
    requestChargeType();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
        children:
        List.generate(typeList.length, (index) => payCell(typeList[index])),
        mainAxisSize: MainAxisSize.min);
  }

  Widget payCell(PayType type) {
    Widget btn = FContainer(
      height: 100.w,
      child: Row(children: [
        LoadAssetImage(type.icon, width: 74.w, height: 74.w),
        SizedBox(width: 20.w),
        FText(type.title, size: 28.sp, color: Color(0xFF666666)),
        Spacer(),
        LoadAssetImage(type == payType ? "login/chekbanbtn" : "login/chekbtn",
            width: 30.w, height: 30.w)
      ]),
    );
    return GestureDetector(onTap: () => onTapType(type), child: btn);
  }

  void onTapType(PayType type) {
    if (type == payType) return;
    payType = type;
    widget.onSelect(type);
    if (!mounted) return;
    setState(() {});
  }

  void requestChargeType() {
    DioUtils().asyncHttpRequest(
      NetConstants.recordAudio,
      method: DioMethod.GET,
      loading: false,
      params: {"conf_key": "pay_method"},
      onSuccess: onChargeType,
    );
  }

  void onChargeType(response) {
    response ??= "";
    if (response.isEmpty) return;
    Map charge = json.decode(response);
    typeList.clear();
    if (Platform.isAndroid) {
      if (charge["android"] == null) return;
      if (charge["android"]?["wexin"] == 1) {
        typeList.add(PayType.PAY_WX);
      }
      if (charge["android"]?["ali"] == 1) {
        typeList.add(PayType.PAY_ALI);
      }
      if (charge["android"]?["union"] == 1) {
        typeList.add(PayType.PAY_SD_KJ);
      }
    } else {
      if (charge["ios"] == null) return;
      if (charge["ios"]?["wexin"] == 1) {
        typeList.add(PayType.PAY_WX);
      }
      if (charge["ios"]?["ali"] == 1) {
        typeList.add(PayType.PAY_ALI);
      }
      if (charge["ios"]?["union"] == 1) {
        typeList.add(PayType.PAY_SD_KJ);
      }
    }
    if (!mounted) return;
    setState(() {});
  }
}
