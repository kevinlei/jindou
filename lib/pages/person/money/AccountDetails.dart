import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class AccountDetails extends StatefulWidget {
  final int recordType;

  const AccountDetails({Key? key, required this.recordType}) : super(key: key);
  @override
  State<AccountDetails> createState() => _AccountDetailSetState();
}

class _AccountDetailSetState extends State<AccountDetails>
    with SingleTickerProviderStateMixin {
  late TabController tabController;
  late String timelab;
  DateTime dateS = DateTime.now();
  DateTime dateE = DateTime.now();
  late double expenditurelab = 0.00; //支出
  late double incomelab = 0.00; //收益
  late int year; //年
  late int month; //月
  late int event_type; //选择筛选项
  int curPage = 1;
  List recordList = [];
  late int recordType = 1;

  @override
  void initState() {
    super.initState();
    DateTime dateNow = DateTime.now();
    dateS = dateNow.add(const Duration(days: -1));
    dateE = dateNow;
    timelab = "${DateTime.now().year}年${DateTime.now().month}月";
    year = DateTime.now().year;
    month = DateTime.now().month;
    event_type = 0;
    recordType = widget.recordType;
    tabController =
        TabController(length: 2, vsync: this, initialIndex: recordType - 1);
    // requesRecord();
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: FContainer(
        padding: EdgeInsets.only(left: 30.w, right: 30.w, bottom: 20.w),
        image: ImageUtils().getAssetsImage("public/huibg"),
        imageFit: BoxFit.fill,
        imageAlign: Alignment.topCenter,
        child: SafeArea(
          child: Scaffold(
            backgroundColor: Colors.transparent,
            body: Column(
              children: [
                header(),
                FContainer(
                  padding: EdgeInsets.only(top: 10.w, right: 0.w, left: 15.w),
                  height: 60.w,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      btnwen(),
                      Row(
                        children: [
                          recordType == 1
                              ? FText(
                                  '支出¥${FuncUtils().formatNum(expenditurelab, 2)}',
                                  size: 23.sp,
                                  color: Colors.black,
                                )
                              : FText(
                                  '支出¥${expenditurelab}',
                                  size: 23.sp,
                                  color: Colors.black,
                                ),
                          recordType == 1
                              ? FText(
                                  '收入¥${FuncUtils().formatNum(incomelab, 2)}',
                                  size: 23.sp,
                                  color: Colors.black,
                                )
                              : FText(
                                  '收入¥${incomelab}',
                                  size: 23.sp,
                                  color: Colors.black,
                                ),
                          btnSwitch(),
                        ],
                      ),
                    ],
                  ),
                ),
                const Divider(color: Color(0xFF142222)),
                SizedBox(
                  height: 1280.w,
                  child: TabBarView(
                    physics: const NeverScrollableScrollPhysics(),
                    controller: tabController,
                    children: [
                      FContainer(
                        child: SafeArea(
                            child: Scaffold(
                          backgroundColor: Colors.transparent,
                          body: FRefreshLayout(
                            onRefresh: callRefresh,
                            onLoad: callLoad,
                            emptyWidget: recordList.isEmpty
                                ? const FEmptyWidget()
                                : null,
                            child: ListView.builder(
                              itemBuilder: (_, index) => recordCell(index),
                              itemExtent: 120.w,
                              itemCount: recordList.length,
                            ),
                          ),
                        )),
                      ),
                      FContainer(
                        child: SafeArea(
                          child: Scaffold(
                            backgroundColor: Colors.transparent,
                            body: FRefreshLayout(
                              onRefresh: callRefresh,
                              onLoad: callLoad,
                              emptyWidget: recordList.isEmpty
                                  ? const FEmptyWidget()
                                  : null,
                              child: ListView.builder(
                                itemBuilder: (_, index) => recordCell(index),
                                itemExtent: 120.w,
                                itemCount: recordList.length,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget recordCell(int index) {
    String mainText = "", subText = "", suffix = "", reasonfix = "";

    var fundData = recordList[index];
    if (recordType == 1) {
      String card = fundData["topic"];
      String type = "";
      int paystate = fundData["event_type"] ?? 1;
      Color States = const Color(0xFF222222);
      if (paystate == 4) {
        if (fundData['apply_status'] == 1) {
          type = "申请中...";
          States = const Color(0xFFFBB143);
        } else if (fundData['apply_status'] == 2) {
          type = "审核中...";
          States = const Color(0xFFFBB143);
        } else if (fundData['apply_status'] == 3) {
          type = "正在付款";
          States = const Color(0xFFFBB143);
        } else if (fundData['apply_status'] == 4) {
          type = "付款成功";
          States = const Color(0xFFB0B0B0);
        } else if (fundData['apply_status'] == 5) {
          type = "提现退回";
          States = const Color(0xFFFE8585);
        }
      }
      mainText = card;
      reasonfix = fundData["apply_reason"];

      int time = fundData["create"];
      subText =
          DateTime.fromMillisecondsSinceEpoch(time).toString().substring(0, 19);
      suffix = "${fundData["change"]}";
      int recharge_type = fundData["recharge_type"];

      int event = fundData["event"];
      String serial_number = fundData["serial_number"];

      return MymodelbacksCell(mainText, subText, suffix, type, States, paystate,
          reasonfix, recordType, recharge_type, event, serial_number);
    } else {
      int paystate = fundData["event_type"] ?? 1;
      Color States = const Color(0xFF222222);
      String type = "";
      String card = fundData["topic"];
      mainText = card;
      int time = fundData["create"];
      subText =
          DateTime.fromMillisecondsSinceEpoch(time).toString().substring(0, 19);
      suffix = "${fundData["change"]}";
      int recharge_type = fundData["recharge_type"];

      int event = fundData["event"];
      String serial_number = fundData["serial_number"];

      return MymodelbacksCell(mainText, subText, suffix, type, States, paystate,
          reasonfix, recordType, recharge_type, event, serial_number);
    }
  }

  Future<void> callRefresh() async {
    curPage = 1;
    if (recordType == 1) {
      requestDepositRecord(true);
    } else {
      requestChargeRecord(true);
    }
  }

  Future<void> callLoad() async {
    curPage += 1;
    if (recordType == 1) {
      requestDepositRecord(false);
    } else {
      requestChargeRecord(false);
    }
  }

  void requestDepositRecord(bool isRefresh) {
    DioUtils().asyncHttpRequest(
      NetConstants.gradeRecord,
      loading: false,
      params: {
        "year": year,
        "month": month,
        "page": curPage,
        "change_type": 0,
        "event_type": event_type,
        "event": recordType == 1 ? 3 : 1,
        "page_size": AppConstants.pageSize,
      },
      method: DioMethod.POST,
      onSuccess: isRefresh ? onRefresh : onLoad,
    );
  }

  void requestChargeRecord(bool isRefresh) {
    DioUtils().asyncHttpRequest(
      NetConstants.gradeRecord,
      loading: false,
      params: {
        "year": year,
        "month": month,
        "page": curPage,
        "change_type": 0,
        "event_type": event_type,
        "event": recordType == 1 ? 3 : 1,
        "page_size": AppConstants.pageSize,
      },
      method: DioMethod.POST,
      onSuccess: isRefresh ? onRefresh : onLoad,
    );
  }

  void onRefresh(response) {
    if (recordType == 1) {
      expenditurelab = response['sum_gains']['sum_reduce'] ?? "0";
      incomelab = response['sum_gains']['sum_add'] ?? "0";
    } else {
      expenditurelab = response['sum_gold']['sum_reduce'] ?? "0";
      incomelab = response['sum_gold']['sum_add'] ?? "0";
    }
    recordList = response?["infos"] ?? [];
    if (!mounted) return;
    setState(() {});
  }

  void onLoad(response) {
    recordList.addAll(response?["infos"] ?? []);
    if (!mounted) return;
    setState(() {});
  }

  Widget btnwen() {
    return GestureDetector(
      onTap: onPresswen,
      child: FContainer(
        height: 50.w,
        child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
          FText(
            timelab,
            size: 28.sp,
            weight: FontWeight.w700,
            color: Colors.black,
          ),
          LoadAssetImage("person/contback", width: 40.w, height: 40.w),
        ]),
      ),
    );
  }

  void onPresswen() async {
    DateTime dateS = DateTime.now();
    DateTime? time = await WidgetUtils()
        .showBottomSheet(FTimedayPicker(initTime: dateS, color: Colors.white));
    if (time == null) return;
    timelab = "${time.year}年${time.month}月";
    year = time.year;
    month = time.month;
    callRefresh();
    setState(() {});
  }

  Widget btnSwitch() {
    return GestureDetector(
      onTap: onPressSwitch,
      child: FContainer(
        height: 50.w,
        child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
          LoadAssetImage("person/ranrst", width: 60.w, height: 60.w),
        ]),
      ),
    );
  }

  Future<void> onPressSwitch() async {
    int? packts = await WidgetUtils().showBottomSheet(
      Moneyshow("sss", onQualityInput, recordType),
      color: const Color(0xFFFFFFFF),
    );
    if (packts == null) return;
    event_type = packts;
    // requesRecord();
    callRefresh();
    setState(() {});
  }

  void onQualityInput(int value) async {}

  Widget header() {
    return FContainer(
      padding: EdgeInsets.symmetric(horizontal: 0.w),
      border: Border(bottom: BorderSide(color: Colors.white.withOpacity(0.08))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          IconButton(
            icon: const Icon(
              Icons.arrow_back_ios,
              color: Colors.black,
            ),
            onPressed: onTapModify,
          ),
          headerItem("收益明细", 0),
          headerItem("钱包明细", 1),
          SizedBox(width: 55.w),
        ],
      ),
    );
  }

  void onTapModify() {
    WidgetUtils().popPage();
  }

  Widget headerItem(String text, int index) {
    Widget child = FContainer(
      height: 80.w,
      align: Alignment.center,
      border: Border(
          bottom: BorderSide(
              color: tabController.index == index
                  ? const Color(0xFF222222)
                  : Colors.transparent)),
      child: FText(
        text,
        size: 34.sp,
        color: tabController.index == index
            ? const Color(0xFF222222)
            : const Color(0xFFA5A3A9),
        weight: FontWeight.w700,
      ),
    );
    return GestureDetector(
      onTap: () => switchtab(index),
      child: child,
    );
  }

  void switchtab(int index) {
    tabController.animateTo(index);
    // requesRecord();
    recordType = index + 1;
    event_type = 0;
    setState(() {});
  }
}
