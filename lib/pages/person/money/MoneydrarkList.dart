import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class MyoneydrarkList extends StatefulWidget {
  /// 1：收益明细 2：钱包明细
  final int recordType;
  final int year;
  final int month;
  final int event_type;
  const MyoneydrarkList(this.recordType, {Key? key, required this.year, required this.month, required this.event_type}) : super(key: key);

  @override
  _MyoneydrarkListState createState() => _MyoneydrarkListState();
}

class _MyoneydrarkListState extends State<MyoneydrarkList> {
  int curPage = 1;
  List recordList = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: FContainer(
        child: SafeArea(child: Scaffold(
          backgroundColor: Colors.transparent,
          body:   FRefreshLayout(
            onRefresh: callRefresh,
            onLoad: callLoad,
            emptyWidget: recordList.isEmpty ? FEmptyWidget() : null,
            child: ListView.builder(
              itemBuilder: (_, index) => recordCell(index),
              itemExtent: 120.w,
              itemCount: recordList.length,
            ),
          ),
        )),
      ),
    );
  }

  // PayApplying ApplyStatus = 1 //用户提现审核已通过，付款申请中 --->申请中
  // WithdrawalsApplying =2 //用户提现审核中 ---> 审核中
  // Paying =3 //付款同意,正在打款中 --->正在付款
  // PaySuccess =4 //付款成功 --->付款成功
  // WithdrawalsReBack =5 //付款失败 --->提现退回
  Widget recordCell(int index) {
    String mainText = "", subText = "", suffix = "", reasonfix = "";

    var fundData = recordList[index];
    if (widget.recordType == 1) {
      String card =
      fundData["topic"];
      String type = "...";
      int paystate = fundData["event_type"] ?? 1;
      Color States = const Color(0xFF222222);
       if(paystate == 2){
        if(fundData['apply_status'] == 1){
      type = "申请中...";
      States = const Color(0xFFFBB143);
        }else if( fundData['apply_status'] == 2){
      type = "审核中...";
      States = const Color(0xFFFBB143);
      }else if( fundData['apply_status'] == 3){
      type = "正在付款";
      States = const Color(0xFFFBB143);
        }else if( fundData['apply_status'] == 4){
      type = "付款成功";
      States = const Color(0xFFB0B0B0);
        }else if( fundData['apply_status'] == 5){
      type = "提现退回";
      States = const Color(0xFFFE8585);
        }
  }
      mainText = card;
      reasonfix=fundData["reason"];

      int time = fundData["create"];
      subText =
          DateTime.fromMillisecondsSinceEpoch(time).toString().substring(0, 19);
      suffix = "${fundData["change"]}";
      return MymodelCell(mainText, subText, suffix,type,States,paystate,reasonfix);
    } else {
      mainText = "充值${fundData["change"]}元";
      int time = fundData["create"] * 1000;
      subText =
          DateTime.fromMillisecondsSinceEpoch(time).toString().substring(0, 19);
      suffix = "+${fundData["gold"]}";
      return MymodelCell(mainText, subText, suffix,"",Color(0xFF222222),1,"");
    }

  }

  String getMainText(fundData) {
    return "";
  }

  Future<void> callRefresh() async {
    curPage = 1;
    if (widget.recordType == 1) {
      requestDepositRecord(true);
    } else {
      requestChargeRecord(true);
    }
  }

  Future<void> callLoad() async {
    curPage += 1;
    if (widget.recordType == 1) {
      requestDepositRecord(false);
    } else {
      requestChargeRecord(false);
    }
  }

  void requestDepositRecord(bool isRefresh) {
    DioUtils().asyncHttpRequest(
      NetConstants.gradeRecord,
      params: {
        "year": 2023,
        "month": 9,
        "page": curPage,
        "change_type": 0,
        "event_type":0,
        "event": widget.recordType==1 ? 3 : 1,
        "page_size": AppConstants.pageSize,
      },
      method: DioMethod.POST,
      onSuccess: isRefresh ? onRefresh : onLoad,
    );
  }

  void requestChargeRecord(bool isRefresh) {
    DioUtils().asyncHttpRequest(
      NetConstants.gradeRecord,
      params: {
        "year": 2023,
        "month": 9,
        "page": curPage,
        "change_type": 0,
        "event_type":0,
        "event": widget.recordType==1 ? 3 : 1,
        "page_size": AppConstants.pageSize,
      },

      onSuccess: isRefresh ? onRefresh : onLoad,
    );
  }

  void onRefresh(response) {
    recordList = response?["infos"] ?? [];
    if (!mounted) return;
    setState(() {});
  }

  void onLoad(response) {
    recordList.addAll(response?["infos"] ?? []);
    if (!mounted) return;
    setState(() {});
  }
}
