import 'package:echo/import.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

class Myreflect extends StatefulWidget {
  final dynamic data;
  const Myreflect(this.data, {Key? key}) : super(key: key);

  @override
  State<Myreflect> createState() => _MyreflectState();
}

class _MyreflectState extends State<Myreflect> with WidgetsBindingObserver {
  TextEditingController controller = TextEditingController();
  LoginProvider provider = LoginProvider();
  ScrollController scroll = ScrollController();
  DepositType depositType = DepositType.NULL;
  bool hasPwd = false;
  bool accept = false;
  // 提现方式
  List<String> payType = [];

  // 提现手续费 [银行卡, 支付宝]
  List<int> payCommission = [0, 0];
  // 用户输入提现值
  double prices = 0;

  // 用户输入计算提现额度
  double withdrawalLimit = 0;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);

    requestBindList();
  }

  void onAcceptSelect(bool accept) => this.accept = accept;
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => provider,
        builder: (_, __) {
          return Scaffold(
            backgroundColor: const Color(0xFF0B0017),
            body: FContainer(
              padding: EdgeInsets.only(left: 30.w, right: 30.w, bottom: 20.w),
              image: ImageUtils().getAssetsImage("public/huibg"),
              imageFit: BoxFit.fill,
              imageAlign: Alignment.topCenter,
              child: SafeArea(
                child: loginContent(),
              ),
            ),
          );
        });
  }

  Future<void> callRefresh() async {
    // provider.callRefresh();
    // homeProvider.requestGift();
  }
  Widget loginContent() {
    return Column(
      children: [
        setContent(),
        const Spacer(),
        depositButton(),
      ],
    );
  }

  Widget depositButton() {
    return FContainer(
      height: 250.h,
      color: const Color(0xFFF5F6F7),
      padding: EdgeInsets.only(bottom: ScreenUtil().bottomBarHeight),
      child: Column(
        children: [
          FcationButton("立即提现",
              isActivate: true, radius: 90.w, onPress: onTapDeposit),
        ],
      ),
    );
  }

  //提现按钮
  void onTapDeposit() async {
    FocusManager.instance.primaryFocus?.unfocus();
    if (controller.text.isEmpty) return WidgetUtils().showToast("请输入要提现的金额");
    double total = UserConstants.userginInfo?.goldInfo.gainsable ?? 0;
    int input = int.parse(controller.text);
    if (input > total) return WidgetUtils().showToast("金额不足");
    if (input < 1000 || input > 99999990) {
      return WidgetUtils().showToast("提现金额是1000~99999990");
    }
    if (depositType == DepositType.NULL) {
      return WidgetUtils().showToast("请选择提现方式");
    }
    requestDeposit(input);
  }

  void requestDeposit(int gold) async {
    await DioUtils().httpRequest(
      NetConstants.deposit,
      method: DioMethod.POST,
      params: {
        "withdrawals_type": depositType.index,
        "change_value": gold,
        "reason": "用户提现"
      },
      onSuccess: onDepositSuccess,
    );
  }

  void onDepositSuccess(response) {
    WidgetUtils().showToast("提现申请成功，注意查收");
    WidgetUtils().popPage('data');
  }

  Widget setContent() {
    return FContainer(
        child: Column(children: [
      const CardAppBar(title: "提现", actions: []),
      SizedBox(height: 30.h),
      Row(
        children: [
          SizedBox(width: 15.h),
          FText("提现金额",
              size: 36.sp,
              color: const Color(0xFF222222),
              weight: FontWeight.bold),
          Row(
            children: [
              Padding(
                padding: EdgeInsets.only(left: 15.w),
                child: FText(
                  withdrawalText(),
                  size: 24.sp,
                  color: const Color(0xFF999999),
                ),
              ),
            ],
          ),
        ],
      ),
      SizedBox(height: 30.h),
      inputField(),
      SizedBox(height: 5.h),
      Container(
        height: 1,
        margin: EdgeInsets.symmetric(horizontal: 15.w),
        color: Colors.black.withOpacity(0.1),
      ),
      SizedBox(height: 10.h),
      Row(
        children: [
          SizedBox(width: 15.h),
          FText("可提现余额：",
              size: 24.sp,
              color: const Color(0xFF222222).withOpacity(0.5),
              weight: FontWeight.bold),
          FText(
              "${FuncUtils().formatNum(UserConstants.userginInfo?.goldInfo.gainsable, 2)}",
              size: 24.sp,
              color: const Color(0xFFFF753F),
              weight: FontWeight.bold),
        ],
      ),
      SizedBox(height: 30.h),
      Row(
        children: [
          SizedBox(width: 15.h),
          FText("提现方式",
              size: 36.sp,
              color: const Color(0xFF222222),
              weight: FontWeight.bold),
        ],
      ),
      DepositTypeSelect(onTypeSelect, payType),
    ]));
  }

  void onTypeSelect(value) {
    depositType = value;
    calculateHandlingFees(prices);
    withdrawalText();
    withrestText();
  }

  Widget inputField() {
    return FContainer(
      height: 88.w,
      margin: EdgeInsets.symmetric(horizontal: 15.w),
      color: Colors.transparent,
      padding: EdgeInsets.symmetric(horizontal: 0.w),
      child: Row(children: [Expanded(child: inputContent()), cancelButton()]),
    );
  }

  void onTapSelf() {
    double total = UserConstants.userginInfo?.goldInfo.gainsable ?? 0.0;
    if (total < 100 || total > 99999990) {
      return WidgetUtils().showToast("提现金额是100~99999990");
    }
    controller.text = "${total.toInt()}";
  }

  Widget cancelButton() {
    Widget btn = FContainer(
      align: Alignment.center,
      child: Text(withrestText()),
    );
    return GestureDetector(onTap: () {}, child: btn);
  }

  String withrestText() {
    switch (depositType) {
      case DepositType.DEP_BANK:
        return withdrawalLimit != 0 ? "实际到账:¥${(withdrawalLimit / 10)} 元" : "实际到账:¥0.00元";
      case DepositType.DEP_ALI:
        return withdrawalLimit != 0 ? "实际到账:¥${(withdrawalLimit / 10)} 元" : "实际到账:¥0.00元";
      default:
        return "实际到账:¥0.00元";
    }
  }

  String withdrawalText() {
    switch (depositType) {
      case DepositType.DEP_BANK:
        return payCommission[0] > 0 ? "提现将扣除${payCommission[0]}%手续费" : "";
      case DepositType.DEP_ALI:
        return payCommission[1] > 0 ? "提现将扣除${payCommission[1]}%手续费" : "";
      default:
        return "";
    }
  }

  // 计算手续费
  void calculateHandlingFees(double price) {
    double nums = price;
    if (nums != 0) {
      withdrawalLimit = price;
      if (depositType == DepositType.DEP_BANK && payCommission[0] != 0) {
        nums = price * payCommission[0];
        withdrawalLimit = (price * 1000000 - (nums / 100) * 1000000) / 1000000;
      } else if (depositType == DepositType.DEP_ALI && payCommission[1] != 0) {
        nums = price * payCommission[1];
        withdrawalLimit = (price * 1000000 - (nums / 100) * 1000000) / 1000000;
      }
      prices = price;
    } else {
      prices = 0;
      withdrawalLimit = 0;
    }
    setState(() {});
  }

  Widget inputContent() {
    InputBorder border = const UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.transparent));
    TextStyle textStyle = TextStyle(
        color: Colors.black, fontWeight: FontWeight.bold, fontSize: 58.sp);
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 0.w),
      child: TextField(
        keyboardType: TextInputType.number,
        maxLength: 10,
        inputFormatters: [FilteringTextInputFormatter.digitsOnly],
        style: textStyle,
        controller: controller,
        onChanged: (value) {
          double price = value.isNotEmpty ? double.parse(value) : 0;
          calculateHandlingFees(price);
        },
        decoration: InputDecoration(
          enabledBorder: border,
          focusedBorder: border,
          contentPadding: EdgeInsets.zero,
          hintText: "输入提现金额",
          hintStyle: TextStyle(
            color: const Color(0xFFBBBBBB),
            fontSize: 40.sp,
            fontWeight: FontWeight.bold,
          ),
          counterText: "",
          prefixStyle: textStyle,
        ),
      ),
    );
  }

  void requestBindList() async {
    await DioUtils().httpRequest(
      NetConstants.bindStatus,
      params: {"type": 0},
      onSuccess: onGetList,
    );
  }

  // [{"Type":1,"CardNumber":"1234567890","Name":"mm"}]
  void onGetList(response) {
    payType.add("银行卡");
    payType.add("支付宝");
    if (response == null) return;
    if (response['PaymentAccountInfo'] == null) return;
    for (var data in response['PaymentAccountInfo']) {
      if (data["account_type"] == 20) {
        // 银行卡
        payCommission[0] = data["gains_withdrawal_convert"];
      } else if (data["account_type"] == 10) {
        // 支付宝
        payCommission[1] = data["gains_withdrawal_convert"];
      }
    }
    if (!mounted) return;
    setState(() {});
  }

  void onAccept(accept) => provider.hasAccept = accept;

  @override
  void didChangeMetrics() {
    super.didChangeMetrics();

  }

  @override
  void dispose() {
    controller.dispose();
    scroll.dispose();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }
}
