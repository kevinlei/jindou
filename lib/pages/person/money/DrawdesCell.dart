import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class DrawdesCell extends StatelessWidget {
  final String mainText;
  final String subText;
  final String suffix;
  final Color? color;
  const DrawdesCell(this.mainText, this.subText, this.suffix, this.color,{Key? key, })
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(onTap: (){

    }, child: FContainer(
      padding: EdgeInsets.symmetric(horizontal: 20.w),
      border: Border(
        bottom: BorderSide(color: Color(0xFF222222).withOpacity(0.05),width: 2.w),
      ),
      child: Row(
        // mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            AspectRatio(
              aspectRatio: 1,
              child: LoadNetImage(
                  NetConstants.ossPath + subText,
                  fit: BoxFit.fill),
            ),
            SizedBox(width: 20.w),
            FText(
              mainText,
              size: 28.sp,
              color: Color(0xFF222222),
              overflow: TextOverflow.ellipsis,
            ),



            const Spacer(),
            Row(children: [
              LoadAssetImage("person/myGold", width: 35.w),
              FText(suffix, size: 28.sp, color:suffix.startsWith("-") ? Color(0xFF222222) : Color(0xFFFF753F) )
            ],),

          ]),

    ),);

  }
}
