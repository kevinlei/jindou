import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class MyrestList extends StatefulWidget {
  /// 1：提现记录 2：充值记录
  final int recordType;
  const MyrestList(this.recordType, {Key? key}) : super(key: key);

  @override
  _MyrestListState createState() => _MyrestListState();
}

class _MyrestListState extends State<MyrestList> {
  int curPage = 1;
  List recordList = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: FContainer(
        padding: EdgeInsets.only(left: 30.w, right: 30.w, bottom: 20.w),
        image: ImageUtils().getAssetsImage("public/huibg"),
        imageFit: BoxFit.fill,
        imageAlign: Alignment.topCenter,
        child: SafeArea(child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: CommonAppBar(
            title: widget.recordType == 1 ? "提现记录" : "充值记录",
            textColor: Colors.black,
            elevation: 0.0,
            backgroundColor:Colors.transparent,
          ),
            body:   FRefreshLayout(
              onRefresh: callRefresh,
              onLoad: callLoad,
              emptyWidget: recordList.isEmpty ? FEmptyWidget() : null,
              child: ListView.builder(
                itemBuilder: (_, index) => recordCell(index),
                itemExtent: 120.w,
                itemCount: recordList.length,
              ),
            ),
        )),
      ),
    );
  }

  // PayApplying ApplyStatus = 1 //用户提现审核已通过，付款申请中 --->申请中
  // WithdrawalsApplying =2 //用户提现审核中 ---> 审核中
  // Paying =3 //付款同意,正在打款中 --->正在付款
  // PaySuccess =4 //付款成功 --->付款成功
  // WithdrawalsReBack =5 //付款失败 --->提现退回
  Widget recordCell(int index) {
    String mainText = "", subText = "", suffix = "", reasonfix = "";

    var fundData = recordList[index];
    if (widget.recordType == 1) {
      String card =
      fundData["withdrawals_type"] == DepositType.DEP_ALI.index ? "提现-到支付宝" : "提现-到银行卡";
      String type = "...";
      int paystate = fundData["withdrawals_type"] ?? 1;
      Color States = const Color(0xFF222222);
      if(fundData['apply_status'] == 1){
         type = "申请中...";
         States = const Color(0xFFFBB143);
      }else if( fundData['apply_status'] == 2){
        type = "审核中...";
        States = const Color(0xFFFBB143);
      }else if( fundData['apply_status'] == 3){
        type = "正在付款";
        States = const Color(0xFFFBB143);
      }else if( fundData['apply_status'] == 4){
        type = "付款成功";
        States = const Color(0xFFB0B0B0);
      }else if( fundData['apply_status'] == 5){
        type = "提现退回";
        States = const Color(0xFFFE8585);
      }
      mainText = card;
      reasonfix=fundData["reason"];

      int time = fundData["create_time"];
      subText =
          DateTime.fromMillisecondsSinceEpoch(time).toString().substring(0, 19);
      suffix = "${fundData["change_value"]}";
      return MymodelCell(mainText, subText, suffix,type,States,paystate,reasonfix);
    } else {
      mainText = "充值${fundData["money"]}元";
      int time = fundData["payTime"] * 1000;
      subText =
          DateTime.fromMillisecondsSinceEpoch(time).toString().substring(0, 19);
      suffix = "+${fundData["gold"]}";
      return MymodelCell(mainText, subText, suffix,"",Color(0xFF222222),1,"");
    }

  }

  String getMainText(fundData) {
    return "";
  }

  Future<void> callRefresh() async {
    curPage = 1;
    if (widget.recordType == 1) {
      requestDepositRecord(true);
    } else {
      requestChargeRecord(true);
    }
  }

  Future<void> callLoad() async {
    curPage += 1;
    if (widget.recordType == 1) {
      requestDepositRecord(false);
    } else {
      requestChargeRecord(false);
    }
  }

  void requestDepositRecord(bool isRefresh) {
    DioUtils().asyncHttpRequest(
      NetConstants.depositRecord,
      params: {
        "startTime": 0,
        "endTime": 0,
        "page": curPage,
        "status": 0,
        "page_num": AppConstants.pageSize,
      },
      method: DioMethod.POST,
      onSuccess: isRefresh ? onRefresh : onLoad,
    );
  }

  void requestChargeRecord(bool isRefresh) {
    DioUtils().asyncHttpRequest(
      NetConstants.chargeRecord,
      params: {
        "startTime": 0,
        "endTime": 0,
        "page": curPage,
        "page_num": AppConstants.pageSize,
      },
      onSuccess: isRefresh ? onRefresh : onLoad,
    );
  }

  void onRefresh(response) {
    recordList = response?["owner_withdrawals_info"] ?? [];
    if (!mounted) return;
    setState(() {});
  }

  void onLoad(response) {
    recordList.addAll(response?["owner_withdrawals_info"] ?? []);
    if (!mounted) return;
    setState(() {});
  }
}
