import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class ChargeSelect extends StatefulWidget {
  final Function(int) onSelect;
  const ChargeSelect(this.onSelect, {Key? key}) : super(key: key);

  @override
  _ChargeSelectState createState() => _ChargeSelectState();
}

class _ChargeSelectState extends State<ChargeSelect> {
  List chargeList = [];
  int chargeNum = 0;

  @override
  void initState() {
    super.initState();
    requestChargeMap();
  }

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
        crossAxisSpacing: 20.w,
        mainAxisSpacing: 20.w,
        mainAxisExtent: 136.w,
      ),
      itemCount: chargeList.length,
      physics: const NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemBuilder: (_, int index) => chargeCell(
          chargeList[index]['exchange_gold'].toString(),
          chargeList[index]['product_price'].toString(),
          chargeList[index]['id']),
    );
  }

  Widget chargeCell(String key, String value, int back) {
    Color colorBg =
        back == chargeNum ? Color(0x1AFF753F) : Color(0xFF0D222222);
    Color colorBorder =
        back == chargeNum ? Color(0xFFFF753F) : Colors.transparent;

    Widget child =
        Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          FText(
            key,
            size: 44.sp,
            color: const Color(0xFF222222),
          ),
          FText("金豆", size: 20.sp, color: const Color(0xFF666666)),
        ],
      ),
      FText("￥$value元", size: 24.sp, color: const Color(0xFF666666)),
    ]);

    Widget btn = FContainer(
      radius: BorderRadius.circular(20.w),
      color: colorBg,
      border: Border.all(color: colorBorder),
      child: child,
    );
    return GestureDetector(onTap: () => onTapCharge(back), child: btn);
  }

  void requestChargeMap() {
    DioUtils().asyncHttpRequest(
      NetConstants.productAudio,
      loading: false,
      method: DioMethod.POST,
      params: {
        "page": {"page_num": 0, "page_offset": 0}
      },
      onSuccess: onChargeMap,
    );
  }

  void onChargeMap(response) {
    try {
      chargeList = response ?? [];
    } catch (e) {}
    if (!mounted) return;
    setState(() {});
  }

  void onTapCharge(int num) {
    if (chargeNum == num) return;
    chargeNum = num;
    widget.onSelect(num);
    if (!mounted) return;
    setState(() {});
  }
}
