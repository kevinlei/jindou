import 'package:echo/import.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class Mymoney extends StatefulWidget {
  const Mymoney({super.key});

  @override
  State<Mymoney> createState() => _MymoneyState();
}

class _MymoneyState extends State<Mymoney> with WidgetsBindingObserver {
  TextEditingController phoneCtr = TextEditingController();
  LoginProvider provider = LoginProvider();
  ScrollController scroll = ScrollController();

  double gold = 0.00;
  double goldlock = 0.00;
  double gains = 0.00;
  double gainslock = 0.00;
  double gold_able = 0.00;
  double gains_able = 0.00;
  int numk = 0;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    callRefresh();
  }

  @override
  void dispose() {
    phoneCtr.dispose();
    scroll.dispose();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  Future<void> callRefresh() async {
    requestDynamicInfo();
    requestpei();
  }

  void requestpei() {
    DioUtils().asyncHttpRequest(
      NetConstants.recordAudio,
      method: DioMethod.GET,
      loading: false,
      params: {"conf_key": "wxtixian"},
      onSuccess: updatepestInfo,
    );
  }

  void updatepestInfo(response) {
    if (response == null) return;
    // 1.显示2关闭
    numk = int.parse(response.toString());
    if (!mounted) return;
    setState(() {});
  }

  void requestDynamicInfo() {
    DioUtils().asyncHttpRequest(
      NetConstants.gradeUserMoney,
      method: DioMethod.GET,
      loading: false,
      onSuccess: updateInfo,
    );
  }

  void updateInfo(response) {
    if (response == null) return;
    gold = response['gold'];
    goldlock = response['gold_lock'];
    gains = response['gains'];
    gainslock = response['gains_lock'];
    gold_able = response['gold_able'];
    gains_able = response['gains_able'];
    UserConstants.userginInfo?.goldInfo.goldable = response['gold_able'];
    UserConstants.userginInfo?.goldInfo.gainsable = response['gains_able'];
    if (!mounted) return;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => provider,
        builder: (_, __) {
          return Scaffold(
            backgroundColor: const Color(0xFF0B0017),
            body: FContainer(
              padding: EdgeInsets.only(left: 30.w, right: 30.w, bottom: 20.w),
              image: ImageUtils().getAssetsImage("public/huibg"),
              imageFit: BoxFit.fill,
              imageAlign: Alignment.topCenter,
              child: SafeArea(child: loginContent()),
            ),
          );
        });
  }

  Widget loginContent() {
    return FRefreshLayout.custom(
      onRefresh: callRefresh,
      firstRefresh: false,
      headerIndex: 0,
      slivers: [
        SliverToBoxAdapter(child: setContent()),
        SliverToBoxAdapter(
          child: FContainer(
            //个人中心
            margin: EdgeInsets.only(
                left: 15.w, top: 20.w, right: 15.w, bottom: 20.w),
            //我的账户
            child: BannerSwipe(BanbarSwper.ZHU_COM.brand),
          ),
        ),
        SliverToBoxAdapter(child: setyutent()),
      ],
    );
  }

  Widget setContent() {
    return FContainer(
        child: Column(children: [
      CardAppBar(title: "我的账户", actions: [
        GestureDetector(
          onTap: onTapSelf,
          child: Row(
            children: [
              FText("明细",
                  size: 23.sp,
                  color: const Color(0x99222222),
                  weight: FontWeight.bold),
              SizedBox(width: 15.h),
            ],
          ),
        )
      ]),
    ]));
  }

  Widget setyutent() {
    return FContainer(
        child: Column(children: [
      Row(
        children: [
          SizedBox(width: 15.h),
          FText("我的余额",
              size: 36.sp,
              color: const Color(0xFF222222),
              weight: FontWeight.bold),
        ],
      ),
      seInsetsent(),
      Row(
        children: [
          SizedBox(width: 15.h),
          FText("我的收益",
              size: 36.sp,
              color: const Color(0xFF222222),
              weight: FontWeight.bold),
        ],
      ),
      seIncomesent(),
      Visibility(
        visible: numk == 1,
        child: seIndrawalsent(),
      ),
    ]));
  }

  //余额布局
  Widget seInsetsent() {
    return FContainer(
      height: 260.w,
      image: ImageUtils().getAssetsImage("person/income"),
      imageFit: BoxFit.fill,
      padding: EdgeInsets.all(30.w),
      radius: BorderRadius.circular(20.w),
      margin: EdgeInsets.symmetric(horizontal: 15.w, vertical: 20.w),
      color: Colors.transparent,
      child: Column(
        children: [
          Row(
            children: [
              LoadAssetImage("person/moneys", width: 72.w, height: 72.w),
              SizedBox(width: 15.h),
              FText("金豆余额", size: 28.sp, color: const Color(0xFFFFFFFF)),
            ],
          ),
          Expanded(
              child: Row(
            children: [
              Expanded(
                child: Container(),
              ),
              chargeButton(),
            ],
          )),
          GestureDetector(
            onTap: onTapDetail,
            child: Row(children: [
              SizedBox(
                width: 500.w,
                child: Text(
                  "¥${gold}",
                  textScaleFactor: 1.0,
                  style: TextStyle(
                      overflow: TextOverflow.ellipsis,
                      fontSize: 56.sp,
                      color: Colors.white,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ]),
          ),
        ],
      ),
    );
  }

  Widget chargeButton() {
    Widget btn = FContainer(
      width: 150.w,
      height: 70.w,
      color: const Color(0xFFFF753F),
      radius: BorderRadius.circular(60.w),
      align: Alignment.center,
      child: FText("充值", color: Colors.white, size: 28.sp),
    );
    return GestureDetector(onTap: onTaponChargeail, child: btn);
  }

  //我的收益
  Widget seIncomesent() {
    return GestureDetector(
      onTap: onTaponcomeeail,
      child: FContainer(
        height: 260.w,
        image: ImageUtils().getAssetsImage("person/balance"),
        imageFit: BoxFit.fill,
        padding: EdgeInsets.all(30.w),
        radius: BorderRadius.circular(20.w),
        margin: EdgeInsets.symmetric(horizontal: 15.w, vertical: 20.w),
        color: Colors.transparent,
        child: Column(
          children: [
            Row(
              children: [
                LoadAssetImage("person/lifostret", width: 72.w, height: 72.w),
                SizedBox(width: 15.h),
                FText("礼物收益", size: 28.sp, color: const Color(0xFFFFFFFF)),
              ],
            ),
            Expanded(
              child: Row(
                children: [
                  Expanded(
                    child: Container(),
                  ),
                  chargesitsButton(),
                ],
              ),
            ),
            GestureDetector(
              onTap: onTapDetail,
              child: Row(children: [
                Text(
                  "¥${FuncUtils().formatNum(gains_able, 2)}",
                  textScaleFactor: 1.0,
                  style: TextStyle(
                    fontSize: 56.sp,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ]),
            ),
          ],
        ),
      ),
    );
  }

  Widget chargesitsButton() {
    Widget btn = FContainer(
      align: Alignment.centerRight,
      child: LoadAssetImage("person/rightmoney", width: 40.w, height: 40.w),
    );
    return GestureDetector(onTap: onTaponcomeeail, child: btn);
  }

  //微信体现
  Widget seIndrawalsent() {
    return GestureDetector(
      onTap: onTaponcomeeail,
      child: FContainer(
        height: 200.w,
        padding: EdgeInsets.all(30.w),
        radius: BorderRadius.circular(20.w),
        margin: EdgeInsets.symmetric(horizontal: 15.w, vertical: 20.w),
        color: Colors.black.withOpacity(0.1),
        child: Column(
          children: [
            Expanded(
                child: Row(
              children: [
                Row(
                  children: [
                    LoadAssetImage("person/vxli", width: 72.w, height: 72.w),
                    SizedBox(
                      height: 100.w,
                      child: Column(
                        children: [
                          FText("收益提现",
                              size: 36.sp,
                              color: const Color(0xFF222222),
                              align: TextAlign.left,
                              weight: FontWeight.bold),
                          Expanded(
                              child: FText("搜索并关注微信公众号“金豆派对”提现",
                                  align: TextAlign.left,
                                  size: 28.sp,
                                  color: const Color(0xFF222222))),
                        ],
                      ),
                    ),
                  ],
                ),
                Expanded(
                  child: Container(),
                ),
                chargwxButton(),
              ],
            )),
          ],
        ),
      ),
    );
  }

  Widget chargwxButton() {
    Widget btn = FContainer(
      align: Alignment.centerRight,
      child: LoadAssetImage("person/backmoney", width: 40.w, height: 40.w),
    );
    return GestureDetector(onTap: onTaponcomeeail, child: btn);
  }

  //点击充值
  void onTaponChargeail() {
    WidgetUtils().pushPage(ChargePage(), fun: callRefresh);
  }

  //点击余额
  void onTapDetail() {}

  //明细按钮
  void onTapSelf() {
    WidgetUtils().pushPage(const AccountDetails(
      recordType: 2,
    ));
  }

  //点击箭头
  void onTaponcomeeail() {
    WidgetUtils().pushPage(const Myeyncome(), fun: callRefresh);
  }

  //点击箭头
  void onTaponmonecomeeail() {
    WidgetUtils().pushPage(const Myeyncome(), fun: callRefresh);
  }

  void onAccept(accept) => provider.hasAccept = accept;
}
