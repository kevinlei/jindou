import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class DepositTypeSelect extends StatefulWidget {
  final Function(DepositType) onSelect;
  final List<String> payType;

  const DepositTypeSelect(this.onSelect, this.payType, {Key? key})
      : super(key: key);

  @override
  _DepositTypeSelectState createState() => _DepositTypeSelectState();
}

class _DepositTypeSelectState extends State<DepositTypeSelect> {
  DepositType depositType = DepositType.NULL;
  var aliCard, bankCard;

  @override
  void initState() {
    super.initState();
    requestBindList();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // Padding(
          //   padding: EdgeInsets.only(left: 25.w),
          //   child: FText("提现方式", size: 32.sp, weight: FontWeight.bold,color: Colors.black,),
          // ),
          // SizedBox(height: 20.w),
          if (widget.payType.contains("支付宝")) typeCell(DepositType.DEP_ALI),
          if (widget.payType.contains("银行卡")) typeCell(DepositType.DEP_BANK),
        ]);
  }

  Widget typeCell(DepositType type) {
    String image = "person/add", text = "";
    bool visible = false;
    Function()? onTap;
    if (type == DepositType.DEP_ALI) {
      if (aliCard == null) {
        text = "绑定支付宝";
        onTap = () => onTapBind(DepositType.DEP_ALI);
      } else {
        text = aliCard["account_number"];
        onTap = () => onTapType(DepositType.DEP_ALI);
        image = "person/al";
        visible = true;
      }
    } else if (type == DepositType.DEP_BANK) {
      if (bankCard == null) {
        text = "绑定银行卡";
        onTap = () => onTapBind(DepositType.DEP_BANK);
      } else {
        text = bankCard["account_number"];
        onTap = () => onTapType(DepositType.DEP_BANK);
        image = "person/un";
        visible = true;
      }
    }
    Widget btn = FContainer(
      height: 110.w,
      margin: EdgeInsets.symmetric(horizontal: 25.w),
      child: Row(children: [
        LoadAssetImage(image, width: 75.w, height: 75.w),
        SizedBox(width: 10.w),
        Expanded(
            child: FText(text, size: 28.sp, color: const Color(0xFF666666))),
        Visibility(
          visible: visible,
          child: LoadAssetImage(
              depositType == type ? "login/chekbanbtn" : "login/chekbtn",
              width: 30.w),
        )
      ]),
    );

    return GestureDetector(onTap: onTap, child: btn);
  }

  void onTapBind(DepositType bindType) async {
    await WidgetUtils().pushPage(DepositBindPage(bindType: bindType),  fun: requestBindList);
    // requestBindList();

  }

  void onTapType(DepositType type) {
    if (depositType == type) return;
    depositType = type;
    widget.onSelect(type);
    if (!mounted) return;
    setState(() {});
  }

  Future<void> requestBindList() async {
    await DioUtils().httpRequest(
      NetConstants.bindStatus,
      params: {"type": 0},
      onSuccess: onGetList,
    );
    if (!mounted) return;
    setState(() {});
  }


  // [{"Type":1,"CardNumber":"1234567890","Name":"mm"}]
  void onGetList(response) {
    if (response == null) return;
    if (response['PaymentAccountInfo'] == null) return;
    for (var data in response['PaymentAccountInfo']) {
      if (data["account_type"] == DepositType.DEP_ALI.index+9) {
        aliCard = data;
      } else if (data["account_type"] == DepositType.DEP_BANK.index+18) {
        bankCard = data;
      }
    }
    if (!mounted) return;
    setState(() {});
  }
}
