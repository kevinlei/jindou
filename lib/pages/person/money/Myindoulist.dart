import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class Myindoulist extends StatefulWidget {
  final Function(int) onSelect;
  const Myindoulist(this.onSelect, {Key? key}) : super(key: key);

  @override
  _MyindoulistState createState() => _MyindoulistState();
}

class _MyindoulistState extends State<Myindoulist> {
  List chargeList = [];
  int chargeNum = 0;

  @override
  void initState() {
    super.initState();
    requestChargeMap();
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      height: 350.w,
      child: GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          crossAxisSpacing: 20.w,
          mainAxisSpacing: 20.w,
          mainAxisExtent: 100.w,
        ),
        itemCount: chargeList.length,
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemBuilder: (_, int index) =>
            chargeCell(chargeList[index]['gains'], chargeList[index]['gold']),
      ),
    );
  }

  Widget chargeCell(int key, int value) {
    Color colorBg =
        key == chargeNum ? Color(0xFF1AFB63B3) : Color(0xFF0D222222);
    Color colorBorder =
        key == chargeNum ? Color(0xFFFF753F) : Colors.transparent;

    Widget child = Column(children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Row(
            children: [
              LoadAssetImage("person/restprint", width: 35.w, height: 35.w),
              FText(
                "$key",
                size: 24.sp,
                color: Color(0xFFFF9542),
              ),
            ],
          ),
          FText(
            '/',
            color: Color(0xFF222222),
          ),
          Row(
            children: [
              FText("￥$value", size: 24.sp, color: Color(0xFFFF753F)),
              LoadAssetImage("person/moneys", width: 35.w, height: 35.w),
            ],
          ),
        ],
      ),
    ], mainAxisAlignment: MainAxisAlignment.spaceEvenly);

    Widget btn = FContainer(
      radius: BorderRadius.circular(20.w),
      color: colorBg,
      border: Border.all(color: colorBorder),
      child: child,
    );
    return GestureDetector(onTap: () => onTapCharge(key), child: btn);
  }

//兑换金豆数量接口
  void requestChargeMap() {
    DioUtils().asyncHttpRequest(
      NetConstants.recordAudio,
      method: DioMethod.GET,
      params: {"conf_key": "gains_exchange_gold"},
      onSuccess: onChargeMap,
    );
  }

  void onChargeMap(response) {
    chargeList = json.decode(response ?? "");
    if (!mounted) return;
    setState(() {});
  }

  void onTapCharge(int num) {
    if (chargeNum == num) return;
    chargeNum = num;
    widget.onSelect(num);
    if (!mounted) return;
    setState(() {});
  }
}
