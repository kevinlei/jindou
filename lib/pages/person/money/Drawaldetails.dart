import 'package:echo/import.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class Drawaldetals extends StatefulWidget {
  //区分送礼或者收礼 1收礼 2送礼
  final int recordType;
  final int event;
  final int? event_type;
  final String year_month;
  final String serial_number;

  const Drawaldetals({super.key, required this.recordType,required this.event, required this.event_type, required this.year_month, required this.serial_number});
  @override
  State<Drawaldetals> createState() => _DrawaldetalsState();
}

class _DrawaldetalsState extends State<Drawaldetals>{
  int curPage = 1;
  List recordList = [];
  String serial_number = "";//订单号
  int gift_sum_price = 0;//全部礼物总价
  int  gold_change = 0;//金币/收益变化数量
  double gold_chbleange = 0;//收益的带小数点
  int create_time=0;//时间
  List get_gift_user_info=[];

  String username="";
  String userid="";
  String userHead="";


  @override
  void initState() {
    super.initState();
    callRefresh();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
            backgroundColor: const Color(0xFF0B0017),
            body: FContainer(
              padding: EdgeInsets.only(left: 0.w, right: 0.w, bottom: 20.w),
              image: ImageUtils().getAssetsImage("public/huibg"),
              imageFit: BoxFit.fill,
              imageAlign: Alignment.topCenter,
              child: SafeArea(child: loginContent()),
            ),
          );
  }

  Widget loginContent() {
    return Column(
      children: [
        setContent(),
        SizedBox(
          height: 910.w,
          child: FContainer(
            child: SafeArea(child: Scaffold(
              backgroundColor: Colors.transparent,
              body:   FRefreshLayout(
                emptyWidget: recordList.isEmpty ? FEmptyWidget() : null,
                child: ListView.builder(
                  itemBuilder: (_, index) => recordCell(index),
                  itemExtent: 100.w,
                  itemCount: recordList.length,
                ),
              ),
            )),
          ),
        ),

      ],
    );
  }
  Widget recordCell(int index) {
    String mainText = "", subText = "", suffix = "", reasonfix = "";

    var fundData = recordList[index];
      mainText = "${fundData["gift_name"]}(${fundData["gift_price"]}) x${fundData["gift_num"]}个";
      suffix = "${fundData["gift_all_price"]}";
    subText = "${fundData["gift_picture"]}";
      return DrawdesCell(mainText, subText, suffix,Color(0xFF222222));

  }
  Future<void> callRefresh() async {
    curPage = 1;
    if (widget.recordType == 1) {
      requestDepositRecord(true);
    } else {
      requestChargeRecord(true);
    }
  }


  void requestDepositRecord(bool isRefresh) {
    DioUtils().asyncHttpRequest(
      NetConstants.gradeRecordetail,
      method: DioMethod.POST,
      params: {"event": widget.event,"event_type":widget.event_type,"year_month":widget.year_month,"serial_number":widget.serial_number},
      onSuccess: onRefresh ,
    );
  }

  void requestChargeRecord(bool isRefresh) {
    DioUtils().asyncHttpRequest(
      NetConstants.gradeRecordetail,
      method: DioMethod.POST,
      params: {"event": widget.event,"event_type":widget.event_type,"year_month":widget.year_month,"serial_number":widget.serial_number},

      onSuccess: onRefresh ,
    );
  }

  void onRefresh(response) {
    serial_number = response['info']['serial_number'];
    gift_sum_price = response['info']['gift_sum_price'];
    create_time = response['info']['create_time'];
    recordList = response["info"]["gift"] ?? [];

    //1收礼 2送礼
    if(widget.recordType==1){

      gold_chbleange = response['info']['gain_change'];
      username= response["info"]["send_gift_user_info"]["user_name"];
      userid= response["info"]["send_gift_user_info"]["user_id"];
      userHead= response["info"]["send_gift_user_info"]["user_head_image"];
    }else{
      gold_change = response['info']['gold_change'];
      get_gift_user_info = response["info"]["get_gift_user_info"] ?? [];
    }
    if (!mounted) return;
    setState(() {});
  }


  Widget setContent() {
    return FContainer(
        child: Column(
            children: [
          CardAppBar(title: widget.recordType == 1 ? "收礼明细":"送礼明细", actions: [

          ]),
          setyutent(),

        ]));
  }

  Widget setyutent() {
    return FContainer(
        child: Column(children: [
          seInsetsent(),
          FContainer(
            height: 60.w,
            color: const Color(0xFF222222).withOpacity(0.1),
            child: Row(
              children: [
                SizedBox(width: 30.h),
                FText(
                    "${DateTime.fromMillisecondsSinceEpoch(create_time).toString().substring(0, 19)}",
                    size: 23.sp,
                    color: const Color(0x99222222),
                    weight: FontWeight.bold),
              ],
            ),
          ),
        ]));
  }

  //余额布局
  Widget seInsetsent() {
    String head = "";
    String name = "";
    String userId = "";
    //1收礼 2送礼
    if(widget.recordType==1){
      head = userHead;
      name = username;
      userId = userid;
    }else{
      if(get_gift_user_info.length==0){

      }else{
        head = get_gift_user_info[0]["user_head_image"] ?? "";
        name = get_gift_user_info[0]["user_name"] ?? "";
        userId = get_gift_user_info[0]["user_id"] ?? "";
      }

    }

    return FContainer(
      padding: EdgeInsets.only(left: 15.w, right: 15.w,),
      height: 360.w,
      radius: BorderRadius.circular(20.w),
      margin: EdgeInsets.symmetric(horizontal: 15.w, vertical: 20.w),
      color: Colors.transparent,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          FText( widget.recordType == 1 ? "收到用户礼物":"送出礼物", size: 28.sp, color: const Color(0xFF222222)),
          SizedBox(height: 15.h),
          FText( widget.recordType == 1 ? "${gold_chbleange}" : "${gold_change}", size: 50.sp, color: const Color(0xFF222222),weight:FontWeight.bold),
          SizedBox(height: 15.h),
          GestureDetector(
            onTap: (){
              onTapSelf(userId);
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircleAvatar(
                  radius: 50.w,
                  backgroundImage:
                  ImageUtils().getImageProvider(NetConstants.ossPath + head),
                  backgroundColor: Color(0xFFEEEEEE),
                ),

                SizedBox(width: 15.h),
                FText("${name} >", size: 28.sp, color: const Color(0xFF222222).withOpacity(0.6)),
              ],),
          ),
          const Spacer(),
      Row(children: [
        FText(widget.recordType == 1 ? "收礼明细" :"送礼明细", size: 32.sp, color: const Color(0xFF222222),weight: FontWeight.bold,),
        FText("(总价值${gift_sum_price}金币)", size: 28.sp, color: const Color(0xFF222222).withOpacity(0.6)),
      ],),
        ],
      ),
    );
  }

  //跳转个人主页按钮
  void onTapSelf(String user_Id) {
    String userId = user_Id;
    if (userId.isEmpty) return;
    WidgetUtils().pushPage(PersonHomepage(userId: userId));
  }





  @override
  void dispose() {
    super.dispose();
  }
}
