import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class MyretreList extends StatefulWidget {
  final Function(int) onSelect;
  final int recordType;
  const MyretreList(this.onSelect, this.recordType,{Key? key}) : super(key: key);

  @override
  _MyretreListState createState() => _MyretreListState();
}

class _MyretreListState extends State<MyretreList> {
  List chargeList = [];
  int chargeNum = 0;

  @override
  void initState() {
    super.initState();
    requestChargeMap();
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(

      child: GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          crossAxisSpacing: 20.w,
          mainAxisSpacing: 20.w,
          mainAxisExtent: 136.w,
        ),
        itemCount: chargeList.length,
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemBuilder: (_, int index) =>
            chargeCell(chargeList[index]['event_type'], chargeList[index]['content']),
      ),
    );
  }

  Widget chargeCell(int key, String value) {
    Color colorBg = Color(0xFF0D222222);
    Color colorBorder = Colors.transparent;


    Widget child = Column(children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          // Row(
          //   children: [
          //     FText(
          //       "$key",
          //       size: 24.sp,
          //       color: Color(0xFFFF9542),
          //     ),
          //   ],
          // ),
          Row(
            children: [
              FText("$value", size: 24.sp, color: Color(0xFFFF753F)),
            ],
          ),
        ],
      ),
    ], mainAxisAlignment: MainAxisAlignment.spaceEvenly);

    Widget btn = FContainer(
      radius: BorderRadius.circular(20.w),
      color: colorBg,
      border: Border.all(color: colorBorder),
      child: child,
    );
    return GestureDetector(onTap: () => onTapCharge(key), child: btn);
  }


  void requestChargeMap() {
    DioUtils().asyncHttpRequest(
      NetConstants.finance,
      method: DioMethod.GET,
      params: {"event":widget.recordType == 1 ? 3:1},
      onSuccess: onChargeMap,
    );
  }

  void onChargeMap(response) {
    chargeList = response;
    if (!mounted) return;
    setState(() {});
  }

  void onTapCharge(int num) {
    // if (chargeNum == num) return;
    chargeNum = num;
    widget.onSelect(num);
    if (!mounted) return;
    setState(() {});
  }
}
