import 'package:echo/import.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class BankcardDest extends StatefulWidget {
  final int event;
  final int? event_type;
  final String year_month;
  final String serial_number;

  const BankcardDest({super.key, required this.event, required this.event_type, required this.year_month, required this.serial_number});

  @override
  State<BankcardDest> createState() => _BankcardDestState();
}

class _BankcardDestState extends State<BankcardDest>{
  int event = 0;
  int event_type = 0;
  double gain_change=0.0;//收益变动
  double rmb_change =0.0;//实际到账
  String serial_number = "";//订单号
  String create_time = "";//订单时间
  int withdrawals_type=0;
  String zhilaber = "";//支付宝账号
  String state_txt1 = "";
  String state_txt2 = "";
  String state_txt3 = "";
  int time1 = 0;
  int time2 = 0;
  int time3 = 0;
  @override
  void initState() {
    super.initState();
    getdevelop();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFF0B0017),
      body: FContainer(
        padding: EdgeInsets.only(left: 30.w, right: 30.w, bottom: 50.w),
        image: ImageUtils().getAssetsImage("public/huibg"),
        imageFit: BoxFit.fill,
        imageAlign: Alignment.topCenter,
        child: SafeArea(child: loginContent()),
      ),
    );
  }

  Widget loginContent() {
    return Column(
      children: [
        setContent(),
        const Spacer(),
        FText("有任何疑问请联系软件官方客服进行处理", size: 28.sp, color: const Color(0xFF222222).withOpacity(0.6)),

      ],
    );
  }

  void getdevelop() {
    DioUtils().asyncHttpRequest(NetConstants.gradeRecordetail,
        method: DioMethod.POST,
        params: {"event": widget.event,"event_type":widget.event_type,"year_month":widget.year_month,"serial_number":widget.serial_number}, onSuccess: (res) {
          if (res == null) return;
          serial_number = res["info"]["serial_number"];
          gain_change = res["info"]["gain_change"];
          rmb_change = res["info"]["rmb_change"];
          withdrawals_type = res["info"]["withdrawals_type"];
          int time = res["info"]["create_time"];
          create_time =
              DateTime.fromMillisecondsSinceEpoch(time).toString().substring(0, 19);
          zhilaber = res["info"]["withdrawals_account"];
          List relist = res["info"]["apply_status_list"];
          state_txt1= relist[0]['state_txt'];
          state_txt2= relist[1]['state_txt'];
          state_txt3= relist[2]['state_txt'];
          time1 = relist[0]['time'];
          time2 = relist[1]['time'];
          time3 = relist[2]['time'];
          // debugPrint(app_name);
          // debugPrint(app_url);
          if (!mounted) return;
          setState(() {});
        });
  }


  Widget setContent() {
    return FContainer(
        child: Column(
            children: [
              CardAppBar(title: "银行卡提现", actions: [
              ]),
              setyutent(),

            ]));
  }

  Widget setyutent() {
    return FContainer(
        child: Column(children: [
          seInsetsent(),
          FContainer(
            height: 2.w,
            color: const Color(0xFF222222).withOpacity(0.1),
          ),
          seBlacksent(),

        ]));
  }

  //余额布局
  Widget seInsetsent() {
    return FContainer(
      padding: EdgeInsets.only(left: 15.w, right: 15.w,),
      height: 300.w,
      radius: BorderRadius.circular(20.w),
      margin: EdgeInsets.symmetric(horizontal: 15.w, vertical: 20.w),
      color: Colors.transparent,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          LoadAssetImage("person/fcks", width: 120.w, height: 120.w),
          SizedBox(height: 15.h),
          FText("${gain_change}", size: 50.sp, color: const Color(0xFF222222),weight:FontWeight.bold),
          SizedBox(height: 15.h),
          FText("实际到账${rmb_change}元", size: 28.sp, color: const Color(0xFF222222).withOpacity(0.6)),
          const Spacer(),
        ],
      ),
    );
  }
  //余额布局
  Widget seBlacksent() {
    return FContainer(
      padding: EdgeInsets.only(left: 15.w, right: 15.w,),
      height: 600.w,
      radius: BorderRadius.circular(20.w),
      margin: EdgeInsets.symmetric(horizontal: 15.w, vertical: 20.w),
      color: Colors.transparent,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
         Row(children: [
           FText("订单号  ", size: 28.sp, color: const Color(0xFF222222).withOpacity(0.6)),
           SizedBox(width: 40.h),
           Expanded(child: FText(serial_number, size: 28.sp, color: const Color(0xFF222222),maxLines: 2,),)
         ],),
          SizedBox(height: 20.h),
          Row(children: [
            FText("创建时间", size: 28.sp, color: const Color(0xFF222222).withOpacity(0.6)),
            SizedBox(width: 30.h),
            FText("${create_time}", size: 28.sp, color: const Color(0xFF222222),maxLines: 2,),
          ],),
          SizedBox(height: 20.h),
          Row(children: [
            FText("提现到  ", size: 28.sp, color: const Color(0xFF222222).withOpacity(0.6)),
            SizedBox(width: 40.h),
            FText(withdrawals_type == 1 ? "支付宝账号 (${zhilaber})":"银行卡账号 (${zhilaber})", size: 28.sp, color: const Color(0xFF222222),maxLines: 2,),
          ],),
          SizedBox(height: 20.h),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              FText("处理进度", size: 28.sp, color: const Color(0xFF222222).withOpacity(0.6)),
              SizedBox(width: 30.h),
              Column(
                children: [
                  SizedBox(height: 5.h),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                    LoadAssetImage(time1>0 ? "person/elliptic":"person/huielliptic", width: 30.w, height: 30.w),
                  ],),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      LoadAssetImage(time2>0 ? "person/straight":"person/huistraight", height: 100.w),
                    ],),
                  Row(
                    children: [
                      LoadAssetImage(time2>0 ? "person/elliptic":"person/huielliptic", height: 30.w),
                    ],),
                  LoadAssetImage(time3>0 ? "person/straight":"person/huistraight", height: 100.w),
                  Row(
                    children: [
                      LoadAssetImage(time3>0 ? "person/elliptic":"person/huielliptic", height: 30.w),
                    ],),

                ],
              ),
              SizedBox(width: 20.h),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                FText("${state_txt1}", size: 28.sp, color: const Color(0xFF222222),weight: FontWeight.bold,),
                FText(time1>0 ?"${DateTime.fromMillisecondsSinceEpoch(time1).toString().substring(0, 19)}":"", size: 28.sp, color: const Color(0xFF222222).withOpacity(0.6)),
                  SizedBox(height: 58.h),
                  FText("${state_txt2}", size: 28.sp, color: const Color(0xFF222222),weight: FontWeight.bold,),
                  FText(time2>0 ?"${DateTime.fromMillisecondsSinceEpoch(time2).toString().substring(0, 19)}":"", size: 28.sp, color: const Color(0xFF222222).withOpacity(0.6)),
                  SizedBox(height: 58.h),
                  FText("${state_txt3}", size: 28.sp, color: const Color(0xFF222222),weight: FontWeight.bold,),
                  FText(time3>0 ?"${DateTime.fromMillisecondsSinceEpoch(time3).toString().substring(0, 19)}":"", size: 28.sp, color: const Color(0xFF222222).withOpacity(0.6)),
              ],),


            ],
          ),
          const Spacer(),
        ],
      ),
    );

  }





  @override
  void dispose() {
    super.dispose();
  }
}
