import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class MymodelbacksCell extends StatelessWidget {
  final String mainText;
  final String subText;
  final String suffix;
  final String NumText;
  final Color? color;
  final int ? paystate;
  final String reason;
  final int  recordType;
  final int?  recharge_type;//区分支付宝微信

  final int  event;//区分收益 明细
  final String  serial_number;//订单号

  const MymodelbacksCell(this.mainText, this.subText, this.suffix, this.NumText, this.color,this.paystate, this.reason,this.recordType,this.recharge_type,this.event,this.serial_number,{Key? key, })
      : super(key: key);

  @override
  Widget build(BuildContext context) {


    String  icons= "";
    if(recordType == 1){
      //收益明细
//1系统操作2收益兑换金币3收到礼物4收益提现5房主实时收益6房主周收益
      if(paystate == 1){
        icons="withdrawal/Group1";
      }else if(paystate == 2){
        icons="withdrawal/Group2";
      }else if(paystate == 3){
        icons="withdrawal/Group3";
      }else if(paystate == 4){
        icons="withdrawal/Group4";
      }else if(paystate == 5){
        icons="withdrawal/Group5";
      }else if(paystate == 6){
        icons="withdrawal/Group5";
      }else
      {
        icons="withdrawal/Group7";
      }
    }else{
      //钱包明细
      //1系统操作2收益兑换金币3用户充值4用户购买礼物5收红包6送红包7用户购买装扮
      //8直接赠送金币9收到直接赠送金币10金币兑换游戏币11金币购买头条12金币购买活动道具
      if(paystate == 1){
        icons="withdrawal/Recharge1";
      }else if(paystate == 3){
        if(recharge_type == 1){
          icons="withdrawal/Recharge2";
        }else if(recharge_type==2){
          icons="withdrawal/Recharge3";
        }
      }else if(paystate == 4){
        icons="withdrawal/Recharge8";
      }else if(paystate == 5 || paystate == 6){
        icons="withdrawal/Recharge5";
      }else if(paystate == 8 || paystate == 9){
        icons="withdrawal/Recharge9";
      }else if(paystate == 10){
        icons="withdrawal/Recharge10";
      }else{
        icons="withdrawal/Group7";
      }


    }

    return GestureDetector(onTap: (){
      //收益明细 3收到礼物 4提现
      if(recordType == 1){
        if(paystate == 3){
          WidgetUtils().pushPage(Drawaldetals(recordType: 1,event: event,event_type:paystate,year_month: subText,serial_number: serial_number));
        }else if(paystate ==4){
          WidgetUtils().pushPage(BankcardDest(event: event,event_type:paystate,year_month: subText,serial_number: serial_number,));

          // final int event_type;
          // final String year_month;
          // final String serial_number;

        }else{
          return;
        }

      }else{
        if(paystate ==4){
          WidgetUtils().pushPage(Drawaldetals(recordType: 2,event: event,event_type:paystate,year_month: subText,serial_number: serial_number));
        }else{
          return;
        }
      }


    }, child:FContainer(

      padding: EdgeInsets.symmetric(horizontal: 20.w),
      border: Border(
        bottom: BorderSide(color: Color(0xFF222222).withOpacity(0.05),width: 2.w),
      ),
      child: Row(
        // mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [


            ClipRRect(
                borderRadius: BorderRadius.circular(20.w),
                child:LoadAssetImage(
                  icons,
                  width: 60.w,
                  height: 60.w,
                  fit: BoxFit.cover,
                )
            ),
            SizedBox(width: 20.w),
            reason.isNotEmpty ?   FContainer(
              height: 120.w,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 10.w),
                    Row(children: [
                      FText(
                        mainText,
                        size: 28.sp,
                        color: Color(0xFF222222),
                        overflow: TextOverflow.ellipsis,
                      ),
                      SizedBox(width: 10.w),
                      FText(
                        NumText,
                        size: 28.sp,
                        color: color,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],),
                    Visibility(
                      child: FText(
                        reason,
                        size: 20.sp,
                        color: color,
                        overflow: TextOverflow.ellipsis,
                      ),
                      visible: reason.isNotEmpty,
                    ),

                    FText(subText, size: 20.sp, color: Color(0xFF222222).withOpacity(0.6)),
                    SizedBox(height: 10.w),
                  ]),
            ):   FContainer(
              height: 100.w,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 10.w),
                    Row(children: [
                      FText(
                        mainText,
                        size: 28.sp,
                        color: Color(0xFF222222),
                        overflow: TextOverflow.ellipsis,
                      ),
                      SizedBox(width: 10.w),
                      FText(
                        NumText,
                        size: 28.sp,
                        color: color,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],),
                    const Spacer(),
                    FText(subText, size: 20.sp, color: Color(0xFF222222).withOpacity(0.6)),
                    SizedBox(height: 10.w),
                  ]),
            ),



            const Spacer(),

            FText(suffix, size: 28.sp, color:suffix.startsWith("-") ? Color(0xFF222222) : Color(0xFFFF753F) )
          ]),

    ));
  }
}
