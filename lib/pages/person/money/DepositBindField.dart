import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:flutter/services.dart';
import 'dart:io';
class DepositBindField extends StatefulWidget {
  final DepositInputType inputType;
  final String? origin;
  final Function(String) onInput;
  final bool? hasAuth;
  const DepositBindField(this.inputType, this.onInput,
      {Key? key, this.origin, this.hasAuth = false})
      : super(key: key);

  @override
  _DepositBindFieldState createState() => _DepositBindFieldState();
}

class _DepositBindFieldState extends State<DepositBindField> {
  TextEditingController controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    controller.text = widget.origin ?? "";
  }

  @override
  void didUpdateWidget(covariant DepositBindField oldWidget) {
    if (oldWidget.origin != widget.origin) {
      controller.text = widget.origin ?? "";
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      height: 88.w,
      margin: EdgeInsets.only(bottom: 40.w),
      radius: BorderRadius.circular(88.w),
      color: Color(0xFF0D222222),
      padding: EdgeInsets.fromLTRB(40.w, 0, 20.w, 00),
      child: Row(children: [
        FText(widget.inputType.mainText, size: 32.sp, nullLength: 0,color: Colors.black,),
        Expanded(child: field()),
        if (widget.inputType == DepositInputType.INPUT_CODE) CodeButton(getCode)
      ]),
    );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  Widget field() {
    List<TextInputFormatter>? formatter;
    if (widget.inputType.formatter != null) {
      formatter = [widget.inputType.formatter!];
    }

    InputBorder border =
        OutlineInputBorder(borderSide: BorderSide(color: Colors.transparent));
    return TextField(
      onChanged: widget.onInput,
      style: TextStyle(color: Colors.black.withOpacity(0.8), fontSize: 28.sp),
      inputFormatters: formatter,
      keyboardType: (widget.inputType != DepositInputType.INPUT_NAME && widget.inputType != DepositInputType.INPUT_ALI) 
        ? TextInputType.numberWithOptions(decimal: true) 
        : TextInputType.text,
      maxLength: widget.inputType.maxLength,
      decoration: InputDecoration(
        focusedBorder: border,
        enabledBorder: border,
        counterText: "",
        contentPadding: EdgeInsets.zero,
        hintText: widget.inputType.hintText,
        hintStyle: TextStyle(color: Color(0xFFBBBBBB), fontSize: 28.sp),
      ),
    );
  }

  Future<bool> getCode() async {
    if (widget.hasAuth == false) {
      WidgetUtils().showToast("请您先前往实名认证");
      return false;
    }
    String phone = UserConstants.userginInfo?.info.phone ?? "";
    if (phone.isEmpty) {
      WidgetUtils().showToast("请输入正确的手机号");
      return false;
    }

    bool isSuccess = false;
    await DioUtils().httpRequest(
      NetConstants.loginCode,
      method: DioMethod.POST,
      params: {"phone": phone, "type": 2,  "platform": Platform.isAndroid ? "android" : "ios"},
      onSuccess: (_) => isSuccess = true,
    );
    return isSuccess;
  }
}
