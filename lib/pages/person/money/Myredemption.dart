import 'package:echo/import.dart';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Myredemption extends StatefulWidget {
  final String origin;
  final void Function(int) onSub;

  const Myredemption(this.origin,this.onSub,{Key? key,}) : super(key: key);


  @override
  _MyredemptionState createState() => _MyredemptionState();
}

class _MyredemptionState extends State<Myredemption> {
  TextEditingController controller = TextEditingController();
  int glas = 0;
  //比例以及倍数
  double reate=0.00;
  int limit_min = 0;
  int multiple = 0;



  @override
  void initState() {
    super.initState();
    requestMap();
  }

  @override
  Widget build(BuildContext context) {

    return  GestureDetector(
      onTap: (){
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: FContainer(
        padding: EdgeInsets.only(
            top: 50.w,
            left: 30.w,
            right: 30.w,
            bottom: ScreenUtil().bottomBarHeight + 60.w),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            FText(
              "兑换金豆",
              size: 34.sp,
              color: Colors.black,
            ),
            FText(
              "1礼物收益=${(1-reate)}金豆,兑换后不可撤回",
              size: 24.sp,
              color: Colors.black.withOpacity(0.6),
            ),
            SizedBox(height: 20.h),
            SizedBox(child:Myindoulist(ondouSelect)),
            SizedBox(height: 20.w),
            seInsetsent(),
            FText(
              "${limit_min}起兑(仅支持兑换${multiple}的倍数) ",
              size: 24.sp,
              color: Colors.black.withOpacity(0.6),
            ),
            SizedBox(height: 50.h),
            FcationButton('兑换',
                isActivate: true, onPress: onTapQuite, radius: 90.w),
            SizedBox(
              height: MediaQuery.of(context).viewInsets.bottom,
            )
          ],
        ),
      ),
    );
  }
  //配置兑换比例
  void requestMap() {
    DioUtils().asyncHttpRequest(
      NetConstants.recordAudio,
      method: DioMethod.GET,
      params: {"conf_key": "gains_exchange_gold_rate"},
      onSuccess: ongoldMap,
    );
  }

  void ongoldMap(response) {
    if (response == null) return;
    var content =  json.decode(response ?? "");
       reate = content['rate'] ?? 0.00;
       limit_min = content['limit_min'] ?? 0;
       multiple = content['multiple'] ?? 0;
    if (!mounted) return;
    setState(() {});
  }



  void ondouSelect(int gold) {
    print('object');
    print(gold);
    glas = gold;
setState(() {
});

  }

  Widget seInsetsent() {
    return FContainer(
      height: 100.w,
      padding: EdgeInsets.fromLTRB(30.w, 6.w, 30.w, 0),
      radius: BorderRadius.circular(20.w),
      margin: EdgeInsets.symmetric(horizontal: 0.w, vertical: 0.w),
      color: Color(0xFF0D222222),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              FContainer(
                width: 300.w,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    LoadAssetImage("person/restprint",
                        width: 35.w, height: 35.w),
                    FContainer(
                      padding: EdgeInsets.fromLTRB(0.w, 6.w, 0.w, 0),
                      width: 250.w,
                      child: textField(),
                    ),
                  ],
                ),
              ),
              const Spacer(),
              FText(
                '=',
                color: Colors.black,
              ),
              const Spacer(),
              FContainer(
                width: 300.w,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(child: FText(glas == 0 ? "0": "${(glas * (1-reate)).toInt()}", size: 28.sp, color: const Color(0xFF222222)),),

                    LoadAssetImage("person/moneys", width: 35.w, height: 35.w),
                    // GestureDetector(
                    //   onTap: onTapSelf,
                    //   child: Row(
                    //     children: [
                    //       FText("全部兑换",
                    //           size: 24.sp,
                    //           color: const Color(0xFF222222),
                    //           weight: FontWeight.bold),
                    //       SizedBox(width: 15.h),
                    //     ],
                    //   ),
                    // )
                  ],
                ),
              ),
            ],
          ),
          Expanded(
              child: Container(
                  child: Row(
            children: [
              Expanded(
                child: Container(),
              ),
            ],
          ))),
        ],
      ),
    );
  }

  //全部兑换
  void onTapSelf() {
    double total = UserConstants.userginInfo?.goldInfo.gainsable ?? 0.0;
    if (total < limit_min || total > 99999990) {
      return WidgetUtils().showToast("提现金额是${limit_min}~99999990");
    }
    controller.text = "${total.toInt()}";
    setState(() {

    });
  }

  Widget textField() {
    InputBorder border =
        OutlineInputBorder(borderSide: BorderSide(color: Colors.transparent));

    return TextField(
      key: Key("Card"),
      style: TextStyle(color: Color(0xFF99222222), fontSize: 30.sp),
      inputFormatters: [
        FilteringTextInputFormatter.allow(RegExp('[0-9]')), //只允许输入数
        LengthLimitingTextInputFormatter(20),
      ],
      onChanged: (value) {
        double price = value.isNotEmpty ? double.parse(value) : 0;
        if(price % multiple !=0 || price <limit_min){
          glas=0;
          setState(() {
          });
          return WidgetUtils().showToast("${limit_min}起兑,兑换金额应为${multiple}的倍数 ");
        }else{
          if(price>0)glas=price.toInt();
        }
        // if(price <limit_min){
        //   return WidgetUtils().showToast("兑换金豆数量是${limit_min}~99999990");
        // }
        setState(() {
        });
      },
      maxLength: 20,
      controller: controller,
      decoration: InputDecoration(
        enabledBorder: border,
        focusedBorder: border,
        contentPadding: EdgeInsets.zero,
        hintStyle: TextStyle(color: Color(0xFF999999), fontSize: 30.sp),
        hintText: "自定义兑换数量",
        counterText: "",
      ),
    );
  }

  Widget btnCell(int page) {
    return GestureDetector(
        onTap: () {},
        child: FContainer(
          child: Column(
            children: [FText('text', size: 20.sp, color: Colors.black)],
          ),
        ));
  }

  // 兑换按钮
  void onTapQuite() {
    if (glas < 100 || glas > 99999990) {
      return WidgetUtils().showToast("兑换金豆数量是100~99999990");
      // return WidgetUtils().showToast("请选择兑换金豆数量");
    }
    requestChargeMap();
  }

  void requestChargeMap() {

    DioUtils().asyncHttpRequest(
      NetConstants.exchangeGift,
      method: DioMethod.POST,
      params: {"src": "app-user", "reason": "用户收益兑换金币", "change_value": glas},
      onSuccess: onChargeMap,
    );
  }

  void onChargeMap(response) {
    if (response == null) return;
    UserConstants.userginInfo?.goldInfo.gold = response['gold'];
    UserConstants.userginInfo?.goldInfo.gains = response['gains'];
    UserConstants.userginInfo?.goldInfo.goldable = response['gold_able'];
    UserConstants.userginInfo?.goldInfo.gainsable = response['gains_able'];


    WidgetUtils().popPage();
    widget.onSub(0);
    setState(() {});
  }
}
