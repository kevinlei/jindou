import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class FTimedayPicker extends StatefulWidget {
  final DateTime initTime;
  final Color? color;
  const FTimedayPicker({Key? key, required this.initTime, this.color})
      : super(key: key);

  @override
  _FTimedayPickerState createState() => _FTimedayPickerState();
}

class _FTimedayPickerState extends State<FTimedayPicker> {
  int minYear = DateTime.now().year;
  int maxYear = DateTime.now().year;
  late FixedExtentScrollController yearCtr;
  late FixedExtentScrollController monthCtr;

  int minyue = 10;
  int maxyue = DateTime.now().month;

  @override
  void initState() {
    super.initState();

    int initMonth = widget.initTime.month;
    yearCtr = FixedExtentScrollController(initialItem: maxYear - minYear);
    monthCtr = FixedExtentScrollController(initialItem: initMonth - 1);

    getAgeAndZodiac();
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      padding: EdgeInsets.only(
          left: 20.w,
          right: 20.w,
          top: 20.w,
          bottom: ScreenUtil().bottomBarHeight + 20.w),
      color: widget.color ?? Colors.black,
      child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [FSheetLine(), titleContent(), pickerContent()]),
    );
  }

  @override
  void dispose() {
    monthCtr.dispose();

    super.dispose();
  }

  Widget titleContent() {
    return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
      const TextButton(onPressed: null, child: SizedBox()),
      FText("时间选择", size: 32.sp, weight: FontWeight.bold),
      TextButton(
          onPressed: onConfirm,
          child: FText("确定", size: 28.sp, color: const Color(0xFFFF753F)))
    ]);
  }

  Widget pickerContent() {
    Widget content = Row(children: [
      PickerWheel(pickerItem(minYear, maxYear, "年", yearCtr),
          onSelect: onYearChange),
      PickerWheel(pickerItem(minyue, maxyue, "月", monthCtr),
          onSelect: onMonthChange),
    ]);
    return SizedBox(height: 360.h, child: content);
  }

  void onYearChange() {
    getAgeAndZodiac();
  }

  void getAgeAndZodiac() {
    int year = minYear, month = 1;
    if (yearCtr.hasClients) {
      year = yearCtr.selectedItem + minYear;
    }
    if (monthCtr.hasClients) {
      month = monthCtr.selectedItem + 1;
    }

    if (!mounted) return;
    setState(() {});
  }

  Widget pickerItem(
      int min, int max, String suffix, FixedExtentScrollController controller) {
    BorderSide borderSide =
        BorderSide(color: const Color(0xFFEDEEF3), width: 2.w);
    return CupertinoPicker.builder(
        childCount: max - min + 1,
        itemExtent: 80.h,
        selectionOverlay:
            FContainer(border: Border(top: borderSide, bottom: borderSide)),
        scrollController: controller,
        onSelectedItemChanged: null,
        itemBuilder: (_, index) => timeItem("${index + min}$suffix"));
  }

  Widget timeItem(String text) {
    return Center(
        child: FText(
      text,
      size: 32.sp,
    ));
  }

  void onMonthChange() {
    int month = monthCtr.selectedItem + minyue;
  }

  void onConfirm() {
    int year = minYear, month = 1;
    if (yearCtr.hasClients) {
      year = yearCtr.selectedItem + minYear;
    }
    if (monthCtr.hasClients) {
      month = monthCtr.selectedItem + 1;
    }

    DateTime time = DateTime(
      year,
      monthCtr.selectedItem + minyue,
    );
    WidgetUtils().popPage(time);
  }
}
