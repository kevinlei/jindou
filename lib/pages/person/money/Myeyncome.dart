import 'package:echo/import.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class Myeyncome extends StatefulWidget {
  const Myeyncome({super.key});

  @override
  State<Myeyncome> createState() => _MyeyncomeState();
}

class _MyeyncomeState extends State<Myeyncome> with WidgetsBindingObserver {
  TextEditingController phoneCtr = TextEditingController();
  LoginProvider provider = LoginProvider();
  ScrollController scroll = ScrollController();
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    provider.requestDynamicInfo();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => provider,
        builder: (_, __) {
          return Scaffold(
            backgroundColor: const Color(0xFF0B0017),
            body: FContainer(
              padding: EdgeInsets.only(left: 30.w, right: 30.w, bottom: 20.w),
              image: ImageUtils().getAssetsImage("public/huibg"),
              imageFit: BoxFit.fill,
              imageAlign: Alignment.topCenter,
              child: SafeArea(child: loginContent()),
            ),
          );
        });
  }

  Future<void> callRefresh() async {
    provider.requestDynamicInfo();
  }

  Widget loginContent() {
    return Column(
      children: [
        setContent(),
      ],
    );
  }

  Widget setContent() {
    return FContainer(
        child: Column(children: [
      CardAppBar(title: "礼物收益", actions: [
        GestureDetector(
          onTap: onTapSelf,
          child: Row(
            children: [
              FText("明细",
                  size: 23.sp,
                  color: const Color(0x99222222),
                  weight: FontWeight.bold),
              SizedBox(width: 15.h),
            ],
          ),
        )
      ]),
      FContainer(
        //个人中心
        margin:
            EdgeInsets.only(left: 15.w, top: 20.w, right: 15.w, bottom: 20.w),
        //我的账户
        child: BannerSwipe(BanbarSwper.TIXIAN_COM.brand),
      ),
      setyutent(),
    ]));
  }

  Widget setyutent() {
    return FContainer(
        child: Column(children: [
      seInsetsent(),
    ]));
  }

  //余额布局
  Widget seInsetsent() {
    return FContainer(
      height: 360.w,
      image: ImageUtils().getAssetsImage("person/monyBg"),
      imageFit: BoxFit.fill,
      padding: EdgeInsets.all(30.w),
      radius: BorderRadius.circular(20.w),
      margin: EdgeInsets.symmetric(horizontal: 15.w, vertical: 20.w),
      color: Colors.transparent,
      child: Column(
        children: [
          Row(
            children: [
              LoadAssetImage("person/lifostret", width: 72.w, height: 72.w),
              SizedBox(width: 15.h),
              FText("礼物收益", size: 28.sp, color: const Color(0xFFFFFFFF)),
            ],
          ),
          Expanded(
              child: Container(
                  child: Row(
            children: [
              Consumer<LoginProvider>(builder: (_, pro, __) {
                return Expanded(
                  child: Row(
                    children: [
                      Text("¥${FuncUtils().formatNum(pro.shouNum, 2)}",
                          textScaleFactor: 1.0,
                          style: TextStyle(
                              fontSize: 56.sp,
                              color: Colors.white,
                              fontWeight: FontWeight.bold)),
                      // Text("不可用余额${NumberFormat("#,##0", "en_US").format(UserConstants.userginInfo?.goldInfo.gainsLock ?? 0.0)}",
                      //     textScaleFactor: 1.0,
                      //     style: TextStyle(
                      //         fontSize: 24.sp,
                      //         color: Colors.white,
                      //     )),
                    ],
                  ),
                );
              }),
            ],
          ))),
          GestureDetector(
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [cancelButton(), confirmButton()])),
        ],
      ),
    );
  }

  //点击余额
  void onTapDetail() {}
  //明细按钮
  void onTapSelf() {
    // WidgetUtils().pushPage(MyrestList(1));
    WidgetUtils().pushPage(const AccountDetails(
      recordType: 1,
    ));
  }

  Widget cancelButton() {
    Widget btn = FContainer(
      width: 280.w,
      height: 80.w,
      border: Border.all(color: const Color(0xFFFFFFFF)),
      radius: BorderRadius.circular(80.r),
      align: Alignment.center,
      child: FText("兑换", color: const Color(0xFFFFFFFF), size: 32.sp),
    );
    return GestureDetector(onTap: backsBtn, child: btn);
  }

  void backsBtn() {
    WidgetUtils().showBottomSheet(
      Myredemption("ddss", onQualityInput),
      color: const Color(0xFFFFFFFF),
    );
  }

  void onQualityInput(int value) {
    provider.requestDynamicInfo();
    setState(() {});
  }

  Widget confirmButton() {
    Widget btn = FContainer(
      width: 280.w,
      height: 80.w,
      gradientBegin: Alignment.centerLeft,
      gradientEnd: Alignment.centerRight,
      gradient: const [Color(0xFFFF753F), Color(0xFFFF753F)],
      radius: BorderRadius.circular(80.w),
      align: Alignment.center,
      child: FText("提现", color: Colors.white, size: 32.sp),
    );
    return GestureDetector(onTap: ButtonSelf, child: btn);
  }

  Future<void> ButtonSelf() async {
    String? text =
        await WidgetUtils().pushPage(const Myreflect('data'), fun: callRefresh);
    if (text is String) {
      setState(() {
        provider.requestDynamicInfo();
      });
    }
  }

  void onAccept(accept) => provider.hasAccept = accept;

  @override
  void didChangeMetrics() {
    super.didChangeMetrics();
  }

  @override
  void dispose() {
    phoneCtr.dispose();
    scroll.dispose();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }
}
