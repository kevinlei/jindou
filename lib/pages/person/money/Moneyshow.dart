import 'package:echo/import.dart';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Moneyshow extends StatefulWidget {
  final String origin;
  final void Function(int) onSub;
  final int recordType;

  const Moneyshow(this.origin,this.onSub,this.recordType,{Key? key,}) : super(key: key);


  @override
  _MoneyshowState createState() => _MoneyshowState();
}

class _MoneyshowState extends State<Moneyshow> {
  TextEditingController controller = TextEditingController();
  int glas = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return  GestureDetector(
      onTap: (){
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: FContainer(
        padding: EdgeInsets.only(
            top: 50.w,
            left: 30.w,
            right: 30.w,
            bottom: ScreenUtil().bottomBarHeight + 20.w),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            FText(
              "选择筛选项",
              size: 34.sp,
              color: Colors.black,
            ),
            SizedBox(height: 40.h),
            SizedBox(child:MyretreList(ondouSelect,widget.recordType)),
            SizedBox(
              height: MediaQuery.of(context).viewInsets.bottom,
            )
          ],
        ),
      ),
    );
  }
  void ondouSelect(int gold) {
    print('object');
    print(gold);
    glas = gold;
    WidgetUtils().popPage(glas);
    widget.onSub(glas);
    setState(() {
    });
  }
}
