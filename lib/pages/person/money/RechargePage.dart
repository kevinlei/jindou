import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:intl/intl.dart';
import 'package:fluwx/fluwx.dart' as fluwx;
import 'package:tobias/tobias.dart';
import 'ChargeType.dart';

class ChargePage extends StatefulWidget {
  @override
  _ChargePageState createState() => _ChargePageState();
}

class _ChargePageState extends State<ChargePage> {
  ChargeProvider provider = ChargeProvider();
  bool accept = false;
  int chargeNum = 0;
  PayType payType = PayType.NULL;

  void onChargeSelect(int gold) => this.chargeNum = gold;
  void onAcceptSelect(bool accept) => this.accept = accept;
  void onTypeSelect(PayType type) => this.payType = type;

  @override
  void initState() {
    super.initState();
    requestDynamicInfo();
  }

  void requestDynamicInfo() {
    AppManager().requestDynamicInfo();
    if (!mounted) return;
    setState(() {});
  }

  @override
  void deactivate() {
    PluginUtils().setListener("ChargePage", null);
    super.deactivate();
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      padding: EdgeInsets.only(
          left: 30.w, right: 30.w, bottom: ScreenUtil().bottomBarHeight + 20.w),
      image: ImageUtils().getAssetsImage("public/huibg"),
      imageFit: BoxFit.fill,
      imageAlign: Alignment.topCenter,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: chargeAppbar(),
          body: Padding(
            padding: EdgeInsets.fromLTRB(20.w, 20.w, 20.w, 0),
            child: ListView(
              children: [
                BannerSwipe(BanbarSwper.PRICE_COM.brand),
                seInsetsent(),
                ChargeSelect(onChargeSelect),
                SizedBox(height: 40.w),
                FText(
                  "支付方式",
                  size: 32.sp,
                  weight: FontWeight.bold,
                  color: Colors.black,
                ),
                SizedBox(height: 30.w),
                ChargeType(onTypeSelect),
                SizedBox(height: 80.w),
                FProSAccept(
                  const [
                    ProtocolType.PRO_CHARGE,
                  ],
                  onAccept: onAcceptSelect,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    FText(
                      "未成年人请注意自我保护谨防上当受骗，享受健康生活。",
                      size: 20.sp,
                      color: const Color(0xFFBBBBBB),
                    ),
                  ],
                ),
                FcationButton(
                  "立即支付",
                  isActivate: true,
                  radius: 90.w,
                  onPress: onTapsNext,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void onTapsNext() async {
    requestDynamicInfo();
    if (!accept) return WidgetUtils().showToast("请先阅读并同意协议");
    if (chargeNum == 0) return WidgetUtils().showToast("请先选择充值数值");
    if (payType == PayType.NULL) return WidgetUtils().showToast("请先选择支付方式");
    switch (payType) {
      case PayType.PAY_WX:
        createOrder('wx');
        break;
      case PayType.PAY_ALI:
        createOrder('ali');
        break;
      case PayType.PAY_SD_KJ:
        createOrder('paypal');
        break;
      default:
    }
  }

  // 统一下单入口
  void createOrder(String type) async {
    Map params = {};
    if (type == 'wx') {
      params = {
        "product_id": chargeNum,
        "pay_type": 10,
        "token": AppConstants.accessToken,
      };
    } else if (type == 'ali') {
      params = {
        //套餐ID
        "product_id": chargeNum,
        "pay_type": 20,
        "token": AppConstants.accessToken,
      };
    } else {
      return WidgetUtils().showToast("暂不支持此支付方式");
    }

    await DioUtils().httpRequest(
      NetConstants.payCreateOrder,
      loading: false,
      method: DioMethod.POST,
      params: params,
      onSuccess: (response) {
        if (type == "wx") {
          wxPay(response);
        } else if (type == "ali") {
          aliPay(response);
        } else {}
      },
    );
  }

  //微信支付
  Future<void> wxPay(Map order) async {
    try {
      bool isInstalled = await fluwx.isWeChatInstalled;
      if (!isInstalled) {
        return WidgetUtils().showToast("请先安装微信");
      }
      fluwx.payWithWeChat(
        appId: order["appid"] ?? "",
        partnerId: order["partnerid"] ?? "",
        prepayId: order["prepayid"] ?? "",
        packageValue: order["package"] ?? "",
        nonceStr: order["noncestr"] ?? "",
        timeStamp: int.parse(order["timestamp"] ?? ""),
        sign: order["sign"] ?? "",
      );
      fluwx.weChatResponseEventHandler.listen((event) {
        if (event.isSuccessful) {
          WidgetUtils().showToast("微信支付成功");
          requestDynamicInfo();
          // 充值下单统计
          AppManager().xinstallFlutterPlugin.reportEventWhenOpenDetailInfo(
                "Recharge",
                chargeNum,
                UserConstants.userginInfo!.info.userId,
              );
        } else {
          WidgetUtils().showToast("微信支付失败");
        }
      });
    } catch (e) {
      WidgetUtils().showToast("微信支付订单数据错误，微信支付失败");
    }
  }

  // 支付宝支付
  void aliPay(String order) async {
    try {
      Tobias tobias = Tobias();
      bool isInstalled = await tobias.isAliPayInstalled;
      if (!isInstalled) {
        return WidgetUtils().showToast("请先安装支付宝");
      }
      Map data = await tobias.pay(order);
      if (data["resultStatus"] == 9000 || data["resultStatus"] == "9000") {
        WidgetUtils().showToast("支付宝支付成功");
        requestDynamicInfo();
        // 充值下单统计
        AppManager().xinstallFlutterPlugin.reportEventWhenOpenDetailInfo(
              "Recharge",
              chargeNum,
              UserConstants.userginInfo!.info.userId,
            );
      } else {
        WidgetUtils().showToast("支付宝支付失败");
      }
    } catch (e) {
      WidgetUtils().showToast("支付宝支付订单数据错误，支付宝支付失败");
    }
  }

  CommonAppBar chargeAppbar() {
    Widget suffix = TextButton(
        onPressed: onTapRecord,
        child: const FText(
          "明细",
          color: Colors.black,
        ));

    return CommonAppBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      title: "金豆充值",
      actions: [suffix],
    );
  }

  //余额布局
  Widget seInsetsent() {
    return FContainer(
      height: 250.w,
      image: ImageUtils().getAssetsImage("person/crePsionback"),
      imageFit: BoxFit.fill,
      padding: EdgeInsets.all(30.w),
      radius: BorderRadius.circular(20.w),
      margin: EdgeInsets.symmetric(horizontal: 0.w, vertical: 20.w),
      color: Colors.transparent,
      child: Column(
        children: [
          Row(
            children: [
              LoadAssetImage("person/moneys", width: 72.w, height: 72.w),
              SizedBox(width: 15.h),
              FText("金豆余额", size: 28.sp, color: const Color(0xFFFFFFFF)),
            ],
          ),
          const Spacer(),
          GestureDetector(
            onTap: onTapDetail,
            child: Row(children: [
              SizedBox(
                width: 500.w,
                child: Text(
                  "¥${UserConstants.userginInfo?.goldInfo.gold}",
                  textScaleFactor: 1.0,
                  style: TextStyle(
                    overflow: TextOverflow.ellipsis,
                    fontSize: 56.sp,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ]),
          ),
        ],
      ),
    );
  }

  //点击余额
  void onTapDetail() {}

  //明细
  void onTapRecord() {
    WidgetUtils().pushPage(const AccountDetails(recordType: 2));
  }
}
