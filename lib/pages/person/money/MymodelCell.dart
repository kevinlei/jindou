import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class MymodelCell extends StatelessWidget {
  final String mainText;
  final String subText;
  final String suffix;
  final String NumText;
  final Color? color;
  final int ? paystate;
  final String reason;
  const MymodelCell(this.mainText, this.subText, this.suffix, this.NumText, this.color,this.paystate, this.reason,{Key? key, })
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(onTap: (){

    }, child: FContainer(
      padding: EdgeInsets.symmetric(horizontal: 20.w),
      border: Border(
        bottom: BorderSide(color: Color(0xFF222222).withOpacity(0.05),width: 2.w),
      ),
      child: Row(
        // mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            ClipRRect(
                borderRadius: BorderRadius.circular(20.w),
                child:LoadAssetImage(
                  paystate == 1 ? "person/al" : "person/un",
                  width: 60.w,
                  height: 60.w,
                  fit: BoxFit.cover,
                )
            ),
            SizedBox(width: 20.w),
            reason.isNotEmpty ?   FContainer(
              height: 120.w,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 10.w),
                    Row(children: [
                      FText(
                        mainText,
                        size: 28.sp,
                        color: Color(0xFF222222),
                        overflow: TextOverflow.ellipsis,
                      ),
                      SizedBox(width: 10.w),
                      FText(
                        NumText,
                        size: 28.sp,
                        color: color,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],),
                    Visibility(
                      child: FText(
                        reason,
                        size: 20.sp,
                        color: color,
                        overflow: TextOverflow.ellipsis,
                      ),
                      visible: reason.isNotEmpty,
                    ),

                    FText(subText, size: 20.sp, color: Color(0xFF222222).withOpacity(0.6)),
                    SizedBox(height: 10.w),
                  ]),
            ):   FContainer(
              height: 100.w,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 10.w),
                    Row(children: [
                      FText(
                        mainText,
                        size: 28.sp,
                        color: Color(0xFF222222),
                        overflow: TextOverflow.ellipsis,
                      ),
                      SizedBox(width: 10.w),
                      FText(
                        NumText,
                        size: 28.sp,
                        color: color,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],),
                    const Spacer(),
                    FText(subText, size: 20.sp, color: Color(0xFF222222).withOpacity(0.6)),
                    SizedBox(height: 10.w),
                  ]),
            ),



            const Spacer(),

            FText(suffix, size: 28.sp, color:suffix.startsWith("-") ? Color(0xFF222222) : Color(0xFFFF753F) )
          ]),

    ),);

  }
}
