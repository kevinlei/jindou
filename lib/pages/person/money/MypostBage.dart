import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'dart:io';
class DepositBindPage extends StatefulWidget {
  final DepositType bindType;

  const DepositBindPage({Key? key, required this.bindType}) : super(key: key);

  @override
  _DepositBindPageState createState() => _DepositBindPageState();
}

class _DepositBindPageState extends State<DepositBindPage> {
  DepositBindProvider provider = DepositBindProvider();

  @override
  void initState() {
    super.initState();
    requestAuthStatus();
    requestBindStatus();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => provider,
        builder: (_, __) {
          return Scaffold(
            backgroundColor: const Color(0xFFF5F6F7),
            body: FContainer(
              padding: EdgeInsets.only(left: 15.w, right: 15.w, bottom: 0.w),
              image: ImageUtils().getAssetsImage("public/huibg"),
              imageFit: BoxFit.fill,
              imageAlign: Alignment.topCenter,
              child: SafeArea(child: Scaffold(
                backgroundColor: Colors.transparent,
                appBar: CommonAppBar(
                  elevation: 0,
                  title: widget.bindType.text,
                  textColor: Colors.black,
                  backgroundColor: Colors.transparent,
                ),
                body: GestureDetector(
                  onTap: () => FocusScope.of(context).unfocus(),
                  child: ListView(children: [
                    SizedBox(height: 40.w),
                    Align(
                      alignment: Alignment.center,
                      child: FText("绑定手机号：${UserConstants.userginInfo?.info.phone}",
                          size: 32.sp,color: Colors.black,),
                    ),
                    SizedBox(height: 40.w),
                    DepositBindField(
                        DepositInputType.INPUT_NAME, provider.onInputName),
                    DepositBindField(
                        widget.bindType == DepositType.DEP_ALI
                            ? DepositInputType.INPUT_ALI
                            : DepositInputType.INPUT_BANK,
                        widget.bindType == DepositType.DEP_ALI
                            ? provider.onInputAli
                            : provider.onInputBank),
                    DepositBindField(
                      DepositInputType.INPUT_CODE,
                      provider.onInputCode,
                      hasAuth: provider.authStatus == AuthStatus.MANUAL_S ||
                          provider.authStatus == AuthStatus.AUTO_S,
                    ),
                  ], padding: EdgeInsets.symmetric(horizontal: 25.w)),
                ),
                bottomNavigationBar:
                Material(color: Colors.transparent, child: confirmButton()),
              ),),
            ),
          );


        });
  }

  Widget confirmButton() {
    double bottom = ScreenUtil().bottomBarHeight;
    return Consumer<DepositBindProvider>(builder: (_, pro, __) {
      Widget btn = FContainer(
        height: 90.w,
        gradient: pro.visible
            ? [Color(0xFF46F5F6), Color(0xFFFFA7FB),Color(0xFFFF82B2)]
            : [Color(0xFF0D222222), Color(0xFF0D222222)],
        gradientBegin: Alignment.centerLeft,
        gradientEnd: Alignment.centerRight,
        //  color: pro.visible ? Color(0xFFA852FF) : Color(0xFFE1E2E5),
        radius: BorderRadius.circular(90.w),
        margin: EdgeInsets.fromLTRB(25.w, 20.h, 25.w, 20.h + bottom),
        align: Alignment.center,
        child: FText("确定", size: 32.sp, weight: FontWeight.bold),
      );
      return GestureDetector(child: btn, onTap: onTapConfirm);
    });
  }

  void onTapConfirm() {
    if (provider.authStatus != AuthStatus.AUTO_S &&
        provider.authStatus != AuthStatus.MANUAL_S) {
      // WidgetUtils().showToast("请您先前往实名认证");
      return onTapAuth();
    }
    String phone = UserConstants.userginInfo?.info.phone ?? "";
    if (phone.isEmpty) {
      return WidgetUtils().showToast("手机号为空");
    }
    if (provider.name.isEmpty) {
      return WidgetUtils().showToast("请输入姓名");
    }
    if (provider.card.isEmpty) {
      return WidgetUtils().showToast("请输入您的账号");
    }
    if (!FuncUtils().isMobileCode(provider.code)) {
      return WidgetUtils().showToast("请输入正确的验证码");
    }
    if (!provider.visible) {
      return WidgetUtils().showToast("您未作任何修改");
    }
    requestBind();
  }

  void requestBind() {
    String phone = UserConstants.userginInfo?.info.phone ?? "";
    DioUtils().asyncHttpRequest(
      NetConstants.bindCard,
      method: DioMethod.POST,
      params: {
        "account_type": widget.bindType.index==1 ? 10 : 20 ,
        "account_number": provider.card,
        "name": provider.name,
        "code": provider.code,
        "phone": phone,
        "platform": Platform.isAndroid ? "android" : "ios",
      },
      onSuccess: onBindSuccess,
    );
  }

  void onBindSuccess(response) {
    WidgetUtils().showToast("绑定成功");
    WidgetUtils().popPage();
  }

  void requestBindStatus() {
    DioUtils().asyncHttpRequest(
      NetConstants.bindStatus,
      params: {"type": widget.bindType.index},
      onSuccess: onGetStatus,
    );
  }

  // [{"Type":1,"CardNumber":"1234567890","Name":"mm"}]
  void onGetStatus(response) {
    provider.onGetBindStatus(response);
    if (!mounted) return;
    setState(() {});
  }


  void requestAuthStatus() {
    DioUtils().asyncHttpRequest(NetConstants.authStatus,
        method: DioMethod.GET, onSuccess: onAuthStatus);
  }

  void onAuthStatus(response) {
    if (response == null) return;

    provider.authStatus = getCheckStatus(response["validate_idcard"]);

    if (provider.authStatus != AuthStatus.AUTO_S &&
        provider.authStatus != AuthStatus.MANUAL_S) {
      // WidgetUtils().showToast("请您先前往实名认证");
      return onTapAuth();
    }
    if (!mounted) return;
    setState(() {});
  }

  AuthStatus getCheckStatus(int? status) {
    for (AuthStatus item in AuthStatus.values) {
      if (item.value == status) return item;
    }
    return AuthStatus.NULL;
  }

  void onTapAuth() async {
    bool? accept =
    await WidgetUtils().showAlert("请前往金豆派对进行实名认证.", confirmText: '前往实名');
    if (accept == null || !accept) return;
    await WidgetUtils().pushPage(AuthPage1());
  }
}
