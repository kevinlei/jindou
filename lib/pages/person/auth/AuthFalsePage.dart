import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class AuthFalsePage extends StatefulWidget {
  const AuthFalsePage({super.key});

  @override
  State<AuthFalsePage> createState() => _AuthFalsePageState();
}

class _AuthFalsePageState extends State<AuthFalsePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF5F6F7),
      appBar: CommonAppBar(
        title: "审核状态",
        textColor: Colors.black,
        elevation: 1,
        backgroundColor: Color(0xFFF5F6F7),
      ),
      body: authContent(),
      bottomNavigationBar: buttonBottom(),
    );
  }

  Widget authContent() {
    return Column(children: [
      SizedBox(
        height: 50.h,
        width: 760.w,
      ),
      LoadAssetImage("set/auth3", width: 340.w, height: 240.w),
      SizedBox(height: 20.h),
      FText('审核未通过', size: 36.sp, color: Color(0xFFBBBBBB)),
    ]);
  }

  Widget buttonBottom() {
    return FContainer(
      height: 220.h,
      margin: EdgeInsets.only(bottom: 40.h),
      color: Colors.transparent,
      child: Column(
        children: [
          FContainer(
            margin: EdgeInsets.only(bottom: 20.h),
            child: GestureDetector(
                onTap: () => onTapProto(),
                child: FText(
                  '前往人工审核',
                  size: 24.sp,
                  color: Colors.black.withOpacity(0.4),
                )),
          ),
          FcationButton(
            "重新认证",
            isActivate: true,
            radius: 90.w,
            onPress: () {
              WidgetUtils().popPage();
            },
          )
        ],
      ),
    );
  }

  //跳转人工审核
  void onTapProto() {
    FocusManager.instance.primaryFocus?.unfocus();
    WidgetUtils().pushReplace(AuthPage3());
  }
}
