
import 'package:echo/import.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AliyunFaceProvider extends ChangeNotifier {
  String nameText = "";
  String cardText = "";
  int autoCount = 0;

  String cardPath1 = "";
  String cardPath2 = "";
  String cardPath3 = "";

  String aliyunToken = "";

  int curPage = 0;
  bool isChecking = false;
  bool isConfirm = false;

  String certifyId = '';

  /// default checking checked exception
  String aliyunType = "default";

  /// 0：正视前方 1：向右转头 2：向左转头 3：张嘴动作 4：眨眼动作
  int cuAliveStep = 0;

  /// 异常状态 1：保持面部在框内 2：环境光线过暗 3：环境光线过亮 4：请勿抖动手机
  String aliyunResult = "请保持适当距离, 正对手机屏幕将面目移入框内";
  // 照片认证方式
  Future<void> getMetaInfos(
    String name,
    String card,
    NetSuccessCallback? onSuccess,
    NetErrorCallback? onError,
  ) async {
    String metainfos;

    nameText = name;
    cardText = card;

    try {
      // 获取客户端metainfos，将信息发送到服务器端，调用服务器端相关接口获取认证ID，即CertifyId。
      metainfos = await AppManager().aliyunFacePlugin.getMetaInfos() ??
          'Unknown metainfos';
    } on PlatformException {
      metainfos = 'Failed to get metainfos.';
    }
    //将metainfos 传到后台
    DioUtils().asyncHttpRequest(
      NetConstants.aliFaceCheck,
      method: DioMethod.POST,
      params: {
        "meta_info": metainfos,
        "cert_name": nameText,
        "cert_no": cardText,
        "mode": 1,
      },
      onSuccess: (res) {
        if (res == null) return;
        debugPrint('====${res}');
        debugPrint('====${res['certify_id']}');
        certifyId = res['certify_id'];
        startVerify(onSuccess, onError);
      },
    );
  }

  Future<void> startVerify(
    NetSuccessCallback? onSuccess,
    NetErrorCallback? onError,
  ) async {
    String verifyResult;
    try {
      // 调用认证接口，CertifyId需要调用服务器端接口获取。
      // 每个CertifyId只能使用一次，否则会返回code: "2002(iOS), 1001(Android)"。
      WidgetUtils().showLoading();
      verifyResult =
          await AppManager().aliyunFacePlugin.verify("certifyId", certifyId) ??
              '-1,error';
    } on PlatformException {
      WidgetUtils().cancelLoading();
      verifyResult = '-2,exception';
    }
    debugPrint('实名认证结$verifyResult');
    debugPrint('实名认证结${verifyResult.contains('1000')}');
    if (verifyResult.contains('1000')) {
      DioUtils().asyncHttpRequest(NetConstants.authManual,
          method: DioMethod.POST,
          params: {
            "name": nameText,
            "idcard": cardText,
            "certify_id": certifyId,
            "FrontPicture": ""
          }, onSuccess: (data) {
        WidgetUtils().showToast("验证成功");
        debugPrint('============');
        onSuccess!(data);
      });
    } else {
      WidgetUtils().cancelLoading();
      onError!(1, '');
      // WidgetUtils().showToast("验证失败，请尝试重新认证");
    }
  }

  //身份证方式
  Future<void> getMetaupsInfos(
    NetSuccessCallback? onSuccess,
    NetErrorCallback? onError,
  ) async {
    String metainfos;
    try {
      // 获取客户端metainfos，将信息发送到服务器端，调用服务器端相关接口获取认证ID，即CertifyId。
      metainfos = await AppManager().aliyunFacePlugin.getMetaInfos() ??
          'Unknown metainfos';
    } on PlatformException {
      metainfos = 'Failed to get metainfos.';
    }

    //将metainfos 传到后台
    debugPrint('***');
    debugPrint(metainfos);
    DioUtils().asyncHttpRequest(
      NetConstants.aliFaceCheck,
      method: DioMethod.POST,
      params: {
        "meta_info": metainfos,
        "mode": 2,
      },
      onSuccess: (res) {
        if (res == null) return;
        debugPrint('====${res}');
        debugPrint('====${res['certify_id']}');
        certifyId = res['certify_id'];
        startVersify(onSuccess, onError);
      },
    );
  }

  Future<void> startVersify(
    NetSuccessCallback? onSuccess,
    NetErrorCallback? onError,
  ) async {
    String verifyResult;
    try {
      // 调用认证接口，CertifyId需要调用服务器端接口获取。
      // 每个CertifyId只能使用一次，否则会返回code: "2002(iOS), 1001(Android)"。
      WidgetUtils().showLoading();
      verifyResult =
          await AppManager().aliyunFacePlugin.verify("certifyId", certifyId) ??
              '-1,error';
    } on PlatformException {
      WidgetUtils().cancelLoading();
      verifyResult = '-2,exception';
    }
    debugPrint('实名认证结$verifyResult');
    debugPrint('实名认证结${verifyResult.contains('1000')}');
    if (verifyResult.contains('1000')) {
      DioUtils().asyncHttpRequest(NetConstants.authManual,
          method: DioMethod.POST,
          params: {"certify_id": certifyId, "FrontPicture": ""},
          onSuccess: (data) {
        WidgetUtils().showToast("验证成功");
        debugPrint('\============');
        onSuccess!(data);
      });
    } else {
      WidgetUtils().cancelLoading();
      onError!(1, '');
      // WidgetUtils().showToast("验证失败，请尝试重新认证");
    }
  }
}
