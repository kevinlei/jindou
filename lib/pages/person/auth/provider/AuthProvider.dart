import 'dart:io';

// import 'package:alive_flutter_plugin/alive_flutter_plugin.dart';
import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:flutter/services.dart';
import 'package:permission_handler/permission_handler.dart';

class AuthProvider extends ChangeNotifier {
  String nameText = "";
  String cardText = "";
  int autoCount = 0;

  String cardPath1 = "";
  String cardPath2 = "";
  String cardPath3 = "";

  String aliveToken = "";

  int curPage = 0;
  bool isChecking = false;
  bool isConfirm = false;

  /// default checking checked exception
  String aliveType = "default";

  /// 0：正视前方 1：向右转头 2：向左转头 3：张嘴动作 4：眨眼动作
  int cuAliveStep = 0;

  /// 异常状态 1：保持面部在框内 2：环境光线过暗 3：环境光线过亮 4：请勿抖动手机
  String aliveResult = "请保持适当距离, 正对手机屏幕将面目移入框内";

  EventChannel eventChannel = EventChannel("yd_alive_flutter_event_channel");
  // final AliveFlutterPlugin alivePlugin = new AliveFlutterPlugin();

  void initAliveSDK() {
    eventChannel
        .receiveBroadcastStream()
        .listen(onAuthStatus, onError: onAuthError);
    WidgetsBinding.instance.addPostFrameCallback((_) {
      // alivePlugin.init(SDkConstants.aliveAuth, 40);
    });
  }

  @override
  void dispose() {
    eventChannel.receiveBroadcastStream().listen(null);
    // alivePlugin.stopLiveDetect();
    if (Platform.isAndroid) {
      // alivePlugin.destroy();
    }
    super.dispose();
  }

  void onPageChanged(int page) {
    curPage = page;
    if (page == 1) {
      // alivePlugin.stopLiveDetect();
      isChecking = false;
    }
    notifyListeners();
  }

  void updateCardPath(int index, String path) {
    if (index == 1) {
      cardPath1 = path;
    } else if (index == 2) {
      cardPath2 = path;
    } else {
      cardPath3 = path;
    }
  }

  String getCardPath(int index) {
    if (index == 1) return cardPath1;
    if (index == 2) return cardPath2;
    if (index == 3) return cardPath3;
    return "";
  }

  void onAuthStatus(response) {
    try {
      debugPrint('当前实名检测$response["currentStep"]');
      if (!(response is Map)) return;
      String type = response["type"];
      if (type == "checking") {
        aliveType = type;
        cuAliveStep = response["currentStep"];
        aliveResult = getStep(cuAliveStep);
        isChecking = true;
        debugPrint("检测中----------------------");
        debugPrint("message---------------------- $aliveResult");
      } else if (type == "checked") {
        aliveType = type;
        cuAliveStep = 0;
        if (Platform.isAndroid) {
          aliveToken = response["message"];
        } else {
          aliveToken = response["token"];
        }
        aliveResult = "检测成功，等待提交";
        // alivePlugin.stopLiveDetect();
        isChecking = false;
        confirmAuto();
        debugPrint("检测结束----------------------");
        debugPrint("message---------------------- $aliveResult");
      } else if (type == "exception") {
        aliveType = type;
        cuAliveStep = 0;
        aliveResult = response["message"];
        if (aliveResult.startsWith("{")) {
          aliveResult = "";
        }
        isChecking = false;
        debugPrint("检测异常----------------------");
        debugPrint("message---------------------- $aliveResult");
        // alivePlugin.stopLiveDetect();
      }
    } catch (err) {
      debugPrint("检测异常Error: $err");
      // alivePlugin.stopLiveDetect();
    }
    notifyListeners();
  }

  // 0——正面，1——右转，2——左转，3——张嘴，4——眨眼。
  String getStep(int step) {
    if (step == 0) return "请正对手机";
    if (step == 1) return "慢慢右转头";
    if (step == 2) return "慢慢左转头";
    if (step == 3) return "请张张嘴";
    if (step == 4) return "请眨眨眼";

    return "";
  }

  void onAuthError(error) {
    debugPrint("-------------------------error: $error");
    WidgetUtils().showToast("识别错误");
  }

  void onTapConfirm() {
    try {
      if (isConfirm) return;
      if (curPage == 0) {
        startAliveCheck();
      } else {
        confirmManual();
      }
    } catch (err) {
      debugPrint('出错了$err');
    }
  }

  void startAliveCheck() async {
    if (isChecking) return;
    if (autoCount >= 3) WidgetUtils().showToast("快捷认证次数已用完");
    bool accept = await FuncUtils().requestPermission(Permission.camera);
    if (!accept) return;
    // await alivePlugin.startLiveDetect();
  }

  void confirmAuto() async {
    if (isConfirm) return;
    if (nameText.isEmpty) return WidgetUtils().showToast("姓名未填写");
    if (cardText.isEmpty) return WidgetUtils().showToast("身份证号未填写");
    if (aliveToken.isEmpty) return WidgetUtils().showToast("检测token为空");
    isConfirm = true;
    await DioUtils().httpRequest(
      NetConstants.authStatus,
      method: DioMethod.POST,
      params: {"cardNo": cardText, "name": nameText, "token": aliveToken},
      onSuccess: (_) => WidgetUtils().popPage(),
    );
    isConfirm = false;
    aliveToken = "";
  }

  void confirmManual() async {
    if (isConfirm) return;
    if (nameText.isEmpty) return WidgetUtils().showToast("姓名未填写");
    if (cardText.isEmpty) return WidgetUtils().showToast("身份证号未填写");
    if (cardPath1.isEmpty || cardPath2.isEmpty || cardPath3.isEmpty) {
      return WidgetUtils().showToast("请提供证件照");
    }
    WidgetUtils().showLoading();
    String? path1 =
        await FuncUtils().requestUpload(cardPath1, UploadType.REAL_ID_CARD);
    if (path1 == null) return WidgetUtils().cancelLoading();
    if (path1.isEmpty) {
      WidgetUtils().showToast("上传证件照失败");
      WidgetUtils().cancelLoading();
      return;
    }
    String? path2 =
        await FuncUtils().requestUpload(cardPath2, UploadType.REAL_ID_CARD);
    if (path2 == null) return WidgetUtils().cancelLoading();
    if (path2.isEmpty) {
      WidgetUtils().showToast("上传证件照失败");
      WidgetUtils().cancelLoading();
      return;
    }
    String? path3 =
        await FuncUtils().requestUpload(cardPath3, UploadType.REAL_ID_CARD);
    if (path3 == null) return WidgetUtils().cancelLoading();
    if (path3.isEmpty) {
      WidgetUtils().showToast("上传证件照失败");
      WidgetUtils().cancelLoading();
      return;
    }

    await DioUtils().httpRequest(
      NetConstants.authManual,
      method: DioMethod.POST,
      params: {
        "idcard": cardText,
        "name": nameText,
        "id_card_front_side_pic": path1,
        "id_card_back_side_pic": path2,
        "holding_id_card": path3,
      },
      onSuccess: (_) => WidgetUtils().popPage(),
    );
    WidgetUtils().cancelLoading();
    isConfirm = false;
  }
}
