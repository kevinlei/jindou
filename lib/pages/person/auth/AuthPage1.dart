import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:flutter/services.dart';

import 'provider/AliyunFaceProvider.dart';

class AuthPage1 extends StatefulWidget {
  @override
  _AuthPage1State createState() => _AuthPage1State();
}

class _AuthPage1State extends State<AuthPage1> {
  String nameText = "";
  String cardText = "";
  TextEditingController editingName = TextEditingController();
  TextEditingController editingCard = TextEditingController();
  AliyunFaceProvider aliyunprovider = AliyunFaceProvider();
  // 剩余快捷次数
  int autoCount = 0;

  bool hasjson = false;

  AuthStatus authStatus = AuthStatus.NULL;
  int Nunbaer=0;

  @override
  void initState() {
    super.initState();
    requestAuthStatus();
    requestNumMap();

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF5F6F7),
      appBar: CommonAppBar(
        title: "实名认证",
        textColor: Colors.black,
        elevation: 1,
        backgroundColor: Color(0xFFF5F6F7),
      ),
      body: hasjson ? authContent() : SizedBox(),
      bottomNavigationBar: hasjson ? buttonBottom() : SizedBox(),
    );
  }

  @override
  void dispose() {
    editingName.dispose();
    editingCard.dispose();
    super.dispose();
  }

  void requestNumMap() {
    DioUtils().asyncHttpRequest(
      NetConstants.recordAudio,
      method: DioMethod.GET,
      params: {"conf_key": "pid-max"},
      onSuccess: onChargeMap,
    );
  }

  void onChargeMap(response) {
    if (response == null) return;
    Nunbaer=int.parse(response);
    if (!mounted) return;
    setState(() {});
  }

  Widget inputField(bool isName) {
    return FContainer(
      height: 88.w,
      color: Color(0xFF000000).withOpacity(0.04),
      radius: BorderRadius.circular(88.w),
      margin: EdgeInsets.only(bottom: 40.h),
      padding: EdgeInsets.symmetric(horizontal: 25.w),
      child: Row(children: [
        FText(
          isName ? "姓名：" : "身份证号：",
          size: 32.sp,
          color: Colors.black,
        ),
        SizedBox(width: 10.w),
        Expanded(child: textField(isName))
      ]),
    );
  }

  Widget textField(bool isName) {
    InputBorder border =
        OutlineInputBorder(borderSide: BorderSide(color: Colors.transparent));
    List<TextInputFormatter>? formatter;
    if (isName) {
      formatter = [
        FilteringTextInputFormatter.allow(
            RegExp(r'[-_\u4E00-\u9FA5a-zA-Z0-9\u2006\.\•\·]'))
      ];
    }
    return TextField(
      key: Key(isName ? "Name" : "Card"),
      controller: isName ? editingName : editingCard,
      style: TextStyle(color: const Color(0xFF99222222), fontSize: 30.sp),
      inputFormatters: formatter,
      maxLength: 18,
      decoration: InputDecoration(
        enabledBorder: border,
        focusedBorder: border,
        contentPadding: EdgeInsets.zero,
        hintStyle: TextStyle(color: Color(0xFF999999), fontSize: 30.sp),
        hintText: isName ? "请输入您的真实姓名" : "请输入您的身份证号",
        counterText: "",
      ),
    );
  }

  Widget authContent() {
    if (authStatus == AuthStatus.NULL) {
      return ListView(
          padding: EdgeInsets.fromLTRB(25.w, 40.h, 25.w, 0),
          children: [
            inputField(true),
            inputField(false),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20.w),
              child: FText(
                "1、根据相关法律法规，在使用直播、提现服务之前需要填写身份信息进行实名认证。\n"
                "2、请您填写真实有效的身份信息，请您明确，实名信息将会被作为身份识别、判定您的金豆派对账号使用归属权的依据。\n"
                "3、您所提交的身份信息，我们将会严密保管，不会被用于其他用途。\n"
                "4、同一个身份证最多认证${Nunbaer}个金豆派对账号。\n",
                size: 20.sp,
                maxLines: 6,
                color: Color(0xFF999999),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20.w),
              child: Text.rich(
                  TextSpan(children: [
                    TextSpan(
                      text: "5、如快捷认证不通过，请选择",
                      style:
                          TextStyle(color: Color(0xFF999999), fontSize: 20.sp),
                    ),
                    TextSpan(
                      text: "人工审核",
                      style:
                          TextStyle(color: Color(0xFF008DF5), fontSize: 20.sp),
                      recognizer: TapGestureRecognizer()
                        ..onTap = () => onTapProto(),
                    ),
                    TextSpan(
                      text: "进行实名认证。",
                      style:
                          TextStyle(color: Color(0xFF999999), fontSize: 20.sp),
                    )
                  ]),
                  textScaleFactor: 1),
            ),
          ]);
    } else {
      return AuthCheckStatus(
          nameText: nameText, cardText: cardText, status: authStatus);
    }
  }

  Widget buttonBottom() {
    switch (authStatus) {
      case AuthStatus.NULL:
        return FContainer(
            margin: EdgeInsets.only(bottom: 40.h),
            color: Colors.transparent,
            child: FcationButton("下一步",
                isActivate: true, radius: 90.w, onPress: onTapNext));
      case AuthStatus.AUTO_S:
        return SizedBox();
      case AuthStatus.MANUAL_S:
        return SizedBox();
      case AuthStatus.AUTO_F:
        return FContainer(
            height: 220.h,
            margin: EdgeInsets.only(bottom: 40.h),
            color: Color(0xFF1F2234),
            child: Column(
              children: [
                FContainer(
                  margin: EdgeInsets.only(bottom: 20.h),
                  child: GestureDetector(
                      onTap: () => onTapProto(),
                      child: FText(
                        '前往人工审核',
                        size: 24.sp,
                        color: Colors.white.withOpacity(0.4),
                      )),
                ),
                FLoginButton("重新认证",
                    isActivate: true, radius: 90.w, onPress: onTapReset)
              ],
            ));
      case AuthStatus.AUTO_W:
        return SizedBox();
      case AuthStatus.MANUAL_W:
        return SizedBox();
      case AuthStatus.MANUAL_F:
        return FContainer(
            height: 220.h,
            margin: EdgeInsets.only(bottom: 40.h),
            color: Color(0xFF1F2234),
            child: Column(
              children: [
                FContainer(
                  margin: EdgeInsets.only(bottom: 20.h),
                  child: GestureDetector(
                      onTap: () => onTapProto(),
                      child: FText(
                        '前往人工审核',
                        size: 24.sp,
                        color: Colors.white.withOpacity(0.4),
                      )),
                ),
                FLoginButton("重新认证",
                    isActivate: true, radius: 90.w, onPress: onTapReset)
              ],
            ));
    }
  }

  void onTapNext() async {
    FocusManager.instance.primaryFocus?.unfocus();
    if (editingName.text.length < 2)
      return WidgetUtils().showToast("请输入您的真实姓名");
    if (!FuncUtils().isIdCard(editingCard.text))
      return WidgetUtils().showToast("请输入正确的身份证号");
    aliyunprovider.getMetaInfos(editingName.text, editingCard.text, (res) {
      requestAuthStatus();
    }, (i, str) {
      WidgetUtils().pushPage(AuthFalsePage(), fun: requestAuthStatus);
    });

    // await WidgetUtils().pushPage(AuthPage2(
    //     nameText: editingName.text,
    //     cardText: editingCard.text,
    //     useCount: autoCount,
    //     initpage: 0));
    // requestAuthStatus();
  }

  void onTapReset() {
    nameText = "";
    cardText = "";
    editingName.clear();
    editingCard.clear();
    authStatus = AuthStatus.NULL;
    if (!mounted) return;
    setState(() {});
  }

  //跳转人工审核
  void onTapProto() {
    FocusManager.instance.primaryFocus?.unfocus();
    WidgetUtils().pushPage(AuthPage3(), fun: requestAuthStatus);
  }

  void requestAuthStatus() {
    debugPrint('6666666');
    WidgetUtils().showLoading();
    DioUtils().asyncHttpRequest(NetConstants.authStatus,
        method: DioMethod.GET, onSuccess: onAuthStatus);
  }

  // {"name":"哈**","card":"4206**********0699","time":1646623641931,"success":false,"msg":"正在人工审核中,请耐心等待","count":0,"status":4}
  void onAuthStatus(response) {
    if (response == null) return;
    autoCount = response["count"] ?? 0;
    authStatus = getCheckStatus(response["audit_status"]);
    nameText = response["name"] ?? "";
    cardText = response["idcard"] ?? "";
    hasjson = true;
    if (!mounted) return;
    WidgetUtils().cancelLoading();
    setState(() {});
  }

  AuthStatus getCheckStatus(int? status) {
    for (AuthStatus item in AuthStatus.values) {
      if (item.value == status) return item;
    }
    return AuthStatus.NULL;
  }
}
