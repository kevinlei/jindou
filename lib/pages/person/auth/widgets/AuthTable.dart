import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class AuthTable extends StatefulWidget {
  final PageController pageCtr;

  const AuthTable({Key? key, required this.pageCtr}) : super(key: key);

  @override
  _AuthTableState createState() => _AuthTableState();
}

class _AuthTableState extends State<AuthTable> {
  int curPage = 0;

  @override
  void initState() {
    super.initState();
    curPage = widget.pageCtr.initialPage;
    widget.pageCtr.addListener(onPageChanged);
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      height: 122.w,
      padding: EdgeInsets.symmetric(horizontal: 25.w),
      align: Alignment.center,
      child:
          Row(children: [buttonCell(0), SizedBox(width: 20.w), buttonCell(1)]),
    );
  }

  @override
  void deactivate() {
    widget.pageCtr.removeListener(onPageChanged);
    super.deactivate();
  }

  Widget buttonCell(int page) {
    Widget btn = FContainer(
      height: 74.w,
      radius: BorderRadius.circular(74.w),
      align: Alignment.center,
      child: Column(
        children: [
          FText(
            page == 0 ? "快捷认证" : "人工审核",
            color: page == curPage ? Colors.white  : Colors.white.withOpacity(0.5),
            size: 32.sp,
          ),
          SizedBox(height: 10.w,),
          Visibility(child: FContainer(
            width: 32.w,
            height: 6.w,
            color: Colors.white,
            radius: BorderRadius.circular(50),
          ),visible: page == curPage,),
        ],
      ),
    );
    return Flexible(
        child: GestureDetector(onTap: () => onTapCell(page), child: btn));
  }

  void onPageChanged() {
    if (!mounted) return;
    curPage = (widget.pageCtr.page ?? 0).toInt();
    setState(() {});
  }

  void onTapCell(int page) {
    widget.pageCtr.jumpToPage(page);
  }
}
