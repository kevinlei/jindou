import 'dart:io';

import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:flutter/services.dart';

class AuthAuto extends StatefulWidget {
  @override
  _AuthAutoState createState() => _AuthAutoState();
}

class _AuthAutoState extends State<AuthAuto>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Column(children: [
      SizedBox(height: 80.h),
      Consumer<AuthProvider>(builder: (_, pro, __) {
        return FText(pro.aliveResult,
            nullLength: 0, size: 32.sp, color: Colors.white);
      }),

      SizedBox(height: 80.h),
      FContainer(
        margin: EdgeInsets.symmetric(horizontal: 40.w),
        width: 340,
        color: Color(0xFF1F2234),
        height: 340,
        radius: BorderRadius.circular(170),
        align: Alignment.center,
        child: cameraContent(),
      ),
      
    ]);
  }

  Widget cameraContent() {
    if (Platform.isAndroid) {
      return AndroidView(
        viewType: 'platform-view-alive',
        creationParams: {"width": 340, "height": 340},
        creationParamsCodec: const StandardMessageCodec(),
        );
    } else if (Platform.isIOS) {
      return UiKitView(
        viewType: 'com.flutter.alive.imageview',
        creationParams: {"width": "340", "height": "340", "radius": "170"},
        creationParamsCodec: const StandardMessageCodec(),
      );
    }
    return LoadAssetImage("set/i_head", width: 140.w);
  }

  @override
  bool get wantKeepAlive => true;
}
