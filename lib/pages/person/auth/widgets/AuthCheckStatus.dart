import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class AuthCheckStatus extends StatelessWidget {
  final String nameText;
  final String cardText;
  final AuthStatus status;

  const AuthCheckStatus(
      {Key? key,
      required this.nameText,
      required this.cardText,
      required this.status})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (status == AuthStatus.NULL) {
      return SizedBox();
    }
    return Column(children: [
      SizedBox(height: 50.h),
      LoadAssetImage(getStatusImg(), width: 340.w, height: 240.w),
      FText(getStatusTxt(), color: Color(0xFFBBBBBB)),
      SizedBox(height: 150.h),
      textContent("姓名：$nameText"),
      textContent("身份证号：$cardText"),
    ]);
  }

  Widget textContent(String text) {
    return FContainer(
      height: 88.w,
      radius: BorderRadius.circular(88.w),
      color: Color(0xFF222222).withOpacity(0.05),
      padding: EdgeInsets.symmetric(horizontal: 25.w),
      align: Alignment.centerLeft,
      margin: EdgeInsets.symmetric(horizontal: 25.w, vertical: 20.h),
      child: FText(text,color: Colors.black,),
    );
  }

  String getStatusImg() {
    switch (status) {
      case AuthStatus.NULL:
        break;
      case AuthStatus.AUTO_S:
        return "set/auth1";
      case AuthStatus.MANUAL_S:
        return "set/auth1";
      case AuthStatus.AUTO_F:
        return "set/auth3";
      case AuthStatus.AUTO_W:
        return "set/auth2";
      case AuthStatus.MANUAL_W:
        return "set/auth2";
      case AuthStatus.MANUAL_F:
        return "set/auth3";
    }
    return "";
  }

  String getStatusTxt() {
    switch (status) {
      case AuthStatus.NULL:
        break;
      case AuthStatus.AUTO_S:
        return "认证完成";
      case AuthStatus.MANUAL_S:
        return "认证完成";
      case AuthStatus.AUTO_F:
        return "认证失败";
      case AuthStatus.AUTO_W:
        return "等待审核中";
      case AuthStatus.MANUAL_W:
        return "等待审核中";
      case AuthStatus.MANUAL_F:
        return "认证失败";
    }
    return "";
  }
}
