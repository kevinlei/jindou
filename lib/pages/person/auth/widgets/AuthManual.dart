import 'dart:io';

import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:flutter/services.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';


class AuthManual extends StatefulWidget {
  @override
  _AuthManualState createState() => _AuthManualState();
}

class _AuthManualState extends State<AuthManual> {
  String nameText = "";
  String cardText = "";
  TextEditingController editingName = TextEditingController();
  TextEditingController editingCard = TextEditingController();
  AuthProvider provider = AuthProvider();

  @override
  Widget build(BuildContext context) {
    return ListView(children: [
      SizedBox(height: 10.h),
      richTextContent(),
      SizedBox(height: 20.h),
      inputField(true),
      inputField(false),
      Container(
        margin: EdgeInsets.symmetric(vertical: 20.h),
        height: 1,
        width: 760.w,
        color: Color(0xFF181818),
      ),
      Padding(
        padding: EdgeInsets.symmetric(horizontal: 30.w),
        child: Row(
          children: [
            FText("请上传身份证正面", size: 32.sp),
            Spacer(),
            GestureDetector(
              onTap: () => exampleBtn(1),
              child: FText(
                "查看示例",
                size: 32.sp,
                color: Color(0xFFBD12F9),
              ),
            ),
          ],
        ),
      ),
      cardCell(1),
      Padding(
        padding: EdgeInsets.symmetric(horizontal: 30.w),
        child: Row(
          children: [
            FText("请上传身份证反面", size: 32.sp),
            Spacer(),
            GestureDetector(
              onTap: () => exampleBtn(2),
              child: FText(
                "查看示例",
                size: 32.sp,
                color: Color(0xFFBD12F9),
              ),
            ),
          ],
        ),
      ),
      cardCell(2),
      Padding(
        padding: EdgeInsets.symmetric(horizontal: 30.w),
        child: Row(
          children: [
            FText("请上传手持身份证照", size: 32.sp),
            Spacer(),
            GestureDetector(
              onTap: () => exampleBtn(3),
              child: FText(
                "查看示例",
                size: 32.sp,
                color: Color(0xFFBD12F9),
              ),
            ),
          ],
        ),
      ),
      cardCell(3),
      SizedBox(height: 150.h),
    ]);
  }

  Widget richTextContent() {
    List<InlineSpan> spans = [
      TextSpan(
        text: "*",
        style: TextStyle(color: Colors.red, fontSize: 32.sp),
      ),
      TextSpan(
        text: "所填内容必须保证为本人的真实信息，提交后无法修改",
        style: TextStyle(color: Colors.white, fontSize: 26.sp),
      ),
    ];

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 30.w),
      child: Text.rich(TextSpan(children: spans), textScaleFactor: 1),
    );
  }

  Widget inputField(bool isName) {
    return FContainer(
      height: 88.w,
      color: Color(0xFF777272).withOpacity(0.2),
      radius: BorderRadius.circular(88.w),
      margin: EdgeInsets.fromLTRB(60.w, 0, 60.w, 20.h),
      padding: EdgeInsets.symmetric(horizontal: 40.w),
      child: Row(children: [
        FText(isName ? "姓名：" : "身份证号：", size: 32.sp),
        SizedBox(width: 10.w),
        Expanded(child: textField(isName))
      ]),
    );
  }

  Widget textField(bool isName) {
    InputBorder border =
        OutlineInputBorder(borderSide: BorderSide(color: Colors.transparent));
    List<TextInputFormatter>? formatter;
    if (isName) {
      formatter = [
        FilteringTextInputFormatter.allow(
            RegExp(r'[-_\u4E00-\u9FA5a-zA-Z0-9\u2006]'))
      ];
    }
    return TextField(
      key: Key(isName ? "Name" : "Card"),
      controller: isName ? editingName : editingCard,
      style: TextStyle(color: Colors.white, fontSize: 30.sp),
      inputFormatters: formatter,
      maxLength: 18,
      decoration: InputDecoration(
        enabledBorder: border,
        focusedBorder: border,
        contentPadding: EdgeInsets.zero,
        hintStyle: TextStyle(color: Color(0xFF999999), fontSize: 30.sp),
        hintText: isName ? "请输入您的真实姓名" : "请输入您的身份证号",
        counterText: "",
      ),
      
    );
  }

  Widget cardCell(int index) {
    Widget btn = Consumer<AuthProvider>(builder: (_, pro, __) {
      String image = pro.getCardPath(index);
      ImageProvider? backImage;
      if (image.isEmpty) {
        backImage = ImageUtils().getAssetsImage("set/card$index");
      } else {
        backImage = FileImage(File(image));
      }

      return GestureDetector(
        onTap: () => onTapCell((value) => pro.updateCardPath(index, value)),
        child: FContainer(
          margin: EdgeInsets.symmetric(vertical: 20.h),
          width: 560.w,
          radius: BorderRadius.circular(20.w),
          height: 352.w,
          image: backImage,
          imageFit: BoxFit.cover,
          foreColor: Colors.black.withOpacity(0.3),
          foreRadius: BorderRadius.circular(20.w),
          align: Alignment.center,
          child: LoadAssetImage("set/picture", width: 120.w),
        ),
      );
    });

    return Center(child: btn);
  }

  final ImagePicker picker = ImagePicker();

  void onTapCell(Function(String) callback) async {
    bool accept = await FuncUtils().requestPermission(Permission.photos);
    if (!accept) return;
    // List<SelectImage>? images =
    //     await WidgetUtils().pushPage(PhotosPage(needCrop: true));
    XFile? file = await picker.pickImage(source: ImageSource.gallery);
    if (file == null || file.path.isEmpty) return;
    CroppedFile? crop = await ImageCropper().cropImage(
      sourcePath: file.path,
      compressQuality: 100,
      uiSettings: [
        AndroidUiSettings(toolbarTitle: '裁剪', statusBarColor: Colors.white),
        IOSUiSettings(title: '裁剪')
      ],
    );
    if (crop == null || crop.path.isEmpty) return;
    callback(crop.path);
    if (!mounted) return;
    setState(() {});
  }

  void exampleBtn(int exampleNum) {
    String image = "set/example$exampleNum";
    WidgetUtils().showFDialog(
      FContainer(
        radius: BorderRadius.circular(20.w),
        width: 540.w,
        margin: EdgeInsets.symmetric(vertical: 380.h, horizontal: 60.w),
        height: 645.w,
        color: Colors.white,
        child: Column(children: [
          Container(
            margin: EdgeInsets.symmetric(vertical: 30.h),
            child: FText("证件上传示例", color: Colors.black, size: 38.sp),
          ),
          Spacer(),
          FContainer(
            margin: EdgeInsets.symmetric(vertical: 20.h),
            width: 540.w,
            radius: BorderRadius.circular(20.w),
            height: 385.w,
            image: ImageUtils().getAssetsImage(image),
            imageFit: BoxFit.fill,
            // foreColor: Colors.black.withOpacity(0.3),
            // foreRadius: BorderRadius.circular(20.w),
            align: Alignment.center,
          ),
          Spacer(),
          Container(
            width: 540.w,
            height: 1,
            color: Colors.grey[100],
          ),
          GestureDetector(
            onTap: () => WidgetUtils().popPage(),
            child: FContainer(
              height: 120.w,
              width: 540.w,
              align: Alignment.center,
              child: FText("知道了", color: Color(0xFF3498E3), size: 34.sp),
            ),
          ),
        ]),
      ),
    );
  }
}
