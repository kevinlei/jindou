
import 'package:echo/pages/person/auth/provider/AliyunFaceProvider.dart';
import 'package:flutter/material.dart';
import 'package:echo/import.dart';


class AuthPage2 extends StatefulWidget {
  final String nameText;
  final String cardText;
  final int useCount;
  final int initpage;
  const AuthPage2({
    Key? key,
    required this.nameText,
    required this.cardText,
    required this.useCount,
    required this.initpage,
  }) : super(key: key);

  @override
  _AuthPage2State createState() => _AuthPage2State();
}

class _AuthPage2State extends State<AuthPage2> {
  PageController pageCtr = PageController();
  AuthProvider provider = AuthProvider();
  AliyunFaceProvider aliyunprovider = AliyunFaceProvider();

  @override
  void initState() {
    super.initState();
    provider.nameText = widget.nameText;
    provider.cardText = widget.cardText;
    provider.autoCount = widget.useCount;
    pageCtr = PageController(initialPage: widget.initpage);
    provider.curPage = widget.initpage;
    // provider.initAliveSDK();
    // aliyunprovider.getMetaInfos("", "");

  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => provider,
        builder: (_, __) {
          return Scaffold(
            backgroundColor: Color(0xFF1F2234),
           
            appBar: CommonAppBar(
              title: "快捷认证",
              textColor: Colors.white,
              elevation: 1,
              backgroundColor: Color(0xFF1F2234),
            ),
            body: AuthAuto(),
            bottomNavigationBar: buttonBottom(),
          );
        });
  }

  @override
  void dispose() {
    pageCtr.dispose();
    super.dispose();
  }

  Widget buttonBottom() {
    return Material(
      color: Colors.transparent,
      child: Consumer<AuthProvider>(builder: (_, pro, __) {
        String text = "";
        if (pro.isChecking || pro.isConfirm) {
          text = "正在检测";
        } else if (pro.curPage == 0) {
          text = "点击检测";
        } else {
          text = "立即提交";
        }
        return FLoginButton(text,isActivate: true,radius: 90.w, onPress: pro.onTapConfirm);
      }),
    );
    // return Material(
    //   color: Colors.transparent,
    //   child: FLoginButton('点击检测',isActivate: true,radius: 90.w, onPress: aliyunprovider.startVerify),
      
    // );
  }
}
