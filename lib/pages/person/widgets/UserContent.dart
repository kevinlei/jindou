import 'package:echo/utils/ext/string_ext.dart';
import 'package:echo/widgets/my_widgets.dart';
import 'package:echo/widgets/myfont_weight.dart';
import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:flutter/services.dart';

import '../homepage/PersonHomepage.dart';

class UserContent extends StatelessWidget {
  const UserContent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget content = Consumer<PersonProvider>(builder: (_, pro, __) {
      String name = UserConstants.userginInfo?.info.name ?? "";
      String id = UserConstants.userginInfo?.info.displayId.toString() ?? "";
      int sex = UserConstants.userginInfo?.info.sex ?? 1;
      int bir = UserConstants.userginInfo?.info.birthday ??
          DateTime.now().millisecondsSinceEpoch;
      int idLevel = UserConstants.userginInfo?.info.displayIdLevel ?? 0;

      return myColumn(children: [
        myRow(
            width: MATCH_PARENT,
            // height: 160.w,
            children: [
              userHead(),
              mySpaceWidth(16.w),
              Expanded(
                child: myColumn(children: [
                  userName(name),
                  myRow(
                      margin: EdgeInsets.only(top: 16.w),
                      children: [
                        myColumn(children: [
                          myRow(children: [
                            myLeftText("ID:${id ?? '--'}",
                                color: '#FF222222'.toColor(), fontSize: 24.sp),
                            copyUserId(id),
                          ], mainAxisSize: MainAxisSize.min),
                          mySpaceHeight(24.w),
                          myRow(children: [
                            ConfigManager().userWealthCharmIcon(
                                UserConstants.userginInfo?.gradeInfo?.userGrade
                                    ?.wealth?.badge ??
                                    '',
                                UserConstants.userginInfo?.gradeInfo?.userGrade
                                    ?.wealth?.grade ??
                                    0,
                                marginRight: 5.w,
                                isWealth: true),
                            ConfigManager().userWealthCharmIcon(
                                UserConstants.userginInfo?.gradeInfo?.userGrade
                                    ?.charm?.badge ??
                                    '',
                                UserConstants.userginInfo?.gradeInfo?.userGrade
                                    ?.charm?.grade ??
                                    0,
                                marginRight: 5.w),
                          ], mainAxisSize: MainAxisSize.min),
                        ], mainAxisSize: MainAxisSize.min),
                        Flexible(
                          child: myGestureDetector(
                              child: myRow(
                                  height: 54.w,
                                  decoration: BoxDecoration(
                                      color: '#FFFF753F'.toColor(),
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(100.w),
                                          bottomLeft: Radius.circular(100.w))),
                                  children: [
                                    myRightText('查看主页',
                                        fontSize: 24.sp, color: Colors.white),
                                    myIcon(
                                      iconPath:
                                      'person/arrow_right_white_main_page',
                                      iconWidth: 20.w,
                                    )
                                  ],
                                  mainAxisSize: MainAxisSize.min,
                                  padding: EdgeInsets.only(
                                    left: 24.w,
                                    right: 10.w,
                                  )),
                              onTap: () {
                                WidgetUtils().pushPage(UserPage(),
                                    fun: () => pro.callRefresh());
                              }),
                        ),
                      ],
                      mainAxisAlignment: MainAxisAlignment.spaceBetween),
                ], mainAxisSize: MainAxisSize.min),
              ),
            ],
            padding: EdgeInsets.only(
              left: 30.w,
            )),
        myColumn(
            children: [
              myGestureDetector(
                  child: myRow(
                    padding: EdgeInsets.symmetric(horizontal: 24.w),
                    decoration: BoxDecoration(
                        color: '#FFF58559'.toColor(),
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(24.w),
                            topRight: Radius.circular(24.w))),
                    children: [
                      myLeftText('我的账户',
                          fontSize: 32.sp,
                          fontWeight: MyFontWeight.medium,
                          color: Colors.white),
                      mySizeIcon('public/arrow_right_white', size: 48.w),
                    ],
                    height: 80.w,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  ),
                  onTap: () {
                    WidgetUtils()
                        .pushPage(Mymoney(), fun: () => pro.callRefresh());
                  }),
              myRow(
                children: [
                  UserCountCell(CountListType.FRIENDS, pro.countFriend),
                  _line(),
                  UserCountCell(CountListType.FOLLOW, pro.countFollow),
                  _line(),
                  UserCountCell(CountListType.FANS, pro.countFans),
                  // UserCountCell(CountListType.FANS, pro.countFans, homePageCtr),
                  // UserCountCell(CountListType.FRIENDS, pro.countFriend, homePageCtr),
                  _line(),
                  UserCountCell(CountListType.VISIT, pro.countVisit),
                  // UserCountCell(CountListType.VISIT, pro.countVisit, homePageCtr),
                ],
                height: 146.w,
                decoration: const BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      Color(0xFFFFFFFF),
                      Color(0x00FFFFFF),
                    ],
                  ),
                ),
              ),
            ],
            mainAxisSize: MainAxisSize.min,
            margin: EdgeInsets.only(top: 48.w, left: 30.w, right: 30.w)),
      ], mainAxisSize: MainAxisSize.min);
      /*    return FContainer(
        color: Colors.transparent,
        margin: EdgeInsets.fromLTRB(25.w, 20.w, 25.w, 0),
        radius: BorderRadius.circular(28.w),
        padding: EdgeInsets.fromLTRB(20.w, 20.w, 20.w, 20.w),
        child: Column(children: [
          FContainer(
            padding: EdgeInsets.only(
              left: 180.w,
            ),
            child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
              userName(name),
              SizedBox(
                width: 8.w,
              ),
              // userAge(sex, bir),
            ]),
          ),
          FContainer(
            padding: EdgeInsets.only(left: 170.w),
            margin: EdgeInsets.only(bottom: 30.w),
            child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
              idLevel != 0 ? BeNum(id) : userId(id),
              copyUserId(id),
              const Spacer(),
              editUserInfo(pro),
            ]),
          ),
          SizedBox(
            height: 40.h,
          ),
          myColumn(children: [
            myRow(
              padding: EdgeInsets.symmetric(horizontal: 24.w),
              decoration: BoxDecoration(
                  color: '#FFF58559'.toColor(),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(24.w),
                      topRight: Radius.circular(24.w))),
              children: [
                myLeftText('我的账户',
                    fontSize: 32.sp,
                    fontWeight: MyFontWeight.medium,
                    color: Colors.white),
                mySizeIcon('public/arrow_right_white', size: 48.w),
              ],
              height: 80.w,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
            ),
            myRow(
              children: [
                UserCountCell(CountListType.FRIENDS, pro.countFriend),
                _line(),
                UserCountCell(CountListType.FOLLOW, pro.countFollow),
                _line(),
                UserCountCell(CountListType.FANS, pro.countFans),
                // UserCountCell(CountListType.FANS, pro.countFans, homePageCtr),
                // UserCountCell(CountListType.FRIENDS, pro.countFriend, homePageCtr),
                _line(),
                UserCountCell(CountListType.VISIT, pro.countVisit),
                // UserCountCell(CountListType.VISIT, pro.countVisit, homePageCtr),
              ],
              height: 146.h,
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Color(0xFFFFFFFF),
                    Color(0x00FFFFFF),
                  ],
                ),
              ),
            ),
          ], mainAxisSize: MainAxisSize.min),
        ]),
      );*/
    });
    return content;
    /*return Stack(
        alignment: Alignment.topCenter,
        clipBehavior: Clip.none,
        children: [
          content,
          */ /*Positioned(
            left: 70.w,
            top: 40.w,
            child: userHead(),
          )*/ /*
        ]);*/
  }

  Widget _line() {
    return Container(
      color: '#1A262730'.toColor(),
      height: 48.w,
      width: 2.w,
    );
  }

  Widget userName(String name) {
    return myLeftText(
      name,
      fontWeight: MyFontWeight.medium,
      fontSize: 36.sp,
      color: Color(0xFF262730),
    );
  }

  Widget userHead() {
    Widget headWidget = Consumer<PersonProvider>(builder: (_, pro, __) {
      String head = UserConstants.userginInfo?.info.headIcon ?? "";
      if (head.isNotEmpty) {
        head = head + OssThumb.OSS_IM_SML.path;
      }
      return FContainer(
        width: 160.w,
        height: 160.w,
        radius: BorderRadius.circular(200.w),
        border: Border.all(
            color: const Color(0xFFA89CC5).withOpacity(0.5), width: 0.w),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(200.w),
          child: LoadNetImage(NetConstants.ossPath + head),
        ),
      );
    });

    return GestureDetector(onTap: onPressHead, child: headWidget);
  }

  /* Widget userAge(int sex, int bir) {
    // 2 是女
    return FContainer(
      height: 26.w,
      image: sex == 0
          ? ImageUtils().getAssetsImage("person/girlBg")
          : ImageUtils().getAssetsImage("person/manBg"),
      imageFit: BoxFit.fitWidth,
      child: Row(
        children: [
          SizedBox(width: 10.w),
          SizedBox(width: 10.w),
          FText("${UserConstants.getUserAge(bir)}",
              color:
                  sex == 0 ? const Color(0xFFFE5592) : const Color(0xFF648FFF),
              size: 16.sp),
          SizedBox(width: 10.w),
        ],
      ),
    );
  }*/

  Widget editUserInfo(PersonProvider pro) {
    return GestureDetector(
      child: FContainer(
        margin: EdgeInsets.only(left: 10.w, right: 10.w),
        align: Alignment.center,
        child: Row(
          children: [
            FText("查看主页", color: Colors.black.withOpacity(0.5), size: 24.sp),
            Icon(
              Icons.arrow_forward_ios_outlined,
              color: Colors.black.withOpacity(0.5),
              size: 20.w,
            )
          ],
        ),
      ),
      onTap: () {
        WidgetUtils().pushPage(UserPage(), fun: () => pro.callRefresh());
      },
    );
  }

  Widget userId(String id) {
    return FContainer(
      margin: EdgeInsets.only(
        left: 10.w,
        right: 10.w,
        top: 5.h,
      ),
      align: Alignment.center,
      child: FText("ID:$id", color: Colors.black.withOpacity(0.4), size: 24.sp),
    );
  }

  // 复制用户ID
  Widget copyUserId(String id) {
    return GestureDetector(
      child: FContainer(
        margin: EdgeInsets.only(left: 6.w),
        align: Alignment.center,
        child: mySizeIcon(
          "public/copy",
          size: 24.w,
        ),
      ),
      onTap: () {
        Clipboard.setData(ClipboardData(text: id));
        WidgetUtils().showToast("ID复制成功");
      },
    );
  }

  void onPressHead() {
    WidgetUtils().pushPage(
        PersonHomepage(userId: UserConstants.userginInfo?.info.userId ?? ""),
        fun: () => PersonProvider().callRefresh());
  }
}
