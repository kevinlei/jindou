import 'package:echo/import.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';

class BanerWage extends StatefulWidget {
  final String url;
  const BanerWage({Key? key, required this.url}) : super(key: key);

  @override
  _BanerWageState createState() => _BanerWageState();
}

class _BanerWageState extends State<BanerWage> {
  bool isLoading = true;
  bool hasClick = false;
  String linkUrl = "";

  @override
  void initState() {
    super.initState();
    String userId = UserConstants.userginInfo?.info.userId ?? "";
    String? prurlt = UserConstants.userginInfo?.info.headIcon ?? "";
    String prurl = NetConstants.ossPath + prurlt;
    if (AppConstants.accessToken != null) {
      linkUrl =
          "${widget.url}?openid=${userId}&nickname=未登录用户&avatar = ${prurl}";
    } else {
      linkUrl =
          "${widget.url}?openid=${userId}&nickname=${UserConstants.userginInfo?.info.name}&avatar = ${prurl}";
    }

    print("客服链接$linkUrl");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const CommonAppBar(title: "在线客服"),
      body: Stack(alignment: Alignment.center, fit: StackFit.expand, children: [
        gameContent(),
        loadingCard(),
      ]),
    );
  }

  Widget gameContent() {
    InAppWebViewOptions options = InAppWebViewOptions(
        transparentBackground: true, clearCache: true, cacheEnabled: false);

    return InAppWebView(
      initialOptions: InAppWebViewGroupOptions(
          //iOS平台配置
          ios: IOSInAppWebViewOptions(
            allowsInlineMediaPlayback: true,
          ),
          crossPlatform: options),
      initialUrlRequest: URLRequest(url: Uri.parse(linkUrl)),
      onProgressChanged: onGameLoad,
      onWebViewCreated: onWebViewCreated,
      gestureRecognizers: null,
    );
  }

  Widget loadingCard() {
    return Visibility(
        visible: isLoading,
        child: const Center(child: FLoading(textColor: Colors.white)));
  }

  void onGameLoad(ctr, int value) {
    if (value / 100 <= 0.999) return;
    isLoading = false;
    if (!mounted) return;
    setState(() {});
  }

  void onWebViewCreated(ctr) {
    ctr.addJavaScriptHandler(handlerName: "closeGame", callback: onGameClose);
  }

  void onGameClose(arg) async {
    if (hasClick) return;
    WidgetUtils().popPage();
    hasClick = true;
  }
}
