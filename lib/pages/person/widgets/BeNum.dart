import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:getwidget/getwidget.dart';
import 'package:shimmer_animation/shimmer_animation.dart';

class BeNum extends StatelessWidget {
  final String id;

  const BeNum( this.id, {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: AlignmentDirectional.centerStart,
      children: [
        FContainer(
          imageFit: BoxFit.fill,
          imageAlign: Alignment.topCenter, 
          image: ImageUtils().getAssetsImage("person/beNum"),
          margin: EdgeInsets.only(left: 20.w, right: 10.w),
          padding:
              EdgeInsets.only(left: 43.w, right: 20.w, top: 5.h, bottom: 5.h),
          align: Alignment.center,
          child: GFShimmer(
            child: FText("ID:$id", size: 22.sp),
            duration: const Duration(milliseconds: 2990),
            showGradient: true,
            gradient: LinearGradient(colors: [Colors.red, Colors.blue, Colors.white,
              Colors.red, Colors.blue, Colors.white]),
          )
        ),
        FContainer(
          child: Stack(
            alignment: AlignmentDirectional.center,
            children: [
              // LoadAssetImage("person/xz", width: 60.w, height: 80.w,),
              LoadAssetImage("person/liang", width: 60.w, height: 80.w,),
              // Shimmer(
              //   colorOpacity: 1,
              //   duration: const Duration(seconds: 5),
              //   child: LoadAssetImage("person/liang", width: 28.w, height: 31.w,),
              // ),
            ],
          ),
        ),
      ],
    );
  }
}


