class ShareData {
  // 分享地址
  String shareUrl = "";
  // 分享标题
  String shareTitle = "";
  // 分享副标题
  String shareSub = "";
  // 分享缩略图地址
  String thumbUrl = "";

  /// 分享平台 1: 会话 2：朋友圈
  int platform = 1;

  ShareData();

  Map<String, dynamic> toJson() {
    assert(shareUrl.isNotEmpty);
    assert(shareTitle.isNotEmpty);
    assert(shareSub.isNotEmpty);
    assert(platform == 1 || platform == 2);
    return {
      "webUrl": shareUrl,
      "webTitle": shareTitle,
      "subTitle": shareSub,
      "thumbUrl": thumbUrl,
      "platform": platform,
    };
  }
}

class PayFuYouData {
  String  mchntCd  = "";
  String  orderDate  = "";
  String  orderAmt  = "";
  String  orderId  = "";
  String  pageNotifyUrl  = "";
  String  backNotifyUrl = "";
  String  goodsName  = "";
  String  goodsDetail  = "";
  String  orderTmStart  = "";
  String  orderTmEnd  = "";
  String  orderToken  = "";
  String  appScheme = "";
  String  payType = "";

  Map<String, dynamic> toJson() {
    assert(mchntCd.isNotEmpty);
    assert(orderDate.isNotEmpty);
    assert(orderAmt.isNotEmpty);
    assert(orderId.isNotEmpty);
    assert(orderTmStart.isNotEmpty);
    assert(goodsName.isNotEmpty);
    assert(goodsDetail.isNotEmpty);
    assert(orderTmEnd.isNotEmpty);
    assert(orderToken.isNotEmpty);
    assert(payType.isNotEmpty);

    return {
      "mchntCd": mchntCd,
      "orderDate": orderDate,
      "orderAmt": orderAmt,
      "orderId": orderId,
      "pageNotifyUrl": pageNotifyUrl,
      "backNotifyUrl": backNotifyUrl,
      "goodsName": goodsName,
      "goodsDetail": goodsDetail,
      "orderTmStart": orderTmStart,
      "orderTmEnd": orderTmEnd,
      "orderToken": orderToken,
      "appScheme": appScheme,
      "payType": payType,
    };
  }
}

class PayWeChatData {
  String appId = "";
  String partnerId = "";
  String prepayId = "";
  String nonceStr = "";
  String timeStamp = "";
  String sign = "";

  Map<String, dynamic> toJson() {
    assert(appId.isNotEmpty);
    assert(partnerId.isNotEmpty);
    assert(prepayId.isNotEmpty);
    assert(nonceStr.isNotEmpty);
    assert(timeStamp.isNotEmpty);
    assert(sign.isNotEmpty);

    return {
      "appId": appId,
      "partnerId": partnerId,
      "prepayId": prepayId,
      "nonceStr": nonceStr,
      "timeStamp": timeStamp,
      "sign": sign,
    };
  }
}

class LocationResult {
  // key
  String clientKey = "";
  // 经度
  double latitude = 0.0;
  // 纬度
  double longitude = 0.0;
  // 省
  String province = "";
  // 城市
  String city = "";
  // 城市编号
  String cityCode = "";
  // 城区
  String district = "";
  // 街道
  String street = "";
  // 门牌号
  String streetNum = "";

  LocationResult(Map? map) {
    this.clientKey = map?["clientKey"] ?? "";
    this.latitude = map?["lat"] ?? 0.0;
    this.longitude = map?["lon"] ?? 0.0;
    this.province = map?["province"] ?? "";
    this.city = map?["city"] ?? "";
    this.cityCode = map?["cityCode"] ?? "";
    this.district = map?["district"] ?? "";
    this.street = map?["street"] ?? "";
    this.streetNum = map?["streetNum"] ?? "";
  }

  String toString() {
    return "clientKey：$clientKey | latitude：$latitude | longitude：$longitude | "
        "province：$province | city：$city | cityCode：$cityCode | "
        "district：$district | street：$street | streetNum：$streetNum";
  }
}

class LocationSearch {
  // title
  String title = "";
  // 经度
  String latitude = "0";
  // 纬度
  String longitude = "0";
  // 省
  String province = "";
  // 城市
  String city = "";
  // 城市编号
  String cityCode = "";
  // adCode
  String adCode = "";
  // 地址
  String address = "";

  LocationSearch(Map? map) {
    this.title = map?["title"] ?? "";
    this.latitude = map?["lat"] ?? "0";
    this.longitude = map?["lon"] ?? "0";
    this.province = map?["province"] ?? "";
    this.city = map?["city"] ?? "";
    this.cityCode = map?["cityCode"] ?? "";
    this.adCode = map?["adCode"] ?? "";
    this.address = map?["address"] ?? "";
  }

  String toString() {
    return "title：$title | latitude：$latitude | longitude：$longitude | "
        "province：$province | city：$city | cityCode：$cityCode | "
        "adCode：$adCode | address：$address";
  }
}
