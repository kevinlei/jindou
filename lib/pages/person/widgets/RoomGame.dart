import 'package:echo/import.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';

class RoomGame extends StatefulWidget {
  final String url;
  final int? magicBoxId;
  const RoomGame({Key? key, required this.url, this.magicBoxId})
      : super(key: key);

  @override
  _RoomGameState createState() => _RoomGameState();
}

class _RoomGameState extends State<RoomGame> {
  bool isLoading = true;
  bool hasClick = false;
  String linkUrl = "";

  @override
  void initState() {
    super.initState();
    int roomId = AppManager().roomInfo.roomMode?.exteriorRoomId ?? 0;
    String userId = UserConstants.userginInfo?.info.userId ?? "";
    linkUrl =
        "${widget.url}?user_id=$userId&id=${widget.magicBoxId}&roomId=$roomId&token=${AppConstants.accessToken}";
    print("游戏连接$linkUrl");
  }

  @override
  Widget build(BuildContext context) {
    return Stack(alignment: Alignment.center, fit: StackFit.expand, children: [
      gameContent(),
      loadingCard(),
    ]);
  }

  Widget gameContent() {
    InAppWebViewOptions options = InAppWebViewOptions(
        transparentBackground: true, clearCache: true, cacheEnabled: false);

    return InAppWebView(
      initialOptions: InAppWebViewGroupOptions(
          //iOS平台配置
          ios: IOSInAppWebViewOptions(
            allowsInlineMediaPlayback: true,
          ),
          crossPlatform: options),
      initialUrlRequest: URLRequest(url: Uri.parse(linkUrl)),
      onProgressChanged: onGameLoad,
      onWebViewCreated: onWebViewCreated,
      gestureRecognizers: null,
    );
  }

  Widget loadingCard() {
    return Visibility(
        visible: isLoading,
        child: const Center(child: FLoading(textColor: Colors.white)));
  }

  void onGameLoad(ctr, int value) {
    if (value / 100 <= 0.999) return;
    isLoading = false;
    if (!mounted) return;
    setState(() {});
  }

  void onWebViewCreated(ctr) {
    ctr.addJavaScriptHandler(handlerName: "closeGame", callback: onGameClose);
  }

  void onGameClose(arg) async {
    if (hasClick) return;
    WidgetUtils().popPage();
    hasClick = true;
  }
}
