import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class BannerPage extends StatelessWidget {
  final String title;
  final String image;

  const BannerPage({Key? key, required this.title, required this.image})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CommonAppBar(title: title),
      body: SingleChildScrollView(
        child: Image.network(NetConstants.ossPath + image)
        // LoadNetImage(NetConstants.ossPath + image, showHolder: false),
      ),
    );
  }
}
