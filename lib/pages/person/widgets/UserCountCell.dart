import 'package:flutter/material.dart';
import 'package:echo/import.dart';

import '../../../widgets/my_widgets.dart';
import '../../../widgets/myfont_weight.dart';

class UserCountCell extends StatelessWidget {
  final CountListType listType;
  final int count;
  // const UserCountCell(this.listType, this.count, {Key? key})
  //     : super(key: key);
  const UserCountCell(this.listType, this.count, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String countText = "$count";
    if (count >= 10000) {
      countText = "${(count / 10000).toStringAsFixed(1)}w";
    }
    return Expanded(
        child: GestureDetector(onTap: () => onTapCount(listType), child: myColumn(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            FText(
              countText,
              weight: MyFontWeight.medium,
              size: 36.sp,
              color: Colors.black,
            ),
            mySpaceHeight(10.w),
            FText(listType.text, color: const Color(0xFF9F9FB9), size: 24.sp),
          ],
          mainAxisSize: MainAxisSize.min,
        )));
  }

  void onTapCount(CountListType type) {
    WidgetUtils().pushPage(RelationPage(type));
  }
}
