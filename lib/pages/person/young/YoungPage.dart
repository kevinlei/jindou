import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class YoungPage extends StatefulWidget {
  const YoungPage({Key? key}) : super(key: key);

  @override
  _YoungPageState createState() => _YoungPageState();
}

class _YoungPageState extends State<YoungPage> {
  @override
  void initState() {
    super.initState();
    initYouthState();
  }

  void initYouthState() async {
    DioUtils().asyncHttpRequest(
      NetConstants.getbaseInfo,
      method: DioMethod.GET,
      onSuccess: updateInfo,
    );
  }

  void updateInfo(response) {
    if (response == null) return;
    UserConstants.userginInfo?.info.teenmode = response['teen_mode'];
    EventUtils().emit(EventConstants.E_MQ_ADOMODE, null);
    if (!mounted) return;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFF5F6F7),
      appBar: const CommonAppBar(
        title: "青少年模式",
        textColor: Colors.black,
        elevation: 1,
        backgroundColor: Color(0xFFF5F6F7),
      ),
      body: ListView(
        padding: EdgeInsets.fromLTRB(45.w, 120.h, 45.w, 0),
        children: [
          LoadAssetImage("person/youngBg", width: 200.w, height: 200.w),
          SizedBox(height: 40.h),
          Center(
              child: FText(
                  UserConstants.userginInfo?.info.teenmode == 1
                      ? "青少年模式已开启"
                      : "青少年模式未开启",
                  size: 36.sp,
                  color: Colors.black,
                  weight: FontWeight.bold)),
          SizedBox(height: 80.h),
          Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
            // Padding(
            //   padding: EdgeInsets.only(top: 10.w, right: 40.w),
            //   child: CircleAvatar(
            //     radius: 10.w,
            //     backgroundColor: Color(0xFFA852FF),
            //   ),
            // ),
            Expanded(
              child: FText("1.青少年模式开启后，用户只能使用适用于青少年使用的功能，且无法进行充值、购买道具等操作",
                  maxLines: 10,
                  size: 32.sp,
                  color: Colors.black.withOpacity(0.3)),
            )
          ]),
          SizedBox(height: 40.h),
          // Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
          //   // Padding(
          //   //   padding: EdgeInsets.only(top: 10.w, right: 40.w),
          //   //   child: CircleAvatar(
          //   //     radius: 10.w,
          //   //     backgroundColor: Color(0xFFA852FF),
          //   //   ),
          //   // ),
          //   Expanded(
          //     child: FText("2.每日22时至次日6时期间无法使用鸣音",
          //         maxLines: 10, size: 32.sp, color: Colors.white.withOpacity(0.3)),
          //   )
          // ]),
          // SizedBox(height: 40.h),
          Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
            // Padding(
            //   padding: EdgeInsets.only(top: 10.w, right: 40.w),
            //   child: CircleAvatar(
            //     radius: 10.w,
            //     backgroundColor: Color(0xFFA852FF),
            //   ),
            // ),
            Expanded(
              child: FText(
                  "2.青少年模式是金豆派对为促进青少年健康成长做出的一次尝试，"
                  "我们会不断的探索和优化更多的场景，以便促进青少年儿童的身心健康发展",
                  maxLines: 10,
                  size: 32.sp,
                  color: Colors.black.withOpacity(0.3)),
            )
          ]),
        ],
      ),
      bottomNavigationBar: buttonBottom(),
    );
  }

  Widget buttonBottom() {
    return Column(mainAxisSize: MainAxisSize.min, children: [
      // TextButton(
      //   onPressed: onTapReset,
      //   child: FText("修改密码", size: 24.sp, color: const Color(0xFF999999)),
      // ),
      // SizedBox(height: 10.h),
      // FLoginButton(hasOpen ? "关闭青少年模式" : "开启青少年模式",isActivate: true, radius: 90.w, onPress: onTapNext),

      FcationButton(
          UserConstants.userginInfo?.info.teenmode == 1 ? "关闭青少年模式" : "开启青少年模式",
          isActivate: true,
          radius: 90.w,
          onPress: onTapNext),
      SizedBox(height: 40.h),
    ]);
  }

  void onTapReset() {
    WidgetUtils()
        .pushPage(const YoungSetPwd(isFirst: false), fun: initYouthState);
  }

  void onTapNext() async {
    // if (hasOpen) {
    //   // 输入密码关闭
    //   await WidgetUtils().pushPage(const YoungResetPwd(), fun: initYouthState);
    // } else {
    //   if (hasPwd) {
    //     // 输入密码开启
    //     await WidgetUtils()
    //         .pushPage(const YoungResetPwd(), fun: initYouthState);
    //   } else {
    //     // 设置密码
    //     WidgetUtils()
    //         .pushPage(const YoungSetPwd(isFirst: true), fun: initYouthState);
    //   }
    // }
    if (UserConstants.userginInfo?.info.teenmode == 1) {
      // 输入密码关闭
      await WidgetUtils().pushPage(const YoungResetPwd(), fun: initYouthState);
    } else {
      // 输入密码开启
      WidgetUtils()
          .pushPage(const YoungSetPwd(isFirst: true), fun: initYouthState);
    }
  }
}
