import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:flutter/services.dart';

class YoungInput extends StatefulWidget {
  final Function(String) onInput;
  final bool? obscureText;
  const YoungInput({Key? key, required this.onInput, this.obscureText = true})
      : super(key: key);

  @override
  _YoungInputState createState() => _YoungInputState();
}

class _YoungInputState extends State<YoungInput> {
  String inputText = "";
  final double cellSize = 112.w;

  @override
  Widget build(BuildContext context) {
    InputBorder border =
        OutlineInputBorder(borderSide: BorderSide(color: Colors.transparent));
    return Center(
      child: FContainer(
        height: cellSize,
        width: cellSize * 4 + 3 * 40.w,
        child: Stack(
          alignment: Alignment.center,
          children: [
            Row(
                children: List.generate(4, (index) => cellItem(index)),
                mainAxisAlignment: MainAxisAlignment.spaceBetween),
            TextField(
              showCursor: false,
              autofocus: true,
              maxLength: 4,
              onChanged: onTextInput,
              keyboardType: TextInputType.number,
              inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              style: TextStyle(color: Colors.transparent, fontSize: 40.sp),
              decoration: InputDecoration(
                enabledBorder: border,
                focusedBorder: border,
                counterText: "",
                contentPadding: EdgeInsets.zero,
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget cellItem(int index) {
    String text = inputText.length > index ? inputText[index] : "";
    return FContainer(
      width: cellSize,
      height: cellSize,
      radius: BorderRadius.circular(16.w),
      color: Colors.white.withOpacity(0.4),
      align: Alignment.center,
      child: FText((widget.obscureText == true && text.isNotEmpty) ? "*" : text,
          nullLength: 0, size: 40.sp, color: Color(0xFFA852FF)),
    );
  }

  void onTextInput(String value) {
    inputText = value;
    widget.onInput(value);
    if (!mounted) return;
    setState(() {});
  }
}
