import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class YoungRetrieve extends StatelessWidget {
  const YoungRetrieve({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF5F6F7),
      appBar: CommonAppBar(
          title: "找回密码",
        textColor: Colors.black,
        elevation: 1,
        backgroundColor: Color(0xFFF5F6F7),
      ),
      body: ListView(
        padding: EdgeInsets.fromLTRB(25.w, 40.h, 25.w, 0),
        children: [
          FText(
              "若您需要重置青少年模式的密码，请您发送申请至邮箱1696197@qq.com，主题为【ID+青少年密码重置】，"
              "邮件中请上传您本人的手持身份证照片，要求本人、身份证在同一照片中，且人脸和身份证字迹清晰可变，您的"
              "身份证仅用于密码重置申诉，金豆派对不会泄露您的个人信息，并且会尽快为您处理！",
              maxLines: 20,
              size: 32.sp,color: Colors.black,),
        ],
      ),
      bottomNavigationBar: FLoginButton("确认",isActivate: true,radius: 90.w, onPress: onTapConfirm),
    );
  }

  void onTapConfirm() {
    WidgetUtils().popPage();
  }
}
