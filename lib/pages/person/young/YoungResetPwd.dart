import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class YoungResetPwd extends StatefulWidget {
  const YoungResetPwd({Key? key}) : super(key: key);

  @override
  _YoungResetPwdState createState() => _YoungResetPwdState();
}

class _YoungResetPwdState extends State<YoungResetPwd> {
  String password = "";

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        backgroundColor: const Color(0xFFF5F6F7),
        appBar: const CommonAppBar(
          title: "输入密码",
          textColor: Colors.black,
          elevation: 1,
          backgroundColor: Color(0xFFF5F6F7),
        ),
        body: ListView(
          padding: EdgeInsets.only(top: 40.h),
          children: [
            const Center(
                child: FText(
              "请输入4位监护密码",
              color: Colors.black,
            )),
            SizedBox(height: 40.h),
            YoungInput(onInput: onTextInput),
            SizedBox(height: 40.h),
            // TextButton(
            //   onPressed: onTapForget,
            //   child: FText("修改密码", size: 24.sp, color: Color(0xFF999999)),
            // )
          ],
        ),
        bottomNavigationBar: FcationButton("确认",
            isActivate: true, radius: 90.w, onPress: onTapConfirm),
      ),
    );
  }

  void onTextInput(value) => password = value;

  void onTapForget() {
    WidgetUtils().pushPage(const YoungRetrieve());
  }

  void onTapConfirm() async {
    if (password.length < 4) {
      return WidgetUtils().showToast("请输入正确的密码");
    } else {
      initYouthState();
    }
  }

  void initYouthState() async {
    DioUtils().asyncHttpRequest(
      NetConstants.getchangeInfo,
      params: {
        "teen_password": password,
        "change_type": 0,
      },
      method: DioMethod.POST,
      onSuccess: updateInfo,
    );
  }

  void updateInfo(response) {
    UserConstants.userginInfo?.info.teenmode = 0;
    EventUtils().emit(EventConstants.E_MQ_ADOMODE, null);
    WidgetUtils().popPage();
    if (!mounted) return;
    setState(() {});
  }
}
