import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class YoungSetPwd extends StatefulWidget {
  final bool isFirst;
  final String? oldPassword;
  const YoungSetPwd({Key? key, required this.isFirst, this.oldPassword})
      : super(key: key);

  @override
  _YoungSetPwdState createState() => _YoungSetPwdState();
}

class _YoungSetPwdState extends State<YoungSetPwd> {
  String password1 = "";
  String password2 = "";

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        backgroundColor: const Color(0xFFF5F6F7),
        appBar: CommonAppBar(title: widget.isFirst ? "青少年模式" : "输入新密码"),
        body: ListView(
          padding: EdgeInsets.only(top: 80.h),
          children: [
            Center(
                child: FText(
              widget.isFirst ? "设置4位数字监护密码" : "输入4位数字监护密码",
              color: Colors.black,
            )),
            SizedBox(height: 40.h),
            YoungInput(onInput: onTextInput1),
            SizedBox(height: 120.h),
            const Center(child: FText("请再次输入监护密码", color: Colors.black)),
            SizedBox(height: 40.h),
            YoungInput(onInput: onTextInput2),
          ],
        ),
        bottomNavigationBar: FContainer(
          padding: EdgeInsets.only(bottom: 40.h),
          // child: FsetButton("确认", radius: 90.w, onPress: onTapConfirm),
          child: FcationButton("确认",
              isActivate: true, radius: 90.w, onPress: onTapConfirm),
        ),
      ),
    );
  }

  void onTextInput1(value) => password1 = value;

  void onTextInput2(value) => password2 = value;

  void onTapConfirm() async {
    if (password1.length != 4 && password1 is int) {
      return WidgetUtils().showToast("请输入正确的密码");
    }
    if (password1 != password2) {
      return WidgetUtils().showToast("请确认您的密码");
    }
    initYouthState();
  }

  void initYouthState() async {
    DioUtils().asyncHttpRequest(
      NetConstants.getchangeInfo,
      params: {
        "teen_password": password2,
        "change_type": 1,
      },
      method: DioMethod.POST,
      onSuccess: updateInfo,
    );
  }

  void updateInfo(response) {
    UserConstants.userginInfo?.info.teenmode = 1;
    EventUtils().emit(EventConstants.E_MQ_ADOMODE, null);
    WidgetUtils().popPage();
    if (!mounted) return;
    setState(() {});
  }
}
