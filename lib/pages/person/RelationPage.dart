import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:echo/import.dart';

import 'homepage/PersonHomepage.dart';

class RelationPage extends StatefulWidget {
  final CountListType listType;
  const RelationPage(this.listType, {Key? key}) : super(key: key);

  @override
  _RelationPageState createState() => _RelationPageState();
}

class _RelationPageState extends State<RelationPage> {
  List userList = [];
  int curPage = 1;
  bool open = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFF5F6F7),
      appBar: CommonAppBar(
        title: "我的${widget.listType.text}",
        textColor: Colors.black,
        elevation: 1,
        backgroundColor: const Color(0xFFF5F6F7),
      ),
      body: FRefreshLayout(
        onRefresh: callRefresh,
        onLoad: callLoad,
        emptyWidget: userList.isEmpty ? const FEmptyWidget() : null,
        child: ListView.builder(
          padding: EdgeInsets.fromLTRB(25.w, 40.h, 25.w, 0),
          itemBuilder: (_, index) => userCell(userList[index]),
          itemExtent: 120.w,
          itemCount: userList.length,
        ),
      ),
    );
  }

  Widget userCell(data) {
    String userHead = data["head_icon"] ?? "";
    if (userHead.isNotEmpty) {
      userHead =
          "$userHead?x-oss-process=image/resize,m_fill,w_100,h_100/quality,q_60";
    }
    return FContainer(
      border: Border(
          bottom: BorderSide(color: Colors.black.withOpacity(0.02), width: 1)),
      child: Row(children: [
        GestureDetector(
          onTap: () {
            WidgetUtils().pushPage(PersonHomepage(userId: data["user_id"]));
          },
          child: CircleAvatar(
              radius: 40.w,
              // backgroundImage:
              // CachedNetworkImageProvider(NetConstants.ossPath + userHead),
              child: ClipRRect(
                //是ClipRRect，不是ClipRect
                borderRadius: BorderRadius.circular(40.w),
                child: CachedNetworkImage(
                  imageUrl: NetConstants.ossPath + userHead,
                  placeholder: (context, url) =>
                      const CircularProgressIndicator(),
                  errorWidget: (context, url, error) => const Icon(Icons.error),
                ),
              )
              // backgroundColor: Color(0xFFBBBBBB),
              ),
        ),
        SizedBox(width: 20.w),
        Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(children: [
                ConstrainedBox(
                  constraints: BoxConstraints(maxWidth: 260.w),
                  child: FText(
                    data["name"],
                    color: Colors.black,
                  ),
                ),
                userAge(data["sex"], data["birthday"] ?? 0)
              ]),
              FText("ID：${data["display_id"]}",
                  size: 24.sp, color: const Color(0xFF999999))
            ]),
        const Spacer(),
        userSuffix(data["user_id"], time: data["update_time"] ?? 0)
      ]),
    );
  }

  Widget userAge(int sex, int bir) {
    if (bir == 0) return const SizedBox();
    return FContainer(
      width: 70.w,
      height: 30.w,
      margin: EdgeInsets.only(left: 10.w),
      padding: EdgeInsets.symmetric(horizontal: 10.w),
      color: sex == 0 ? const Color(0xFFFFE5E5) : const Color(0xFFE5EFFF),
      radius: BorderRadius.circular(30.w),
      child: Row(children: [
        LoadAssetImage(sex == 0 ? "person/sex2" : "person/sex1",
            width: 20.w, height: 20.w),
        FText("${UserConstants.getUserAge(bir)}",
            size: 23.sp,
            color: sex == 0 ? const Color(0xFFFF617F) : const Color(0xFF43A0FF))
      ]),
    );
  }

  Widget userSuffix(String userId, {int time = 0}) {
    switch (widget.listType) {
      case CountListType.FOLLOW:
        Widget btn = FSizeButton(
          "取消关注",
          width: 125.w,
          height: 50.w,
          txtSize: 24.sp,
          isActivate: true,
          onPress: () => onTapUnFollow(userId),
        );
        return btn;
      case CountListType.FANS:
        Widget btn = FSizeButton(
          "回粉",
          width: 125.w,
          height: 50.w,
          txtSize: 24.sp,
          isActivate: true,
          onPress: () => onTapFollow(userId),
        );
        return btn;
      case CountListType.FRIENDS:
        Widget btn = FSizeButton(
          "互相关注",
          width: 125.w,
          height: 50.w,
          txtSize: 24.sp,
          isActivate: true,
          onPress: () => onTapUnFollow(userId),
        );
        return btn;
      case CountListType.VISIT:
        return FText(
            DateTime.fromMillisecondsSinceEpoch(time)
                .toString()
                .substring(0, 19),
            color: Colors.black,
            size: 22.sp);
    }
  }

  void onTapUnFollow(String userId) {
    DioUtils().asyncHttpRequest(
      NetConstants.unFollow,
      method: DioMethod.POST,
      params: {"flow_user_id": userId, "flow_action": 20},
      onSuccess: (_) => onRequest(userId),
    );
  }

  void onTapFollow(String userId) {
    DioUtils().asyncHttpRequest(
      NetConstants.followUser,
      method: DioMethod.POST,
      params: {"flow_user_id": userId, "flow_action": 10},
      onSuccess: (_) => onRequest(userId),
    );
  }

  void onRequest(response) {
    userList.removeWhere((e) => e["user_id"] == response);
    if (!mounted) return;
    setState(() {});
  }

  Future<void> callRefresh() async {
    curPage = 1;
    open = true;
    requestList(true);
  }

  Future<void> callLoad() async {
    if (!open) return;
    curPage += 1;
    requestList(false);
  }

  void requestList(bool isRefresh) {
    String netUrl = "";
    String mode = "10";
    DioMethod method = DioMethod.GET;
    switch (widget.listType) {
      case CountListType.FOLLOW:
        netUrl = NetConstants.followList;
        mode = "10";
        break;
      case CountListType.FANS:
        netUrl = NetConstants.followList;
        method = DioMethod.GET;
        mode = "20";
        break;
      case CountListType.FRIENDS:
        netUrl = NetConstants.followList;
        mode = "30";
        break;
      case CountListType.VISIT:
        netUrl = NetConstants.visitorList;
        mode = "20";
        break;
    }
    if (netUrl.isEmpty) return;
    DioUtils().asyncHttpRequest(
      netUrl,
      method: method,
      loading: false,
      params: {
        "page": curPage,
        "mode": mode,
        "page_size": AppConstants.pageSize
      },
      onSuccess: isRefresh ? onRefresh : onLoad,
    );
  }

  void onRefresh(response) {
    List lists = response?["list"] ?? [];
    if (response?["list"] == null || lists.isEmpty) {
      open = false;
    } else {
      userList = lists;
    }
    if (!mounted) return;
    setState(() {});
  }

  void onLoad(response) {
    List lists = response?["list"] ?? [];
    if (response?["list"] == null || lists.isEmpty) {
      open = false;
    } else {
      userList.addAll(lists);
    }
    if (!mounted) return;
    setState(() {});
  }
}
