import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class RoomCreate extends StatefulWidget {
  final EnterRoomType roomType;

  const RoomCreate(this.roomType, {Key? key})
      : super(key: key);

  @override
  _RoomCreateState createState() => _RoomCreateState();
}

class _RoomCreateState extends State<RoomCreate> {
  // 房间名称
  String roomName = "";
  // 房间封面
  String roomCover = "";
  // 房间标签
  int roomTip = -1;
  // 房间类型
  bool isOpen = true;
  // 房间密码
  String roomPwd = "";

  AuthStatus authStatus = AuthStatus.NULL;

  @override
  void initState() {
    super.initState();

    getUserInfo();
    requestAuthStatus();
  }

  @override
  Widget build(BuildContext context) {
    return  GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap:  () => FocusManager.instance.primaryFocus?.unfocus(),
      child: FContainer(
          padding: EdgeInsets.only(left: 15.w, right: 15.w, bottom: 20.w),
          image: ImageUtils().getAssetsImage("public/huibg"),
          imageFit: BoxFit.fill,
          imageAlign: Alignment.topCenter,
          child: SafeArea(child:Scaffold(
            appBar: CommonAppBar(
              title: "创建房间",
              textColor: Colors.black,
              elevation: 0,
              backgroundColor: Colors.transparent,
            ),
            backgroundColor:Colors.transparent,
            body: SafeArea(
              child: ListView(
                  padding: EdgeInsets.fromLTRB(25.w, 20.h, 25.w, 0),
                  children: [
                    roomNameEdit(),
                    SizedBox(height: 60.w),
                    Visibility(child: FText("房间封面", size: 32.sp,color: Colors.black,), visible: true),
                    Visibility(child: RoomCover(onCoverInput), visible: true),
                    FText("房间标签", size: 32.sp,color: Colors.black,),
                    FText("加上标签能吸引更多的人哦~", size: 20.sp, color: Color(0xFFBBBBBB)),
                    SizedBox(height: 30.w),
                    RoomTips(widget.roomType, onTipsSelected),
                    SizedBox(height: 40.w),
                    FText("房间类型", size: 32.sp,color: Colors.black,),
                    FText("公开房间能吸引更多的人哦~", size: 20.sp, color: Color(0xFFBBBBBB)),
                    SizedBox(height: 30.w),
                    RoomOpen(onTypeSelected, onPwdInput),
                    SizedBox(height: 20.w),
                  ]),
            ),
            bottomNavigationBar: createButton(),
          ),
          )),
    );
  }

  Widget roomNameEdit() {
    Widget roomName = TextField(
      maxLength: 10,
      style: TextStyle(color: Colors.black, fontSize: 30.sp),
      // inputFormatters: [FilteringTextInputFormatter.deny(RegExp(r"[\s]"))],
      onChanged: onNameInput,
      decoration: InputDecoration(
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.transparent)),
        focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.transparent)),
        contentPadding: EdgeInsets.zero,
        hintText: "请为房间取一个昵称",
        hintStyle: TextStyle(color: Color(0xFF999999), fontSize: 28.sp),
        counterText: "",
      ),
    );

    return SizedBox(
      height: 45.h,
      child: Row(children: [
        FText("房间名称", size: 32.sp,color: Colors.black,),
        SizedBox(width: 20.w),
        Expanded(child: roomName)
      ]),
    );
  }

  Widget createButton() {
    return SafeArea(
        child: FcationButton("立即创建",
            isActivate: true, radius: 60.w, onRequest: onTapCreate));
  }

  void onNameInput(value) => roomName = value;

  void onCoverInput(value) => roomCover = value;

  void onTipsSelected(tip) => roomTip = tip;

  void onTypeSelected(value) => isOpen = value;

  void onPwdInput(value) => roomPwd = value;

  void requestAuthStatus() {
    DioUtils()
        .asyncHttpRequest(NetConstants.authStatus, onSuccess: onAuthStatus);
  }

  void onAuthStatus(response) {
    if (response == null) return;
    authStatus = getCheckStatus(response["audit_status"]);
  }

  AuthStatus getCheckStatus(int? status) {
    for (AuthStatus item in AuthStatus.values) {
      if (item.value == status) return item;
    }
    return AuthStatus.NULL;
  }

  bool checkInput() {
    if (roomName.isEmpty) {
      WidgetUtils().showToast("请为房间取一个昵称");
      return false;
    }
    if (widget.roomType == EnterRoomType.ROOM_CLUB && roomCover.isEmpty) {
      WidgetUtils().showToast("请为房间取设置一个封面");
      return false;
    }
    if (roomTip == -1) {
      WidgetUtils().showToast("请为房间设置一个标签");
      return false;
    }
    if (!isOpen && roomPwd.length < 4) {
      WidgetUtils().showToast("请为私密房间设置密码");
      return false;
    }
    return true;
  }

  void getUserInfo() async {
    await DioUtils().httpRequest(NetConstants.getUserInfo,
        method: DioMethod.GET, params: {"userId": ""}, onSuccess: (response) {
          if (response == null) return;
          UserConstants.userginInfo = LoginUserModel.fromJson(response);
        });
  }

  Future<void> onTapCreate() async {
    int role = UserConstants.userginInfo?.info.userRole ?? UserRole.DEF.index;
    if (role < UserRole.OFFICIAL.index &&
        authStatus != AuthStatus.AUTO_S &&
        authStatus != AuthStatus.MANUAL_S) {
      return onTapAuth();
      // return WidgetUtils().showToast("请先前往实名认证");
    }

    if (!checkInput()) return;
    WidgetUtils().showLoading();
    // if (widget.roomType == EnterRoomType.ROOM_PRIVATE) {
    //   bool lawfulTxt = await FuncUtils()
    //       .checkTextLawful(roomName, CheckTxtType.TXT_ROOM_NAME);
    //   if (!lawfulTxt) return WidgetUtils().cancelLoading();
    // }

    // if (widget.roomType == EnterRoomType.ROOM_CLUB) {}
    String? image = "";
    image = await FuncUtils().requestUpload(roomCover, UploadType.CLUB_COVER);
    if (image == null) return WidgetUtils().cancelLoading();
    if (image.isEmpty) {
      WidgetUtils().showToast("上传图片失败");
      return WidgetUtils().cancelLoading();
    }
    await requestCreate(image);
    WidgetUtils().cancelLoading();
  }

  Future<void> requestCreate(String image) async {
    String clubId = UserConstants.userginInfo?.info.userId ?? "";
    await DioUtils().httpRequest(
      NetConstants.roomCreate,
      method: DioMethod.POST,
      params: {
        "room_type":2,
        "room_name": roomName,
        "room_topic_id": roomTip,
        "room_image": image,
        "room_password": roomPwd,
        "ex_guild_id": clubId,
      },
      onSuccess: onCreate,
      // onError: (_, __) => WidgetUtils().popPage(),
    );
  }

  void onCreate(response) async {
    if (response == null) return;
    WidgetUtils().showToast("公会房间创建申请中，请等待处理");
    WidgetUtils().popPage();
  }

  void onTapAuth() async {
    bool? accept =
    await WidgetUtils().showAlert("请前往金豆派对进行实名认证.", confirmText: '前往实名');
    if (accept == null || !accept) return;
    await WidgetUtils().pushPage(AuthPage1());
  }
}
