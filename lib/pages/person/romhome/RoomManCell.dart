import 'package:echo/import.dart';
import 'package:flutter/material.dart';

import 'model/room_manages_model.dart';

class RoomManCell extends StatelessWidget {
  final RoomManagesModel model;
  const RoomManCell(this.model, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget content = FContainer(
      color: const Color(0xFF222222).withOpacity(0.1),
      margin: EdgeInsets.fromLTRB(25.w, 0, 25.w, 20.w),
      radius: BorderRadius.circular(16.w),
      padding: EdgeInsets.symmetric(horizontal: 20.w),
      child: Row(children: [
        headWidget(),
        SizedBox(width: 20.w),
        Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              FText(model.name, color: Colors.black),
              SizedBox(height: 20.w),
              Row(children: [
                roomTagContent(),
                SizedBox(width: 10.w),
                userWidget()
              ])
            ])
      ]),
    );
    return GestureDetector(onTap: onTapRoom, child: content);
  }

  Widget roomTagContent() {
    // if (model.enterType == EnterRoomType.ROOM_CLUB.value) {
    //   return LoadNetImage(NetConstants.ossPath + (model.image ?? ""),
    //       width: 82.w, height: 36.w);
    // }
    return FContainer(
      radius: BorderRadius.circular(30.w),
      height: 40.w,
      constraints: BoxConstraints(
        minWidth: 140.w,
        maxWidth: 200.w,
      ),
      padding: EdgeInsets.symmetric(horizontal: 10.w),
      border: Border.all(color: const Color(0xFF979797)),
      child: Row(mainAxisSize: MainAxisSize.min, children: [
        LoadAssetImage("person/charm", width: 25.w, format: ImageFormat.WEBP),
        SizedBox(width: 10.w),
        ConstrainedBox(
            constraints: BoxConstraints(maxWidth: 140.w),
            child: FText(
              model.name,
              size: 20.sp,
              overflow: TextOverflow.ellipsis,
              color: Colors.black,
            )),
        // FText(model.name , size: 20.sp,color: Colors.black,)
      ]),
    );
  }

  Widget headWidget() {
    String roomCover = "";
    // if (model.enterType == EnterRoomType.ROOM_PRIVATE.value) {
    //   roomCover = NetConstants.ossPath + (model.image);
    // } else if (model.enterType == EnterRoomType.ROOM_CLUB.value) {
    //   roomCover = NetConstants.ossPath + model.image;
    // }
    roomCover = NetConstants.ossPath + model.image;
    Widget lock = FContainer(
      width: 160.w,
      height: 160.w,
      radius: BorderRadius.circular(20.w),
      color: Colors.black.withOpacity(0.4),
      align: Alignment.center,
      child: LoadAssetImage("entertainment/lock3", width: 60.w),
    );

    return FContainer(
      width: 160.w,
      height: 160.w,
      radius: BorderRadius.circular(20.w),
      image: ImageUtils().getImageProvider(roomCover),
      imageFit: BoxFit.cover,
      align: Alignment.center,
      child: Visibility(visible: model.isFollow == 1, child: lock),
    );
  }

  Widget userWidget() {
    return FContainer(
      height: 40.w,
      constraints: BoxConstraints(minWidth: 90.w),
      border: Border.all(color: const Color(0xFF979797)),
      radius: BorderRadius.circular(40.w),
      child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        FText("${model.personNum}人",
            size: 22.sp, color: const Color(0xFF999999))
      ]),
    );
  }

  void onTapRoom() async {
    // WidgetUtils().pushPage(RoomPage(model.exRoomId));
    AppManager().joinRoom(model.exRoomId);
  }
}
