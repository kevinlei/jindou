import 'package:echo/import.dart';
import 'package:echo/utils/ext/string_ext.dart';
import 'package:echo/widgets/my_widgets.dart';
import 'package:echo/widgets/myfont_weight.dart';
import 'package:flutter/material.dart';

import 'model/room_manages_model.dart';

class RoomManList extends StatefulWidget {
  final RoomManageType manageType;

  const RoomManList(this.manageType, {Key? key}) : super(key: key);

  @override
  _RoomManListState createState() => _RoomManListState();
}

class _RoomManListState extends State<RoomManList> {
  int curPage = 1;

  List roomList = [];

  Map avatarMap = {};

  bool endList = false;

  @override
  Widget build(BuildContext context) {
    return FRefreshLayout(
      onRefresh: callRefresh,
      onLoad: callLoad,
      emptyWidget: emptyWidget(),
      child: ListView.builder(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemBuilder: (_, index) => RoomManCell(roomList[index]),
        itemCount: roomList.length,
        itemExtent: 220.h,
        padding: EdgeInsets.only(top: 40.w),
      ),
    );
  }

  Widget? emptyWidget() {
    if (roomList.isNotEmpty) return null;
    if (widget.manageType == RoomManageType.ROOM_SELF) {
      return myColumn(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            myCenterText(
              '您还没有创建房间呦',
              fontWeight: MyFontWeight.medium,
              color: '#FF222222'.toColor(),
              fontSize: 32.sp,
            ),
            mySpaceHeight(60.w),
            createButton()
          ],
          mainAxisSize: MainAxisSize.min);
    } else {
      return FEmptyWidget();
    }
  }

  Widget createButton() {
    Widget btn = FSizeButton(
      "创建房间",
      width: 288.w,
      height: 88.w,
      onPress: onTapCreate,
      txtSize: 32.sp,
      fontWeight: MyFontWeight.medium,
      isActivate: true,
    );
    return btn;
  }

  void onTapCreate() async {
    await WidgetUtils().pushPage(RoomCresspt());
    callRefresh();
  }

  Future<void> callRefresh() async {
    endList = false;
    curPage = 1;
    requestList(true);
  }

  Future<void> callLoad() async {
    if (widget.manageType == RoomManageType.ROOM_SELF || endList) return;
    curPage += 1;
    requestList(false);
  }

  void requestList(bool isRefresh) {
    DioUtils().asyncHttpRequest(
      NetConstants.roomPowerList,
      loading: false,
      params: {"page_offset": curPage, "room_flag": widget.manageType.index},
      onSuccess: isRefresh ? onRefresh : onLoad,
    );
  }

  void onRefresh(response) {
    roomList.clear();
    setState(() {});

    if (widget.manageType == RoomManageType.ROOM_SELF) {
      if (response["rooms"] == null) return;
      for (var data in response["rooms"]) {
        roomList.add(RoomManagesModel.fromJson(data));
      }
    } else {
      if (response?["avatarMap"] != null)
        avatarMap = response?["avatarMap"] ?? {};
      if (response?["rooms"].length != 0) {
        for (var element in response?["rooms"] ?? []) {
          RoomManagesModel model = RoomManagesModel.fromJson(element);
          // if (model.ownerHead == null) {
          //   model.ownerHead = avatarMap[model.ownerId];
          // }
          roomList.add(model);
        }
      } else {
        endList = true;
      }
    }
    if (!mounted) return;
    setState(() {});
  }

  void onLoad(response) {
    if (widget.manageType == RoomManageType.ROOM_SELF) {
      for (var data in response["rooms"]) {
        roomList.add(RoomManagesModel.fromJson(data));
      }
    } else {
      if (response?["avatarMap"] != null)
        avatarMap.addAll(response?["avatarMap"] ?? {});
      if (response?["rooms"].length != 0) {
        for (var element in response?["rooms"] ?? []) {
          RoomManagesModel model = RoomManagesModel.fromJson(element);
          if (model.owner.ownerId == null) {
            // model.owner.ownerId = avatarMap[owner.ownerId];
          }
          roomList.add(model);
        }
      } else {
        endList = true;
      }
    }
    if (!mounted) return;
    setState(() {});
  }
}
