import 'package:echo/import.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../../widgets/FContainer.dart';

class RoomInsputName extends StatefulWidget {
  final String origin;
  final Function(String) onNameInput;
  final int? maxLen;
  const RoomInsputName(this.origin, this.onNameInput, {Key? key, this.maxLen})
      : super(key: key);

  @override
  _RoomInsputNameState createState() => _RoomInsputNameState();
}


class _RoomInsputNameState extends State<RoomInsputName> {
  int count = 0;
  TextEditingController controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    controller.text = widget.origin;
    count = widget.origin.length;
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      margin: EdgeInsets.fromLTRB(0, 30.w, 0, 40.w),
      height: 75.w,
      padding: EdgeInsets.symmetric(horizontal: 20.w),
      radius: BorderRadius.circular(20.w),
      color: const Color(0xFF0F222222),
      child: Row(children: [Expanded(child: inputField()), countText()]),
    );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  Widget inputField() {
    InputBorder border = const OutlineInputBorder(
        borderSide: BorderSide(color: Colors.transparent));
    return TextField(
      maxLength: widget.maxLen ?? 10,
      onChanged: onTextInput,
      controller: controller,
      style: TextStyle(color: Colors.black, fontSize: 28.sp),
      // inputFormatters: [FilteringTextInputFormatter.deny(RegExp(r"[\s]"))],
      decoration: InputDecoration(
        enabledBorder: border,
        focusedBorder: border,
        hintText: "",
        contentPadding: EdgeInsets.zero,
        hintStyle: TextStyle(color: const Color(0xFF999999), fontSize: 28.sp),
        counterText: "",
      ),
    );
  }

  Widget countText() {
    return FRichText([
      FRichTextItem()
        ..text = "$count"
        ..color = const Color(0xFFBBBBBB),
      FRichTextItem()
        ..text = "/${widget.maxLen ?? 10}"
        ..color = const Color(0xFFBBBBBB),
    ], fontSize: 26.sp);
  }

  void onTextInput(String value) {
    count = value.length;
    widget.onNameInput(value);
    if (!mounted) return;
    setState(() {});
  }
}

class RoomNoticeInput extends StatefulWidget {
  final String origin;
  final String hintText;
  final Function(String) onTextInput;

  const RoomNoticeInput(this.origin, this.hintText, this.onTextInput,
      {Key? key})
      : super(key: key);

  @override
  _RoomNoticeInputState createState() => _RoomNoticeInputState();
}

class _RoomNoticeInputState extends State<RoomNoticeInput> {
  TextEditingController controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    controller.text = widget.origin;
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      height: 260.w,
      radius: BorderRadius.circular(16.w),
      padding: EdgeInsets.fromLTRB(20.w, 10.w, 20.w, 0),
      margin: EdgeInsets.fromLTRB(0, 30.w, 0, 40.w),
      color: const Color(0xFF0F222222),
      child: inputField(),
    );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  Widget inputField() {
    InputBorder border = const OutlineInputBorder(
        borderSide: BorderSide(color: Colors.transparent));
    return TextField(
      maxLength: 200,
      maxLines: 6,
      controller: controller,
      onChanged: widget.onTextInput,
      // inputFormatters: [FilteringTextInputFormatter.deny(RegExp(r"[\s]"))],
      style: TextStyle(color: Colors.black, fontSize: 28.sp),
      decoration: InputDecoration(
        enabledBorder: border,
        focusedBorder: border,
        hintText: widget.hintText,
        hintStyle: TextStyle(color: const Color(0xFFBBBBBB), fontSize: 28.sp),
        counterStyle:
        TextStyle(color: const Color(0xFFBBBBBB), fontSize: 26.sp),
        contentPadding: EdgeInsets.zero,
      ),
    );
  }
}
