import 'package:echo/import.dart';
import 'package:echo/pages/home/v2/widget/my_room_widget.dart';
import 'package:echo/utils/ext/state_ext.dart';
import 'package:echo/utils/ext/string_ext.dart';
import 'package:echo/utils/list_util.dart';
import 'package:echo/widgets/CommonCard.dart';
import 'package:flutter/material.dart';

import '../../../widgets/my_widgets.dart';
import '../../../widgets/myfont_weight.dart';
import 'model/room_manages_model.dart';

class MyRoomPage extends StatefulWidget {
  const MyRoomPage({Key? key}) : super(key: key);

  @override
  _MyRoomPageState createState() => _MyRoomPageState();
}

class _MyRoomPageState extends State<MyRoomPage> {
  bool _isLoad = false;

  @override
  void initState() {
    super.initState();
    _getCreateRoom();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CommonCard(
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          CardAppBar(title: "我的房间"),
          SizedBox(height: 40.h),

          /*Container(
            child: RoomManList(RoomManageType.ROOM_SELF),
            height: 300.h,
          ),*/
          if (_isLoad) showCreateRoom(),
          if (_isLoad)
            Expanded(
                child: Container(
              child: MyRoomWidget(),
              padding: EdgeInsets.only(left: 32.w, right: 32.w),
            )),
          // MyRoomWidget(/*homeController, tabController, index*/)

          // TabBar(
          //   isScrollable: true,
          //   labelStyle: TextStyle(fontWeight: FontWeight.w600, fontSize: 36.sp),
          //   unselectedLabelStyle:
          //   TextStyle(fontWeight: FontWeight.w400, fontSize: 28.sp),
          //   tabs: [
          //     Tab(text: "我的房间", height: 60.h),
          //     Tab(text: "收藏房间", height: 60.h),
          //     Tab(text: "管理房间", height: 60.h),
          //   ],
          //   controller: tabController,
          //   indicatorColor: Color(0xFFF3A6FF),
          //   indicatorSize: TabBarIndicatorSize.label,
          // ),

          /*  Expanded(
            child: TabBarView(
              controller: tabController,
              children: [
                RoomManList(RoomManageType.ROOM_SELF),
                // RoomManList(RoomManageType.ROOM_FAVORITE, widget.homePageCtr),
                // RoomManList(RoomManageType.ROOM_MANAGE, widget.homePageCtr),
              ],
            ),
          )*/
        ]),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  List roomList = [];

  void _getCreateRoom() {
    DioUtils().asyncHttpRequest(NetConstants.roomPowerList,
        loading: false,
        params: {"page_offset": 1, "room_flag": RoomManageType.ROOM_SELF.index},
        onSuccess: (dynamic response) {
      refresh(() {
        _isLoad = true;
        if (null == response || response["rooms"] == null) return;
        roomList.clear();
        for (var data in response["rooms"]) {
          roomList.add(RoomManagesModel.fromJson(data));
        }
      });
    }, onError: (int code, String msg) {
      refresh(() {
        _isLoad = true;
      });
    });
  }

  Widget showCreateRoom() {
    if (!ListUtil.isListHasData(roomList)) {
      return myColumn(
          width: MATCH_PARENT,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            mySpaceHeight(60.w),
            myCenterText(
              '您还没有创建房间呦',
              fontWeight: MyFontWeight.medium,
              color: '#FF222222'.toColor(),
              fontSize: 32.sp,
            ),
            mySpaceHeight(60.w),
            FSizeButton(
              "创建房间",
              width: 288.w,
              height: 88.w,
              onPress: onTapCreate,
              txtSize: 32.sp,
              fontWeight: MyFontWeight.medium,
              isActivate: true,
            ),
            mySpaceHeight(60.w),
          ],
          mainAxisSize: MainAxisSize.min);
    }

    return ListView.builder(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemBuilder: (_, index) => RoomManCell(roomList[index]),
      itemCount: roomList.length,
      itemExtent: 220.h,
      padding: EdgeInsets.only(top: 40.w),
    );
  }

  void onTapCreate() async {
    await WidgetUtils().pushPage(RoomCresspt());
    _getCreateRoom();
  }
}
