import 'package:echo/import.dart';
import 'package:f_verification_box/f_verification_box.dart';
import 'package:flutter/material.dart';

class RoomCresspt extends StatefulWidget {
  RoomCresspt({Key? key}) : super(key: key);
  @override
  State<RoomCresspt> createState() => _RoomCressptState();
}

class _RoomCressptState extends State<RoomCresspt> with WidgetsBindingObserver {
  LoginProvider provider = LoginProvider();
  ScrollController scroll = ScrollController();
  TextEditingController phoneCtr = TextEditingController();
  late int audit_status = 0;

  @override
  void initState() {
    super.initState();
    // WidgetsBinding.instance.addObserver(this);
    provider.decodePwd();

    callRefresh();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => provider,
        builder: (_, __) {
          return Scaffold(
            backgroundColor: const Color(0xFF0B0017),
            body: FContainer(
              padding: EdgeInsets.only(left: 30.w, right: 30.w, bottom: 20.w),
              image: ImageUtils().getAssetsImage("public/huibg"),
              imageFit: BoxFit.fill,
              imageAlign: Alignment.topCenter,
              child: SafeArea(child: loginContent()),
            ),
          );
        });
  }

  Widget loginContent() {
    return Column(children: [
      CommonAppBar(
        title: "创建房间",
        textColor: Colors.black,
        actions: [
          GestureDetector(
            onTap: () {},
            child: FContainer(
              width: 112.w,
              height: 64.w,
              image: ImageUtils().getAssetsImage("dynamic/dMore"),
            ),
          ),
        ],
      ),

      Expanded(child: inputContent()),
      // inputContent(),
      Column(
        children: [
          Consumer<LoginProvider>(builder: (_, pro, __) {
            return FcationButton(
              "创建房间",
              isActivate: audit_status == 1 ? true : false,
              padding: 40.w,
              onPress: audit_status == 1 ? creshow : null,
            );
          }),
        ],
      ),
    ]);
  }

  void creshow() async {
    WidgetUtils().showBottomSheet(const RoomCreshow(), color: Colors.white);
  }

  Widget inputContent() {
    return ListView(controller: scroll, children: [
      seIndrawalsent(),
      seInsent(),
    ]);
  }

  Widget seIndrawalsent() {
    return FContainer(
      height: 240 * 2.w,
      padding: EdgeInsets.all(30.w),
      radius: BorderRadius.circular(20.w),
      margin: EdgeInsets.symmetric(horizontal: 15.w, vertical: 20.w),
      color: Colors.black.withOpacity(0.1),
      child: Column(
        children: [
          FContainer(
            width: 190 * 2.w,
            height: 17 * 2.w,
            imageFit: BoxFit.fill,
            image: ImageUtils().getAssetsImage("person/banfan"),
          ),
          Expanded(
              child: Column(
            children: [
              SizedBox(height: 30.w),
              Expanded(
                child: FText(
                    "1.房主应履行对房间的监督、管理职责，并严格依据相关法律法规，规范房间成员的行为和发布的信息；\n"
                    "2.根据《网络游戏管理暂行办法》的要求，用户在未绑定手机号、及未完成实名认证的情況下，部分功能的使用将受到限制。",
                    maxLines: 10,
                    size: 32.sp,
                    color: Colors.black.withOpacity(0.3)),
              ),

              // chargwxButton(),
            ],
          )),
        ],
      ),
    );
  }

  Widget seInsent() {
    return FContainer(
      height: 156 * 2.w,
      padding: EdgeInsets.all(30.w),
      radius: BorderRadius.circular(20.w),
      margin: EdgeInsets.symmetric(horizontal: 15.w, vertical: 20.w),
      color: Colors.black.withOpacity(0.1),
      child: Column(
        children: [
          Expanded(
              child: Column(
            children: [
              FContainer(
                width: 190 * 2.w,
                height: 17 * 2.w,
                imageFit: BoxFit.fill,
                image: ImageUtils().getAssetsImage("person/banhang"),
              ),
              Expanded(
                child: Row(
                  children: [
                    LoadAssetImage("person/banipone",
                        width: 48.w, height: 48.w),
                    FText("绑定手机号",
                        maxLines: 10,
                        size: 32.sp,
                        color: Colors.black.withOpacity(0.3)),
                    Expanded(
                      child: Container(),
                    ),
                    chargiponeButton(),
                  ],
                ),
              ),
              Row(
                children: [
                  LoadAssetImage("person/banshi", width: 48.w, height: 48.w),
                  FText("完成实名认证",
                      maxLines: 10,
                      size: 32.sp,
                      color: Colors.black.withOpacity(0.3)),
                  Expanded(
                    child: Container(),
                  ),
                  chargwxButton(),
                ],
              ),
            ],
          )),
        ],
      ),
    );
  }

  Widget chargiponeButton() {
    Widget btn = FContainer(
      align: Alignment.centerRight,
      child: Row(
        children: [
          FText(UserConstants.userginInfo?.info.phone ?? "",
              maxLines: 10, size: 32.sp, color: Colors.black.withOpacity(0.3)),
          LoadAssetImage("person/balk", width: 40.w, height: 40.w),
        ],
      ),
    );
    return GestureDetector(onTap: onTaponphonecomeeail, child: btn);
  }

  Widget chargwxButton() {
    Widget btn = FContainer(
      align: Alignment.centerRight,
      child: Row(
        children: [
          FText(audit_status == 1 ? "认证完成" : "去绑定",
              maxLines: 10, size: 32.sp, color: Colors.black.withOpacity(0.3)),
          LoadAssetImage("person/balk", width: 40.w, height: 40.w),
        ],
      ),
    );
    return GestureDetector(onTap: onTaponmonecomeeail, child: btn);
  }

  //手机号认证
  void onTaponphonecomeeail() {}
  //点击箭头
  void onTaponmonecomeeail() {
    if (audit_status == 1) {
    } else {
      WidgetUtils().pushPage(AuthPage1(), fun: callRefresh);
    }
  }

  Future<void> callRefresh() async {
    WidgetUtils().showLoading();
    DioUtils().asyncHttpRequest(NetConstants.authStatus,
        method: DioMethod.GET, onSuccess: onAuthStatus);
  }

  void onAuthStatus(response) {
    if (response == null) return;
    audit_status = response["audit_status"];
    print('object');
    print(response["audit_status"]);
    print(audit_status);
    if (!mounted) return;
    WidgetUtils().cancelLoading();
    setState(() {});
    if (!mounted) return;
    WidgetUtils().cancelLoading();
    setState(() {});
  }

  @override
  void didChangeMetrics() {
    super.didChangeMetrics();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      if (!mounted) return;
      double keyboardHeight = MediaQuery.of(context).viewInsets.bottom;
      if (keyboardHeight <= 0) return;
      double max = scroll.position.maxScrollExtent;
      scroll.animateTo(max,
          duration: const Duration(milliseconds: 200), curve: Curves.linear);
      Future.delayed(const Duration(milliseconds: 200), () {
        double max2 = scroll.position.maxScrollExtent;
        scroll.animateTo(max2,
            duration: const Duration(milliseconds: 1), curve: Curves.linear);
      });
    });
  }

  @override
  void dispose() {
    scroll.dispose();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }
}
