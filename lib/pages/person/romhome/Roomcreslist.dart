import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class Roomcreslist extends StatefulWidget {
  final Function(int) onSelect;
  const Roomcreslist(this.onSelect, {Key? key}) : super(key: key);

  @override
  _RoomcreslistState createState() => _RoomcreslistState();
}

class _RoomcreslistState extends State<Roomcreslist> {
  List<RoomTypes> chargeList = [];
  int chargeNum = 0;

  @override
  void initState() {
    super.initState();
    requestChargeMap();
  }

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 4,
        crossAxisSpacing: 20.w,
        mainAxisSpacing: 20.w,
        mainAxisExtent: 200.w,
      ),
      itemCount: chargeList.length,
      physics: const NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemBuilder: (_, int index) =>
          // chargeCell(keys[index], chargeList[keys[index]]),
          chargeCell(chargeList[index].topicName.toString(), index),
    );
  }

  Widget chargeCell(String key, int value) {
    Color colorBg = value == chargeNum
        ? const Color(0xFF1AFB63B3)
        : const Color(0xFF0D222222);
    Color colorBorder =
        value == chargeNum ? const Color(0xFFFF753F) : Colors.transparent;
    Widget child =
        Column(mainAxisAlignment: MainAxisAlignment.center, children: [
      Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          FContainer(
            width: 80.w,
            height: 80.w,
            image: ImageUtils().getImageProvider(
                NetConstants.ossPath + chargeList[value].topicIcon.toString(),
                holderImg: "logo"),
            imageFit: BoxFit.fill,
          ),
          SizedBox(height: 30.w),
          FText(
            key,
            size: 24.sp,
            color: const Color(0xFF222222).withOpacity(0.5),
          ),
        ],
      ),
    ]);

    Widget btn = FContainer(
      radius: BorderRadius.circular(20.w),
      color: colorBg,
      border: Border.all(color: colorBorder),
      child: child,
    );
    return GestureDetector(onTap: () => onTapCharge(value), child: btn);
  }

//获取type接口
  void requestChargeMap() {
    DioUtils().asyncHttpRequest(
      NetConstants.getelation,
      params: {"pid": 30},
      onSuccess: onChargeMap,
    );
  }

  void onChargeMap(response) {
    response ??= [];
    chargeList.clear();
    for (var data in response) {
      chargeList.add(RoomTypes.fromJson(data));
    }
    if (!mounted) return;
    setState(() {});
  }

  void onTapCharge(int num) {
    if (chargeNum == num) return;
    chargeNum = num;
    widget.onSelect(num);
    if (!mounted) return;
    setState(() {});
  }
}
