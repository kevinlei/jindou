import 'package:json_annotation/json_annotation.dart';

part 'aubents_model.g.dart';

@JsonSerializable()
class AubentsModel {
  @JsonKey(name: 'quity_detail')
  final String? quityDetail;
  @JsonKey(name: 'quity_icon')
  final String? quityIcon;
  @JsonKey(name: 'quity_name')
  final String? quityName;

  const AubentsModel({
    this.quityDetail,
    this.quityIcon,
    this.quityName,
  });

  factory AubentsModel.fromJson(Map<String, dynamic> json) =>
      _$AubentsModelFromJson(json);

  Map<String, dynamic> toJson() => _$AubentsModelToJson(this);
}
