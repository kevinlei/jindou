// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'aubents_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AubentsModel _$AubentsModelFromJson(Map<String, dynamic> json) => AubentsModel(
      quityDetail: json['quity_detail'] as String?,
      quityIcon: json['quity_icon'] as String?,
      quityName: json['quity_name'] as String?,
    );

Map<String, dynamic> _$AubentsModelToJson(AubentsModel instance) =>
    <String, dynamic>{
      'quity_detail': instance.quityDetail,
      'quity_icon': instance.quityIcon,
      'quity_name': instance.quityName,
    };
