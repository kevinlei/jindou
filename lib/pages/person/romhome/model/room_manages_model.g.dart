// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'room_manages_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RoomManagesModel _$RoomManagesModelFromJson(Map<String, dynamic> json) =>
    RoomManagesModel(
      exRoomId: json['ex_room_id'] as int,
      name: json['name'] as String,
      topicName: json['topic_name'] as String,
      hotNum: json['hot_num'] as int,
      personNum: json['person_num'] as int,
      image: json['image'] as String,
      owner: Owner.fromJson(json['owner'] as Map<String, dynamic>),
      usersImage: (json['users_image'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
      isFollow: json['is_follow'] as int,
    );

Map<String, dynamic> _$RoomManagesModelToJson(RoomManagesModel instance) =>
    <String, dynamic>{
      'ex_room_id': instance.exRoomId,
      'name': instance.name,
      'topic_name': instance.topicName,
      'hot_num': instance.hotNum,
      'person_num': instance.personNum,
      'image': instance.image,
      'owner': instance.owner,
      'users_image': instance.usersImage,
      'is_follow': instance.isFollow,
    };

Owner _$OwnerFromJson(Map<String, dynamic> json) => Owner(
      ownerName: json['owner_name'] as String,
      ownerId: json['owner_id'] as int,
      ownerImage: json['owner_image'] as String,
    );

Map<String, dynamic> _$OwnerToJson(Owner instance) => <String, dynamic>{
      'owner_name': instance.ownerName,
      'owner_id': instance.ownerId,
      'owner_image': instance.ownerImage,
    };
