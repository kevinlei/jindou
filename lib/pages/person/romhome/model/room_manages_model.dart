import 'package:json_annotation/json_annotation.dart';

part 'room_manages_model.g.dart';

@JsonSerializable()
class RoomManagesModel {
  @JsonKey(name: 'ex_room_id')
  final int exRoomId;
  final String name;
  @JsonKey(name: 'topic_name')
  final String topicName;
  @JsonKey(name: 'hot_num')
  final int hotNum;
  @JsonKey(name: 'person_num')
  final int personNum;
  final String image;
  final Owner owner;
  @JsonKey(name: 'users_image')
  final List<String> usersImage;
  @JsonKey(name: 'is_follow')
  final int isFollow;

  const RoomManagesModel({
   required  this.exRoomId,
    required this.name,
    required this.topicName,
    required this.hotNum,
    required this.personNum,
    required this.image,
    required  this.owner,
    required this.usersImage,
    required this.isFollow,
  });

  factory RoomManagesModel.fromJson(Map<String, dynamic> json) =>
      _$RoomManagesModelFromJson(json);

  Map<String, dynamic> toJson() => _$RoomManagesModelToJson(this);
}

@JsonSerializable()
class Owner {
  @JsonKey(name: 'owner_name')
  final String ownerName;
  @JsonKey(name: 'owner_id')
  final int ownerId;
  @JsonKey(name: 'owner_image')
  final String ownerImage;

  const Owner({
   required this.ownerName,
    required this.ownerId,
    required this.ownerImage,
  });

  factory Owner.fromJson(Map<String, dynamic> json) =>
      _$OwnerFromJson(json);

  Map<String, dynamic> toJson() => _$OwnerToJson(this);
}
