import 'package:echo/import.dart';
import 'package:echo/widgets/CommonCard.dart';
import 'package:flutter/material.dart';

class PersonRoom extends StatefulWidget {

  const PersonRoom( {Key? key}) : super(key: key);

  @override
  _PersonRoomState createState() => _PersonRoomState();
}

class _PersonRoomState extends State<PersonRoom>
    with SingleTickerProviderStateMixin {
  late TabController tabController;

  @override
  void initState() {
    super.initState();
    tabController = TabController(length: 1, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CommonCard(
        child: Column(children: [
          CardAppBar(title: "我的房间"),
          SizedBox(height: 40.h),
          // TabBar(
          //   isScrollable: true,
          //   labelStyle: TextStyle(fontWeight: FontWeight.w600, fontSize: 36.sp),
          //   unselectedLabelStyle:
          //   TextStyle(fontWeight: FontWeight.w400, fontSize: 28.sp),
          //   tabs: [
          //     Tab(text: "我的房间", height: 60.h),
          //     Tab(text: "收藏房间", height: 60.h),
          //     Tab(text: "管理房间", height: 60.h),
          //   ],
          //   controller: tabController,
          //   indicatorColor: Color(0xFFF3A6FF),
          //   indicatorSize: TabBarIndicatorSize.label,
          // ),
          Expanded(
            child: TabBarView(
              controller: tabController,
              children: [
                RoomManList(RoomManageType.ROOM_SELF),
                // RoomManList(RoomManageType.ROOM_FAVORITE, widget.homePageCtr),
                // RoomManList(RoomManageType.ROOM_MANAGE, widget.homePageCtr),
              ],
            ),
          )
        ], crossAxisAlignment: CrossAxisAlignment.start),
      ),
    );
  }

  AppBar manageAppbar() {
    return AppBar(
      title: FText('房间', size: 36.sp, weight: FontWeight.bold),
      bottom: PreferredSize(
        preferredSize: Size.fromHeight(50.h),
        child: TabBar(
          labelStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 32.sp),
          labelColor: Colors.black,
          unselectedLabelStyle:
          TextStyle(fontWeight: FontWeight.normal, fontSize: 32.sp),
          unselectedLabelColor: Color(0xFF999999),
          tabs: [
            Text("我的房间", textScaleFactor: 1.0),
            Text("收藏房间", textScaleFactor: 1.0),
            Text("管理房间", textScaleFactor: 1.0)
          ],
          controller: tabController,
          indicatorColor: Color(0xFFA852FF),
          indicatorSize: TabBarIndicatorSize.label,
        ),
      ),
    );
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }
}

