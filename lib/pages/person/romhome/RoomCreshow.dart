import 'package:echo/import.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class RoomCreshow extends StatefulWidget {
  const RoomCreshow({
    Key? key,
  }) : super(key: key);

  @override
  _RoomCreshowState createState() => _RoomCreshowState();
}

class _RoomCreshowState extends State<RoomCreshow> {
  TextEditingController editingName = TextEditingController();
  List<RoomTypes> chargeList = [];
  late int type = 0;
  @override
  void initState() {
    super.initState();
    requestChargeMap();
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      padding: EdgeInsets.only(
          top: 20.w,
          left: 30.w,
          right: 30.w,
          bottom: ScreenUtil().bottomBarHeight + 50.w),
      // padding: EdgeInsets.only(
      //     top: 10.w, bottom: ScreenUtil().bottomBarHeight + 20.w),
      child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(child: FSheetLine()),
            Center(
                child: FText(
              "创建房间",
              weight: FontWeight.bold,
              color: Colors.black,
              size: 36.sp,
            )),
            SizedBox(height: 20.h),
            Row(
              children: [
                FText(
                  "房间名称",
                  size: 30.sp,
                  color: Colors.black,
                ),
              ],
            ),
            inputField(),
            Row(
              children: [
                FText(
                  "房间类型",
                  size: 30.sp,
                  color: Colors.black,
                ),
              ],
            ),
            SizedBox(height: 20.h),
            Roomcreslist(ondouSelect),
            // giftList(),
            FcationButton("创建房间",
                isActivate: true, radius: 90.w, onPress: onTapCancel),
          ]),
    );
  }

  void ondouSelect(int gold) {
    print(gold);
    type = chargeList[gold].topicId!;
  }

  Widget inputField() {
    return FContainer(
      height: 88.w,
      color: const Color(0xFF777272).withOpacity(0.2),
      radius: BorderRadius.circular(88.w),
      margin: EdgeInsets.fromLTRB(0.w, 20.w, 0.w, 20.h),
      padding: EdgeInsets.symmetric(horizontal: 20.w),
      child:
          Row(children: [SizedBox(width: 10.w), Expanded(child: textField())]),
    );
  }

  Widget textField() {
    InputBorder border = const OutlineInputBorder(
        borderSide: BorderSide(color: Colors.transparent));
    List<TextInputFormatter>? formatter;
    formatter = [
      FilteringTextInputFormatter.allow(
          RegExp(r'[-_\u4E00-\u9FA5a-zA-Z0-9\u2006\.]'))
    ];

    return TextField(
      key: const Key("Name"),
      controller: editingName,
      style: TextStyle(color: Colors.black, fontSize: 30.sp),
      inputFormatters: formatter,
      maxLength: 18,
      decoration: InputDecoration(
        enabledBorder: border,
        focusedBorder: border,
        contentPadding: EdgeInsets.zero,
        hintStyle: TextStyle(color: const Color(0xFF999999), fontSize: 30.sp),
        hintText: '请输入房间名称',
        counterText: "",
      ),
    );
  }

  Widget buttonCell(String img, String text, Function() onTap) {
    Widget btn = FContainer(
      width: 150.w,
      height: 140.w,
      child:
          Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
        LoadAssetImage(img, width: 80.w),
        FText(text, color: const Color(0xFF999999), size: 22.sp)
      ]),
    );
    return GestureDetector(onTap: onTap, child: btn);
  }

  void onTapCancel() {
    if (editingName.text.isEmpty) {
      WidgetUtils().showToast("请输入房间名称");
      return;
    }

    DioUtils().asyncHttpRequest(
      NetConstants.roomCreate,
      method: DioMethod.POST,
      params: {
        "user_id": UserConstants.userginInfo!.info.userId,
        "room_name": editingName.text,
        "room_type": 1,
        'room_topic_id': type
      },
      onSuccess: (response) {
        WidgetUtils().popPage();
        WidgetUtils().popPage();
      },
    );
  }

  void requestChargeMap() {
    DioUtils().asyncHttpRequest(NetConstants.getelation,
        params: {"pid": 30}, method: DioMethod.GET, onSuccess: onChargeMap);
  }

  void onChargeMap(response) {
    response ??= [];
    chargeList.clear();
    for (var data in response) {
      chargeList.add(RoomTypes.fromJson(data));
    }
    type = chargeList[0].topicId!;
    if (!mounted) return;
    setState(() {});
  }
  // void requestHandle(String secret) {
  //   DioUtils().asyncHttpRequest(
  //     NetConstants.officialHandle,
  //     method: DioMethod.POST,
  //     params: {"id": widget.model.id, "secret": secret, "remark": ""},
  //     onSuccess: (response) {
  //       WidgetUtils().popPage();
  //       WidgetUtils().showToast("动态已隐藏");
  //       widget.onDel(widget.model);
  //     },
  //   );
  // }
}
