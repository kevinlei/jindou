import 'package:echo/import.dart';
import 'package:echo/pages/person/dress/DressHome.dart';
import 'package:echo/pages/person/romhome/MyRoomPage.dart';
import 'package:echo/utils/ext/string_ext.dart';
import 'package:echo/widgets/my_widgets.dart';
import 'package:flutter/material.dart';
import 'package:fluwx/fluwx.dart' as fluwx;
import 'auth/provider/AliyunFaceProvider.dart';
import 'helpcenter/HelpCenter.dart';

class PersonPage extends StatefulWidget {
  final PageController homePageCtr;

  const PersonPage(this.homePageCtr, {Key? key}) : super(key: key);

  @override
  _PersonPageState createState() => _PersonPageState();
}

class _PersonPageState extends State<PersonPage>
    with AutomaticKeepAliveClientMixin {
  PersonProvider provider = PersonProvider();
  late PageController homePageCtr;
  HomePageProvider homeProvider = HomePageProvider();
  AliyunFaceProvider aliyunprovider = AliyunFaceProvider();
  late EasyRefreshController _controller;

  // 轮播图列表
  bool showPwd = true;

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    widget.homePageCtr.addListener(() {
      if (widget.homePageCtr.page == 3) {
        callRefresh();
      }
    });
    FocusManager.instance.primaryFocus?.unfocus();
    _controller = EasyRefreshController();
  }

  @override
  void dispose() {
    widget.homePageCtr.dispose();
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: provider),
        // ChangeNotifierProvider.value(value: homeProvider),
      ],
      child: Scaffold(
        backgroundColor: const Color(0xFFFFFFFF),
        body: FContainer(
          image: ImageUtils().getAssetsImage("public/huibg"),
          imageFit: BoxFit.fill,
          imageAlign: Alignment.topCenter,
          child: SafeArea(
            child: FRefreshLayout.custom(
              onRefresh: callRefresh,
              controller: _controller,
              headerIndex: 0,
              slivers: [
                SliverAppBar(
                  automaticallyImplyLeading: false,
                  pinned: true,
                  backgroundColor: Colors.transparent,
                  actions: [modifyButton()],
                  expandedHeight: 522.w,
                  flexibleSpace: flexibleSpace(),
                ),
                // SliverToBoxAdapter(child: UserContent(),),

                // UserContent(),
                SliverToBoxAdapter(
                  child: FContainer(
                    //个人中心
                    margin: EdgeInsets.only(
                        left: 30.w, top: 32.w, right: 30.w, bottom: 0.w),
                    child: BannerSwipe(BanbarSwper.PERSON_COM.brand),
                  ),
                ),
                /* SliverToBoxAdapter(
                  child: Visibility(
                    visible: UserConstants.userginInfo?.info.teenmode != 1,
                    child: accountContent(),
                  ),
                ),*/
                SliverToBoxAdapter(
                  child: Visibility(
                    visible: UserConstants.userginInfo?.info.teenmode != 1,
                    child: manageContent(),
                  ),
                ),
                SliverToBoxAdapter(child: _listWidget()),
                // SliverToBoxAdapter(child: SizedBox(height: 200.h))
              ],
            ),
          ),
        ),
      ),
    );
  }

  // 账户详情
  Widget accountUser() {
    Widget box = FContainer(
      color: Colors.white,
      radius: BorderRadius.circular(28.w),
      margin: EdgeInsets.only(left: 25.w, right: 25.w, bottom: 20.w, top: 20.w),
      padding: EdgeInsets.symmetric(horizontal: 30.w, vertical: 30.h),
      child: Consumer<PersonProvider>(builder: (_, pro, __) {
        return Column(children: [
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            FText(
              "我的账户",
              color: Colors.black,
              size: 32.sp,
              align: TextAlign.start,
              weight: FontWeight.bold,
            ),
            const Spacer(),
            GestureDetector(
              onTap: onTapMoneydify,
              child: FContainer(
                height: 52.w,
                align: Alignment.center,
                gradientBegin: Alignment.centerLeft,
                gradientEnd: Alignment.centerRight,
                // gradient: [Color(0xFFEB9DFF), Color(0xFFA24BFE)],
                radius: BorderRadius.circular(50),
                child: Row(children: [
                  LoadAssetImage("person/myGold", width: 35.w),
                  FText("${pro.gold}",
                      color: const Color(0xFFFF753F),
                      size: 36.sp,
                      weight: FontWeight.bold),
                  LoadAssetImage("person/more", width: 20.w),
                ]),
              ),
            )
          ]),
        ]);
      }),
    );
    return GestureDetector(
      onTap: onTapModify,
      child: box,
    );
  }

  Future<void> callRefresh() async {
    provider.callRefresh();
    // homeProvider.requestGift();
    setState(() {});
  }

  Widget modifyButton() {
    return FContainer(
      child: Row(
        children: [
          IconButton(
              onPressed: onTapModify,
              icon: LoadAssetImage("person/setup", width: 50.w)),
          SizedBox(width: 30.h),
        ],
      ),
    );
  }

  Widget flexibleSpace() {
    return FlexibleSpaceBar(
      background: FContainer(
        child: Stack(children: [
          FContainer(
            height: 522.w,
            width: double.infinity,
            color: Colors.transparent,
            foreGradientBegin: Alignment.center,
            foreGradientEnd: Alignment.bottomCenter,
          ),
          const Positioned(bottom: 0, left: 0, right: 0, child: UserContent()),
        ]),
      ),
    );
  }

  // 账户详情
  Widget accountContent() {
    Widget box = FContainer(
      color: Colors.white,
      radius: BorderRadius.circular(28.w),
      margin: EdgeInsets.only(left: 25.w, right: 25.w, bottom: 20.w, top: 20.w),
      padding: EdgeInsets.symmetric(horizontal: 30.w, vertical: 30.h),
      child: Consumer<PersonProvider>(builder: (_, pro, __) {
        return Column(children: [
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            FText(
              "我的账户",
              color: Colors.black,
              size: 32.sp,
              align: TextAlign.start,
              weight: FontWeight.bold,
            ),
            GestureDetector(
              onTap: onHide,
              child: FContainer(
                width: 50.w,
                height: 80.w,
                align: Alignment.center,
                child: LoadAssetImage(
                    showPwd ? "person/enpusty" : "person/mupas",
                    width: 30.w),
              ),
            ),
            const Spacer(),
            GestureDetector(
              onTap: onTapMoneydify,
              child: FContainer(
                height: 52.w,
                align: Alignment.center,
                gradientBegin: Alignment.centerLeft,
                gradientEnd: Alignment.centerRight,
                // gradient: [Color(0xFFEB9DFF), Color(0xFFA24BFE)],
                radius: BorderRadius.circular(50),
                child: Row(children: [
                  LoadAssetImage("person/myGold", width: 35.w),
                  FText(showPwd ? '${pro.gold}' : '****',
                      color: const Color(0xFFFF753F),
                      size: 36.sp,
                      weight: FontWeight.bold),
                  LoadAssetImage("person/more", width: 20.w),
                ]),
              ),
            )
          ]),
        ]);
      }),
    );
    return GestureDetector(
      onTap: onTapMoneydify,
      child: box,
    );
  }

  void onHide() {
    showPwd = !showPwd;
    if (!mounted) return;
    setState(() {});
  }

  // 等级信息
  Widget accountCell(
      String title, var levelInfo, final GestureTapCallback? onTap) {
    // 魅力
    String iconName = "";
    int lv = 0;
    // if (title == "魅力等级") {
    //   ConfigLevel? level = ConfigManager().getLevel(levelInfo?.charmExp ?? 0);
    //   iconName = level?.charmPicture ?? "default/person/charm/no.png";
    //   lv = int.tryParse(level?.grade ?? "0") ?? 0;
    // } else if (title == "财富等级") {
    //   ConfigLevel? level = ConfigManager().getLevel(levelInfo?.richExp ?? 0);
    //   iconName = level?.wealthPicture ?? "default/person/wealth/no.png";
    //   lv = int.tryParse(level?.grade ?? "0") ?? 0;
    // } else {
    //   iconName =
    //       ConfigManager().getTitle(levelInfo?.peerageLv ?? 0)?.headPicture ??
    //           "default/person/myNobility.png";
    // }

    return Flexible(
      child: GestureDetector(
        onTap: onTap,
        child: FContainer(
          height: 72.w,
          padding: EdgeInsets.symmetric(horizontal: 20.w),
          color: const Color(0xFF322C4A),
          radius: BorderRadius.circular(10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              FText(title, size: 24.sp, color: Colors.white.withOpacity(0.8)),
              const Spacer(),
              LoadNetImage(NetConstants.ossPath + iconName,
                  width: 32.w, height: 32.w),
              SizedBox(width: 5.w),
              FText("Lv.$lv",
                  align: TextAlign.end,
                  weight: FontWeight.w600,
                  size: 28.sp,
                  color: Colors.white),
              SizedBox(width: 5.w),
              Icon(Icons.arrow_forward_ios_outlined,
                  size: 20.w, color: const Color(0xFF8B7EB4)),
            ],
          ),
        ),
      ),
    );
  }

  Widget manageContent() {
    return myRow(
        margin: EdgeInsets.only(top: 32.h, left: 30.w, right: 30.w),
        decoration:
        myRoundCornerBoxDecoration(radius: 24.w, color: Colors.white),
        padding:
        EdgeInsets.only(left: 32.w, right: 32.w, top: 16.w, bottom: 24.w),
        children: [
          manageCell("person/wdfj", "我的房间", onTaproomModify),
          // manageCell("person/dress", "个性装扮", onTapDress),
          /* manageCell("person/grade", "我的等级", () {
          onTapLevel(0);
          provider.requestSelfInfo();
        }),*/
          // Consumer<HomePageProvider>(builder: (_, pro, __) {
          //   return manageCell("person/gift", "我的礼物", () {
          //     onTapGift(pro.giftList);
          //   });
          // }),
          manageCell("person/wdlw", "我的礼物", () {
            onTapGift([]);
          }),
          // manageCell("person/gift", "我的礼物", onTapModify),
          manageCell("person/ghdt", "公会大厅", onTapRoomModify),
        ],
        mainAxisAlignment: MainAxisAlignment.spaceAround);
  }

  Widget manageCell(String img, String name, Function() onPress) {
    return myGestureDetector(
        child: myColumn(
            children: [
              mySizeIcon(
                img,
                size: 136.w,
              ),
              mySpaceHeight(8.h),
              myCenterText(name,
                  fontSize: 24.sp, color: const Color(0xCC222222))
            ],
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center),
        onTap: onPress);
  }

  Widget setContent() {
    return FContainer(
      color: const Color(0xFFFFFFFF),
      margin: EdgeInsets.only(left: 25.w, right: 25.w, bottom: 20.w, top: 0.w),
      radius: BorderRadius.circular(28.w),
      child: Column(
        children: [
          SizedBox(height: 35.w),
          Row(
            children: [
              SizedBox(width: 35.w),
              FText(
                "更多服务",
                color: Colors.black,
                size: 32.sp,
                align: TextAlign.start,
                weight: FontWeight.bold,
              ),
            ],
          ),
          GridView.count(
            crossAxisCount: 4,
            shrinkWrap: true,
            padding: EdgeInsets.all(20.w),
            physics: const NeverScrollableScrollPhysics(),
            children: [
              // setCell("person/grade", "我的等级", () {
              //   onTapLevel(0);
              // }),
              setCell("person/secure", "实名认证", onTapAuth),
              // setCell("person/myInvite", "邀请好友", onTapInvite),
              setCell("person/invite", "青少年模式", () {
                WidgetUtils().pushPage(const YoungPage(), fun: callRefresh);
              }),
              setCell("person/service", "联系客服", onTapservicePage),

              // setCell("person/mySetting", "设置", onTapModify),
              setCell("person/centrality", "帮助中心", () async {
                WidgetUtils().pushPage(HelpCenter());
                // WidgetUtils().pushPage(TextPage());

                // 广告测试
                // AppConstants.oppo(1);

                // DioUtils().httpRequest(
                //   NetConstants.test,
                //   loading: false,
                // );
                // WidgetUtils().showToast("正在开发中");
              }),
              setCell("person/feedback", "反馈意见", onTapFeedbackPage),
              setCell("person/about", "关于我们", onTapAboutPage),
              // setCell("person/club", "公会大厅", onTapClub),
            ],
          ),
        ],
      ),
    );
  }

  Widget _listWidget() {
    return myColumn(
        margin:
        EdgeInsets.only(left: 30.w, top: 32.w, right: 30.w, bottom: 44.w),
        children: [
          _listItem('person/wddj', '我的等级', onTap: () {
            onTapLevel(0);
            provider.requestSelfInfo();
          }),
          mySpaceHeight(32.w),
          _listItem('person/smrz', '实名认证', onTap: () {
            onTapAuth();
          }),
          mySpaceHeight(32.w),
          _listItem('person/qsnms', '青少年模式', onTap: () {
            WidgetUtils().pushPage(const YoungPage(), fun: callRefresh);
          }),
          mySpaceHeight(32.w),
          _listItem('person/lxkf', '联系客服', onTap: () {
            onTapservicePage();
          }),
          mySpaceHeight(32.w),
          _listItem('person/bzzx', '帮助中心', onTap: () {
            WidgetUtils().pushPage(HelpCenter());
          }),
          mySpaceHeight(32.w),
          _listItem('person/fkyj', '反馈意见', onTap: () {
            onTapFeedbackPage();
          }),
          mySpaceHeight(32.w),
          _listItem('person/gywm', '关于我们', onTap: () {
            onTapAboutPage();
          }),
          mySpaceHeight(32.w),
        ],
        decoration:
        myRoundCornerBoxDecoration(radius: 24.w, color: Colors.white),
        padding:
        EdgeInsets.only(left: 24.w, right: 24.w, top: 24.w, bottom: 32.w));
  }

  Widget _listItem(
      String iconPath,
      String label, {
        required GestureTapCallback? onTap,
      }) {
    return myGestureDetector(
        child: myRow(children: [
          mySizeIcon(iconPath, size: 56.w),
          mySpaceWidth(16.w),
          Expanded(
              child: myLeftText(
                label,
                fontSize: 24.sp,
                color: '#FF1A1A1A'.toColor(),
              )),
          mySizeIcon(
            'public/arrow_right_grey',
            size: 48.w,
          ),
        ]),
        onTap: onTap);
  }

  Widget setCell(String img, String name, Function() onPress) {
    Widget btn = FContainer(
      align: Alignment.center,
      child:
      Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
        LoadAssetImage(img, width: 64.w, height: 64.w),
        FText(
          name,
          size: 24.sp,
          color: const Color(0xFF9F9FB9),
        )
      ]),
    );

    return GestureDetector(onTap: onPress, child: btn);
  }

  void onTaproomModify() {
    WidgetUtils().pushPage(MyRoomPage(), fun: callRefresh);
  }

  void onTapModify() {
    WidgetUtils().pushPage(SetPage(), fun: callRefresh);
  }

  void onTapRoomModify() {
    WidgetUtils().pushPage(ClubPage(), fun: callRefresh);
  }

  void onTapMoneydify() {
    WidgetUtils().pushPage(Mymoney(), fun: callRefresh);
  }

  void onTapGift(List<ConfigGift> giftList) {
    WidgetUtils().pushPage(
        HomepageGifts(
          UserConstants.userginInfo!.info.userId,
          UserConstants.userginInfo!.info.headIcon,
          UserConstants.userginInfo!.info.name,
        ),
        fun: callRefresh);
  }

  void onTapDress() async {
    await WidgetUtils().pushPage(DressHome());
    provider.requestSelfInfo();
  }

  void onTapLevel(int index) {
    WidgetUtils().pushPage(
        LevelPage(
          index: index,
        ),
        fun: callRefresh);
  }

  void onTapservicePage() async {
    // fluwx.openWeChatCustomerServiceChat(
    //     corpId: 'wwdef5ce0ec115ae2b',
    //     url: 'https://work.weixin.qq.com/kfid/kfc5e127a02eb7e8381');

    WidgetUtils().pushPage(const BanerWage(
        url:
        "https://chatbot.weixin.qq.com/webapp/auth/7bZcj7lwQ11RGGNqTtpxdudrkwQBC5"));
  }

  void requestGlobalConfig() {
    DioUtils().httpRequest(
      NetConstants.wxRecord,
      loading: false,
      params: {"userid": "11111"},
      method: DioMethod.POST,
      onSuccess: _onGlobalConfig,
    );
  }

  void _onGlobalConfig(response) {
    if (response == null) return;
  }

  void onTapFeedbackPage() {
    WidgetUtils().pushPage(FeedbackPage(
        type: FeedbackType.APP_PROBLEM_FEEDBACK,
        userId: UserConstants.userginInfo?.info.userId.toString()));
  }

  void onTapYoung() {
    WidgetUtils().pushPage(const YoungPage(), fun: callRefresh);
  }

  void onTapAuth() {
    WidgetUtils().showLoading();
    DioUtils().asyncHttpRequest(NetConstants.authStatus,
        method: DioMethod.GET, onSuccess: onAuthStatus);
  }

  void onAuthStatus(response) {
    if (response == null) return;
    if (response["validate_idcard"] == 0) {
      if (response["mode"] == 2) {
        aliyunprovider.getMetaupsInfos((res) {
          //认证成功后跳转审核状态
          debugPrint("object");
          debugPrint(res);
          // requestAuthStatus();
        }, (i, str) {
          print(str);
          WidgetUtils().pushPage(AuthFalsePage(), fun: callRefresh);
        });
      } else {
        WidgetUtils().pushPage(AuthPage1(), fun: callRefresh);
      }
    } else {
      WidgetUtils().pushPage(AuthPage1(), fun: callRefresh);
    }
    if (!mounted) return;
    WidgetUtils().cancelLoading();
    setState(() {});
  }

  // 关于我们
  void onTapAboutPage() {
    WidgetUtils().pushPage(const AboutPage(), fun: callRefresh);
  }
//
// void onTapClub() {
//   WidgetUtils().pushPage(ClubPage(widget.homePageCtr));
// }
//
// void onTapInvite() {
//   WidgetUtils().pushPage(InvitePage());
// }
//
// void onTapService() async {
//   fluwx.openWeChatCustomerServiceChat(
//       corpId: 'wwb0e92838cb3722b9',
//       url: 'https://work.weixin.qq.com/kfid/kfcece594b6ac0c69da');
// }
//
// void onTapCharge() {
//   // WidgetUtils().showBottomSheet(ChargeDialog(), onCallback: oncallRefresh);
//   WidgetUtils().pushPage(ChargePage(), fun: callRefresh);
// }
}

class TestPage extends StatefulWidget {
  const TestPage({Key? key}) : super(key: key);

  @override
  State<TestPage> createState() => _TestPageState();
}

class _TestPageState extends State<TestPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SizedBox(
          height: 200.w,
          width: 750.w,
          child: const SVGASimpleImage(assetsName: "assets/enter.svga"),
        ),
      ),
    );
  }
}
