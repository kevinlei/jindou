import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class PersonProvider extends ChangeNotifier {
  // 关注
  int _countFollow = 0;
  int get countFollow => _countFollow;
  // 粉丝
  int _countFans = 0;
  int get countFans => _countFans;
  // 足迹
  int _countFriend = 0;
  int get countFriend => _countFriend;
  // 访客
  int _countVisit = 0;
  int get countVisit => _countVisit;
  // 金币
  double _gold = 0.00;
  double get gold => _gold;

  LoginUserModel? _userInfo;
  LoginUserModel? get userInfo => _userInfo;

  String serviceId = "";

  void updateUserInfo({LoginUserModel? model}) {
    _userInfo = model;
    notifyListeners();
  }

  void updateRelationCount(int a, int b, int c, int d) {
    _countFollow = a;
    _countFans = b;
    _countFriend = c;
    _countVisit = d;
    notifyListeners();
  }

  Future<void> callRefresh() async {
    requestSelfInfo();
    requestRelationCount();
    requestService();

  }

  void requestSelfInfo() {
    DioUtils().asyncHttpRequest(NetConstants.getUserInfo,
        method: DioMethod.GET,
        params: {"userId": UserConstants.userginInfo?.info.userId},
        onSuccess: getUserInfoSuccess,
        loading: false);
  }

  void requestRelationCount() async {
    //"fansCount":0,"followsCount":1,"blacksCount":0,"friendCount":0
    DioUtils().asyncHttpRequest(
      NetConstants.getRelationCount,
      params: {"userId": UserConstants.userginInfo?.info.userId},
      loading: false,
      onSuccess: (response) => updateRelationCount(
          response["flow"] ?? 0,
          response["fans"] ?? 0,
          response["friend"] ?? 0,
          response["visitor"] ?? 0),
    );
  }

  void requestService() {
    // DioUtils().asyncHttpRequest(
    //   NetConstants.gameConfig,
    //   loading: false,
    //   method: DioMethod.POST,
    //   params: {
    //     "key": AppConstants.appEnv == AppEnv.ENV_LIN
    //         ? "prod_service"
    //         : "dev_service"
    //   },
    //   onSuccess: (response) => serviceId = response ?? "",
    // );
  }
  void getUserInfoSuccess(response) async {
    if (response == null) return;
    _gold = double.tryParse(response['gold_info']['gold'].toString()) ?? 0;
    UserConstants.userginInfo = LoginUserModel.fromJson(response);
    updateUserInfo(model: UserConstants.userginInfo);
  }
  notifyListeners();
}
