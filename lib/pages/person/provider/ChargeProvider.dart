import 'package:echo/constants/AppManager.dart';
import 'package:echo/import.dart';
import 'package:flutter/cupertino.dart';
import '../../../constants/AppConstants.dart';
import '../../../constants/EnumConstants.dart';
import '../../../constants/NetConstants.dart';
import '../../../utils/DioUtils.dart';
import '../../../utils/WidgetUtils.dart';
import 'package:fluwx/fluwx.dart' as fluwx;
import 'package:tobias/tobias.dart';

class ChargeProvider extends ChangeNotifier {
  bool accept = false;
  int chargeNum = 0;
  PayType payType = PayType.NULL;
  double crestNum = 0;

  void onChargeSelect(int gold) => this.chargeNum = gold;
  void onAcceptSelect(bool accept) => this.accept = accept;
  void onTypeSelect(PayType type) => this.payType = type;

  void onTapPay() {
    notifyListeners();

    if (!accept) return WidgetUtils().showToast("请先阅读并同意协议");
    if (chargeNum == 0) return WidgetUtils().showToast("请先选择充值数值");
    if (payType == PayType.NULL) return WidgetUtils().showToast("请先选择支付方式");
    switch (payType) {
      case PayType.PAY_WX:
        createOrder('wx');
        break;
      case PayType.PAY_ALI:
        createOrder('ali');
        break;
      case PayType.PAY_SD_KJ:
        createOrder('paypal');
        break;
      default:
    }
  }

  // 统一下单入口
  void createOrder(String type) async {
    Map params = {};
    if (type == 'wx') {
      params = {
        "product_id": chargeNum,
        "pay_type": 10,
        "token": AppConstants.accessToken,
      };
    } else if (type == 'ali') {
      params = {
        //套餐ID
        "product_id": chargeNum,
        "pay_type": 20,
        "token": AppConstants.accessToken,
      };
    } else {
      return WidgetUtils().showToast("暂不支持此支付方式");
    }

    await DioUtils().httpRequest(
      NetConstants.payCreateOrder,
      method: DioMethod.POST,
      loading: false,
      params: params,
      onSuccess: (response) {
        if (type == "wx") {
          wxPay(response);
        } else if (type == "ali") {
          aliPay(response);
        } else {}
      },
    );
  }

  //微信支付
  Future<void> wxPay(Map order) async {
    try {
      bool isInstalled = await fluwx.isWeChatInstalled;
      if (!isInstalled) {
        return WidgetUtils().showToast("请先安装微信");
      }
      fluwx.payWithWeChat(
        appId: order["appid"] ?? "",
        partnerId: order["partnerid"] ?? "",
        prepayId: order["prepayid"] ?? "",
        packageValue: order["package"] ?? "",
        nonceStr: order["noncestr"] ?? "",
        timeStamp: int.parse(order["timestamp"] ?? ""),
        sign: order["sign"] ?? "",
      );
      fluwx.weChatResponseEventHandler.listen((event) {
        if (event.isSuccessful) {
          WidgetUtils().showToast("微信支付成功");
          requestDynamicInfo();
          // 充值下单统计
          AppManager().xinstallFlutterPlugin.reportEventWhenOpenDetailInfo(
                "Recharge",
                chargeNum,
                UserConstants.userginInfo!.info.userId,
              );
        } else {
          WidgetUtils().showToast("微信支付失败");
        }
      });
    } catch (e) {
      WidgetUtils().showToast("微信支付订单数据错误，微信支付失败");
    }
  }

  // 支付宝支付
  void aliPay(String order) async {
    try {
      Tobias tobias = Tobias();
      bool isInstalled = await tobias.isAliPayInstalled;
      if (!isInstalled) {
        return WidgetUtils().showToast("请先安装支付宝");
      }
      Map data = await tobias.pay(order);
      if (data["resultStatus"] == 9000 || data["resultStatus"] == "9000") {
        WidgetUtils().showToast("支付宝支付成功");
        requestDynamicInfo();
        // 充值下单统计
        AppManager().xinstallFlutterPlugin.reportEventWhenOpenDetailInfo(
              "Recharge",
              chargeNum,
              UserConstants.userginInfo!.info.userId,
            );
      } else {
        WidgetUtils().showToast("支付宝支付失败");
      }
    } catch (e) {
      WidgetUtils().showToast("支付宝支付订单数据错误，支付宝支付失败");
    }
  }

  // 更新用户财富信息
  void requestDynamicInfo() async {
    await DioUtils().httpRequest(
      NetConstants.gradeUserMoney,
      method: DioMethod.GET,
      onSuccess: updateInfo,
    );
  }

  void updateInfo(response) {
    if (response == null) return;
    UserConstants.userginInfo?.goldInfo.gold = response['gold'];
    UserConstants.userginInfo?.goldInfo.goldLock = response['gold_lock'];
    UserConstants.userginInfo?.goldInfo.gains = response['gains'];
    UserConstants.userginInfo?.goldInfo.gainsLock = response['gains_lock'];
    UserConstants.userginInfo?.goldInfo.goldable = response['gold_able'];
    UserConstants.userginInfo?.goldInfo.gainsable = response['gains_able'];
    crestNum = response['gold'];
    notifyListeners();
  }
}
