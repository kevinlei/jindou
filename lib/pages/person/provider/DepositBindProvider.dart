import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class DepositBindProvider extends ChangeNotifier {
  AuthStatus authStatus = AuthStatus.NULL;

  String originName = "";
  String originCard = "";

  String name = "";
  String card = "";
  String code = "";

  bool visible = false;

  // [{"Type":1,"CardNumber":"1234567890","Name":"mm"}
  void onGetBindStatus(response) {
    if (response['PaymentAccountInfo'] == null) return;
    for (var data in response['PaymentAccountInfo']) {
      if (data["account_type"] == DepositType.DEP_ALI.index + 9 ||
          data["account_type"] == DepositType.DEP_BANK.index + 12) {
        originName = data["name"];
        originCard = data["account_number"];
        name = data["name"];
        card = data["account_number"];
      }
    }
  }

  void onInputName(String value) {
    name = value;
    _checkInput();
  }

  void onInputAli(String value) {
    card = value;
    _checkInput();
  }

  void onInputBank(String value) {
    card = value;
    _checkInput();
  }

  void onInputCode(String value) {
    code = value;
    _checkInput();
  }

  void _checkInput() {
    bool canClick = true;
    if (name.isEmpty || card.isEmpty || !FuncUtils().isMobileCode(code)) {
      canClick = false;
    }
    if (name == originName && card == originCard) {
      canClick = false;
    }
    if (canClick != visible) {
      visible = canClick;
      notifyListeners();
    }
  }
}
