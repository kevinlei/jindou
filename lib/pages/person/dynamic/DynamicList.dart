import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class DynamicList extends StatefulWidget {
  final DynamicListType listType;
  final PageController homePageCtr;
  final PageController controller;
  final int index;
  const DynamicList(
      this.listType, this.homePageCtr, this.controller, this.index,
      {Key? key})
      : super(key: key);

  @override
  _DynamicListState createState() => _DynamicListState();
}

class _DynamicListState extends State<DynamicList>
    with AutomaticKeepAliveClientMixin {
  DynamicProvider provider = DynamicProvider();

  @override
  void initState() {
    super.initState();
    widget.homePageCtr.addListener(() {
      if (widget.homePageCtr.page == 1 &&
          widget.controller.page == widget.index) {
        provider.initListType(widget.listType);
      }
    });
    widget.controller.addListener(() {
      if (widget.controller.page == widget.index) {
        provider.initListType(widget.listType);
      }
    });
    provider.initListType(widget.listType);
  }

  @override
  void dispose() {
    super.dispose();
    widget.controller.dispose();
    widget.homePageCtr.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    Widget child = Stack(
      children: [
        listContent(),
      ],
    );
    return ChangeNotifierProvider(
      create: (_) => provider,
      builder: (_, __) => child,
    );
  }

  @override
  void deactivate() {
    PluginUtils().setListener("DynamicList", null);
    super.deactivate();
  }

  Widget listContent() {
    double bottom = ScreenUtil().bottomBarHeight + 60.w;
    return Consumer<DynamicProvider>(builder: (_, pro, __) {
      return FRefreshLayout(
        onRefresh: pro.refreshList,
        onLoad: pro.loadingList,
        firstRefresh: false,
        emptyWidget: pro.dyList.isEmpty ? const FEmptyWidget() : null,
        child: ListView.builder(
          cacheExtent: ScreenUtil().screenHeight,
          padding: EdgeInsets.fromLTRB(0, 20.w, 0, bottom),
          itemBuilder: (_, index) => DynamicCell(
            pro.dyList[index],
            onUpdate: pro.updateDynamic,
            onDel: pro.onDelDynamic,
            onHidden: pro.onHidden,
          ),
          itemCount: pro.dyList.length,
        ),
      );
    });
  }

  Widget btnPublish() {
    double bottom = ScreenUtil().bottomBarHeight + 450.w;
    return Positioned(
        right: 40.w,
        bottom: bottom,
        child: GestureDetector(
          onTap: onPressPublish,
          child: LoadAssetImage(
            "dynamic/dPublish",
            width: 140.w,
            color: Colors.white,
          ),
        ));
  }

  void onPressPublish() {
    WidgetUtils().pushPage(const PublishPage());
  }

  void onLocation(String key, String method, data) {
    if (key != "DynamicList") return;
    provider.refreshList();
  }

  @override
  bool get wantKeepAlive => true;
}
