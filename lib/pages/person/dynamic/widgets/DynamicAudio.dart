import 'dart:io';
import 'package:logger/logger.dart' show Level;
import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class DynamicAudio extends StatefulWidget {
  final String audioPath;
  final bool fileSource;
  final int duration;

  const DynamicAudio(this.audioPath,
      {Key? key, this.duration = 0, this.fileSource = false})
      : super(key: key);
  @override
  _DynamicAudioState createState() => _DynamicAudioState();
}

class _DynamicAudioState extends State<DynamicAudio> {
  FlutterSoundPlayer? soundPlayer;

  @override
  void dispose() {
    if (soundPlayer != null) {
      soundPlayer!.closePlayer();
      soundPlayer = null;
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Widget audioAnim = SizedBox(
        width: 320.r, child: SVGASimpleImage(assetsName: "assets/audio.svga"));
    Widget audioIdle = LoadAssetImage("dynamic/dAudio", width: 320.r);
    Widget button = GestureDetector(
      onTap: onPressPlay,
      child: LoadAssetImage(
          (soundPlayer != null && soundPlayer!.isPlaying)
              ? "dynamic/dAudioOff"
              : "dynamic/dAudioOn",
          width: 60.r),
    );
    List<Widget> children = [
      button,
      SizedBox(width: 18.h),
      (soundPlayer != null && soundPlayer!.isPlaying) ? audioAnim : audioIdle,
      Spacer(),
      Visibility(
          child: FText("${widget.duration}s", size: 26.sp),
          visible: widget.duration > 0),
      SizedBox(width: 10.r),
    ];
    return FContainer(
        height: 90.w,
        width: 480.w,
        gradient: [Color(0xFF46F5F6), Color(0xFFFFA7FB), Color(0xFFFF82B2)],
        gradientBegin: Alignment.topLeft,
        gradientEnd: Alignment.bottomRight,
        padding: EdgeInsets.symmetric(horizontal: 20.r),
        radius: BorderRadius.circular(20.w),
        child: Row(children: children));
  }

  void onPressPlay() async {
    if (soundPlayer == null) {
      soundPlayer = FlutterSoundPlayer(logLevel: Level.error);
      await soundPlayer!.closePlayer();
      await soundPlayer!.openPlayer();
    }
    soundPlayer!.isPlaying ? _onStop() : _onPlay();
  }

  // 播放录音
  void _onPlay() async {
    if (soundPlayer == null) return;
    try {
      bool exit = await recordPathExit();
      if (!exit) return WidgetUtils().showToast("播放路径出错");

      String source = widget.audioPath;
      if (!widget.fileSource) {
        source = NetConstants.ossPath + source;
      }
      bool state = source.contains('pcm');

      await soundPlayer!.startPlayer(
          fromURI: source,
          codec: !state ? Codec.aacADTS : Codec.pcm16,
          sampleRate: 8000,
          whenFinished: update);
      update();
    } catch (e) {
      debugPrint("播放音频出错：$e");
    }
  }

  // 停止播放
  void _onStop() async {
    if (soundPlayer == null) return;
    if (!soundPlayer!.isPlaying) return;
    try {
      await soundPlayer!.stopPlayer();
      update();
    } catch (e) {
      debugPrint("暂停音频出错：$e");
    }
  }

  Future<bool> recordPathExit() async {
    if (widget.audioPath.isEmpty) return false;
    if (!widget.fileSource) return true;
    bool exit = await File(widget.audioPath).exists();
    return exit;
  }

  void update() {
    if (!mounted) return;
    setState(() {});
  }
}
