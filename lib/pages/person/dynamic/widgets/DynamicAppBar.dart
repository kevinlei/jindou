import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class DynamicAppBar extends StatefulWidget implements PreferredSizeWidget {
  final PageController controller;
  const DynamicAppBar(this.controller, {Key? key}) : super(key: key);

  @override
  _DynamicAppBarState createState() => _DynamicAppBarState();

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}

class _DynamicAppBarState extends State<DynamicAppBar> {
  int curPage = 0;
  final List<String> table = ["推荐", "最新", "关注"];

  @override
  void initState() {
    super.initState();
    curPage = widget.controller.initialPage;
    widget.controller.addListener(onPageChanged);
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> tables =
        List.generate(table.length, (index) => tabCell(index));
    return FContainer(
      color: Colors.transparent,
      padding: EdgeInsets.only(
        left: 20.w,
        top: 0,
        right: 20.w,
      ),
      child: Row(
          children: tables
            ..addAll([
              const Spacer(),
              btnSearch(),
            ])),
    );
  }

  Widget tabCell(int page) {
    double size = curPage == page ? 36.sp : 28.sp;
    Color color =
        curPage == page ? Colors.black : Colors.black.withOpacity(0.8);
    FontWeight weight = curPage == page ? FontWeight.bold : FontWeight.normal;
    Widget btn = FContainer(
      width: 120.w,
      height: 60.w,
      child: Stack(alignment: Alignment.bottomCenter, children: [
        Positioned(
            child: Visibility(
          visible: curPage == page,
          child: Container(
            width: 32.w,
            height: 5.h,
            color: Colors.black,
            child: null,
          ),
        )),
        FText(table[page], color: color, size: size, weight: weight),
      ]),
    );
    return GestureDetector(child: btn, onTap: () => onTapPage(page));
  }

  Widget btnSearch() {
    return FButton(
      isTheme: true,
      child: Row(
        children: [
          LoadAssetImage("dynamic/Send", width: 30.w, height: 30.w),
          SizedBox(
            width: 10.w,
          ),
          FText(
            "发布",
            size: 24.sp,
            color: Colors.white,
            align: TextAlign.center,
          ),
        ],
      ),
      onTap: () {
        WidgetUtils().pushPage(const PublishPage());
      },
    );
  }

  void onTapPage(int page) {
    widget.controller.jumpToPage(page);
  }

  void onPressSearch() {
    // WidgetUtils().pushPage(SquareSearch(searchType: SearchType.DYNAMIC));
  }

  @override
  void deactivate() {
    super.deactivate();
    widget.controller.removeListener(onPageChanged);
  }

  void onPageChanged() {
    if (!mounted) return;
    curPage = (widget.controller.page ?? 0).toInt();
    setState(() {});
  }
}
