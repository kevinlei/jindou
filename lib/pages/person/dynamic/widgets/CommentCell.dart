import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class CommentCell extends StatelessWidget {
  final DynamicCom dynamicCom;
  final Function(int comId, String userId, int postId) onTap;
  final Function(int comId, int likeState, String userId, int postId) onLike;
  const CommentCell(this.dynamicCom,
      {Key? key, required this.onTap, required this.onLike})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Widget> children = [
      userContent(dynamicCom),
      Padding(
        padding: EdgeInsets.only(left: 80.w),
        child: dynamicCom.parentname?.length==0 ? comText(dynamicCom.content, dynamicCom.id,null,dynamicCom):comText(dynamicCom.content, dynamicCom.id,dynamicCom.parentname,dynamicCom),
      )
    ];
    List<DynamicCom> comList = [];

    if (comList.isNotEmpty) {
      children.add(Padding(
        padding: EdgeInsets.only(left: 80.w),
        child: Column(
            children: List.generate(
          comList.length,
          (index) =>
              relyCell(comList[index], parentName: dynamicCom.userInfo.name),
        )),
      ));
    }
    children.add(SizedBox(
      height: 5.h,
    ));
    children.add(
        Padding(
          padding: EdgeInsets.only(top: 15.w),
          child:   Divider(
            color: const Color(0xFF222222).withOpacity(0.05),
            height: 3.h,
          ),
        ),
      );

    return Column(children: children);
  }

  // 评论用户信息部分
  Widget userContent(DynamicCom model) {
    // 用户姓名部分
    Widget user = GestureDetector(
      onTap: () => onTap(model.id, model.userInfo.userId, model.postId),
      child: FContainer(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              FText(
                model.userInfo.name,
                size: 28.sp,
                overflow: TextOverflow.ellipsis,
                color: Colors.black,
              ),
              FText(getDyTime(model.createdAt),
                  size: 26.sp, color: const Color(0xFF999999))
            ]),
      ),
    );

    return Row(children: [
      GestureDetector(
        onTap: () {
          //点击头像跳转个人主页
          String userId = model.userInfo.userId;
          if (userId.isEmpty) return;
          WidgetUtils().pushPage(PersonHomepage(userId: userId));
        },
        child: CircleAvatar(
          radius: model.postId > 0 ? 30.w : 40.w,
          backgroundImage: ImageUtils()
              .getImageProvider(NetConstants.ossPath + model.userInfo.headIcon),
        ),
      ),
      SizedBox(width: 10.w),
      Expanded(child: user),
      likeButton(model),
    ]);
  }

  Widget comText(String text, int comId, String? parentName,DynamicCom model) {
    TextStyle styleN =
        TextStyle(fontSize: 28.sp, color: Colors.black.withOpacity(0.5));
    TextStyle styleS =
        TextStyle(fontSize: 28.sp, color: const Color(0xFFF1A3FF));
    Widget content;
    if (parentName != null) {
      content = DynamicText(text: "回复", textStyle: styleN, children: [
        TextSpan(text: parentName + "：", style: styleS),
        TextSpan(text: text),
      ]);
    } else {
      content = DynamicText(text: text, textStyle: styleN);
    }
    return GestureDetector(
        onTap: () => onTap(model.id, model.userInfo.userId, model.postId),
        child: FContainer(
      align: Alignment.centerLeft,
      child: content,
    ));
  }

  Widget relyCell(DynamicCom model, {String? parentName}) {
    List<Widget> children = [
      userContent(model),
      comText(dynamicCom.content, dynamicCom.id,null,dynamicCom)
    ];

    List<DynamicCom> comList = [];
    children.addAll(List.generate(
      comList.length,
      (index) => relyCell(comList[index], parentName: model.userInfo.name),
    ));

    children.add(SizedBox(height: 20.h));
    return Column(
        children: children, crossAxisAlignment: CrossAxisAlignment.start);
  }

  Widget likeButton(DynamicCom model) {
    return Column(
      children: [
        TextButton(
          onPressed: () => onLike(
              model.id, model.likeState, model.userInfo.userId, model.parentId),
          style: TextButton.styleFrom(foregroundColor: Colors.transparent),
          child: Row(children: [
            LoadAssetImage(
                model.likeState == 1 ? "dynamic/dLike" : "dynamic/dUnLike",
                width: 30.w),
            SizedBox(width: 10.w),
            SizedBox(
                child: FText("${model.likeCount}",
                    color: Colors.black, size: 22.sp))
          ]),
        ),
        GestureDetector(
          onTap: () => onTap(model.id, model.userInfo.userId, model.postId),
          child: FText(
            "回复",
            size: 24.sp,
            color: Colors.black.withOpacity(0.5),
          ),
        ),

      ],
    );
  }

  String getDyTime(int timeStamp) {
    Duration difference = DateTime.now()
        .difference(DateTime.fromMillisecondsSinceEpoch(timeStamp));
    if (difference.inMinutes < 1) {
      return "刚刚";
    } else if (difference.inHours < 1) {
      return "${difference.inMinutes}分钟前";
    }
    if (difference.inDays < 1) {
      return "${difference.inHours}小时前";
    }
    if (difference.inDays < 30) {
      return "${difference.inDays}天前";
    }
    return DateTime.fromMillisecondsSinceEpoch(timeStamp)
        .toString()
        .substring(0, 11);
  }
}
