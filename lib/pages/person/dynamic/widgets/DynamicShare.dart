import 'package:echo/import.dart';
import 'package:flutter/material.dart';
import 'package:im_flutter_sdk/im_flutter_sdk.dart';
import 'package:fluwx/fluwx.dart' as fluwx;

import '../model/dynamic_liemodel.dart';

class DynamicShare extends StatefulWidget {
  final DynamicLiemodel model;
  final bool? showMore;
  final Function(DynamicLiemodel) onDel;
  final Function(DynamicLiemodel) onUpdate;
  final Function(DynamicLiemodel) onHidden;

  const DynamicShare(
    this.model, {
    Key? key,
    this.showMore = false,
    required this.onDel,
    required this.onUpdate,
    required this.onHidden,
  }) : super(key: key);

  @override
  _DynamicShareState createState() => _DynamicShareState();
}

class _DynamicShareState extends State<DynamicShare> {
  bool hasFollow = false;
  bool hasBlack = false;

  @override
  void initState() {
    super.initState();
    requestRelation();
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      padding: EdgeInsets.only(
          top: 10.w, bottom: ScreenUtil().bottomBarHeight + 20.w),
      child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(child: FSheetLine()),
            Center(
                child: FText(
              "更多操作",
              weight: FontWeight.bold,
              color: Colors.black,
              size: 36.sp,
            )),
            SizedBox(height: 20.h),
            shareContent(),
            if (widget.showMore == true) moreContent(),
            FcationButton("取消",
                isActivate: true, radius: 90.w, onPress: onTapCancel),
          ]),
    );
  }

  Widget shareContent() {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(children: const [
        // buttonCell("dynamic/share1", "好友", onShareFriend),
        // buttonCell("dynamic/share2", "微信好友", onShareWeChat),
        // buttonCell("dynamic/share4", "朋友圈", onShareWXCircle),
        // buttonCell("dynamic/share3", "QQ好友", onShareQQ),
        // buttonCell("dynamic/share5", "QQ空间", onShareQQCircle),
      ]),
    );
  }

  Widget moreContent() {
    String userId = widget.model.userInfo?.userId ?? "";
    bool showFollow = (userId != UserConstants.userginInfo?.info.userId);
    bool showChat =
        (userId != UserConstants.userginInfo?.info.userId) && !hasBlack;
    bool showUnlike = userId != UserConstants.userginInfo?.info.userId;
    bool showReport = userId != UserConstants.userginInfo?.info.userId;
    bool showDelete = userId == UserConstants.userginInfo?.info.userId;
    bool showAuth = userId == UserConstants.userginInfo?.info.userId;
    // int selfRole = UserConstants.userInfo?.userRole ?? UserRole.DEF.index;
    int selfRole = 4;
    // int otherRole = widget.model.userInfo?.userRole ?? UserRole.DEF.index;
    // bool showHandle = userId != UserConstants.userInfo?.id &&
    //     selfRole >= UserRole.OFFICIAL.index &&
    //     selfRole > otherRole;
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(children: [
        if (showFollow) buttonCell("dynamic/bo1", "关注", onTapFollow),
        if (showChat) buttonCell("dynamic/bo2", "私聊", onTapChat),
        // if (showUnlike) buttonCell("dynamic/bo3", "不喜欢", onTapUnlike),
        if (showReport) buttonCell("dynamic/bo4", "举报", onTapReport),
        if (showDelete) buttonCell("dynamic/bo5", "删除", onTapDelete),
        // if (showAuth) buttonCell("dynamic/bo6", "权限", onTapAuth),
        // if (showHandle) buttonCell("dynamic/bo7", "隐藏动态", onTapHandle),
      ]),
    );
  }

  Widget buttonCell(String img, String text, Function() onTap) {
    Widget btn = FContainer(
      width: 150.w,
      height: 140.w,
      child: Column(children: [
        LoadAssetImage(img, width: 80.w),
        FText(text, color: Color(0xFF999999), size: 22.sp)
      ], mainAxisAlignment: MainAxisAlignment.spaceEvenly),
    );
    return GestureDetector(onTap: onTap, child: btn);
  }

  void onTapCancel() {
    WidgetUtils().popPage();
  }

  void onShareFriend() async {
    Map? userData = await WidgetUtils().showBottomSheet(FShareFriend());
    if (userData == null) return;
    String userId = userData["user"];
    String conId = userData["conId"];
    String head = userData["head"];
    String name = userData["name"];
    if (userId == UserConstants.userginInfo?.info.userId) {
      return WidgetUtils().showToast("不能给自己私信");
    }
    HxMsgExt msgExt = HxMsgExt(userId, head, name);
    String describe = "哇~这个动态真是太有内涵了，分享给你看看！";
    // String resource = widget.model.resource ?? "";
    // if (widget.model.content_text.isNotEmpty) {
    //   describe = widget.model.content_text;
    // }
    // if (resource.isNotEmpty &&
    //     widget.model.type == DynamicType.TEXT_IMAGE.index) {
    //   resource = resource.split(";").first;
    //   resource = resource.split("@").first;
    // }
    // String userName = widget.model.userInfo?.name ?? "";
    // String userHead = widget.model.userInfo?.head ?? "";
    // if (resource.isEmpty) {
    //   resource = userHead;
    // }
    // if (widget.model.userInfo != null && resource.isEmpty) {
    //   resource = widget.model.userInfo!.head;
    // }

    // HXDynamicMsg msgBody = HXDynamicMsg(widget.model.id, int.parse(widget.model.type.toString()),
    //     userName, userHead, resource, describe);

    // AppManager().sendHxChatMsg(conId, msgExt, msgBody);
    WidgetUtils().showToast("分享成功");
    WidgetUtils().popPage();
  }

  void onShareWeChat() {
    PluginUtils().weChatShareWebPage(fluwx.WeChatScene.SESSION);
  }

  void onShareWXCircle() {
    PluginUtils().weChatShareWebPage(fluwx.WeChatScene.TIMELINE);
  }

  void onShareQQ() {
    // PluginUtils().shareQQMsg(getShareData(1));
  }

  void onShareQQCircle() {
    // PluginUtils().shareQQMsg(getShareData(2));
  }

  void onTapFollow() {
    String userId = widget.model.userInfo?.userId ?? "";
    if (userId == UserConstants.userginInfo?.info.userId) return;
    DioUtils().asyncHttpRequest(
      NetConstants.followUser,
      method: DioMethod.POST,
      params: {"flow_user_id": userId, "flow_action": 10},
      onSuccess: (_) {
        // hasFollow = true;
        WidgetUtils().showToast("关注成功");
        if (!mounted) return;
        setState(() {});
      },
    );
  }

  void onTapChat() async {
    try {
      String userId = widget.model.userInfo?.userId ?? "";
      if (userId == UserConstants.userginInfo?.info.userId) return;
      String hxId = widget.model.hxuid ?? "";
      String toName = widget.model.userInfo?.name ?? "";
      String toHead = widget.model.userInfo?.headIcon ?? "";
      if (hxId.isEmpty) return;
      await WidgetUtils()
          .pushPage(MsgChat(hxId, HxMsgExt(userId, toHead, toName)));
      requestRelation();
    } on EMError catch (e) {
      debugPrint('创建会话失败: $e');
    }
  }

  void onTapUnlike() {
    widget.onHidden(widget.model);
  }

  void onTapReport() {
    WidgetUtils().pushPage(FeedbackPage(
        type: FeedbackType.DYNAMIC_REPORTING_USER,
        userId: widget.model.userId,
        dynamic_id: widget.model.id.toString()));
  }

  void onTapDelete() async {
    bool? accept = await WidgetUtils().showAlert("确认删除这一瞬间吗？");
    if (accept == null || !accept) return;

    int? dynamicId = widget.model.id;
    DioUtils().asyncHttpRequest(
      NetConstants.dynamicDel,
      method: DioMethod.POST,
      params: {"post_id": dynamicId, "user_id": widget.model.userId},
      onSuccess: (response) {
        WidgetUtils().popPage();
        widget.onDel(widget.model);
      },
    );
  }

  void onTapAuth() async {
    // DynamicAuthType? authType = await WidgetUtils()
    //     .showBottomSheet(PbAuthDialog(auth: getAuth(widget.model.authority)));
    // if (authType == null || authType.index == widget.model.authority) return;
    // requestUpdateAuth(authType);
  }

  DynamicAuthType? getAuth(int auth) {
    for (DynamicAuthType type in DynamicAuthType.values) {
      if (type.index == auth) return type;
    }
    return null;
  }

  void onTapHandle() async {
    // bool hasPwd = await hasSecondaryPwd();
    // if (!hasPwd) {
    //   return WidgetUtils().showToast("请先设置您的二级密码");
    // }
    // String? pwd = await WidgetUtils().showFDialog(SecondaryDialog());
    // if (pwd == null) return WidgetUtils().showToast("请输入二级密码");
    // if (pwd.length < 6) return WidgetUtils().showToast("二级密码输入错误");
    // requestHandle(pwd);
  }

  Future<bool> hasSecondaryPwd() async {
    bool hasPwd = false;
    await DioUtils().httpRequest(
      NetConstants.secondaryStatus,
      onSuccess: (response) => hasPwd = response == 1,
    );
    return hasPwd;
  }

  void requestHandle(String secret) {
    DioUtils().asyncHttpRequest(
      NetConstants.officialHandle,
      method: DioMethod.POST,
      params: {"id": widget.model.id, "secret": secret, "remark": ""},
      onSuccess: (response) {
        WidgetUtils().popPage();
        WidgetUtils().showToast("动态已隐藏");
        widget.onDel(widget.model);
      },
    );
  }

  ShareData getShareData(int platform) {
    ShareData shareData = ShareData();
    shareData.shareUrl = NetConstants.shareUrl + "?id=${widget.model.id}";
    shareData.shareTitle = "你的好友给你分享了一条动态，快去看看吧~";
    if (widget.model.contentText.toString().isNotEmpty) {
      shareData.shareTitle = widget.model.commentCount.toString();
    }
    shareData.shareSub = "哇~这个动态真是太有内涵了，分享给你看看！";
    if (widget.model.userInfo != null) {
      String? head = widget.model.userInfo?.headIcon;
      shareData.thumbUrl = NetConstants.ossPath + head!;
    }
    shareData.platform = platform;
    return shareData;
  }

  // "isFriend":false,"iFollowThey":false,"theyFollowMe":false,"eachBlack":false,"iBlackThey":false,"theyBlackMe":false
  void requestRelation() {
    String userId = widget.model.userInfo?.userId ?? "";
    if (userId == UserConstants.userginInfo?.info.userId) return;
    if (widget.showMore == false) return;
    DioUtils().asyncHttpRequest(
      NetConstants.getRelation,
      params: {"user_id": userId},
      method: DioMethod.POST,
      onSuccess: (response) {
        if (response?["theyBlackMe"] == true ||
            response?["eachBlack"] == true ||
            response?["iBlackThey"] == true) {
          this.hasBlack = true;
        }
        if (response?["iFollowThey"] == true || response?["isFriend"] == true) {
          this.hasFollow = true;
        }
        if (!mounted) return;
        setState(() {});
      },
    );
  }

  void requestUpdateAuth(DynamicAuthType type) {
    DioUtils().asyncHttpRequest(
      NetConstants.dynamicAuth,
      method: DioMethod.POST,
      params: {"id": widget.model.id, "types": type.index},
      onSuccess: (res) => onUpdateAuth(type),
    );
  }

  void onUpdateAuth(DynamicAuthType type) {
    // widget.model.visibility = type.index;
    WidgetUtils().popPage();
    widget.onUpdate(widget.model);
  }
}
