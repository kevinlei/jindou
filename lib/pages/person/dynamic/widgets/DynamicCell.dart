import 'package:cached_network_image/cached_network_image.dart';
import 'package:echo/import.dart';
import 'package:flutter/material.dart';
import 'package:im_flutter_sdk/im_flutter_sdk.dart';

import '../../../../widgets/Foindmater.dart';
import '../model/dynamic_liemodel.dart';

class DynamicCell extends StatelessWidget {
  final DynamicLiemodel model;
  final Function(DynamicLiemodel) onDel;
  final Function(DynamicLiemodel) onUpdate;
  final Function(DynamicLiemodel) onHidden;
  final bool? enableTap;
  final bool? toHomepage;
  final bool? showDivider;

  DynamicCell(
    this.model, {
    Key? key,
    required this.onUpdate,
    required this.onDel,
    required this.onHidden,
    this.enableTap = true,
    this.toHomepage = true,
    this.showDivider = false,
  }) : super(key: key);
  bool hasFollow = false;
  bool hasBlack = false;
  final List<Color> topicColor1 = [
    const Color(0xFF3DB69B),
    const Color(0xFFAEA01D),
    const Color(0xFF7D5BB4),
  ];
  final List<Color> topicColor2 = [
    const Color(0xFF6CA494).withOpacity(0.2),
    const Color(0xFFA4946C).withOpacity(0.2),
    const Color(0xFF816CA4).withOpacity(0.2),
  ];

  @override
  Widget build(BuildContext context) {
    Widget child =
        Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      userInfoContent(),
      // SizedBox(height: 5.h),

      InfoConten(),
      if (showDivider == true) const Divider(color: Color(0xFFD8D8D8)),
    ]);

    child = FContainer(
      color: Colors.transparent,
      // gradient: [Color(0xFF322D4D), Color(0xFF29243C)],
      // gradientBegin: Alignment.topLeft,
      // gradientEnd: Alignment.bottomRight,
      radius: BorderRadius.circular(20.w),
      margin: EdgeInsets.only(
        left: 10.w,
        right: 10.w,
        top: 0.w,
        bottom: 0.w,
      ),
      padding: EdgeInsets.fromLTRB(25.w, 20.w, 25.w, 0),
      child: child,
    );
    if (enableTap == false) return child;
    return GestureDetector(onTap: onTapDynamic, child: child);
  }

  Widget InfoConten() {
    return FContainer(
      color: Colors.transparent,
      // gradient: [Color(0xFF322D4D), Color(0xFF29243C)],
      // gradientBegin: Alignment.topLeft,
      // gradientEnd: Alignment.bottomRight,
      radius: BorderRadius.circular(20.w),
      margin: EdgeInsets.only(
        left: 90.w,
        right: 0.w,
        top: 0.w,
        bottom: 0.w,
      ),
      padding: EdgeInsets.fromLTRB(15.w, 0.w, 0.w, 0),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        DynamicText(text: model.contentText.toString()),
        dyContent(),
        //暂时屏蔽
        // dyTopics(),
        dynamicBottom(),
      ]),
    );
  }

  Widget userInfoContent() {
    String userHead = model.userInfo?.headIcon ?? "";
    String userName = model.userInfo?.name ?? "";
    String userIdStr = model.userInfo?.userId ?? "";
    int userSex = model.userInfo?.sex ?? 0;
    if (userHead.isNotEmpty) {
      userHead =
          "$userHead?x-oss-process=image/resize,m_fill,w_100,h_100/quality,q_60";
    }
    ImageProvider headImage =
        CachedNetworkImageProvider(NetConstants.ossPath + userHead);

    Widget headWidget = GestureDetector(
      onTap: onTapHead,
      child: CircleAvatar(
          radius: 45.r,
          backgroundColor: const Color(0xFF999999),
          backgroundImage: headImage),
    );
    headWidget = Stack(alignment: Alignment.bottomRight, children: [
      headWidget,
      // LoadAssetImage(userSex == 2 ? 'dynamic/sex2' : 'dynamic/sex1',
      //     width: 20.w)
    ]);

    Widget nameWidget = ConstrainedBox(
      constraints: BoxConstraints(maxWidth: 460.w),
      child: FText(
        userName,
        weight: FontWeight.bold,
        color: Colors.black,
        size: 32.sp,
      ),
    );

    return Row(children: [
      headWidget,
      SizedBox(width: 20.w),
      Expanded(
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          nameWidget,
          //性别 年龄  地址
          FText(
              userSex == 0
                  ? '女 | ${UserConstants.getUserAge(model.userInfo?.birthday ?? 0)}岁 | ${model.address ?? ""}'
                  : '男 | ${UserConstants.getUserAge(model.userInfo?.birthday ?? 0)}岁 | ${model.address ?? ""}',
              color: const Color(0xFF999999),
              size: 24.sp),
        ]),
      ),
      //   (userIdStr == UserConstants.userginInfo?.info.userId)?
      //     KsPopupMenuButton(
      //       shape: const TooltipShape(),
      //       color: const Color(0xFF252734),
      //       offset: Offset(30, 60.h),
      //       elevation: 0,
      //       itemBuilder: (_) => [
      //         popupMenuItem(
      //             "删帖", onTapDelete),
      //       ],
      //       child: dyBtnMore(),
      //     ):KsPopupMenuButton(
      //   shape: const TooltipShape(),
      //   color: const Color(0xFF252734),
      //   offset: Offset(30, 60.h),
      //   elevation: 0,
      //   itemBuilder: (_) => [
      //
      //     popupMenuItem(model.isfollow == 0?"关注":"取消关注", onTapFollow),
      //
      //     popupMenuItem(
      //         "私聊", onTapChat),
      //
      //     popupMenuItem(
      //         "举报", onTapBlack),
      //   ],
      //   child: dyBtnMore(),
      // ),
      dyBtnMore(),
    ]);
  }

  Widget dyBtnMore() {
    return IconButton(
        onPressed: onTapMore,
        icon: LoadAssetImage("dynamic/mMore", width: 45.r));
  }

  Widget dyTopics() {
    List<String> topicreList = [];
    topicreList.add(model.topicName1 ?? "");
    topicreList.add(model.topicName2 ?? "");
    topicreList.add(model.topicName3 ?? "");
    // List<DynamicTopic> topicList = model.topicInfo ?? [];
    List<Widget> children = List.generate(
        topicreList.length, (index) => topicCell(index, topicreList[index]));
    // if (model.position != null) {
    //   children.insertAll(0, [
    //     LoadAssetImage("dynamic/loc", width: 28.w),
    //     FText(model.position?.location ?? "",
    //         size: 22.sp, color: const Color(0xFF666666))
    //   ]);
    // }
    return Padding(
      padding: EdgeInsets.only(top: 10.h),
      child: Wrap(spacing: 10.w, runSpacing: 10.h, children: children),
    );
  }

  Widget topicCell(int index, topic) {
    return FContainer(
      padding: EdgeInsets.fromLTRB(20.w, 0, 20.w, 0),
      color: topicColor2[index % 3],
      height: 40.w,
      radius: BorderRadius.circular(40.w),
      child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        FText("#${topic}", size: 22.sp, color: topicColor1[index % 3])
      ]),
    );
  }

  Widget dyContent() {
    int? dyType = model.contentType;
    if (dyType == DynamicType.TEXT_IMAGE.index) {
      return DynamicImg(model.resourceUrl.join(","));
    } else if (dyType == DynamicType.TEXT_AUDIO.index) {
      return DynamicAudio(model.resourceUrl.join(","));
    }
    // else if (dyType == DynamicType.VOTE_TEXT.index) {
    //   return DynamicVote(model.votesInfo ?? [], model.comment_count,
    //       onVote: requestVoteDy, isText: true);
    // } else if (dyType == DynamicType.VOTE_IMAGE.index) {
    //   return DynamicVote(model.votesInfo ?? [], model.comment_count,
    //       onVote: requestVoteDy, isText: false);
    // }
    return const SizedBox();
  }

  Widget dynamicBottom() {
    int shareNum = model.forwardCount ?? 0,
        comNum = model.commentCount ?? 0,
        likeNum = model.likeCount ?? 0;
    Widget bottom = Row(children: [
      Expanded(
        child:
            FText(getTimeString(), color: const Color(0xFF999999), size: 26.sp),
      ),
      Row(
        children: [
          // btnBottom(
          //     "dynamic/dShare", shareNum > 0 ? "$shareNum" : "0", onTapShare),
          btnBottom("dynamic/dMsg", comNum > 0 ? "$comNum" : "0", null),
          btnBottom(model.likeState == 1 ? "dynamic/dLike" : "dynamic/dUnLike",
              likeNum > 0 ? "$likeNum" : "0", onTapLike)
        ],
      ),
    ]);

    //   Widget bottom = Row(children: [
    //       Expanded(
    //       child:
    //       FText(getTimeString(), color: const Color(0xFF999999), size: 26.sp),
    //   ),
    // ]);
    return bottom;
  }

  Widget btnBottom(String img, String text, Function()? onPress) {
    return TextButton(
      onPressed: onPress,
      style: TextButton.styleFrom(foregroundColor: Colors.transparent),
      child: Row(children: [
        LoadAssetImage(
          img,
          width: 32.w,
          height: 28.w,
          fit: BoxFit.fitWidth,
        ),
        SizedBox(width: 5.w),
        SizedBox(
            child: FText(
          text,
          size: 22.sp,
          color: const Color(0xFF222222),
          align: TextAlign.right,
        ))
      ]),
    );
  }

  String getTimeString() {
    int? timeStamp = model.createdAt;
    Duration difference = DateTime.now()
        .difference(DateTime.fromMillisecondsSinceEpoch(timeStamp!));
    if (difference.inMinutes < 1) {
      return "刚刚";
    } else if (difference.inHours < 1) {
      return "${difference.inMinutes}分钟前";
    }
    if (difference.inDays < 1) {
      return "${difference.inHours}小时前";
    }
    if (difference.inDays < 30) {
      return "${difference.inDays}天前";
    }
    return DateTime.fromMillisecondsSinceEpoch(timeStamp)
        .toString()
        .substring(0, 10);
  }

  void requestVoteDy(int voteId) {
    DioUtils().asyncHttpRequest(
      NetConstants.dynamicVote,
      method: DioMethod.POST,
      params: {
        "type": 2,
        "dynamic_id": model.id,
        "dynamic_vote_id": voteId,
      },
      onSuccess: (_) => onVoteCell(voteId),
    );
  }

  void onTapDelete() async {
    bool? accept = await WidgetUtils().showAlert("确认删除这一瞬间吗？");
    if (accept == null || !accept) return;

    int? dynamicId = model.id;
    DioUtils().asyncHttpRequest(
      NetConstants.dynamicDel,
      method: DioMethod.POST,
      params: {"post_id": dynamicId, "user_id": model.userInfo?.userId},
      onSuccess: (response) {
        WidgetUtils().popPage();
        onDel:
        onDel;
      },
    );
  }

  void onVoteCell(int voteId) {
    // int? index = model.votesInfo?.indexWhere((element) => element.id == voteId);
    // if (index == null) return;
    // model.votesInfo?[index].ifConfirm = 1;
    // model.votesInfo?[index].count += 1;
    // model.like_count += 1;
    // onUpdate(model);
  }

  void requestLike() {
    DioUtils().asyncHttpRequest(
      NetConstants.dynamicVote,
      method: DioMethod.POST,
      params: {"type": 1, "post_id": model.id, "comment_id": 0},
      onSuccess: onLikeCell,
    );
  }

  void onLikeCell(response) {
    int like = model.likeState ?? 0;
    int num = model.likeCount ?? 0;
    if (like == 1) {
      like = 2;
      num -= 1;
    } else {
      like = 1;
      num += 1;
    }
    model.likeState = like;
    model.likeCount = num;
    onUpdate(model);
  }

  void onTapHead() {
    if (toHomepage == false) return;
    String userId = model.userInfo?.userId ?? "";
    if (userId.isEmpty) return;
    WidgetUtils().pushPage(PersonHomepage(userId: userId));
  }

  void onTapFollow() {
    String userId = model.userInfo?.userId ?? "";
    if (userId == UserConstants.userginInfo?.info.userId) return;
    DioUtils().asyncHttpRequest(
      NetConstants.followUser,
      method: DioMethod.POST,
      params: {
        "flow_user_id": userId,
        "flow_action": model.isfollow == 0 ? 10 : 20
      },
      onSuccess: (_) {
        hasFollow = true;
        model.isfollow == 0;
        // if (!mounted) return;
      },
    );
  }

  void onTapMore() {
    WidgetUtils().showBottomSheet(
        DynamicShare(
          model,
          showMore: true,
          onDel: onDel,
          onUpdate: onUpdate,
          onHidden: onHidden,
        ),
        color: Colors.white);
  }

  Widget dressImage() {
    return FContainer(
      height: 30.h,
      margin: EdgeInsets.symmetric(horizontal: 10.w),
      radius: BorderRadius.circular(30.w),
      align: Alignment.center,
      child: Row(children: [
        SizedBox(width: 5.w),
        LoadAssetImage("person/xiaPrice", width: 25.w),
      ]),
    );
  }

  KsPopupMenuItem popupMenuItem(String t, Function() tap) {
    return KsPopupMenuItem(
        onTap: tap,
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(width: 15.w),
              Container(
                child: FText(t,
                    color: const Color(0xFFFFFFFF),
                    size: 26.sp,
                    align: TextAlign.center),
              ),
              // Visibility(
              //   visible: vates == t,
              //   child: SizedBox(width: 5.w),
              // ),
              // Visibility(
              //   maintainAnimation: true,
              //   maintainSize: true,
              //   maintainState: true,
              //   visible: vates == t,
              //   child: LoadAssetImage(r, width: 24.w),
              // ),
              SizedBox(width: 15.w),
            ],
          ),
        ));
  }

  void onTapBlack() {}
  void onTapChat() async {
    try {
      String userId = model.userInfo?.userId ?? "";
      if (userId == UserConstants.userginInfo?.info.userId) return;
      String hxId = model.hxuid ?? "";
      String toName = model.userInfo?.name ?? "";
      String toHead = model.userInfo?.headIcon ?? "";
      if (hxId.isEmpty) return;
      await WidgetUtils()
          .pushPage(MsgChat(hxId, HxMsgExt(userId, toHead, toName)));
      // requestRelation();
    } on EMError catch (e) {
      print('创建会话失败: $e');
    }
  }

//分享暂时不做
  void onTapShare() {
    // WidgetUtils().showBottomSheet(DynamicShare(
    //   model,
    //   onUpdate: onUpdate,
    //   onDel: onDel,
    //   onHidden: onHidden,
    // ));
  }

  void onTapLike() {
    requestLike();
  }

  void onTapDynamic() async {
    bool? del = await WidgetUtils().pushPage(CommentPage(dynamicId: model.id));
    if (del == null || !del) return;
    onDel(model);
  }
}
