import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class DynamicLabel extends StatefulWidget {
  final Function(int) onSelect;
  const DynamicLabel(this.onSelect, {Key? key}) : super(key: key);

  @override
  _DynamicLabelState createState() => _DynamicLabelState();
}

class _DynamicLabelState extends State<DynamicLabel> {
  List chargeList = [];
  int chargeNum = 0;

  @override
  void initState() {
    super.initState();
    requestChargeMap();
  }

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
        crossAxisSpacing: 20.w,
        mainAxisSpacing: 20.w,
        mainAxisExtent: 80.w,
      ),
      itemCount: chargeList.length,
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemBuilder: (_, int index) =>
          chargeCell(chargeList[index]['name'], chargeList[index]['type']),
    );
  }

  Widget chargeCell(String key, int value) {
    Color colorBg = Color(0xFFF5F6F7);
    Color colorBorder = value == chargeNum ? Color(0xFFFF753F) : Color(0x80222222);
    Color textColor = value == chargeNum ? Color(0xFFFF753F) : Color(0xFF222222);


    Widget child = Column(children: [
      FText(key, size: 28.sp,color:textColor,),
      // FText("$value", size: 24.sp, color:Color(0xFF666666)),
    ], mainAxisAlignment: MainAxisAlignment.spaceEvenly);

    Widget btn = FContainer(
      radius: BorderRadius.circular(40.w),
      color: colorBg,
      border: Border.all(color: colorBorder,width: 2.w),
      child: child,
    );
    return GestureDetector(onTap: () => onTapCharge(value), child: btn);
  }

  void requestChargeMap() {
    DioUtils().asyncHttpRequest(
      NetConstants.recordAudio,
      method: DioMethod.GET,
      params: {"conf_key": "inform_dynamic"},
      onSuccess: onChargeMap,
    );
  }

  void onChargeMap(response) {
    var jsonStr = jsonDecode(response);
    jsonStr ??= [];
    chargeList.clear();
    chargeList=jsonStr;
    if (!mounted) return;
    setState(() {});
  }

  void onTapCharge(int num) {
    if (chargeNum == num) return;
    chargeNum = num;
    widget.onSelect(num);
    if (!mounted) return;
    setState(() {});
  }
}
