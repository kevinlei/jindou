import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class HomeDynamicImg extends StatelessWidget {
  final String imgList;

  const HomeDynamicImg(this.imgList, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<String> pathList = imgList.split(",");
    if (imgList.length == 1) {
      List<String> imageData = pathList[0].split(",");
      String imagePath = imageData[0];
      imagePath += OssThumb.OSS_IM_FILL.path;
      Widget image = ClipRRect(
        borderRadius: BorderRadius.circular(10.r),
        child: ConstrainedBox(
          constraints: BoxConstraints(maxWidth: 500.w, maxHeight: 360.w),

          child: LoadNetImage(
            NetConstants.ossPath + imagePath,
          ),
        ),
      );
      return GestureDetector(
          onTap: () => onPressImage(pathList, 0), child: image);
    }
    return imgListContent(pathList);
  }

  Widget imgListContent(List<String> pathList) {
    List<Widget> photos = List.generate(1, (index) {
      List<String> imageData = pathList[index].split("@");
      imageData[0] += OssThumb.OSS_IM_FILL.path;
      Widget image = ClipRRect(
        borderRadius: BorderRadius.circular(10.r),
        child: LoadNetImage(NetConstants.ossPath + imageData[0],
            width: 240.r, height: 150.r),
      );

      return GestureDetector(
          onTap: () => onPressImage(pathList, index), child: image);
    });

    return Wrap(spacing: 10.r, runSpacing: 10.r, children: photos);
  }

  void onPressImage(List<String> pathList, int index) {
    List<String> imageList = [];
    for (String path in pathList) {
      imageList.add(path.split("@")[0]);
    }
    WidgetUtils().pushPage(PhotoPreview(imageList, index: index));
  }
}
