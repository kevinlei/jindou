import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class DynamicVote extends StatelessWidget {
  final List<DynamicVoteModel> options;
  final int total;
  final bool isText;
  final Function(int) onVote;
  DynamicVote(this.options, this.total,
      {Key? key, required this.isText, required this.onVote})
      : super(key: key);

  final List<String> prefix = ["A", "B", "C", "D"];
  final List<Color> gradientColor = [
    Color(0xFFDEEEFF),
    Colors.transparent,
    Colors.transparent
  ];

  @override
  Widget build(BuildContext context) {
    bool hasVote = hasSelfVote();
    List<Widget> voteOptions;
    options.sort((a, b) => a.id.compareTo(b.id));
    if (isText) {
      voteOptions = List.generate(
          options.length, (index) => voteTextOption(index, hasVote));
    } else {
      voteOptions = List.generate(
          options.length, (index) => voteImgOption(index, hasVote));
      voteOptions = [
        Wrap(children: voteOptions, spacing: 10.w, runSpacing: 10.w),
        SizedBox(height: 20.w)
      ];
    }

    voteOptions.add(Row(children: [
      LoadAssetImage("dynamic/dVoteIcon", width: 30.w),
      SizedBox(width: 20.w),
      FText("$total人参与投票", size: 24.sp, color: Colors.black.withOpacity(0.5),)
    ]));

    return Column(
        children: voteOptions,
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start);
  }

  Widget voteTextOption(int index, bool hasVote) {
    Color optionColor = hasVote ? Color(0xFFB1D7FF) : Color(0xFF999999);
    // 文本内容
    Widget textContent = Expanded(
      child: FText("${prefix[index]}. ${options[index].content}",
          overflow: TextOverflow.ellipsis, size: 28.sp, color: optionColor),
    );
    // 进度
    int progress = getVoteProgress(index);
    // 进度文本
    Widget textProgress = FText("$progress%",
        color: hasVote ? Color(0xFF1287FF) : Colors.transparent, size: 28.sp);

    Widget voteCard = FContainer(
      height: 90.w,
      padding: EdgeInsets.symmetric(horizontal: 20.w),
      radius: BorderRadius.circular(10.w),
      border: Border.all(color: optionColor, width: 0.5.w),
      gradientBegin: Alignment.centerLeft,
      gradientEnd: Alignment.centerRight,
      gradient: hasVote ? gradientColor : null,
      stops: [progress / 100, progress / 100, 1],
      margin: EdgeInsets.only(right: 100.w, bottom: 20.w),
      child: Row(children: [textContent, SizedBox(width: 20.w), textProgress]),
    );

    return GestureDetector(
        onTap: () => hasVote ? null : onPressVote(index), child: voteCard);
  }

  Widget voteImgOption(int index, bool hasVote) {
    String image = options[index].content + OssThumb.OSS_IM_FILL.path;
    Widget photoCard = ClipRRect(
      borderRadius: BorderRadius.only(
          topRight: Radius.circular(10.w), topLeft: Radius.circular(10.w)),
      child: LoadNetImage(NetConstants.ossPath + image,
          width: 280.w, height: 240.w),
    );
    // 边框
    Border border = Border.all(
        color: hasVote ? Color(0xFFB1D7FF) : Color(0xFFE7E8F1), width: 0.5.w);

    int progress = getVoteProgress(index);
    // 进度文本
    Widget textProgress = FText(hasVote ? "$progress%" : "${prefix[index]}",
        color: hasVote ? Color(0xFF1287FF) : Colors.black, size: 28.sp);

    Widget progressCard = FContainer(
      width: 280.w,
      height: 75.w,
      color: hasVote ? Color(0xFFDEEEFF) : Color(0xFFFFFFFF),
      radius: BorderRadius.only(
          bottomLeft: Radius.circular(10.w),
          bottomRight: Radius.circular(10.w)),
      align: Alignment.center,
      border: border,
      child: textProgress,
    );

    Widget voteCard = Column(
        children: [photoCard, progressCard], mainAxisSize: MainAxisSize.min);
    return GestureDetector(
        onTap: () => hasVote ? null : onPressVote(index), child: voteCard);
  }

  bool hasSelfVote() {
    for (DynamicVoteModel vote in options) {
      if (vote.ifConfirm == 1) return true;
    }
    return false;
  }

  int getVoteProgress(int index) {
    if (total == 0) return 0;

    if (index < options.length - 1) {
      int count = options[index].count;
      return (count / total * 100).floor();
    } else {
      int count = 0;
      for (int i = 0; i < options.length - 1; i++) {
        count += (options[i].count / total * 100).floor();
      }
      return 100 - count;
    }
  }

  // 1:动态点赞 2:投票 3:评论点赞
  void onPressVote(int index) => onVote(options[index].id);
}
