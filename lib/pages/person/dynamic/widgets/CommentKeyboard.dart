import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:flutter/services.dart';

class CommentKeyboard extends StatefulWidget {
  final FocusNode focusNode;
  final Function(String) onSend;

  const CommentKeyboard(this.focusNode, this.onSend, {Key? key})
      : super(key: key);

  @override
  _CommentKeyboardState createState() => _CommentKeyboardState();
}

class _CommentKeyboardState extends State<CommentKeyboard> {
  // 是否正在输入
  bool isInputting = false;
  TextEditingController controller = TextEditingController();
// 是否打开键盘
  bool isEmo = false;
  @override
  Widget build(BuildContext context) {
    return RawKeyboardListener(
      focusNode: FocusNode(),
      onKey: (event) {
        if (event.runtimeType == RawKeyDownEvent) {
          if (event.logicalKey.keyId == 4294967309) {
            onTapSend();
          }
        }
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        mainAxisSize: MainAxisSize.min,
        children: [
          FContainer(
            color: Colors.white,
            padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 10.w),
            child: Row(children: [
              SizedBox(width: 20.w),
              Expanded(child: textInput()),
              SizedBox(width: 20.w),
            ]),
          ),
          operationPanel(),
        ],
      ),
    );
  }

  Widget operationPanel() {
    if (isEmo) {
      return SizedBox(
        height: 500.w,
        child: EmotionFooter(
          controller,
          color: Colors.white,
        ),
      );
    } else {
      return SizedBox(height: 0);
    }
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  Widget keyboardMenu() {
    BoxShadow shadow = BoxShadow(
      color: Color(0xFF000000).withOpacity(0.05),
      spreadRadius: 2,
      blurRadius: 6,
    );
    return FContainer(
      height: 100.h,
      shadow: [shadow],
      color: Color(0xFFFFFFFF),
      padding: EdgeInsets.symmetric(horizontal: 25.w, vertical: 10.h),
      child: Row(children: [
        SizedBox(width: 20.w),
        Expanded(child: textInput()),
        SizedBox(width: 20.w),
        // if (isInputting)
        // ElevatedButton(onPressed: onTapSend, child: Text("发送")),
      ]),
    );
  }

  Widget textInput() {
    InputBorder border = OutlineInputBorder(
        borderRadius: BorderRadius.circular(50.w),
        borderSide: BorderSide(color: Colors.transparent));
    return TextField(
      maxLines: 2,
      minLines: 1,
      controller: controller,
      onChanged: onTextInput,
      focusNode: widget.focusNode,
      inputFormatters: [FilteringTextInputFormatter.deny(RegExp(r"\n"))],
      onTap: () {
        if (isEmo) {
          setState(() {
            isEmo = false;
          });
        }
      },
      style: TextStyle(color: Colors.black, fontSize: 30.sp),
      decoration: InputDecoration(
        fillColor: Color(0xFF222222).withOpacity(0.05),
        filled: true,
        contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 20.w),
        enabledBorder: border,
        focusedBorder: border,
        hintStyle: TextStyle(color: Colors.grey, fontSize: 24.sp),
        hintText: "输入评论让世界更有趣~",
        suffixIcon: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            GestureDetector(
                onTap: onTapEmo,
                child: LoadAssetImage(
                  "room/em",
                  width: 45.w,
                  color: Colors.white,
                )),
            SizedBox(width: 10.w),
            GestureDetector(
              onTap: onTapSend,
              child: FContainer(
                  radius: const BorderRadius.all(Radius.circular(50)),
                  gradient: const [
                    Color(0xFFFF753F),
                    Color(0xFFFF753F),
                    Color(0xFFFF753F)
                  ],
                  gradientBegin: Alignment.topLeft,
                  gradientEnd: Alignment.bottomRight,
                  align: Alignment.center,
                  width: 120.w,
                  height: 55.w,
                  child: FText(
                    "发送",
                    size: 24.sp,
                    color: Colors.white,
                  )),
            ),
            SizedBox(width: 10.w),
          ],
        ),
      ),
    );
  }

  void onTapEmo() {
    if (!isEmo) {
      SystemChannels.textInput.invokeMethod('TextInput.hide');
      setState(() {
        isEmo = true;
      });
    }
  }

  void onTextInput(String value) {
    if ((!isInputting && value.isNotEmpty) || (isInputting && value.isEmpty)) {
      isInputting = value.isNotEmpty;
      if (!mounted) return;
      setState(() {});
    }
  }

  void onTapSend() async {
    String text = controller.text;
    if (text.isEmpty) return WidgetUtils().showToast("请输入要发送的内容");
    if (controller.text.trimRight().isEmpty) return;
    if (isEmo) {
      setState(() {
        isEmo = false;
      });
    }
    WidgetUtils().showLoading();
    // bool lawful =
    //     await FuncUtils().checkTextLawful(controller.text, CheckTxtType.circle);
    // if (!lawful) return WidgetUtils().cancelLoading();
    widget.onSend(controller.text.trimRight());
    WidgetUtils().cancelLoading();
    controller.clear();
    isInputting = false;
    if (!mounted) return;
    setState(() {});
  }
}
