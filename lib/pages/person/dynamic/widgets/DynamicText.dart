import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class DynamicText extends StatefulWidget {
  final String text;
  final TextStyle? textStyle;
  final int? maxLine;
  final bool? expand;
  final List<InlineSpan>? children;

  const DynamicText(
      {required this.text,
      this.textStyle,
      this.maxLine,
      this.expand,
      this.children});

  @override
  _DynamicTextState createState() => _DynamicTextState();
}

class _DynamicTextState extends State<DynamicText> {
  bool expand = false;

  @override
  void initState() {
    super.initState();
    expand = widget.expand ?? false;
  }

  @override
  Widget build(BuildContext context) {
    if (widget.text.isEmpty) return SizedBox();
    return LayoutBuilder(builder: (_, size) {
      final TextSpan textSpan = TextSpan(
        text: (widget.text),
        style:
            widget.textStyle ?? TextStyle(color: Colors.black, fontSize: 32.sp),
        children: widget.children,
      );
      final TextPainter textPainter = TextPainter(
          text: textSpan,
          maxLines: widget.maxLine ?? 3,
          textDirection: TextDirection.ltr);
      textPainter.layout(maxWidth: size.maxWidth);

      if (textPainter.didExceedMaxLines) {
        return Column(children: [
          RichText(
              text: textSpan,
              maxLines: expand ? 100 : (widget.maxLine ?? 3),
              overflow: TextOverflow.ellipsis),
          Align(
            alignment: Alignment.centerLeft,
            child: GestureDetector(
            onTap: onPressExpand,
              child: FText(expand ? "收起" : "全文",
               color:Color(0xFFFF753F), size: 32.sp),
            ),
            // child: TextButton(
            //     onPressed: onPressExpand,
            //     child: FText(expand ? "收起" : "全文",
            //         color:Color(0xFFFF753F), size: 24.sp)),
          ),
          SizedBox(height: 10.h),
        ], crossAxisAlignment: CrossAxisAlignment.start);
      }
      return RichText(text: textSpan);
    });
  }

  void onPressExpand() {
    expand = !expand;
    if (!mounted) return;
    setState(() {});
  }
}
