import 'package:json_annotation/json_annotation.dart';

part 'publish_topic.g.dart';

@JsonSerializable()
class PublishTopic extends Object {
  @JsonKey(name: 'id')
  late int id;

  @JsonKey(name: 'name')
  late String topic;

  @JsonKey(name: 'count')
  late int count;

  PublishTopic();

  factory PublishTopic.fromJson(Map<String, dynamic> srcJson) =>
      _$PublishTopicFromJson(srcJson);

  Map<String, dynamic> toJson() => _$PublishTopicToJson(this);
}
