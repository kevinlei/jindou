// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'publish_topic.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PublishTopic _$PublishTopicFromJson(Map<String, dynamic> json) => PublishTopic()
  ..id = json['id'] as int
  ..topic = json['name'] as String
  ..count = json['count'] as int;

Map<String, dynamic> _$PublishTopicToJson(PublishTopic instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.topic,
      'count': instance.count,
    };
