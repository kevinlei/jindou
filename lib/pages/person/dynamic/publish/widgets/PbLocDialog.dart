import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:flutter/services.dart';
import 'package:permission_handler/permission_handler.dart';

class PbLocDialog extends StatefulWidget {
  const PbLocDialog({Key? key}) : super(key: key);

  @override
  State<PbLocDialog> createState() => _PbLocDialogState();
}

class _PbLocDialogState extends State<PbLocDialog> {
  TextEditingController controller = TextEditingController();

  bool isInputting = false;
  List<LocationSearch> searchList = [];
  List searList = [];

  @override
  void initState() {
    super.initState();
    PluginUtils().setListener("PbLocDialog", onLocation);
    getSelfLoc();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return FContainer(
      padding: EdgeInsets.fromLTRB(20.w, 20.w, 20.w, 0),
      height: size.height * 0.8,
      child: Column(children: [
        FSheetLine(),
        FText(
          "选择地点",
          size: 32.sp,
          weight: FontWeight.bold,
          color: Colors.black,
        ),
        Row(children: [
          Expanded(child: inputContent()),
          Visibility(
            visible: isInputting,
            child: TextButton(
                onPressed: onTapSearch,
                child: FText(
                  "搜索",
                  color: Colors.black,
                )),
          )
          // TextButton(onPressed: onTapSearch, child: FText("搜索",color: Colors.black,)),
        ]),
        buttonUnLoc(),
        Divider(color: Color(0xFFEEF1F4)),
        Expanded(child: listContent())
      ], mainAxisSize: MainAxisSize.min),
    );
  }

  @override
  void deactivate() {
    PluginUtils().setListener("PbLocDialog", null);
    super.deactivate();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  Widget listContent() {
    if (searList.isEmpty) return FEmptyWidget();
    return ListView.separated(
      itemBuilder: (_, index) => locationCell(index),
      separatorBuilder: (_, __) => Divider(color: Color(0xFFEEF1F4)),
      itemCount: searList.length,
    );
  }

  Widget locationCell(int index) {
    Widget btn = FContainer(
      height: 110.w,
      child: Row(children: [
        LoadAssetImage("barone/loc2", width: 35.w),
        SizedBox(width: 20.w),
        Expanded(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                FText(
                  searList[index]['name'],
                  size: 28.sp,
                  color: Colors.black,
                ),
                FText(searList[index]['address'],
                    size: 24.sp, color: Color(0xFF999999)),
              ]),
        )
      ]),
    );
    return GestureDetector(onTap: () => onTapCell(index), child: btn);
  }

  Widget inputContent() {
    return FContainer(
      height: 60.w,
      margin: EdgeInsets.symmetric(vertical: 30.h),
      radius: BorderRadius.circular(60.w),
      color: Color(0xFF222222).withOpacity(0.05),
      padding: EdgeInsets.symmetric(horizontal: 20.w),
      child: Row(children: [
        LoadAssetImage("barone/search", width: 30.w),
        SizedBox(width: 10.w),
        Expanded(child: inputField()),
      ]),
    );
  }

  Widget inputField() {
    InputBorder border =
        OutlineInputBorder(borderSide: BorderSide(color: Colors.transparent));
    return TextField(
      minLines: 1,
      controller: controller,
      onChanged: onTextInput,
      // inputFormatters: [FilteringTextInputFormatter.deny(RegExp(r"[\s]"))],
      style: TextStyle(color: Colors.black, fontSize: 26.sp),
      decoration: InputDecoration(
        contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10.w),
        enabledBorder: border,
        focusedBorder: border,
        counterText: "",
        hintStyle: TextStyle(color: Color(0xB0BBBBBB), fontSize: 26.sp),
        hintText: "输入你的地址吧~",
      ),
    );
  }

  Widget buttonUnLoc() {
    Widget btn = FContainer(
      height: 110.w,
      child: Row(children: [
        LoadAssetImage("barone/loc3", width: 35.w),
        SizedBox(width: 20.w),
        FText(
          "不显示位置",
          size: 28.sp,
          color: Color(0xFFFF753F),
        ),
        Spacer(),
        // LoadAssetImage("barone/check2", width: 30.w)
      ]),
    );
    return GestureDetector(onTap: onTapUnLoc, child: btn);
  }

  void onTextInput(String value) {
    if ((isInputting && value.isEmpty) || (!isInputting && value.isNotEmpty)) {
      isInputting = value.isNotEmpty;
      if (!mounted) return;
      setState(() {});
    }
  }

  void getSelfLoc() async {
    // bool accept =
    //     await FuncUtils().requestPermission(Permission.locationWhenInUse);
    // if (!accept) return;

    PluginUtils().initLocation('longitude', 'latitude');
  }

  void onLocation(String key, String method, data) {
    print('object');
    print(key);
    print(method);
    print(data);
    print(data['message'].toList()[0]);
    searList = data['message'].toList();
    print(searList);
    setState(() {});
    // if (method == 'PbLocDialog') {
    //   LocationResult result = LocationResult(data['message']);
    // print(result);
    //   print(result.clientKey);
    // if (result.clientKey != "PbLocDialog") return;
    // UserConstants.location = result;
    // print(UserConstants.location.toString());
    // } else if (method == ChannelMethod.LOC_SEARCHED.method) {
    //   if (data is! List) return;
    //   searchList.clear();
    //   for (var item in data) {
    //     LocationSearch search = LocationSearch(item);
    //     searchList.add(search);
    //   }
    //   if (!mounted) return;
    // }
  }

  void onTapUnLoc() {
    FocusManager.instance.primaryFocus?.unfocus();
    WidgetUtils().popPage({"show": false, "loc": "", "lat": "", "lon": ""});
  }

  void onTapSearch() {
    FocusManager.instance.primaryFocus?.unfocus();

    if (controller.text.trim().isEmpty)
      return WidgetUtils().showToast("搜索内容不可为空");
    // if (UserConstants.location == null)
    //   return WidgetUtils().showToast("还未获取到自己的位置");
    PluginUtils().startLocation(controller.text);
  }

  void onTapCell(int index) {
    FocusManager.instance.primaryFocus?.unfocus();
    WidgetUtils().popPage({
      "show": true,
      "loc": searList[index]['name'],
      "lat": searList[index]['latitude'],
      "lon": searList[index]['longitude']
    });
  }
}
