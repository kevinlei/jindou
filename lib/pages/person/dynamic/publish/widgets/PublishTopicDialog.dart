import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class PublishTopicDialog extends StatefulWidget {
  final List<PublishTopic> topicList;

  const PublishTopicDialog({Key? key, required this.topicList})
      : super(key: key);

  @override
  _PublishTopicDialogState createState() => _PublishTopicDialogState();
}

class _PublishTopicDialogState extends State<PublishTopicDialog> {
  TopicProvider provider = TopicProvider();

  @override
  void initState() {
    super.initState();
    provider.topicList = widget.topicList;
    provider.requestHotTopic();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return ChangeNotifierProvider(
        create: (_) => provider,
        builder: (_, __) {
          return Consumer<TopicProvider>(builder: (_, pro, __) {
            return FContainer(
              padding: EdgeInsets.fromLTRB(20.w, 20.w, 20.w, 0),
              height: size.height * 0.8,
              child: Column(children: [
                FSheetLine(),
                titleContent(),
                TopicInput(pro: pro),
                listTitle(),
                SizedBox(height: 20.h),
                listContent(),
              ], mainAxisSize: MainAxisSize.min),
            );
          });
        });
  }

  Widget titleContent() {
    return Row(children: [
      SizedBox(width: 100.w),
      FText("添加话题", size: 32.sp, weight: FontWeight.bold,color: Colors.black,),
      buttonOk(),
    ], mainAxisAlignment: MainAxisAlignment.spaceBetween);
  }

  Widget buttonOk() {
    Widget btn = FContainer(
      color: Color(0xFF2F2F42),
      width: 100.w,
      margin: EdgeInsets.only(left: 20.w),
      height: 45.w,
      radius: BorderRadius.circular(45.w),
      align: Alignment.center,
      child: FText("确认", size: 26.sp),
    );
    return GestureDetector(child: btn, onTap: onTapConfirm);
  }

  Widget listTitle() {
    return Row(children: [
      SizedBox(width: 10.w),
      FText(provider.controller.text.isEmpty ? "热门话题" : "推荐话题",
          weight: FontWeight.bold,color: Colors.black,)
    ]);
  }

  Widget listContent() {
    List<PublishTopic> list = provider.controller.text.isEmpty
        ? provider.hotList
        : provider.searchList;
    if (list.isEmpty) {
      return Expanded(
        child: FEmptyWidget(
            text: provider.controller.text.isEmpty ? "暂无话题" : "暂无推荐",),
      );
    }
    return Expanded(
      child: ListView.builder(
        itemBuilder: (_, index) => topicCell(index, list[index]),
        itemExtent: 90.w,
        itemCount: list.length,
      ),
    );
  }

  Widget topicCell(int index, PublishTopic topic) {
    Widget btn = FContainer(
      padding: EdgeInsets.symmetric(horizontal: 16.w),
     // border: Border(bottom: BorderSide(color: Color(0xFFEEF1F4))),
      child: Row(children: [
        index < 3 ? Expanded(child: Row(
          children: [
            LoadAssetImage("barone/hot${index + 1}", width: 40.w,height: 40.w,),
            SizedBox(width: 10.w,),
            FText("${topic.topic}", size: 24.sp,color: Colors.black.withOpacity(0.8),),
          ],
        )) : Expanded(child: Row(
          children: [
            SizedBox(width: 10.w,),
            FText("${index + 1}", size: 24.sp,color: Color(0xFF9F9FB9),),
            SizedBox(width: 25.w,),
            FText("${topic.topic}", size: 24.sp,color: Colors.black.withOpacity(0.8)),
          ],
        )),
        SizedBox(width: 20.w),
        LoadAssetImage("barone/ic1", width: 30.w),
        SizedBox(width: 5.w),
        FText("${topic.count}条发布", size: 26.sp, color: Color(0xFF999999))
      ]),
    );
    return GestureDetector(onTap: () => provider.onTapTopic(topic), child: btn);
  }

  void onTapConfirm() {
    WidgetUtils().popPage(provider.topicList);
  }
}
