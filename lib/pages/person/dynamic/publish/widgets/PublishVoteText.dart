import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class PublishVoteText extends StatelessWidget {
  final List<String> leads = ["A.", "B.", "C.", "D."];

  @override
  Widget build(BuildContext context) {
    List<String> voteTexts = context.watch<PublishProvider>().voteTexts;
    List<Widget> children = [inputTitle(context)];

    int length = voteTexts.length;
    children.addAll(List.generate(length, (index) {
      bool showDel = (index == voteTexts.length - 1) && index > 1;
      return PbVoteInput(leads[index], showDel, onDel: () {
        Provider.of<PublishProvider>(context, listen: false).onDelVoteTxt();
      }, onTextInput: (value) {
        Provider.of<PublishProvider>(context, listen: false)
            .onVoteInput(index, value);
      });
    }));
    if (length < 4) children.add(inputAdd(context));

    return FContainer(
      radius: BorderRadius.circular(10.w),
      color: Color(0xFFFF753F).withOpacity(0.2),
      padding: EdgeInsets.all(20.w),
      child: Column(children: children),
    );
  }

  Widget inputTitle(BuildContext context) {
    return Row(children: [
      // LoadAssetImage("publish/ic3", width: 35.w),
      // SizedBox(width: 10.w),
      FText("投票选项", weight: FontWeight.bold,size: 24.sp,color: Colors.black.withOpacity(0.6),),
      Spacer(),
      IconButton(
          onPressed: () => onTapClean(context),
          color: Colors.grey,
          icon: Icon(Icons.cancel))
    ]);
  }

  Widget inputAdd(BuildContext context) {
    Widget btn = FContainer(
      height: 90.h,
      child: Row(children: [
        Icon(Icons.add_circle, color: Colors.grey),
        SizedBox(width: 10.w),
        FText("添加选项 (最多4个选项，最少2个选项)", color: Color(0xFFBBBBBB), size: 28.sp)
      ]),
    );
    return GestureDetector(
        onTap: () {
          Provider.of<PublishProvider>(context, listen: false).onAddVoteTxt();
        },
        child: btn);
  }

  void onTapClean(BuildContext context) {
    EventUtils().emit(EventConstants.E_CLEAN_VOTE, "");
    Provider.of<PublishProvider>(context, listen: false).onCleanVoteTxt();
  }
}
