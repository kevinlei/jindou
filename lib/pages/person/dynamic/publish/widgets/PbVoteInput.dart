import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class PbVoteInput extends StatefulWidget {
  final String lead;
  final bool showDel;
  final Function() onDel;
  final Function(String) onTextInput;

  const PbVoteInput(this.lead, this.showDel,
      {Key? key, required this.onDel, required this.onTextInput})
      : super(key: key);

  @override
  _PbVoteInputState createState() => _PbVoteInputState();
}

class _PbVoteInputState extends State<PbVoteInput> {
  TextEditingController controller = TextEditingController();
  FocusNode focus = FocusNode();

  @override
  void initState() {
    super.initState();
    EventUtils().on(EventConstants.E_EMOTION_INPUT,
        EventCall("PbVoteInput${widget.lead}", onEmotionInput));
    EventUtils().on(EventConstants.E_EMOTION_DEL,
        EventCall("PbVoteInput${widget.lead}", onEmotionDel));
    EventUtils().on(EventConstants.E_CLEAN_VOTE,
        EventCall("PbVoteInput${widget.lead}", onCleanVote));
  }

  @override
  Widget build(BuildContext context) {
    Widget btn = IconButton(
        onPressed: widget.onDel, color: Colors.grey, icon: Icon(Icons.cancel));
    // bool visible = (index == voteTexts.length - 1) && index > 1;
    return FContainer(
      height: 68.h,
      color: Color(0xFFFF753F).withOpacity(0.2),
      radius: BorderRadius.all(Radius.circular(10)),
      margin: EdgeInsets.only(bottom: 10.w),
      padding: EdgeInsets.only(left: 15.w),
   //   border: Border(bottom: BorderSide(color: Color(0xFFEDEEF3))),
      child: Row(children: [
        FText(widget.lead, color: Color(0xFFBBBBBB), size: 28.sp),
        SizedBox(width: 10.w),
        Expanded(child: field()),
        Visibility(child: btn, visible: widget.showDel)
      ]),
    );
  }

  @override
  void deactivate() {
    EventUtils().off(EventConstants.E_EMOTION_INPUT,
        EventCall("PbVoteInput${widget.lead}", onEmotionInput));
    EventUtils().off(EventConstants.E_EMOTION_DEL,
        EventCall("PbVoteInput${widget.lead}", onEmotionDel));
    EventUtils().off(EventConstants.E_CLEAN_VOTE,
        EventCall("PbVoteInput${widget.lead}", onCleanVote));
    super.deactivate();
  }

  @override
  void dispose() {
    controller.dispose();
    focus.dispose();
    super.dispose();
  }

  Widget field() {
    InputBorder border =
        OutlineInputBorder(borderSide: BorderSide(color: Colors.transparent));
    return TextField(
      maxLength: 15,
      onChanged: widget.onTextInput,
      controller: controller,
      focusNode: focus,
      style: TextStyle(color: Colors.black, fontSize: 28.sp),
      onTap: () => EventUtils().emit(EventConstants.E_INPUT_ACTIVE, ""),
      decoration: InputDecoration(
        focusedBorder: border,
        enabledBorder: border,
        counterText: "",
        hintText: "选项(15字以内)",
        contentPadding: EdgeInsets.zero,
        hintStyle: TextStyle(color: Color(0xFFBBBBBB), fontSize: 28.sp),
      ),
    );
  }

  void onEmotionInput(em) {
    if (!focus.hasFocus || !(em is String)) return;
    if (controller.text.length >= 15) return;
    String text = controller.text;
    int start = controller.selection.baseOffset;
    int end = controller.selection.extentOffset;
    String f = text.substring(0, start);
    String e = text.substring(end, text.length);
    controller.value = controller.value.copyWith(
      text: f + em + e,
      selection: TextSelection(
          baseOffset: start + em.length, extentOffset: start + em.length),
    );
  }

  void onEmotionDel(v) {
    if (!focus.hasFocus) return;
    String text = controller.text;
    int start = controller.selection.baseOffset;
    int end = controller.selection.extentOffset;

    String f, e;
    if (start == end) {
      if (start == 0) return;
      List<String> characters = text.characters.toList();
      int remove = getStartCharacter(characters, start);
      if (remove == -1) return;
      int length = characters[remove].length;
      f = characters.getRange(0, remove).join();
      e = characters.getRange(remove + 1, characters.length).join();
      controller.value = controller.value.copyWith(
        text: f + e,
        selection: TextSelection(
            baseOffset: start - length, extentOffset: start - length),
      );
    } else {
      f = text.substring(0, start);
      e = text.substring(end, text.length);
      controller.value = controller.value.copyWith(
        text: f + e,
        selection: TextSelection(baseOffset: start, extentOffset: start),
      );
    }
  }

  void onCleanVote(v) {
    controller.clear();
  }

  int getStartCharacter(List<String> characters, int start) {
    int offset = 0;
    for (int i = 0; i < characters.length; i++) {
      offset += characters[i].length;
      if (offset == start) {
        return i;
      }
    }
    return -1;
  }
}
