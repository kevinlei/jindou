import 'dart:io';

import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';

class PublishVoteImage extends StatelessWidget {
  final List<String> leads = ["A", "B", "C", "D"];

  @override
  Widget build(BuildContext context) {
    return FContainer(
      radius: BorderRadius.circular(10.w),
      color: Color(0xFFFF753F).withOpacity(0.2),
      padding: EdgeInsets.all(20.w),
      child: Column(children: [
        inputTitle(context),
        Row(children: inputCellList(context))
      ]),
    );
  }

  Widget inputTitle(BuildContext context) {
    return Row(children: [
     // LoadAssetImage("publish/ic3", width: 35.w),
      SizedBox(width: 10.w),
      FText("投票选项",color: Colors.black.withOpacity(0.6), weight: FontWeight.bold,size: 24.sp,),
      Spacer(),
      IconButton(
          onPressed: () {
            Provider.of<PublishProvider>(context, listen: false)
                .onCleanVoteImg();
          },
          color: Colors.grey,
          icon: Icon(Icons.cancel))
    ]);
  }

  List<Widget> inputCellList(BuildContext context) {
    List<String> voteImages = context.watch<PublishProvider>().voteImages;
    int length = voteImages.length;
    return List.generate(
      length,
      (index) {
        String image = voteImages[index];
        Widget? child;
        if (image.isEmpty) {
          child = Icon(Icons.add, color: Color(0xFFBABAC3));
        } else {
          if (index == getLastCell(voteImages)) {
            child = btnDelete(() {
              Provider.of<PublishProvider>(context, listen: false)
                  .onDelVoteImg(index);
            });
          }
        }

        return Column(children: [
          GestureDetector(
            onTap: () => onTapAdd(context, index),
            child: FContainer(
              width: 150.w,
              height: 150.w,
              imageFit: BoxFit.cover,
              image: image.isEmpty ? null : FileImage(File(voteImages[index])),
              radius: BorderRadius.circular(10.w),
              margin: EdgeInsets.only(bottom: 10.w, left: 8.w, right: 8.w),
              color: Color(0xFFFF753F).withOpacity(0.2),
              align: image.isEmpty ? Alignment.center : Alignment.topRight,
              child: child,
            ),
          ),
          FText(leads[index], size: 24.sp, color: Color(0xFF999999))
        ]);
      },
    );
  }

  Widget btnDelete(Function() onDel) {
    return SizedBox(
      width: 50.w,
      height: 40.w,
      child: IconButton(
        padding: EdgeInsets.zero,
        color: Color(0xFFA852FF),
        icon: Icon(Icons.cancel),
        onPressed: onDel,
      ),
    );
  }

  int getLastCell(List<String> voteImages) {
    for (int i = voteImages.length - 1; i >= 0; i--) {
      if (voteImages[i].isNotEmpty) return i;
    }
    return -1;
  }

  final ImagePicker picker = ImagePicker();

  void onTapAdd(BuildContext context, int index) async {
    bool accept = await FuncUtils().requestPermission(Permission.photos);
    if (!accept) return;

    XFile? file = await picker.pickImage(source: ImageSource.gallery);
    if (file == null || file.path.isEmpty) return;

    Provider.of<PublishProvider>(context, listen: false)
        .onAddVoteImg(index, file.path);
  }
}
