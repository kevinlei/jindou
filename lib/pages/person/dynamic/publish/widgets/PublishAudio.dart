import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class PublishAudio extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    String audioPath = context.watch<PublishProvider>().audioPath;
    int audioTime = context.watch<PublishProvider>().audioTime;
    Widget content = Row(children: [
      DynamicAudio(audioPath, fileSource: true, duration: audioTime),
      IconButton(
        padding: EdgeInsets.zero,
        color: Color(0xFFA852FF),
        icon: Icon(Icons.cancel),
        onPressed: () => onTapDelete(context),
      )
    ], mainAxisAlignment: MainAxisAlignment.spaceBetween);
    return Visibility(child: content, visible: audioPath.isNotEmpty);
  }

  void onTapDelete(BuildContext context) {
    Provider.of<PublishProvider>(context, listen: false).onAudioUpdate("", 0);
  }
}
