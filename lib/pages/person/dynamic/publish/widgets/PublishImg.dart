import 'dart:io';
import 'dart:typed_data';

import 'package:echo/import.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:video_thumbnail/video_thumbnail.dart';

class PublishImg extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<String> txtImages = context.watch<PublishProvider>().txtImages;
    List<Widget> children = List.generate(txtImages.length, (index) {
      return photoCell(txtImages[index], () {
          Provider.of<PublishProvider>(context, listen: false).onDeleteImg(index);
      });
    });

    if (txtImages.length < 4)
      children.add(btnAdd(() => onTapAdd(context, txtImages.length)));
    return Align(
      alignment: Alignment.centerLeft,
      child: Wrap(children: children, spacing: 10.w, runSpacing: 10.w),
    );
  }

  Widget photoCell(String image, Function() onDel) {
    // if (image.isPhoto) {
    //   return imageCell(image.path, onDel);
    // } else {
    //   return videoCell(image.path, onDel);
    // }
    return imageCell(File(image), onDel);
  }

  Widget imageCell(File file, Function() onDel) {
    return FContainer(
      width: 160.w,
      height: 160.w,
      imageFit: BoxFit.cover,
      image: FileImage(file),
      radius: BorderRadius.circular(10.w),
      color: Colors.white10,
      align: Alignment.topRight,
      child: btnDelete(onDel),
    );
  }

  Widget videoCell(File file, Function() onDel) {
    return FutureBuilder<Uint8List?>(
        future: VideoThumbnail.thumbnailData(video: file.path),
        builder: (_, shot) {
          ImageProvider? image;
          if (shot.hasData && shot.data != null) {
            image = MemoryImage(shot.data!);
          }
          return FContainer(
            width: 220.w,
            height: 220.w,
            image: image,
            imageFit: BoxFit.cover,
            radius: BorderRadius.circular(10.w),
            color: Colors.white10,
            align: Alignment.topRight,
            child: btnDelete(onDel),
          );
        });
  }

  Widget btnDelete(Function() onDel) {
    return GestureDetector(
      onTap: onDel,
      child: FContainer(
        padding: EdgeInsets.only(right: 25.w),
        child: SizedBox(
          width: 20.w,
          height: 30.w,
          child: IconButton(
            padding: EdgeInsets.zero,
            color: Colors.grey.withOpacity(0.8),
            icon: Icon(Icons.cancel),
            onPressed: onDel,
          ),
        ),
      ),
    );
  }

  //添加图片按钮
  Widget btnAdd(Function() onAdd) {
    return GestureDetector(
      onTap: onAdd,
      child: FContainer(
        radius: BorderRadius.circular(8),
        color: Color(0xFF191B24).withOpacity(0.05),
        margin: EdgeInsets.only(left: 10.w,),
        padding: EdgeInsets.all(24.w),
        child: FContainer(
          width: 120.w,
          height: 120.w,
          radius: BorderRadius.circular(10.w),
          image: ImageUtils().getAssetsImage("dynamic/camera"),
        ),
      ),
    );
  }

  final ImagePicker picker = ImagePicker();

  void onTapAdd(BuildContext context, int length) async {
    bool accept = await FuncUtils().requestPermission(Permission.photos);
    if (!accept) return;

    List<XFile> files = await picker.pickMultiImage();
    if (files.isEmpty) return;
    List<String> images = [];
    for (int i = 0; i < 4 - length; i++) {
      if (i < files.length) {
        images.add(files[i].path);
      }
    }

    // List<SelectImage>? images =
    //     await WidgetUtils().pushPage(PhotosPage(count: 4 - length));
    // if (images == null || images.isEmpty) return;
    Provider.of<PublishProvider>(context, listen: false).onAddImg(images);
  }
}
