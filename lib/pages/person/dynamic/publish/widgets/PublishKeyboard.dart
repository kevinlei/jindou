import 'package:echo/import.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class PublishKeyboard extends StatefulWidget {
  final Function(List<PublishTopic>) updateTopic;

  const PublishKeyboard({Key? key, required this.updateTopic})
      : super(key: key);

  @override
  _PublishKeyboardState createState() => _PublishKeyboardState();
}

class _PublishKeyboardState extends State<PublishKeyboard> {
  @override
  TextEditingController controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    EventUtils().on(
        EventConstants.E_TAP_SCREEN, EventCall("PublishKeyboard", onTapScreen));
    EventUtils().on(EventConstants.E_INPUT_ACTIVE,
        EventCall("PublishKeyboard", onInputActive));
    EventUtils().on(EventConstants.E_TAP_SCREEN,
        EventCall("PublishKeyboard", onInputActive));
  }

  @override
  void deactivate() {
    EventUtils().off(
        EventConstants.E_TAP_SCREEN, EventCall("PublishKeyboard", onTapScreen));
    EventUtils().off(EventConstants.E_INPUT_ACTIVE,
        EventCall("PublishKeyboard", onInputActive));
    EventUtils().off(EventConstants.E_TAP_SCREEN,
        EventCall("PublishKeyboard", onInputActive));
    super.deactivate();
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      color: Colors.white,
      child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [keyboardMenu(), keyboardFoot()]),
    );
  }

  Widget keyboardMenu() {
    return FContainer(
      height: 100.h,
      color: Colors.transparent,
      padding: EdgeInsets.symmetric(horizontal: 25.w, vertical: 10.h),
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        btnInput(),
        btnEmotion(),
        btnMicro(),
        btnVote(),
        btnWord()
      ]),
    );
  }

  bool isKeyboardActive = false;

  bool isEmotionActive = false;
  bool isAudioActive = false;
  bool isVoteActive = false;

  double keyboardHeight = 0;

  Widget keyboardFoot() {
    double kFootHeight = keyboardHeight <= 0 ? 620.h : keyboardHeight;
    Widget child;
    if (isKeyboardActive || isEmotionActive) {
      child = EmotionFooter(controller);
    } else if (isAudioActive) {
      child = AudioFooter(onSend: onAudioInput);
    } else if (isVoteActive) {
      child = footVote();
    } else {
      kFootHeight = 0;
      child = FContainer();
    }
    return SizedBox(height: kFootHeight, child: child);
  }

  Widget footVote() {
    return FContainer(
      color: Color(0xFFFFFFFF),
      child: Column(
        children: [
          SizedBox(
            height: 40.w,
          ),
          Align(
            alignment: Alignment.center,
            child: FText(
              "请选择投票方式",
              color: Colors.black,
              size: 32.sp,
              weight: FontWeight.bold,
            ),
          ),
          Expanded(
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  subVoteWidget("文字投票", "投票只支持文字", "dynamic/vote_t", onVoteTxt),
                  subVoteWidget("图片投票", "投票只支持图片", "dynamic/vote_i", onVoteImg),
                ]),
          ),
        ],
      ),
    );
  }

  Widget subVoteWidget(String title, String desc, String image,
      final GestureTapCallback? onTap) {
    return GestureDetector(
      onTap: onTap,
      child: FContainer(
        width: 276.w,
        height: 334.w,
        image: ImageUtils().getAssetsImage(image),
        child: Column(
          children: [
            SizedBox(
              height: 32.w,
            ),
            FText(
              title,
              color: Colors.black.withOpacity(0.8),
              size: 28.sp,
              weight: FontWeight.bold,
            ),
            SizedBox(
              height: 16.w,
            ),
            FText(
              desc,
              color: Colors.black.withOpacity(0.6),
              size: 24.sp,
            ),
          ],
        ),
      ),
    );
  }

  Widget btnInput() {
    return Container(
        width: 56 * 2.w,
        height: 36 * 2.h,
        decoration: null,
        child: IconButton(
            onPressed: onPressInput,
            icon: LoadAssetImage("barone/txt1", width: 48.sp)));
  }

  Widget btnEmotion() {
    return Container(
        width: 56 * 2.w,
        height: 36 * 2.h,
        decoration: isEmotionActive
            ? BoxDecoration(
                border: Border.all(width: 1, color: Color(0xFFFF753F)),
                borderRadius: BorderRadius.all(Radius.circular(36.h)),
              )
            : null,
        child: IconButton(
            onPressed: onPressEmotion,
            icon: LoadAssetImage(
                isEmotionActive ? "barone/emo2" : "barone/emo1",
                width: 48.sp)));
  }

  Widget btnMicro() {
    return Container(
        width: 56 * 2.w,
        height: 36 * 2.h,
        decoration: isAudioActive
            ? BoxDecoration(
                border: Border.all(width: 1, color: Color(0xFFFF753F)),
                borderRadius: BorderRadius.all(Radius.circular(36.h)),
              )
            : null,
        child: IconButton(
            onPressed: onPressMicro,
            icon: LoadAssetImage(
                isAudioActive ? "barone/audio2" : "barone/audio1",
                width: 48.sp)));
  }

  Widget btnVote() {
    return Container(
      width: 56 * 2.w,
      height: 36 * 2.h,
      decoration: isVoteActive
          ? BoxDecoration(
              border: Border.all(width: 1, color: Color(0xFFFF753F)),
              borderRadius: BorderRadius.all(Radius.circular(36.h)),
            )
          : null,
      child: IconButton(
          onPressed: onPressVote,
          color: isVoteActive ? Colors.amber : Colors.white,
          icon: LoadAssetImage(isVoteActive ? "barone/vote2" : "barone/vote1",
              width: 48.sp)),
    );
  }

  Widget btnWord() {
    return Container(
        width: 56 * 2.w,
        height: 36 * 2.h,
        decoration: null,
        child: IconButton(
            onPressed: onTapTopic,
            icon: LoadAssetImage("barone/world", width: 48.sp)));
  }

  void onTapScreen(value) {
    if (isKeyboardActive || isVoteActive || isEmotionActive || isAudioActive) {
      isKeyboardActive = false;
      isVoteActive = false;
      isEmotionActive = false;
      isAudioActive = false;
      if (!mounted) return;
      setState(() {});
    }
  }

  void onInputActive(v) {
    if (isEmotionActive || isAudioActive || isVoteActive) {
      isEmotionActive = false;
      isAudioActive = false;
      isVoteActive = false;
      if (!mounted) return;
      setState(() {});
    }
  }

  void onPressInput() async {
    if (isKeyboardActive) return;
    bool canClick =
        Provider.of<PublishProvider>(context, listen: false).onClickText();
    if (!canClick) return;
    isKeyboardActive = true;
    isEmotionActive = false;
    isAudioActive = false;
    isVoteActive = false;
    if (!mounted) return;
    setState(() {});
    await SystemChannels.textInput.invokeMethod("TextInput.show");
  }

  void onPressEmotion() async {
    if (isEmotionActive) return;
    // if (isKeyboardActive) {
    //   await SystemChannels.textInput.invokeMethod("TextInput.hide");
    //   await Future.delayed(Duration(milliseconds: 50));
    // }
    SystemChannels.textInput.invokeMethod("TextInput.hide");
    isKeyboardActive = false;
    isEmotionActive = true;
    isAudioActive = false;
    isVoteActive = false;
    if (!mounted) return;
    setState(() {});
  }

  void onPressMicro() async {
    if (isAudioActive) return;
    bool canClick =
        Provider.of<PublishProvider>(context, listen: false).onClickAudio();
    if (!canClick) return;
    SystemChannels.textInput.invokeMethod("TextInput.hide");
    isKeyboardActive = false;
    isAudioActive = true;
    isEmotionActive = false;
    isVoteActive = false;
    if (!mounted) return;
    setState(() {});
  }

  void onPressVote() async {
    if (isVoteActive) return;
    SystemChannels.textInput.invokeMethod("TextInput.hide");
    isKeyboardActive = false;
    isVoteActive = true;
    isEmotionActive = false;
    isAudioActive = false;
    if (!mounted) return;
    setState(() {});
  }

  void onVoteTxt() {
    Provider.of<PublishProvider>(context, listen: false).onClickVoteText();
  }

  void onVoteImg() {
    Provider.of<PublishProvider>(context, listen: false).onClickVoteImg();
  }

  void onAudioInput(String path, int time) {
    Provider.of<PublishProvider>(context, listen: false)
        .onAudioUpdate(path, time);
  }

  void onTapTopic() async {
    List<PublishTopic> topicList =
        Provider.of<PublishProvider>(context, listen: false).topicList;
    List<PublishTopic>? list = await WidgetUtils().showBottomSheet(
        PublishTopicDialog(topicList: topicList),
        enableDrag: false,
        color: Colors.white);
    if (list == null) return;
    widget.updateTopic(list);
  }
}
