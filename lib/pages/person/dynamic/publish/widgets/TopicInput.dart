import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:flutter/services.dart';

class TopicInput extends StatelessWidget {
  final TopicProvider pro;

  const TopicInput({Key? key, required this.pro}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      inputContent(),
      selectContent(),
    ], mainAxisSize: MainAxisSize.min);
  }

  Widget inputContent() {
    bool isInputting = pro.controller.text.isNotEmpty;
    return Row(children: [
      Expanded(
        child: FContainer(
          height: 60.w,
          margin: EdgeInsets.symmetric(vertical: 30.h),
          radius: BorderRadius.circular(60.w),
          color: Colors.black.withOpacity(0.05),
          padding: EdgeInsets.symmetric(horizontal: 20.w),
          child: Row(children: [
            LoadAssetImage("barone/search", width: 30.w),
            SizedBox(width: 10.w),
            Expanded(child: inputField()),
          ]),
        ),
      ),
      Visibility(child: buttonCancel(), visible: isInputting)
    ]);
  }

  Widget inputField() {
    InputBorder border =
        OutlineInputBorder(borderSide: BorderSide(color: Colors.transparent));
    return TextField(
      minLines: 1,
      controller: pro.controller,
      onChanged: pro.onTextInput,
      maxLength: 10,
      // inputFormatters: [FilteringTextInputFormatter.deny(RegExp(r"[\s]"))],
      style: TextStyle(color: Colors.black, fontSize: 26.sp),
      decoration: InputDecoration(
        contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10.w),
        enabledBorder: border,
        focusedBorder: border,
        counterText: "",
        hintStyle: TextStyle(color: Color(0xFFBBBBBB), fontSize: 26.sp),
        hintText: "可以获得更多互动~",
      ),
    );
  }

  Widget buttonCancel() {
    Widget btn = FContainer(
      color: Color(0xFF2F2F42),
      width: 100.w,
      margin: EdgeInsets.only(left: 20.w),
      height: 45.w,
      radius: BorderRadius.circular(45.w),
      align: Alignment.center,
      child: FText("取消", size: 26.sp),
    );

    return GestureDetector(child: btn, onTap: pro.onTapCancel);
  }

  Widget selectContent() {
    bool isInputting = pro.controller.text.isNotEmpty;
    return SizedBox(
      height: 260.h,
      width: double.infinity,
      child: Column(children: [
        FRichText([
          FRichTextItem()
            ..text = isInputting ? "添加话题" : "选择话题  ${pro.topicList.length}/3"
            ..color = Colors.black.withOpacity(0.8)
            ..size = 28.sp
            ..weight = FontWeight.bold,
          FRichTextItem()
            ..text = isInputting ? "（点击话题添加最多3个）" : ""
            ..color = Color(0xFF999999)
            ..size = 26.sp
            ..weight = FontWeight.normal
        ]),
        SizedBox(height: 30.h),
        Wrap(spacing: 20.w, runSpacing: 20.w, children: [
          if (isInputting) inputCell(),
          if (!isInputting && pro.topicList.length > 0) topicCell(0),
          if (!isInputting && pro.topicList.length > 1) topicCell(1),
          if (!isInputting && pro.topicList.length > 2) topicCell(2),
        ])
      ], crossAxisAlignment: CrossAxisAlignment.start),
    );
  }

  Widget inputCell() {
    Widget btn = FContainer(
      padding: EdgeInsets.fromLTRB(30.w, 0, 10.w, 0),
      color: Color(0xFFF6F8FA),
      height: 40.w,
      radius: BorderRadius.circular(40.w),
      child: Column(children: [
        Row(children: [
          FText("#${pro.controller.text}",
              size: 24.sp, color: Color(0xFF999999)),
          Icon(Icons.add, size: 20.w)
        ], mainAxisSize: MainAxisSize.min)
      ], mainAxisAlignment: MainAxisAlignment.center),
    );
    return GestureDetector(onTap: pro.onAddTopic, child: btn);
  }

  Widget topicCell(int index) {
    Widget btn = FContainer(
      padding: EdgeInsets.fromLTRB(20.w, 0, 20.w, 0),
      color: Colors.black.withOpacity(0.05),
      height: 40.w,
      border: Border.all(color: Color(0xFFFF753F)),
      radius: BorderRadius.circular(40.w),
      child: Column(children: [
        Row(children: [
          FText("#${pro.topicList[index].topic}", size: 22.sp,color: Color(0xFFFF753F),),
          SizedBox(width: 5.w,),
          Icon(Icons.clear, size: 20.w)
        ], mainAxisSize: MainAxisSize.min)
      ], mainAxisAlignment: MainAxisAlignment.center),
    );
    return GestureDetector(onTap: () => pro.onDelTopic(index), child: btn);
  }
}
