import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class PbAuthDialog extends StatelessWidget {
  final DynamicAuthType? auth;

  const PbAuthDialog({Key? key, this.auth}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FContainer(
      padding: EdgeInsets.only(bottom: ScreenUtil().bottomBarHeight),
      child: Column(children: [
        buttonCell(DynamicAuthType.ALL_SEE),
        // buttonCell(DynamicAuthType.STRANGER),
        buttonCell(DynamicAuthType.FRIEND),
        // buttonCell(DynamicAuthType.ONLY_SELF),
        FContainer(height: 20.h, color: Color(0xFFEDEEF3)),
        buttonCancel()
      ], mainAxisSize: MainAxisSize.min),
    );
  }

  Widget buttonCell(DynamicAuthType type) {
    return GestureDetector(
      onTap: () => onTapAuth(type),
      child: FContainer(
        height: 100.w,
        align: Alignment.center,
        // border: type != DynamicAuthType.ONLY_SELF
        //     ? Border(
        //         bottom: BorderSide(color: Color(0xFFEEEEEE).withOpacity(0.5)))
        //     : null,

        border: Border(
                bottom: BorderSide(color: Color(0xFFEEEEEE).withOpacity(0.5)))
            ,
        child: FText(type.text,
            color: auth == type ? Color(0xFFA852FF) : Colors.black),
      ),
    );
  }

  Widget buttonCancel() {
    return GestureDetector(
      onTap: onTapCancel,
      child: FContainer(
        height: 100.w,
        align: Alignment.center,
        child: FText("取消",color: Colors.black,),
      ),
    );
  }

  void onTapAuth(DynamicAuthType authType) {
    WidgetUtils().popPage(authType);
  }

  void onTapCancel() {
    WidgetUtils().popPage();
  }
}
