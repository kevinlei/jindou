import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class PublishBottom extends StatelessWidget {
  final Function(DynamicAuthType?) updateAuth;
  final Function(PublishTopic topic) onDelTopic;
  final Function(Map?) updateLoc;

  const PublishBottom(
      {Key? key,
      required this.updateAuth,
      required this.onDelTopic,
      required this.updateLoc})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Column(children: [
        SizedBox(height: 10.w),
        tipsContent(),
        SizedBox(height: 10.w),
        Column(children: [
          SizedBox(height: 25.w),
          locContent(),
          hautiContent(),
          authContent(),
          SizedBox(height: 25.w),
        ]),
        SizedBox(height: 10.w),
      ], mainAxisSize: MainAxisSize.min),
    );
  }

  Widget hautiContent() {
    return FContainer(
      padding: EdgeInsets.symmetric(horizontal: 15.w),
      margin: EdgeInsets.only(
        left: 30.w,
        right: 30.w,
      ),
      child: Consumer<PublishTopicProvider>(builder: (_, pro, __) {
        print(pro.topicList);
        List picList = [];
        if (pro.topicList.length > 0) {
          for (var i = 0; i < pro.topicList.length; i++) {
            print(pro.topicList[i].topic);
            picList.add(pro.topicList[i].topic);
          }
        }
        Widget btn = SizedBox(
          height: 64.w,
          child: Row(children: [
            LoadAssetImage(
              "barone/hauti",
              width: 32.w,
              height: 32.w,
            ),
            SizedBox(width: 10.w),
            FText("添加话题", size: 26.sp, color: Color(0xFF999999)),
            Spacer(),
            FText(pro.topicList.length == 0 ? "公开" : picList.toString(),
                size: 26.sp, color: Color(0xFF999999)),
            Icon(Icons.arrow_forward_ios, size: 30.w, color: Color(0xFF999999))
          ]),
        );
        return GestureDetector(
            onTap: () => onTapTopic(pro.topicList), child: btn);
      }),
    );
  }

  void onTapTopic(List<PublishTopic> topicList) async {
    // List<PublishTopic> topicList =
    //     Provider.of<PublishProvider>(context as BuildContext, listen: false).topicList;
    List<PublishTopic>? list = await WidgetUtils().showBottomSheet(
        PublishTopicDialog(topicList: topicList),
        enableDrag: false,
        color: Colors.white);
    if (list == null) return;
  }

  Widget tipsContent() {
    return SizedBox(
      height: 50.w,
      child: Consumer<PublishTopicProvider>(builder: (_, pro, __) {
        return ListView.separated(
          padding: EdgeInsets.only(left: 25.w),
          scrollDirection: Axis.horizontal,
          itemBuilder: (_, index) {
            return tipCell(pro.topicList[index]);
          },
          separatorBuilder: (_, __) => SizedBox(width: 20.w),
          itemCount: pro.topicList.length,
        );
      }),
    );
  }

  Widget tipCell(PublishTopic topic) {
    Widget btn = FContainer(
      border: Border.all(color: Color(0xFF8C73C2), width: 1.w),
      padding: EdgeInsets.fromLTRB(30.w, 0, 10.w, 0),
      color: Color(0xFF2F2F42).withOpacity(0.05),
      radius: BorderRadius.circular(40.w),
      child: Row(children: [
        FText("#${topic.topic}",
            size: 22.sp, color: Colors.white.withOpacity(0.8)),
        SizedBox(width: 10.w),
        SizedBox(
          width: 40.w,
          height: 40.w,
          child: Icon(Icons.cancel, color: Color(0xFFBBBBBB), size: 25.w),
        )
      ]),
    );
    return GestureDetector(onTap: () => onDelTopic(topic), child: btn);
  }

  Widget locContent() {
    Widget btn = FContainer(
      color: Colors.transparent,
      height: 64.w,
      margin: EdgeInsets.only(
        left: 30.w,
        right: 30.w,
      ),
      radius: BorderRadius.circular(50.w),
      padding: EdgeInsets.symmetric(horizontal: 15.w),
      child: Row(children: [
        LoadAssetImage(
          "dynamic/location",
          width: 32.w,
          height: 32.w,
        ),
        SizedBox(width: 10.w),
        FText("添加位置", size: 24.sp, color: Color(0xFF999999)),
        Spacer(),
        Consumer<PublishTopicProvider>(
          builder: (_, pro, __) {
            String location = pro.location;
            if (location.length > 6) {
              location = location.substring(0, 6) + "...";
            }
            return FText(location.isEmpty ? "" : location,
                size: 24.sp, color: Color(0xFF999999));
          },
        ),
        Icon(Icons.arrow_forward_ios, size: 30.w, color: Color(0xFF999999))
      ]),
    );
    return GestureDetector(onTap: onTapLoc, child: btn);
  }

  Widget authContent() {
    return FContainer(
      padding: EdgeInsets.symmetric(horizontal: 15.w),
      margin: EdgeInsets.only(
        left: 30.w,
        right: 30.w,
      ),
      child: Consumer<PublishTopicProvider>(builder: (_, pro, __) {
        Widget btn = SizedBox(
          height: 64.w,
          child: Row(children: [
            LoadAssetImage(
              "dynamic/person",
              width: 32.w,
              height: 32.w,
            ),
            SizedBox(width: 10.w),
            FText("谁可以看", size: 26.sp, color: Color(0xFF999999)),
            Spacer(),
            FText(pro.authType == null ? "公开" : pro.authType!.text,
                size: 26.sp, color: Color(0xFF999999)),
            Icon(Icons.arrow_forward_ios, size: 30.w, color: Color(0xFF999999))
          ]),
        );
        return GestureDetector(
            onTap: () => onTapAuth(pro.authType), child: btn);
      }),
    );
  }

  void onTapLoc() async {
    Map? data =
        await WidgetUtils().showBottomSheet(PbLocDialog(), color: Colors.white);
    if (data == null) return;
    bool showLoc = data["show"] ?? false;
    if (!showLoc) {
      updateLoc(null);
    } else {
      updateLoc(data);
    }
  }

  void onTapAuth(DynamicAuthType? authType) async {
    DynamicAuthType? type = await WidgetUtils()
        .showBottomSheet(PbAuthDialog(auth: authType), color: Colors.white);
    if (type == null || type == authType) return;
    updateAuth(type);
  }
}
