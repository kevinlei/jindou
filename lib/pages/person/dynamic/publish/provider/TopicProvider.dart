import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class TopicProvider extends ChangeNotifier {
  bool isSearching = false;
  TextEditingController controller = TextEditingController();

  List<PublishTopic> topicList = [];
  List<PublishTopic> hotList = [];
  List<PublishTopic> searchList = [];

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  void onTextInput(String value) {
    notifyListeners();
    requestSearchTopic();
  }

  void onTapCancel() {
    controller.clear();
    notifyListeners();
  }

  void onTapTopic(PublishTopic topic) {
    if (topicList.length > 2) return WidgetUtils().showToast("最多添加3个话题");
    if (hasSameTopic(topic)) return WidgetUtils().showToast("已有该话题");
    topicList.add(topic);
    controller.clear();
    notifyListeners();
  }

  void onAddTopic() {
    if (topicList.length > 2) return WidgetUtils().showToast("最多添加3个话题");
    if (controller.text.trimRight().length < 2)
      return WidgetUtils().showToast("话题至少两个字");
    if (hasTopicText(controller.text)) return WidgetUtils().showToast("已有该话题");
    requestAddTopic(controller.text);
  }

  bool hasTopicText(String text) {
    for (PublishTopic topic in topicList) {
      if (topic.topic == text) return true;
    }
    return false;
  }

  bool hasSameTopic(PublishTopic topic) {
    for (PublishTopic item in topicList) {
      if (item.topic == topic.topic || item.id == topic.id) return true;
    }
    return false;
  }

  void onDelTopic(int index) {
    topicList.removeAt(index);
    notifyListeners();
  }

  void requestHotTopic() {
    DioUtils().asyncHttpRequest(
      NetConstants.hotTopics,
      params: {"topic_name": controller.text.trimRight()},
      method: DioMethod.POST,
      onSuccess: onHotTopic,
    );
  }

  void onHotTopic(response) {
    response ??= [];
    hotList.clear();
    for (var data in response) {
      hotList.add(PublishTopic.fromJson(data));
    }
    notifyListeners();
  }

  void requestSearchTopic() async {
    if (controller.text.trimRight().length < 2) return;
    if (isSearching) return;
    isSearching = true;
    await DioUtils().httpRequest(
      NetConstants.hotTopics,
      method: DioMethod.POST,
      params: {"topic_name": controller.text.trimRight()},
      onSuccess: onSearchTopic,
    );
    isSearching = false;
  }

  void onSearchTopic(response) {
    response ??= [];
    searchList.clear();
    for (var data in response) {
      searchList.add(PublishTopic.fromJson(data));
    }
    notifyListeners();
  }

  void requestAddTopic(String text) {
    DioUtils().asyncHttpRequest(
      NetConstants.addTopic,
      method: DioMethod.POST,
      params: {"topic": text},
      onSuccess: onAddSuccess,
    );
  }

  void onAddSuccess(response) {
    if (response == null) return;
    topicList.add(PublishTopic.fromJson(response));
    controller.clear();
    notifyListeners();
  }
}
