import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class PublishTopicProvider extends ChangeNotifier {
  DynamicAuthType? authType;
  List<PublishTopic> topicList = [];

  String location = "";

  void updateAuth(DynamicAuthType? auth) {
    authType = auth;
    notifyListeners();
  }

  void onDelTopic(PublishTopic topic) {
    topicList.removeWhere((e) => e.id == topic.id);
    notifyListeners();
  }

  void updateTopic(List<PublishTopic> list) {
    topicList = list;
    notifyListeners();
  }

  void updateLoc(String data) {
    location = data;
    notifyListeners();
  }
}
