import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class PublishProvider extends ChangeNotifier {
  AuthStatus authStatus = AuthStatus.NULL;
  int pubExp = 0;
  PublishType curType = PublishType.TEXT_IMG;

  DynamicAuthType? authType;
  List<PublishTopic> topicList = [];

  String location = "";
  String longitude = "";
  String latitude = "";

  List<String> txtImages = [];
  String audioPath = "";
  int audioTime = 0;
  List<String> voteTexts = ["", ""];
  List<String> voteImages = List.generate(4, (_) => "");

  bool onClickText() {
    bool can = _canClick(PublishType.TEXT_IMG);
    if (!can) return false;
    curType = PublishType.TEXT_IMG;
    notifyListeners();
    return true;
  }

  bool onClickAudio() {
    bool can = _canClick(PublishType.TEXT_AUDIO);
    if (!can) return false;
    curType = PublishType.TEXT_AUDIO;
    notifyListeners();
    return true;
  }

  bool onClickVoteText() {
    bool can = _canClick(PublishType.VOTE_TXT);
    if (!can) return false;
    curType = PublishType.VOTE_TXT;
    notifyListeners();
    return true;
  }

  bool onClickVoteImg() {
    bool can = _canClick(PublishType.VOTE_IMG);
    if (!can) return false;
    curType = PublishType.VOTE_IMG;
    notifyListeners();
    return true;
  }

  void onDeleteImg(int index) {
    txtImages.removeAt(index);
    notifyListeners();
  }

  void onAddImg(List<String> images) {
    txtImages.addAll(images);
    notifyListeners();
  }

  void onAudioUpdate(String path, int time) {
    audioTime = time;
    audioPath = path;
    notifyListeners();
  }

  void onCleanVoteTxt() {
    voteTexts = ["", ""];
    notifyListeners();
  }

  void onAddVoteTxt() {
    voteTexts.add("");
    notifyListeners();
  }

  void onDelVoteTxt() {
    voteTexts.removeLast();
    notifyListeners();
  }

  void onVoteInput(int index, String value) {
    if (index < voteTexts.length) {
      voteTexts[index] = value;
      notifyListeners();
    }
  }

  void onCleanVoteImg() {
    voteImages = List.generate(4, (_) => "");
    notifyListeners();
  }

  void onAddVoteImg(int index, String value) {
    if (index < voteImages.length) {
      voteImages[index] = value;
      notifyListeners();
    }
  }

  void onDelVoteImg(int index) {
    if (index < voteImages.length) {
      voteImages[index] = "";
      notifyListeners();
    }
  }

  bool _canClick(PublishType type) {
    if (type == curType) return true;
    switch (type) {
      case PublishType.TEXT_IMG:
        if (audioPath.isNotEmpty) {
          WidgetUtils().showToast("语音状态不可添加图片");
          return false;
        }
        if (!_isVoteTxtEmpty() || !_isVoteImgEmpty()) {
          WidgetUtils().showToast("投票状态不可添加图片");
          return false;
        }
        break;
      case PublishType.TEXT_AUDIO:
        if (txtImages.isNotEmpty) {
          WidgetUtils().showToast("图文状态不可添加语音");
          return false;
        }
        if (!_isVoteTxtEmpty() || !_isVoteImgEmpty()) {
          WidgetUtils().showToast("投票状态不可添加语音");
          return false;
        }
        break;
      case PublishType.VOTE_TXT:
        if (txtImages.isNotEmpty) {
          WidgetUtils().showToast("图文状态不可添加投票");
          return false;
        }
        if (audioPath.isNotEmpty) {
          WidgetUtils().showToast("语音状态不可添加投票");
          return false;
        }
        if (!_isVoteImgEmpty()) {
          WidgetUtils().showToast("图片投票状态不可切换");
          return false;
        }
        break;
      case PublishType.VOTE_IMG:
        if (txtImages.isNotEmpty) {
          WidgetUtils().showToast("图文状态不可添加投票");
          return false;
        }
        if (audioPath.isNotEmpty) {
          WidgetUtils().showToast("语音状态不可添加投票");
          return false;
        }
        if (!_isVoteTxtEmpty()) {
          WidgetUtils().showToast("文本投票状态不可切换");
          return false;
        }
        break;
    }
    return true;
  }

  bool _isVoteTxtEmpty() {
    if (voteTexts.isEmpty) return true;
    for (String txt in voteTexts) {
      if (txt.trim().isNotEmpty) return false;
    }
    return true;
  }

  bool _isVoteImgEmpty() {
    if (voteImages.isEmpty) return true;
    for (String img in voteImages) {
      if (img.isNotEmpty) return false;
    }
    return true;
  }

  bool _canPbVoteTxt(String text) {
    if (text.trim().isEmpty) {
      WidgetUtils().showToast("请添加投票标题");
      return false;
    }
    int count = 0;
    for (String vote in voteTexts) {
      if (vote.trim().isEmpty) {
        WidgetUtils().showToast("投票选项不可为空");
        return false;
      }
      count += 1;
    }
    if (count < 2) {
      WidgetUtils().showToast("至少有两个选项");
      return false;
    }
    return true;
  }

  bool _canPbVoteImg(String text) {
    if (text.trim().isEmpty) {
      WidgetUtils().showToast("请添加投票标题");
      return false;
    }
    for (int i = 1; i < voteImages.length; i++) {
      if (voteImages[i - 1].isEmpty) {
        if (voteImages[i].isNotEmpty) {
          WidgetUtils().showToast("投票选项不可为空");
          return false;
        } else if (i <= 2) {
          WidgetUtils().showToast("投票选项不可为空");
          return false;
        }
      }
    }
    return true;
  }

  void onTapPublish(String text) {
    // int role = UserConstants.userInfo?.userRole ?? UserRole.DEF.index;
    // if (role < UserRole.OFFICIAL.index &&
    //     authStatus != AuthStatus.AUTO_S &&
    //     authStatus != AuthStatus.MANUAL_S) {
    //   return onTapAuth();
    //   // return WidgetUtils().showToast("请先前往实名认证");
    // }

    // int richExp = UserConstants.userInfo?.levelInfo.richExp ?? 0;
    // int charmExp = UserConstants.userInfo?.levelInfo.charmExp ?? 0;
    // if (role < UserRole.OFFICIAL.index &&
    //     richExp < pubExp &&
    //     charmExp < pubExp) {
    //   return WidgetUtils().showToast("您的等级不够");
    // }
    if (curType == PublishType.TEXT_IMG) {
      if (txtImages.isEmpty) {
        if (text.trim().isEmpty) return WidgetUtils().showToast("请添加发布内容");
        debugPrint("发布纯文本");
        pubTextOnly(text);
      } else {
        debugPrint("发布图文");
        pubTextImg(text);
      }
    } else if (curType == PublishType.TEXT_AUDIO) {
      if (audioPath.isEmpty) return WidgetUtils().showToast("请添加音频");
      debugPrint("发布音频：$audioPath");
      pubAudio(text);
    } else if (curType == PublishType.VOTE_TXT) {
      bool can = _canPbVoteTxt(text);
      if (!can) return;
      debugPrint("发布文本投票");
      for (String vote in voteTexts) {
        debugPrint("text：$vote");
      }
      pubVoteTxt(text);
    } else if (curType == PublishType.VOTE_IMG) {
      bool can = _canPbVoteImg(text);
      if (!can) return;
      debugPrint("发布图片投票");
      pubVoteImg(text);
    }
  }

  void pubTextOnly(String text) async {
    WidgetUtils().showLoading();
    Map params = {
      "content_type": DynamicType.TEXT_ONLY.index,
      "content": text,
      "ids": getTopicString(),
      "location": getLocData(),
      "visibility": authType?.index ?? 1,
      "status": 1,
    };
    requestPub(params);
  }

  void pubTextImg(String text) async {
    WidgetUtils().showLoading();
    // bool lawful = true;
    // if (text.trim().isNotEmpty) {
    //   lawful = await FuncUtils().checkTextLawful(text, CheckTxtType.dynamic);
    // }

    String resource = "";
    for (int index = 0; index < txtImages.length; index++) {
      if (txtImages[index].isEmpty) continue;
      String? path = await FuncUtils()
          .requestUpload(txtImages[index], UploadType.DYNAMIC_SQUARE);
      if (path == null) return WidgetUtils().cancelLoading();
      if (path.isEmpty) {
        WidgetUtils().showToast("上传失败");
        return WidgetUtils().cancelLoading();
      }

      // if (lawful) {
      //   lawful = await FuncUtils()
      //       .checkImageLawful([path], CheckImgType.IMG_DYDYNAMIC);
      // }
      // else if (!txtImages[index] && lawful) {
      //    lawful = await FuncUtils().checkVideoLawful([path]);
      // }

      resource += "$path";
      if (index != txtImages.length - 1) resource += ",";
    }
    if (resource.isEmpty) return WidgetUtils().cancelLoading();
    Map params = {
      "content_type": DynamicType.TEXT_IMAGE.index,
      "content": text,
      "ids": getTopicString(),
      "location": getLocData(),
      "resource_url": resource.split(","),
      "visibility": authType?.index ?? 1,
      "status": 1,
    };
    requestPub(params);
  }

  void pubAudio(String text) async {
    WidgetUtils().showLoading();
    bool lawful = true;
    if (text.trim().isNotEmpty) {
      lawful = await FuncUtils().checkTextLawful(text, CheckTxtType.dynamic);
    }
    String? resource =
        await FuncUtils().requestUpload(audioPath, UploadType.DYNAMIC_AUIDO);
    if (resource == null) return WidgetUtils().cancelLoading();
    if (resource.isEmpty) {
      WidgetUtils().showToast("上传失败");
      WidgetUtils().cancelLoading();
      return;
    }
    Map params = {
      "content_type": DynamicType.TEXT_AUDIO.index,
      "content": text,
      "ids": getTopicString(),
      "resource": resource,
      "location": getLocData(),
      "visibility": authType?.index ?? 1,
      "status": lawful ? 1 : 0,
    };
    requestPub(params);
  }

  void pubVoteTxt(String text) async {
    WidgetUtils().showLoading();
    List resource = [];
    for (String item in voteTexts) {
      resource.add({"content": item});
    }

    bool lawful = true;
    lawful = await FuncUtils()
        .checkTextLawful(text + resource.join(""), CheckTxtType.dynamic);

    Map params = {
      "content_type": DynamicType.VOTE_TEXT.index,
      "content": text,
      "ids": getTopicString(),
      "location": getLocData(),
      "visibility": authType?.index ?? 1,
      "votes": resource,
      "status": lawful ? 1 : 0,
    };
    requestPub(params);
  }

  void pubVoteImg(String text) async {
    WidgetUtils().showLoading();
    bool lawful = true;
    if (text.trim().isNotEmpty) {
      lawful = await FuncUtils().checkTextLawful(text, CheckTxtType.dynamic);
    }

    List resource = [];
    for (String image in voteImages) {
      if (image.isEmpty) break;
      String? path =
          await FuncUtils().requestUpload(image, UploadType.DYNAMIC_SQUARE);
      if (path == null) return WidgetUtils().cancelLoading();
      if (path.isEmpty) {
        WidgetUtils().showToast("上传失败");
        return WidgetUtils().cancelLoading();
      }

      // if (lawful) {
      //   lawful = await FuncUtils()
      //       .checkImageLawful([path], CheckImgType.IMG_DYDYNAMIC);
      // }

      resource.add({"content": path});
    }

    if (resource.isEmpty) return WidgetUtils().cancelLoading();

    Map params = {
      "content_type": DynamicType.VOTE_IMAGE.index,
      "content": text,
      "ids": getTopicString(),
      "location": getLocData(),
      "visibility": authType?.index ?? 1,
      "votes": resource,
      "status": lawful ? 1 : 0,
    };
    requestPub(params);
  }

  Map getLocData() {
    return {
      "longitude": longitude,
      "latitude": latitude,
      "location": location,
    };
  }

  String getTopicString() {
    String text = "";
    for (int index = 0; index < topicList.length; index++) {
      text += topicList[index].id.toString();
      if (index != topicList.length - 1) {
        text += ";";
      }
    }
    return text;
  }

  void requestPub(Map params) async {
    await DioUtils().httpRequest(
      NetConstants.publish,
      method: DioMethod.POST,
      params: params,
      onSuccess: (res) => onPubSuccess(res),
      onError: (code, msg) => onError(code, msg),
    );
    WidgetUtils().cancelLoading();
  }

  void onPubSuccess(response) {
    WidgetUtils().showToast("发布成功");
    WidgetUtils().popPage();

    // int dynamicId = response["id"] ?? 0;
    // if (dynamicId == 0) {
    //   WidgetUtils().popPage();
    //   WidgetUtils().showToast("您的动态疑似存在违规，请等待审核");
    //   return;
    // }
    // WidgetUtils().pushReplace(CommentPage(dynamicId: dynamicId));
  }

  void onError(code, msg) {
    WidgetUtils().showToast(msg);
  }

  void onTapAuth() async {
    bool? accept =
        await WidgetUtils().showAlert("请前往金豆派对语音进行实名认证.", confirmText: '前往实名');
    if (accept == null || !accept) return;
    await WidgetUtils().pushPage(AuthPage1());
  }
}
