import 'dart:convert';

import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class PublishPage extends StatefulWidget {
  const PublishPage({Key? key}) : super(key: key);

  @override
  _PublishPageState createState() => _PublishPageState();
}

class _PublishPageState extends State<PublishPage> {
  TextEditingController controller = TextEditingController();
  FocusNode focus = FocusNode();
  PublishProvider provider = PublishProvider();
  PublishTopicProvider topicProvider = PublishTopicProvider();

  @override
  void initState() {
    super.initState();
    EventUtils().on(EventConstants.E_EMOTION_INPUT,
        EventCall("PublishPage", onEmotionInput));
    EventUtils().on(
        EventConstants.E_EMOTION_DEL, EventCall("PublishPage", onEmotionDel));
    requestSelfInfo();
    requestAuthStatus();
    requestPubLevel();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: provider),
        ChangeNotifierProvider.value(value: topicProvider),
      ],
      child: GestureDetector(
    onTap: () {
    FocusScope.of(context).requestFocus(FocusNode());
         },
        child: Scaffold(
          backgroundColor: const Color(0xFFFFFFFF),
          resizeToAvoidBottomInset: false,
          body: FContainer(
            // image: ImageUtils().getAssetsImage("dynamic/dPushBg"),
            imageFit: BoxFit.fill,
            imageAlign: Alignment.topCenter,
            child: Column(children: [
              headerContent(),
              textInput(),
              Expanded(child: publishContent()),
              // bottomSheetWidget(),
            ]),
          ),
        ),
      ),

    );
  }

  Widget bottomSheetWidget() {
    return SafeArea(
      child: GestureDetector(
        onTap: () {},
        child: Column(mainAxisSize: MainAxisSize.min, children: [
          PublishBottom(
            onDelTopic: onDelTopic,
            updateAuth: updateAuth,
            updateLoc: updateLoc,
          ),
          PublishKeyboard(updateTopic: updateTopic)
        ]),
      ),
    );
  }

  Widget headerContent() {
    return FContainer(
      margin: EdgeInsets.only(top: ScreenUtil().statusBarHeight),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          CloseButton(
            color: Colors.black.withOpacity(0.8),
          ),
          Expanded(child: SizedBox()),
          btnPublish(),
          SizedBox(width: 15.w),
        ],
      ),
    );
  }

  @override
  void deactivate() {
    EventUtils().off(EventConstants.E_EMOTION_INPUT,
        EventCall("PublishPage", onEmotionInput));
    EventUtils().off(
        EventConstants.E_EMOTION_DEL, EventCall("PublishPage", onEmotionDel));
    super.deactivate();
  }

  @override
  void dispose() {
    controller.dispose();
    focus.dispose();
    super.dispose();
  }

  Widget btnPublish() {
    return FButton(

      isTheme: true,
      onTap: () => provider.onTapPublish(controller.text),
      child: FText(
        "发布",
        size: 24.sp,
        color: Colors.white,
      ),
    );
  }

  // 文本输入框
  Widget textInput() {
    return TextField(
      minLines: 5,
      maxLines: 8,
      maxLength: 500,
      focusNode: focus,
      controller: controller,
      onTap: () => EventUtils().emit(EventConstants.E_INPUT_ACTIVE, ""),
      style: TextStyle(color: Colors.black, fontSize: 28.sp),
      decoration: InputDecoration(
        contentPadding: EdgeInsets.symmetric(horizontal: 20.w),
        enabledBorder: const OutlineInputBorder(
            borderSide: BorderSide(color: Colors.transparent)),
        focusedBorder: const OutlineInputBorder(
            borderSide: BorderSide(color: Colors.transparent)),
        hintText: "分享你的心情给更多懂的人......",
        counterStyle:
            TextStyle(color: const Color(0xFF999999), fontSize: 28.sp),
        hintStyle: TextStyle(color: const Color(0xFF999999), fontSize: 28.sp),
      ),
    );
  }

  Widget publishContent() {
    return SingleChildScrollView(
      padding: EdgeInsets.all(20.w),
      child: Consumer<PublishProvider>(builder: (_, pro, __) {
        if (pro.curType == PublishType.TEXT_IMG) return PublishImg();
        if (pro.curType == PublishType.TEXT_AUDIO) return PublishAudio();
        if (pro.curType == PublishType.VOTE_TXT) return PublishVoteText();
        if (pro.curType == PublishType.VOTE_IMG) return PublishVoteImage();
        return const SizedBox();
      }),
    );
  }

  void requestPubLevel() {
    // DioUtils().asyncHttpRequest(
    //   NetConstants.gameConfig,
    //   method: DioMethod.POST,
    //   params: {"key": "DynamicConfig"},
    //   onSuccess: onPubLevel,
    // );
  }

  void onPubLevel(response) {
    try {
      Map? data = json.decode(response ?? "");
      provider.pubExp = data?["experience"] ?? 0;
    } catch (e) {}
  }

  void requestAuthStatus() {
    DioUtils().asyncHttpRequest(NetConstants.authStatus,
        method: DioMethod.GET, onSuccess: onAuthStatus);
  }

  void requestSelfInfo() {
    DioUtils().asyncHttpRequest(
      NetConstants.getUserInfo,
      method: DioMethod.GET,
      params: {"userId": ""},
      onSuccess: onUserInfo,
    );
  }

  void onUserInfo(response) async {
    if (response == null) return;
    UserConstants.userginInfo = LoginUserModel.fromJson(response);
  }

  void onAuthStatus(response) {
    if (response == null) return;
    provider.authStatus = getCheckStatus(response["status"]);
  }

  AuthStatus getCheckStatus(int? status) {
    for (AuthStatus item in AuthStatus.values) {
      if (item.value == status) return item;
    }
    return AuthStatus.NULL;
  }

  void updateAuth(DynamicAuthType? auth) {
    topicProvider.updateAuth(auth);
    provider.authType = auth;
  }

  void updateLoc(Map? data) {
    if (data == null) {
      topicProvider.updateLoc("");
      provider.location = "";
      provider.latitude = "";
      provider.longitude = "";
    } else {
      topicProvider.updateLoc(data["loc"]);
      provider.location = data["loc"];
      provider.latitude = data["lat"];
      provider.longitude = data["lon"];
    }
  }

  void onDelTopic(PublishTopic topic) {
    topicProvider.onDelTopic(topic);
    provider.topicList.removeWhere((e) => e.id == topic.id);
  }

  void updateTopic(List<PublishTopic> list) {
    topicProvider.updateTopic(list);
    provider.topicList = list;
  }

  void onEmotionInput(em) {
    if (!focus.hasFocus || !(em is String)) return;
    if (controller.text.length >= 500) return;
    String text = controller.text;
    int start = controller.selection.baseOffset;
    int end = controller.selection.extentOffset;
    String f = text.substring(0, start);
    String e = text.substring(end, text.length);
    controller.value = controller.value.copyWith(
      text: f + em + e,
      selection: TextSelection(
          baseOffset: start + em.length, extentOffset: start + em.length),
    );
  }

  void onEmotionDel(v) {
    if (!focus.hasFocus) return;
    String text = controller.text;
    int start = controller.selection.baseOffset;
    int end = controller.selection.extentOffset;

    String f, e;
    if (start == end) {
      if (start == 0) return;
      List<String> characters = text.characters.toList();
      int remove = getStartCharacter(characters, start);
      if (remove == -1) return;
      int length = characters[remove].length;
      f = characters.getRange(0, remove).join();
      e = characters.getRange(remove + 1, characters.length).join();
      controller.value = controller.value.copyWith(
        text: f + e,
        selection: TextSelection(
            baseOffset: start - length, extentOffset: start - length),
      );
    } else {
      f = text.substring(0, start);
      e = text.substring(end, text.length);
      controller.value = controller.value.copyWith(
        text: f + e,
        selection: TextSelection(baseOffset: start, extentOffset: start),
      );
    }
  }

  int getStartCharacter(List<String> characters, int start) {
    int offset = 0;
    for (int i = 0; i < characters.length; i++) {
      offset += characters[i].length;
      if (offset == start) {
        return i;
      }
    }
    return -1;
  }
}
