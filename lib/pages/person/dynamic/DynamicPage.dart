import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class DynamicPage extends StatefulWidget {
  final PageController homePageCtr;
  const DynamicPage(this.homePageCtr, {Key? key}) : super(key: key);

  @override
  _DynamicPageState createState() => _DynamicPageState();
}

class _DynamicPageState extends State<DynamicPage> {
  late PageController controller = PageController(initialPage: 1);
  final List _imageUrls = [];

  // 是否开启青少年模式
  bool adolescentMode = false;

  @override
  void initState() {
    super.initState();
    EventUtils().on(
      EventConstants.E_MQ_ADOMODE,
      EventCall("DynamicPage", (v) {
        adolescentMode = UserConstants.userginInfo?.info.teenmode == 1;
        setState(() {});
      }),
    );
    adolescentMode = UserConstants.userginInfo?.info.teenmode == 1;
  }

  @override
  void dispose() {
    EventUtils()
        .off(EventConstants.E_MQ_ADOMODE, EventCall("DynamicPage", (v) {}));
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return !adolescentMode
        ? Scaffold(
            resizeToAvoidBottomInset: false,
            body: FContainer(
              image: ImageUtils().getAssetsImage("public/huibg"),
              imageFit: BoxFit.fill,
              imageAlign: Alignment.topCenter,
              child: SafeArea(
                child: Column(
                  children: [
                    DynamicAppBar(controller),
                    SizedBox(height: 20.h),
                    BanDymicSwipe(BanbarSwper.DYT_COM.brand),
                    Expanded(child: dynamicContent()),
                  ],
                ),
              ),
            ),
          )
        : const AdolescentMode();
  }

  Widget dyBanrtContent() {
    return SizedBox(
      height: 140,
      child: Swiper(
        layout: SwiperLayout.CUSTOM,
        customLayoutOption: CustomLayoutOption(startIndex: -1, stateCount: 3)
            .addRotate([0.0, 0.0, 0.0]).addTranslate([
          const Offset(-320.0, 0.0),
          const Offset(0.0, 0.0),
          const Offset(320.0, 0.0)
        ]),
        itemCount: _imageUrls.length,
        itemWidth: 310.0,
        itemHeight: 140.0,
        scale: 0.6,
        //轮播图之间的间距
        viewportFraction: 0.8,
        //当前视窗比例，小于1时就会在屏幕内，可以看见旁边的轮播图
        indicatorLayout: PageIndicatorLayout.COLOR,
        //轮播图之间的间距
        autoplay: true,
        control: const SwiperControl(
          color: Colors.transparent,
        ),
        itemBuilder: (BuildContext context, int index) {
          return Container(
            //超出部分，可裁剪
            clipBehavior: Clip.hardEdge,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
            ),
            child: Image.network(
              _imageUrls[index],
              fit: BoxFit.fill,
            ),
          );
        },

        pagination: const SwiperPagination(
            builder: RectSwiperPaginationBuilder(
          color: Colors.black54,
          activeColor: Colors.white,
        )),
      ),
    );
  }

  Widget dynamicContent() {
    return PageView(controller: controller, children: [
      DynamicList(
          DynamicListType.DYNAMIC_FOLLOW, widget.homePageCtr, controller, 0),
      DynamicList(
          DynamicListType.DYNAMIC_RECOMMEND, widget.homePageCtr, controller, 1),
      DynamicList(
          DynamicListType.DYNAMIC_NEARBY, widget.homePageCtr, controller, 2),
    ]);
  }
}
