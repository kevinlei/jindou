import 'package:json_annotation/json_annotation.dart';

part 'dynamic_com.g.dart';

@JsonSerializable()
class DynamicCom {
  final int id;
  @JsonKey(name: 'post_id')
  final int postId;
  @JsonKey(name: 'user_id')
  final String userId;
  @JsonKey(name: 'user_info')
  final UserInfo userInfo;
  final String content;
  @JsonKey(name: 'parent_id')
  final int parentId;
  @JsonKey(name: 'like_count')
  final int likeCount;
  @JsonKey(name: 'created_at')
  final int createdAt;
  final int state;
  @JsonKey(name: 'like_state')
  final int likeState;
  @JsonKey(name: 'parent_name')
  final String? parentname;


  const DynamicCom({
    required  this.id,
    required  this.postId,
    required this.userId,
    required this.userInfo,
    required  this.content,
    required this.parentId,
    required this.likeCount,
    required this.createdAt,
    required this.state,
    required this.likeState,
     this.parentname,
  });

  factory DynamicCom.fromJson(Map<String, dynamic> json) =>
      _$DynamicComFromJson(json);

  Map<String, dynamic> toJson() => _$DynamicComToJson(this);
}

@JsonSerializable()
class UserInfo {
  @JsonKey(name: 'user_id')
  final String userId;
  @JsonKey(name: 'head_icon')
  final String headIcon;
  final String name;
  final int sex;
  final int birthday;

  const UserInfo({
    required  this.userId,
    required this.headIcon,
    required  this.name,
    required  this.sex,
    required this.birthday,
  });

  factory UserInfo.fromJson(Map<String, dynamic> json) =>
      _$UserInfoFromJson(json);

  Map<String, dynamic> toJson() => _$UserInfoToJson(this);
}
