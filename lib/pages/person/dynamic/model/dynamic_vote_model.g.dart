// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dynamic_vote_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DynamicVoteModel _$DynamicVoteModelFromJson(Map<String, dynamic> json) =>
    DynamicVoteModel()
      ..id = json['id'] as int
      ..communityId = json['community_id'] as int
      ..content = json['content'] as String
      ..count = json['count'] as int
      ..ifConfirm = json['if_confirm'] as int?;

Map<String, dynamic> _$DynamicVoteModelToJson(DynamicVoteModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'community_id': instance.communityId,
      'content': instance.content,
      'count': instance.count,
      'if_confirm': instance.ifConfirm,
    };
