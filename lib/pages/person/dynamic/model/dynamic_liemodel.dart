import 'package:json_annotation/json_annotation.dart';

part 'dynamic_liemodel.g.dart';

@JsonSerializable()
class DynamicLiemodel {
  final int? id;
  @JsonKey(name: 'user_id')
  final String? userId;
  @JsonKey(name: 'user_info')
  final UserInfo? userInfo;
  @JsonKey(name: 'content_text')
  final String? contentText;
  final int? visibility;
  final int? visible;
  final int? type;
  @JsonKey(name: 'forward_count')
  final int? forwardCount;
  @JsonKey(name: 'comment_count')
  final int? commentCount;
  @JsonKey(name: 'like_count')
  late  int? likeCount;
  @JsonKey(name: 'resource_url')
  final dynamic resourceUrl;
  final String? address;
  @JsonKey(name: 'content_type')
  final int? contentType;
  @JsonKey(name: 'topic_name1')
  final String? topicName1;
  @JsonKey(name: 'topic_name2')
  final String? topicName2;
  @JsonKey(name: 'topic_name3')
  final String? topicName3;
  @JsonKey(name: 'vote_type')
  final int? voteType;
  final List<Options>? options;
  @JsonKey(name: 'expiry_time')
  final int? expiryTime;
  @JsonKey(name: 'like_state')
  late  int? likeState;
  @JsonKey(name: 'created_at')
  final int? createdAt;
  @JsonKey(name: 'updated_at')
  final int? updatedAt;
  final int? state;
  final int? isfollow;
  @JsonKey(name: 'hx_uid')
  final String? hxuid;
  @JsonKey(name: 'hx_password')
  final String? hxpassword;
  @JsonKey(name: 'hx_nick_name')
  final String? hxnickname;


   DynamicLiemodel({
    this.id,
    this.userId,
    this.userInfo,
    this.contentText,
    this.visibility,
    this.visible,
    this.type,
    this.forwardCount,
    this.commentCount,
    this.likeCount,
    this.resourceUrl,
    this.address,
    this.contentType,
    this.topicName1,
    this.topicName2,
    this.topicName3,
    this.voteType,
    this.options,
    this.expiryTime,
    this.likeState,
    this.createdAt,
    this.updatedAt,
    this.state,
     this.isfollow,
     this.hxuid,
   this.hxpassword,
    this.hxnickname,
  });

  factory DynamicLiemodel.fromJson(Map<String, dynamic> json) =>
      _$DynamicLiemodelFromJson(json);

  Map<String, dynamic> toJson() => _$DynamicLiemodelToJson(this);
}

@JsonSerializable()
class UserInfo {
  @JsonKey(name: 'user_id')
  final String? userId;
  @JsonKey(name: 'head_icon')
  final String? headIcon;
  final String? name;
  final int? sex;
  final int? birthday;

  const UserInfo({
    this.userId,
    this.headIcon,
    this.name,
    this.sex,
    this.birthday,
  });

  factory UserInfo.fromJson(Map<String, dynamic> json) =>
      _$UserInfoFromJson(json);

  Map<String, dynamic> toJson() => _$UserInfoToJson(this);
}

@JsonSerializable()
class Options {
  final String? opt;
  final int? count;

  const Options({
    this.opt,
    this.count,
  });

  factory Options.fromJson(Map<String, dynamic> json) =>
      _$OptionsFromJson(json);

  Map<String, dynamic> toJson() => _$OptionsToJson(this);
}
