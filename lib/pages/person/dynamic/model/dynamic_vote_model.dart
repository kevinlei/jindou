import 'package:json_annotation/json_annotation.dart';

part 'dynamic_vote_model.g.dart';

@JsonSerializable()
class DynamicVoteModel extends Object {
  @JsonKey(name: 'id')
  late int id;

  @JsonKey(name: 'community_id')
  late int communityId;

  @JsonKey(name: 'content')
  late String content;

  @JsonKey(name: 'count')
  late int count;

  @JsonKey(name: 'if_confirm')
  int? ifConfirm;

  DynamicVoteModel();

  factory DynamicVoteModel.fromJson(Map<String, dynamic> srcJson) =>
      _$DynamicVoteModelFromJson(srcJson);

  Map<String, dynamic> toJson() => _$DynamicVoteModelToJson(this);
}
