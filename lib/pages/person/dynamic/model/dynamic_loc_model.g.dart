// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dynamic_loc_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DynamicLocModel _$DynamicLocModelFromJson(Map<String, dynamic> json) =>
    DynamicLocModel()
      ..id = json['id'] as int
      ..dynamicId = json['dynamic_id'] as int
      ..longitude = json['longitude'] as String
      ..latitude = json['latitude'] as String
      ..location = json['location'] as String?
      ..distance = (json['distance'] as num).toDouble();

Map<String, dynamic> _$DynamicLocModelToJson(DynamicLocModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'dynamic_id': instance.dynamicId,
      'longitude': instance.longitude,
      'latitude': instance.latitude,
      'location': instance.location,
      'distance': instance.distance,
    };
