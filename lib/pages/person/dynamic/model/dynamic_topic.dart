import 'package:json_annotation/json_annotation.dart';

part 'dynamic_topic.g.dart';

@JsonSerializable()
class DynamicTopic extends Object {
  @JsonKey(name: 'id')
  late int id;

  @JsonKey(name: 'dynamic_id')
  late int dynamicId;

  @JsonKey(name: 'topic_id')
  late int topicId;

  @JsonKey(name: 'topic')
  late String topic;

  DynamicTopic();

  factory DynamicTopic.fromJson(Map<String, dynamic> srcJson) =>
      _$DynamicTopicFromJson(srcJson);

  Map<String, dynamic> toJson() => _$DynamicTopicToJson(this);
}
