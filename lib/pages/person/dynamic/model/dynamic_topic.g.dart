// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dynamic_topic.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DynamicTopic _$DynamicTopicFromJson(Map<String, dynamic> json) => DynamicTopic()
  ..id = json['id'] as int
  ..dynamicId = json['dynamic_id'] as int
  ..topicId = json['topic_id'] as int
  ..topic = json['topic'] as String;

Map<String, dynamic> _$DynamicTopicToJson(DynamicTopic instance) =>
    <String, dynamic>{
      'id': instance.id,
      'dynamic_id': instance.dynamicId,
      'topic_id': instance.topicId,
      'topic': instance.topic,
    };
