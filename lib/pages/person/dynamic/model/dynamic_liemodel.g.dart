// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dynamic_liemodel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DynamicLiemodel _$DynamicLiemodelFromJson(Map<String, dynamic> json) =>
    DynamicLiemodel(
      id: json['id'] as int?,
      userId: json['user_id'] as String?,
      userInfo: json['user_info'] == null
          ? null
          : UserInfo.fromJson(json['user_info'] as Map<String, dynamic>),
      contentText: json['content_text'] as String?,
      visibility: json['visibility'] as int?,
      visible: json['visible'] as int?,
      type: json['type'] as int?,
      forwardCount: json['forward_count'] as int?,
      commentCount: json['comment_count'] as int?,
      likeCount: json['like_count'] as int?,
      resourceUrl: json['resource_url'],
      address: json['address'] as String?,
      contentType: json['content_type'] as int?,
      topicName1: json['topic_name1'] as String?,
      topicName2: json['topic_name2'] as String?,
      topicName3: json['topic_name3'] as String?,
      voteType: json['vote_type'] as int?,
      options: (json['options'] as List<dynamic>?)
          ?.map((e) => Options.fromJson(e as Map<String, dynamic>))
          .toList(),
      expiryTime: json['expiry_time'] as int?,
      likeState: json['like_state'] as int?,
      createdAt: json['created_at'] as int?,
      updatedAt: json['updated_at'] as int?,
      state: json['state'] as int?,
      isfollow: json['isfollow'] as int?,
      hxuid: json['hx_uid'] as String?,
      hxpassword: json['hx_password'] as String?,
      hxnickname: json['hx_nick_name'] as String?,
    );

Map<String, dynamic> _$DynamicLiemodelToJson(DynamicLiemodel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'user_id': instance.userId,
      'user_info': instance.userInfo,
      'content_text': instance.contentText,
      'visibility': instance.visibility,
      'visible': instance.visible,
      'type': instance.type,
      'forward_count': instance.forwardCount,
      'comment_count': instance.commentCount,
      'like_count': instance.likeCount,
      'resource_url': instance.resourceUrl,
      'address': instance.address,
      'content_type': instance.contentType,
      'topic_name1': instance.topicName1,
      'topic_name2': instance.topicName2,
      'topic_name3': instance.topicName3,
      'vote_type': instance.voteType,
      'options': instance.options,
      'expiry_time': instance.expiryTime,
      'like_state': instance.likeState,
      'created_at': instance.createdAt,
      'updated_at': instance.updatedAt,
      'state': instance.state,
      'isfollow': instance.isfollow,
      'hx_uid': instance.hxuid,
      'hx_password': instance.hxpassword,
      'hx_nick_name': instance.hxnickname,
    };

UserInfo _$UserInfoFromJson(Map<String, dynamic> json) => UserInfo(
      userId: json['user_id'] as String?,
      headIcon: json['head_icon'] as String?,
      name: json['name'] as String?,
      sex: json['sex'] as int?,
      birthday: json['birthday'] as int?,
    );

Map<String, dynamic> _$UserInfoToJson(UserInfo instance) => <String, dynamic>{
      'user_id': instance.userId,
      'head_icon': instance.headIcon,
      'name': instance.name,
      'sex': instance.sex,
      'birthday': instance.birthday,
    };

Options _$OptionsFromJson(Map<String, dynamic> json) => Options(
      opt: json['opt'] as String?,
      count: json['count'] as int?,
    );

Map<String, dynamic> _$OptionsToJson(Options instance) => <String, dynamic>{
      'opt': instance.opt,
      'count': instance.count,
    };
