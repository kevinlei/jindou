// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dynamic_com.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DynamicCom _$DynamicComFromJson(Map<String, dynamic> json) => DynamicCom(
      id: json['id'] as int,
      postId: json['post_id'] as int,
      userId: json['user_id'] as String,
      userInfo: UserInfo.fromJson(json['user_info'] as Map<String, dynamic>),
      content: json['content'] as String,
      parentId: json['parent_id'] as int,
      likeCount: json['like_count'] as int,
      createdAt: json['created_at'] as int,
      state: json['state'] as int,
      likeState: json['like_state'] as int,
      parentname: json['parent_name'] as String?,
    );

Map<String, dynamic> _$DynamicComToJson(DynamicCom instance) =>
    <String, dynamic>{
      'id': instance.id,
      'post_id': instance.postId,
      'user_id': instance.userId,
      'user_info': instance.userInfo,
      'content': instance.content,
      'parent_id': instance.parentId,
      'like_count': instance.likeCount,
      'created_at': instance.createdAt,
      'state': instance.state,
      'like_state': instance.likeState,
      'parent_name': instance.parentname,
    };

UserInfo _$UserInfoFromJson(Map<String, dynamic> json) => UserInfo(
      userId: json['user_id'] as String,
      headIcon: json['head_icon'] as String,
      name: json['name'] as String,
      sex: json['sex'] as int,
      birthday: json['birthday'] as int,
    );

Map<String, dynamic> _$UserInfoToJson(UserInfo instance) => <String, dynamic>{
      'user_id': instance.userId,
      'head_icon': instance.headIcon,
      'name': instance.name,
      'sex': instance.sex,
      'birthday': instance.birthday,
    };
