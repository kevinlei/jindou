import 'package:json_annotation/json_annotation.dart';

part 'dynamic_loc_model.g.dart';

@JsonSerializable()
class DynamicLocModel extends Object {
  @JsonKey(name: 'id')
  late int id;

  @JsonKey(name: 'dynamic_id')
  late int dynamicId;

  @JsonKey(name: 'longitude')
  late String longitude;

  @JsonKey(name: 'latitude')
  late String latitude;

  @JsonKey(name: 'location')
  String? location;

  @JsonKey(name: 'distance')
  late double distance;

  DynamicLocModel();

  factory DynamicLocModel.fromJson(Map<String, dynamic> srcJson) =>
      _$DynamicLocModelFromJson(srcJson);

  Map<String, dynamic> toJson() => _$DynamicLocModelToJson(this);
}
