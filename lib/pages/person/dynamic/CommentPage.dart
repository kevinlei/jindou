import 'package:echo/import.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'model/dynamic_liemodel.dart';

class CommentPage extends StatefulWidget {
  final int? dynamicId;
  const CommentPage({Key? key, required this.dynamicId}) : super(key: key);

  @override
  _CommentPageState createState() => _CommentPageState();
}

class _CommentPageState extends State<CommentPage> {
  DynamicLiemodel? model;
  int replyId = 0;
  String secondserid = "";
  int secondarypostId = 0;
  List<DynamicCom> comList = [];
  List<String> unlikeList = [];
  FocusNode focusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    decodeUnlike();
    requestDynamicInfo();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: Scaffold(
        body: FContainer(
          image: ImageUtils().getAssetsImage("public/huibg"),
          imageFit: BoxFit.fill,
          imageAlign: Alignment.topCenter,
          child: SafeArea(
              child: Column(
            children: [
              CommonAppBar(
                title: "动态",
                textColor: Colors.black,
                actions: [
                  GestureDetector(
                    onTap: () {},
                    child: FContainer(
                      width: 112.w,
                      height: 64.w,
                      image: ImageUtils().getAssetsImage("dynamic/dMore"),
                    ),
                  ),
                ],
              ),
              Expanded(
                  child: SingleChildScrollView(
                child: ConstrainedBox(
                    constraints:
                        BoxConstraints(maxHeight: ScreenUtil().screenHeight),
                    child: Column(children: [
                      if (model != null)
                        FContainer(height: 10.w, color: Colors.transparent),
                      if (model != null)
                        DynamicCell(model!,
                            enableTap: false,
                            onUpdate: onUpdate,
                            onDel: onDel,
                            onHidden: onHidden),
                      Container(
                        height: 10.h,
                        color: Color(0xFF222222).withOpacity(0.05),
                      ),
                      pinContent(),
                      Expanded(child: commentContent()),
                      SizedBox(height: 100.h)
                    ])),
              )),
            ],
          )),
        ),
        bottomSheet: CommentKeyboard(focusNode, onSend),

        // WidgetUtils().showBottomSheet(
        // setRoomInput(text, maxLength, onTap),
        // color: const Color(0xFF1D1928));
      ),
    );
  }

  @override
  void dispose() {
    focusNode.dispose();
    super.dispose();
  }

  Widget pinContent() {
    return FContainer(
      padding:
          EdgeInsets.only(left: 30.w, right: 30.w, bottom: 20.w, top: 30.w),
      child: Row(
        children: [
          FText(
            '全部评论',
            size: 32.sp,
            overflow: TextOverflow.ellipsis,
            color: Color(0xFF222222),
          ),
        ],
      ),
    );
  }

  Widget commentContent() {
    return ListView.builder(
      padding: EdgeInsets.symmetric(horizontal: 25.w),
      itemBuilder: (_, index) => CommentCell(
        comList[index],
        onTap: onTapCom,
        onLike: onTapLike,
      ),
      itemCount: comList.length,
    );
  }

  void decodeUnlike() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String list = preferences.getString(SharedKey.KEY_NO_LIKE.value) ?? "";
    unlikeList = list.split("&");
  }

  void onUpdate(DynamicLiemodel model) {
    this.model = model;
    if (!mounted) return;
    setState(() {});
  }

  void onDel(DynamicLiemodel model) {
    WidgetUtils().popPage(true);
  }

  void onHidden(DynamicLiemodel model) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    unlikeList.add("${model.id}");
    preferences.setString(SharedKey.KEY_NO_LIKE.value, unlikeList.join("&"));
    WidgetUtils().popPage(true);
  }

  void requestDynamicInfo() {
    DioUtils().asyncHttpRequest(
      NetConstants.dynamicInfo,
      method: DioMethod.POST,
      loading: false,
      params: {"post_id": widget.dynamicId},
      onSuccess: updateInfo,
    );
  }

  void updateInfo(response) {
    replyId = 0;
    secondserid = " ";
    secondarypostId = 0;
    if (response?["post_info"] == null) return;
    model = DynamicLiemodel.fromJson(response["post_info"]);
    List comment = response?["comment"] ?? [];
    comList.clear();
    for (var data in comment) {
      comList.add(DynamicCom.fromJson(data));
    }
    if (!mounted) return;
    setState(() {});
  }

  void onSend(String text) {
    if (model == null) return;

    FocusManager.instance.primaryFocus?.unfocus();
    if (replyId == 0) {
      DioUtils().asyncHttpRequest(
        NetConstants.dyComment,
        method: DioMethod.POST,
        params: {
          "type": replyId == 0 ? 1 : 2,
          "content": text,
          "user_id": model!.userId,
          "post_id": model!.id,
          "parent_id": replyId,
        },
        onSuccess: (_) => requestDynamicInfo(),
      );
    } else {
      DioUtils().asyncHttpRequest(
        NetConstants.dyReplies,
        method: DioMethod.POST,
        params: {
          "type": replyId == 0 ? 1 : 2,
          "content": text,
          "user_id": replyId == 0 ? model!.userId : secondserid,
          "post_id": replyId == 0 ? model!.id : secondarypostId,
          "id": replyId,
        },
        onSuccess: (_) => requestDynamicInfo(),
      );
    }
  }

  void onTapCom(int comId, String userId, int postId) {
    replyId = comId;
    secondserid = userId;
    secondarypostId = postId;
    if (focusNode.hasFocus) {
      SystemChannels.textInput.invokeMethod("TextInput.show");
    } else {
      focusNode.requestFocus();
    }
  }

  void onTapLike(int comId, int likeState, String userId, int postId) {
    if (model == null) return;
    DioUtils().asyncHttpRequest(
      NetConstants.dynamicVote,
      method: DioMethod.POST,
      params: {
        "type": 3,
        "post_id": model?.id,
        "comment_id": comId,
        "like_state": likeState,
        "user_id": userId
      },
      onSuccess: (_) => requestDynamicInfo(),
    );
  }
}
