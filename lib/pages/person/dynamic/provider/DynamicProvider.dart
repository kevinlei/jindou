import 'package:flutter/material.dart';
import 'package:echo/import.dart';

import '../model/dynamic_liemodel.dart';

class DynamicProvider extends ChangeNotifier {
  late DynamicListType listType;

  int curPage = 1;
  List<DynamicLiemodel> dyList = [];

  List<String> unlikeList = [];

  void initListType(type) async {
    listType = type;
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String list = preferences.getString(SharedKey.KEY_NO_LIKE.value) ?? "";
    unlikeList = list.split("&");
    refreshList();
  }

  Future<void> refreshList() async {
    curPage = 1;
    await _requestDyList(true);
  }

  Future<void> loadingList() async {
    curPage += 1;
    await _requestDyList(false);
  }

  // 请求推荐列表/关注列表/附近列表
  Future<void> _requestDyList(bool refresh) async {
    String url = NetConstants.dyRecList;
    Map params = {"page": curPage, "page_size": 10};
    if (listType == DynamicListType.DYNAMIC_FOLLOW) {
      url = NetConstants.dyNearbyList;
    } else if (listType == DynamicListType.DYNAMIC_NEARBY) {
      url = NetConstants.dyFollowList;
    }
    await DioUtils().httpRequest(
      url,
      method: DioMethod.POST,
      loading: false,
      params: params,
      onSuccess: refresh ? onRefresh : onLoad,
    );
  }

  void onRefresh(response) {
    dyList.clear();
    response ??= [];
    for (var data in response) {
      DynamicLiemodel model = DynamicLiemodel.fromJson(data);
      if (!unlikeList.contains("${model.id}")) {
        dyList.add(model);
      }
    }
    notifyListeners();
  }

  void onLoad(response) {
    response ??= [];
    if (response.isEmpty) return;
    for (var data in response) {
      DynamicLiemodel model = DynamicLiemodel.fromJson(data);
      if (!unlikeList.contains("${model.id}")) {
        dyList.add(model);
      }
    }
    notifyListeners();
  }

  void updateDynamic(DynamicLiemodel model) async {
    int index = dyList.indexWhere((element) => element.id == model.id);
    dyList[index] = model;
    notifyListeners();
  }

  void onDelDynamic(DynamicLiemodel model) async {
    dyList.removeWhere((element) => element.id == model.id);
    notifyListeners();
  }

  void onHidden(DynamicLiemodel model) async {
    dyList.removeWhere((element) => element.id == model.id);
    notifyListeners();
    SharedPreferences preferences = await SharedPreferences.getInstance();
    unlikeList.add("${model.id}");
    preferences.setString(SharedKey.KEY_NO_LIKE.value, unlikeList.join("&"));
  }
}
