// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'roomshome_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RoomshomeModel _$RoomshomeModelFromJson(Map<String, dynamic> json) =>
    RoomshomeModel(
      exteriorRoomId: json['exterior_room_id'] as int?,
      roomId: json['room_id'] as int?,
      roomName: json['room_name'] as String?,
      roomPersonNum: json['room_person_num'] as int?,
      roomImage: json['room_image'] as String?,
      roomRole: json['room_role'] as int?,
      isBanChat: json['is_ban_chat'] as int?,
      isBanMic: json['is_ban_mic'] as int?,
    );

Map<String, dynamic> _$RoomshomeModelToJson(RoomshomeModel instance) =>
    <String, dynamic>{
      'exterior_room_id': instance.exteriorRoomId,
      'room_id': instance.roomId,
      'room_name': instance.roomName,
      'room_person_num': instance.roomPersonNum,
      'room_image': instance.roomImage,
      'room_role': instance.roomRole,
      'is_ban_chat': instance.isBanChat,
      'is_ban_mic': instance.isBanMic,
    };
