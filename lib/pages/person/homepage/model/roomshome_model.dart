import 'package:json_annotation/json_annotation.dart';

part 'roomshome_model.g.dart';

@JsonSerializable()
class RoomshomeModel {
  @JsonKey(name: 'exterior_room_id')
  final int? exteriorRoomId;
  @JsonKey(name: 'room_id')
  final int? roomId;
  @JsonKey(name: 'room_name')
  final String? roomName;
  @JsonKey(name: 'room_person_num')
  final int? roomPersonNum;
  @JsonKey(name: 'room_image')
  final String? roomImage;
  @JsonKey(name: 'room_role')
  final int? roomRole;
  @JsonKey(name: 'is_ban_chat')
  final int? isBanChat;
  @JsonKey(name: 'is_ban_mic')
  final int? isBanMic;

  const RoomshomeModel({
    this.exteriorRoomId,
    this.roomId,
    this.roomName,
    this.roomPersonNum,
    this.roomImage,
    this.roomRole,
    this.isBanChat,
    this.isBanMic,
  });

  factory RoomshomeModel.fromJson(Map<String, dynamic> json) =>
      _$RoomshomeModelFromJson(json);

  Map<String, dynamic> toJson() => _$RoomshomeModelToJson(this);
}
