import 'package:flutter/material.dart';
import 'package:echo/import.dart';

/// 拉黑操作
class BannedPage extends StatefulWidget {
  final String userId;
  BannedPage({Key? key, required this.userId}) : super(key: key);

  @override
  _BannedPageState createState() => _BannedPageState();
}

class _BannedPageState extends State<BannedPage> {
  String nickName = "";
  String displayId = "";
  String password = "";
  String endDate = "";
  String desc = "";
  int endTime = 0;

  @override
  void initState() {
    super.initState();
    requestUserInfo(widget.userId);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: CommonAppBar(
        title: "封号处理",
        textColor: Colors.white,
        elevation: 1,
        backgroundColor: Color(0xFF252734),
      ),
      body: FContainer(
        height: double.infinity,
        color: Color(0xFF252734),
        padding: EdgeInsets.only(left: 20.w, right: 20.w, top: 20.w, bottom: 0),
        child: SingleChildScrollView(
          child: Column(children: [
            SizedBox(height: 20.w),
            buildTitleWidget("用户信息"),
            SizedBox(height: 20.w),
            buildInputWidget(nickName, "homepage/user", false),
            SizedBox(height: 20.w),
            buildInputWidget(displayId, "homepage/id1", false),
            SizedBox(height: 20.w),
            buildTitleWidget("封号时长"),
            SizedBox(height: 20.w),
            GestureDetector(
              onTap: onTimePress,
              child: buildInputWidget(endDate, "homepage/time", false),
            ),
            SizedBox(height: 20.w),
            buildTitleWidget("二级密码"),
            SizedBox(height: 20.w),
            buildInputWidget(password, "homepage/password", true,
                maxLen: 6, obscureText: true, onChanged: (value) {
              password = value;
              setState(() {});
            }),
            SizedBox(height: 20.w),
            buildTitleWidget("封禁理由"),
            SizedBox(height: 20.w),
            buildBoxWidget(),
            SizedBox(height: 20.w),
            Row(
              children: [
                Flexible(
                    child: FLoginButton("取消",
                        fontWeight: FontWeight.normal,
                        bgColor: Color(0xFF8F36FF),
                        txtColor: Colors.white,
                        radius: 90.w, onPress: () {
                  WidgetUtils().popPage();
                })),
                Flexible(
                  child: FLoginButton("确认",
                      fontWeight: FontWeight.normal,
                      isActivate: true,
                      radius: 90.w,
                      onPress: onPress),
                )
              ],
            ),
          ]),
        ),
      ),
    );
  }

  Widget buildTitleWidget(String title) {
    return Align(
      alignment: Alignment.centerLeft,
      child: FText(
        title,
        size: 28.sp,
        weight: FontWeight.normal,
        color: Colors.white.withOpacity(0.8),
      ),
    );
  }

  Widget buildBoxWidget() {
    InputBorder border =
        OutlineInputBorder(borderSide: BorderSide(color: Colors.transparent));
    return FContainer(
      radius: BorderRadius.circular(8),
      color: Color(0xff1E1F2A),
      height: 220.h,
      width: double.infinity,
      child: TextField(
        maxLength: 60,
        style: TextStyle(color: Colors.white, fontSize: 30.sp),
        onChanged: (value) {
          desc = value;
          setState(() {});
        },
        decoration: InputDecoration(
          focusedBorder: border,
          enabledBorder: border,
          disabledBorder: border,
          counterText: "",
          contentPadding: EdgeInsets.all(5),
          hintText: "请输入封禁理由",
          hintStyle: TextStyle(color: Color(0xFFBBBBBB), fontSize: 26.sp),
        ),
      ),
    );
  }

  Widget buildInputWidget(String hintText, String? icon, bool enabled,
      {bool obscureText = false,
      int? maxLen,
      ValueChanged<String>? onChanged}) {
    InputBorder border =
        OutlineInputBorder(borderSide: BorderSide(color: Colors.transparent));

    return FContainer(
      radius: BorderRadius.circular(8),
      color: Color(0xff1E1F2A),
      height: 84.h,
      width: double.infinity,
      child: Row(
        children: [
          SizedBox(
            width: 18.w,
          ),
          LoadAssetImage(
            icon,
            width: 45.w,
            color: Colors.white.withOpacity(0.8),
          ),
          SizedBox(width: 18.w),
          Flexible(
              child: TextField(
            enabled: enabled,
            obscureText: obscureText,
            maxLength: maxLen,
            style: TextStyle(color: Colors.white, fontSize: 30.sp),
            onChanged: onChanged,
            decoration: InputDecoration(
              focusedBorder: border,
              enabledBorder: border,
              disabledBorder: border,
              counterText: "",
              contentPadding: EdgeInsets.zero,
              hintText: hintText,
              hintStyle: TextStyle(color: Color(0xFFBBBBBB), fontSize: 26.sp),
            ),
          )),
        ],
      ),
    );
  }

  void requestUserInfo(String userId) {
    DioUtils().asyncHttpRequest(
      NetConstants.getUserInfo,
      method: DioMethod.GET,
      params: {"userId": userId},
      onSuccess: (result) {
        nickName = result['name'] ?? "";
        displayId = result['displayId'] ?? "";
        if (!mounted) return;
        setState(() {});
      },
    );
  }

  void onTimePress() async {
    FocusManager.instance.primaryFocus?.unfocus();
    int? time = await WidgetUtils().showBottomSheet(TimePicker());
    if (time == null) return;
    endTime = time;
    int year = DateTime.fromMillisecondsSinceEpoch(time).year;
    int month = DateTime.fromMillisecondsSinceEpoch(time).month;
    int day = DateTime.fromMillisecondsSinceEpoch(time).day;
    endDate = "$year-$month-$day";
    if (!mounted) return;
    setState(() {});
  }

  void onPress() {
    if (endTime == 0) return WidgetUtils().showToast("请选择封禁时间!");
    if (desc.isEmpty) return WidgetUtils().showToast("请填写封禁理由!");
    if (password.isEmpty) return WidgetUtils().showToast("请填二级密码!");

    // DioUtils().asyncHttpRequest(
    //   NetConstants.denyUser,
    //   method: DioMethod.POST,
    //   params: {
    //     "pwd": password.trim(),
    //     "userId": widget.userId,
    //     "endTime": endTime,
    //     "reason": desc.trim(),
    //   },
    //   onSuccess: (result) {
    //     WidgetUtils().popPage();
    //     WidgetUtils().showToast("封禁成功");
    //   },
    // );
  }
}
