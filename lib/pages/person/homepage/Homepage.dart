import 'dart:io';
import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import '../club/model/club_roomwho_mdoe.dart';
import 'dart:ui' as ui;

class Homepage extends StatefulWidget {
  final String userId;
  const Homepage({Key? key, required this.userId}) : super(key: key);

  @override
  _HomepageState createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  HomePageProvider pageProvider = HomePageProvider();
  HomePageUserProvider userProvider = HomePageUserProvider();
  HomepagePhotoProvider photoProvider = HomepagePhotoProvider();
  late final ClubRoomwhoMdoe model;
  GlobalKey globalKey = GlobalKey();
  List<Uint8List> images = [];

  @override
  void initState() {
    super.initState();
    FocusManager.instance.primaryFocus?.unfocus();
    initHomepage();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: pageProvider),
        ChangeNotifierProvider.value(value: userProvider),
        ChangeNotifierProvider.value(value: photoProvider),
      ],
      child: Scaffold(
        backgroundColor: Colors.white,
        body: FRefreshLayout.custom(
            onRefresh: callRefresh,
            firstRefresh: false,
            slivers: [
              Consumer<HomePageUserProvider>(builder: (_, pro, __) {
                return HomePageAppBar(
                  userId: widget.userId,
                  head_icon: pro.infoModel?.info.headIcon ?? "",
                  onPush: callRefresh,
                );
              }),
              inNameCard(),
              inRoomCard(),
              // inClubCard(),
              HomePageList()
            ]),
        bottomNavigationBar: buttonContent(),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget inNameCard() {
    return SliverToBoxAdapter(
      child: Consumer<HomePageUserProvider>(builder: (_, pro, __) {
        int idLevel = pro.infoModel?.info.displayIdLevel ?? 0;
        String id = pro.infoModel?.info.displayId.toString() ?? "";
        String wealthbadge =
            pro.infoModel?.gradeInfo.userGrade.wealth.badge.toString() ?? "";
        String charmbadge =
            pro.infoModel?.gradeInfo.userGrade.charm.badge.toString() ?? "";
        return Visibility(
          // visible: pro.inRoomInfo != null,
          child: FContainer(
            // height: 104.w,
            radius: BorderRadius.circular(10),
            margin: EdgeInsets.fromLTRB(32.w, 40.h, 32.w, 0),
            // padding: EdgeInsets.all(4.w),
            child: FContainer(
              padding: EdgeInsets.only(left: 15.w),
              radius: BorderRadius.circular(10.h),
              gradientBegin: Alignment.centerLeft,
              gradientEnd: Alignment.centerRight,
              child: Column(
                children: [
                  Row(
                    children: [
                      Row(
                        children: [
                          ConstrainedBox(
                            constraints: BoxConstraints(maxWidth: 260.w),
                            child: FText(
                              pro.infoModel?.info.name ?? "",
                              size: 36.sp,
                              overflow: TextOverflow.ellipsis,
                              weight: FontWeight.bold,
                              color: Colors.black,
                            ),
                          ),
                          SizedBox(width: 10.w),
                          userAge(
                            pro.infoModel?.info.sex ?? 1,
                            pro.infoModel?.info.birthday ?? 0,
                          ),
                        ],
                      ),
                      const Spacer(),
                      FContainer(
                        width: 160.w,
                        height: 40.w,
                        padding: EdgeInsets.symmetric(horizontal: 10.w),
                        color: const Color(0xFF8C2828),
                        radius: BorderRadius.circular(30.w),
                        child: Row(children: [
                          FContainer(
                            width: 20.w,
                            height: 20.w,
                            color: const Color(0xFF3AE5B5),
                            radius: BorderRadius.circular(10.w),
                          ),
                          SizedBox(
                            width: 10.w,
                          ),
                          FText(pro.infoModel?.online == 20 ? "她正在线" : "她正离线",
                              size: 24.sp, color: const Color(0xFFFFFFFF))
                        ]),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10.w,
                  ),
                  Row(
                    children: [
                      ConfigManager().userWealthCharmIcon(
                        pro.infoModel != null
                            ? pro.infoModel!.gradeInfo.userGrade.wealth.badge
                            : "",
                        pro.infoModel != null
                            ? pro.infoModel!.gradeInfo.userGrade.wealth.grade
                            : 0,
                        marginRight: 5.w,
                        isWealth: true,
                      ),
                      ConfigManager().userWealthCharmIcon(
                        pro.infoModel != null
                            ? pro.infoModel!.gradeInfo.userGrade.charm.badge
                            : "",
                        pro.infoModel != null
                            ? pro.infoModel!.gradeInfo.userGrade.charm.grade
                            : 0,
                        marginRight: 5.w,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10.w,
                  ),
                  Row(
                    children: [
                      LoadAssetImage("person/id", width: 50.w),
                      idLevel != 0 ? BeNum(id) : userId(id),
                      copyUserId(id),
                      FText(
                          '| 被${pro.infoModel?.info != null ? pro.infoModel?.visitorCount : 0}人关注',
                          color: const Color(0xFF222222).withOpacity(0.6)),
                    ],
                  ),
                  Row(
                    children: [
                      LoadAssetImage("person/pusts", width: 30.w),
                    ],
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: FText(pro.infoModel?.info.sign.toString() ?? "",
                            maxLines: 10,
                            size: 28.sp,
                            color: Colors.black.withOpacity(0.3)),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
        );
      }),
    );
  }

  Future<void> _capturePng() async {
    final RenderRepaintBoundary boundary =
        globalKey.currentContext!.findRenderObject()! as RenderRepaintBoundary;

    if (boundary.debugNeedsPaint) {
      await Future.delayed(const Duration(milliseconds: 20));
      return _capturePng();
    }

    final ui.Image image = await boundary.toImage();
    final ByteData? byteData =
        await image.toByteData(format: ui.ImageByteFormat.png);
    final Uint8List pngBytes = byteData!.buffer.asUint8List();
    images.add(pngBytes);
    setState(() {});
  }

  Widget userId(String id) {
    return FContainer(
      margin: EdgeInsets.only(
        left: 10.w,
        right: 10.w,
        top: 5.h,
      ),
      align: Alignment.center,
      child: FText("ID:$id", color: Colors.black.withOpacity(0.4), size: 24.sp),
    );
  }

  // 复制用户ID
  Widget copyUserId(String id) {
    return GestureDetector(
      child: FContainer(
        align: Alignment.center,
        child: LoadAssetImage(
          "person/rootzi",
          width: 50.w,
        ),
      ),
      onTap: () {
        Clipboard.setData(ClipboardData(text: id));
        WidgetUtils().showToast("ID复制成功");
      },
    );
  }

  Widget userAge(int sex, int bir) {
    return GenderAge(sex == 1, bir);
  }

  Widget inRoomCard() {
    return SliverToBoxAdapter(
      child: Consumer<HomePageUserProvider>(builder: (_, pro, __) {
        return Visibility(
          visible: pro.inRoomInfo != null,
          child: FContainer(
            height: 104.w,
            radius: BorderRadius.circular(10),
            margin: EdgeInsets.fromLTRB(32.w, 40.h, 32.w, 0),
            gradient: [
              const Color(0xFFFF753F).withOpacity(0.2),
              const Color(0xFFFF753F).withOpacity(0.2),
            ],
            gradientBegin: Alignment.centerLeft,
            gradientEnd: Alignment.centerRight,
            child: FContainer(
              padding: EdgeInsets.symmetric(horizontal: 12.w),
              radius: BorderRadius.circular(10.h),
              gradient: [
                const Color(0xFFFF753F).withOpacity(0.2),
                const Color(0xFFFF753F).withOpacity(0.2),
              ],
              gradientBegin: Alignment.centerLeft,
              gradientEnd: Alignment.centerRight,
              child: Row(
                children: [
                  userHead(),
                  SizedBox(
                    width: 16.w,
                  ),
                  Expanded(
                    child: Row(
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 10.w,
                            ),
                            FText(
                              "正在房间",
                              weight: FontWeight.bold,
                              size: 28.sp,
                            ),
                            SizedBox(
                              height: 8.w,
                            ),
                            FText(
                              pro.inRoomInfo?.roomName ?? "获取失败",
                              color: Colors.black.withOpacity(0.6),
                              size: 24.sp,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  FButton(
                    isTheme: true,
                    onTap: () =>
                        onTapJumpRoom(pro.inRoomInfo?.exteriorRoomId ?? 0),
                    child: FText(
                      "去找TA",
                      size: 24.sp,
                      color: Colors.white,
                      align: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      }),
    );
  }

  Widget userHead() {
    return Consumer<HomePageUserProvider>(builder: (_, pro, __) {
      String image = NetConstants.ossPath + (pro.inRoomInfo?.roomImage ?? "");
      return FContainer(
        width: 80.w,
        height: 80.w,
        foreImage: pro.infoModel?.roomInfo != null
            ? ImageUtils().getAssetsImage("person/voiceFoot")
            : null,
        image: ImageUtils().getImageProvider(image),
        imageFit: BoxFit.fill,
        radius: BorderRadius.circular(80.w),
        border: Border.all(color: const Color(0xFF9A43FF), width: 4.w),
      );
    });
  }

  Widget inClubCard() {
    return SliverToBoxAdapter(
      child: Padding(
        padding: EdgeInsets.only(bottom: 20.h),
        child: ClubCell(
          model: model,
        ),
      ),
    );
  }

  Widget buttonContent() {
    return Consumer<HomePageUserProvider>(builder: (_, pro, __) {
      if (widget.userId == UserConstants.userginInfo?.info.userId) {
        return const SizedBox();
      }
      return Row(children: [
        SizedBox(width: 80.w),
        buttonFollow(pro.hasFollow),
        SizedBox(width: 20.w),
        buttonChat(),
        SizedBox(width: 80.w),
      ]);
    });
  }

  Widget buttonFollow(bool hasFollow) {
    Widget btn = FContainer(
      height: 88.w,
      radius: BorderRadius.circular(88.w),
      border: Border.all(color: const Color(0xFFFF753F)),
      align: Alignment.center,
      margin: EdgeInsets.symmetric(vertical: 10.h),
      child: FText(
        hasFollow ? "已关注" : "关注",
        size: 28.sp,
        color: const Color(0xFFFF753F),
      ),
    );
    return Flexible(
      child: GestureDetector(
        onTap: () => onTapFollow(hasFollow),
        child: Padding(
          padding: EdgeInsets.only(
              bottom: Platform.isIOS ? ScreenUtil().bottomBarHeight : 0),
          child: btn,
        ),
      ),
    );
  }

  Widget buttonChat() {
    Widget btn = FContainer(
      height: 88.w,
      radius: BorderRadius.circular(88.w),
      align: Alignment.center,
      gradientBegin: Alignment.topLeft,
      gradientEnd: Alignment.bottomRight,
      margin: EdgeInsets.symmetric(vertical: 10.h),
      gradient: [
        const Color(0xFF46F5F6).withOpacity(1.0),
        const Color(0xFFFFA7FB).withOpacity(1.0),
        const Color(0xFFFF82B2).withOpacity(1.0)
      ],
      child: FText(
        "私 聊",
        size: 28.sp,
        color: Colors.white,
      ),
    );
    return Flexible(
        child: GestureDetector(
            onTap: onTapChat,
            child: Padding(
              padding: EdgeInsets.only(
                  bottom: Platform.isIOS ? ScreenUtil().bottomBarHeight : 0),
              child: btn,
            )));
  }

  void onTapJumpRoom(int inRoomId) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    int roomId = preferences.getInt(SharedKey.KEY_ROOM_ID.value) ?? 0;
    if (roomId == inRoomId) {
      WidgetUtils().showToast("已在该房间无法重复进入!");
      return;
    }
    // WidgetUtils().pushPage(RoomPage(inRoomId));
    AppManager().joinRoom(inRoomId);
  }

  void onTapFollow(bool hasFollow) {
    userProvider.requestFollow(hasFollow);
  }

  void onTapChat() {
    String conId = userProvider.infoModel?.info.hxInfo.uid ?? "";
    if (conId.isEmpty) return;
    String user = widget.userId;
    String head = userProvider.infoModel?.info.headIcon ?? "";
    String name = userProvider.infoModel?.info.name ?? "";
    HxMsgExt msgExt = HxMsgExt(user, head, name);

    WidgetUtils().pushPage(MsgChat(conId, msgExt));
  }

  void onTapTab(int page) {
    pageProvider.update();
    callRefresh();
  }

  void initHomepage() async {
    userProvider.requestUserInfo(widget.userId);
    userProvider.requestRelationCount();
    userProvider.requestUserClub(widget.userId);
    userProvider.requestRelation();
    pageProvider.requestUserSet(widget.userId);
    pageProvider.requestDynamic(true);
    if (widget.userId != userProvider.infoModel?.info.userId) {
      userProvider.requestInRoom();
    }
  }

  Future<void> callRefresh() async {
    if (widget.userId != userProvider.infoModel?.info.userId) {
      userProvider.requestInRoom();
    }
    userProvider.requestRelation();
    pageProvider.requestDynamic(true);
    userProvider.requestUserInfo(widget.userId);
  }
}
