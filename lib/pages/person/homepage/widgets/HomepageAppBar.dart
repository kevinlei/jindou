import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class HomePageAppBar extends StatelessWidget {
  final String userId;
  final String head_icon;
  final Function() onPush;

  const HomePageAppBar({Key? key, required this.userId,required this.head_icon, required this.onPush})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      pinned: true,
      backgroundColor: const Color(0xFFF5F6F7),
      title: const Text("个人主页",
          style: TextStyle(
            color: Colors.white,
          )),
      centerTitle: true,
      elevation: 0,
      actions: [
        idButton(),
        userId == UserConstants.userginInfo?.info.userId
            ? modifyButton()
            : const FText('')
      ],
      expandedHeight: 454.h,
      flexibleSpace: flexibleSpace(),
    );
  }

  Widget shareButton() {
    Widget btn = Padding(
      padding: EdgeInsets.only(right: 32.w),
      child: LoadAssetImage("public/dShare", width: 36.w),
    );
    return GestureDetector(onTap: onTapShare, child: btn);
  }

  Widget idButton() {
    Widget btn = Padding(
      padding: EdgeInsets.only(right: 30.w),
      child: LoadAssetImage("person/id", width: 42.w),
    );
    return Consumer<HomePageUserProvider>(builder: (_, pro, __) {
      bool visible = true;
      if (UserConstants.userginInfo?.info.userId == userId) {
        visible = false;
      } else {
        // int selfRole = UserConstants.userInfo?.userRole ?? UserRole.DEF.index;
        int selfRole = 4;
        // int otherRole = pro.infoModel?.userRole ?? UserRole.DEF.index;
        int otherRole = 4;
        if (selfRole >= UserRole.OFFICIAL.index && selfRole > otherRole) {
          visible = true;
        } else {
          visible = false;
        }
      }

      return Visibility(
          visible: visible,
          child: GestureDetector(onTap: onIdModify, child: btn));
    });
  }

  Widget modifyButton() {
    Widget btn = Padding(
      padding: EdgeInsets.only(right: 32.w),
      // child: LoadAssetImage("homepage/modify", width: 36.w),
    );
    return GestureDetector(onTap: onTapModify, child: btn);
  }

  FlexibleSpaceBar flexibleSpace() {
    return FlexibleSpaceBar(
      background: Stack(
          fit: StackFit.expand,
          alignment: Alignment.bottomLeft,
          children: [
            // HomepagePhoto(),
        Container(
        height: 454.h,
        child: LoadNetImage(
          NetConstants.ossPath + head_icon,
          fit: BoxFit.cover, height: 454.h,),
      ),
            HomePageUser(),
          ]),
    );
  }

  void onTapShare() {}

  void onTapModify() {
    // WidgetUtils().pushPage(UserPage(), fun: onPush);
  }

  void onIdModify() {
    // WidgetUtils().pushPage(BannedPage(userId: userId), fun: onPush);
  }
}
