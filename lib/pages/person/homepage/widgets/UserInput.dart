import 'package:flutter/material.dart';
import 'package:echo/import.dart';

import '../../romhome/RoomInspuName.dart';

// ignore: must_be_immutable
class UserInput extends StatelessWidget {
  final bool inputName;
  final String origin;
  final String? hintText;
  final int? maxLen;
  final String titleName;
  UserInput(this.inputName, this.origin, this.titleName ,{Key? key, this.hintText, this.maxLen})
      : super(key: key);

  String inputText = "";

  @override
  Widget build(BuildContext context) {
    return FContainer(
        padding: EdgeInsets.only(left: 30.w, right: 30.w, bottom: 20.w),
    image: ImageUtils().getAssetsImage("public/huibg"),
    imageFit: BoxFit.fill,
    imageAlign: Alignment.topCenter,
    child: SafeArea(child:Scaffold(
        backgroundColor: Colors.transparent,
    appBar: inputAppbar(),
    body: ListView(
    padding: EdgeInsets.fromLTRB(25.w, 0, 25.w, 0),
    children: [
    inputName
        ? RoomInsputName(origin, onTextInput, maxLen: maxLen)
        : RoomNoticeInput(origin, hintText ?? "", onTextInput)
    ],
    ),
    )));

  }

  AppBar inputAppbar() {
    return AppBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      leading: BackButton(onPressed: onTapClose,color: Colors.black,),
      title: FText(titleName,
          size: 36.sp, color: Colors.black, weight: FontWeight.bold),
      actions: [btnConfirm()],
    );
  }

  Widget btnConfirm() {
    Widget btn = FContainer(
      width: 100.w,
      height: 63.w,
      margin: EdgeInsets.only(right: 25.w),
      radius: BorderRadius.circular(63.w),
      gradientBegin: Alignment.topLeft,
      gradientEnd: Alignment.bottomRight,
      gradient: [Color(0xFFFF753F), Color(0xFFFF753F),Color(0xFFFF753F)],
   //   gradient: [Color(0xFF473A5D),Color(0xFF38295C)],
      align: Alignment.center,
      child: FText("确认", size: 26.sp,color: Colors.white,),
    );
    return Center(
      child: GestureDetector(onTap: onTapConfirm, child: btn),
    );
  }

  void onTextInput(value) => inputText = value;

  void onTapClose() {
    FocusManager.instance.primaryFocus?.unfocus();
    WidgetUtils().popPage();
  }

  void onTapConfirm() {
    FocusManager.instance.primaryFocus?.unfocus();
    WidgetUtils().popPage(inputText);
  }
}
