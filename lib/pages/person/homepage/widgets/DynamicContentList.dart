import 'package:flutter/material.dart';
import 'package:echo/import.dart';

import '../../dynamic/model/dynamic_liemodel.dart';

class DynamicContentList extends StatefulWidget {
  final String userId;
  const DynamicContentList(this.userId, {Key? key}) : super(key: key);

  @override
  _DynamicContentListState createState() => _DynamicContentListState();
}

class _DynamicContentListState extends State<DynamicContentList> {
  HomePageProvider provider = HomePageProvider();
  bool isSelf = false;
  int curPage = 1;
  bool dynamicState = true;
  String vates = '默认排序';

  @override
  void initState() {
    super.initState();
    isSelf = widget.userId == UserConstants.userginInfo?.info.userId;


  }

  Future<void> onRefreshxx() async {
    requestDynamic(true);
  }

  Future<void> onLoadxx() async {
    requestDynamic(false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: const Color(0xFFF5F6F7),
        appBar: const CommonAppBar(
          title: "动态列表",
          textColor: Colors.black,
          elevation: 1,
          backgroundColor: Color(0xFFF5F6F7),
        ),
        body: FRefreshLayout(
            onRefresh: onRefreshxx, onLoad: onLoadxx, child: dynamicContent()));
  }

  Widget dynamicContent() {
    if (provider.dynamicList.isEmpty) {
      return SizedBox(
        height: 800.h,
        child: Center(
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
            LoadAssetImage("public/trends", width: 160.w, height: 160.w),
            SizedBox(height: 10.w),
            const FText("暂无动态", color: Color(0xFF999999)),
            // if (isSelf) buttonPublish()
          ]),
        ),
      );
    }

    return ListView.builder(
      itemBuilder: (_, index) => DynamicCell(provider.dynamicList[index],
          onUpdate: provider.updateDynamic,
          onDel: provider.onDelDynamic,
          onHidden: provider.onHidden,
          toHomepage: false),
      itemCount: provider.dynamicList.length,
    );
  }

  Widget buttonPublish() {
    Widget btn = FContainer(
      width: 160.w,
      height: 60.w,
      margin: EdgeInsets.only(top: 20.h),
      radius: BorderRadius.circular(60.w),
      color: const Color(0xFF8F36FF),
      align: Alignment.center,
      child: const FText("发布动态"),
    );
    return GestureDetector(onTap: onTapPublish, child: btn);
  }

  void onTapPublish() {
    // WidgetUtils().pushReplace(PublishPage(widget.homePageCtr));
  }

  void requestDynamic(bool isRefresh) {
    if (widget.userId.isEmpty) return;
    if (isRefresh) {
      dynamicState = true;
      curPage = 1;
    } else if (dynamicState) {
      curPage += 1;
    }
    if (!dynamicState) return;
    DioUtils().asyncHttpRequest(NetConstants.userDynamic,
        method: DioMethod.POST,
        loading: false,
        params: {"page": curPage, "user_id": widget.userId,"page_size":10},
        onSuccess: isRefresh ? onRefresh : onLoad,
        onError: (e, code) => dynamicState = false);
  }

  void onRefresh(response) {
    provider.dynamicList.clear();
    response ??= [];
    for (var data in response) {
      DynamicLiemodel model = DynamicLiemodel.fromJson(data);
      provider.dynamicList.add(model);
    }
    if (!mounted) return;
    setState(() {});
  }

  void onLoad(response) {
    response ??= [];
    if (response.isEmpty) {
      dynamicState = false;
      return;
    }
    for (var data in response) {
      DynamicLiemodel model = DynamicLiemodel.fromJson(data);
      provider.dynamicList.add(model);
    }
    if (!mounted) return;
    setState(() {});
  }
}
