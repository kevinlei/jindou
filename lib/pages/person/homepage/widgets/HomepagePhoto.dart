import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class HomepagePhoto extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget content = Consumer<HomepagePhotoProvider>(builder: (_, pro, __) {
      if (pro.photoList.isEmpty)
        return LoadAssetImage("login/default", fit: BoxFit.cover, height: 454.h);
      return Container(
        height: 454.h,
        child: Swiper(
          autoplay: pro.photoList.length > 1,
          itemBuilder: (_, index) => LoadNetImage(
              NetConstants.ossPath + pro.photoList[index],
              fit: BoxFit.cover, height: 454.h,),
          itemCount: pro.photoList.length,
        ),
      );
    });
    return content;
  }
}
