import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class HomePageTab extends StatelessWidget {
  final TabController tabController;
  final Function(int) onTap;
  const HomePageTab(this.tabController, this.onTap, {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverPersistentHeader(
      pinned: true,
      delegate: HomepageTabDelegate(TabBar(
        isScrollable: true,
        labelStyle: TextStyle(fontWeight: FontWeight.w600, fontSize: 36.sp),
        labelColor: Colors.white,
        unselectedLabelStyle:
            TextStyle(fontWeight: FontWeight.normal, fontSize: 36.sp),
        unselectedLabelColor: Colors.white.withOpacity(0.5),
        tabs: [Tab(text: "资料"), Tab(text: "动态")],
        controller: tabController,
        indicatorColor: Color(0xFFB965FF),
        indicatorSize: TabBarIndicatorSize.label,
        onTap: onTap,
      )),
    );
  }
}

class HomepageTabDelegate extends SliverPersistentHeaderDelegate {
  final Widget child;

  HomepageTabDelegate(this.child);

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Material(
      color: Color(0xFF26213B),
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 10.h),
        child: child,
      ),
    );
  }

  @override
  double get maxExtent => 80.h;

  @override
  double get minExtent => 80.h;

  @override
  bool shouldRebuild(covariant SliverPersistentHeaderDelegate oldDelegate) {
    return false;
  }
}
