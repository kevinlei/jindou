import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class HomePageUser extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    String audioPath = "";
    return Padding(
      padding: EdgeInsets.fromLTRB(40.w, 0, 20.w, 20.h),
      child: Row(children: [
        Column(children: [
          const Spacer(),
          // userHead(),
          Consumer<HomePageUserProvider>(builder: (_, pro, __) {
            audioPath = pro.infoModel?.info.voice ?? "";
            return Visibility(
              visible: audioPath.isNotEmpty,
              child: GestureDetector(
                onTap: () {
                  try {
                    if (audioPath.isEmpty) {
                      return WidgetUtils().showToast("播放路径错误");
                    }
                    String source = NetConstants.ossPath + audioPath;
                    debugPrint("播放录音地址$source");
                    bool state = source.contains('wav');
                    AppManager().soundPlayer.startPlayer(
                          fromURI: source,
                          codec: !state ? Codec.aacADTS : Codec.pcm16WAV,
                          sampleRate: 44100,
                        );
                  } catch (err) {
                    debugPrint("播放录音错误：$err");
                  }
                },
                child: LoadAssetImage("person/audios", width: 120.w),
              ),
            );
          }),
          // SizedBox(height: 20.h),
        ]),
        SizedBox(width: 5.w),
        Expanded(
          child: Consumer<HomePageUserProvider>(builder: (_, pro, __) {
            return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Spacer(),
                  SizedBox(height: 4.h),
                  // (pro.infoModel?.displayIdLevel ?? 0) != 0
                ]);
          }),
        ),
        Column(children: [
          const Spacer(),
          Consumer<HomePageUserProvider>(builder: (_, prop, __) {
            //prop.infoModel.info 照片墙
            return Visibility(visible: true, child: cardfouter());
          }),
        ]),
      ]),
    );
  }

  Widget cardfouter() {
    return Consumer<HomePageUserProvider>(builder: (_, pro, __) {
      List<String>? photos = pro.infoModel?.info.photos;
      return Row(
        children: List.generate(
            pro.infoModel?.info.photos?.length ?? 0,
            (index) => FContainer(
                  width: 72.w,
                  height: 72.w,
                  margin: EdgeInsets.only(right: 10.w),
                  radius: BorderRadius.circular(10.w),
                  color: Colors.black,
                  image: ImageUtils().getImageProvider(
                      NetConstants.ossPath + photos![index].toString()),
                  imageFit: BoxFit.cover,
                )),
      );
    });
  }

  Widget userHead() {
    return Consumer<HomePageUserProvider>(builder: (_, pro, __) {
      String image =
          NetConstants.ossPath + (pro.infoModel?.info.headIcon ?? "");
      return FContainer(
        width: 160.w,
        height: 160.w,
        image: ImageUtils().getImageProvider(image),
        radius: BorderRadius.circular(168.w),
        border: Border.all(color: const Color(0xFF9A43FF), width: 4.w),
      );
    });
  }

  Widget richIcon(LoginUserModel? infoModel) {
    String exp = infoModel?.gradeInfo.userGrade.wealth.badge ?? "";
    int grade = infoModel?.gradeInfo.userGrade.wealth.grade ?? 0;
    return ConfigManager().userWealthCharmIcon(exp, grade, isWealth: true);
  }

  Widget charmIcon(LoginUserModel? infoModel) {
    String exp = infoModel?.gradeInfo.userGrade.charm.badge ?? "";
    int grade = infoModel?.gradeInfo.userGrade.charm.grade ?? 0;
    return ConfigManager().userWealthCharmIcon(exp, grade);
  }

  Widget userAge(int sex, int bir) {
    return FContainer(
      width: 78.w,
      height: 40.w,
      padding: EdgeInsets.symmetric(horizontal: 10.w),
      color: sex == 0 ? const Color(0xFFFFE5E5) : const Color(0xFFE5EFFF),
      radius: BorderRadius.circular(30.w),
      child: Row(children: [
        LoadAssetImage(sex == 0 ? "person/sex2" : "person/sex1",
            width: 20.w, height: 20.w),
        FText("${UserConstants.getUserAge(bir)}",
            size: 24.sp,
            color: sex == 0 ? const Color(0xFFFF617F) : const Color(0xFF43A0FF))
      ]),
    );
  }

  void onTapTag(Function() call) async {
    // await WidgetUtils().pushPage(CreateUser3());
    call();
  }
}
