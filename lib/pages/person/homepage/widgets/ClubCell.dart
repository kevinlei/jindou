import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class ClubCell extends StatelessWidget {
  // final ClubModel? model;
  final PageController homePageCtr;

  // const ClubCell(this.homePageCtr, {Key? key, this.model}) : super(key: key);
  const ClubCell(this.homePageCtr, {Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    Widget club = Row(children: [
      clubCover(),
      SizedBox(width: 20.w),
      FContainer(
        padding: EdgeInsets.only(top: 20.w, bottom: 20.w),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              clubName(),
              SizedBox(height: 5.w),
              // FText("公会ID：${model?.clubInfo.clubDisplayId ?? ""}",
              FText("公会ID：",
                  color: Colors.white.withOpacity(0.8), size: 24.sp),
              SizedBox(height: 5.w),
              Row(
                children: [
                  LoadAssetImage(
                    "club/roomNum",
                    width: 20.w,
                  ),
                  SizedBox(
                    width: 10.w,
                  ),
                  // FText("${model?.clubInfo.memberCount}",
                    FText("",
                      size: 22.sp, color: Colors.white),
                  FContainer(
                    margin: EdgeInsets.only(left: 10.w, right: 10.w),
                    width: 20.w,
                    align: Alignment.center,
                    child: FText("|"),
                  ),
                  LoadAssetImage(
                    "club/roomPersonNum",
                    width: 20.w,
                  ),
                  SizedBox(
                    width: 10.w,
                  ),
                  // FText("${model?.clubInfo.memberCount}人",
                    FText("人",
                      size: 22.sp, color: Colors.white),
                ],
                crossAxisAlignment: CrossAxisAlignment.center,
              )
            ]),
      )
    ]);

    return GestureDetector(
      onTap: onTapClub,
      child: FContainer(
        margin: EdgeInsets.symmetric(horizontal: 25.w),
        height: 200.h,
        radius: BorderRadius.circular(20.w),
        color: Color(0xFF26263B),
        padding: EdgeInsets.symmetric(horizontal: 20.w),
        child: Stack(children: [
          club,
          // Positioned(
          //   left: -20.w,
          //   child: LoadAssetImage("homepage/club", width: 104.w),
          // )
        ], clipBehavior: Clip.none),
      ),
    );
  }

  Widget clubCover() {
    // String cover = model?.clubInfo.clubCover ?? "";
    return FContainer(
      width: 160.w,
      height: 160.w,
      radius: BorderRadius.circular(20.w),
      // image: ImageUtils().getImageProvider(NetConstants.ossPath + cover),
      image:ImageUtils().getImageProvider(NetConstants.ossPath +""),
      imageFit: BoxFit.cover,
    );
  }

  Widget clubName() {
    return ConstrainedBox(
      constraints: BoxConstraints(maxWidth: 360.w),
      child: FText(
        // model?.clubInfo.clubName ?? "",
        "",
        weight: FontWeight.bold,
      ),
    );
  }

  void onTapClub() {
    // if (model == null) return;
    // if (model!.clubInfo.clubStatus == 0)
      return WidgetUtils().showToast("公会仍在审核");
    // WidgetUtils()
    //     .pushPage(ClubInfoPage(homePageCtr, clubId: model!.clubInfo.clubId));
  }
}
