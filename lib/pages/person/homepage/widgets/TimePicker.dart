import 'package:flutter/cupertino.dart';
import 'package:echo/import.dart';

class TimePicker extends StatefulWidget {
  @override
  _TimePickerState createState() => _TimePickerState();
}

class _TimePickerState extends State<TimePicker> {
  int age = 0;
  int maxDays = 31;
  Map<String, int> monthDays = {
    "1": 31,
    "2": 28,
    "3": 31,
    "4": 30,
    "5": 31,
    "6": 30,
    "7": 31,
    "8": 31,
    "9": 30,
    "10": 31,
    "11": 30,
    "12": 31
  };

  int minYear = DateTime.now().year;
  int maxYear = 2099;
  DateTime dateTime = DateTime.now();

  late FixedExtentScrollController yearCtr =
      FixedExtentScrollController(initialItem: maxYear - minYear);
  FixedExtentScrollController monthCtr =
      FixedExtentScrollController(initialItem: 0);
  FixedExtentScrollController dayCtr =
      FixedExtentScrollController(initialItem: 0);

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      padding: EdgeInsets.only(
          top: 10.w, bottom: ScreenUtil().bottomBarHeight + 20.w),
      child: Column(children: [
        FSheetLine(),
        pickerContent(),
        Row(children: [btnCancel(), btnConfirm()])
      ], mainAxisSize: MainAxisSize.min),
    );
  }

  @override
  void dispose() {
    yearCtr.dispose();
    monthCtr.dispose();
    dayCtr.dispose();
    super.dispose();
  }

  Widget pickerContent() {
    Widget content = Row(children: [
      PickerWheel(pickerItem(minYear, maxYear, "年", yearCtr),
          onSelect: onYearChange),
      PickerWheel(pickerItem(1, 12, "月", monthCtr), onSelect: onMonthChange),
      PickerWheel(pickerItem(1, maxDays, "日", dayCtr), onSelect: onDayChange),
    ]);
    return SizedBox(height: 360.h, child: content);
  }

  Widget btnCancel() {
    Widget btn = FContainer(
      height: 90.w,
      color: Color(0xFF1E1F2A),
      margin: EdgeInsets.symmetric(horizontal: 50.w),
      radius: BorderRadius.circular(90.w),
      border: Border.all(color: Color(0xFF999999)),
      align: Alignment.center,
      child: FText("取消",
          size: 32.sp, weight: FontWeight.bold, color: Color(0xFF999999)),
    );
    return Flexible(child: GestureDetector(child: btn, onTap: onCancel));
  }

  Widget btnConfirm() {
    Widget btn = FContainer(
      height: 90.w,
      color: Color(0xFF8F36FF),
      margin: EdgeInsets.symmetric(horizontal: 50.w),
      radius: BorderRadius.circular(90.w),
      border: Border.all(color: Color(0xFF999999)),
      align: Alignment.center,
      child: FText("确认", size: 32.sp, weight: FontWeight.bold),
    );
    return Flexible(child: GestureDetector(child: btn, onTap: onConfirm));
  }

  Widget pickerItem(
      int min, int max, String suffix, FixedExtentScrollController controller) {
    BorderSide borderSide = BorderSide(color: Color(0xFFEDEEF3), width: 2.w);
    return CupertinoPicker.builder(
        childCount: max - min + 1,
        itemExtent: 100.h,
        selectionOverlay:
            FContainer(border: Border(top: borderSide, bottom: borderSide)),
        scrollController: controller,
        onSelectedItemChanged: null,
        itemBuilder: (_, index) => ageItem("${index + min}" + suffix));
  }

  Widget ageItem(String text) {
    return Center(child: FText(text, size: 32.sp));
  }

  void onYearChange() {}

  void onMonthChange() {
    int month = monthCtr.selectedItem + 1;
    int days = monthDays[month.toString()]!;
    if (maxDays != days) {
      maxDays = days;
      dayCtr.jumpTo(0);
    }
  }

  void onDayChange() {}

  void onCancel() {
    WidgetUtils().popPage();
  }

  void onConfirm() {
    int year = minYear, month = 1, day = 1;
    if (yearCtr.hasClients) {
      year = yearCtr.selectedItem + minYear;
    }
    if (monthCtr.hasClients) {
      month = monthCtr.selectedItem + 1;
    }
    if (dayCtr.hasClients) {
      day = dayCtr.selectedItem + 1;
    }
    DateTime time = DateTime(year, month, day);
    WidgetUtils().popPage(time.millisecondsSinceEpoch);
  }
}
