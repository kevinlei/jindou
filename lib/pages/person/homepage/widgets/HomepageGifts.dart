import 'package:flutter/material.dart';
import 'package:echo/import.dart';

import '../../../../widgets/Foindmater.dart';
import '../../../../widgets/myfont_weight.dart';

class HomepageGifts extends StatefulWidget {
  final String userId;
  final String userHeadIcon;
  final String userName;
  const HomepageGifts(this.userId, this.userHeadIcon, this.userName, {Key? key})
      : super(key: key);

  @override
  State<HomepageGifts> createState() => _HomepageGiftsState();
}

class _HomepageGiftsState extends State<HomepageGifts> {
  @override
  String vates = CountLiType.NIMOR.text;
  List<GiftList> memberList = [];
  int total_count = 0;
  int receive_count = 0;
  int receive_value = 0;

  @override
  void initState() {
    super.initState();
    requestSetUser();
  }

  void requestSetUser() {
    int type = 0;
    if (vates == CountLiType.NIMOR.text) {}
    type = 0;
    if (vates == CountLiType.PRICE.text) type = 1;
    if (vates == CountLiType.LISU.text) type = 2;

    DioUtils().asyncHttpRequest(
      NetConstants.giftWall,
      params: {
        "sort_type": type,
        "user_id": widget.userId,
      },
      onSuccess: onSetStatus,
    );
  }

  void onSetStatus(response) {
    if (response == null) return;
    total_count = response['total_count'];
    receive_count = response['receive_count'];
    receive_value = response['receive_value'];
    List comment = response?["gift_list"] ?? [];
    memberList.clear();
    for (var data in comment) {
      GiftList memberModel = GiftList.fromJson(data);
      memberList.add(memberModel);
    }
    if (!mounted) return;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FContainer(
        color: const Color(0xFFFFB396),
        padding: EdgeInsets.only(top: ScreenUtil().statusBarHeight),
        /*image: ImageUtils().getAssetsImage("public/gitfstbg"),
        imageFit: BoxFit.fitWidth,*/
        height: ScreenUtil().screenHeight,
        imageAlign: Alignment.topCenter,
        child: SingleChildScrollView(
          padding: EdgeInsets.zero,
          child: Column(mainAxisSize: MainAxisSize.max, children: [
            CardAppBar(title: "收到的礼物", actions: [
              GestureDetector(
                onTap: onTapSelf,
                child: Row(
                  children: [
                    // LoadAssetImage(
                    //   "person/gitprst",
                    //   width: 30.w,
                    //   height: 30.w,
                    //   color: Colors.black,
                    // ),
                    SizedBox(width: 30.h),
                  ],
                ),
              )
            ]),
            SizedBox(height: 20.h),
            Row(children: [
              SizedBox(width: 32.w),
              userHead(),
              SizedBox(width: 20.w),
              Expanded(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                    FText(
                      widget.userName,
                      size: 28.sp,
                      weight: MyFontWeight.medium,
                      color: Color(0xFF222222),
                    ),
                    FText("价值：${receive_value}",
                        size: 24.sp, color: Color(0xB3222222)),
                  ])),
              Expanded(child: Container()),
              Expanded(
                child: Row(
                  children: [
                    Expanded(child: Container()),
                    FText("${receive_count}",
                        size: 56.sp,
                        align: TextAlign.right,
                        weight: MyFontWeight.medium,
                        color: Color(0xFF222222)),
                    FText(
                      "/",
                      size: 23.sp,
                      align: TextAlign.right,
                      color: Color(0xFF222222),
                      weight: MyFontWeight.medium,
                    ),
                    FText("${total_count}",
                        size: 32.sp,
                        weight: MyFontWeight.medium,
                        align: TextAlign.right,
                        color: Color(0xFF222222)),
                    SizedBox(width: 40.w),
                  ],
                ),
              ),
            ]),
            SizedBox(height: 60.h),
            Row(
              children: [
                SizedBox(width: 32.w),
                Expanded(
                    child: Row(
                  children: [
                    FText(
                      "全部礼物 (${receive_count})",
                      size: 28.sp,
                      align: TextAlign.right,
                      color: Color(0xFF222222),
                      weight: MyFontWeight.medium,
                    ),
                    const Spacer(),
                    KsPopupMenuButton(
                      shape: const TooltipShape(),
                      color: const Color(0xFF252734),
                      offset: Offset(30, 60.h),
                      elevation: 0,
                      itemBuilder: (_) => [
                        popupMenuItem(CountLiType.NIMOR.text, "person/forRuner",
                            onTapBlack),
                        popupMenuItem(CountLiType.PRICE.text, "person/forRuner",
                            onTapLiBlack),
                        popupMenuItem(CountLiType.LISU.text, "person/forRuner",
                            onTapNumBlack),
                      ],
                      child: dressImage(),
                    ),
                  ],
                )),
                SizedBox(width: 20.w),
              ],
            ),
            SizedBox(height: 20.h),
            // Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
            //   SizedBox(width: 30.w),
            //   Expanded(
            //     child: Column(children: [
            //       SizedBox(height: 100.h),
            //       headWidget(2),
            //       nameWidget(2),
            //       valueWidget(2)
            //     ]),
            //   ),
            //   SizedBox(width: 10.w),
            //   Expanded(
            //     child: Column(children: [
            //       headWidget(1),
            //       nameWidget(1),
            //       valueWidget(1),
            //     ]),
            //   ),
            //   SizedBox(width: 10.w),
            //   Expanded(
            //     child: Column(children: [
            //       SizedBox(height: 140.h),
            //       headWidget(3),
            //       nameWidget(3),
            //       valueWidget(3)
            //     ]),
            //   ),
            //   SizedBox(width: 30.w),
            // ]),
            GridView.builder(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              padding: EdgeInsets.symmetric(horizontal: 32.w),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 4,
                mainAxisExtent: 214.h,
                crossAxisSpacing: 10.w,
                mainAxisSpacing: 10.h,
              ),
              // itemCount: giftList.length > 3 ? (giftList.length - 3) : 0,
              itemCount: memberList.length,
              // itemBuilder: (_, i) => giftItem(giftList[i + 3]),
              itemBuilder: (_, i) => giftItem(i),
            ),
          ]),
        ),
      ),
    );
  }

  KsPopupMenuItem popupMenuItem(String t, String r, Function() tap) {
    return KsPopupMenuItem(
        onTap: tap,
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(width: 15.w),
              Container(
                child: FText(t,
                    color: const Color(0xFFFFFFFF),
                    size: 26.sp,
                    align: TextAlign.center),
              ),
              Visibility(
                visible: vates == t,
                child: SizedBox(width: 5.w),
              ),
              Visibility(
                maintainAnimation: true,
                maintainSize: true,
                maintainState: true,
                visible: vates == t,
                child: LoadAssetImage(r, width: 24.w),
              ),
              SizedBox(width: 15.w),
            ],
          ),
        ));
  }

  void onTapBlack() {
    vates = CountLiType.NIMOR.text;
    setState(() {});
    requestSetUser();
  }

  void onTapLiBlack() {
    vates = CountLiType.PRICE.text;
    setState(() {});
    requestSetUser();
  }

  void onTapNumBlack() {
    vates = CountLiType.LISU.text;
    setState(() {});
    requestSetUser();
  }

  void onTapSelf() {}

  Widget dressImage() {
    return FContainer(
      height: 30.h,
      margin: EdgeInsets.symmetric(horizontal: 10.w),
      radius: BorderRadius.circular(30.w),
      align: Alignment.center,
      child: Row(children: [
        FText(vates, size: 24.sp, color: Colors.black),
        SizedBox(width: 5.w),
        LoadAssetImage("person/xiaPrice", width: 25.w),
      ]),
    );
  }

  Widget userHead() {
    String avatar = widget.userHeadIcon;
    return FContainer(
      width: 96.w,
      height: 96.w,
      image: avatar.isEmpty
          ? null
          : ImageUtils().getImageProvider(NetConstants.ossPath + avatar),
      radius: BorderRadius.circular(168.w),
      imageFit: BoxFit.fill,
      border: Border.all(
          color: const Color(0xFFA89CC5).withOpacity(0.2), width: 4.w),
    );
  }

  //  Widget giftItem(ConfigGift gift) {
  Widget giftItem(int i) {
    return FContainer(
      color: const Color(0xCCFFFFFF),
      radius: BorderRadius.circular(20.w),
      child: Column(children: [
        Expanded(
          child: AspectRatio(
            aspectRatio: 1,
            // child: LoadNetImage(NetConstants.ossPath + gift.picture),
            child: LoadNetImage(
                NetConstants.ossPath + memberList[i].giftPicture.toString(),
                fit: BoxFit.fill),
          ),
        ),
        FContainer(
          height: 40.h,
          radius: BorderRadius.only(
            bottomLeft: Radius.circular(20.w),
            bottomRight: Radius.circular(20.w),
          ),
          color: Colors.transparent,
          align: Alignment.center,
          // child: FText(gift.name,
          //     size: 20.sp, color: Colors.white.withOpacity(0.8)),
          child: FText(memberList[i].giftName.toString(),
              size: 20.sp, color: Color(0xFF222222)),
        ),
        FText(
          'X${memberList[i].giftNum}',
          size: 20.sp,
          color: Color(0xFF222222),
          weight: MyFontWeight.bold,
        ),
        SizedBox(height: 10.h),
      ]),
    );
  }
}

class TooltipShape extends ShapeBorder {
  const TooltipShape();

  final BorderSide _side = BorderSide.none;
  final BorderRadiusGeometry _borderRadius = BorderRadius.zero;

  @override
  EdgeInsetsGeometry get dimensions => EdgeInsets.all(_side.width);

  @override
  Path getInnerPath(
    Rect rect, {
    TextDirection? textDirection,
  }) {
    final Path path = Path();

    path.addRRect(
      _borderRadius.resolve(textDirection).toRRect(rect).deflate(_side.width),
    );

    return path;
  }

  @override
  Path getOuterPath(Rect rect, {TextDirection? textDirection}) {
    final Path path = Path();
    final RRect rrect = _borderRadius.resolve(textDirection).toRRect(rect);

    path.moveTo(0, 10);
    path.quadraticBezierTo(0, 0, 10, 0);
    path.lineTo(rrect.width - 30, 0);
    path.lineTo(rrect.width - 20, -10);
    path.lineTo(rrect.width - 10, 0);
    path.quadraticBezierTo(rrect.width, 0, rrect.width, 10);
    path.lineTo(rrect.width, rrect.height - 10);
    path.quadraticBezierTo(
        rrect.width, rrect.height, rrect.width - 10, rrect.height);
    path.lineTo(10, rrect.height);
    path.quadraticBezierTo(0, rrect.height, 0, rrect.height - 10);

    return path;
  }

  @override
  void paint(Canvas canvas, Rect rect, {TextDirection? textDirection}) {}

  @override
  ShapeBorder scale(double t) => RoundedRectangleBorder(
        side: _side.scale(t),
        borderRadius: _borderRadius * t,
      );
}
