import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import '../../dynamic/model/dynamic_liemodel.dart';
import 'DynamicContentList.dart';

class HomePageList extends StatelessWidget {
  HomePageList({Key? key}) : super(key: key);

  bool isSelf = false;

  @override
  Widget build(BuildContext context) {
    HomePageProvider provider = context.watch<HomePageProvider>();
    isSelf = provider.userId == UserConstants.userginInfo?.info.userId;
    return userInfoList(context, provider);
  }

  Widget userInfoList(BuildContext context, HomePageProvider provider) {
    return SliverToBoxAdapter(
      child: Column(children: [
        FContainer(
          height: 400.h,
          padding: EdgeInsets.symmetric(horizontal: 32.w, vertical: 32.h),
          margin: EdgeInsets.fromLTRB(32.w, 20.h, 32.w, 20.h),
          color: Colors.white,
          radius: BorderRadius.circular(20.w),
          align: Alignment.centerLeft,
          child: Consumer<HomePageUserProvider>(builder: (_, pro, __) {
            return Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  FText(
                    "个人资料",
                    size: 32.sp,
                    weight: FontWeight.w600,
                    color: Colors.black,
                  ),
                  const Spacer(),
                  FText(
                    "年龄：${UserConstants.getUserAge(pro.infoModel?.info.birthday ?? 0)}",
                    size: 24.sp,
                    color: Colors.black,
                  ),
                  SizedBox(height: 20.h),
                  FText(
                    "星座：${UserConstants.getZodiacName(pro.infoModel?.info.birthday ?? 0)}",
                    size: 24.sp,
                    color: Colors.black,
                  ),
                  SizedBox(height: 20.h),
                  FText(
                    "家乡：${pro.infoModel?.info.city ?? ""}",
                    size: 24.sp,
                    color: Colors.black,
                  ),
                  SizedBox(height: 20.h),
                  FText(
                    "职业：${pro.infoModel?.info.job ?? ""}",
                    size: 24.sp,
                    color: Colors.black,
                  ),
                  const Spacer(),

                  const Divider(color: Color(0xFF14222222)),
                  // FContainer(
                  //   height: 150.w,
                  //   child: ListView.builder(
                  //       padding: EdgeInsets.only(top: 30.w),
                  //       physics: ScrollPhysics(),
                  //       shrinkWrap: true,
                  //       itemCount: 1,
                  //       itemBuilder: (_, index) => Wrap(
                  //             spacing: 30.w,
                  //             runSpacing: 10.w,
                  //             children: List.generate(userTags.length,
                  //                 (index) => tagCell(userTags[index])),
                  //           )),
                  // )
                ]);
          }),
        ),

        Consumer<HomePageUserProvider>(builder: (_, pro, __) {
          return GestureDetector(
            onTap: () {
              if (provider.dynamicList.isEmpty) return;
              WidgetUtils().pushPage(DynamicContentList(provider.userId),
                  fun: () {
                provider.requestDynamic(true);
              });
            },
            child: FContainer(
              height: 272.h,
              padding: EdgeInsets.symmetric(horizontal: 32.w, vertical: 32.h),
              margin: EdgeInsets.fromLTRB(32.w, 0, 32.w, 20.h),
              color: const Color(0xFF8C73C2).withOpacity(0.1),
              radius: BorderRadius.circular(20.w),
              align: Alignment.centerLeft,
              child: Column(children: [
                Row(children: [
                  FText(
                    "动态",
                    size: 32.sp,
                    weight: FontWeight.w600,
                    color: Colors.black,
                  ),
                  FText("（${pro.infoModel?.dynamicInfo?.count}）",
                      size: 24.sp, color: const Color(0xFFCBCAD2)),
                  const Spacer(),
                  buttonMore(),
                ]),
                const Spacer(),
                pro.infoModel?.dynamicInfo?.count == 0
                    ? emptyGiftWidgetdt()
                    : GridView.builder(
                        padding: EdgeInsets.zero,
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 4,
                          mainAxisExtent: 150.h,
                          crossAxisSpacing: 20.w,
                        ),
                        itemCount: provider.dynamicList.length > 4
                            ? 4
                            : provider.dynamicList.length,
                        itemBuilder: (_, i) =>
                            dynamicItem(provider.dynamicList[i]),
                      ),
                const Spacer(),
              ]),
            ),
          );
        }),
        if(false)
        Consumer<HomePageUserProvider>(builder: (_, pro, __) {
          List<GiftList> giftList = [];
          giftList = pro.infoModel?.giftInfo?.giftList ?? [];
          return GestureDetector(
            onTap: () => onTapGift(context, giftList),
            child: FContainer(
              height: 292.h,
              padding: EdgeInsets.symmetric(horizontal: 32.w, vertical: 32.h),
              margin: EdgeInsets.fromLTRB(32.w, 0, 32.w, 20.h),
              color: const Color(0xFF8C73C2).withOpacity(0.1),
              radius: BorderRadius.circular(20.w),
              align: Alignment.centerLeft,
              child: Column(children: [
                Row(children: [
                  FText(
                    "收到的礼物",
                    size: 32.sp,
                    weight: FontWeight.w600,
                    color: Colors.black,
                  ),
                  FText("（${pro.infoModel?.giftInfo?.giftCount}款）",
                      size: 24.sp, color: const Color(0xFFCBCAD2)),
                  const Spacer(),
                  buttonMore(),
                ]),
                const Spacer(),
                pro.infoModel?.giftInfo?.giftList?.length == 0
                    ? emptyGiftWidget("礼物", () {})
                    : GridView.builder(
                        padding: EdgeInsets.zero,
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 4,
                          mainAxisExtent: 160.h,
                          crossAxisSpacing: 20.w,
                        ),
                        itemCount: giftList.length > 4 ? 4 : giftList.length,
                        itemBuilder: (_, i) => giftItem(giftList[i]),
                      ),
                const Spacer(),
              ]),
            ),
          );
        }),

        // FContainer(
        //   height: 292.h,
        //   padding: EdgeInsets.symmetric(horizontal: 32.w, vertical: 32.h),
        //   margin: EdgeInsets.fromLTRB(32.w, 0, 32.w, 40.h),
        //   color: Color(0xFF8C73C2).withOpacity(0.1),
        //   radius: BorderRadius.circular(20.w),
        //   align: Alignment.centerLeft,
        //   child: Consumer<HomePageUserProvider>(builder: (_, pro, __) {
        //     List<ExtUserAvatar> avatars = pro.infoModel?.avatars ?? [];
        //     return Column(children: [
        //       Row(children: [
        //         FText("个性装扮", size: 32.sp, weight: FontWeight.w600),
        //         const Spacer(),
        //         buttonMore(),
        //       ]),
        //       const Spacer(),
        //       avatars.length == 0
        //           ? emptyGiftWidget("装扮", () {
        //               WidgetUtils().pushPage(DressStore());
        //             })
        //           : GridView.builder(
        //               padding: EdgeInsets.zero,
        //               shrinkWrap: true,
        //               physics: NeverScrollableScrollPhysics(),
        //               gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        //                 crossAxisCount: 4,
        //                 mainAxisExtent: 160.h,
        //                 crossAxisSpacing: 20.w,
        //               ),
        //               itemCount: avatars.length > 4 ? 4 : avatars.length,
        //               itemBuilder: (_, i) => avatarItem(avatars[i]),
        //             ),
        //       const Spacer(),
        //     ]);
        //   }),
        // ),
      ]),
    );
  }

  Widget emptyGiftWidget(String title, final GestureTapCallback? onTap) {
    return GestureDetector(
      onTap: onTap,
      child: FContainer(
        child: Column(
          children: [
            FText(
              isSelf ? "您目前还未收到$title" : "该用户目前没有$title",
              color: Colors.black.withOpacity(0.6),
              size: 24.sp,
            ),
            // Visibility(
            //   visible: !isSelf || title == "装扮",
            //   child: FContainer(
            //     gradient: const [
            //       Color(0xFF46F5F6),
            //       Color(0xFFFFA7FB),
            //       Color(0xFFFF82B2)
            //     ],
            //     gradientBegin: Alignment.topLeft,
            //     gradientEnd: Alignment.bottomRight,
            //     radius: BorderRadius.circular(56.w),
            //     width: 180.w,
            //     margin: EdgeInsets.symmetric(horizontal: 25.w, vertical: 20.h),
            //     padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 10.w),
            //     align: Alignment.center,
            //     child: FText(
            //       isSelf ? "去购买" : "送Ta$title",
            //       size: 28.sp,
            //       color: Colors.white,
            //     ),
            //   ),
            // )
          ],
        ),
      ),
    );
  }

  Widget emptyGiftWidgetdt() {
    return GestureDetector(
      onTap: () => isSelf ? WidgetUtils().pushPage(const PublishPage()):{},
      child: FContainer(
        child: Column(
          children: [
            FText(
              isSelf ? "还没有动态哟～" : "该用户还没有发布过动态唷～",
              size: 24.sp,
              color: Colors.black.withOpacity(0.6),
            ),
            Visibility(
                visible: isSelf,
                child: FContainer(
                  gradientBegin: Alignment.topLeft,
                  gradientEnd: Alignment.bottomRight,
                  gradient: const [
                    Color(0xFFFF753F),
                    Color(0xFFFF753F),
                    Color(0xFFFF753F)
                  ],
                  radius: BorderRadius.circular(56.w),
                  width: 180.w,
                  margin:
                      EdgeInsets.symmetric(horizontal: 25.w, vertical: 20.h),
                  padding:
                      EdgeInsets.symmetric(horizontal: 10.w, vertical: 10.w),
                  align: Alignment.center,
                  child: FText(
                    "去发布",
                    size: 28.sp,
                  ),
                ))
          ],
        ),
      ),
    );
  }

  Widget tagCell(String text) {
    return FContainer(
      height: 48.w,
      radius: BorderRadius.circular(40.w),
      padding: EdgeInsets.symmetric(horizontal: 10.w),
      constraints: BoxConstraints(minWidth: 100.w),
      color: Colors.white,
      child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: [FText(text, size: 24.sp)]),
    );
  }

  Widget buttonMore() {
    return Row(
      children: [
        FText(
          "查看更多",
          size: 28.sp,
          color: Colors.black.withOpacity(0.6),
        ),
        LoadAssetImage(
          "person/expans",
          width: 25.w,
        ),
      ],
    );
  }

  Widget dynamicItem(DynamicLiemodel data) {
    if (data.contentType == 1) {
      return RepaintBoundary(
        child: Container(
          width: 150.h,
          height: 150.h,
          alignment: Alignment.center,
          decoration: BoxDecoration(
              color: const Color(0xFFFF753F).withOpacity(0.5),
              border: Border.all(
                  width: 1, color: const Color(0xFFFF753F).withOpacity(0.5)),
              borderRadius: const BorderRadius.all(Radius.circular(5))),
          child: Text(
            textAlign: TextAlign.center,
            data.contentText ?? " ",
            textDirection: TextDirection.ltr,
            style: const TextStyle(color: Colors.white),
          ),
        ),
      );
    }
    return FContainer(
      color: Colors.white,
      height: 150.h,
      radius: BorderRadius.circular(20.w),
      child: HomeDynamicImg(
          data.resourceUrl == null ? "" : data.resourceUrl.join(",")),
    );
  }

  Widget giftItem(GiftList gift) {
    return FContainer(
      color: const Color(0xFFFF753F).withOpacity(0.1),
      radius: BorderRadius.circular(20.w),
      child: Column(children: [
        Expanded(
          child: AspectRatio(
            aspectRatio: 1,
            child: LoadNetImage(
                NetConstants.ossPath + gift.giftPicture.toString()),
          ),
        ),
        FContainer(
          height: 40.h,
          radius: BorderRadius.only(
            bottomLeft: Radius.circular(20.w),
            bottomRight: Radius.circular(20.w),
          ),
          color: const Color(0xFFFF753F).withOpacity(0.5),
          align: Alignment.center,
          child: FText('x ${gift.giftNum.toString()}',
              size: 20.sp, color: Colors.white.withOpacity(0.8)),
        ),
      ]),
    );
  }

  // Widget avatarItem(ExtUserAvatar avatar) {
  //   ConfigGoods? goods = ConfigManager().getGoods(avatar.itemId);
  //   return FContainer(
  //     color: const Color(0xFF2A243D).withOpacity(0.8),
  //     radius: BorderRadius.circular(20.w),
  //     child: Column(children: [
  //       Expanded(
  //         child: AspectRatio(
  //           aspectRatio: 1,
  //           child: goods == null
  //               ? null
  //               : LoadNetImage(NetConstants.ossPath + goods.picture),
  //         ),
  //       ),
  //       FContainer(
  //         height: 40.h,
  //         radius: BorderRadius.only(
  //           bottomLeft: Radius.circular(20.w),
  //           bottomRight: Radius.circular(20.w),
  //         ),
  //         color: const Color(0xFF342D4D),
  //         align: Alignment.center,
  //         child: FText(goods?.title ?? "",
  //             size: 20.sp, color: Colors.white.withOpacity(0.8)),
  //       ),
  //     ]),
  //   );
  // }

  Widget giftsContent(List<ConfigGift> gifts) {
    int length = gifts.length < 3 ? 0 : (gifts.length - 3);
    return SliverPadding(
      padding: EdgeInsets.symmetric(horizontal: 25.w),
      sliver: SliverGrid(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 4,
          mainAxisExtent: 200.w,
          mainAxisSpacing: 20.w,
          crossAxisSpacing: 20.w,
        ),
        delegate: SliverChildBuilderDelegate(
          (_, index) => giftCell(gifts[index + 3]),
          childCount: length,
        ),
      ),
    );
  }

  Widget giftCell(ConfigGift gift) {
    BoxShadow boxShadow = BoxShadow(
      color: const Color(0xFF000000).withOpacity(0.05),
      spreadRadius: 2,
      blurRadius: 5,
    );

    return FContainer(
      color: Colors.white,
      radius: BorderRadius.circular(12.w),
      shadow: [boxShadow],
      padding: EdgeInsets.symmetric(vertical: 20.w),
      child: Column(children: [
        Expanded(
          child: AspectRatio(
            aspectRatio: 1.0,
            child: LoadNetImage(NetConstants.ossPath + gift.picture,
                fit: BoxFit.cover),
          ),
        ),
        SizedBox(height: 10.w),
        FText(gift.name + "x${gift.count}",
            overflow: TextOverflow.ellipsis, size: 20.sp),
      ]),
    );
  }

  void onTapGift(BuildContext context, giftList) {
    LoginUserModel? infoModel =
        Provider.of<HomePageUserProvider>(context, listen: false).infoModel;
    WidgetUtils().pushPage(HomepageGifts(
      infoModel!.info.userId,
      infoModel.info.headIcon,
      infoModel.info.name,
    ));
  }
}
