import 'package:flutter/material.dart';
import 'package:echo/import.dart';

import '../../dynamic/model/dynamic_liemodel.dart';

class HomePageProvider extends ChangeNotifier {
  String userId = "";

  bool showOnline = true;
  bool showRelation = true;
  bool showGiftWall = true;

  List<ConfigGift> giftList = [];
  List<DynamicLiemodel> dynamicList = [];
  List<DynamicLiemodel> dynamicListPho = [];

  List<String> unlikeList = [];

  // "ShowOnlineStatus":true,"ShowRelation":true,"ShowGiftWall":true
  Future<void> requestUserSet(String userIds) async {
    userId = userIds;
    if (userId == UserConstants.userginInfo?.info.userId) return;
    // await DioUtils().httpRequest(
    //   NetConstants.getUserSet + userId,
    //   onSuccess: (response) {
    //     showOnline = response?["showOnlineStatus"] ?? true;
    //     showRelation = response?["showRelation"] ?? true;
    //     showGiftWall = response?["showGiftWall"] ?? true;
    //   },
    // );
  }

  Future<void> decodeUnLike() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String list = preferences.getString(SharedKey.KEY_NO_LIKE.value) ?? "";
    unlikeList = list.split("&");
  }

  void requestDynamic(bool isRefresh) {
    if (userId.isEmpty) return;
    DioUtils().asyncHttpRequest(
      NetConstants.userDynamic,
      method: DioMethod.POST,
      loading: false,
      params: {"page": 1, "user_id": userId, "page_size": 20},
      onSuccess: onRefresh,
    );
  }

  void onRefresh(response) {
    dynamicList.clear();
    dynamicListPho.clear();
    response ??= [];
    for (var data in response) {
      DynamicLiemodel model = DynamicLiemodel.fromJson(data);
      if (!unlikeList.contains("${model.id}")) {
        dynamicList.add(model);
        if (model.resourceUrl != '') dynamicListPho.add(model);
      }
    }
    notifyListeners();
  }

  void updateDynamic(DynamicLiemodel model) {
    print('111111111');
    print(dynamicList);
    int index = dynamicList.indexWhere((element) => element.id == model.id);
    dynamicList[index] = model;
    notifyListeners();
  }

  void onDelDynamic(DynamicLiemodel model) {
    dynamicList.removeWhere((element) => element.id == model.id);
    notifyListeners();
  }

  void onHidden(DynamicLiemodel model) async {
    dynamicList.removeWhere((element) => element.id == model.id);
    notifyListeners();
    SharedPreferences preferences = await SharedPreferences.getInstance();
    unlikeList.add("${model.id}");
    preferences.setString(SharedKey.KEY_NO_LIKE.value, unlikeList.join("&"));
  }

  void update() => notifyListeners();
}
