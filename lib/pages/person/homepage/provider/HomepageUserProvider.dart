import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import '../../club/model/club_roomwho_mdoe.dart';

class HomePageUserProvider extends ChangeNotifier {
  String userId = "";
  LoginUserModel? infoModel;
  ClubRoomwhoMdoe? clubModel;
  //个人主页礼物列表
  List<GiftList> giftList = [];
  // 关注
  int countFollow = 0;
  // 粉丝
  int countFans = 0;
  // 足迹
  int countFriend = 0;
  // 访客
  int countVisit = 0;

  bool hasFollow = false;

  RoomshomeModel? inRoomInfo;

  // "IsFriend":false,"IFollowThey":false,"TheyFollowMe":false,"EachBlack":false,"IBlackThey":false,"TheyBlackMe":false
  Future<void> requestRelation() async {
    if (this.userId == UserConstants.userginInfo?.info.userId) return;
    await DioUtils().httpRequest(
      NetConstants.relahomeship,
      method: DioMethod.GET,
      loading: false,
      params: {"user_id": userId},
      onSuccess: (response) {
        if (response == 10 || response == 20) {
          this.hasFollow = true;
        }
        notifyListeners();
      },
    );
  }

  void requestFollow(bool _hasFollow) {
    DioUtils().asyncHttpRequest(
      _hasFollow ? NetConstants.unFollow : NetConstants.followUser,
      method: DioMethod.POST,
      loading: false,
      params: {
        "flow_user_id": this.userId,
        "flow_action": _hasFollow ? 20 : 10
      },
      onSuccess: (_) {
        hasFollow = !_hasFollow;
        notifyListeners();
      },
    );
  }

  void requestUserInfo(String userId) {
    this.userId = userId;
    DioUtils().asyncHttpRequest(
      NetConstants.gethomeUserInfo,
      loading: false,
      method: DioMethod.GET,
      params: {"user_id": userId},
      onSuccess: onGetInfo,
    );
  }

  void requestInRoom() {
    if (userId == UserConstants.userginInfo?.info.userId) return;
    DioUtils().asyncHttpRequest(
      NetConstants.inRoom,
      method: DioMethod.POST,
      loading: false,
      params: {"user_id": userId},
      onSuccess: onInRoom,
    );
  }

  void onInRoom(response) {
    if (response == null) return;
    inRoomInfo = RoomshomeModel.fromJson(response);

    notifyListeners();
  }

  void requestRelationCount() {
    DioUtils().asyncHttpRequest(
      NetConstants.getRelationCount,
      loading: false,
      params: {"userId": this.userId},
      onSuccess: (response) => updateRelationCount(
          response["follows"] ?? 0,
          response["fans"] ?? 0,
          response["friend"] ?? 0,
          response["visitor"] ?? 0),
    );
  }

  void updateRelationCount(int a, int b, int c, int d) {
    countFollow = a;
    countFans = b;
    countFriend = c;
    countVisit = d;
    notifyListeners();
  }

  void onGetInfo(response) {
    if (response == null) return;
    infoModel = LoginUserModel.fromJson(response);
    giftList = infoModel?.giftInfo?.giftList ?? [];

    notifyListeners();
  }

  void requestUserClub(String userId) {
    this.userId = userId;
    if (this.userId.isEmpty) return;
    DioUtils().asyncHttpRequest(
      NetConstants.userClub,
      method: DioMethod.POST,
      loading: false,
      params: {"userId": userId},
      onSuccess: onSelfClub,
    );
  }

  void onSelfClub(response) {
    if (response == null) return;
    try {
      clubModel = ClubRoomwhoMdoe.fromJson(response);
      notifyListeners();
    } catch (e) {}
  }

  void update() => notifyListeners();
}
