import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class HomepagePhotoProvider extends ChangeNotifier {
  String userId = "";
  List photoList = [];
  String audioPath = "";

  void requestPhotos(String userId) {
    this.userId = userId;
    DioUtils().asyncHttpRequest(
      "${NetConstants.photoWall}/${this.userId}",
      onSuccess: onGetPhotos,
    );
  }

  void onGetPhotos(response) {
    response ??= [];
    photoList.clear();
    for (var url in response) {
      if (url["url"].startsWith(UploadType.DYNAMIC_SQUARE.value)) {
        photoList.add(url["url"]);
      }
    }
    notifyListeners();
  }
}
