import 'dart:io';

import 'package:flutter/material.dart';
import 'package:echo/import.dart';

// 公会签约
class ClubSign extends StatefulWidget {
  final ClubModel model;

  const ClubSign({Key? key, required this.model}) : super(key: key);

  @override
  _ClubSignState createState() => _ClubSignState();
}

class _ClubSignState extends State<ClubSign> {
  late ClubModel model;
  int signCount = 1;

  bool accept = false;

  @override
  void initState() {
    super.initState();
    model = widget.model;
    requestSignCount();
    requestClubInfo();
  }

  @override
  Widget build(BuildContext context) {
    String statusText = "", timeText = "";
    bool showTime = true;
    int userStatus = model.userStatus;
    if (userStatus == InClubStatus.IN_CLUB.index) {
      statusText = "未签约";
      showTime = false;
    } else if (userStatus == InClubStatus.SIGN_APPLY.index) {
      statusText = "签约申请中";
      String start = DateTime.fromMillisecondsSinceEpoch(model.signStart)
          .toString()
          .substring(0, 10);
      String end = DateTime.fromMillisecondsSinceEpoch(model.signTime)
          .toString()
          .substring(0, 10);
      timeText = "$start-$end";
    } else if (userStatus == InClubStatus.SIGNED.index) {
      statusText = "已签约";
      String start = DateTime.fromMillisecondsSinceEpoch(model.signStart)
          .toString()
          .substring(0, 10);
      String end = DateTime.fromMillisecondsSinceEpoch(model.signTime)
          .toString()
          .substring(0, 10);
      timeText = "$start-$end";
    } else if (userStatus == InClubStatus.DIS_APPLY.index) {
      statusText = "解约申请中";
      String start = DateTime.fromMillisecondsSinceEpoch(model.signStart)
          .toString()
          .substring(0, 10);
      String end = DateTime.fromMillisecondsSinceEpoch(model.signTime)
          .toString()
          .substring(0, 10);
      timeText = "$start-$end";
    } else if (userStatus == InClubStatus.QUIT_APPLY.index) {
      statusText = "退出申请中";
      String start = DateTime.fromMillisecondsSinceEpoch(model.signStart)
          .toString()
          .substring(0, 10);
      String end = DateTime.fromMillisecondsSinceEpoch(model.signTime)
          .toString()
          .substring(0, 10);
      timeText = "$start-$end";
    }

    return Scaffold(
      appBar: CommonAppBar(
          title: "公会签约",
          textColor: Colors.white,
          elevation: 1,
          backgroundColor: Color(0xFF1F2234),
      ),
      backgroundColor: Color(0xFF1F2234),
      body: Column(children: [
        SizedBox(height: 40.h),
        buttonList("公会名称", model.clubInfo.clubName),
        buttonList("已签约人数", "$signCount人"),
        buttonList("我的状态", statusText),
        Visibility(child: buttonList("签约时间", timeText), visible: showTime),
      ]),
      bottomNavigationBar:Padding(padding: EdgeInsets.only( bottom: Platform.isIOS? ScreenUtil().bottomBarHeight : 0),child:buttonBottom(),),
    );
  }

  Widget buttonList(String title, String suffix) {
    return FContainer(
      height: 96.w,
      radius: BorderRadius.circular(20.w),
      color: Color(0xFF2C2F40),
      margin: EdgeInsets.fromLTRB(25.w, 0, 25.w, 20.h),
      padding: EdgeInsets.symmetric(horizontal: 25.w),
      child: Row(children: [
        FText(title, size: 32.sp),
        Spacer(),
        FText(suffix, size: 28.sp),
      ]),
    );
  }

  Widget buttonBottom() {
    int clubRole = model.clubRole;
    int userStatus = model.userStatus;
    if (userStatus == InClubStatus.IN_CLUB.index) {
      return Column(children: [
        FProAccept([ProtocolType.PRO_CLUB_SIGN], onAccept: onAccept),
        Row(children: [
          Flexible(child: FLoginButton("申请签约",isActivate: true, radius: 90.w, onPress: requestSign)),
          Flexible(child: FLoginButton("申请退出",isActivate: true, radius: 90.w, onPress: requestQuit)),
        ])
      ], mainAxisSize: MainAxisSize.min);
    } else if (userStatus == InClubStatus.SIGN_APPLY.index) {
      return FLoginButton("申请签约中",isActivate: true, radius: 90.w);
    } else if (userStatus == InClubStatus.SIGNED.index &&
        clubRole != ClubRole.OWNER.index) {
      return FLoginButton("申请解约",isActivate: true, radius: 90.w, onPress: requestUnSign);
    } else if (userStatus == InClubStatus.DIS_APPLY.index) {
      return FLoginButton("申请解约中",isActivate: true, radius: 90.w);
    } else if (userStatus == InClubStatus.QUIT_APPLY.index) {
      return FLoginButton("申请退出中",isActivate: true, radius: 90.w);
    }

    return SizedBox();
  }

  void onAccept(bool value) => accept = value;

  void requestSignCount() {
    DioUtils().asyncHttpRequest(
      NetConstants.signCount,
      params: {"guildId": model.clubInfo.clubId},
      onSuccess: onSignCount,
    );
  }

  void onSignCount(response) {
    signCount = response ?? 1;
    if (!mounted) return;
    setState(() {});
  }

  // 申请签约
  void requestSign() async {
    if (!accept) return WidgetUtils().showToast("请先阅读并同意协议");
    int? time = await WidgetUtils().showFDialog(SignTime());
    if (time == null || time == 0) return;
    DioUtils().asyncHttpRequest(
      NetConstants.applySign,
      method: DioMethod.POST,
      params: {"type": time},
      onSuccess: onApplySign,
    );
  }

  // 申请解约
  void requestUnSign() {
    DioUtils().asyncHttpRequest(
      NetConstants.applyUnSign,
      method: DioMethod.POST,
      onSuccess: onApplyUnSign,
    );
  }

  // 申请退出
  void requestQuit() {
    if (!accept) return WidgetUtils().showToast("请先阅读并同意协议");
    DioUtils().asyncHttpRequest(
      NetConstants.applyQuit,
      method: DioMethod.POST,
      onSuccess: onApplyQuit,
    );
  }

  void onApplySign(response) {
    model.userStatus = InClubStatus.SIGN_APPLY.index;
    if (!mounted) return;
    setState(() {});
    requestClubInfo();
  }

  void onApplyUnSign(response) {
    model.userStatus = InClubStatus.DIS_APPLY.index;
    if (!mounted) return;
    setState(() {});
  }

  void onApplyQuit(response) {
    model.userStatus = InClubStatus.QUIT_APPLY.index;
    if (!mounted) return;
    setState(() {});
  }

  void requestClubInfo() async {
    await DioUtils().httpRequest(
      NetConstants.clubInfo,
      method: DioMethod.POST,
      params: {"guild_id":int.parse(model.clubInfo.clubId),"page":1,"page_size":20},
      onSuccess: onClubInfo,
    );
  }

  void onClubInfo(response) {
    if (response == null) return;
    model = ClubModel.fromJson(response);
    if (!mounted) return;
    setState(() {});
  }
}
