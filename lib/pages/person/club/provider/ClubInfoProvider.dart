import 'package:flutter/material.dart';
import 'package:echo/import.dart';

import '../model/club_mspostmdoel.dart';
import '../model/club_roomst_model.dart';

class ClubInfoProvider extends ChangeNotifier {
  ClubModel? selfClub;

  ClubModel? model;
  //获取用户的公会
  ClubRoomstModel? selfClubls;
//  公会详情
  ClubRoomstModel? modeldel;
//房间详情
  ClubMspostmdoel? postmdoel;

  GuildDataFw? guildDataFw;

  bool hasNewMsg = false;
  bool accept = false;

  void onAccept(bool value) => accept = value;

  // 请求工会首页数据
  Future<void> requestGuildDataFW() async {
    if (modeldel == null) return;
    // String guildId = model!.clubInfo.clubId.toString();
    // if (guildId.isEmpty) return;
    // 角色不对 不请求
    int clubRole = int.parse(modeldel?.memberType.toString() ?? "");
    if (clubRole != ClubRole.OWNER.index) return;

    // await DioUtils().httpRequest(
    //   NetConstants.getGuildDataFW,
    //   params: {"guildId": guildId},
    //   onSuccess: onGuildDataFW,
    // );
  }

  void onGuildDataFW(response) {
    if (response == null) {
      return;
    }
    try {
      guildDataFw = guildDataFwFromJson(response);
    } catch (err) {
      debugPrint('角色不对错误$err');
    }
    notifyListeners();
  }

  Future<void> requestClubInfo(String clubId) async {
    if (clubId.isEmpty) return;
    accept = false;
    await DioUtils().httpRequest(
      NetConstants.clubInfo,
      params: {"guild_id": int.parse(clubId), "page": 1, "page_size": 20},
      method: DioMethod.POST,
      onSuccess: onClubInfo,
    );
  }

  void onClubInfo(response) {
    if (response == null) return;
    modeldel = ClubRoomstModel.fromJson(response);
    notifyListeners();
    // getClubMsg();
  }

  Future<void> requestSelfClub() async {
    String userId = UserConstants.userginInfo?.info.userId ?? "";
    await DioUtils().httpRequest(
      NetConstants.userClub,
      method: DioMethod.POST,
      params: {"userId": userId},
      onSuccess: onSelfClub,
    );
  }

  void onSelfClub(response) {
    if (response == null) {
      selfClubls = null;
      return;
    }
    try {
      selfClubls = ClubRoomstModel.fromJson(response);
    } catch (err) {}
  }

//房间详情
  Future<void> requestFangClub(String room_id, String guild_id) async {
    await DioUtils().httpRequest(
      NetConstants.userdetails,
      method: DioMethod.POST,
      params: {
        "guild_id": int.parse(guild_id),
        "ex_room_id": int.parse(room_id)
      },
      onSuccess: onSelfflitClub,
    );
  }

  void onSelfflitClub(response) {
    if (response == null) {
      postmdoel = null;
      return;
    }
    try {
      postmdoel = ClubMspostmdoel.fromJson(response);
    } catch (err) {}

    notifyListeners();
  }

  // // 是否有新消息
  // void getClubMsg() {
  //   if (model == null) return;
  //   String clubId = model!.clubInfo.clubId.toString();
  //   int clubRole = model!.clubRole;
  //   // if (clubRole != ClubRole.OWNER.index) return;
  //   DioUtils().asyncHttpRequest(
  //     NetConstants.clubMsg,
  //     method: DioMethod.POST,
  //     params: {"guildId": clubId},
  //     onSuccess: onNewMsg,
  //   );
  // }

  // 申请加入
  void requestJoin() {
    if (modeldel == null) return;
    // if (!accept) return WidgetUtils().showToast("请先阅读并同意协议");
    requestAuthStatus();
  }

  void requestAuthStatus() {
    DioUtils().asyncHttpRequest(NetConstants.authStatus, method: DioMethod.GET,
        onSuccess: (data) {
      int success = data["audit_status"] ?? 0;
      if (success == 1) {
        String? clubId = modeldel?.guildInfo?.guildId.toString();
        DioUtils().asyncHttpRequest(
          NetConstants.applyJoin,
          method: DioMethod.POST,
          params: {"guild_id": int.parse(clubId!)},
          onSuccess: onApplyJoin,
        );
      } else {
        WidgetUtils().showToast("请先进行实名认证");
      }
    });
  }

  void onNewMsg(response) {
    hasNewMsg = response == true;
    notifyListeners();
  }

  void onApplyJoin(response) {
    modeldel!.guildrelationType = InClubStatus.SIGN_APPLY.index;
    notifyListeners();
  }
}
