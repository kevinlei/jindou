import 'package:echo/import.dart';
import 'package:flutter/material.dart';

import '../model/club_lieb_model.dart';

class ClubRoomProvider extends ChangeNotifier {
  String ownerId = "";
  List<ClubLiebModel> roomList = [];

  void requestRoom(String ownerId) {
    if (ownerId.isEmpty) return;
    this.ownerId = ownerId;
    DioUtils().asyncHttpRequest(
      NetConstants.clubRoom,
      method: DioMethod.POST,
      params: {"page": 1, "guild_id":int.parse(ownerId) ,"page_size":20},
      onSuccess: onRoomList,
    );
  }

  void onRoomList(response) {
    response['rooms'] ??= [];
    roomList.clear();
    for (var data in response['rooms']) {
      roomList.add(ClubLiebModel.fromJson(data));
    }
    notifyListeners();
  }
}
