import 'package:flutter/material.dart';
import 'package:echo/import.dart';

import '../model/club_membart_model.dart';


class ClubMemberProvider extends ChangeNotifier {
  List<ClubMembartModel> memberList = [];
  List<ClubMembartModel> otherMember = [];

  void requestMember(String clubId) {
    DioUtils().asyncHttpRequest(
      NetConstants.clubMember,
      params: {"guild_id":  int.parse(clubId), "page": 1,"page_size":10},
      method: DioMethod.POST,
      onSuccess: updateMember,
    );
  }

  void updateMember(response) {
    response ??= [];
    memberList.clear();
    otherMember.clear();
    for (var data in response) {
      ClubMembartModel memberModel = ClubMembartModel.fromJson(data);
      memberList.add(memberModel);
      // if (memberModel.clubRole != ClubRole.OWNER.index) {
        otherMember.add(memberModel);
      // }
    }
    notifyListeners();
  }
}
