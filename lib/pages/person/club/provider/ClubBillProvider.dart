import 'package:flutter/material.dart';
import 'package:echo/import.dart';

import '../model/club_lieb_model.dart';

class ClubBillProvider extends ChangeNotifier {
  String clubId = "";
  ClubModel? clubModel;
  ClubLiebModel? roomModel;

  bool isClubBill = true;
  int curPage = 1;
  int billType = 1;
  DateTime? dateS, dateE;

  List billData = [];

  double today = 0.0;
  double all = 0.0;

  void initBillData(
      String clubId, ClubModel? clubModel, ClubLiebModel? roomModel) {
    this.clubId = clubId;
    this.clubModel = clubModel;
    this.roomModel = roomModel;
    if (roomModel == null) {
      isClubBill = true;
    } else {
      isClubBill = false;
    }
    requestTotalValue();
  }

  void requestTotalValue() {
    DioUtils().asyncHttpRequest(
      isClubBill
          ? NetConstants.clubTotalBillValue
          : NetConstants.clubRoomTotalBillValue,
      method: DioMethod.POST,
      params: isClubBill
          ? {"guildId": clubId}
          : {"guildId": clubId, "roomId": "${roomModel?.roomId}"},
      onSuccess: onUpdateTotal,
    );
  }

  void onUpdateTotal(response) {
    if (isClubBill) {
      all = response?["all"] ?? 0;
    } else {
      all = response?["allCount"] ?? 0;
    }
    notifyListeners();
  }

  void onTapTab(int type) async {
    if (billType != type) {
      billType = type;
      notifyListeners();
    }
    if (type == 4) {
      List? date = await WidgetUtils().showFDialog(ClubBillTime());
      if (date == null || date.length != 2) return;
      dateS = date.first;
      dateE = date.last;
      notifyListeners();
    }
    callRefresh();
  }

  Future<void> callRefresh() async {
    curPage = 1;
    await requestBillValue();
    requestTimeBill(true);
  }

  Future<void> callLoad() async {
    curPage += 1;
    requestTimeBill(false);
  }

  Future<void> requestBillValue() async {
    await DioUtils().httpRequest(
      isClubBill
          ? NetConstants.clubTimeBillValue
          : NetConstants.clubRoomTimeBillValue,
      method: DioMethod.POST,
      params: isClubBill
          ? {
              "startTime": getStartTime(),
              "endTime": getEndTime(),
              "guildId": clubId,
            }
          : {
              "startTime": getStartTime(),
              "endTime": getEndTime(),
              "guildId": clubId,
              "roomId": "${roomModel?.roomId}",
            },
      onSuccess: onBillValue,
    );
  }

  void onBillValue(response) {
    today = response ?? 0.0;
    notifyListeners();
  }

  void requestTimeBill(bool isRefresh) {
    DioUtils().asyncHttpRequest(
      isClubBill
          ? NetConstants.clubTimeBillList
          : NetConstants.clubRoomTimeBillList,
      method: DioMethod.POST,
      params: getTimeParams(),
      onSuccess: isRefresh ? onRefresh : onLoad,
    );
  }

  void onRefresh(response) {
    billData.clear();
    billData = response?["data"] ?? [];
    notifyListeners();
  }

  void onLoad(response) {
    billData.addAll(response?["data"] ?? []);
    notifyListeners();
  }

  Map getTimeParams() {
    if (isClubBill) {
      return {
        "startTime": getStartTime(),
        "endTime": getEndTime(),
        "guildId": clubId,
        "pageIndex": curPage,
        "pageSize": AppConstants.pageSize,
      };
    }
    return {
      "startTime": getStartTime(),
      "endTime": getEndTime(),
      "pageIndex": curPage,
      "guildId": clubId,
      "pageSize": AppConstants.pageSize,
      "roomId": "${roomModel?.roomId}",
    };
  }

  int? getStartTime() {
    DateTime dateNow = DateTime.now();
    if (billType == 1) {
      return DateTime(dateNow.year, dateNow.month, dateNow.day)
          .millisecondsSinceEpoch;
    } else if (billType == 2) {
      int week = dateNow.weekday;
      return DateTime(dateNow.year, dateNow.month, dateNow.day - (week - 1))
          .millisecondsSinceEpoch;
    } else if (billType == 3) {
      return DateTime(dateNow.year, dateNow.month, 1).millisecondsSinceEpoch;
    } else if (billType == 4) {
      return dateS?.millisecondsSinceEpoch;
    }
    return null;
  }

  int? getEndTime() {
    DateTime dateNow = DateTime.now();
    if (billType == 1) {
      return DateTime(dateNow.year, dateNow.month, dateNow.day + 1)
          .millisecondsSinceEpoch;
    } else if (billType == 2) {
      int week = dateNow.weekday;
      return DateTime(dateNow.year, dateNow.month, dateNow.day + (7 - week) + 1)
          .millisecondsSinceEpoch;
    } else if (billType == 3) {
      return DateTime(dateNow.year, dateNow.month + 1, 1)
          .millisecondsSinceEpoch;
    } else if (billType == 4) {
      return dateE?.millisecondsSinceEpoch;
    }
    return null;
  }
}
