import 'package:flutter/material.dart';
import 'package:echo/import.dart';

import '../../../widgets/myfont_weight.dart';
import 'model/club_roomwho_mdoe.dart';

class ClubPage extends StatefulWidget {
  const ClubPage({Key? key}) : super(key: key);

  @override
  _ClubPageState createState() => _ClubPageState();
}

class _ClubPageState extends State<ClubPage> {
  ClubRoomwhoMdoe? selfClub;

  List<ClubssModel> clubList = [];

  @override
  void initState() {
    super.initState();
    callRefresh();
  }

  @override
  Widget build(BuildContext context) {
    return  FContainer(
      padding: EdgeInsets.only(left: 15.w, right: 15.w, bottom: 20.w),
      image: ImageUtils().getAssetsImage("public/huibg"),
      imageFit: BoxFit.fill,
      imageAlign: Alignment.topCenter,
      child: SafeArea(child: Scaffold(

        backgroundColor: Colors.transparent,
        appBar: CommonAppBar(
            title: "公会中心",
            textColor: Colors.black,
            elevation: 0,
            // backgroundColor: Colors.transparent,
            actions: [buttonCreate()]),
        body: FRefreshLayout(
          onRefresh: callRefresh,
          firstRefresh: false,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              searchContent(),
               FContainer(
                 margin: EdgeInsets.only(
                     left: 30.w, top: 15.w, right: 30.w, bottom: 15.w),
                 child:  BannerSwipe(BanbarSwper.GOHUI_COM.brand),
               ),

              SelfClubContent(model: selfClub),
              FContainer(
                color: const Color(0xFFFFFFFF),
                margin: EdgeInsets.only(
                    left: 30.w, top: 15.w, right: 30.w, bottom: 15.w),
                radius: BorderRadius.circular(15.w),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.fromLTRB(25.w, 30.h, 25.w, 30.h),
                      child: FText("推荐公会", weight: FontWeight.bold, size: 36.sp,color: Colors.black,),
                    ),
                    clubContent(),
                    SizedBox(
                      height: 30.w,
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 20.w,
              ),
            ],
          ),
        ),
      )),
    );


  }

  Widget searchContent() {
    return FContainer(
      image: ImageUtils().getAssetsImage("room/guildBg"),
      imageFit: BoxFit.fill,
      imageAlign: Alignment.topCenter,
      margin: EdgeInsets.only(left: 30.w, top: 30.w, right: 30.w, bottom: 15.w),
      height: 270.w,
      child: FContainer(
        padding: EdgeInsets.all(32.w),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            FText(
              "搜索公会",
              size: 36.sp,
              color: Colors.white,
            ),
            SizedBox(
              height: 5.h,
            ),
            FText(
              "搜索你感兴趣的公会加入吧～",
              size: 24.sp,
              color: Colors.white.withOpacity(0.6),
            ),
            const Spacer(),
            searchButton(),
            SizedBox(
              height: 10.w,
            ),
          ],
        ),
      ),
    );
  }

  Widget searchButton() {
    return GestureDetector(
      onTap: onTapSearch,
      child: FContainer(
        height: 64.h,
        border: Border.all(color: Colors.white.withOpacity(0.3), width: 1.w),
        padding: EdgeInsets.only(
          left: 20.w,
        ),
        margin: EdgeInsets.only(
          right: 10.w,
        ),
        radius: BorderRadius.circular(15.w),
        gradient: const [Color(0xFF33FFFFFF), Color(0xFF33FFFFFF)],
        gradientBegin: Alignment.centerLeft,
        gradientEnd: Alignment.centerRight,
        align: Alignment.center,
        child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
          LoadAssetImage("room/search", width: 40.w, height: 40.w),
          SizedBox(width: 10.w),
          FText("搜索公会昵称、ID", size: 26.sp, color: const Color(0xFFFFFFFF),weight: MyFontWeight.medium,),
        ]),
      ),
    );
  }

  Widget clubContent() {
    if (clubList.isEmpty) return const FEmptyWidget();
    return GridView.builder(
      padding: EdgeInsets.symmetric(horizontal: 25.w),
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 1,
        mainAxisExtent: 176.h,
        crossAxisSpacing: 20.w,
        mainAxisSpacing: 20.w,
      ),
      itemCount: clubList.length,
      itemBuilder: (_, index) =>
          ClubListCell(info: clubList[index], call: callRefresh),
    );
  }

  Widget buttonCreate() {
    bool visible = selfClub == null;
    return Visibility(
      visible: visible,
      child: TextButton(
          onPressed: onTapCreate,
          child: FText("创建公会", color: Colors.black.withOpacity(0.6))),
    );
  }

  void onTapSearch() {
    WidgetUtils().pushPage(const ClubSearch());
  }

  void onTapCreate() async {

    await WidgetUtils().pushPage(ClubCreate());
    callRefresh();
  }

  Future<void> callRefresh() async {
    requestSelfClub();
    requestClubList();
  }

  void requestSelfClub() {
    String userId = UserConstants.userginInfo?.info.userId ?? "";
    DioUtils().asyncHttpRequest(
      NetConstants.userClub,
      method: DioMethod.POST,
      params: {"userId": userId},
      onSuccess: onSelfClub,
    );
  }

  void requestClubList() {
    DioUtils().asyncHttpRequest(NetConstants.clubList,
        method: DioMethod.POST,
        params: {"page":1,"page_size":1000},
        onSuccess: onUpdateList);
  }
  void onSelfClub(response) {
    selfClub = null;
    if (response == null) {
      if (!mounted) return;
      setState(() {});
      return;
    }
    // try {
    //
    // } catch (err) {}
    selfClub = ClubRoomwhoMdoe.fromJson(response);
    if (!mounted) return;
    setState(() {});
  }

  void onUpdateList(response) {
    response['list'] ??= [];
    clubList.clear();
    for (var data in response['list']) {
      clubList.add(ClubssModel.fromJson(data));
    }
    if (!mounted) return;
    setState(() {});
  }
}
