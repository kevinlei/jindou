import 'package:flutter/material.dart';
import 'package:echo/import.dart';

import 'model/club_membart_model.dart';


class ClubMember extends StatefulWidget {
  final String clubId;
  final String ownerId;
  final int clubRole;

  const ClubMember(
      {Key? key,
      required this.clubId,
      required this.clubRole,
      required this.ownerId})
      : super(key: key);

  @override
  _ClubMemberState createState() => _ClubMemberState();
}

class _ClubMemberState extends State<ClubMember> {
  int memberStatus = 0;
  int curPage = 1;
  List<ClubMembartModel> memberList = [];

  @override
  Widget build(BuildContext context) {
    return FContainer(
          padding: EdgeInsets.only(left: 15.w, right: 15.w, bottom: 20.w),
          image: ImageUtils().getAssetsImage("public/huibg"),
          imageFit: BoxFit.fill,
          imageAlign: Alignment.topCenter,
          child: SafeArea(child:Scaffold(
            backgroundColor: Colors.transparent,
            appBar: memberAppbar(),
            body: FRefreshLayout(
              onRefresh: callRefresh,
              onLoad: callLoad,
              child: ListView.builder(
                padding: EdgeInsets.fromLTRB(25.w, 20.h, 25.w, 0),
                itemBuilder: (_, index) => ClubMemberCell(
                  memberList[index],
                  clubId: widget.clubId,
                  ownerId: widget.ownerId,
                  memberStatus: memberStatus,
                  onRefresh: callRefresh,
                ),
                itemExtent: 162.w,
                itemCount: memberList.length,
              ),
            ),

    )

    ),
    );
  }


  CommonAppBar memberAppbar() {
    Widget suffix = memberStatus == 0
        ? PopupMenuButton(
            icon: FText("管理", size: 28.sp),
            onSelected: onTapManage,
            offset: const Offset(0, kToolbarHeight),
            elevation: 0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(18.w),
              side: const BorderSide(color: Color(0xFFECEDEF)),
            ),
            itemBuilder: (_) => <PopupMenuEntry<String>>[
              PopupMenuItem<String>(
                value: "成员管理",
                child: Center(
                    child: FText("成员管理",
                        size: 26.sp, color: const Color(0xFF666666))),
              ),
              PopupMenuDivider(height: 1.h),
              PopupMenuItem<String>(
                value: "权限管理",
                child: Center(
                    child: FText("权限管理",
                        size: 26.sp, color: const Color(0xFF666666))),
              ),
            ],
          )
        : Padding(
            padding: EdgeInsets.only(right: 25.w),
            child: TextButton(
              onPressed: onTapDone,
              child: const FText(
                "完成",
                color: Colors.black,
              ),
            ),
          );

    return CommonAppBar(
      title: "成员管理",
      elevation: 0,
      backgroundColor: Colors.transparent,
      textColor: Colors.black,
      actions: [
        Visibility(
            child: suffix, visible: widget.clubRole == ClubRole.OWNER.index)
      ],
    );
  }

  void onTapManage(String value) {
    if (value == "成员管理") {
      memberStatus = 1;
      if (!mounted) return;
      setState(() {});
    } else if (value == "权限管理") {
      memberStatus = 2;
      if (!mounted) return;
      setState(() {});
    }
  }

  void onTapDone() {
    memberStatus = 0;
    if (!mounted) return;
    setState(() {});
  }

  Future<void> callRefresh() async {
    curPage = 1;
    requestMember(true);
  }

  Future<void> callLoad() async {
    curPage += 1;
    requestMember(false);
  }

  void requestMember(bool isRefresh) {
    DioUtils().asyncHttpRequest(
      NetConstants.clubMember,
      params: {"guild_id": int.parse(widget.clubId) , "page": curPage,"page_size":20},
      method: DioMethod.POST,
      onSuccess: isRefresh ? onRefresh : onLoad,
    );
  }

  void onRefresh(response) {
    response ??= [];
    memberList.clear();
    for (var data in response) {
      ClubMembartModel memberModel = ClubMembartModel.fromJson(data);
      memberList.add(memberModel);
    }
    if (!mounted) return;
    setState(() {});
  }

  void onLoad(response) {
    response ??= [];
    if (response.isEmpty) return;
    for (var data in response) {
      ClubMembartModel memberModel = ClubMembartModel.fromJson(data);
      memberList.add(memberModel);
    }
    if (!mounted) return;
    setState(() {});
  }
}
