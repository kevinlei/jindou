import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class ClubInfoPage1 extends StatefulWidget {
  final String clubId;
  const ClubInfoPage1({Key? key, required this.clubId}) : super(key: key);

  @override
  _ClubInfoPageState createState() => _ClubInfoPageState();
}

class _ClubInfoPageState extends State<ClubInfoPage> {
  ClubInfoProvider infoProvider = ClubInfoProvider();
  ClubMemberProvider memberProvider = ClubMemberProvider();
  ClubRoomProvider roomProvider = ClubRoomProvider();

  @override
  void initState() {
    super.initState();
    callRefresh();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: infoProvider),
        ChangeNotifierProvider.value(value: memberProvider),
        ChangeNotifierProvider.value(value: roomProvider),
      ],
      child: Scaffold(
        backgroundColor: Colors.white,
        body: FRefreshLayout.custom(
          onRefresh: callRefresh,
          firstRefresh: false,
          slivers: [
            const ClubInfoAppBar(),
            clubInfo(),
            const SliverToBoxAdapter(child: Divider(color: Color(0xFFEEEEEE))),
            const ClubMemberContent(),
            const SliverToBoxAdapter(child: Divider(color: Color(0xFFEEEEEE))),
            const ClubRoomContent(),
            SliverToBoxAdapter(child: SizedBox(height: 40.h)),
            SliverPadding(
              padding: EdgeInsets.fromLTRB(25.w, 0, 25.w, 0),
              sliver: SliverToBoxAdapter(child: FText("公会介绍", size: 36.sp)),
            ),
            SliverPadding(
                padding: EdgeInsets.fromLTRB(25.w, 0, 25.w, 120.h),
                sliver: SliverToBoxAdapter(
                  child: Consumer<ClubInfoProvider>(builder: (_, pro, __) {
                    debugPrint(pro.modeldel?.guildInfo?.guildIntro);

                    return FText(pro.modeldel?.guildInfo?.guildIntro ?? "",
                        size: 28.sp,
                        maxLines: 20,
                        color: const Color(0xFF666666));
                  }),
                )),
          ],
        ),
        bottomNavigationBar: bottomButton(),
      ),
    );
  }

  Widget clubInfo() {
    return SliverPadding(
      padding: EdgeInsets.fromLTRB(25.w, 48.h, 25.w, 20.h),
      sliver: SliverToBoxAdapter(
        child: Consumer<ClubInfoProvider>(builder: (_, pro, __) {
          ImageProvider? ownerHead;
          String head = '';
          if (pro.model != null) {
            head = pro.model!.clubOwner.ownerHead;
            // ownerHead =
            //     ImageUtils().getImageProvider(NetConstants.ossPath + head);
          }
          return Row(children: [
            GestureDetector(
                onTap: () {
                  if (pro.model == null) return;
                  WidgetUtils()
                      .pushPage(PersonHomepage(userId: pro.model!.clubOwner.ownerId));
                },
                child: FContainer(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(60.w),
                    child: LoadNetImage(NetConstants.ossPath + head),
                  ),
                )
                // CircleAvatar(
                //     radius: 120.w / 2,
                //     backgroundImage: ownerHead,
                //     backgroundColor: Colors.blueGrey.withOpacity(0.5)),
                ),
            SizedBox(width: 20.w),
            Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(children: [
                    FText(pro.model?.clubOwner.ownerName ?? "", size: 36.sp),
                    SizedBox(width: 10.w),
                    LoadAssetImage("room/i_club1", width: 94.w)
                  ]),
                  FText("ID：" + (pro.model?.clubOwner.ownerDisplayId ?? ""),
                      size: 24.sp, color: const Color(0xFF999999))
                ])
          ]);
        }),
      ),
    );
  }

  Widget bottomButton() {
    return Consumer<ClubInfoProvider>(builder: (_, pro, __) {
      String selfClubId = pro.selfClub?.clubInfo.clubId.toString() ?? "";
      // 自己的公会不为空
      if (selfClubId.isNotEmpty && selfClubId != widget.clubId) {
        return const SizedBox();
      }
      int clubRole = pro.model?.clubRole ?? ClubRole.DEF.index;
      int userStatus = pro.model?.userStatus ?? InClubStatus.DEFAULT.index;
      // 非公会成员 1：申请加入中 2：申请加入
      if (clubRole == ClubRole.DEF.index) {
        if (userStatus == InClubStatus.JOIN_APPLY.index) {
          return FcationButton("申请加入中", radius: 90.w);
        } else {
          return Column(children: [
            FProAccept([ProtocolType.PRO_CLUB_JOIN], onAccept: pro.onAccept),
            FLoginButton("申请加入",
                isActivate: true, radius: 90.w, onPress: pro.requestJoin)
          ], mainAxisSize: MainAxisSize.min);
        }
      }

      return const SizedBox();
    });
  }

  Future<void> callRefresh() async {
    await infoProvider.requestSelfClub();
    await infoProvider.requestClubInfo(widget.clubId);
    memberProvider.requestMember(widget.clubId);
    String ownerId = infoProvider.model?.clubOwner.ownerId ?? "";
    roomProvider.requestRoom(ownerId);
  }
}
