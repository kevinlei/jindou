import 'package:flutter/material.dart';
import 'package:echo/import.dart';

import 'model/club_lieb_model.dart';
// 公会流水
class ClubBill extends StatefulWidget {
  final String clubId;
  final ClubModel? clubModel;
  final ClubLiebModel? roomModel;

  const ClubBill(this.clubId, {Key? key, this.clubModel, this.roomModel})
      : super(key: key);

  @override
  _ClubBillState createState() => _ClubBillState();
}

class _ClubBillState extends State<ClubBill> {
  ClubBillProvider provider = ClubBillProvider();

  @override
  void initState() {
    super.initState();
    provider.initBillData(widget.clubId, widget.clubModel, widget.roomModel);
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => provider,
        builder: (_, __) {
          return Scaffold(
            backgroundColor: Color(0xFF1F2234),
            appBar: CommonAppBar(
                title: provider.isClubBill ? "公会流水" : "房间流水",
              textColor: Colors.white,
              elevation: 1,
              backgroundColor: Color(0xFF1F2234),
            ),
            body: Column(children: [
              ClubBillTop(
                  clubModel: widget.clubModel, roomModel: widget.roomModel),
              Divider(color: Color(0xFFEEEEEE)),
              todayWidget(),
              Divider(color: Color(0xFFEEEEEE)),
              tabContent(),
              Expanded(child: billList()),
            ]),
          );
        });
  }

  Widget todayWidget() {
    return Padding(
      padding: EdgeInsets.fromLTRB(25.w, 20.h, 0, 20.h),
      child: Align(
        alignment: Alignment.centerLeft,
        child: Consumer<ClubBillProvider>(builder: (_, pro, __) {
          String text = "今日流水：";
          if (pro.billType == 2) {
            text = "本周流水：";
          } else if (pro.billType == 3) {
            text = "本月流水：";
          } else if (pro.billType == 4) {
            text = "筛选流水：";
          }
          return FRichText([
            FRichTextItem()
              ..text = text
              ..size = 32.sp
              ..color = Colors.white,
            FRichTextItem()
              ..text = "${pro.today}"
              ..size = 32.sp
              ..color = Color(0xFFD6AC6A),
          ]);
        }),
      ),
    );
  }

  Widget tabContent() {
    return Consumer<ClubBillProvider>(builder: (_, pro, __) {
      return Row(
          children: [tableCell(1), tableCell(2), tableCell(3), tableCell(4)]);
    });
  }

  Widget tableCell(int type) {
    return Flexible(
      child: GestureDetector(
        onTap: () => provider.onTapTab(type),
        child: FContainer(
          height: 80.w,
          align: Alignment.center,
          border: Border(
              bottom: BorderSide(
                  color: type == provider.billType
                      ? Color(0xFFA852FF)
                      : Colors.white,
                  width: 4.w)),
          child: Text(getTabText(type),
              textScaleFactor: 1.0, style: getTabStyle(type)),
        ),
      ),
    );
  }

  Widget billList() {
    return Consumer<ClubBillProvider>(builder: (_, pro, __) {
      return FRefreshLayout(
        onRefresh: pro.callRefresh,
        onLoad: pro.callLoad,
        emptyWidget: pro.billData.isEmpty ? FEmptyWidget() : null,
        child: ListView.builder(
          itemBuilder: (_, index) => billCell(pro.billData[index]),
          itemExtent: 110.w,
          itemCount: pro.billData.length,
        ),
      );
    });
  }

  // {"roomName":"测2","giveUserName":"会长","receiveUserName":"会长","count":1724.0,"giftInfo":{},"createTime":1649958292285}
  Widget billCell(data) {
    // String giveUser = data["giveUserName"] ?? "";
    // String receiveUser = data["receiveUserName"] ?? "";
    // giveUser =
    //     giveUser.length >= 5 ? (giveUser.substring(0, 5) + "...") : giveUser;
    // receiveUser = receiveUser.length >= 5
    //     ? (receiveUser.substring(0, 5) + "...")
    //     : receiveUser;
    // String main = "$giveUser赠送$receiveUser";
    // main += giftString(data);
    // int time = data["createTime"] ?? 0;
    // String sub =
    //     DateTime.fromMillisecondsSinceEpoch(time).toString().substring(0, 19);
    // double count = data["count"] ?? 0;
    // String suf = count > 0 ? "+$count" : "$count";
    // return BillItem(main, sub, suf);
    return data;
  }

  String giftString(data) {
    Map giftInfo = data["giftInfo"] ?? {};
    String content = "";
    // for (String giftId in giftInfo.keys) {
    //   ConfigGift? gift = ConfigManager().getGift(int.parse(giftId));
    //   if (gift == null) continue;
    //   content += "${gift.name} x ${giftInfo[giftId]}个";
    // }
    return content;
  }

  String getTabText(int type) {
    if (type == 1) return "今日流水";
    if (type == 2) return "本周流水";
    if (type == 3) return "本月流水";
    if (type == 4) {
      if (provider.dateS != null && provider.dateE != null) {
        String textS =
            "${provider.dateS!.year}/${provider.dateS!.month}/${provider.dateS!.day}";
        String textE =
            "${provider.dateE!.year}/${provider.dateE!.month}/${provider.dateE!.day}";
        return textS + "~\n" + textE;
      }
      return "自定义";
    }

    return "";
  }

  TextStyle getTabStyle(int type) {
    bool showSmall =
        type == 4 && provider.dateS != null && provider.dateE != null;
    if (type == provider.billType) {
      return TextStyle(
          fontSize: showSmall ? 24.sp : 32.sp,
          color: Color(0xFFA852FF),
          fontWeight: FontWeight.bold);
    }
    return TextStyle(
        fontSize: showSmall ? 24.sp : 32.sp, color: Color(0xFF999999));
  }
}
