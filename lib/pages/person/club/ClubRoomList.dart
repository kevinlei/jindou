import 'package:flutter/material.dart';
import 'package:echo/import.dart';

import 'model/club_lieb_model.dart';

class ClubRoomList extends StatefulWidget {
  final String clubId;
  final String ownerId;
  final int clubRole;

  const ClubRoomList(
      {Key? key,
      required this.ownerId,
      required this.clubRole,
      required this.clubId})
      : super(key: key);

  @override
  _ClubRoomListState createState() => _ClubRoomListState();
}

class _ClubRoomListState extends State<ClubRoomList> {
  List<ClubLiebModel> roomList = [];
  int curPage = 1;

  @override
  Widget build(BuildContext context) {
    return FContainer(
      padding: EdgeInsets.only(left: 15.w, right: 15.w, bottom: 20.w),
      image: ImageUtils().getAssetsImage("public/huibg"),
      imageFit: BoxFit.fill,
      imageAlign: Alignment.topCenter,
      child: SafeArea(child: Scaffold(
        appBar: createAppbar(),
        backgroundColor: Colors.transparent,
        body: FRefreshLayout(
          onRefresh: callRefresh,
          onLoad: callLoad,
          emptyWidget: roomList.isEmpty ? FEmptyWidget() : null,
          child: ListView.builder(
            padding: EdgeInsets.fromLTRB(25.w, 40.h, 25.w, 0),
            itemBuilder: (_, index) => roomCell(index),
            itemCount: roomList.length,
            itemExtent: 220.w,
          ),
        ),
      )

      ));



  }

  Widget roomCell(int index) {
    Widget roomCover = FContainer(
      width: 160.w,
      height: 160.w,
      imageFit: BoxFit.cover,
      image: ImageUtils()
          .getImageProvider(NetConstants.ossPath + roomList[index].image.toString()),
      radius: BorderRadius.circular(20.w),
      color: Color(0xFFEEEEEE),
    );

    Widget roomInfo = Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              ConstrainedBox(
                constraints: BoxConstraints(maxWidth: 200.w),

                child:FText(roomList[index].name ??"",size: 28.sp, overflow: TextOverflow.ellipsis,color: Colors.black,weight: FontWeight.w600,),
              ),
              SizedBox(width: 10.w),
              FText("ID：${roomList[index].exId}",
                  size: 20.sp, color: Color(0xFF999999),overflow: TextOverflow.ellipsis),
            ],
          ),

          SizedBox(height: 10.w),


          Row(
            children: [
              Visibility(
                visible: (roomList[index].owner?.ownerName ?? "").isNotEmpty,
                child:
                Row(children: [
                  headerCell(
                      "房主:${roomList[index].owner?.ownerName.toString()}", ''),
                ],),
                // Row(children: [
                //   // LoadAssetImage("homepage/count", width: 20.w),
                //   SizedBox(width: 5.w),
                //   FText("${roomList[index].owner?.ownerName.toString()}",
                //       size: 20.sp, color: Color(0xFF999999)),
                // ])
              ),

              Visibility(
                visible: roomList[index].personNum.toString().isNotEmpty,
                child:Row(children: [
                  headerCell(
                      "${roomList[index].personNum.toString()}", 'person/postrut'),
                ],),
              ),


            ],
          ),





        ]);

    return GestureDetector(
      onTap: () => onTapRoom(index),
      child: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: [
                Color(0x00FF3F3F),
                Color(0xA1FF753F).withOpacity(0.4),
                Color(0xFFFF753F).withOpacity(0.4)
              ],
            ),
            borderRadius: BorderRadius.all(Radius.circular(20.w))),
        // radius: BorderRadius.circular(20.w),
        margin: EdgeInsets.only(bottom: 20.h),
        padding: EdgeInsets.symmetric(horizontal: 20.w),
        /* gradient: [Color(0x00FF3F3F).withOpacity(0.4), Color(0xA1FF753F).withOpacity(0.4), Color(0xFFFF753F).withOpacity(0.4)],
        gradientBegin: Alignment.centerLeft,
        gradientEnd: Alignment.centerRight,*/
        child: Row(children: [
          roomCover,
          SizedBox(width: 20.w),
          roomInfo,
          const Spacer(),
          buttonCheck(index),
        ]),
      ),
    );
  }

  Widget headerCell(String text, image) {
    return FContainer(
      color: Color(0xFFFF753F).withOpacity(0.6),
      margin: EdgeInsets.only(right: 8.w),
      padding: EdgeInsets.only(left: 15.w, right: 15.w, top: 5.h, bottom: 5.h),
      radius: BorderRadius.circular(50),
      child: Row(
        children: [
          Visibility(
            visible: image != "",
            child: LoadAssetImage(
              image,
              width: 20.w,
            ),
          ),
          SizedBox(
            width: 5.w,
          ),
          FText(
            text,
            color: Color(0xFFFFFFFF),
            size: 24.sp,
          ),
        ],
      ),
    );
  }
  CommonAppBar createAppbar() {
    Widget suffix = Visibility(
        visible: widget.clubRole == ClubRole.VICE.index,
        child: TextButton(onPressed: onTapCreate, child: FText("创建",color: Colors.black,)));
    return CommonAppBar(
      title: "公会房间列表",
      elevation: 0,
      backgroundColor:Colors.transparent,
      textColor: Colors.black,
      actions: [suffix],
    );
  }

  Widget buttonCheck(int index) {
    Widget btn = FContainer(
      child:Row(
        children: [
          FText(
            "详情",
            size: 28.sp,
            color: Colors.black.withOpacity(0.6),
          ),
          LoadAssetImage(
            "person/restublt",
            width: 16.w,
          ),
        ],
      ),
    );

    return Visibility(
        visible: widget.clubRole == ClubRole.VICE.index,
        child: GestureDetector(onTap: () => onTapCheck(index), child: btn));
  }

  void onTapRoom(int index) async {
    //房间详情

    WidgetUtils().pushPage(CLubProstdrass(clubId:roomList[index].exId.toString(),room_id: roomList[index].guildId.toString(),REstroom_id: roomList[index].roomId,));

    // int roomId = roomList[index].roomId;
    // SharedPreferences preferences = await SharedPreferences.getInstance();
    // preferences.setInt(SharedKey.KEY_ROOM_ID.value, roomId);
    // WidgetUtils().popPage();
    // WidgetUtils().popPage();
    // WidgetUtils().popPage();
    // widget.homePageCtr.nextPage(
    //     duration: const Duration(milliseconds: 200), curve: Curves.linear);
    // widget.homePageCtr.jumpToPage(1);
  }

  void onTapCheck(int index) {
    // WidgetUtils().pushPage(ClubBill(widget.clubId, roomModel: roomList[index]));
  }

  void onTapCreate() async {
    WidgetUtils()
        .pushPage(RoomCreate(EnterRoomType.ROOM_CLUB));
  }

  Future<void> callRefresh() async {
    curPage = 1;
    requestRoom(true);
  }

  Future<void> callLoad() async {
    curPage += 1;
    requestRoom(false);
  }

  void requestRoom(bool isRefresh) {
    DioUtils().asyncHttpRequest(
      NetConstants.clubRoom,
      method: DioMethod.POST,
      params: {"page": curPage, "guild_id": int.parse(widget.ownerId) ,"page_size":20},
      onSuccess: isRefresh ? onRefresh : onLoad,
    );
  }

  void onRefresh(response) {
    response['rooms'] ??= [];
    roomList.clear();
    for (var data in response['rooms']) {
      roomList.add(ClubLiebModel.fromJson(data));
    }
    if (!mounted) return;
    setState(() {});
  }

  void onLoad(response) {
    response['rooms'] ??= [];
    for (var data in response['rooms']) {
      roomList.add(ClubLiebModel.fromJson(data));
    }
    if (!mounted) return;
    setState(() {});
  }
}
