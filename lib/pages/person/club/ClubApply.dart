import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class ClubApply extends StatefulWidget {
  final String clubId;

  const ClubApply({Key? key, required this.clubId}) : super(key: key);

  @override
  _ClubApplyState createState() => _ClubApplyState();
}

class _ClubApplyState extends State<ClubApply>
    with SingleTickerProviderStateMixin {
  late TabController tabController;

  @override
  void initState() {
    super.initState();
    tabController = TabController(length: 2, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
        padding: EdgeInsets.only(left: 15.w, right: 15.w, bottom: 20.w),
    image: ImageUtils().getAssetsImage("public/huibg"),
    imageFit: BoxFit.fill,
    imageAlign: Alignment.topCenter,
    child: SafeArea(child:
    Scaffold(
        backgroundColor: Colors.transparent,
        appBar: applyAppbar(),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TabBar(
              padding: EdgeInsets.symmetric(vertical: 20.h),
              labelStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 30.sp),
              labelColor: Colors.black,
              unselectedLabelStyle:
              TextStyle(fontWeight: FontWeight.normal, fontSize: 30.sp),
              unselectedLabelColor: Color(0xFF999999),
              tabs: [
                Text("入会申请", textScaleFactor: 1.0),
                // Text("签约申请", textScaleFactor: 1.0),
                Text("解约申请", textScaleFactor: 1.0),
                // Text("退出申请", textScaleFactor: 1.0),
              ],
              controller: tabController,
              indicatorColor: Color(0xFFFF6E60),
              indicatorSize: TabBarIndicatorSize.label,
            ),
            Expanded(
                child: TabBarView(children: [
                  ClubApplyList(ClubApplyType.APPLY_ENTER, clubId: widget.clubId),
                  // ClubApplyList(ClubApplyType.APPLY_SIGN, clubId: widget.clubId),
                  ClubApplyList(ClubApplyType.APPLY_UN_SIGN, clubId: widget.clubId),
                  // ClubApplyList(ClubApplyType.APPLY_QUIT, clubId: widget.clubId),
                ], controller: tabController)
            ),
          ],
        )
    ),
    ));


  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  CommonAppBar applyAppbar() {
    return CommonAppBar(
      title: "审核管理",
      textColor: Colors.black,
      elevation: 0,
      backgroundColor: Colors.transparent,
    );
  }
}
