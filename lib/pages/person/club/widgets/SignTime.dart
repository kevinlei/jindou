import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class SignTime extends StatefulWidget {
  @override
  _SignTimeState createState() => _SignTimeState();
}

class _SignTimeState extends State<SignTime> {
  int time = 1;

  List<String> timeText = ["半年", "一年", "两年", "三年"];

  @override
  Widget build(BuildContext context) {
    return Center(
      child: FContainer(
        width: 560.w,
        height: 400.w,
        padding: EdgeInsets.fromLTRB(35.w, 40.w, 35.w, 20.w),
        color: Color(0xFF1F2234),
        radius: BorderRadius.circular(28.w),
        child: content(),
      ),
    );
  }

  Widget content() {
    return Column(children: [
      FText("签约时长", size: 36.sp),
      Spacer(),
      Row(
          children:
              List.generate(timeText.length, (index) => selectCell(index + 1)),
          mainAxisAlignment: MainAxisAlignment.spaceBetween),
      Spacer(),
      Row(
          children: [buttonCancel(), buttonConfirm()],
          mainAxisAlignment: MainAxisAlignment.spaceEvenly)
    ]);
  }

  Widget selectCell(int index) {
    Widget btn = FContainer(
      width: 108.w,
      height: 108.w,
      radius: BorderRadius.circular(16.w),
      color: index == time ? Color(0xFFA852FF) : Color(0xFF2C2F40),
      child: FText(timeText[index - 1],
          color: index == time ? Colors.white : Color(0xFF999999),
          size: 28.sp),
      align: Alignment.center,
    );
    return GestureDetector(onTap: () => onSelect(index), child: btn);
  }

  Widget buttonCancel() {
    Widget btn = FContainer(
      width: 210.w,
      height: 80.w,
      border: Border.all(color: Color(0xFFCCCCCC)),
      radius: BorderRadius.circular(80.w),
      align: Alignment.center,
      child: FText("再想想", size: 32.sp, color: Color(0xFF999999)),
    );
    return GestureDetector(onTap: onTapCancel, child: btn);
  }

  Widget buttonConfirm() {
    Widget btn = FSizeButton(
      "确定",
      width: 210.w,
      height: 80.w,
      isActivate:true,
      onPress: onTapConfirm,
    );
    return btn;
  }

  void onSelect(int index) {
    if (index == time) return;
    time = index;
    if (!mounted) return;
    setState(() {});
  }

  void onTapCancel() {
    WidgetUtils().popPage();
  }

  void onTapConfirm() {
    WidgetUtils().popPage(time);
  }
}
