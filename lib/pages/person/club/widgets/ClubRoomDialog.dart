import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import '../model/club_lieb_model.dart';

class ClubRoomDialog extends StatefulWidget {
  final String ownerId;

  const ClubRoomDialog({Key? key, required this.ownerId}) : super(key: key);

  @override
  _ClubRoomDialogState createState() => _ClubRoomDialogState();
}

class _ClubRoomDialogState extends State<ClubRoomDialog> {
  List<ClubLiebModel> roomList = [];
  int selectRoom = -1;
  int curPage = 1;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      color: Color(0xFF1F2234),
      padding: EdgeInsets.only(
          left: 20.w,
          right: 20.w,
          top: 20.w,
          bottom: ScreenUtil().bottomBarHeight + 20.w),
      child: Column(children: [
        FSheetLine(),
        listContent(),
        SizedBox(height: 20.w),
        Row(
            children: [buttonCancel(), buttonConfirm()],
            mainAxisAlignment: MainAxisAlignment.spaceEvenly)
      ], mainAxisSize: MainAxisSize.min),
    );
  }

  Widget listContent() {
    return SizedBox(
      height: 860.w,
      child: FRefreshLayout(
        onRefresh: callRefresh,
        onLoad: callLoad,
        child: ListView.builder(
          itemBuilder: (_, index) => roomCell(index),
          itemCount: roomList.length,
          itemExtent: 194.w,
        ),
      ),
    );
  }

  Widget roomCell(int index) {
    Widget btn = FContainer(
        radius: BorderRadius.circular(20.w),
        border: index == selectRoom
            ? Border.all(color: Color(0xFFA852FF))
            : Border.all(color: Colors.transparent),
        padding: EdgeInsets.symmetric(horizontal: 16.w),
        child: Row(children: [
          FContainer(
            width: 160.w,
            height: 160.w,
            imageFit: BoxFit.cover,
            image: ImageUtils().getImageProvider(
                NetConstants.ossPath + roomList[index].image.toString()),
            color: Color(0xFFEEEEEE),
            radius: BorderRadius.circular(20.w),
          ),
          SizedBox(width: 20.w),
          Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                FText(roomList[index].name ?? "", size: 36.sp),
                SizedBox(height: 10.w),
                FText("ID：${roomList[index].roomId}", color: Color(0xFF999999)),
                SizedBox(height: 10.w),
                Row(children: [
                  LoadAssetImage("homepage/count", width: 20.w),
                  SizedBox(width: 5.w),
                  FText("${roomList[index].personNum}",
                      size: 20.sp, color: Color(0xFF999999)),
                ])
              ]),
        ]));

    return GestureDetector(onTap: () => onTapRoom(index), child: btn);
  }

  Widget buttonCancel() {
    Widget btn = FContainer(
      width: 210.w,
      height: 80.w,
      border: Border.all(color: Color(0xFFCCCCCC)),
      radius: BorderRadius.circular(80.w),
      align: Alignment.center,
      child: FText("再想想", size: 32.sp, color: Color(0xFF999999)),
    );
    return GestureDetector(onTap: onTapCancel, child: btn);
  }

  Widget buttonConfirm() {
    Widget btn = FSizeButton(
      "确定",
      width: 210.w,
      height: 80.w,
      isActivate: true,
      onPress: onTapConfirm,
    );
    return btn;
  }

  void onTapRoom(int index) {
    if (index == selectRoom) return;
    selectRoom = index;
    if (!mounted) return;
    setState(() {});
  }

  void onTapCancel() {
    WidgetUtils().popPage();
  }

  void onTapConfirm() async {
    if (selectRoom == -1) return WidgetUtils().showToast("请选择一个房间");
    bool? accept = await WidgetUtils().showAlert("单个房间仅可设置一次厅主，后续不支持更改");
    if (accept == null || !accept) return;
    WidgetUtils().popPage(roomList[selectRoom].roomId);
  }

  Future<void> callRefresh() async {
    curPage = 1;
    requestRoom(true);
  }

  Future<void> callLoad() async {
    curPage += 1;
    requestRoom(false);
  }

  void requestRoom(bool isRefresh) {
    if (widget.ownerId.isEmpty) return;
    DioUtils().asyncHttpRequest(
      NetConstants.clubRoom,
      method: DioMethod.POST,
      params: {"page": curPage, "guild_id": widget.ownerId},
      onSuccess: isRefresh ? onRefresh : onLoad,
    );
  }

  void onRefresh(response) {
    response['rooms'] ??= [];
    roomList.clear();
    for (var data in response['rooms']) {
      roomList.add(ClubLiebModel.fromJson(data));
    }
    if (!mounted) return;
    setState(() {});
  }

  void onLoad(response) {
    response['rooms'] ??= [];
    for (var data in response['rooms']) {
      roomList.add(ClubLiebModel.fromJson(data));
    }
    if (!mounted) return;
    setState(() {});
  }
}
