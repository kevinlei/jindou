import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class ClubListCell extends StatelessWidget {
  final ClubssModel info;
  final Function()? call;
  const ClubListCell({Key? key, required this.info, this.call})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget btn = FContainer(
      radius: BorderRadius.circular(20.w),
      padding: EdgeInsets.all(24.w),
      color: const Color(0xFF0D000000),
      child: Row(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(20.w),
            child: LoadNetImage(
              NetConstants.ossPath +
                  info.guildAvatar +
                  OssThumb.OSS_IM_SML.path,
              width: 128.w,
              height: 128.w,
              fit: BoxFit.cover,
            ),
          ),
          SizedBox(
            width: 24.w,
          ),
          clubInfo(),
        ],
      ),
    );
    return GestureDetector(onTap: onTapClub, child: btn);
  }

  Widget clubInfo() {
    return FContainer(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Row(children: [
            FText(
              info.guildName,
              color: Colors.black,
              weight: FontWeight.bold,
              overflow: TextOverflow.ellipsis,
              size: 28.sp,
            ),
            SizedBox(width: 10.w),
          ]),
          FText("ID：${info.guildId}",
              size: 24.sp, color: Colors.black.withOpacity(0.8)),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              LoadAssetImage(
                "person/roomNum",
                width: 20.w,
              ),
              SizedBox(
                width: 10.w,
              ),
              FText("${info.rooms}", size: 22.sp, color: Colors.black),
              FContainer(
                margin: EdgeInsets.only(left: 10.w, right: 10.w),
                width: 20.w,
                align: Alignment.center,
                child: const FText(
                  "|",
                  color: Colors.black,
                ),
              ),
              LoadAssetImage(
                "person/roomPersonNum",
                width: 20.w,
              ),
              SizedBox(
                width: 10.w,
              ),
              FText("${info.memberCount}人", size: 22.sp, color: Colors.black),
            ],
          )
        ],
      ),
    );
  }

  Widget clubInfo1() {
    return FContainer(
      color: const Color(0xFF000000).withOpacity(0.4),
      radius: BorderRadius.only(
        bottomRight: Radius.circular(20.w),
        bottomLeft: Radius.circular(20.w),
      ),
      //    padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 10.h),
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            FText("ID：${info.guildId}", size: 22.sp, color: Colors.white)
          ]),
    );
  }

  void onTapClub() async {
    await WidgetUtils().pushPage(ClubInfoPage(clubId: info.guildId.toString()));
    if (call == null) return;
    call!();
  }
}
