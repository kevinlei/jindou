import 'dart:io';
import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import '../model/club_mspostmdoel.dart';

class CLubProstdrass extends StatefulWidget {
  final String clubId;
  final String room_id;
  final int REstroom_id; //设置厅长房间ID
  const CLubProstdrass(
      {Key? key,
      required this.clubId,
      required this.room_id,
      required this.REstroom_id})
      : super(key: key);

  @override
  _CLubProstdrassState createState() => _CLubProstdrassState();
}

class _CLubProstdrassState extends State<CLubProstdrass> {
  ClubInfoProvider infoProvider = ClubInfoProvider();

  @override
  void initState() {
    super.initState();
    callRefresh();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: infoProvider),
      ],
      child: FContainer(
        padding: EdgeInsets.only(left: 15.w, right: 15.w, bottom: 20.w),
        image: ImageUtils().getAssetsImage("public/huibg"),
        imageFit: BoxFit.fill,
        imageAlign: Alignment.topCenter,
        child: SafeArea(
          child: Scaffold(
            backgroundColor: Colors.transparent,
            body: FRefreshLayout(
              onRefresh: callRefresh,
              firstRefresh: false,
              child: FContainer(
                child: Consumer<ClubInfoProvider>(builder: (_, pro, __) {
                  int clubRole =
                      pro.postmdoel?.memberType ?? ClubRole.DEF.index;
                  int userRole = UserConstants.userginInfo?.info.userRole ??
                      UserRole.DEF.index;
                  print('66666666');
                  print(clubRole);
                  print(userRole);
                  return SingleChildScrollView(
                    child: Column(
                      children: [
                        CommonAppBar(
                          title: "房间详情",
                          actions: [
                            exitButton(clubRole, userRole, pro.postmdoel),
                          ],
                        ),
                        headerContent(pro),
                        Visibility(
                            visible: clubRole == ClubRole.VICE.index,
                            child: GestureDetector(
                                onTap: () => {},
                                child: datashenContent(pro.postmdoel))),
                        Visibility(
                            visible: clubRole == ClubRole.VICE.index,
                            child: GestureDetector(
                                onTap: () => {},
                                child: datalistContent(pro.postmdoel))),
                      ],
                    ),
                  );
                }),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget exitButton(int role, userRole, ClubMspostmdoel? model) {
    return Visibility(
      visible: ((role == ClubRole.DEF.index) &&
              (userRole != InClubStatus.SIGN_APPLY.index))
          ? false
          : true,
      child: TextButton(
        onPressed: () {
          onTapSign(model);
        },
        child: LoadAssetImage(
          "person/bakspck",
          width: 60.w,
          height: 60.w,
        ),
      ),
      //true显示
    );
  }

  Widget datashenContent(ClubMspostmdoel? club) {
    return FContainer(
      color: const Color(0xFFFFFFFF),
      margin: EdgeInsets.only(left: 25.w, right: 25.w, bottom: 20.w, top: 15.w),
      radius: BorderRadius.circular(28.w),
      padding: EdgeInsets.all(32.w),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              SizedBox(
                width: 40.w,
              ),
              Column(
                children: [
                  ConstrainedBox(
                    constraints: BoxConstraints(maxWidth: 200.w),
                    child: FText(
                      club?.roomInfo?.owner?.ownerName ?? "",
                      size: 32.sp,
                      overflow: TextOverflow.ellipsis,
                      color: const Color(0xFF8376F3),
                      weight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(
                    height: 10.w,
                  ),
                  FText(
                    "厅主名称",
                    size: 24.sp,
                    color: Colors.black.withOpacity(0.5),
                    weight: FontWeight.bold,
                  ),
                ],
              ),
              const Spacer(),
              Column(
                children: [
                  FText(
                    "|",
                    size: 32.sp,
                    color: const Color(0xFF222222).withOpacity(0.2),
                    weight: FontWeight.bold,
                  ),
                ],
              ),
              const Spacer(),
              GestureDetector(
                child: FContainer(
                  child: Column(
                    children: [
                      FText(
                        '${club?.roomSerialSum}',
                        size: 32.sp,
                        color: const Color(0xFFFF753F),
                        weight: FontWeight.bold,
                      ),
                      SizedBox(
                        height: 10.w,
                      ),
                      FText(
                        "厅总流水",
                        size: 24.sp,
                        color: Colors.black.withOpacity(0.5),
                        weight: FontWeight.bold,
                      ),
                    ],
                  ),
                ),
                onTap: () {
                  // onTapsBill(club);
                },
              ),
              SizedBox(
                width: 40.w,
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget datalistContent(ClubMspostmdoel? club) {
    return FContainer(
      color: const Color(0xFFFFFFFF),
      margin: EdgeInsets.only(left: 25.w, right: 25.w, bottom: 20.w, top: 15.w),
      radius: BorderRadius.circular(28.w),
      padding: EdgeInsets.all(32.w),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              SizedBox(
                width: 30.w,
              ),
              Column(
                children: [
                  FText(
                    '${club?.daySerialSum}',
                    size: 32.sp,
                    color: const Color(0xFF222222),
                    weight: FontWeight.bold,
                  ),
                  SizedBox(
                    height: 10.w,
                  ),
                  FText(
                    "今日流水",
                    size: 24.sp,
                    color: Colors.black.withOpacity(0.6),
                    weight: FontWeight.bold,
                  ),
                ],
              ),
              const Spacer(),
              Column(
                children: [
                  FText(
                    '${club?.weekSerialSum}',
                    size: 32.sp,
                    color: const Color(0xFF222222),
                    weight: FontWeight.bold,
                  ),
                  SizedBox(
                    height: 10.w,
                  ),
                  FText(
                    "本周流水",
                    size: 24.sp,
                    color: Colors.black.withOpacity(0.6),
                    weight: FontWeight.bold,
                  ),
                ],
              ),
              const Spacer(),
              GestureDetector(
                child: FContainer(
                  child: Column(
                    children: [
                      FText(
                        '${club?.monthSerialSum}',
                        size: 32.sp,
                        color: const Color(0xFF222222),
                        weight: FontWeight.bold,
                      ),
                      SizedBox(
                        height: 10.w,
                      ),
                      FText(
                        "本月流水",
                        size: 24.sp,
                        color: Colors.black.withOpacity(0.6),
                        weight: FontWeight.bold,
                      ),
                    ],
                  ),
                ),
                onTap: () {
                  // onTapsBill(club);
                },
              ),
              SizedBox(
                width: 30.w,
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget dataGridViewCell(String title, int day, yesterday) {
    return Expanded(
      child: FContainer(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Expanded(
                    child: FText(
                  title,
                  size: 24.sp,
                  color: Colors.white.withOpacity(0.6),
                )),
                FText(
                  "",
                  // "明细",
                  size: 24.sp,
                  color: Colors.white.withOpacity(0.6),
                ),
                Icon(
                  Icons.arrow_forward_ios_outlined,
                  size: 22.w,
                  color: const Color(0xFF8B7EB4),
                ),
              ],
            ),
            FContainer(
              margin: EdgeInsets.only(
                top: 50.h,
                bottom: 50.h,
              ),
              child: FText(
                '${day}',
                color: Colors.white,
                size: 32.sp,
              ),
            ),
            FContainer(
              padding: EdgeInsets.all(12.w),
              radius: BorderRadius.circular(5),
              color: const Color(0xFF221E34),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  FText(
                    '${yesterday}',
                    color: const Color(0xFFC0C0C0),
                    size: 24.sp,
                  ),
                  FText(
                    "昨日",
                    color: const Color(0xFFC0C0C0),
                    size: 24.sp,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget clubCell(String img, String name, Function() onPress) {
    Widget btn = FContainer(
      align: Alignment.center,
      child:
          Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
        LoadAssetImage(img, width: 64.w, height: 64.w),
        FText(
          name,
          size: 24.sp,
          color: const Color(0xFF9F9FB9),
        )
      ]),
    );

    return GestureDetector(onTap: onPress, child: btn);
  }

  Widget headerContent(ClubInfoProvider pro) {
    return FContainer(
      padding: EdgeInsets.all(30.w),
      child: Column(
        children: [
          Row(
            children: [
              SizedBox(
                width: 160.w,
                height: 160.w,
                child: ClipOval(
                  child: pro.postmdoel != null
                      ? LoadNetImage(
                          NetConstants.ossPath +
                              (pro.postmdoel?.roomInfo?.image ?? ""),
                          width: 160.w,
                        )
                      : LoadAssetImage(
                          "login/default",
                          width: 160.w,
                        ),
                ),
              ),
              SizedBox(
                width: 20.w,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      ConstrainedBox(
                        constraints: BoxConstraints(maxWidth: 200.w),
                        child: FText(
                          pro.postmdoel?.roomInfo?.name ?? "",
                          overflow: TextOverflow.ellipsis,
                          size: 36.sp,
                          color: const Color(0xFF8376F3),
                          weight: FontWeight.bold,
                        ),
                      ),
                      // FText(
                      //     pro.postmdoel?.roomInfo?.name ?? "",
                      //     size: 36.sp,
                      //     color: Colors.black,
                      //     weight: FontWeight.bold,
                      //   ),
                      userAge(pro.postmdoel?.sex ?? 0,
                          pro.postmdoel?.birthday ?? 0),
                    ],
                  ),
                  SizedBox(
                    height: 10.w,
                  ),
                  Row(
                    children: [
                      headerCell(
                          "ID: ${pro.postmdoel?.roomInfo?.exRoomId.toString()}",
                          ''),
                    ],
                  ),
                  SizedBox(
                    height: 15.w,
                  ),
                  headerCell(
                      "创建时间:${DateTime.fromMillisecondsSinceEpoch(pro.postmdoel?.roomInfo?.createTime ?? 0 * 1000).toString().substring(0, 19)}",
                      ""),
                ],
              ),
              const Spacer(),
              Visibility(
                  visible: pro.postmdoel?.memberType == ClubRole.VICE.index,
                  child: GestureDetector(
                    onTap: () => {},
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        GestureDetector(
                          onTap: () => onTapMore(pro.postmdoel),
                          child: FContainer(
                            child: Row(
                              children: [
                                FText("进入房间", color: Colors.black, size: 24.sp),
                                Icon(
                                  Icons.arrow_forward_ios_outlined,
                                  color: Colors.black,
                                  size: 20.w,
                                )
                              ],
                            ),
                          ),
                        ),
                        Container(
                          height: 110.w,
                        ),
                      ],
                    ),
                  )),
            ],
          ),
          SizedBox(
            height: 30.w,
          ),
        ],
      ),
    );
  }

  Widget userAge(int sex, int bir) {
    return FContainer(
      width: 78.w,
      height: 40.w,
      padding: EdgeInsets.symmetric(horizontal: 10.w),
      color: sex == 0 ? const Color(0xFFFFE5E5) : const Color(0xFFE5EFFF),
      radius: BorderRadius.circular(30.w),
      child: Row(children: [
        LoadAssetImage(sex == 0 ? "person/sex2" : "person/sex1",
            width: 20.w, height: 20.w),
        FText("${UserConstants.getUserAge(bir)}",
            size: 24.sp,
            color: sex == 0 ? const Color(0xFFFF617F) : const Color(0xFF43A0FF))
      ]),
    );
  }

  Widget headerBtnCell(String text, image) {
    return FContainer(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Visibility(
            visible: image != "",
            child: LoadAssetImage(
              image,
              width: 48.w,
              height: 48.w,
            ),
          ),
          SizedBox(
            width: 10.w,
          ),
          FText(
            text,
            size: 28.sp,
            color: Colors.black,
            weight: FontWeight.bold,
          ),
        ],
      ),
    );
  }

  Widget headerCell(String text, image) {
    return FContainer(
      color: const Color(0xFFAFFFFFF),
      margin: EdgeInsets.only(right: 8.w),
      padding: EdgeInsets.only(left: 10.w, right: 10.w, top: 8.h, bottom: 8.h),
      radius: BorderRadius.circular(50),
      child: Row(
        children: [
          Visibility(
            visible: image != "",
            child: LoadAssetImage(
              image,
              width: 20.w,
            ),
          ),
          SizedBox(
            width: 5.w,
          ),
          FText(
            text,
            color: Colors.black,
            size: 20.sp,
          ),
        ],
      ),
    );
  }

  Future<void> callRefresh() async {
    await infoProvider.requestFangClub(widget.clubId, widget.room_id);
  }

  void onTapAboutPage() {}
  // 公会签约
  void onTapClubApply(String? clubId) {
    if (clubId == null) return;
    WidgetUtils().pushPage(ClubApply(clubId: clubId));
  }

  void onTapSign(ClubMspostmdoel? model) async {
    String? inputpassword = await WidgetUtils().showFDialog(moreWord(
      guild_id: int.parse(widget.clubId),
      room_id: int.parse(widget.room_id),
      REstroom_id: widget.REstroom_id,
    ));
    if (inputpassword is String) {
      print('55555555');
      print(inputpassword);
      callRefresh();
    }
  }

  // 进入房间
  void onTapMore(ClubMspostmdoel? model) async {
    // WidgetUtils().pushPage(RoomPage(int.parse(widget.clubId)));
    AppManager().joinRoom(int.parse(widget.clubId));
  }
}
