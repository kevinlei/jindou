import 'package:flutter/material.dart';
import 'package:echo/import.dart';

import '../model/club_roomwho_mdoe.dart';

class ClubCell extends StatelessWidget {
  final ClubRoomwhoMdoe? model;
  const ClubCell({Key? key, this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget club = Row(children: [
      clubCover(),
      SizedBox(width: 20.w),
      FContainer(
        padding: EdgeInsets.only(top: 20.w, bottom: 20.w),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              clubName(),
              SizedBox(height: 5.w),
              FText("公会ID：${model?.guildId ?? ""}",
                  color: Colors.black.withOpacity(0.8), size: 24.sp),
              SizedBox(height: 5.w),
              Row(
                children: [
                  LoadAssetImage(
                    "person/roomNum",
                    width: 20.w,
                  ),
                  SizedBox(
                    width: 10.w,
                  ),
                  FText("${model?.Rooms}",
                      size: 22.sp, color: Colors.black),
                  FContainer(
                    margin: EdgeInsets.only(left: 10.w, right: 10.w),
                    width: 20.w,
                    align: Alignment.center,
                    child: FText("|",color: Colors.black,),
                  ),
                  LoadAssetImage(
                    "person/roomPersonNum",
                    width: 20.w,
                  ),
                  SizedBox(
                    width: 10.w,
                  ),
                  FText("${model?.memberCount}人",
                      size: 22.sp, color: Colors.black),
                ],
                crossAxisAlignment: CrossAxisAlignment.center,
              )
            ]),
      )
    ]);

    return GestureDetector(
      onTap: onTapClub,
      child: FContainer(
        margin: EdgeInsets.symmetric(horizontal: 25.w),
        height: 200.h,
        radius: BorderRadius.circular(20.w),
        color: Color(0xFF222222).withOpacity(0.05),
        padding: EdgeInsets.symmetric(horizontal: 20.w),
        child: Stack(children: [
          club,
          // Positioned(
          //   left: -20.w,
          //   child: LoadAssetImage("homepage/club", width: 104.w),
          // )
        ], clipBehavior: Clip.none),
      ),
    );
  }

  Widget clubCover() {
    String cover = model?.guildAvatar ?? "";
    return FContainer(
      width: 160.w,
      height: 160.w,
      radius: BorderRadius.circular(20.w),
      image: ImageUtils().getImageProvider(NetConstants.ossPath + cover),
      imageFit: BoxFit.cover,
    );
  }

  Widget clubName() {
    return ConstrainedBox(
      constraints: BoxConstraints(maxWidth: 360.w),
      child: FText(
        model?.guildName ?? "",
        weight: FontWeight.bold,
        color: Colors.black,
      ),
    );
  }

  void onTapClub() {
    if (model == null) return;
    // if (model!.clubInfo.clubStatus == 0) {
    //   return WidgetUtils().showToast("公会仍在审核");
    // }
    WidgetUtils().pushPage(ClubInfoPage(clubId: model?.guildId.toString() ?? ""));
  }
}
