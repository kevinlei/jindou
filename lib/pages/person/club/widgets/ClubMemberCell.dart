import 'package:flutter/material.dart';
import 'package:echo/import.dart';

import '../model/club_membart_model.dart';

class ClubMemberCell extends StatelessWidget {
  final int? memberStatus;
  final String clubId;
  final String ownerId;
  final ClubMembartModel model;
  final Function() onRefresh;

  const ClubMemberCell(
    this.model, {
    Key? key,
    this.memberStatus = 0,
    required this.clubId,
    required this.ownerId,
    required this.onRefresh,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FContainer(
      gradient: [
        Color(0xFF0046F5F6),
        Color(0xFFA1FFA7FB),
        Color(0xFFFF82B2).withOpacity(0.4)
      ],
      gradientBegin: Alignment.topLeft,
      gradientEnd: Alignment.bottomRight,
      margin: EdgeInsets.only(top: 10.w),
      radius: BorderRadius.circular(10.w),
      child: Row(children: [
        SizedBox(
          width: 20.w,
        ),
        GestureDetector(
          onTap: () {
            WidgetUtils().pushPage(PersonHomepage(userId: model.userId.toString()));
          },
          child: CircleAvatar(
            radius: 50.w,
            backgroundImage: ImageUtils().getImageProvider(
                NetConstants.ossPath + (model.userInfo?.headIcon ?? "")),
            backgroundColor: const Color(0xFFEEEEEE),
          ),
        ),
        SizedBox(width: 20.w),
        Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(children: [memberName(), memberIcon(), memberAge()]),
              Row(
                children: [
                  FText("ID：${model.userInfo?.displayId}",
                      size: 28.sp, color: const Color(0xFF999999)),
                  SizedBox(width: 10.w),
                  FText("总流水:${model.serialsum}",
                      size: 28.sp, color: const Color(0xFFFF753F))
                ],
              ),
            ]),
        const Spacer(),
        // memberSuffix(),

        if (model.memberType == ClubRole.VICE.index) headerCell("会长", ''),
        if (model.memberType == ClubRole.OWNER.index) headerCell("副会长", ''),
        if (model.memberType == ClubRole.FUWER.index) headerCell("管理员", ''),
        if (model.memberType == ClubRole.GUER.index) headerCell("厅长", ''),
        if (model.memberType == ClubRole.TINER.index) headerCell("财务", ''),
        SizedBox(
          width: 10.w,
        )
      ]),
    );
  }

  Widget memberName() {
    return ConstrainedBox(
      constraints: BoxConstraints(maxWidth: 200.w),
      child: FText(
        model.userInfo?.name.toString() ?? "",
        overflow: TextOverflow.ellipsis,
        size: 32.sp,
        color: Colors.black,
      ),
    );
  }

  Widget memberIcon() {
    if (model.memberType == ClubRole.OWNER.index) {
      return Padding(
        padding: EdgeInsets.only(left: 5.w),
        child: LoadAssetImage("room/i_club1", width: 78.w),
      );
    }
    return const SizedBox();
  }

  Widget memberAge() {
    return FContainer(
      width: 70.w,
      height: 30.w,
      margin: EdgeInsets.only(left: 5.w),
      padding: EdgeInsets.symmetric(horizontal: 10.w),
      color: model.userInfo?.sex == 0
          ? const Color(0xFFFFE5E5)
          : const Color(0xFFE5EFFF),
      radius: BorderRadius.circular(30.w),
      child: Row(children: [
        LoadAssetImage(model.userInfo?.sex == 0 ? "person/sex2" : "person/sex1",
            width: 20.w, height: 20.w),
        FText("${UserConstants.getUserAge(model.userInfo?.birthday ?? 0)}",
            size: 24.sp,
            color: model.userInfo?.sex == 0
                ? const Color(0xFFFF617F)
                : const Color(0xFF43A0FF))
      ]),
    );
  }

  Widget headerCell(String text, image) {
    return FContainer(
      color: Color(0xFFB57AFF).withOpacity(0.6),
      margin: EdgeInsets.only(right: 8.w),
      padding: EdgeInsets.only(left: 15.w, right: 15.w, top: 5.h, bottom: 5.h),
      radius: BorderRadius.circular(50),
      child: Row(
        children: [
          Visibility(
            visible: image != "",
            child: LoadAssetImage(
              image,
              width: 20.w,
            ),
          ),
          SizedBox(
            width: 5.w,
          ),
          FText(
            text,
            color: Color(0xFFFFFFFF),
            size: 24.sp,
          ),
        ],
      ),
    );
  }

  Widget memberSuffix() {
    if (model.userId == UserConstants.userginInfo?.info.userId) {
      return const SizedBox();
    }

    // if (memberStatus == 0) {
    //   // 1 黑名单 2 关注 3 粉丝 4 好友 5陌生人
    //   bool hasFollow = model.relation == 2 || model.relation == 4;
    //   return GestureDetector(
    //     onTap: hasFollow ? null : onTapFollow,
    //     child: FContainer(
    //       radius: BorderRadius.circular(60.w),
    //       color: hasFollow ? const Color(0xFF1F1C2E) : const Color(0xFFA852FF),
    //       width: 160.w,
    //       height: 60.w,
    //       align: Alignment.center,
    //       child: FText(hasFollow ? "已关注" : "+关注", size: 28.sp),
    //     ),
    //   );
    // }
    // if (memberStatus == 1 && model.clubRole != ClubRole.OWNER.index) {
    //   bool hasSigned = model.userStatus == InClubStatus.SIGNED.index ||
    //       model.userStatus == InClubStatus.DIS_APPLY.index ||
    //       model.userStatus == InClubStatus.QUIT_APPLY.index;
    //
    //   return GestureDetector(
    //     onTap: hasSigned ? null : onTapKick,
    //     child: FContainer(
    //       radius: BorderRadius.circular(60.w),
    //       color: hasSigned ? const Color(0xFF1F1C2E) : const Color(0xFFA852FF),
    //       width: 160.w,
    //       height: 60.w,
    //       align: Alignment.center,
    //       child: FText(hasSigned ? "已签约" : "踢出", size: 28.sp),
    //     ),
    //   );
    // }
    // if (memberStatus == 2 && model.clubRole != ClubRole.OWNER.index) {
    //   bool isRoomOwner = model.clubRole == ClubRole.VICE.index;
    //   debugPrint('厅主设置${model.clubRole}');
    //   return GestureDetector(
    //     onTap: isRoomOwner ? onTapCancel : onTapManage,
    //     child: FContainer(
    //       radius: BorderRadius.circular(60.w),
    //       color:
    //           isRoomOwner ? const Color(0xFF1F1C2E) : const Color(0xFFA852FF),
    //       width: 160.w,
    //       height: 60.w,
    //       align: Alignment.center,
    //       child: FText(
    //         isRoomOwner ? "取消厅主" : "设为厅主",
    //         size: 28.sp,
    //         color: isRoomOwner ? const Color(0xFF919191) : Colors.white,
    //       ),
    //     ),
    //   );
    // }
    return const SizedBox();
  }

  void onTapFollow() {
    DioUtils().asyncHttpRequest(
      NetConstants.followUser,
      method: DioMethod.POST,
      params: {"targetUserId": model.userId},
      onSuccess: onFollow,
    );
  }

  void onFollow(response) {
    // 1 黑名单 2 关注 3 粉丝 4 好友 5 陌生人
    onRefresh();
  }

  void onTapKick() async {
    bool? accept = await WidgetUtils().showAlert("确认踢出Ta吗？");
    if (accept == null || !accept) return;
    DioUtils().asyncHttpRequest(
      NetConstants.kickOutClub,
      method: DioMethod.POST,
      params: {"userId": model.userId},
      onSuccess: (_) => onRefresh(),
    );
  }

  void onTapManage() async {
    int? roomId =
        await WidgetUtils().showBottomSheet(ClubRoomDialog(ownerId: ownerId));
    if (roomId == null || roomId == 0) return;
    requestSetRoom(false, roomId: roomId);
  }

  void onTapCancel() async {
    bool? accept = await WidgetUtils().showAlert("确认取消Ta的厅主吗？");
    if (accept == null || !accept) return;
    requestSetRoom(true);
  }

  void requestSetRoom(bool isCancel, {int? roomId = 0}) {
    DioUtils().asyncHttpRequest(
      NetConstants.setRoomOwner,
      method: DioMethod.POST,
      params: {
        "uid": model.userId,
        "type": isCancel ? 0 : 1,
        "guildId": clubId,
        "roomId": isCancel ? 0 : roomId,
      },
      onSuccess: (_) => onRefresh(),
    );
  }
}
