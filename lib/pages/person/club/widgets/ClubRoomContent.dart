import 'package:flutter/material.dart';
import 'package:echo/import.dart';

import '../model/club_lieb_model.dart';
import '../model/club_roomst_model.dart';

class ClubRoomContent extends StatelessWidget {
  const ClubRoomContent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FContainer(
      color: const Color(0xFFFFFFFF),
      radius: BorderRadius.circular(15.w),
      margin: EdgeInsets.only(left: 30.w, right: 30.w, top: 15.w, bottom: 15.w),
      padding: EdgeInsets.all(30.w),
      child: Column(
          children: [titleContent(), SizedBox(height: 20.h), listContent()]),
    );
  }

  Widget titleContent() {
    return Row(children: [
      FText("公会房间列表",
          size: 36.sp, color: Colors.black, weight: FontWeight.bold),
      const Spacer(),
      Consumer<ClubInfoProvider>(builder: (_, pro, __) {
        return GestureDetector(
          onTap: () => onTapMore(pro.modeldel),
          child: FContainer(
            child: Row(children: [
              LoadAssetImage(
                "person/rightrooms",
                width: 40.w,
                height: 40.w,
              ),
            ]),
          ),
        );
      }),
    ]);
  }

  Widget listContent() {
    return SizedBox(
        height: 216.w,
        child: Consumer2<ClubInfoProvider, ClubRoomProvider>(
            builder: (_, pro1, pro2, __) {
          int length = pro2.roomList.length;
          int clubRole = pro1.model?.clubRole ?? ClubRole.DEF.index;
          if (clubRole == ClubRole.OWNER.index) {
            length += 1;
          }
          return ListView.builder(
            padding: EdgeInsets.zero,
            scrollDirection: Axis.horizontal,
            itemExtent: 216.w,
            itemCount: length,
            itemBuilder: (_, index) => (index < pro2.roomList.length)
                ? roomCell(pro2.roomList[index])
                : roomAdd(() {
                    pro2.requestRoom(pro2.ownerId);
                  }),
          );
        }));
  }

  Widget roomCell(ClubLiebModel model) {
    return GestureDetector(
      onTap: () async {
        // WidgetUtils().pushPage(RoomPage(model.exId));
        AppManager().joinRoom(model.exId);
      },
      child: FContainer(
        color: const Color(0xFFEDEEF3),
        margin: EdgeInsets.only(right: 20.w),
        radius: BorderRadius.circular(20.w),
        align: Alignment.bottomCenter,
        imageFit: BoxFit.cover,
        image: ImageUtils()
            .getImageProvider(NetConstants.ossPath + (model.image ?? '')),
        child: roomInfo(model.name ?? "", model.exId.toString(),
            model.topicName.toString()),
      ),
    );
  }

  Widget roomAdd(Function() call) {
    return GestureDetector(
      onTap: () => onTapCreate(call),
      child: FContainer(
        color: const Color(0xFFEDEEF3),
        margin: EdgeInsets.only(right: 20.w),
        radius: BorderRadius.circular(20.w),
        child: Icon(Icons.add, size: 80.w, color: const Color(0xFFBBBBBB)),
      ),
    );
  }

  Widget roomInfo(String name, String id, String topic_name) {
    return FContainer(
      color: Colors.transparent,
      radius: BorderRadius.only(
        bottomRight: Radius.circular(20.w),
        bottomLeft: Radius.circular(20.w),
      ),
      width: double.infinity,
      padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 10.h),
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          // mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              children: [
                headerCell(topic_name, ''),
              ],
            ),
            Expanded(child: Center()),
            FText(name, color: Colors.black, overflow: TextOverflow.ellipsis),
            FText("ID：$id", size: 22.sp, color: Colors.black)
          ]),
    );
  }

  Widget headerCell(String text, image) {
    return FContainer(
      color: Color(0xFF26000000),
      margin: EdgeInsets.only(right: 8.w),
      padding: EdgeInsets.only(left: 15.w, right: 15.w, top: 5.h, bottom: 5.h),
      radius: BorderRadius.circular(50),
      child: Row(
        children: [
          Visibility(
            visible: image != "",
            child: LoadAssetImage(
              image,
              width: 20.w,
            ),
          ),
          SizedBox(
            width: 5.w,
          ),
          FText(
            text,
            color: Colors.black,
            size: 24.sp,
          ),
        ],
      ),
    );
  }

  void onTapMore(ClubRoomstModel? model) async {
    if (model == null) return;
    WidgetUtils().pushPage(ClubRoomList(
        clubId: model.guildInfo?.guildId.toString() ?? '',
        ownerId: model.guildId.toString(),
        clubRole: model.memberType ?? 0));
  }

  void onTapCreate(Function() call) async {
    // await WidgetUtils()
    //     .pushPage(RoomCreate(EnterRoomType.ROOM_CLUB, homePageCtr));

    call();
  }
}
