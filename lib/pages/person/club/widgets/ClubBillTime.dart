import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class ClubBillTime extends StatefulWidget {
  @override
  _ClubBillTimeState createState() => _ClubBillTimeState();
}

class _ClubBillTimeState extends State<ClubBillTime> {
  final List<String> weekend = ["周一", "周二", "周三", "周四", "周五", "周六", "周日"];
  double cellSize = 84.w;
  // 当前时间
  DateTime dateNow = DateTime.now();
  // 所选的月
  DateTime dateCur = DateTime.now();
  // 开始时间
  DateTime dateStart = DateTime.now();
  // 结束时间
  DateTime dateEnd = DateTime.now();

  int clickTime = 0;

  @override
  void initState() {
    super.initState();
    dateCur = DateTime(dateNow.year, dateNow.month, dateNow.day);
    dateStart = DateTime(dateNow.year, dateNow.month, dateNow.day);
    dateEnd = DateTime(dateStart.year, dateStart.month, dateStart.day + 1);
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: FContainer(
        color: Colors.white,
        radius: BorderRadius.circular(20.w),
        padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 20.h),
        child: Column(children: [
          monthSelect(),
          weekendSelect(),
          SizedBox(height: 10.h),
          Column(children: List.generate(6, (hor) => daySelect(hor)))
        ], mainAxisSize: MainAxisSize.min),
      ),
    );
  }

  Widget monthSelect() {
    return Row(children: [
      TextButton(onPressed: null, child: FText("", size: 26.sp, nullLength: 0)),
      IconButton(
        onPressed: onTapPre,
        iconSize: 40.w,
        icon: Icon(Icons.arrow_back_ios, color: Color(0xFFBBBBBB)),
      ),
      FText("${dateCur.year}年${dateCur.month}月",
          size: 28.sp, weight: FontWeight.bold),
      IconButton(
        onPressed: onTapNext,
        iconSize: 40.w,
        icon: Icon(Icons.arrow_forward_ios, color: Color(0xFFBBBBBB)),
      ),
      TextButton(
          onPressed: onTapConfirm,
          child: FText(
            "确认",
            size: 26.sp,
            color: Colors.black,
          )),
    ], mainAxisSize: MainAxisSize.min);
  }

  Widget weekendSelect() {
    List<Widget> children = List.generate(7, (index) {
      return FContainer(
        width: cellSize,
        height: 40.w,
        align: Alignment.center,
        child: FText(weekend[index], size: 24.sp, color: Color(0xFF999999)),
      );
    });
    return Row(children: children, mainAxisSize: MainAxisSize.min);
  }

  Widget daySelect(int hor) {
    List<Widget> children =
        List.generate(7, (day) => dayCell(hor * 7 + day + 1));
    return Row(children: children, mainAxisSize: MainAxisSize.min);
  }

  Widget dayCell(int day) {
    int dayText = getDayText(day);
    int curDays = getCurMonthDays();
    Color textColor = Colors.black;
    Color backColor = Colors.transparent;
    BorderRadius radius = BorderRadius.zero;
    DateTime cellTime = DateTime(dateCur.year, dateCur.month, dayText);
    if (dayText <= 0) {
      textColor = Color(0xFFBBBBBB);
    } else if (dayText > curDays) {
      textColor = Color(0xFFBBBBBB);
    }
    if (cellTime == dateStart || cellTime == dateEnd) {
      textColor = Colors.white;
      backColor = Color(0xFFA852FF);
      radius = BorderRadius.circular(3.w);
    } else if (cellTime.isBefore(dateEnd) && cellTime.isAfter(dateStart)) {
      textColor = Color(0xFFA852FF);
      backColor = Color(0xFFA852FF).withOpacity(0.08);
    }

    return GestureDetector(
      onTap: () => onTapCell(cellTime),
      child: FContainer(
        width: cellSize,
        height: cellSize,
        radius: radius,
        align: Alignment.center,
        color: backColor,
        child: Column(children: [
          FText(
              dateStart == cellTime
                  ? "开始时间"
                  : dateEnd == cellTime
                      ? "结束时间"
                      : "",
              nullLength: 0,
              size: 14.sp),
          FText("${cellTime.day}", color: textColor, size: 28.sp),
          FText("", nullLength: 0, size: 14.sp),
        ], mainAxisAlignment: MainAxisAlignment.spaceAround),
      ),
    );
  }

  int getDayText(int day) {
    int first = 7 - (dateCur.day - dateCur.weekday) % 7 + 1;
    return day - first + 1;
  }

  // 获取本月的总天数
  int getCurMonthDays() {
    DateTime date = DateTime(dateCur.year, dateCur.month + 1, 0);
    return date.day;
  }

  void onTapPre() {
    // if (dateNow.month - dateCur.month > 0) return;
    DateTime date = DateTime(dateCur.year, dateCur.month - 1, dateCur.day);
    dateCur = date;
    if (!mounted) return;
    setState(() {});
  }

  void onTapNext() {
    // if (dateCur.month - dateNow.month > 0) return;
    DateTime date = DateTime(dateCur.year, dateCur.month + 1, dateCur.day);
    dateCur = date;
    if (!mounted) return;
    setState(() {});
  }

  void onTapCell(DateTime time) {
    if (clickTime == 0) {
      dateStart = time;
      dateEnd = time;
      clickTime = 1;
    } else if (clickTime == 1) {
      if (time == dateStart) return;
      if (time.isBefore(dateStart)) {
        dateStart = time;
        dateEnd = time;
        clickTime = 1;
      } else {
        dateEnd = time;
        clickTime = 0;
      }
    }
    if (!mounted) return;
    setState(() {});
  }

  void onTapConfirm() {
    if (dateStart == dateEnd) return WidgetUtils().showToast("请选择结束时间");
    debugPrint("开始时间$dateStart -- 结束时间$dateEnd");
    WidgetUtils().popPage([dateStart, dateEnd]);
  }
}
