import 'package:flutter/material.dart';
import 'package:echo/import.dart';

import '../model/club_roomwho_mdoe.dart';

class SelfClubContent extends StatelessWidget {
  final ClubRoomwhoMdoe? model;
  const SelfClubContent({Key? key, this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget content = FContainer(
      color: const Color(0xFFFFFFFF),
      margin: EdgeInsets.only(left: 30.w, top: 15.w, right: 30.w, bottom: 15.w),
      radius: BorderRadius.circular(15.w),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.fromLTRB(25.w, 40.h, 25.w, 20.h),
            child: FText("我的公会", weight: FontWeight.bold, size: 36.sp,color: Colors.black,),
          ),
          ClubCell(model: model),
          SizedBox(
            height: 32.w,
          ),
        ],
      ),
    );
    return Visibility(visible: model != null, child: content);
  }
}
