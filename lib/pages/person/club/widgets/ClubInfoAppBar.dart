import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class ClubInfoAppBar extends StatelessWidget {
  const ClubInfoAppBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      pinned: true,
      leading: buttonBack(),
      actions: [buttonMsg(), buttonSet()],
      expandedHeight: 580.w,
      flexibleSpace: flexibleSpace(),
    );
  }

  Widget buttonBack() {
    return IconButton(
        onPressed: onTapBack,
        icon: const Icon(Icons.arrow_back_ios, color: Colors.white));
  }

  Widget buttonMsg() {
    return Consumer<ClubInfoProvider>(builder: (_, pro, __) {
      int clubRole = pro.modeldel?.memberType ?? ClubRole.DEF.index;
      if (clubRole == ClubRole.OWNER.index) {
        return IconButton(
            onPressed: () => onTapManage(pro.modeldel?.guildId.toString()),
            icon: Stack(
                alignment: Alignment.center,
                clipBehavior: Clip.none,
                children: [
                  LoadAssetImage("club/msg", width: 38.w),
                  redPoint(pro.hasNewMsg)
                ]));
      }
      return const SizedBox();
    });
  }

  Widget redPoint(bool show) {
    return Positioned(
        top: -10.w,
        right: -10.w,
        child: Visibility(
          visible: show,
          child: CircleAvatar(radius: 10.w, backgroundColor: Colors.red),
        ));
  }

  Widget buttonSet() {
    return Consumer<ClubInfoProvider>(builder: (_, pro, __) {
      // 自己是公会会长
      int clubRole = pro.modeldel?.memberType ?? ClubRole.DEF.index;
      String clubId = pro.modeldel?.guildId.toString() ?? "";
      if (clubRole == ClubRole.DEF.index) return const SizedBox();
      int userStatus =
          pro.modeldel?.guildrelationType ?? InClubStatus.DEFAULT.index;
      if (userStatus == InClubStatus.DEFAULT.index ||
          userStatus == InClubStatus.JOIN_APPLY.index) return const SizedBox();

      if (clubRole == ClubRole.OWNER.index) {
        return IconButton(
            onPressed: () =>
                onTapSet(pro.model, () => pro.requestClubInfo(clubId)),
            icon: LoadAssetImage("club/set", width: 36.w));
      }
      return IconButton(
          onPressed: () => onTapSign(pro.model),
          icon: LoadAssetImage("club/set", width: 36.w));
    });
  }

  Widget flexibleSpace() {
    return FlexibleSpaceBar(
      background: SizedBox(
        height: 580.w,
        child: Stack(children: [
          clubInfo(),
          Positioned(left: 0, right: 0, bottom: 0, child: bottom())
        ]),
      ),
    );
  }

  Widget clubInfo() {
    return Consumer<ClubInfoProvider>(builder: (_, pro, __) {
      ImageProvider clubBg;
      ImageProvider? clubCover;
      if (pro.modeldel == null) {
        clubBg = ImageUtils().getAssetsImage("person/bg");
      } else {
        String bg = pro.modeldel?.guildInfo?.guildAvatar ?? "";
        // String cover = pro.modeldel?.clubInfo?.clubCover ?? "";
        if (bg.isEmpty) {
          clubBg = ImageUtils().getAssetsImage("person/bg");
        } else {
          clubBg = ImageUtils().getImageProvider(NetConstants.ossPath + bg);
        }
        // clubCover = ImageUtils().getImageProvider(NetConstants.ossPath + cover);
      }
      return FContainer(
        image: clubBg,
        imageFit: BoxFit.cover,
        align: Alignment.bottomCenter,
        padding: EdgeInsets.only(bottom: 80.w),
        child: Column(mainAxisSize: MainAxisSize.min, children: [
          FContainer(
            width: 174.w,
            height: 174.w,
            image: clubCover,
            imageFit: BoxFit.cover,
            radius: BorderRadius.circular(20.w),
            color: const Color(0xFFEEEEEE),
          ),
          SizedBox(height: 40.w),
          FText(pro.model?.clubInfo.clubName ?? "",
              size: 36.sp, color: Colors.white),
          SizedBox(height: 10.w),
          FText("公会ID：${pro.model?.clubInfo.clubDisplayId ?? ""}",
              size: 20.sp, color: const Color(0xFFDDDDDD))
        ]),
      );
    });
  }

  Widget bottom() {
    return FContainer(
      height: 46.w,
      color: Colors.white,
      radius: BorderRadius.only(
        topRight: Radius.circular(20.w),
        topLeft: Radius.circular(20.w),
      ),
    );
  }

  void onTapBack() {
    WidgetUtils().popPage();
  }

  void onTapManage(String? clubId) {
    if (clubId == null) return;
    WidgetUtils().pushPage(ClubApply(clubId: clubId));
  }

  void onTapSign(ClubModel? model) {
    if (model == null) return;
    WidgetUtils().pushPage(SignedMember(model: model, isOwner: false));
  }

  void onTapSet(ClubModel? model, Function() call) async {
    if (model == null) return;
    await WidgetUtils().pushPage(ClubSet(model: model));
    call();
  }
}
