import 'package:flutter/material.dart';
import 'package:echo/import.dart';

import '../model/club_membart_model.dart';
import '../model/club_roomst_model.dart';

class ClubMemberContent extends StatelessWidget {
  const ClubMemberContent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FContainer(
      color: const Color(0xFFFFFFFF),
      radius: BorderRadius.circular(15.w),
      margin: EdgeInsets.only(left: 30.w, right: 30.w, top: 0.w, bottom: 15.w),
      padding: EdgeInsets.all(30.w),
      child: Column(children: [titleContent(), listContent()]),
    );
  }

  Widget titleContent() {
    return Row(children: [
      FText("公会成员", size: 36.sp, color: Colors.black, weight: FontWeight.bold),
      const Spacer(),
      Consumer<ClubInfoProvider>(builder: (_, pro, __) {
        int clubRole = pro.modeldel?.memberType ?? ClubRole.DEF.index;
        int userStatus = pro.modeldel?.memberType ?? InClubStatus.SIGNED.index;
        // 非正式成员或者不是公会成员不能查看
        bool visible = !(clubRole == ClubRole.DEF.index ||
            userStatus == InClubStatus.SIGN_APPLY.index ||
            userStatus == InClubStatus.JOIN_APPLY.index);
        return Visibility(
          visible: visible,
          child: GestureDetector(
            onTap: () => onTapMore(pro.modeldel),
            child: FContainer(
              child: Row(children: [
                LoadAssetImage(
                  "person/rightrooms",
                  width: 40.w,
                  height: 40.w,
                ),
              ]),
            ),
          ),
        );
      })
    ]);
  }

  Widget listContent() {
    return SizedBox(
        height: 176.w,
        child: Consumer<ClubMemberProvider>(builder: (_, pro, __) {
          int length = pro.otherMember.length;
          length = length > 5 ? 5 : length;
          List<Widget> children = List.generate(
              length, (index) => memberCell(pro.otherMember[index]));
          if (length <= 4) {
            children.add(buttonAdd());
          }
          return GridView(
            padding: EdgeInsets.zero,
            physics: const NeverScrollableScrollPhysics(),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 5,
              mainAxisExtent: 176.w,
              crossAxisSpacing: 20.w,
            ),
            children: children,
          );
        }));
  }

  Widget memberCell(ClubMembartModel model) {
    return Column(children: [
      Expanded(
        child: AspectRatio(
          aspectRatio: 1.0,
          child: GestureDetector(
            onTap: () {
              WidgetUtils().pushPage(PersonHomepage(userId: model.userId.toString()));
            },
            child: CircleAvatar(
              backgroundColor: const Color(0xFFEDEEF3),
              backgroundImage: ImageUtils().getImageProvider(
                  NetConstants.ossPath + (model.userInfo?.headIcon ?? "")),
            ),
          ),
        ),
      ),
      FText(
        model.userInfo?.name ?? "",
        size: 20.sp,
        color: Color(0xCC222222),
      )
    ]);
  }

  Widget buttonAdd() {
    return Consumer<ClubInfoProvider>(builder: (_, pro, __) {
      int clubRole = pro.model?.clubRole ?? ClubRole.DEF.index;
      int userStatus = pro.model?.userStatus ?? InClubStatus.DEFAULT.index;
      // 非正式成员或者不是公会成员不能邀请
      if (clubRole == ClubRole.DEF.index ||
          userStatus == InClubStatus.DEFAULT.index ||
          userStatus == InClubStatus.JOIN_APPLY.index) {
        return const SizedBox();
      }
      return Column(children: [
        Expanded(
          child: GestureDetector(
            onTap: () => onTapShare(pro.model),
            child: const AspectRatio(
                aspectRatio: 1.0,
                child: CircleAvatar(
                  backgroundColor: Color(0xFFEDEEF3),
                  child: Icon(Icons.add, color: Colors.black),
                )),
          ),
        ),
        FText("", nullLength: 0, size: 20.sp)
      ]);
    });
  }

  void onTapMore(ClubRoomstModel? model) {
    if (model == null) return;
    WidgetUtils().pushPage(ClubMember(
        clubId: model.guildId.toString(),
        ownerId: model.guildId.toString(),
        clubRole: model.memberType ?? 0));
  }

  void onTapShare(ClubModel? model) async {
    if (model == null) return;
    Map? userData =
        await WidgetUtils().showBottomSheet(const FShareFriend(btnText: "邀请"));
    if (userData == null) return;
    String userId = userData["user"];
    String conId = userData["conId"];
    String head = userData["head"];
    String name = userData["name"];
    if (userId == UserConstants.userginInfo?.info.userId) return;
    HxMsgExt msgExt = HxMsgExt(userId, head, name);
    String clubId = model.clubInfo.clubId;
    String displayId = model.clubInfo.clubDisplayId;
    String clubName = model.clubInfo.clubName;
    String clubCover = model.clubInfo.clubCover;
    // AppManager().sendHxChatMsg(
    //     conId, msgExt, HXClubMsg(clubId, displayId, clubName, clubCover));
  }
}
