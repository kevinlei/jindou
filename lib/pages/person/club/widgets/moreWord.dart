// 房间详情弹窗
import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:flutter/services.dart';

class moreWord extends StatefulWidget {

  final int guild_id;
  final int room_id;
  final int REstroom_id;//设置厅长房间ID


  moreWord({required this.guild_id, super.key, required this.room_id,required this.REstroom_id});


  @override
  State<moreWord> createState() => _moreWordState();
}

class _moreWordState extends State<moreWord> {
  String textValue = "";
  String name = "";
  String user_id = "";
  int member_type=0;

  @override
  void initState() {
    super.initState();

  }


  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: [
        FContainer(
          padding: EdgeInsets.all(40.w),
          width: 650.w,
          height: 520.w,
          radius: BorderRadius.circular(20.w),
          color: Colors.white,
          child: Column(
            children: [
              FText(
                "厅长设置",
                size: 34.sp,
                color: Colors.black,
              ),
              SizedBox(height: 10.w),
              Expanded(
                child: Container(
                  child: Column(
                    children: [
                      SizedBox(height: 20.w),
                      Row(
                          children: [FText('   ID',color: Colors.black,) ,Expanded(child: inputField())]),
                      SizedBox(height: 30.w),
                      Row(
                          children: [FText('昵称',color: Colors.black,) ,Expanded(child: inputWenField())]),
                      SizedBox(height: 20.w),
                      Visibility(
                        visible: member_type == 0 ,
                        child:  FText('非本公会成员，无法设置厅长',color: Color(0xFFF03737),),

                      ),

                    ],
                  ),
                ),
              ),
              Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [GestureDetector(
                    onTap: () => WidgetUtils().popPage(textValue),
                    child: FContainer(
                      width: 210.w,
                      height: 80.w,
                      border: Border.all(color: const Color(0xFFFF753F)),
                      radius: BorderRadius.circular(80.r),
                      align: Alignment.center,
                      child: FText( "取消" ,
                          color: const Color(0xFFFF753F), size: 32.sp),
                    ),
                  ), GestureDetector(
                 onTap: () =>{
                  if (name.length==0 ){
                    WidgetUtils().showToast("请输入ID"),
                  }else{
                    DioUtils().asyncHttpRequest(
                      NetConstants.ownerguild,
                      method: DioMethod.POST,
                      params: {"guild_id": int.parse(widget.room_id.toString()) ,"room_owner_id":user_id ,"room_id":widget.REstroom_id},
                      onSuccess: onPusetModel,
                    ),
                  }

                 },
                  child: FContainer(
                  width: 210.w,
                  height: 80.w,
                  gradientBegin: Alignment.topLeft,
                  gradientEnd: Alignment.bottomRight,
                  gradient: const [Color(0xFFFF753F), Color(0xFFFF753F), Color(0xFFFF753F)],
                  radius: BorderRadius.circular(80.w),
                  align: Alignment.center,
                  child: FText( "确定" ,
                      color: Colors.white, size: 32.sp),
                ),
              )])
            ],
          ),
        )
      ],
    );
  }

  Widget inputWenField() {
    return FContainer(
      height: 88.w,
      radius: BorderRadius.circular(88.w),
      margin: EdgeInsets.symmetric(horizontal: 15.w),
      color: name.length ==0 ? const Color(0xFFFF8CC7).withOpacity(0.2) :const Color(0xFFFF8CC7),
      padding: EdgeInsets.symmetric(horizontal: 25.w),
      child: Row(
          children: [Expanded(child: Container()), FText(name.length ==0 ? "输入ID自动获取" : name,color:name.length ==0 ? Color(0xFFFF8CC7).withOpacity(0.2):Color(0xFFFFFFFF),size: 30.sp,align:TextAlign.center),Expanded(child: Container()),]),
    );
  }





  Widget inputField() {
    return FContainer(
      height: 88.w,
      radius: BorderRadius.circular(88.w),
      margin: EdgeInsets.symmetric(horizontal: 15.w),
      color: const Color(0xFF222222).withOpacity(0.05),
      padding: EdgeInsets.symmetric(horizontal: 25.w),
      child: Row(
          children: [Expanded(child: textField())]),
    );
  }

  Widget textField() {
    InputBorder border = const OutlineInputBorder(
        borderSide: BorderSide(color: Colors.transparent));

    return TextField(
      maxLength: 8,
      style: TextStyle(color: Colors.black, fontSize: 30.sp),
      inputFormatters: [FilteringTextInputFormatter.digitsOnly],
      // onChanged: onTextInput,
      onChanged: (vaue){
        setState(() {
          textValue=vaue;
          if(textValue.length ==8)
          DioUtils().asyncHttpRequest(
            NetConstants.ownersearch,
            method: DioMethod.POST,
            params: {"guild_id": widget.guild_id,"display_id":int.parse(textValue)},
            onSuccess: onApplyJoin,
          );
        });
      },
      decoration: InputDecoration(
        focusedBorder: border,
        enabledBorder: border,
        counterText: "",
        contentPadding: EdgeInsets.zero,
        hintText: "输入ID",
        hintStyle: TextStyle(color: const Color(0xFFBBBBBB), fontSize: 30.sp),
      ),
    );
  }

  void onApplyJoin(response) {
    if (response == null) return;
    name = response['name'];
    user_id=response['user_id'];
    member_type=response['member_type'];
    setState(() {

    });


  }
  void onPusetModel(response) {
    WidgetUtils().popPage(textValue);


  }



}