import 'package:flutter/material.dart';
import 'package:echo/import.dart';

import '../model/club_member_model.dart';

class ClubApplyList extends StatefulWidget {
  final ClubApplyType applyType;
  final String clubId;

  const ClubApplyList(this.applyType, {Key? key, required this.clubId})
      : super(key: key);

  @override
  _ClubApplyListState createState() => _ClubApplyListState();
}

class _ClubApplyListState extends State<ClubApplyList> {
  List<ClubMemberModel> applyList = [];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FRefreshLayout(
      onRefresh: requestList,
      emptyWidget: applyList.isEmpty ? FEmptyWidget() : null,
      child: ListView.builder(
        padding: EdgeInsets.only(top: 20.h),
        itemBuilder: (_, index) => applyCell(applyList[index]),
        itemCount: applyList.length,
        itemExtent: 180.w,
      ),
    );
  }

  Widget applyCell(ClubMemberModel model) {
    Widget userInfo = Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(children: [
            memberName(model.userInfo.name),
            SizedBox(width: 5.w),
            memberAge(model.userInfo.sex, model.userInfo.birthday)
          ]),
          FText("ID：${model.userInfo.displayId.toString()}",
              size: 28.sp, color: const Color(0xFF999999)),
        ]);

    Widget content = FContainer(
      height: 180.w,
      margin: EdgeInsets.all(10.w),
      gradient: [
        Color(0xFF0046F5F6),
        Color(0xFFA1FFA7FB),
        Color(0xFFFF82B2).withOpacity(0.4)
      ],
      gradientBegin: Alignment.topLeft,
      gradientEnd: Alignment.bottomRight,
      radius: BorderRadius.circular(20.w),
      //   radius: BorderRadius.circular(5.w),
      padding: EdgeInsets.symmetric(horizontal: 25.w),
      // border: Border(bottom: BorderSide(color: Color(0xFFEEEEEE))),
      child: Row(children: [
        memberHead(model.userInfo.headIcon),
        SizedBox(width: 20.w),
        userInfo,
        Spacer(),
        agreeAction(model),
      ]),
    );

    return Slidable(
      endActionPane: ActionPane(
        extentRatio: 0.2,
        motion: const ScrollMotion(),
        children: [
          refuseAction(model),
        ],
      ),
      child: content,
    );
  }

  CustomSlidableAction refuseAction(ClubMemberModel model) {
    return CustomSlidableAction(
      onPressed: (_) => onTapRefuse(model.userId),
      backgroundColor: Colors.transparent,
      child: FContainer(
        width: 120.w,
        height: 128.w,
        color: Color(0xFFFF753F),
        radius: BorderRadius.circular(20.w),
        padding: EdgeInsets.all(10.w),
        align: Alignment.center,
        child: FText("拒绝",
            align: TextAlign.center, size: 24.sp, color: Color(0xFFFFFFFF)),
      ),
    );
  }

  Widget agreeAction(ClubMemberModel model) {
    return GestureDetector(
      child: FContainer(
        width: 96.w,
        height: 56.w,
        gradient: [Color(0xFF46F5F6), Color(0xFFFFA7FB), Color(0xFFFF82B2)],
        gradientBegin: Alignment.topLeft,
        gradientEnd: Alignment.bottomRight,
        padding: EdgeInsets.all(10.w),
        radius: BorderRadius.circular(56.w),
        child: FText(
          "同意",
          align: TextAlign.center,
          size: 24.sp,
        ),
      ),
      onTap: () => onTapAgree(model.userId),
    );
  }

  Widget memberHead(String head) {
    return CircleAvatar(
      radius: 50.w,
      backgroundImage:
          ImageUtils().getImageProvider(NetConstants.ossPath + head),
      backgroundColor: Color(0xFFEEEEEE),
    );
  }

  Widget memberName(String name) {
    return ConstrainedBox(
      constraints: BoxConstraints(maxWidth: 200.w),
      child: FText(name, overflow: TextOverflow.ellipsis, size: 32.sp),
    );
  }

  Widget memberAge(int sex, int bir) {
    return FContainer(
      width: 70.w,
      height: 30.w,
      padding: EdgeInsets.symmetric(horizontal: 10.w),
      color: sex == 0 ? Color(0xFFFFE5E5) : Color(0xFFE5EFFF),
      radius: BorderRadius.circular(30.w),
      child: Row(children: [
        LoadAssetImage(sex == 0 ? "person/sex2" : "person/sex1",
            width: 20.w, height: 20.w),
        FText("${UserConstants.getUserAge(bir)}",
            size: 24.sp,
            color: sex == 0 ? Color(0xFFFF617F) : Color(0xFF43A0FF))
      ]),
    );
  }

  Widget timeText(int start, int end) {
    String startTime =
        DateTime.fromMillisecondsSinceEpoch(start).toString().substring(0, 10);
    String endTime =
        DateTime.fromMillisecondsSinceEpoch(end).toString().substring(0, 10);
    return FText("申请签约时间：$startTime - $endTime",
        size: 24.sp, color: Color(0xFFBBBBBB));
  }

  void onTapAgree(String userId) {
    requestAgree(userId, true);
  }

  void onTapRefuse(String userId) {
    requestAgree(userId, false);
  }

  void requestAgree(String userId, bool agree) {
    int accept = agree ? 1 : 2;
    String requestUrl = "";
    switch (widget.applyType) {
      case ClubApplyType.APPLY_ENTER:
        requestUrl = NetConstants.agreeJoin;
        break;
      case ClubApplyType.APPLY_SIGN:
        requestUrl = NetConstants.agreeSign;
        break;
      case ClubApplyType.APPLY_UN_SIGN:
        requestUrl = NetConstants.agreeUnSign;
        break;
      case ClubApplyType.APPLY_QUIT:
        requestUrl = NetConstants.agreeQuit;
        break;
    }
    DioUtils().asyncHttpRequest(
      requestUrl,
      method: DioMethod.POST,
      params: {
        "guild_id": int.parse(widget.clubId),
        "user_id": userId,
        "check_type": accept
      },
      onSuccess: (_) => onAgree(userId),
    );
  }

  Future<void> requestList() async {
    String requestUrl = "";
    switch (widget.applyType) {
      case ClubApplyType.APPLY_ENTER:
        requestUrl = NetConstants.joinList;
        break;
      case ClubApplyType.APPLY_SIGN:
        requestUrl = NetConstants.signList;
        break;
      case ClubApplyType.APPLY_UN_SIGN:
        requestUrl = NetConstants.unSignList;
        break;
      case ClubApplyType.APPLY_QUIT:
        requestUrl = NetConstants.quitList;
        break;
    }

    DioUtils().asyncHttpRequest(
      requestUrl,
      method: DioMethod.POST,
      params: {
        "guild_id": int.parse(widget.clubId),
        "page": 1,
        "page_size": 100
      },
      onSuccess: onRefresh,
    );
  }

  void onRefresh(response) {
    response ??= [];
    applyList.clear();
    for (var data in response) {
      applyList.add(ClubMemberModel.fromJson(data));
    }
    if (!mounted) return;
    setState(() {});
  }

  void onAgree(String userId) {
    applyList.removeWhere((e) => e.userId == userId);
    if (!mounted) return;
    setState(() {});
  }
}
