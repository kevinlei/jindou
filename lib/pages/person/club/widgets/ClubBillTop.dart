import 'package:flutter/material.dart';
import 'package:echo/import.dart';

import '../model/club_lieb_model.dart';

class ClubBillTop extends StatelessWidget {
  final ClubModel? clubModel;
  final ClubLiebModel? roomModel;

  const ClubBillTop({Key? key, this.clubModel, this.roomModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FContainer(
      margin: EdgeInsets.only(top: 20.h),
      padding: EdgeInsets.symmetric(horizontal: 25.w),
      height: 160.w,
      child: Row(children: [
        coverWidget(),
        SizedBox(width: 20.w),
        infoContent(),
        Spacer(),
        totalWidget()
      ]),
    );
  }

  Widget infoContent() {
    String name = "", id = "", count = "";
    if (clubModel != null) {
      name = clubModel!.clubInfo.clubName;
      id = "公会ID：" + clubModel!.clubInfo.clubDisplayId;
      count = "${clubModel!.clubInfo.memberCount}";
    } else if (roomModel != null) {
      name = roomModel?.name ?? "";
      id = "房间ID：" + (roomModel?.exId.toString() ?? '');
      count = "${roomModel?.personNum}";
    }

    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ConstrainedBox(
              constraints: BoxConstraints(maxWidth: 300.w), child: FText(name)),
          SizedBox(height: 5.w),
          FText(id, color: Color(0xFF999999), size: 20.sp),
          SizedBox(height: 5.w),
          Row(children: [
            LoadAssetImage("person/postrut", width: 20.w),
            SizedBox(width: 5.w),
            FText("$count", size: 20.sp, color: Color(0xFF999999)),
          ])
        ]);
  }

  Widget coverWidget() {
    ImageProvider? coverImage;
    if (clubModel != null) {
      coverImage = ImageUtils().getImageProvider(
          NetConstants.ossPath + clubModel!.clubInfo.clubCover);
    } else if (roomModel != null) {
      coverImage = ImageUtils()
          .getImageProvider(NetConstants.ossPath + (roomModel?.image ?? ''));
    }
    return FContainer(
      width: 160.w,
      height: 160.w,
      radius: BorderRadius.circular(20.w),
      color: Color(0xFFBBBBBB),
      image: coverImage,
      imageFit: BoxFit.cover,
    );
  }

  Widget totalWidget() {
    return SizedBox(
      width: 200.w,
      child: Column(children: [
        LoadAssetImage("person/roomPersonNum", width: 86.w, height: 106.w),
        Consumer<ClubBillProvider>(builder: (_, pro, __) {
          return FText("${pro.all}", color: Color(0xFFD6AC6A), size: 32.sp);
        })
      ], mainAxisAlignment: MainAxisAlignment.center),
    );
  }
}
