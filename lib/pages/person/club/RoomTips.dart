import 'dart:io';

import 'package:echo/import.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';

class RoomInputName extends StatefulWidget {
  final String origin;
  final Function(String) onNameInput;
  final int? maxLen;
  const RoomInputName(this.origin, this.onNameInput, {Key? key, this.maxLen})
      : super(key: key);

  @override
  _RoomInputNameState createState() => _RoomInputNameState();
}

class _RoomInputNameState extends State<RoomInputName> {
  int count = 0;
  TextEditingController controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    controller.text = widget.origin;
    count = widget.origin.length;
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      margin: EdgeInsets.fromLTRB(0, 30.w, 0, 40.w),
      height: 75.w,
      padding: EdgeInsets.symmetric(horizontal: 20.w),
      radius: BorderRadius.circular(20.w),
      color: const Color(0xFF2A2D3E),
      child: Row(children: [Expanded(child: inputField()), countText()]),
    );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  Widget inputField() {
    InputBorder border = const OutlineInputBorder(
        borderSide: BorderSide(color: Colors.transparent));
    return TextField(
      maxLength: widget.maxLen ?? 10,
      onChanged: onTextInput,
      controller: controller,
      style: TextStyle(color: Colors.white, fontSize: 28.sp),
      // inputFormatters: [FilteringTextInputFormatter.deny(RegExp(r"[\s]"))],
      decoration: InputDecoration(
        enabledBorder: border,
        focusedBorder: border,
        hintText: "",
        contentPadding: EdgeInsets.zero,
        hintStyle: TextStyle(color: const Color(0xFF999999), fontSize: 28.sp),
        counterText: "",
      ),
    );
  }

  Widget countText() {
    return FRichText([
      FRichTextItem()
        ..text = "$count"
        ..color = const Color(0xFFBBBBBB),
      FRichTextItem()
        ..text = "/${widget.maxLen ?? 10}"
        ..color = const Color(0xFFBBBBBB),
    ], fontSize: 26.sp);
  }

  void onTextInput(String value) {
    count = value.length;
    widget.onNameInput(value);
    if (!mounted) return;
    setState(() {});
  }
}

class RoomCover extends StatefulWidget {
  final String? origin;
  final Function(String) onSelectCover;

  const RoomCover(this.onSelectCover, {Key? key, this.origin})
      : super(key: key);

  @override
  _RoomCoverState createState() => _RoomCoverState();
}

class _RoomCoverState extends State<RoomCover> {
  File? imageFile;

  @override
  Widget build(BuildContext context) {
    ImageProvider? imageProvider;
    if (imageFile != null) {
      imageProvider = FileImage(imageFile!);
    } else if (widget.origin != null && widget.origin!.isNotEmpty) {
      imageProvider =
          ImageUtils().getImageProvider(NetConstants.ossPath + widget.origin!);
    }
    return Align(
      alignment: Alignment.centerLeft,
      child: GestureDetector(
        onTap: onPressCover,
        child: FContainer(
          width: 200.w,
          height: 200.w,
          radius: BorderRadius.circular(30.w),
          align: Alignment.center,
          color: const Color(0xFFEDEEF3),
          image: imageProvider,
          imageFit: BoxFit.cover,
          imageAlign: Alignment.center,
          margin: EdgeInsets.symmetric(vertical: 30.w),
          child: imageFile == null
              ? const Icon(Icons.add, color: Colors.white)
              : null,
        ),
      ),
    );
  }

  final ImagePicker picker = ImagePicker();

  void onPressCover() async {
    SystemChannels.textInput.invokeMethod('TextInput.hide');
    bool accept = await FuncUtils().requestPermission(Permission.photos);
    if (!accept) return;

    XFile? file = await picker.pickImage(source: ImageSource.gallery);
    if (file == null || file.path.isEmpty) return;
    CroppedFile? crop = await ImageCropper().cropImage(
      sourcePath: file.path,
      compressQuality: 100,
      uiSettings: [
        AndroidUiSettings(toolbarTitle: '裁剪', statusBarColor: Colors.white),
        IOSUiSettings(title: '裁剪')
      ],
    );
    if (crop == null || crop.path.isEmpty) return;

    imageFile = File(crop.path);
    widget.onSelectCover(imageFile!.path);
    if (!mounted) return;
    setState(() {});
  }
}

class RoomTips extends StatefulWidget {
  final EnterRoomType roomType;
  final Function(int) onSelect;
  final int? origin;
  const RoomTips(this.roomType, this.onSelect, {Key? key, this.origin})
      : super(key: key);

  @override
  _RoomTipsState createState() => _RoomTipsState();
}

class _RoomTipsState extends State<RoomTips> {
  List tips = [];
  int curIndex = -1;

  @override
  void initState() {
    super.initState();
    requestRoomTips();
  }

  @override
  Widget build(BuildContext context) {
    return Wrap(
      spacing: 20.w,
      runSpacing: 20.w,
      children: List.generate(tips.length, (index) => tipsCell(index)),
    );
  }

  Widget tipsCell(int index) {
    return GestureDetector(
      onTap: () => onPressTips(index),
      child: FContainer(
        height: 70.w,
        constraints: BoxConstraints(minWidth: 150.w),
        padding: EdgeInsets.symmetric(horizontal: 50.w),
        border: Border.all(
            color: index == curIndex
                ? const Color(0xFFFF753F)
                : const Color(0xFF260000)),
        color: index == curIndex
            ? const Color(0xFFFF753F)
            : const Color(0xFFF5F6F7),
        radius: BorderRadius.circular(70.w),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: [
              FText(
                tips[index]["topic_name"],
                size: 26.sp,
                color: index == curIndex
                    ? const Color(0xFFFFFFFF)
                    : const Color(0xFF252734),
              )
            ]),
      ),
    );
  }

  void onPressTips(int index) {
    if (curIndex == index) return;
    curIndex = index;
    widget.onSelect(tips[index]["topic_id"]);
    if (!mounted) return;
    setState(() {});
  }

  void requestRoomTips() {
    DioUtils().asyncHttpRequest(
      NetConstants.getelation,
      loading: false,
      method: DioMethod.GET,
      params: {'pid': 21},
      onSuccess: updateRoomTips,
    );
  }

  void updateRoomTips(response) {
    if (response == null) return;
    tips = response;
    curIndex =
        tips.indexWhere((element) => element["topic_id"] == widget.origin);
    curIndex = curIndex < 0 ? 0 : curIndex;
    widget.onSelect(tips[curIndex]["topic_id"]);
    if (!mounted) return;
    setState(() {});
  }
}

class RoomOpen extends StatefulWidget {
  final Function(bool) onSelect;
  final Function(String) onInput;
  const RoomOpen(this.onSelect, this.onInput, {Key? key}) : super(key: key);

  @override
  _RoomOpenState createState() => _RoomOpenState();
}

class _RoomOpenState extends State<RoomOpen> {
  bool isOpen = true;

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Row(children: [openCell(true), SizedBox(width: 20.w), openCell(false)]),
      SizedBox(height: 60.w),
      Visibility(
          child: FText(
            "密码设置",
            size: 32.sp,
            color: Colors.black,
          ),
          visible: !isOpen),
      SizedBox(height: 20.w),
      Visibility(child: FCodeInput(widget.onInput), visible: !isOpen),
    ]);
  }

  Widget openCell(bool open) {
    return GestureDetector(
      onTap: () => onPressCell(open),
      child: FContainer(
        width: 180.w,
        height: 70.w,
        align: Alignment.center,
        border: Border.all(
            color: isOpen == open
                ? const Color(0xFFFF753F)
                : const Color(0xFF26000000)),
        color:
            isOpen == open ? const Color(0xFFFF753F) : const Color(0xFFF5F6F7),
        radius: BorderRadius.circular(70.w),
        child: FText(
          open ? "公开房间" : "密码房间",
          size: 26.sp,
          color: isOpen == open
              ? const Color(0xFFFFFFFF)
              : const Color(0xFF252734),
        ),
      ),
    );
  }

  void onPressCell(bool open) {
    if (isOpen == open) return;
    isOpen = open;
    widget.onSelect(open);
    if (!mounted) return;
    setState(() {});
  }
}
