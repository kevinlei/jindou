import 'package:flutter/material.dart';
import 'package:echo/import.dart';

import 'model/club_member_model.dart';

class SignedMember extends StatefulWidget {
  final ClubModel model;
  final bool isOwner;
  const SignedMember({Key? key, required this.model, required this.isOwner})
      : super(key: key);

  @override
  _SignedMemberState createState() => _SignedMemberState();
}

class _SignedMemberState extends State<SignedMember> {
  List<ClubMemberModel> memberList = [];
  int curPage = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const CommonAppBar(
        title: "签约列表",
        textColor: Colors.white,
        elevation: 1,
        backgroundColor: Color(0xFF221E34),
      ),
      backgroundColor: const Color(0xFF221E34),
      body: FRefreshLayout(
        onRefresh: callRefresh,
        onLoad: callLoad,
        emptyWidget: memberList.isEmpty ? const FEmptyWidget() : null,
        child: ListView.builder(
          padding: EdgeInsets.fromLTRB(25.w, 20.h, 25.w, 0),
          itemExtent: 162.w,
          itemBuilder: (_, index) => memberCell(index),
          itemCount: memberList.length,
        ),
      ),
    );
  }

  Widget memberCell(int index) {
    return FContainer(
      child: Row(children: [
        GestureDetector(
          onTap: () {
            WidgetUtils().pushPage(PersonHomepage(userId: memberList[index].userId));
          },
          child: FContainer(
            width: 100.w,
            height: 100.w,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(50.w),
              child: LoadNetImage(
                  NetConstants.ossPath + memberList[index].userInfo.headIcon),
            ),
          ),
          // CircleAvatar(
          //   radius: 50.w,
          //   backgroundImage: ImageUtils().getImageProvider(
          //       NetConstants.ossPath + memberList[index].userHead),
          //   backgroundColor: Color(0xFFEEEEEE),
          // ),
        ),
        SizedBox(width: 20.w),
        Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(children: [
                memberName(index),
                memberIcon(index),
                memberAge(index)
              ]),
              FText("ID：${memberList[index].userInfo.displayId}",
                  size: 28.sp, color: const Color(0xFF999999))
            ]),
        const Spacer(),
      ]),
    );
  }

  Widget memberName(int index) {
    return ConstrainedBox(
      constraints: BoxConstraints(maxWidth: 200.w),
      child: FText(memberList[index].userInfo.name,
          overflow: TextOverflow.ellipsis, size: 32.sp),
    );
  }

  Widget memberIcon(int index) {
    if (memberList[index].applyType == ClubRole.OWNER.index) {
      return Padding(
        padding: EdgeInsets.only(left: 5.w),
        child: LoadAssetImage("room/i_club1", width: 78.w),
      );
    }
    return const SizedBox();
  }

  Widget memberAge(int index) {
    return FContainer(
      width: 70.w,
      height: 30.w,
      margin: EdgeInsets.only(left: 5.w),
      padding: EdgeInsets.symmetric(horizontal: 10.w),
      color: memberList[index].userInfo.sex == 0
          ? const Color(0xFFFFE5E5)
          : const Color(0xFFE5EFFF),
      radius: BorderRadius.circular(30.w),
      child: Row(children: [
        LoadAssetImage(
            memberList[index].userInfo.sex == 0 ? "person/sex2" : "person/sex1",
            width: 20.w,
            height: 20.w),
        FText(
            "${UserConstants.getUserAge(memberList[index].userInfo.birthday)}",
            size: 24.sp,
            color: memberList[index].userInfo.sex == 0
                ? const Color(0xFFFF617F)
                : const Color(0xFF43A0FF))
      ]),
    );
  }

  void onTapSign() async {
    await WidgetUtils().pushPage(ClubSign(model: widget.model));
    callRefresh();
  }

  Future<void> callRefresh() async {
    curPage = 1;
    requestSignList(true);
  }

  Future<void> callLoad() async {
    curPage += 1;
    requestSignList(false);
  }

  void requestSignList(bool isRefresh) {
    DioUtils().asyncHttpRequest(
      NetConstants.signMember,
      params: {"guildId": widget.model.clubInfo.clubId, "page": curPage},
      onSuccess: isRefresh ? onRefresh : onLoad,
    );
  }

  void onRefresh(response) {
    memberList.clear();
    response ??= [];
    for (var data in response) {
      memberList.add(ClubMemberModel.fromJson(data));
    }
    if (!mounted) return;
    setState(() {});
  }

  void onLoad(response) {
    response ??= [];
    for (var data in response) {
      memberList.add(ClubMemberModel.fromJson(data));
    }
    if (!mounted) return;
    setState(() {});
  }
}
