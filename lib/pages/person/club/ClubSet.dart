import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';

class ClubSet extends StatefulWidget {
  final ClubModel model;

  const ClubSet({Key? key, required this.model}) : super(key: key);

  @override
  _ClubSetState createState() => _ClubSetState();
}

class _ClubSetState extends State<ClubSet> {
  late ClubModel model;

  @override
  void initState() {
    super.initState();
    model = widget.model;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFF1F2234),
      appBar: const CommonAppBar(
        title: "公会设置",
        textColor: Colors.white,
        elevation: 1,
        backgroundColor: Color(0xFF1F2234),
      ),
      body: ListView(children: [
        FContainer(
          margin: EdgeInsets.fromLTRB(25.w, 40.h, 25.w, 20.h),
          color: const Color(0xFF2C2F40),
          padding: EdgeInsets.all(20.w),
          radius: BorderRadius.circular(20.w),
          child: Column(children: [
            nameContent(),
            Divider(color: Colors.white.withOpacity(0.4)),
            coverContent(),
            // Divider(color: Colors.white.withOpacity(0.4)),
            //  bgContent(),
          ]),
        ),
        buttonList("公会简介", onTapDes),
        buttonList("签约列表", onTapSignList),
        buttonList("公会流水", onTapBill),
      ]),
    );
  }

  Widget coverContent() {
    Widget btn = FContainer(
      child: Row(children: [
        FText("公会封面", size: 32.sp),
        const Spacer(),
        FContainer(
          width: 120.w,
          height: 120.w,
          image: ImageUtils().getImageProvider(
              NetConstants.ossPath + model.clubInfo.clubCover),
          imageFit: BoxFit.cover,
          radius: BorderRadius.circular(20.w),
          color: const Color(0xFFEEEEEE),
        ),
        SizedBox(width: 40.w),
        Icon(Icons.arrow_forward_ios,
            color: const Color(0xFFEEEEEE), size: 32.w)
      ]),
    );
    return GestureDetector(onTap: onTapCover, child: btn);
  }

  Widget bgContent() {
    String imageBg = model.clubInfo.clubBg ?? "";
    ImageProvider clubBg;
    if (imageBg.isEmpty) {
      clubBg = ImageUtils().getAssetsImage("person/bg");
    } else {
      clubBg = ImageUtils().getImageProvider(NetConstants.ossPath + imageBg);
    }

    Widget btn = FContainer(
      child: Row(children: [
        FText("公会背景", size: 32.sp),
        const Spacer(),
        FContainer(
          width: 120.w,
          height: 120.w,
          imageFit: BoxFit.cover,
          image: clubBg,
          radius: BorderRadius.circular(20.w),
          color: const Color(0xFFEEEEEE),
        ),
        SizedBox(width: 40.w),
        Icon(Icons.arrow_forward_ios,
            color: const Color(0xFFEEEEEE), size: 32.w)
      ]),
    );
    return GestureDetector(onTap: onTapBg, child: btn);
  }

  Widget nameContent() {
    Widget btn = FContainer(
      padding: EdgeInsets.only(top: 30.h),
      child: Row(children: [
        FText("公会名称", size: 32.sp),
        const Spacer(),
        FText(model.clubInfo.clubName,
            size: 26.sp, color: const Color(0xFF666666)),
        SizedBox(width: 40.w),
        Icon(Icons.arrow_forward_ios,
            color: const Color(0xFFEEEEEE), size: 32.w)
      ]),
    );
    return GestureDetector(onTap: onTapName, child: btn);
  }

  Widget buttonList(String title, Function() onTap) {
    Widget btn = FContainer(
      height: 96.w,
      radius: BorderRadius.circular(20.w),
      color: const Color(0xFF2C2F40),
      margin: EdgeInsets.fromLTRB(25.w, 0, 25.w, 20.h),
      padding: EdgeInsets.symmetric(horizontal: 25.w),
      child: Row(children: [
        FText(title, size: 32.sp),
        const Spacer(),
        Icon(Icons.arrow_forward_ios,
            color: const Color(0xFFEEEEEE), size: 32.w)
      ]),
    );
    return GestureDetector(onTap: onTap, child: btn);
  }

  final ImagePicker picker = ImagePicker();

  void onTapCover() async {
    bool accept = await FuncUtils().requestPermission(Permission.photos);
    if (!accept) return;

    XFile? file = await picker.pickImage(source: ImageSource.gallery);
    if (file == null || file.path.isEmpty) return;
    CroppedFile? crop = await ImageCropper().cropImage(
      sourcePath: file.path,
      compressQuality: 100,
      uiSettings: [
        AndroidUiSettings(toolbarTitle: '裁剪', statusBarColor: Colors.white),
        IOSUiSettings(title: '裁剪')
      ],
    );
    if (crop == null || crop.path.isEmpty) return;

    WidgetUtils().showLoading();
    String? imageUrl =
        await FuncUtils().requestUpload(crop.path, UploadType.CLUB_HEADIMG);
    if (imageUrl == null) return WidgetUtils().cancelLoading();
    if (imageUrl.isEmpty) {
      WidgetUtils().showToast("上传封面失败");
      WidgetUtils().cancelLoading();
      return;
    }
    // bool lawful = await FuncUtils().checkImageLawful(
    //     [imageUrl], CheckImgType.IMG_GUILD,
    //     guildId: model.clubInfo.clubId);
    // if (!lawful) return WidgetUtils().cancelLoading();

    await requestModify("headpicture", imageUrl, (_) {
      model.clubInfo.clubCover = imageUrl;
      if (!mounted) return;
      setState(() {});
    });
    WidgetUtils().cancelLoading();
  }

  void onTapBg() async {
    bool accept = await FuncUtils().requestPermission(Permission.photos);
    if (!accept) return;

    XFile? file = await picker.pickImage(source: ImageSource.gallery);
    if (file == null || file.path.isEmpty) return;
    CroppedFile? crop = await ImageCropper().cropImage(
      sourcePath: file.path,
      compressQuality: 100,
      uiSettings: [
        AndroidUiSettings(toolbarTitle: '裁剪', statusBarColor: Colors.white),
        IOSUiSettings(title: '裁剪')
      ],
    );
    if (crop == null || crop.path.isEmpty) return;

    WidgetUtils().showLoading();
    String? imageUrl =
        await FuncUtils().requestUpload(crop.path, UploadType.CLUB_COVER);
    if (imageUrl == null) return WidgetUtils().cancelLoading();
    if (imageUrl.isEmpty) {
      WidgetUtils().showToast("上传背景失败");
      WidgetUtils().cancelLoading();
      return;
    }

    // bool lawful =
    //     await FuncUtils().checkImageLawful([imageUrl], CheckImgType.IMG_GUILD);
    // if (!lawful) return WidgetUtils().cancelLoading();

    await requestModify("backgroundPicture", imageUrl, (_) {
      model.clubInfo.clubBg = imageUrl;
      if (!mounted) return;
      setState(() {});
    });
    WidgetUtils().cancelLoading();
  }

  void onTapName() async {
    String? name = await WidgetUtils()
        .pushPage(UserInput(true, model.clubInfo.clubName, "公会名称"));
    if (name == null || name.isEmpty) return;
    if (name == model.clubInfo.clubName) return;
    WidgetUtils().showLoading();
    bool lawful =
        await FuncUtils().checkTextLawful(name, CheckTxtType.guildname);
    if (!lawful) return WidgetUtils().cancelLoading();
    await requestModify("name", name, (_) {
      model.clubInfo.clubName = name;
      if (!mounted) return;
      setState(() {});
    });
    WidgetUtils().cancelLoading();
  }

  void onTapDes() async {
    String? des = await WidgetUtils().pushPage(UserInput(
        false, model.clubInfo.clubDes, "公会介绍",
        hintText: "请输入不少于10个字的描述～"));
    if (des == null || des.isEmpty) return;
    if (des == model.clubInfo.clubDes) return;
    if (des.trimRight().length < 10)
      return WidgetUtils().showToast("请输入不少于10个字的描述~");
    WidgetUtils().showLoading();
    bool lawful = await FuncUtils().checkTextLawful(des, CheckTxtType.notice);
    if (!lawful) return WidgetUtils().cancelLoading();
    await requestModify("desc", des, (_) {
      model.clubInfo.clubDes = des;
      debugPrint("--------------------------");
      if (!mounted) return;
      setState(() {});
    });
    WidgetUtils().cancelLoading();
  }

  void onTapSignList() {
    WidgetUtils().pushPage(SignedMember(model: model, isOwner: true));
  }

  void onTapBill() {
    // WidgetUtils().pushPage(
    //     ClubBill(widget.model.clubInfo.clubId.toString(), clubModel: widget.model));
  }

  Future<void> requestModify(
      String key, String value, Function(dynamic) onSuccess) async {
    Map params = {
      "name": model.clubInfo.clubName,
      "headpicture": model.clubInfo.clubCover,
      "notice": model.clubInfo.clubNotice ?? "",
      "desc": model.clubInfo.clubDes,
      "backgroundPicture": model.clubInfo.clubBg ?? "",
      "ownerId": model.clubOwner.ownerId,
      "guildPresidentOfName": UserConstants.userginInfo?.info.name,
      "guildPresidentPhone": UserConstants.userginInfo?.info.phone ?? "",
    };
    params[key] = value;
    await DioUtils().httpRequest(
      NetConstants.modifyClub,
      method: DioMethod.POST,
      params: params,
      onSuccess: onSuccess,
    );
  }
}
