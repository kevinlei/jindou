import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class ClubSearch extends StatefulWidget {
  const ClubSearch({Key? key}) : super(key: key);

  @override
  _ClubSearchState createState() => _ClubSearchState();
}

class _ClubSearchState extends State<ClubSearch> {
  bool isInputting = false;
  List<ClubssModel> clubList = [];
  String inputText = "";
  int curPage = 1;

  @override
  Widget build(BuildContext context) {
    return FContainer(
        padding: EdgeInsets.only(left: 30.w, right: 30.w, bottom: 20.w),
        image: ImageUtils().getAssetsImage("public/huibg"),
        imageFit: BoxFit.fill,
        imageAlign: Alignment.topCenter,
        child: SafeArea(
          child: Scaffold(
            backgroundColor: Colors.transparent,
            appBar: searchAppbar(),
            body: FRefreshLayout(
              onRefresh: callRefresh,
              onLoad: callLoad,
              firstRefresh: false,
              emptyWidget: clubList.isEmpty ? const FEmptyWidget() : null,
              child: listContent(),
            ),
          ),
        ));
  }

  AppBar searchAppbar() {
    return AppBar(
      leading: IconButton(
        onPressed: () {
          WidgetUtils().popPage();
        },
        icon: const Icon(Icons.arrow_back_ios, color: Colors.black),
      ),
      automaticallyImplyLeading: false,
      elevation: 0,
      backgroundColor: Colors.transparent,
      title: FContainer(
        height: 65.w,
        radius: BorderRadius.circular(65.w),
        padding: EdgeInsets.symmetric(horizontal: 25.w),
        color: const Color(0xFF0D000000),
        child: inputField(),
      ),
      actions: [buttonSearch()],
    );
  }

  Widget inputField() {
    InputBorder border = const OutlineInputBorder(
        borderSide: BorderSide(color: Colors.transparent));
    return TextField(
      style: TextStyle(color: Colors.black, fontSize: 26.sp),
      onChanged: onTextInput,
      decoration: InputDecoration(
        enabledBorder: border,
        focusedBorder: border,
        contentPadding: EdgeInsets.zero,
        hintStyle: TextStyle(color: const Color(0xFFBBBBBB), fontSize: 26.sp),
        hintText: "请输入公会的昵称或ID",
      ),
    );
  }

  Widget buttonSearch() {
    return TextButton(
        onPressed: isInputting ? onTapSearch : onTapCancel,
        child: FText(
          isInputting ? "搜索" : "取消",
          size: 26.sp,
          color: Colors.black,
        ));
  }

  Widget listContent() {
    return GridView.builder(
      padding: EdgeInsets.all(25.w),
      physics: const NeverScrollableScrollPhysics(),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 1,
        mainAxisExtent: 176.h,
        crossAxisSpacing: 20.w,
        mainAxisSpacing: 20.w,
      ),
      itemCount: clubList.length,
      itemBuilder: (_, index) => ClubListCell(info: clubList[index]),
    );
  }

  void onTextInput(String value) {
    inputText = value;
    if ((value.isEmpty && isInputting) || (value.isNotEmpty && !isInputting)) {
      isInputting = value.isNotEmpty;
      if (!mounted) return;
      setState(() {});
    }
  }

  void onTapSearch() {
    if (inputText.trim().isEmpty) return WidgetUtils().showToast("搜素内容不能为空");
    requestSearch(true);
  }

  void onTapCancel() {
    FocusManager.instance.primaryFocus?.unfocus();
    WidgetUtils().popPage();
  }

  Future<void> callRefresh() async {
    if (inputText.trim().isEmpty) return;
    curPage = 1;
    requestSearch(true);
  }

  Future<void> callLoad() async {
    curPage += 1;
    requestSearch(false);
  }

  void requestSearch(bool isRefresh) {
    DioUtils().asyncHttpRequest(
      NetConstants.clubSearch,
      method: DioMethod.POST,
      params: {"guild_name_or_id": inputText.trim()},
      onSuccess: isRefresh ? onRefresh : onLoad,
    );
  }

  void onRefresh(response) {
    clubList.clear();
    response ??= [];
    if (response == null) return setState(() {});
    clubList = (response as List<dynamic>)
        .map((e) => ClubssModel.fromJson(e as Map<String, dynamic>))
        .toList();
    if (!mounted) return;
    setState(() {});
  }

  void onLoad(response) {}
}
