import 'dart:io';
import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:intl/intl.dart';

import 'model/club_roomst_model.dart';


class ClubInfoPage extends StatefulWidget {
  final String clubId;

  const ClubInfoPage({Key? key, required this.clubId}) : super(key: key);

  @override
  _ClubInfoPageState createState() => _ClubInfoPageState();
}

class _ClubInfoPageState extends State<ClubInfoPage> {
  ClubInfoProvider infoProvider = ClubInfoProvider();
  ClubMemberProvider memberProvider = ClubMemberProvider();
  ClubRoomProvider roomProvider = ClubRoomProvider();

  @override
  void initState() {
    super.initState();
    callRefresh();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: infoProvider),
        ChangeNotifierProvider.value(value: memberProvider),
        ChangeNotifierProvider.value(value: roomProvider),
      ],
      child: FContainer(
        padding: EdgeInsets.only(left: 15.w, right: 15.w, bottom: 20.w),
        image: ImageUtils().getAssetsImage("public/huibg"),
        imageFit: BoxFit.fill,
        imageAlign: Alignment.topCenter,
        child: SafeArea(child: Scaffold(
          backgroundColor: Colors.transparent,
          body: FRefreshLayout(
            onRefresh: callRefresh,
            firstRefresh: false,
            child: FContainer(
              child: Consumer<ClubInfoProvider>(builder: (_, pro, __) {
                int clubRole = pro.modeldel?.memberType ?? ClubRole.DEF.index;
                int userRole =
                UserConstants.userginInfo?.info.userRole ?? UserRole.DEF.index;

                return SingleChildScrollView(
                  child: Column(
                    children: [
                      CommonAppBar(
                        title: "",
                        actions: [
                          exitButton(clubRole, userRole, pro.modeldel),
                        ],
                      ),
                      headerContent(pro),
                      // 自己公会
                      // Visibility(
                      //   visible: clubRole == ClubRole.VICE.index,
                      //   child: roleContent(pro.modeldel),
                      // ),
                      // Visibility(
                      //   visible: clubRole == ClubRole.VICE.index,
                      //   child: dataContent(pro.modeldel),
                      // ),
                        //审核管理
                      Visibility(
                        visible: clubRole == ClubRole.VICE.index,
                        child: datashenContent(pro.modeldel),
                      ),
                      Visibility(
                        visible: clubRole == ClubRole.VICE.index,
                        child: datalistContent(pro.modeldel),
                      ),


                      Visibility(
                        visible: clubRole == ClubRole.NORMAL.index ||
                        clubRole == ClubRole.VICE.index ||
                            clubRole == ClubRole.OWNER.index ||
                            clubRole == ClubRole.FUWER.index||
                            clubRole == ClubRole.GUER.index ||
                            clubRole == ClubRole.TINER.index ||
                            clubRole == ClubRole.CNER.index,
                        child: const ClubMemberContent(),

                      ),
                      Visibility(
                        visible: clubRole == ClubRole.DEF.index ||
                            clubRole == ClubRole.NORMAL.index ||
                            clubRole == ClubRole.VICE.index ||
                            clubRole == ClubRole.OWNER.index ||
                            clubRole == ClubRole.FUWER.index||
                            clubRole == ClubRole.GUER.index ||
                            clubRole == ClubRole.TINER.index ||
                            clubRole == ClubRole.CNER.index,

                        child: const ClubRoomContent(),
                      ),
                      Visibility(
                        visible: clubRole == ClubRole.DEF.index ||
                            clubRole == ClubRole.NORMAL.index ||
                            clubRole == ClubRole.VICE.index ||
                            clubRole == ClubRole.OWNER.index ||
                            clubRole == ClubRole.FUWER.index||
                            clubRole == ClubRole.GUER.index ||
                            clubRole == ClubRole.TINER.index ||
                            clubRole == ClubRole.CNER.index,
                        child: clubInfoContent(),
                      ),
                    ],
                  ),
                );
              }),
            ),
          ),
          bottomNavigationBar: Padding(
              padding: EdgeInsets.only(
                  bottom: Platform.isIOS ? ScreenUtil().bottomBarHeight : 0),
              child: bottomButton()),
        ),),
      ),


    );
  }

  Widget exitButton(int role, userRole, ClubRoomstModel? model) {
    return Visibility(
      visible: ((role == ClubRole.DEF.index) &&
              (userRole != InClubStatus.SIGN_APPLY.index))
          ? false
          : true,
      child: TextButton(
          onPressed: () {
            onTapSign(model);
          },
          child: LoadAssetImage("person/bakspck", width: 60.w,height: 60.w,),),
      //true显示
    );
  }
  Widget datashenContent(ClubRoomstModel? club) {
    return FContainer(
      color: const Color(0xFFFFFFFF),
      margin: EdgeInsets.only(left: 25.w, right: 25.w, bottom: 20.w, top: 15.w),
      radius: BorderRadius.circular(28.w),
      padding: EdgeInsets.all(32.w),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              LoadAssetImage("person/sopnteds", width: 40.w,height: 40.w,),
              SizedBox(
                width: 20.h,
              ),
              FText(
                "成员审核",
                size: 32.sp,
                color: Colors.black,
                weight: FontWeight.bold,
              ),
              const Spacer(),
              GestureDetector(
                child: FContainer(
                  child: Row(children: [
                    LoadAssetImage("person/rightrooms", width: 40.w,height: 40.w,),
                  ]),
                ),
                onTap: () {
                  onTapsBill(club);
                },
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget datalistContent(ClubRoomstModel? club) {
    return FContainer(
      color: const Color(0xFFFFFFFF),
      margin: EdgeInsets.only(left: 25.w, right: 25.w, bottom: 20.w, top: 15.w),
      radius: BorderRadius.circular(28.w),
      padding: EdgeInsets.all(32.w),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              SizedBox(
                width: 30.w,
              ),
              Column(children: [
                FText(
                  '${club?.GuildDaySerial}',
                  size: 32.sp,
                  color: Color(0xFF8376F3),
                  weight: FontWeight.bold,
                ),
                SizedBox(
                  height: 10.w,
                ),
                FText(
                  "今日流水",
                  size: 24.sp,
                  color: Colors.black.withOpacity(0.6),
                  weight: FontWeight.bold,
                ),
              ],),
              const Spacer(),
              Column(children: [
                FText(
               '${club?.GuildWeekSerial}',
                  size: 32.sp,
                  color: Color(0xFFE570F9),
                  weight: FontWeight.bold,
                ),
                SizedBox(
                  height: 10.w,
                ),
                FText(
                  "本周流水",
                  size: 24.sp,
                  color: Colors.black.withOpacity(0.6),
                  weight: FontWeight.bold,
                ),
              ],),
              const Spacer(),
              GestureDetector(
                child: FContainer(
                  child:    Column(children: [
                    FText(
                    '${club?.GuildMonthSerial}',
                      size: 32.sp,
                      color: Color(0xFFFF753F),
                      weight: FontWeight.bold,
                    ),
                    SizedBox(
                      height: 10.w,
                    ),
                    FText(
                      "本月流水",
                      size: 24.sp,
                      color: Colors.black.withOpacity(0.6),
                      weight: FontWeight.bold,
                    ),
                  ],),
                ),
                onTap: () {
                  onTapsBill(club);
                },
              ),
              SizedBox(
                width: 30.w,
              ),

            ],
          ),
        ],
      ),
    );
  }


  Widget dataContent(ClubRoomstModel? club) {
    return FContainer(
      color: const Color(0xFF28233B),
      margin: EdgeInsets.only(left: 25.w, right: 25.w, bottom: 20.w, top: 15.w),
      radius: BorderRadius.circular(28.w),
      padding: EdgeInsets.all(32.w),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              FText(
                "公会数据",
                size: 32.sp,
                color: Colors.black,
                weight: FontWeight.bold,
              ),
              const Spacer(),
              GestureDetector(
                child: FContainer(
                  color: const Color(0xFF493E6A),
                  width: 40.w,
                  height: 40.w,
                  radius: BorderRadius.circular(50),
                  child: Icon(Icons.arrow_forward_ios,
                      color: Colors.black.withOpacity(0.8), size: 15.w),
                ),
                onTap: () {
                  onTapBill(club);
                },
              ),
            ],
          ),
          dataGridView(),
        ],
      ),
    );
  }

  Widget dataGridView() {
    return FContainer(
      child: Column(
        children: [
          SizedBox(
            height: 20.h,
          ),
          Row(
            children: [
              dataGridViewCell(
                  "今日流水",
                  infoProvider.guildDataFw?.onTheDayFw ?? 0,
                  infoProvider.guildDataFw?.yesterdayFw ?? 0),
              dataGridViewLine(),
              dataGridViewCell(
                  "今日收益",
                  infoProvider.guildDataFw?.theDayRevenue ?? 0,
                  infoProvider.guildDataFw?.yesterdayRevenue ?? 0),
            ],
          ),
          SizedBox(
            height: 40.h,
          ),
          // Row(
          //   children: [
          //     dataGridViewCell("成员流水"),
          //     dataGridViewLine(),
          //     dataGridViewCell("公会流水"),
          //   ],
          // )
        ],
      ),
    );
  }

  Widget dataGridViewLine() {
    return FContainer(
      margin: EdgeInsets.only(left: 24.w, right: 24.w),
      height: 178.h,
      width: 2.w,
      color: const Color(0xFF231939).withOpacity(0.6),
    );
  }

  Widget dataGridViewCell(String title, int day, yesterday) {
    return Expanded(
      child: FContainer(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Expanded(
                    child: FText(
                  title,
                  size: 24.sp,
                  color: Colors.white.withOpacity(0.6),
                )),
                FText(
                  "",
                  // "明细",
                  size: 24.sp,
                  color: Colors.white.withOpacity(0.6),
                ),
                Icon(
                  Icons.arrow_forward_ios_outlined,
                  size: 22.w,
                  color: const Color(0xFF8B7EB4),
                ),
              ],
            ),
            FContainer(
              margin: EdgeInsets.only(
                top: 50.h,
                bottom: 50.h,
              ),
              child: FText(
                '${day}',
                color: Colors.white,
                size: 32.sp,
              ),
            ),
            FContainer(
              padding: EdgeInsets.all(12.w),
              radius: BorderRadius.circular(5),
              color: const Color(0xFF221E34),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  FText(
                    '${yesterday}',
                    color: const Color(0xFFC0C0C0),
                    size: 24.sp,
                  ),
                  FText(
                    "昨日",
                    color: const Color(0xFFC0C0C0),
                    size: 24.sp,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget roleContent(ClubRoomstModel? club) {
    return FContainer(
      color: const Color(0xFF28233B),
      margin: EdgeInsets.only(left: 25.w, right: 25.w, bottom: 20.w, top: 15.w),
      radius: BorderRadius.circular(28.w),
      child: GridView.count(
        crossAxisCount: 4,
        shrinkWrap: true,
        padding: EdgeInsets.all(20.w),
        physics: const NeverScrollableScrollPhysics(),
        children: [
          clubCell("club/memberInfo", "公会资料", () {
            onTapSet(club);
          }),
          clubCell("club/memberAudit", "审核管理", () {
            onTapClubApply(club!.guildId.toString());
          }),
          clubCell("club/member", "成员管理", () {
            onTapClubMemberMore(club);
          }),
          clubCell("club/guildRoom", "房间管理", () {
            onTapClubRoomListMore(club);
          }),
          // clubCell("club/wallet", "公会钱包", onTapAboutPage),
          clubCell("club/guildData", "公会数据", () {
            onTapBill(club);
          }),
        ],
      ),
    );
  }

  Widget clubCell(String img, String name, Function() onPress) {
    Widget btn = FContainer(
      align: Alignment.center,
      child:
          Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
        LoadAssetImage(img, width: 64.w, height: 64.w),
        FText(
          name,
          size: 24.sp,
          color: const Color(0xFF9F9FB9),
        )
      ]),
    );

    return GestureDetector(onTap: onPress, child: btn);
  }

  //公会介绍
  Widget clubInfoContent() {
    return FContainer(
      color: const Color(0xFFFFFFFF),
      width: double.infinity,
      radius: BorderRadius.circular(15.w),
      margin: EdgeInsets.only(left: 30.w, right: 30.w, top: 15.w, bottom: 15.w),
      padding: EdgeInsets.all(30.w),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          FText("公会介绍", size: 36.sp,color: Colors.black,weight :FontWeight.bold),
          SizedBox(
            height: 10.w,
          ),
          Consumer<ClubInfoProvider>(builder: (_, pro, __) {
            return FText(pro.modeldel?.guildInfo?.guildIntro ?? "",
                size: 28.sp,
                maxLines: 20,
                color: Colors.black.withOpacity(0.6));
          })
        ],
      ),
    );
  }

  Widget headerContent(ClubInfoProvider pro) {
    return FContainer(
      padding: EdgeInsets.all(30.w),
      child: Column(
        children: [
          Row(
            children: [
              SizedBox(
                width: 160.w,
                height: 160.w,
                child: ClipOval(
                  child: pro.modeldel != null
                      ? LoadNetImage(
                          NetConstants.ossPath + (pro.modeldel?.guildInfo?.guildAvatar ?? ""),
                          width: 160.w,
                        )
                      : LoadAssetImage(
                          "login/default",
                          width: 160.w,
                        ),
                ),
              ),
              SizedBox(
                width: 20.w,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  FText(
                    pro.modeldel?.guildInfo?.guildName ?? "",
                    size: 36.sp,
                    color: Colors.black,
                    weight: FontWeight.bold,
                  ),
                  SizedBox(
                    height: 38.w,
                  ),
                  Row(
                    children: [
                      headerCell(
                          "ID: ${pro.modeldel?.guildInfo?.guildId}", ''),
                      headerCell("${pro.modeldel?.guildInfo?.memberCount ?? 0}",
                          "person/roomPersonNum"),
                      headerCell("${pro.modeldel?.guildInfo?.rooms ?? 0}",
                          "person/roomNum"),
                    ],
                  )
                ],
              ),
            ],
          ),
          SizedBox(
            height: 30.w,
          ),
          // FContainer(
          //   radius: BorderRadius.circular(15.w),
          //   color: Color(0xFF302C49),
          //   height: 120.h,
          //   child: Row(
          //     mainAxisAlignment: MainAxisAlignment.spaceAround,
          //     children: [
          //       headerBtnCell("三星公会", "club/icon2"),
          //       FContainer(
          //         height: 35.w,
          //         width: 2.w,
          //         color: Colors.white.withOpacity(0.3),
          //       ),
          //       headerBtnCell("成员贡献", "club/icon1"),
          //     ],
          //   ),
          // ),
        ],
      ),
    );
  }

  Widget headerBtnCell(String text, image) {
    return FContainer(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Visibility(
            visible: image != "",
            child: LoadAssetImage(
              image,
              width: 48.w,
              height: 48.w,
            ),
          ),
          SizedBox(
            width: 10.w,
          ),
          FText(
            text,
            size: 28.sp,
            color: Colors.black,
            weight: FontWeight.bold,
          ),
        ],
      ),
    );
  }

  Widget headerCell(String text, image) {
    return FContainer(
      color: const Color(0xFFAFFFFFF),
      margin: EdgeInsets.only(right: 8.w),
      padding: EdgeInsets.only(left: 16.w, right: 16.w, top: 8.h, bottom: 8.h),
      radius: BorderRadius.circular(50),
      child: Row(
        children: [
          Visibility(
            visible: image != "",
            child: LoadAssetImage(
              image,
              width: 20.w,
            ),
          ),
          SizedBox(
            width: 5.w,
          ),
          FText(
            text,
            color: Colors.black,
            size: 24.sp,
          ),
        ],
      ),
    );
  }

  Widget bottomButton() {
    return Consumer<ClubInfoProvider>(builder: (_, pro, __) {
      String selfClubId = pro.modeldel?.guildId.toString() ?? "";
      // 自己的公会不为空
      if (selfClubId.isNotEmpty && selfClubId != widget.clubId) {
        return const SizedBox();
      }
      int clubRole = pro.modeldel?.memberType ?? ClubRole.DEF.index;
      int userStatus = pro.modeldel?.guildrelationType ?? InClubStatus.SIGNED.index;
      // 非公会成员 1：申请加入中 2：申请加入
      if (clubRole == ClubRole.DEF.index) {
        if (userStatus == InClubStatus.SIGN_APPLY.index) {
          return FcationButton("申请加入中", isActivate: true, radius: 90.w);
        } else {
          return Column(mainAxisSize: MainAxisSize.min, children: [
            // FProAccept(const [ProtocolType.PRO_CLUB_JOIN],
            //     onAccept: pro.onAccept),
            FcationButton("申请加入",
                isActivate: true, radius: 90.w, onPress: () => requestJoin(pro))
          ]);
        }
      }

      return const SizedBox();
    });
  }

  Future<void> requestJoin(ClubInfoProvider pro) async {
    // bool? accept = await WidgetUtils().showAlert("是否申请加入此公会？");
    // if (accept == null || !accept) return;
    pro.requestJoin();
  }

  Future<void> callRefresh() async {
    await infoProvider.requestSelfClub();
    await infoProvider.requestClubInfo(widget.clubId);
    // await infoProvider.requestGuildDataFW();

    memberProvider.requestMember(widget.clubId);
    String ownerId = widget.clubId;
    roomProvider.requestRoom(ownerId);
  }

  void onTapAboutPage() {}

  // 公会签约
  void onTapClubApply(String? clubId) {
    if (clubId == null) return;
    WidgetUtils().pushPage(ClubApply(clubId: clubId));
  }

  // 公会签约状态
  void onTapSign(ClubRoomstModel? model) async {
    // await WidgetUtils().pushPage(ClubSign(model: model!));
  }

  // 成员审核按钮
  void onTapsBill(ClubRoomstModel? model) {
    if (model == null) return;
    WidgetUtils().pushPage(ClubApply(clubId: model.guildId.toString()));
  }

  // 公会数据
  void onTapBill(ClubRoomstModel? model) {
    if (model == null) return;
    // WidgetUtils().pushPage(ClubBill(model.guildId.toString(), clubModel: model));
  }

  void onTapClubMemberMore(ClubRoomstModel? model) {
    if (model == null) return;
    // WidgetUtils().pushPage(ClubMember(
    //     clubId: model.clubInfo.clubId.toString(),
    //     ownerId: model.clubOwner.ownerId,
    //     clubRole: model.memberType));
  }

  void onTapClubRoomListMore(ClubRoomstModel? model) async {
    if (model == null) return;
    // WidgetUtils().pushPage(ClubRoomList(
    //     clubId: model.clubInfo.clubId.toString(),
    //     ownerId: model.clubOwner.ownerId,
    //     clubRole: model.));
  }

  void onTapSet(ClubRoomstModel? model) async {
    if (model == null) return;

    // int clubRole = model.clubRole;
    //
    // if (clubRole == ClubRole.OWNER.index) {
    //   await WidgetUtils().pushPage(ClubSet(model: model));
    // }
  }
}
