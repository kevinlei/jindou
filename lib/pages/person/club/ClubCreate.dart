import 'dart:io';

import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';

class ClubCreate extends StatefulWidget {
  @override
  _ClubCreateState createState() => _ClubCreateState();
}

class _ClubCreateState extends State<ClubCreate> {
  bool hasAccept = false;
  bool isApplying = false;

  String clubName = "";
  File? coverPath;
  String clubDes = "";
  String? realName = "";

  AuthStatus authStatus = AuthStatus.NULL;

  @override
  void initState() {
    super.initState();
    getUserInfo();
    requestAuthStatus();
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
        padding: EdgeInsets.only(left: 30.w, right: 30.w, bottom: 20.w),
        image: ImageUtils().getAssetsImage("public/huibg"),
        imageFit: BoxFit.fill,
        imageAlign: Alignment.topCenter,
        child: SafeArea(
            child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: const CommonAppBar(
            title: "创建公会",
            textColor: Colors.black,
            elevation: 0,
            backgroundColor: Colors.transparent,
          ),
          body: SingleChildScrollView(
            child: SizedBox(
              height: ScreenUtil().screenHeight -
                  kToolbarHeight -
                  ScreenUtil().statusBarHeight -
                  ScreenUtil().bottomBarHeight,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(height: 80.h),
                    clubCover(),
                    SizedBox(height: 20.h),
                    FText(
                      "公会封面",
                      size: 32.sp,
                      color: Colors.black,
                    ),
                    SizedBox(height: 80.h),
                    SetListItem("姓名",
                        suffix: FText(
                            realName ??
                                (UserConstants.userginInfo?.info.name ?? ""),
                            size: 26.sp,
                            color: const Color(0xFF999999))),
                    SetListItem("手机号",
                        suffix: FText(
                            UserConstants.userginInfo?.info.phone ?? "",
                            size: 26.sp,
                            color: const Color(0xFF999999))),
                    SetListItem("公会名称",
                        onPress: onTapName,
                        suffix: FText(clubName.isEmpty ? "请输入公会名称" : clubName,
                            size: 26.sp, color: const Color(0xFF999999))),
                    const Divider(color: Color(0xFFEEEEEE)),
                    SetListItem("公会介绍",
                        onPress: onTapDes,
                        suffix: FText(
                            clubDes.isEmpty ? "请输入不少于10个字的描述～" : clubDes,
                            size: 26.sp,
                            color: const Color(0xFF999999))),
                    const Spacer(),
                    FProSAccept([ProtocolType.PRO_CLUB_CREATE],
                        onAccept: onSelect),
                    SizedBox(height: 10.h),
                    FcationButton(isApplying ? "申请审核中" : "申请创建",
                        radius: 90.w, isActivate: true, onPress: onTapCreate),
                  ]),
            ),
          ),
        )));
  }

  Widget clubCover() {
    Widget btn = FContainer(
      width: 170.w,
      height: 170.w,
      image: coverPath == null ? null : FileImage(coverPath!),
      imageFit: BoxFit.cover,
      radius: BorderRadius.circular(20.w),
      color: const Color(0xFFEDEEF3),
      child: coverPath == null ? const Icon(Icons.add) : null,
    );
    return GestureDetector(onTap: onTapCover, child: btn);
  }

  final ImagePicker picker = ImagePicker();

  void onTapCover() async {
    bool accept = await FuncUtils().requestPermission(Permission.photos);
    if (!accept) return;

    XFile? file = await picker.pickImage(source: ImageSource.gallery);
    if (file == null || file.path.isEmpty) return;
    CroppedFile? crop = await ImageCropper().cropImage(
      sourcePath: file.path,
      compressQuality: 100,
      uiSettings: [
        AndroidUiSettings(toolbarTitle: '裁剪', statusBarColor: Colors.white),
        IOSUiSettings(title: '裁剪')
      ],
    );
    if (crop == null || crop.path.isEmpty) return;

    coverPath = File(crop.path);
    if (!mounted) return;
    setState(() {});
  }

  void onTapName() async {
    String? name =
        await WidgetUtils().pushPage(UserInput(true, clubName, "公会名称"));
    if (name == null || name.isEmpty) return;
    if (name == clubName) return;
    clubName = name;
    if (!mounted) return;
    setState(() {});
  }

  void onTapDes() async {
    String? des = await WidgetUtils().pushPage(
        UserInput(false, clubDes, "公会简介", hintText: "请输入不少于10个字的描述～"));
    if (des == null || des.isEmpty) return;
    if (des == clubDes) return;
    if (des.trimRight().length < 10)
      return WidgetUtils().showToast("请输入不少于10个字的描述~");
    clubDes = des;
    if (!mounted) return;
    setState(() {});
  }

  void onSelect(value) => hasAccept = value;

  void requestAuthStatus() {
    DioUtils().asyncHttpRequest(NetConstants.authStatus,
        method: DioMethod.GET, onSuccess: onAuthStatus);
  }
  void onAuthStatus(response) {
    if (response == null) return;

    realName = response["name"];
    authStatus = getCheckStatus(response["audit_status"]);
    if (authStatus != AuthStatus.AUTO_S &&
        authStatus != AuthStatus.MANUAL_S) {
      // WidgetUtils().showToast("请您先前往实名认证");
      return onTapAuth();
    }
    if (!mounted) return;
    setState(() {});
  }

  AuthStatus getCheckStatus(int? status) {
    for (AuthStatus item in AuthStatus.values) {
      if (item.value == status) return item;
    }
    return AuthStatus.NULL;
  }

  void onTapAuth() async {
    bool? accept =
    await WidgetUtils().showAlert("请前往金豆派对进行实名认证.", confirmText: '前往实名');
    if (accept == null || !accept) return;
    WidgetUtils().pushPage( AuthPage1(), fun: () {
      getUserInfo();
      requestAuthStatus();
    });
  }

  void getUserInfo() async {
    await DioUtils().httpRequest(NetConstants.getUserInfo,
        method: DioMethod.GET, params: {"userId": ""}, onSuccess: (response) {
      if (response == null) return;
      UserConstants.userginInfo = LoginUserModel.fromJson(response);
    });
  }

  void onTapCreate() async {
    if (
        authStatus != AuthStatus.AUTO_S &&
        authStatus != AuthStatus.MANUAL_S) {
      return WidgetUtils().showToast("请先前往实名认证");
    }
    if (isApplying) return;
    if (clubName.trimRight().isEmpty) return WidgetUtils().showToast("请添加公会名称");
    if (coverPath == null) return WidgetUtils().showToast("请添加公会封面");
    if (clubDes.trimRight().isEmpty) return WidgetUtils().showToast("请添加公会介绍");
    if (!hasAccept) return WidgetUtils().showToast("请先阅读并同意协议");

    WidgetUtils().showLoading();
    String? image = await FuncUtils()
        .requestUpload(coverPath!.path, UploadType.CLUB_HEADIMG);
    if (image == null) return WidgetUtils().cancelLoading();
    if (image.isEmpty) {
      WidgetUtils().showToast("上传公会封面失败");
      WidgetUtils().cancelLoading();
      return;
    }

    // bool lawfulImg =
    //     await FuncUtils().checkImageLawful([image], CheckImgType.IMG_GUILD);
    // if (!lawfulImg) return WidgetUtils().cancelLoading();

    // bool lawfulTxt =
    //     await FuncUtils().checkTextLawful(clubName, CheckTxtType.guildname);
    // if (!lawfulTxt) return WidgetUtils().cancelLoading();
    //
    // lawfulTxt = await FuncUtils().checkTextLawful(clubDes, CheckTxtType.notice);
    // if (!lawfulTxt) return WidgetUtils().cancelLoading();

    requestCreate(image);
    WidgetUtils().cancelLoading();
  }

  Future<void> requestCreate(String cover) async {
    String userId = UserConstants.userginInfo?.info.userId ?? "";
    String userName = UserConstants.userginInfo?.info.name ?? "";
    String userPhone = UserConstants.userginInfo?.info.phone ?? "";

    await DioUtils().httpRequest(
      NetConstants.createClub,
      method: DioMethod.POST,
      params: {
        "guild_name": clubName,
        "guild_intro": clubDes,
        "ownerId": userId,
        "guild_avatar": cover,
        "name": userName,
        "phone_number": userPhone,
        "code": 111111
      },
      onSuccess: (_) => WidgetUtils().popPage(),
    );
  }
}
