import 'package:json_annotation/json_annotation.dart';

part 'club_mspostmdoel.g.dart';

@JsonSerializable()
class ClubMspostmdoel {
  @JsonKey(name: 'member_type')
  final int? memberType;
  @JsonKey(name: 'day_serial_sum')
  final int? daySerialSum;
  @JsonKey(name: 'week_serial_sum')
  final int? weekSerialSum;
  @JsonKey(name: 'month_serial_sum')
  final int? monthSerialSum;
  @JsonKey(name: 'room_serial_sum')
  final int? roomSerialSum;
  @JsonKey(name: 'room_info')
  final RoomInfo? roomInfo;
  final int? sex;
  final int? birthday;

  const ClubMspostmdoel({
    this.memberType,
    this.daySerialSum,
    this.weekSerialSum,
    this.monthSerialSum,
    this.roomSerialSum,
    this.roomInfo,
    this.sex,
    this.birthday,
  });

  factory ClubMspostmdoel.fromJson(Map<String, dynamic> json) =>
      _$ClubMspostmdoelFromJson(json);

  Map<String, dynamic> toJson() => _$ClubMspostmdoelToJson(this);
}

@JsonSerializable()
class RoomInfo {
  @JsonKey(name: 'ex_room_id')
  final int? exRoomId;
  @JsonKey(name: 'create_time')
  final int? createTime;
  final String? image;
  final String? name;
  final Owner? owner;

  const RoomInfo({
    this.exRoomId,
    this.createTime,
    this.image,
    this.name,
    this.owner,
  });

  factory RoomInfo.fromJson(Map<String, dynamic> json) =>
      _$RoomInfoFromJson(json);

  Map<String, dynamic> toJson() => _$RoomInfoToJson(this);
}

@JsonSerializable()
class Owner {
  @JsonKey(name: 'owner_id')
  final int? ownerId;
  @JsonKey(name: 'owner_image')
  final String? ownerImage;
  @JsonKey(name: 'owner_name')
  final String? ownerName;

  const Owner({
    this.ownerId,
    this.ownerImage,
    this.ownerName,
  });

  factory Owner.fromJson(Map<String, dynamic> json) =>
      _$OwnerFromJson(json);

  Map<String, dynamic> toJson() => _$OwnerToJson(this);
}
