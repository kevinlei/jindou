import 'package:json_annotation/json_annotation.dart';

part 'club_membart_model.g.dart';

@JsonSerializable()
class ClubMembartModel {
  @JsonKey(name: 'user_id')
  final String? userId;
  @JsonKey(name: 'guild_id')
  final int? guildId;
  @JsonKey(name: 'member_type')
  final int? memberType;
  @JsonKey(name: 'serial_sum')
  final int? serialsum;

  @JsonKey(name: 'user_info')
  final UserInfo? userInfo;

  const ClubMembartModel({
    this.userId,
    this.guildId,
    this.memberType,
    this.userInfo,
    this.serialsum,
  });

  factory ClubMembartModel.fromJson(Map<String, dynamic> json) =>
      _$ClubMembartModelFromJson(json);

  Map<String, dynamic> toJson() => _$ClubMembartModelToJson(this);
}

@JsonSerializable()
class UserInfo {
  @JsonKey(name: 'user_id')
  final String? userId;
  final String? name;
  final int? gender;
  @JsonKey(name: 'head_icon')
  final String? headIcon;
  final int? sex;
  final int? birthday;
  @JsonKey(name: 'display_id')
  final int? displayId;


  const UserInfo({
    this.userId,
    this.name,
    this.gender,
    this.headIcon,
    this.sex,
    this.birthday,
    this.displayId,
  });

  factory UserInfo.fromJson(Map<String, dynamic> json) =>
      _$UserInfoFromJson(json);

  Map<String, dynamic> toJson() => _$UserInfoToJson(this);
}
