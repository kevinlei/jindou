import 'package:json_annotation/json_annotation.dart';

part 'club_roomst_model.g.dart';

@JsonSerializable()
class ClubRoomstModel {
  @JsonKey(name: 'guild_id')
  final int? guildId;
  @JsonKey(name: 'member_type')
  final int? memberType;
  @JsonKey(name: 'guild_info')
  final GuildInfo ?guildInfo;
  @JsonKey(name: 'member_list')
  final List<MemberList>? memberList;
  @JsonKey(name: 'room_list')
  final RoomList? roomList;
  @JsonKey(name: 'guild_relation_type')
  late  int guildrelationType;
  @JsonKey(name: 'guild_day_serial')
  late  int? GuildDaySerial;
  @JsonKey(name: 'guild_week_serial')
  late  int? GuildWeekSerial ;
  @JsonKey(name: 'guild_month_serial')
  late  int? GuildMonthSerial;
   ClubRoomstModel({
    this.guildId,
    this.memberType,
    this.guildInfo,
    this.memberList,
    this.roomList,
    required this.guildrelationType,
     required this.GuildDaySerial,
     required this.GuildWeekSerial,
     required this.GuildMonthSerial,
  });

  factory ClubRoomstModel.fromJson(Map<String, dynamic> json) =>
      _$ClubRoomstModelFromJson(json);

  Map<String, dynamic> toJson() => _$ClubRoomstModelToJson(this);
}

@JsonSerializable()
class GuildInfo {
  @JsonKey(name: 'guild_id')
  final int? guildId;
  @JsonKey(name: 'guild_name')
  final String? guildName;
  @JsonKey(name: 'guild_avatar')
  final String guildAvatar;
  @JsonKey(name: 'member_count')
  final int? memberCount;
  // final RoomCount? RoomCount;
  final int? rooms;
  @JsonKey(name: 'guild_intro')
  final String ?guildIntro;


  const GuildInfo({
    this.guildId,
    this.guildName,
   required this.guildAvatar,
    this.memberCount,
    // this.RoomCount,
    this.rooms,
  required    this.guildIntro,
  });

  factory GuildInfo.fromJson(Map<String, dynamic> json) =>
      _$GuildInfoFromJson(json);

  Map<String, dynamic> toJson() => _$GuildInfoToJson(this);
}

@JsonSerializable()
class RoomCount {
  final List<Guild>? guild;

  const RoomCount({
    this.guild,
  });

  factory RoomCount.fromJson(Map<String, dynamic> json) =>
      _$RoomCountFromJson(json);

  Map<String, dynamic> toJson() => _$RoomCountToJson(this);
}

@JsonSerializable()
class Guild {
  @JsonKey(name: 'guild_id')
  final int? guildId;
  @JsonKey(name: 'room_number')
  final int? roomNumber;

  const Guild({
    this.guildId,
    this.roomNumber,
  });

  factory Guild.fromJson(Map<String, dynamic> json) =>
      _$GuildFromJson(json);

  Map<String, dynamic> toJson() => _$GuildToJson(this);
}

@JsonSerializable()
class MemberList {
  @JsonKey(name: 'user_id')
  final String? userId;
  @JsonKey(name: 'member_type')
  final int? memberType;
  @JsonKey(name: 'user_info')
  final UserInfo? userInfo;

  const MemberList({
    this.userId,
    this.memberType,
    this.userInfo,
  });

  factory MemberList.fromJson(Map<String, dynamic> json) =>
      _$MemberListFromJson(json);

  Map<String, dynamic> toJson() => _$MemberListToJson(this);
}

@JsonSerializable()
class UserInfo {
  @JsonKey(name: 'user_id')
  final String? userId;
  @JsonKey(name: 'head_icon')
  final String? headIcon;
  final String? name;
  final int? sex;
  final int? birthday;

  const UserInfo({
    this.userId,
    this.headIcon,
    this.name,
    this.sex,
    this.birthday,
  });

  factory UserInfo.fromJson(Map<String, dynamic> json) =>
      _$UserInfoFromJson(json);

  Map<String, dynamic> toJson() => _$UserInfoToJson(this);
}

@JsonSerializable()
class RoomList {
  @JsonKey(name: 'top_rooms')
  final List<TopRooms>? topRooms;
  final List<Rooms>? rooms;
  final int? count;

  const RoomList({
    this.topRooms,
    this.rooms,
    this.count,
  });

  factory RoomList.fromJson(Map<String, dynamic> json) =>
      _$RoomListFromJson(json);

  Map<String, dynamic> toJson() => _$RoomListToJson(this);
}

@JsonSerializable()
class TopRooms {
  @JsonKey(name: 'ex_id')
  final int? exId;
  final String? image;
  final String? name;
  final Owner? owner;
  @JsonKey(name: 'person_num')
  final int? personNum;
  @JsonKey(name: 'topic_name')
  final String? topicName;

  const TopRooms({
    this.exId,
    this.image,
    this.name,
    this.owner,
    this.personNum,
    this.topicName,
  });

  factory TopRooms.fromJson(Map<String, dynamic> json) =>
      _$TopRoomsFromJson(json);

  Map<String, dynamic> toJson() => _$TopRoomsToJson(this);
}

@JsonSerializable()
class Owner {
  @JsonKey(name: 'owner_id')
  final int? ownerId;
  @JsonKey(name: 'owner_image')
  final String? ownerImage;
  @JsonKey(name: 'owner_name')
  final String? ownerName;

  const Owner({
    this.ownerId,
    this.ownerImage,
    this.ownerName,
  });

  factory Owner.fromJson(Map<String, dynamic> json) =>
      _$OwnerFromJson(json);

  Map<String, dynamic> toJson() => _$OwnerToJson(this);
}

@JsonSerializable()
class Rooms {
  @JsonKey(name: 'ex_id')
  final int? exId;
  final String? image;
  final String? name;
  final Owner? owner;
  @JsonKey(name: 'person_num')
  final int? personNum;
  @JsonKey(name: 'topic_name')
  final String? topicName;

  const Rooms({
    this.exId,
    this.image,
    this.name,
    this.owner,
    this.personNum,
    this.topicName,
  });

  factory Rooms.fromJson(Map<String, dynamic> json) =>
      _$RoomsFromJson(json);

  Map<String, dynamic> toJson() => _$RoomsToJson(this);
}
