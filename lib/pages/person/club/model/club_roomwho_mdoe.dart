import 'package:json_annotation/json_annotation.dart';

part 'club_roomwho_mdoe.g.dart';

@JsonSerializable()
class ClubRoomwhoMdoe {
  @JsonKey(name: 'guild_id')
  final int ?guildId;
  @JsonKey(name: 'guild_name')
  final String guildName;
  @JsonKey(name: 'guild_avatar')
  final String guildAvatar;
  @JsonKey(name: 'member_count')
  final int memberCount;
  @JsonKey(name: 'rooms')
  final int ?Rooms;
  @JsonKey(name: 'guild_intro')
  final String ?guildIntro;

  const ClubRoomwhoMdoe({
       this.guildId,
    required this.guildName,
    required this.guildAvatar,
    required this.memberCount,
     this.Rooms,
     this.guildIntro,
  });

  factory ClubRoomwhoMdoe.fromJson(Map<String, dynamic> json) =>
      _$ClubRoomwhoMdoeFromJson(json);

  Map<String, dynamic> toJson() => _$ClubRoomwhoMdoeToJson(this);
}

@JsonSerializable()
class RoomCount {
  final List<Guild>? guild;

  const RoomCount({
    this.guild,
  });

  factory RoomCount.fromJson(Map<String, dynamic> json) =>
      _$RoomCountFromJson(json);

  Map<String, dynamic> toJson() => _$RoomCountToJson(this);
}

@JsonSerializable()
class Guild {
  @JsonKey(name: 'guild_id')
  final int? guildId;
  @JsonKey(name: 'room_number')
  final int? roomNumber;

  const Guild({
    this.guildId,
    this.roomNumber,
  });

  factory Guild.fromJson(Map<String, dynamic> json) =>
      _$GuildFromJson(json);

  Map<String, dynamic> toJson() => _$GuildToJson(this);
}
