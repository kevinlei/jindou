// To parse this JSON data, do
//
//     final guildDataFw = guildDataFwFromJson(jsonString);

import 'dart:convert';

GuildDataFw guildDataFwFromJson(Map<String, dynamic> json) => GuildDataFw.fromJson(json);

String guildDataFwToJson(GuildDataFw data) => json.encode(data.toJson());

class GuildDataFw {
  GuildDataFw({
    required this.onTheDayFw,
    required this.theDayRevenue,
    required this.yesterdayFw,
    required this.yesterdayRevenue,
  });

  int onTheDayFw;
  int theDayRevenue;
  int yesterdayFw;
  int yesterdayRevenue;

  factory GuildDataFw.fromJson(Map<String, dynamic> json) => GuildDataFw(
    onTheDayFw: json["onTheDayFW"],
    theDayRevenue: json["theDayRevenue"],
    yesterdayFw: json["yesterdayFW"],
    yesterdayRevenue: json["yesterdayRevenue"],
  );

  Map<String, dynamic> toJson() => {
    "onTheDayFW": onTheDayFw,
    "theDayRevenue": theDayRevenue,
    "yesterdayFW": yesterdayFw,
    "yesterdayRevenue": yesterdayRevenue,
  };
}
