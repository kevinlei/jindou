import 'package:json_annotation/json_annotation.dart';

part 'club_lieb_model.g.dart';

@JsonSerializable()
class ClubLiebModel {
  @JsonKey(name: 'ex_id')
  final int exId;
  @JsonKey(name: 'room_id')
  final int roomId;
  @JsonKey(name: 'guild_id')
  final int guildId;
  final String? image;
  final String? name;
  final Owner? owner;
  @JsonKey(name: 'person_num')
  final int? personNum;
  @JsonKey(name: 'topic_name')
  final String? topicName;

  const ClubLiebModel({
   required this.exId,
   required this.roomId,
    this.image,
    this.name,
    this.owner,
    this.personNum,
    this.topicName,
   required this.guildId,
  });

  factory ClubLiebModel.fromJson(Map<String, dynamic> json) =>
      _$ClubLiebModelFromJson(json);

  Map<String, dynamic> toJson() => _$ClubLiebModelToJson(this);
}

@JsonSerializable()
class Owner {
  @JsonKey(name: 'owner_id')
  final int? ownerId;
  @JsonKey(name: 'owner_image')
  final String? ownerImage;
  @JsonKey(name: 'owner_name')
  final String ownerName;

  const Owner({
    this.ownerId,
    this.ownerImage,
   required this.ownerName,
  });

  factory Owner.fromJson(Map<String, dynamic> json) =>
      _$OwnerFromJson(json);

  Map<String, dynamic> toJson() => _$OwnerToJson(this);
}
