// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'club_mspostmdoel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ClubMspostmdoel _$ClubMspostmdoelFromJson(Map<String, dynamic> json) =>
    ClubMspostmdoel(
      memberType: json['member_type'] as int?,
      daySerialSum: json['day_serial_sum'] as int?,
      weekSerialSum: json['week_serial_sum'] as int?,
      monthSerialSum: json['month_serial_sum'] as int?,
      roomSerialSum: json['room_serial_sum'] as int?,
      roomInfo: json['room_info'] == null
          ? null
          : RoomInfo.fromJson(json['room_info'] as Map<String, dynamic>),
      sex: json['sex'] as int?,
      birthday: json['birthday'] as int?,
    );

Map<String, dynamic> _$ClubMspostmdoelToJson(ClubMspostmdoel instance) =>
    <String, dynamic>{
      'member_type': instance.memberType,
      'day_serial_sum': instance.daySerialSum,
      'week_serial_sum': instance.weekSerialSum,
      'month_serial_sum': instance.monthSerialSum,
      'room_serial_sum': instance.roomSerialSum,
      'room_info': instance.roomInfo,
      'sex': instance.sex,
      'birthday': instance.birthday,
    };

RoomInfo _$RoomInfoFromJson(Map<String, dynamic> json) => RoomInfo(
      exRoomId: json['ex_room_id'] as int?,
      createTime: json['create_time'] as int?,
      image: json['image'] as String?,
      name: json['name'] as String?,
      owner: json['owner'] == null
          ? null
          : Owner.fromJson(json['owner'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$RoomInfoToJson(RoomInfo instance) => <String, dynamic>{
      'ex_room_id': instance.exRoomId,
      'create_time': instance.createTime,
      'image': instance.image,
      'name': instance.name,
      'owner': instance.owner,
    };

Owner _$OwnerFromJson(Map<String, dynamic> json) => Owner(
      ownerId: json['owner_id'] as int?,
      ownerImage: json['owner_image'] as String?,
      ownerName: json['owner_name'] as String?,
    );

Map<String, dynamic> _$OwnerToJson(Owner instance) => <String, dynamic>{
      'owner_id': instance.ownerId,
      'owner_image': instance.ownerImage,
      'owner_name': instance.ownerName,
    };
