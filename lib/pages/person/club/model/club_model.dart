import 'package:json_annotation/json_annotation.dart';
part 'club_model.g.dart';

@JsonSerializable()
class ClubModel extends Object {
  @JsonKey(name: 'clubRole')
  late int clubRole;

  @JsonKey(name: 'userStatus')
  late int userStatus;

  @JsonKey(name: 'signStart')
  late int signStart;

  @JsonKey(name: 'signTime')
  late int signTime;

  @JsonKey(name: 'cubInfo')
  late ClubInfo clubInfo;

  @JsonKey(name: 'ownerInfo')
  late ClubOwner clubOwner;

  ClubModel();

  factory ClubModel.fromJson(Map<String, dynamic> srcJson) =>
      _$ClubModelFromJson(srcJson);

  Map<String, dynamic> toJson() => _$ClubModelToJson(this);
}

@JsonSerializable()
class ClubssbModel extends Object {
  @JsonKey(name: 'guild_id')
  late int guildId;

  @JsonKey(name: 'guild_name')
  late String guildName;

  @JsonKey(name: 'guild_avatar')
  late String guildAvatar;

  @JsonKey(name: 'member_count')
  late int memberCount;

  @JsonKey(name: 'state')
  late int? state;
  @JsonKey(name: 'rooms')
  late int Rooms;
  @JsonKey(name: 'guild_intro')
  late String guildIntro;

  @JsonKey(name: 'RoomCount')
  late ClubInfo clubInfo;

  ClubssbModel();
  factory ClubssbModel.fromJson(Map<String, dynamic> srcJson) =>
      _$ClubssbModelFromJson(srcJson);

  Map<String, dynamic> toJson() => _$ClubssbModelToJson(this);
}

@JsonSerializable()
class ClubssModel extends Object {
  @JsonKey(name: 'guild_id')
  late int guildId;

  @JsonKey(name: 'guild_name')
  late String guildName;

  @JsonKey(name: 'guild_avatar')
  late String guildAvatar;

  @JsonKey(name: 'member_count')
  late int memberCount;

  late int rooms;

  @JsonKey(name: 'guild_intro')
  late String? guildIntro;

  ClubssModel();
  factory ClubssModel.fromJson(Map<String, dynamic> srcJson) =>
      _$ClubssModelFromJson(srcJson);

  Map<String, dynamic> toJson() => _$ClubssModelToJson(this);
}

@JsonSerializable()
class ClubInfo extends Object {
  @JsonKey(name: 'clubId')
  late String clubId;

  @JsonKey(name: 'guild_id')
  late int guildId;

  @JsonKey(name: 'guild_name')
  late String guildname;

  @JsonKey(name: 'guild_avatar')
  late String guildavatar;

  @JsonKey(name: 'member_count')
  late int membercount;

  @JsonKey(name: 'clubDisplayId')
  late String clubDisplayId;

  @JsonKey(name: 'clubName')
  late String clubName;

  @JsonKey(name: 'clubCover')
  late String clubCover;

  @JsonKey(name: 'clubBg')
  String? clubBg;

  @JsonKey(name: 'clubNotice')
  String? clubNotice;

  @JsonKey(name: 'clubDes')
  late String clubDes;

  @JsonKey(name: 'memberCount')
  late int memberCount;

  @JsonKey(name: 'clubStatus')
  late int clubStatus;

  ClubInfo();

  factory ClubInfo.fromJson(Map<String, dynamic> srcJson) =>
      _$ClubInfoFromJson(srcJson);

  Map<String, dynamic> toJson() => _$ClubInfoToJson(this);
}

@JsonSerializable()
class ClubOwner extends Object {
  @JsonKey(name: 'ownerId')
  late String ownerId;

  @JsonKey(name: 'ownerDisplayId')
  late String ownerDisplayId;

  @JsonKey(name: 'ownerName')
  late String ownerName;

  @JsonKey(name: 'ownerHead')
  late String ownerHead;

  ClubOwner();

  factory ClubOwner.fromJson(Map<String, dynamic> srcJson) =>
      _$ClubOwnerFromJson(srcJson);

  Map<String, dynamic> toJson() => _$ClubOwnerToJson(this);
}
