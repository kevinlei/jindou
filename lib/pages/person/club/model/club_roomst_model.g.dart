// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'club_roomst_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ClubRoomstModel _$ClubRoomstModelFromJson(Map<String, dynamic> json) =>
    ClubRoomstModel(
      guildId: json['guild_id'] as int?,
      memberType: json['member_type'] as int?,
      guildInfo: json['guild_info'] == null
          ? null
          : GuildInfo.fromJson(json['guild_info'] as Map<String, dynamic>),
      memberList: (json['member_list'] as List<dynamic>?)
          ?.map((e) => MemberList.fromJson(e as Map<String, dynamic>))
          .toList(),
      roomList: json['room_list'] == null
          ? null
          : RoomList.fromJson(json['room_list'] as Map<String, dynamic>),
      guildrelationType: json['guild_relation_type'] as int,
      GuildDaySerial: json['guild_day_serial'] as int?,
      GuildWeekSerial: json['guild_week_serial'] as int?,
      GuildMonthSerial: json['guild_month_serial'] as int?,
    );

Map<String, dynamic> _$ClubRoomstModelToJson(ClubRoomstModel instance) =>
    <String, dynamic>{
      'guild_id': instance.guildId,
      'member_type': instance.memberType,
      'guild_info': instance.guildInfo,
      'member_list': instance.memberList,
      'room_list': instance.roomList,
      'guild_relation_type': instance.guildrelationType,
      'guild_day_serial': instance.GuildDaySerial,
      'guild_week_serial': instance.GuildWeekSerial,
      'guild_month_serial': instance.GuildMonthSerial,
    };

GuildInfo _$GuildInfoFromJson(Map<String, dynamic> json) => GuildInfo(
      guildId: json['guild_id'] as int?,
      guildName: json['guild_name'] as String?,
      guildAvatar: json['guild_avatar'] as String,
      memberCount: json['member_count'] as int?,
      rooms: json['rooms'] as int?,
      guildIntro: json['guild_intro'] as String?,
    );

Map<String, dynamic> _$GuildInfoToJson(GuildInfo instance) => <String, dynamic>{
      'guild_id': instance.guildId,
      'guild_name': instance.guildName,
      'guild_avatar': instance.guildAvatar,
      'member_count': instance.memberCount,
      'rooms': instance.rooms,
      'guild_intro': instance.guildIntro,
    };

RoomCount _$RoomCountFromJson(Map<String, dynamic> json) => RoomCount(
      guild: (json['guild'] as List<dynamic>?)
          ?.map((e) => Guild.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$RoomCountToJson(RoomCount instance) => <String, dynamic>{
      'guild': instance.guild,
    };

Guild _$GuildFromJson(Map<String, dynamic> json) => Guild(
      guildId: json['guild_id'] as int?,
      roomNumber: json['room_number'] as int?,
    );

Map<String, dynamic> _$GuildToJson(Guild instance) => <String, dynamic>{
      'guild_id': instance.guildId,
      'room_number': instance.roomNumber,
    };

MemberList _$MemberListFromJson(Map<String, dynamic> json) => MemberList(
      userId: json['user_id'] as String?,
      memberType: json['member_type'] as int?,
      userInfo: json['user_info'] == null
          ? null
          : UserInfo.fromJson(json['user_info'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$MemberListToJson(MemberList instance) =>
    <String, dynamic>{
      'user_id': instance.userId,
      'member_type': instance.memberType,
      'user_info': instance.userInfo,
    };

UserInfo _$UserInfoFromJson(Map<String, dynamic> json) => UserInfo(
      userId: json['user_id'] as String?,
      headIcon: json['head_icon'] as String?,
      name: json['name'] as String?,
      sex: json['sex'] as int?,
      birthday: json['birthday'] as int?,
    );

Map<String, dynamic> _$UserInfoToJson(UserInfo instance) => <String, dynamic>{
      'user_id': instance.userId,
      'head_icon': instance.headIcon,
      'name': instance.name,
      'sex': instance.sex,
      'birthday': instance.birthday,
    };

RoomList _$RoomListFromJson(Map<String, dynamic> json) => RoomList(
      topRooms: (json['top_rooms'] as List<dynamic>?)
          ?.map((e) => TopRooms.fromJson(e as Map<String, dynamic>))
          .toList(),
      rooms: (json['rooms'] as List<dynamic>?)
          ?.map((e) => Rooms.fromJson(e as Map<String, dynamic>))
          .toList(),
      count: json['count'] as int?,
    );

Map<String, dynamic> _$RoomListToJson(RoomList instance) => <String, dynamic>{
      'top_rooms': instance.topRooms,
      'rooms': instance.rooms,
      'count': instance.count,
    };

TopRooms _$TopRoomsFromJson(Map<String, dynamic> json) => TopRooms(
      exId: json['ex_id'] as int?,
      image: json['image'] as String?,
      name: json['name'] as String?,
      owner: json['owner'] == null
          ? null
          : Owner.fromJson(json['owner'] as Map<String, dynamic>),
      personNum: json['person_num'] as int?,
      topicName: json['topic_name'] as String?,
    );

Map<String, dynamic> _$TopRoomsToJson(TopRooms instance) => <String, dynamic>{
      'ex_id': instance.exId,
      'image': instance.image,
      'name': instance.name,
      'owner': instance.owner,
      'person_num': instance.personNum,
      'topic_name': instance.topicName,
    };

Owner _$OwnerFromJson(Map<String, dynamic> json) => Owner(
      ownerId: json['owner_id'] as int?,
      ownerImage: json['owner_image'] as String?,
      ownerName: json['owner_name'] as String?,
    );

Map<String, dynamic> _$OwnerToJson(Owner instance) => <String, dynamic>{
      'owner_id': instance.ownerId,
      'owner_image': instance.ownerImage,
      'owner_name': instance.ownerName,
    };

Rooms _$RoomsFromJson(Map<String, dynamic> json) => Rooms(
      exId: json['ex_id'] as int?,
      image: json['image'] as String?,
      name: json['name'] as String?,
      owner: json['owner'] == null
          ? null
          : Owner.fromJson(json['owner'] as Map<String, dynamic>),
      personNum: json['person_num'] as int?,
      topicName: json['topic_name'] as String?,
    );

Map<String, dynamic> _$RoomsToJson(Rooms instance) => <String, dynamic>{
      'ex_id': instance.exId,
      'image': instance.image,
      'name': instance.name,
      'owner': instance.owner,
      'person_num': instance.personNum,
      'topic_name': instance.topicName,
    };
