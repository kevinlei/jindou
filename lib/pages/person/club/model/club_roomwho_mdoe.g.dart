// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'club_roomwho_mdoe.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ClubRoomwhoMdoe _$ClubRoomwhoMdoeFromJson(Map<String, dynamic> json) =>
    ClubRoomwhoMdoe(
      guildId: json['guild_id'] as int?,
      guildName: json['guild_name'] as String,
      guildAvatar: json['guild_avatar'] as String,
      memberCount: json['member_count'] as int,
      Rooms: json['rooms'] as int?,
      guildIntro: json['guild_intro'] as String?,
    );

Map<String, dynamic> _$ClubRoomwhoMdoeToJson(ClubRoomwhoMdoe instance) =>
    <String, dynamic>{
      'guild_id': instance.guildId,
      'guild_name': instance.guildName,
      'guild_avatar': instance.guildAvatar,
      'member_count': instance.memberCount,
      'rooms': instance.Rooms,
      'guild_intro': instance.guildIntro,
    };

RoomCount _$RoomCountFromJson(Map<String, dynamic> json) => RoomCount(
      guild: (json['guild'] as List<dynamic>?)
          ?.map((e) => Guild.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$RoomCountToJson(RoomCount instance) => <String, dynamic>{
      'guild': instance.guild,
    };

Guild _$GuildFromJson(Map<String, dynamic> json) => Guild(
      guildId: json['guild_id'] as int?,
      roomNumber: json['room_number'] as int?,
    );

Map<String, dynamic> _$GuildToJson(Guild instance) => <String, dynamic>{
      'guild_id': instance.guildId,
      'room_number': instance.roomNumber,
    };
