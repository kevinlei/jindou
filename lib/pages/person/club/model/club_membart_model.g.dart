// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'club_membart_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ClubMembartModel _$ClubMembartModelFromJson(Map<String, dynamic> json) =>
    ClubMembartModel(
      userId: json['user_id'] as String?,
      guildId: json['guild_id'] as int?,
      memberType: json['member_type'] as int?,
      userInfo: json['user_info'] == null
          ? null
          : UserInfo.fromJson(json['user_info'] as Map<String, dynamic>),
      serialsum: json['serial_sum'] as int?,
    );

Map<String, dynamic> _$ClubMembartModelToJson(ClubMembartModel instance) =>
    <String, dynamic>{
      'user_id': instance.userId,
      'guild_id': instance.guildId,
      'member_type': instance.memberType,
      'serial_sum': instance.serialsum,
      'user_info': instance.userInfo,
    };

UserInfo _$UserInfoFromJson(Map<String, dynamic> json) => UserInfo(
      userId: json['user_id'] as String?,
      name: json['name'] as String?,
      gender: json['gender'] as int?,
      headIcon: json['head_icon'] as String?,
      sex: json['sex'] as int?,
      birthday: json['birthday'] as int?,
      displayId: json['display_id'] as int?,
    );

Map<String, dynamic> _$UserInfoToJson(UserInfo instance) => <String, dynamic>{
      'user_id': instance.userId,
      'name': instance.name,
      'gender': instance.gender,
      'head_icon': instance.headIcon,
      'sex': instance.sex,
      'birthday': instance.birthday,
      'display_id': instance.displayId,
    };
