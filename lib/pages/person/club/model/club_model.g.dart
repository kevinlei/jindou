// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'club_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ClubModel _$ClubModelFromJson(Map<String, dynamic> json) => ClubModel()
  ..clubRole = json['clubRole'] as int
  ..userStatus = json['userStatus'] as int
  ..signStart = json['signStart'] as int
  ..signTime = json['signTime'] as int
  ..clubInfo = ClubInfo.fromJson(json['cubInfo'] as Map<String, dynamic>)
  ..clubOwner = ClubOwner.fromJson(json['ownerInfo'] as Map<String, dynamic>);

Map<String, dynamic> _$ClubModelToJson(ClubModel instance) => <String, dynamic>{
      'clubRole': instance.clubRole,
      'userStatus': instance.userStatus,
      'signStart': instance.signStart,
      'signTime': instance.signTime,
      'cubInfo': instance.clubInfo,
      'ownerInfo': instance.clubOwner,
    };

ClubssbModel _$ClubssbModelFromJson(Map<String, dynamic> json) => ClubssbModel()
  ..guildId = json['guild_id'] as int
  ..guildName = json['guild_name'] as String
  ..guildAvatar = json['guild_avatar'] as String
  ..memberCount = json['member_count'] as int
  ..state = json['state'] as int?
  ..Rooms = json['rooms'] as int
  ..guildIntro = json['guild_intro'] as String
  ..clubInfo = ClubInfo.fromJson(json['RoomCount'] as Map<String, dynamic>);

Map<String, dynamic> _$ClubssbModelToJson(ClubssbModel instance) =>
    <String, dynamic>{
      'guild_id': instance.guildId,
      'guild_name': instance.guildName,
      'guild_avatar': instance.guildAvatar,
      'member_count': instance.memberCount,
      'state': instance.state,
      'rooms': instance.Rooms,
      'guild_intro': instance.guildIntro,
      'RoomCount': instance.clubInfo,
    };

ClubssModel _$ClubssModelFromJson(Map<String, dynamic> json) => ClubssModel()
  ..guildId = json['guild_id'] as int
  ..guildName = json['guild_name'] as String
  ..guildAvatar = json['guild_avatar'] as String
  ..memberCount = json['member_count'] as int
  ..rooms = json['rooms'] as int
  ..guildIntro = json['guild_intro'] as String?;

Map<String, dynamic> _$ClubssModelToJson(ClubssModel instance) =>
    <String, dynamic>{
      'guild_id': instance.guildId,
      'guild_name': instance.guildName,
      'guild_avatar': instance.guildAvatar,
      'member_count': instance.memberCount,
      'rooms': instance.rooms,
      'guild_intro': instance.guildIntro,
    };

ClubInfo _$ClubInfoFromJson(Map<String, dynamic> json) => ClubInfo()
  ..clubId = json['clubId'] as String
  ..guildId = json['guild_id'] as int
  ..guildname = json['guild_name'] as String
  ..guildavatar = json['guild_avatar'] as String
  ..membercount = json['member_count'] as int
  ..clubDisplayId = json['clubDisplayId'] as String
  ..clubName = json['clubName'] as String
  ..clubCover = json['clubCover'] as String
  ..clubBg = json['clubBg'] as String?
  ..clubNotice = json['clubNotice'] as String?
  ..clubDes = json['clubDes'] as String
  ..memberCount = json['memberCount'] as int
  ..clubStatus = json['clubStatus'] as int;

Map<String, dynamic> _$ClubInfoToJson(ClubInfo instance) => <String, dynamic>{
      'clubId': instance.clubId,
      'guild_id': instance.guildId,
      'guild_name': instance.guildname,
      'guild_avatar': instance.guildavatar,
      'member_count': instance.membercount,
      'clubDisplayId': instance.clubDisplayId,
      'clubName': instance.clubName,
      'clubCover': instance.clubCover,
      'clubBg': instance.clubBg,
      'clubNotice': instance.clubNotice,
      'clubDes': instance.clubDes,
      'memberCount': instance.memberCount,
      'clubStatus': instance.clubStatus,
    };

ClubOwner _$ClubOwnerFromJson(Map<String, dynamic> json) => ClubOwner()
  ..ownerId = json['ownerId'] as String
  ..ownerDisplayId = json['ownerDisplayId'] as String
  ..ownerName = json['ownerName'] as String
  ..ownerHead = json['ownerHead'] as String;

Map<String, dynamic> _$ClubOwnerToJson(ClubOwner instance) => <String, dynamic>{
      'ownerId': instance.ownerId,
      'ownerDisplayId': instance.ownerDisplayId,
      'ownerName': instance.ownerName,
      'ownerHead': instance.ownerHead,
    };
