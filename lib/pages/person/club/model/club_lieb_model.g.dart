// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'club_lieb_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ClubLiebModel _$ClubLiebModelFromJson(Map<String, dynamic> json) =>
    ClubLiebModel(
      exId: json['ex_id'] as int,
      roomId: json['room_id'] as int,
      image: json['image'] as String?,
      name: json['name'] as String?,
      owner: json['owner'] == null
          ? null
          : Owner.fromJson(json['owner'] as Map<String, dynamic>),
      personNum: json['person_num'] as int?,
      topicName: json['topic_name'] as String?,
      guildId: json['guild_id'] as int,
    );

Map<String, dynamic> _$ClubLiebModelToJson(ClubLiebModel instance) =>
    <String, dynamic>{
      'ex_id': instance.exId,
      'room_id': instance.roomId,
      'guild_id': instance.guildId,
      'image': instance.image,
      'name': instance.name,
      'owner': instance.owner,
      'person_num': instance.personNum,
      'topic_name': instance.topicName,
    };

Owner _$OwnerFromJson(Map<String, dynamic> json) => Owner(
      ownerId: json['owner_id'] as int?,
      ownerImage: json['owner_image'] as String?,
      ownerName: json['owner_name'] as String,
    );

Map<String, dynamic> _$OwnerToJson(Owner instance) => <String, dynamic>{
      'owner_id': instance.ownerId,
      'owner_image': instance.ownerImage,
      'owner_name': instance.ownerName,
    };
