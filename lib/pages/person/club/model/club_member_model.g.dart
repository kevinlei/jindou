// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'club_member_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ClubMemberModel _$ClubMemberModelFromJson(Map<String, dynamic> json) =>
    ClubMemberModel(
      userId: json['user_id'] as String,
      applyType: json['apply_type'] as int,
      userInfo: UserInfo.fromJson(json['user_info'] as Map<String, dynamic>),
      createdAt: json['created_at'] as int,
    );

Map<String, dynamic> _$ClubMemberModelToJson(ClubMemberModel instance) =>
    <String, dynamic>{
      'user_id': instance.userId,
      'apply_type': instance.applyType,
      'user_info': instance.userInfo,
      'created_at': instance.createdAt,
    };

UserInfo _$UserInfoFromJson(Map<String, dynamic> json) => UserInfo(
      displayId: json['display_id'] as int,
      userId: json['user_id'] as String,
      headIcon: json['head_icon'] as String,
      name: json['name'] as String,
      sex: json['sex'] as int,
      birthday: json['birthday'] as int,
    );

Map<String, dynamic> _$UserInfoToJson(UserInfo instance) => <String, dynamic>{
      'display_id': instance.displayId,
      'user_id': instance.userId,
      'head_icon': instance.headIcon,
      'name': instance.name,
      'sex': instance.sex,
      'birthday': instance.birthday,
    };
