import 'package:json_annotation/json_annotation.dart';

part 'club_member_model.g.dart';

@JsonSerializable()
class ClubMemberModel {
  @JsonKey(name: 'user_id')
  final String userId;
  @JsonKey(name: 'apply_type')
  final int applyType;
  @JsonKey(name: 'user_info')
  final UserInfo userInfo;
  @JsonKey(name: 'created_at')
  final int createdAt;

  const ClubMemberModel({
    required this.userId,
    required this.applyType,
    required  this.userInfo,
    required this.createdAt,
  });

  factory ClubMemberModel.fromJson(Map<String, dynamic> json) =>
      _$ClubMemberModelFromJson(json);

  Map<String, dynamic> toJson() => _$ClubMemberModelToJson(this);
}

@JsonSerializable()
class UserInfo {
  @JsonKey(name: 'display_id')
  final int displayId;
  @JsonKey(name: 'user_id')
  final String userId;
  @JsonKey(name: 'head_icon')
  final String headIcon;
  final String name;
  final int sex;
  final int birthday;

  const UserInfo({
    required this.displayId,
    required  this.userId,
    required  this.headIcon,
    required  this.name,
    required  this.sex,
    required  this.birthday,
  });

  factory UserInfo.fromJson(Map<String, dynamic> json) =>
      _$UserInfoFromJson(json);

  Map<String, dynamic> toJson() => _$UserInfoToJson(this);
}
