import 'dart:async';

import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class CodeLoginButton extends StatefulWidget {
  final Future<bool> Function() getCode;

  const CodeLoginButton(this.getCode, {Key? key}) : super(key: key);
  @override
  _CodeLoginButtonState createState() => _CodeLoginButtonState();
}

class _CodeLoginButtonState extends State<CodeLoginButton> {
  bool isGetCoding = true;
  int curSecond = 60;
  int maxSecond = 60;
  Timer? timer;

  @override
  void initState() {
    super.initState();
    startTimeCount();
  }

  @override
  void dispose() {
    if (timer != null) {
      timer!.cancel();
      timer = null;
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    String text = "获取验证码";
    Color color = const Color(0xFFFFFFFF);
    if (isGetCoding) {
      text = "已发送($curSecond)";
      color = const Color(0xFF222222);
    }

    return GestureDetector(
      onTap: onPressButton,
      child: FContainer(
        width: 180.w,
        height: 60.w,
        radius: BorderRadius.circular(30.w),
        gradient: isGetCoding
            ? [const Color(0xFFFF753F), const Color(0xFFFF753F)]
            : [const Color(0xFFFF753F), const Color(0xFFFF753F)],
        gradientBegin: Alignment.centerLeft,
        gradientEnd: Alignment.centerRight,
        align: Alignment.center,
        child: FText(text, size: 26.sp, weight: FontWeight.w600, color: color),
      ),
    );
  }

  void onPressButton() async {
    if (isGetCoding) return;
    bool success = await widget.getCode();
    if (!success) return;
    isGetCoding = true;
    if (!mounted) return;
    setState(() {});
    startTimeCount();
  }

  void startTimeCount() {
    timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      curSecond -= 1;
      if (curSecond < 0) {
        isGetCoding = false;
        curSecond = maxSecond;
        timer.cancel();
      }
      if (!mounted) return;
      setState(() {});
    });
  }
}
