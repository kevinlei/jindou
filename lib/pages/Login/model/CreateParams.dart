class CreateParams {
  int userSex = 1;

  String userNick = "";

  String userHead = "";

  int userAge = 0;

  String userLoc = "";

  String inviteCode = "";

  List tagList = [];

  String channelId = "";

  Map<String, dynamic> toMap() {
    return {
      "nickname": userNick,
      "picture": userHead,
      "sex": userSex,
      "birthday": userAge,
      "city": userLoc,
      "inviteCode": inviteCode,
      "tags": tagList,
      "channelId": channelId,
    };
  }
}
