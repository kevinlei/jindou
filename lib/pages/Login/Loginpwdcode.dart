import 'package:echo/import.dart';
import 'dart:io';
import 'package:f_verification_box/f_verification_box.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class Loginpwdcode extends StatefulWidget {
  final String phone;
  Loginpwdcode({Key? key, required this.phone}) : super(key: key);
  @override
  State<Loginpwdcode> createState() => _LoginpwdcodeState();
}

class _LoginpwdcodeState extends State<Loginpwdcode>
    with WidgetsBindingObserver {
  LoginProvider provider = LoginProvider();
  ScrollController scroll = ScrollController();
  TextEditingController phoneCtr = TextEditingController();
  late final VerificationBox OnSubmitted;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    provider.decodePwd();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => provider,
        builder: (_, __) {
          return Scaffold(
            backgroundColor: const Color(0xFF0B0017),
            body: FContainer(
              padding: EdgeInsets.only(left: 30.w, right: 30.w, bottom: 20.w),
              image: ImageUtils().getAssetsImage("public/huibg"),
              imageFit: BoxFit.fill,
              imageAlign: Alignment.topCenter,
              child: SafeArea(child: loginContent()),
            ),
          );
        });
  }

  Widget loginContent() {
    return Column(children: [
      loginAppbar(),
      Expanded(child: inputContent()),
      CodeButton(requestCode),
      Column(
        children: [
          Consumer<LoginProvider>(builder: (_, pro, __) {
            return FcationButton(
              "下一步",
              isActivate:
                  pro.code.isNotEmpty && pro.code.length == 6 ? true : false,
              padding: 40.w,
              onPress: pro.code.isNotEmpty && pro.code.length == 6
                  ? quickLogin
                  : null,
            );
          }),
        ],
      ),
    ]);
  }

  Widget inputContent() {
    return ListView(controller: scroll, children: [
      SizedBox(height: 98.h),
      Align(
        alignment: Alignment.centerLeft,
        child: Consumer<LoginProvider>(builder: (_, pro, __) {
          return Container(
            padding: EdgeInsets.only(left: 40.w),
            child: FText("填写验证码",
                size: 48.sp,
                color: const Color(0xFF1E1D23),
                weight: FontWeight.bold),
          );
        }),
      ),
      SizedBox(height: 30.h),
      Container(
        padding: EdgeInsets.only(left: 40.w),
        child: Row(
          children: [
            FText("已发送验证码至 ",
                size: 28.sp, color: const Color(0xFF222222).withOpacity(0.4)),
            FText(widget.phone.replaceRange(3, 7, "****"),
                size: 28.sp, color: const Color(0xFF222222)),
          ],
        ),
      ),
      SizedBox(height: 40.h),
      Container(
        height: 150.h,
        child: Consumer<LoginProvider>(builder: (_, pro, __) {
          return VerificationBox(
            borderWidth: 0.5,
            unfocus: false,
            //      onSubmitted:Function
            onChanged: (value) {
              setState(() {
                pro.code = value;
                debugPrint(value);
              });
            },
          );
        }),
      ),
    ]);
  }

  Future<void> onPwdInput(String value) async {
    value = value.replaceAll(" ", "").trim();
    provider.onPwdInput(value);
  }

  Future<bool> requestCode() async {
    bool success = false;
    await DioUtils().httpRequest(
      NetConstants.loginCode,
      method: DioMethod.POST,
      params: {
        "phone": widget.phone,
        "zone": "",
        "temp":SMSType.SMS_TEMPLATE_LOGIN.value,
        "platform": Platform.isAndroid ? "android" : "ios"
      },
      onSuccess: (_) => success = true,
    );
    return success;
  }

  @override
  void didChangeMetrics() {
    super.didChangeMetrics();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      if (!mounted) return;
      double keyboardHeight = MediaQuery.of(context).viewInsets.bottom;
      if (keyboardHeight <= 0) return;
      double max = scroll.position.maxScrollExtent;
      scroll.animateTo(max,
          duration: const Duration(milliseconds: 200), curve: Curves.linear);
      Future.delayed(const Duration(milliseconds: 200), () {
        double max2 = scroll.position.maxScrollExtent;
        scroll.animateTo(max2,
            duration: const Duration(milliseconds: 1), curve: Curves.linear);
      });
    });
  }

  @override
  void dispose() {
    scroll.dispose();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  //获取验证码
  Future<void> quickLogin() async {
    // if (!FuncUtils().isMobile(widget.phone)) {
    //   WidgetUtils().showToast("请输入正确的手机号");
    // }
    DioUtils().asyncHttpRequest(
      NetConstants.validateCode,
      method: DioMethod.POST,
      params: {
        "phone": widget.phone,
        "code": provider.code,
        "platform": Platform.isAndroid ? "android" : "ios"
      },
      onSuccess: onReportSuccess,
    );
  }

  void onReportSuccess(response) {
    WidgetUtils().pushPage(Loginrestpasswrd(
      passwrdkey: response.toString(),
      phone: widget.phone,
    ));
  }

  Widget loginAppbar() {
    return Row(children: [
      btnSwitch(),
      const Spacer(),
    ]);
  }

  Widget btnSwitch() {
    return GestureDetector(
      onTap: onPressSwitch,
      child: FContainer(
        height: 50.w,
        child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
          LoadAssetImage("login/blckback", width: 60.w, height: 60.w),
        ]),
      ),
    );
  }

  void onPressSwitch() {
    FocusManager.instance.primaryFocus?.unfocus();
    WidgetUtils().popPage();
  }
}
