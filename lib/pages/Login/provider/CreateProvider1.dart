import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class CreateProvider1 extends ChangeNotifier {
  String userNick = "";
  String userHead = "";
  int userSex = 0;
  int userAge = 0;
  String userLoc = "";
  String inviteCode = "";

  bool canCreate = false;

  void initUserData(int sex, String name, String head, int userage) {
    userSex = sex;
    userNick = name;
    userHead = head;
    userAge = userage;
  }

  void onNameInput(value) {
    userNick = value;
    _checkInput();
  }

  void onAgeInput(value) {
    userAge = value;
    _checkInput();
  }

  void onLocInput(value) {
    userLoc = value;
    _checkInput();
  }

  void onCodeInput(value) {
    inviteCode = value;
    _checkInput();
  }

  void _checkInput() {
    bool canClick = true;
    if (userNick.isEmpty || userNick.length > 10) {
      canClick = false;
    }
    if (userAge == 0) {
      canClick = false;
    }
    if (userLoc.isEmpty) {
      canClick = false;
    }
    if (canClick != canCreate) {
      canCreate = canClick;
      notifyListeners();
    }
  }

  // 用户信息
  Future<bool> infoUserpst() async {
    bool success = false;
    await DioUtils().httpRequest(
      NetConstants.getUserInfo,
      method: DioMethod.GET,
      params: null,
      onSuccess: (res) => onLoginSuccess(res),
    );
    return success;
  }

  Future<void> onLoginSuccess(response) async {
    UserConstants.userginInfo = LoginUserModel.fromJson(response);
    userNick = UserConstants.userginInfo?.info.name ?? "";
    userHead = UserConstants.userginInfo?.info.headIcon ?? "";
    userSex = UserConstants.userginInfo?.info.sex ?? 0;
    userAge = UserConstants.userginInfo?.info.birthday ?? 0;
  }

  void getUserInfo() async {
    FocusManager.instance.primaryFocus?.unfocus();
    bool state = false;
    await DioUtils().httpRequest(NetConstants.getUserInfo,
        method: DioMethod.GET,
        params: {"userId": ""},
        onSuccess: (response) async {
          if (response == null) {
            state = true;
          } else {
            UserConstants.userginInfo = LoginUserModel.fromJson(response);
            // AppManager().connectMq();
            AppManager().loginSDK();
            WidgetUtils().pushRemove(const NavigatorPage());
            WidgetUtils().cancelLoading();

            // 注销冷静期内
            // if (UserConstants.userginInfo?.info.isLogOff == true) {
            //   bool accept = false;

            //   if (!accept) {
            //     AppConstants.accessToken = "";
            //     AppConstants.refreshToken = "";
            //     SharedPreferences preferences =
            //         await SharedPreferences.getInstance();
            //     await preferences.clear();
            //     preferences.setBool(SharedKey.KEY_UNIQUE.value, false);
            // } else {
            //   // AppManager().connectMq();
            //   AppManager().loginSDK();
            //   WidgetUtils().pushRemove(const NavigatorPage());
            //   WidgetUtils().cancelLoading();
            // }
          }
        },
        onError: (code, msg) => debugPrint("getUserInfo请求错误$msg"));
  }

  void onCreate() async {
    if (userNick.isEmpty || userNick.length > 10) {
      return WidgetUtils().showToast("请填写昵称最多10个字");
    }
    if (userNick.isEmpty || userNick.length < 2) {
      return WidgetUtils().showToast("请填写昵称最少2个字");
    }
    if (userAge == 0) {
      return WidgetUtils().showToast("请"
          "选择生日");
    }
    FocusManager.instance.primaryFocus?.unfocus();
    // WidgetUtils().showLoading();
    // bool lawful = await FuncUtils()
    //     .checkTextLawful(userNick, CheckTxtType.TXT_NICK_NAME, isReg: true);
    // WidgetUtils().cancelLoading();
    // if (!lawful) return;

    CreateParams params = CreateParams();
    params.userSex = userSex;
    params.userNick = userNick;
    params.userHead = userHead;
    params.userAge = userAge;
    params.userLoc = userLoc;
    params.inviteCode = inviteCode;
    // WidgetUtils().pushPage(CreateUser3(params: params));
  }
}
