import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:flutter_html/flutter_html.dart';


class ProtocolPage extends StatefulWidget {
  final ProtocolType type;

  const ProtocolPage({Key? key, required this.type}) : super(key: key);

  @override
  _ProtocolPageState createState() => _ProtocolPageState();
}

class _ProtocolPageState extends State<ProtocolPage> {
  String htmlText = "";

  @override
  void initState() {
    super.initState();
    requestProtocol();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF5F6F7),
      appBar: CommonAppBar(
        title: widget.type.text,
        textColor: Colors.black,
        elevation: 1,
        backgroundColor: Color(0xFFF5F6F7),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(horizontal: 25.w),
        child: Html(data: htmlText),
      ),
    );
  }

  void requestProtocol() {
    DioUtils().asyncHttpRequest(
      NetConstants.protocol,
      method: DioMethod.GET,
      params: {"id": widget.type.proId},
      onSuccess: onUpdateText,
    );
  }

  void onUpdateText(response) {
    htmlText = response?["content"] ?? "";
    if (!mounted) return;
    setState(() {});
  }
}
