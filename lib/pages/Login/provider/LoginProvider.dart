import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class LoginProvider extends ChangeNotifier {
  bool onLoginCode = true;
  bool rememberPwd = false;
  bool canLogin = false;
  String phone = "";
  String code = "";
  String pwd = "";
  String passpwd = "";
  String neirongtext = "";
  //登录接口记录服务器返回code
  String phonecode = "";
  // 上次登录记住的手机号
  String sharedPhone = "";

  // 上次登录记住的密码
  String sharedPwd = "";

  // 昵称
  String name = "";
  // 头像
  String HeadIcon = "";
  // 生日
  int birthday = 0;
  // 性别
  int sex = 0;
//反馈图片数组
  List<String> urlList = [];

  bool hasAccept = false;

  int chargeNum = 0;

  double? shouNum = 0.0;

  void onChargeSelect(int gold) => this.chargeNum = gold;

  void onRemember(remember) => rememberPwd = remember;

  void onneirongtextInput(String value) {
    if ((neirongtext.isEmpty && value.isNotEmpty) ||
        (neirongtext.isNotEmpty && value.isEmpty)) {
      notifyListeners();
    }
    neirongtext = value;
  }

  void onPhoneInput(String value) {
    if ((phone.isEmpty && value.isNotEmpty) ||
        (phone.isNotEmpty && value.isEmpty)) {
      notifyListeners();
    }
    phone = value;
    _checkInput();
  }

  void onCodeInput(String value) {
    if (value.length == 6) {
      code = value;
    } else {
      code = "";
    }
    notifyListeners();
    _checkInput();
  }

  void onPwdInput(String value) {
    pwd = value;
    notifyListeners();
    _checkInput();
  }

  void onpasspwdInput(String value) {
    passpwd = value;
    notifyListeners();
    _checkInput();
  }

  void onSwitch() {
    onLoginCode = !onLoginCode;
    notifyListeners();
    _checkInput();
  }

  // 更新用户财富信息
  void requestDynamicInfo() async {
    await DioUtils().httpRequest(
      NetConstants.gradeUserMoney,
      method: DioMethod.GET,
      onSuccess: updateInfo,
    );
  }

  void updateInfo(response) {
    if (response == null) return;
    UserConstants.userginInfo?.goldInfo.gold = response['gold'];
    UserConstants.userginInfo?.goldInfo.goldLock = response['gold_lock'];
    UserConstants.userginInfo?.goldInfo.gains = response['gains'];
    UserConstants.userginInfo?.goldInfo.gainsLock = response['gains_lock'];
    UserConstants.userginInfo?.goldInfo.goldable = response['gold_able'];
    UserConstants.userginInfo?.goldInfo.gainsable = response['gains_able'];
    shouNum = UserConstants.userginInfo?.goldInfo.gainsable;
    notifyListeners();
  }

  // 请求验证码
  Future<bool> requestPhoneCode(int sendType) async {
    // if (!FuncUtils().isMobile(phone)) {
    //   WidgetUtils().showToast("请输入正确的手机号");
    //   return false;
    // }
    bool success = false;
    await DioUtils().httpRequest(
      NetConstants.loginCode,
      method: DioMethod.POST,
      params: {
        "phone": phone,
        "zone": "",
        "temp": sendType == 1
            ? SMSType.SMS_TEMPLATE_PASSWORD.value
            : SMSType.SMS_TEMPLATE_LOGIN.value,
        "send_type": sendType,
        "platform": Platform.isAndroid ? "android" : "ios"
      },
      onSuccess: (_) => success = true,
    );
    return success;
  }

  // 验证码/密码登录
  Future<bool> requestPhones(String login_type) async {
    bool success = false;
    await DioUtils().httpcodeRequest(
      NetConstants.loginUser,
      method: DioMethod.POST,
      isSuccess: true,
      params: {
        "phone": phone,
        "login_type": login_type,
        "platform": Platform.isAndroid ? "android" : "ios",
        "code": code,
        "pass_word": pwd
      },
      onSuccess: (res, code, msg) => onLoginSuccess(res, code, msg),
    );
    return success;
  }

  void decodePwd() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String? pwd = preferences.getString(SharedKey.KEY_PWD_SAVE.value);
    if (pwd == null || pwd.isEmpty) return;
    try {
      Map pwdMap = jsonDecode(pwd);
      sharedPhone = pwdMap["phone"];
      sharedPwd = pwdMap["pwd"];
      rememberPwd = sharedPwd.isNotEmpty;
    } catch (e) {
      debugPrint("json解析错误");
    }
  }

  void _checkInput() {
    bool _canClick = true;
    if (!FuncUtils().isMobile(phone)) {
      _canClick = false;
    }
    if (onLoginCode) {
      if (!FuncUtils().isMobileCode(code)) {
        _canClick = false;
      }
    } else {
      if (pwd.isEmpty) {
        _canClick = false;
      }
    }

    if (_canClick != canLogin) {
      canLogin = _canClick;
      notifyListeners();
    }
  }

  // 检验
  Future<bool> checkInput() async {
    // if (!FuncUtils().isMobile(phone)) {
    //   WidgetUtils().showToast("请输入正确的手机号");
    //   return false;
    // }
    if (!FuncUtils().isMobileCode(code) && onLoginCode) {
      WidgetUtils().showToast("请输入正确的验证码");
      return false;
    }
    if (pwd.isEmpty && !onLoginCode) {
      WidgetUtils().showToast("请输入密码");
      return false;
    }
    if (!hasAccept && !(FuncUtils().isMobileCode(code) && onLoginCode)) {
      WidgetUtils().showToast("请先同意用户协议");
      return false;
    }
    return true;
  }

  Map getLoginParams() {
    if (onLoginCode) {
      return {
        "client_id": Platform.isAndroid ? "android" : "ios",
        "grant_type": "sms_code",
        "phone": phone,
        "sms_code": code,
        "phone_zone": "+86",
        "machine_code": AppConstants.machineCode,
        "os_type": Platform.isAndroid ? 1 : 2,
        "client_secret":
            Platform.isAndroid ? "mobile_secret_android" : "mobile_secret_ios",
        "scope":
            "openid user game offline_access chat social common guild payment",
      };
    }
    return {
      "client_id": Platform.isAndroid ? "android" : "ios",
      "grant_type": "pwd",
      "username": phone,
      "pwd": pwd,
      "machine_code": AppConstants.machineCode,
      "os_type": Platform.isAndroid ? 1 : 2,
      "client_secret":
          Platform.isAndroid ? "mobile_secret_android" : "mobile_secret_ios",
      "scope":
          "openid user game offline_access chat social common guild payment",
    };
  }

  Future<void> onLoginSuccess(response, code, msg) async {
    phonecode = code.toString();
    //登录验证码code不同处理
    if (int.parse(code.toString()) == 101000) {
      UserConstants.userginInfo = LoginUserModel.fromJson((response));
      AppConstants.accessToken = UserConstants.userginInfo?.token ?? "";
      SharedPreferences preferences = await SharedPreferences.getInstance();
      preferences.setString(
          SharedKey.KEY_TOKEN.value, UserConstants.userginInfo?.token ?? "");
      preferences.setString(SharedKey.KEY_CODE.value, code.toString());
      HeadIcon = UserConstants.userginInfo?.info.headIcon ?? "";
      name = UserConstants.userginInfo?.info.name ?? "";
      birthday = UserConstants.userginInfo?.info.birthday ?? 0;
      sex = UserConstants.userginInfo?.info.sex ?? 0;
      // 注册统计
      Future.delayed(const Duration(seconds: 0), () async {
        AppManager().xinstallFlutterPlugin.reportRegister();
        AppManager().xinstallFlutterPlugin.reportEventWhenOpenDetailInfo(
            "register", 1, "userId: ${UserConstants.userginInfo!.info.userId}");
      });
      WidgetUtils().pushPage(Loginpersonal(
        name,
        birthday,
        sex,
        HeadIcon,
      ));
    } else if (int.parse(code.toString()) == 1) {
      AppConstants.accessToken = response["token"].toString();
      UserConstants.userginInfo = LoginUserModel.fromJson((response));
      SharedPreferences preferences = await SharedPreferences.getInstance();
      preferences.setString(
          SharedKey.KEY_TOKEN.value, response["token"].toString());
      preferences.setString(SharedKey.KEY_CODE.value, code.toString());
      // AppManager().connectMq();
      AppManager().loginSDK();
      WidgetUtils().pushRemove(const NavigatorPage());
    } else {
      WidgetUtils().showToast(msg);
    }

    return code;
  }

// void getUserInfo() async {
//   FocusManager.instance.primaryFocus?.unfocus();
//   bool state = false;
//   await DioUtils().httpRequest(NetConstants.getUserInfo,
//       method: DioMethod.POST,
//       params: {"userId": ""},
//       onSuccess: (response) async {
//         if (response == null) {
//           state = true;
//         } else {
//           UserConstants.userInfo = UserInfoModel.fromJson(response);
//
//           // 注销冷静期内
//           if (UserConstants.userInfo?.isLogOff == true) {
//             bool accept = false;
//             await WidgetUtils().showFDialog(MinimalistMode(
//               "您的账号已提交注销申请",
//               "您的账号将于${UserConstants.userInfo?.logOffTime}后注销; 如您想放弃注销流程，请点击“放弃注销”; 如确定注销此账号，点击“了解”后可通过其他账号进行登录。",
//               (bool state) async {
//                 if (state) {
//                   await DioUtils().httpRequest(
//                     NetConstants.calLogOff,
//                     onSuccess: (res) => accept = true,
//                   );
//                 }
//               },
//               confirmText: "放弃注销",
//               cancelText: "了解",
//             ));
//
//             if (!accept) {
//               AppConstants.accessToken = "";
//               AppConstants.refreshToken = "";
//               SharedPreferences preferences =
//                   await SharedPreferences.getInstance();
//               await preferences.clear();
//               preferences.setBool(SharedKey.KEY_UNIQUE.value, false);
//             } else {
//               // await AppManager().loginEmClient();
//               AppManager().connectMq();
//               WidgetUtils().pushRemove(NavigatorPage());
//               WidgetUtils().cancelLoading();
//             }
//           } else {
//             // await AppManager().loginEmClient();
//             AppManager().connectMq();
//             WidgetUtils().pushRemove(NavigatorPage());
//             WidgetUtils().cancelLoading();
//           }
//         }
//       },
//       onError: (code, msg) => debugPrint("getUserInfo请求错误$msg"));
//   if (state) getNick();
// }
// }

// void getNick() async {
// String userNick = "";
// await DioUtils().httpRequest(NetConstants.createName,
//     onSuccess: (response) {
//       String id = response?["displayId"] ?? "";
//       String start = response?["startName"] ?? "";
//       userNick = start + id;
//     },
//     onError: (code, msg) => debugPrint("userNick请求错误$msg"));
// if (userNick.isEmpty)
//   return WidgetUtils().showToast('网络错误，用户注册失败，请尝试重新打开应用后注册!');
// WidgetUtils().pushPage((CreateUser1(userNick)));
// }
}
