import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';

class Loginpersonal extends StatefulWidget {
  String name;
  int birthday;
  int sex;
  String headStr;
  Loginpersonal(this.name, this.birthday, this.sex, this.headStr, {Key? key})
      : super(key: key);
  @override
  _LoginpersonalState createState() => _LoginpersonalState();
}

class _LoginpersonalState extends State<Loginpersonal>
    with WidgetsBindingObserver {
  String inputText = "";
  TextEditingController phoneCtr = TextEditingController();
  TextEditingController NameCtr = TextEditingController();

  final FocusNode focusNode = FocusNode();
  CreateProvider1 provider = CreateProvider1();
  ScrollController scroll = ScrollController();
  final ImagePicker picker = ImagePicker();
  int _groupValue = 0; // 当前组选中的值
  List sampleData = [];
  final int sexNum = 0;
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);

    provider.initUserData(
        widget.sex, widget.name, widget.headStr, widget.birthday);
    // provider.decodePwd();
    provider.infoUserpst();
    sexNum == provider.userSex;
    sampleData.add(RadioModel(
      provider.userSex == 0 ? false : true,
      '男',
    ));
    sampleData.add(RadioModel(
      provider.userSex == 0 ? true : false,
      '女',
    ));
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => provider,
        builder: (_, __) {
          return Scaffold(
            backgroundColor: Colors.white,
            body: GestureDetector(
              onTap: () {
                FocusScope.of(context).requestFocus(FocusNode());
              },
              child: FContainer(
                padding: EdgeInsets.only(left: 30.w, right: 30.w, bottom: 20.w),
                image: ImageUtils().getAssetsImage("public/huibg"),
                imageFit: BoxFit.fill,
                imageAlign: Alignment.topCenter,
                child: SafeArea(child: loginContent()),
              ),
            ),
          );
        });
  }

  Widget loginContent() {
    return Container(
      child: Column(children: [
        loginAppbar(),
        const Divider(),
        SizedBox(child: inputContent(), ),
        const Spacer(),
        Consumer<CreateProvider1>(builder: (_, pro, __) {
          return FcationButton("提交",
              isActivate: pro.userNick.isNotEmpty,
              txtColor: Colors.white,
              padding: 0,
              radius: 90.w,
              onPress: pro.userNick.isNotEmpty ? onTapConfirm : null);
        }),
      ]),
    );
  }

  void _changed(int value) {
    setState(() {
      _groupValue = value;
    });
  }

  Widget inputContent() {
    return Column(
      children: [
        Consumer<CreateProvider1>(builder: (_, pro, __) {
          return GestureDetector(
            onTap: onTapHead,
            child: newheadWidget(),
          );
        }),
        SizedBox(height: 20.h),
        Row(
          children: [
            SizedBox(width: 50.h),
            Text("昵称",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: const Color(0xFF222222).withOpacity(0.7),
                    fontSize: 28.sp)),
            SizedBox(width: 20.h),
            Expanded(
              child: Consumer<CreateProvider1>(builder: (_, pro, __) {
                return CreateInput(true,
                    userNick: provider.userNick, onInput: provider.onNameInput);
              }),
            ),
            SizedBox(width: 60.h),
          ],
        ),
        SizedBox(child:Row(
          children: [
            SizedBox(width: 50.h),
            Text("生日",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color:  Color(0xFF222222).withOpacity(0.7),
                    fontSize: 28.sp)),
            SizedBox(width: 20.h),
            Expanded(child: CreateAge(provider.onAgeInput, provider.userAge)),
            SizedBox(width: 60.h),
          ],
        ),),

        Row(
          children: [
            SizedBox(width: 50.h),
            Text("性别",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: const Color(0xFF222222).withOpacity(0.7),
                    fontSize: 28.sp)),
            SizedBox(width: 20.h),
            Row(
              children: List<Widget>.generate(sampleData.length, (index) {
                return InkWell(
                  // child: _buttonstyle(sampleData[index]),
                  child: SixButton("${sampleData[index].buttonText}",
                      isActivate: sampleData[index].isSelected,
                      txtColor: Colors.white,
                      padding: 5.w,
                      radius: 90.w,
                      onPress: () => onTapRespst(index)),
                  // onTap: (){
                  //   setState(() {
                  //     sampleData.forEach((element) => element.isSelected = false);
                  //     debugPrint(sampleData[index].buttonText);
                  //     sampleData[index].isSelected = true;
                  //   });
                  // },
                );
              }),
            ),
            SizedBox(width: 60.h),
          ],
        ),

      ],
    );
  }

  Widget _buttonstyle(sampleData) {
    return Container(
      padding: const EdgeInsets.only(right: 20),
      child: Stack(
        children: [
          Container(
            height: 20,
            width: 30,
            padding: const EdgeInsets.symmetric(vertical: 10),
            decoration: BoxDecoration(
              borderRadius: const BorderRadius.all(Radius.circular(4)),
              border: Border.all(
                  width: 1,
                  color: sampleData.isSelected ? Colors.blue : Colors.grey),
            ),
          ),
          SizedBox(
            height: 20,
            width: 30,
            child: Center(child: Text("${sampleData.buttonText}")),
          ),
        ],
      ),
    );
  }

  void onTapRespst(int index) {
    setState(() {
      for (var element in sampleData) {
        element.isSelected = false;
      }
      sampleData[index].isSelected = true;
      if (index == 0) {
        widget.sex = 1;
        provider.userSex = 1;
      } else {
        widget.sex = 0;
        provider.userSex = 0;
      }
    });
  }

  Widget newheadWidget() {
    ImageProvider image = ImageUtils().getImageProvider(
        NetConstants.ossPath + provider.userHead,
        holderImg: "login/default");
    return FContainer(
      width: 190.w,
      height: 190.w,
      color: Colors.white12,
      radius: BorderRadius.circular(190.w),
      imageFit: BoxFit.cover,
      image: image,
      padding: EdgeInsets.only(top: 130.w),
      child: Visibility(
        visible: true,
        child: LoadAssetImage("login/camera", width: 20.w),
      ),
    );
  }

  Widget loginAppbar() {
    return Row(children: [
      Expanded(
          child: Align(
        alignment: Alignment.centerLeft,
        child: IconButton(
            onPressed: onPressSwitch,
            icon: const Icon(Icons.arrow_back_ios, color: Colors.black)),
      )),
      Expanded(
        child: Text("完善个人信息",
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Colors.black,
                fontSize: 36.sp,
                fontWeight: FontWeight.w600)),
      ),
      Expanded(
          child: GestureDetector(
        onTap: onPrescript,
        child: FContainer(
          height: 60.w,
          child: Column(
            children: [
              SizedBox(height: 12.w),
              Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                FText("跳过   ",
                    size: 28.sp,
                    color: const Color(0xF3222222).withOpacity(0.6))
              ]),
            ],
          ),
        ),
      ))
    ]);
  }

  void onPressSwitch() {
    FocusManager.instance.primaryFocus?.unfocus();
    WidgetUtils().popPage();
  }

//跳过按钮
  void onPrescript() {
    // AppManager().connectMq();
    AppManager().loginSDK();
    WidgetUtils().pushRemove(const NavigatorPage());
  }

  void onTapHead() async {
    bool accept = await FuncUtils().requestPermission(Permission.photos);
    if (!accept) return;

    XFile? file = await picker.pickImage(source: ImageSource.gallery);
    if (file == null || file.path.isEmpty) return;
    CroppedFile? crop = await ImageCropper().cropImage(
      sourcePath: file.path,
      compressQuality: 100,
      cropStyle: CropStyle.circle,
      uiSettings: [
        AndroidUiSettings(toolbarTitle: '裁剪', statusBarColor: Colors.white),
        IOSUiSettings(title: '裁剪')
      ],
    );
    if (crop == null || crop.path.isEmpty) return;

    WidgetUtils().showLoading();
    String? url =
        await FuncUtils().requestUpload(crop.path, UploadType.USER_IMAGE);
    if (url == null) return WidgetUtils().cancelLoading();
    if (url.isEmpty) {
      WidgetUtils().showToast("头像上传失败");
      WidgetUtils().cancelLoading();
      return;
    }
    // bool lawfulImg =
    // await FuncUtils().checkImageLawful([url], CheckImgType.IMG_HEAD);
    // if (!lawfulImg) return WidgetUtils().cancelLoading();
    debugPrint(url);
    provider.userHead = url;
    setState(() {});
    WidgetUtils().cancelLoading();
  }

  @override

  Future<void> onPhoneInput(String value) async {
    value = value.replaceAll(" ", "").trim();
    // provider.onPhoneInput(value);
  }

  Widget phoneSuffix() {
    return Consumer<CreateProvider1>(builder: (_, pro, __) {
      Widget btn = GestureDetector(
        onTap: onClean,
        child: FContainer(
          width: 60.w,
          height: 60.w,
          child: const Icon(Icons.clear, color: Color(0xFFBBBBBB)),
        ),
      );
      return Visibility(visible: pro.userHead.isNotEmpty, child: btn);
    });
  }

  void onClean() {
    phoneCtr.clear();
    // provider.onPhoneInput("");
  }

  @override
  void dispose() {
    phoneCtr.dispose();
    NameCtr.dispose();
    scroll.dispose();
    super.dispose();
  }

  void onTapConfirm() {
    //提交数据
    // if (inputText.trimRight().isEmpty) return WidgetUtils().showToast("请输入内容");
    requestReportUser();
  }

  void requestReportUser() {
    if (widget.name.isEmpty) return;
    DioUtils().asyncHttpRequest(
      NetConstants.getUserupdate,
      method: DioMethod.POST,
      params: {
        "name": provider.userNick,
        "head_icon": provider.userHead,
        "sex": provider.userSex,
        "birthday": provider.userAge,
        "desc": inputText
      },
      onSuccess: onReportSuccess,
    );
  }

  void onReportSuccess(response) {
    // AppManager().connectMq();
    AppManager().loginSDK();
    WidgetUtils().pushRemove(const NavigatorPage());
  }
}

class RadioModel {
  bool isSelected;
  final String buttonText;

  RadioModel(
    this.isSelected,
    this.buttonText,
  );
}
