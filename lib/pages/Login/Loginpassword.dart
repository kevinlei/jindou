import 'package:echo/import.dart';
import 'package:echo/pages/Login/widgets/Loinputpassword.dart';
import 'package:flutter/material.dart';

class Loginpassword extends StatefulWidget {
  const Loginpassword({super.key});

  @override
  State<Loginpassword> createState() => _LoginpasswordState();
}

class _LoginpasswordState extends State<Loginpassword>
    with WidgetsBindingObserver {
  TextEditingController phoneCtr = TextEditingController();
  TextEditingController pwdCtr = TextEditingController();
  LoginProvider provider = LoginProvider();
  ScrollController scroll = ScrollController();
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    provider.decodePwd();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => provider,
        builder: (_, __) {
          return Scaffold(
            backgroundColor: const Color(0xFF0B0017),
            body: FContainer(
              padding: EdgeInsets.only(left: 30.w, right: 30.w, bottom: 20.w),
              image: ImageUtils().getAssetsImage("public/huibg"),
              imageFit: BoxFit.fill,
              imageAlign: Alignment.topCenter,
              child: SafeArea(child: loginContent()),
            ),
          );
        });
  }

  Widget loginContent() {
    return Column(children: [
      loginAppbar(),
      Expanded(child: inputContent()),
      Column(
        children: [
          btngroup(),
          Consumer<LoginProvider>(
            builder: (_, pro, __) {
              return FcationButton(
                pro.phone.isNotEmpty && pro.pwd.isNotEmpty ? "登录" : "下一步",
                isActivate: pro.phone.isNotEmpty && pro.pwd.isNotEmpty,
                padding: 40.w,
                onPress: (pro.phone.isNotEmpty && pro.pwd.isNotEmpty) == true
                    ? quickLogin
                    : null,
              );
            },
          ),
          SizedBox(height: 100.h),
        ],
      ),
      FProSAccept(
        const [
          ProtocolType.PRO_USER,
          ProtocolType.PRO_PRIVACY,
          ProtocolType.FUTKAN,
        ],
        onAccept: onAccept,
      ),
    ]);
  }

  // 密码登录
  Future<void> quickLogin() async {
    bool state = provider.hasAccept;
    if (!state) return WidgetUtils().showToast("请先同意用户协议");
    FocusManager.instance.primaryFocus?.unfocus();
    if (provider.onLoginCode) {
      bool result = await provider.requestPhones("pwd");
      if (result) {}
    }
  }

  Widget loginAppbar() {
    return Row(children: [
      btnSwitch(),
      const Spacer(),
    ]);
  }

  Widget btnSwitch() {
    return GestureDetector(
      onTap: onPressSwitch,
      child: FContainer(
        height: 50.w,
        child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
          LoadAssetImage("login/blckback", width: 60.w, height: 60.w),
        ]),
      ),
    );
  }

  void onPressSwitch() {
    FocusManager.instance.primaryFocus?.unfocus();
    WidgetUtils().popPage();
  }

  Widget inputContent() {
    Widget nextInput = Consumer<LoginProvider>(builder: (_, pro, __) {
      return Loinputpassword(
        inputType: LoginInputType.INPUT_PWD,

        onInput: onPwdInput,
        controller: pwdCtr,
        // suffix: pro.onLoginCode ? CodeButton(pro.requestPhoneCode) : null,
      );
    });
    return ListView(
      controller: scroll,
      children: [
        SizedBox(height: 98.h),
        Align(
          alignment: Alignment.centerLeft,
          child: Consumer<LoginProvider>(builder: (_, pro, __) {
            return Container(
              padding: EdgeInsets.only(left: 40.w),
              child: FText("账号登录",
                  size: 48.sp,
                  color: const Color(0xFF1E1D23),
                  weight: FontWeight.bold),
            );
          }),
        ),
        SizedBox(height: 40.h),
        Loinputpassword(
          key: const Key("INPUT_HAO"),
          controller: phoneCtr,
          onInput: onPhoneInput,
          suffix: phoneSuffix(),
          inputType: LoginInputType.INPUT_HAO,
        ),
        Consumer<LoginProvider>(
          builder: (_, pro, __) {
            return nextInput;
          },
        ),
        Row(
          children: [
            const FText(''),
            const Spacer(),
            btnright(),
            SizedBox(width: 70.h),
          ],
        ),
      ],
    );
  }

  Widget btnright() {
    return GestureDetector(
      onTap: onPrescript,
      child: FContainer(
        height: 60.w,
        child: Column(
          children: [
            SizedBox(height: 12.w),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              FText("忘记密码?",
                  size: 28.sp, color: const Color(0xF3222222).withOpacity(0.6))
            ]),
          ],
        ),
      ),
    );
  }

  void onPrescript() {
    WidgetUtils().pushPage(const Loginforget());
  }

  Widget btngroup() {
    return GestureDetector(
      onTap: onPresbtngroup,
      child: FContainer(
        height: 60.w,
        child: Column(
          children: [
            SizedBox(height: 12.w),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              FText("验证码登录 ",
                  size: 28.sp,
                  weight: FontWeight.bold,
                  color: const Color(0xFF222222)),
              LoadAssetImage("login/group1", width: 20.w, height: 20.w),
            ]),
          ],
        ),
      ),
    );
  }

  void onPresbtngroup() {
    WidgetUtils().pushPage(const Loginipone());
  }

  void onAccept(accept) => provider.hasAccept = accept;

  @override
  void didChangeMetrics() {
    super.didChangeMetrics();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      if (!mounted) return;
      double keyboardHeight = MediaQuery.of(context).viewInsets.bottom;
      if (keyboardHeight <= 0) return;
      double max = scroll.position.maxScrollExtent;
      scroll.animateTo(max,
          duration: const Duration(milliseconds: 200), curve: Curves.linear);
      Future.delayed(const Duration(milliseconds: 200), () {
        double max2 = scroll.position.maxScrollExtent;
        scroll.animateTo(max2,
            duration: const Duration(milliseconds: 1), curve: Curves.linear);
      });
    });
  }

  @override
  void dispose() {
    phoneCtr.dispose();
    scroll.dispose();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  Future<void> onPhoneInput(String value) async {
    value = value.replaceAll(" ", "").trim();
    provider.onPhoneInput(value);
  }

  Future<void> onPwdInput(String value) async {
    value = value.replaceAll(" ", "").trim();
    provider.onPwdInput(value);
  }

  Widget phoneSuffix() {
    return Consumer<LoginProvider>(builder: (_, pro, __) {
      Widget btn = GestureDetector(
        onTap: onClean,
        child: FContainer(
          width: 60.w,
          height: 60.w,
          child: const Icon(Icons.clear, color: Color(0xFFBBBBBB)),
        ),
      );
      return Visibility(visible: pro.phone.isNotEmpty, child: btn);
    });
  }

  void onClean() {
    phoneCtr.clear();
    provider.onPhoneInput("");
  }

  Widget phoneLead() {
    return FContainer(
      padding: EdgeInsets.only(right: 10.w),
      margin: EdgeInsets.only(right: 10.w),
      border: const Border(right: BorderSide(color: Color(0xFFBBBBBB))),
      child: FText("+86",
          size: 34.sp, color: Colors.white, weight: FontWeight.bold),
    );
  }
}
