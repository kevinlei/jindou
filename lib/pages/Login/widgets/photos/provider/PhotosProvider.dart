import 'package:flutter/material.dart';

class PhotosProvider extends ChangeNotifier {
  int maxCount = 1;

  // 选择的照片
  List<int> selectList = [];

  bool hasSelected(int index) {
    return selectList.contains(index);
  }

  void onPressPhoto(int index) {
    if (hasSelected(index)) {
      selectList.remove(index);
    } else {
      if (maxCount == 1) {
        selectList.clear();
        selectList.add(index);
      } else {
        if (selectList.length >= maxCount) return;
        selectList.add(index);
      }
    }
    notifyListeners();
  }

  void onPressCancel() {
    if (selectList.isEmpty) return;
    selectList.clear();
    notifyListeners();
  }
}
