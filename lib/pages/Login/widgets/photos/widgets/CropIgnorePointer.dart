import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class CropIgnorePointer extends SingleChildRenderObjectWidget {
  const CropIgnorePointer(
      {Key? key,
      Widget? child,
      this.unIgnoreRectList,
      this.ignoringSemantics = true})
      : super(key: key, child: child);

  final bool ignoringSemantics;
  final List<Rect>? unIgnoreRectList;

  @override
  PhotoCropRenderIgnorePointer createRenderObject(BuildContext context) {
    return PhotoCropRenderIgnorePointer(
      unIgnoreRect: unIgnoreRectList,
      ignoring: ignoringSemantics,
    );
  }

  @override
  void updateRenderObject(
      BuildContext context, PhotoCropRenderIgnorePointer renderObject) {
    renderObject
      ..unIgnoreRectList = unIgnoreRectList
      ..ignoringSemantics = ignoringSemantics;
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(
        DiagnosticsProperty<List<Rect>>('unIgnoreRectList', unIgnoreRectList));
    properties.add(DiagnosticsProperty<bool>(
        'ignoringSemantics', ignoringSemantics,
        defaultValue: null));
  }
}

class PhotoCropRenderIgnorePointer extends RenderProxyBox {
  PhotoCropRenderIgnorePointer({
    RenderBox? child,
    List<Rect>? unIgnoreRect,
    bool ignoring = true,
  })  : _unIgnoreRectList = unIgnoreRect,
        _ignoringSemantics = ignoring,
        super(child);

  bool get ignoringSemantics => _ignoringSemantics!;
  bool? _ignoringSemantics;
  set ignoringSemantics(bool value) {
    if (value == _ignoringSemantics) return;
    _ignoringSemantics = value;
    if (_ignoringSemantics == null || !_ignoringSemantics!)
      markNeedsSemanticsUpdate();
  }

  List<Rect>? _unIgnoreRectList;
  List<Rect>? get unIgnoreRectList => _unIgnoreRectList;
  set unIgnoreRectList(List<Rect>? value) {
    _unIgnoreRectList = value;
    if (_ignoringSemantics == null || !_ignoringSemantics!)
      markNeedsSemanticsUpdate();
  }

  @override
  bool hitTest(BoxHitTestResult result, {required Offset position}) {
    return _hitTestIgnore(position) &&
        super.hitTest(result, position: position);
  }

  @override
  void visitChildrenForSemantics(RenderObjectVisitor visitor) {
    if (child != null) visitor(child!);
  }

  bool _hitTestIgnore(Offset offset) {
    bool ignore = false;
    if (_unIgnoreRectList != null && _unIgnoreRectList!.isNotEmpty == true) {
      for (var rect in _unIgnoreRectList!) {
        if (rect.contains(offset)) {
          ignore = true;
          break;
        }
      }
    }
    return ignore;
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(
        DiagnosticsProperty<List<Rect>>('unIgnoreRectList', unIgnoreRectList));
    properties
        .add(DiagnosticsProperty<bool>('ignoringSemantics', ignoringSemantics));
  }
}
