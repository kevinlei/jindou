import 'dart:ui' as ui;

import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class PhotoCropPainter extends CustomPainter {
  final ui.Image image;
  final Rect cropRect;

  PhotoCropPainter(this.image, this.cropRect);

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint();
    paint.isAntiAlias = true;
    Rect pixelRect = Rect.fromLTRB(
      cropRect.left * (ScreenUtil().pixelRatio ?? 1.0),
      cropRect.top * (ScreenUtil().pixelRatio ?? 1.0),
      cropRect.right * (ScreenUtil().pixelRatio ?? 1.0),
      cropRect.bottom * (ScreenUtil().pixelRatio ?? 1.0),
    );
    canvas.drawImageRect(
        image,
        pixelRect,
        Rect.fromCenter(
            center: Offset(size.width / 2, size.height / 2),
            width: cropRect.width,
            height: cropRect.height),
        paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}
