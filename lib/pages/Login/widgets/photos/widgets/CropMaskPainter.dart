import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class CropMaskPainter extends CustomPainter {
  final Rect borderRect;

  CropMaskPainter({this.borderRect = Rect.zero});

  Paint painter = Paint()..isAntiAlias = true;
  Paint linePainter = Paint()..isAntiAlias = true;

  double cornerOffset = 8.w;
  double cornerSize = 50.w;

  @override
  void paint(Canvas canvas, Size size) {
    drawBlackMask(canvas, size);
    // 边框
    canvas.drawRect(borderRect, painter..color = Colors.white);
    // 边角
    drawCorners(canvas);
    // 裁剪区域
    canvas.drawRect(
        borderRect,
        painter
          ..color = Colors.black
          ..blendMode = BlendMode.dstOut);
    canvas.restore();
    canvas.restore();
    // 线框
    drawLineBorder(canvas);
  }

  void drawBlackMask(Canvas canvas, Size size) {
    painter.color = Colors.black87;
    painter.blendMode = BlendMode.srcOver;
    canvas.save();
    final Rect background = Offset.zero & size;
    canvas.saveLayer(background, painter);
    canvas.drawRect(background, painter);
  }

  void drawLineBorder(Canvas canvas) {
    linePainter.color = Colors.white;
    linePainter.strokeWidth = 0.3;

    double cropWidth = borderRect.right - borderRect.left;
    double cropHeight = borderRect.bottom - borderRect.top;
    // 横向
    canvas.drawLine(
        Offset(borderRect.left, borderRect.top + cropHeight / 4 * 1),
        Offset(borderRect.right, borderRect.top + cropHeight / 4 * 1),
        linePainter);
    canvas.drawLine(
        Offset(borderRect.left, borderRect.top + cropHeight / 4 * 2),
        Offset(borderRect.right, borderRect.top + cropHeight / 4 * 2),
        linePainter);
    canvas.drawLine(
        Offset(borderRect.left, borderRect.top + cropHeight / 4 * 3),
        Offset(borderRect.right, borderRect.top + cropHeight / 4 * 3),
        linePainter);
    // 纵向
    canvas.drawLine(
        Offset(borderRect.left + cropWidth / 4 * 1, borderRect.top),
        Offset(borderRect.left + cropWidth / 4 * 1, borderRect.bottom),
        linePainter);
    canvas.drawLine(
        Offset(borderRect.left + cropWidth / 4 * 2, borderRect.top),
        Offset(borderRect.left + cropWidth / 4 * 2, borderRect.bottom),
        linePainter);
    canvas.drawLine(
        Offset(borderRect.left + cropWidth / 4 * 3, borderRect.top),
        Offset(borderRect.left + cropWidth / 4 * 3, borderRect.bottom),
        linePainter);
  }

  void drawCorners(Canvas canvas) {
    // 左上角
    Rect cornerLT = Rect.fromLTRB(
        borderRect.left - cornerOffset,
        borderRect.top - cornerOffset,
        borderRect.left + cornerSize - cornerOffset,
        borderRect.top + cornerSize - cornerOffset);
    canvas.drawRect(cornerLT, painter..color = Colors.white);
    // 右上角
    Rect cornerRT = Rect.fromLTRB(
        borderRect.right - cornerSize + cornerOffset,
        borderRect.top - cornerOffset,
        borderRect.right + cornerOffset,
        borderRect.top + cornerSize - cornerOffset);
    canvas.drawRect(cornerRT, painter..color = Colors.white);
    // 左下角
    Rect cornerLB = Rect.fromLTRB(
        borderRect.left - cornerOffset,
        borderRect.bottom - cornerSize + cornerOffset,
        borderRect.left + cornerSize - cornerOffset,
        borderRect.bottom + cornerOffset);
    canvas.drawRect(cornerLB, painter..color = Colors.white);
    // 右下角
    Rect cornerRB = Rect.fromLTRB(
        borderRect.right - cornerSize + cornerOffset,
        borderRect.bottom - cornerSize + cornerOffset,
        borderRect.right + cornerOffset,
        borderRect.bottom + cornerOffset);
    canvas.drawRect(cornerRB, painter..color = Colors.white);
  }

  @override
  bool shouldRepaint(covariant CropMaskPainter oldDelegate) {
    return true;
  }
}
