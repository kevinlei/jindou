import 'dart:math' as math;

import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class VideoProgress extends StatefulWidget {
  final double? lineHeight;

  final double? barRadius;

  final double value;

  final Function(double) updateValue;

  const VideoProgress(
      {Key? key,
      this.lineHeight,
      this.barRadius,
      required this.value,
      required this.updateValue})
      : super(key: key);

  @override
  _VideoProgressState createState() => _VideoProgressState();
}

class _VideoProgressState extends State<VideoProgress> {
  double progressWidth = 0;

  double progress = 0;

  bool isDrag = false;

  @override
  Widget build(BuildContext context) {
    double lineHeight = widget.lineHeight ?? 5.h;
    double barRadius = widget.barRadius ?? 10.w;
    double progressHeight = math.max(lineHeight, barRadius * 2);
    if (!isDrag) {
      progress = widget.value;
    }
    progress = progress < 0 ? 0 : progress;
    progress = progress > 1 ? 1 : progress;
    return GestureDetector(
      onTap: onTap,
      onTapDown: onPressDown,
      onHorizontalDragUpdate: onDragUpdate,
      onHorizontalDragEnd: onDragEnd,
      child: Container(
        height: progressHeight,
        color: Colors.transparent,
        child: CustomPaint(
          size: MediaQuery.of(context).size,
          painter: _VideoProgressPainter(
              lineHeight, barRadius, progress, getProgressWidth),
        ),
      ),
    );
  }

  void getProgressWidth(double width) => progressWidth = width;

  void onTap() {
    progress = progress < 0 ? 0 : progress;
    progress = progress > 1 ? 1 : progress;
    widget.updateValue(progress);
  }

  void onPressDown(TapDownDetails details) {
    if (progressWidth == 0) return;
    progress = details.localPosition.dx / progressWidth;
    progress = double.parse(progress.toStringAsFixed(2));
  }

  void onDragUpdate(DragUpdateDetails details) {
    if (progressWidth == 0) return;
    progress = details.localPosition.dx / progressWidth;
    progress = double.parse(progress.toStringAsFixed(2));
    isDrag = true;
    if (!mounted) return;
    setState(() {});
  }

  void onDragEnd(DragEndDetails details) {
    if (progressWidth == 0) return;
    progress = progress < 0 ? 0 : progress;
    progress = progress > 1 ? 1 : progress;
    isDrag = false;
    widget.updateValue(progress);
  }
}

class _VideoProgressPainter extends CustomPainter {
  final double lineHeight;

  final double barRadius;

  final double value;

  final Function(double) backWidth;

  _VideoProgressPainter(
      this.lineHeight, this.barRadius, this.value, this.backWidth);

  // 画笔
  Paint _progressPaint = Paint();

  @override
  void paint(Canvas canvas, Size size) {
    backWidth(size.width);
    double lineWidth = size.width - barRadius * 2;
    _progressPaint.color = Color(0xFFF6F8FF).withOpacity(0.5);
    // 绘制进度条背景
    Rect back = Rect.fromCenter(
        center: Offset(size.width / 2, size.height / 2),
        width: lineWidth,
        height: lineHeight);
    canvas.drawRRect(RRect.fromRectAndRadius(back, Radius.circular(lineHeight)),
        _progressPaint);
    // 绘制进度条
    _progressPaint.color = Colors.yellow;
    Rect progress = Rect.fromCenter(
        center: Offset(lineWidth * value / 2 + barRadius, size.height / 2),
        width: lineWidth * value,
        height: lineHeight);
    canvas.drawRRect(
        RRect.fromRectAndRadius(progress, Radius.circular(lineHeight)),
        _progressPaint);
    // 绘制bar
    canvas.drawCircle(Offset(lineWidth * value + barRadius, size.height / 2),
        barRadius, _progressPaint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
