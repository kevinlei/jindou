import 'package:echo/import.dart';
import 'package:flutter/material.dart';

import 'CropIgnorePointer.dart';
import 'CropMaskPainter.dart';

class PhotoCropMask extends StatefulWidget {
  final Function(Rect) onDragEnd;

  const PhotoCropMask({Key? key, required this.onDragEnd}) : super(key: key);

  @override
  _PhotoCropMaskState createState() => _PhotoCropMaskState();
}

class _PhotoCropMaskState extends State<PhotoCropMask> {
  // 裁剪框
  Rect cropBorder = Rect.zero;
  // 不忽略四个角的拖拽事件
  List<Rect> ignoreList = [];
  // 拖拽的角 0：左上 1：右上 2：左下 3：右下
  int dragCorner = -1;

  // 裁剪框的偏移 向上偏移一部分让裁剪框尽可能居中
  late double paddingHor, paddingVer;
  // 裁剪尺寸最大时的边界
  late double borderMaxL, borderMaxT, borderMaxR, borderMaxB;
  // 裁剪尺寸最小时的边界
  late double borderMinL, borderMinT, borderMinR, borderMinB;
  // 最小 / 最大裁剪尺寸
  late double minCropSize, maxCropSize;

  @override
  void initState() {
    super.initState();
    initializeBorderRect();
    ignoreList = getIgnoreRectList();
    widget.onDragEnd(cropBorder);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onPanStart: onMoveStart,
      onPanUpdate: onMoveUpdate,
      onPanEnd: onMoveEnd,
      child: CropIgnorePointer(
        unIgnoreRectList: ignoreList,
        child: CustomPaint(
          size: MediaQuery.of(context).size,
          painter: CropMaskPainter(borderRect: cropBorder),
        ),
      ),
    );
  }

  void onMoveStart(DragStartDetails details) {
    judgeDragCorner(details.localPosition);
  }

  void onMoveUpdate(DragUpdateDetails details) {
    if (dragCorner == -1 || !mounted) return;
    moveBorderRect(details.delta);
    ignoreList = getIgnoreRectList();
    setState(() {});
  }

  void onMoveEnd(DragEndDetails details) {
    widget.onDragEnd(cropBorder);
  }

  // 判断用户拖拽的是哪个角
  void judgeDragCorner(Offset point) {
    for (int index = 0; index < ignoreList.length; index++) {
      Rect cornerRect = ignoreList[index];
      bool inWidth =
          point.dx >= cornerRect.left && point.dx <= cornerRect.right;
      bool inHeight =
          point.dy >= cornerRect.top && point.dy <= cornerRect.bottom;
      if (inWidth && inHeight) {
        dragCorner = index;
        break;
      }
    }
  }

  // 计算出裁剪框的控制点
  List<Rect> getIgnoreRectList() {
    // 四角控制区域的尺寸
    double cornerSize = 100.w;
    Rect rectTL = Rect.fromLTWH(
        cropBorder.left - 30.w, cropBorder.top - 30.w, cornerSize, cornerSize);
    Rect rectTR = Rect.fromLTWH(cropBorder.right + 30.w - cornerSize,
        cropBorder.top - 30.w, cornerSize, cornerSize);
    Rect rectBL = Rect.fromLTWH(cropBorder.left - 30.w,
        cropBorder.bottom + 30.w - cornerSize, cornerSize, cornerSize);
    Rect rectBR = Rect.fromLTWH(cropBorder.right + 30.w - cornerSize,
        cropBorder.bottom + 30.w - cornerSize, cornerSize, cornerSize);
    return [rectTL, rectTR, rectBL, rectBR];
  }

  void initializeBorderRect() {
    paddingHor = 25.w;
    paddingVer = 160.w;
    minCropSize = 300.w;
    maxCropSize = ScreenUtil().screenWidth - paddingHor * 2;

    borderMaxL = paddingHor;
    borderMaxT = (ScreenUtil().screenHeight - paddingVer - maxCropSize) / 2;
    borderMaxR = paddingHor + maxCropSize;
    borderMaxB = (ScreenUtil().screenHeight - paddingVer + maxCropSize) / 2;

    Offset pointLT = Offset(borderMaxL, borderMaxT);
    Offset pointRB = Offset(borderMaxR, borderMaxB);

    cropBorder = Rect.fromPoints(pointLT, pointRB);
  }

  void moveBorderRect(Offset detail) {
    double curL = cropBorder.left;
    double curT = cropBorder.top;
    double curR = cropBorder.right;
    double curB = cropBorder.bottom;

    curT += detail.dy;
    curB += detail.dy;

    if (curT <= 20.h ||
        curB >=
            (ScreenUtil().screenHeight - ScreenUtil().bottomBarHeight - 200.h))
      return;
    cropBorder = Rect.fromLTRB(curL, curT, curR, curB);
    return;
  }
}
