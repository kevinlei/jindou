import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class CameraAppBar extends StatefulWidget implements PreferredSizeWidget {
  final Future<bool> Function(FlashMode) setFlash;
  final Function() setDir;

  const CameraAppBar({Key? key, required this.setFlash, required this.setDir})
      : super(key: key);

  @override
  _CameraAppBarState createState() => _CameraAppBarState();

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
}

class _CameraAppBarState extends State<CameraAppBar>
    with SingleTickerProviderStateMixin {
  late AnimationController animCtr;
  late Animation<double> anim;
  FlashMode curFlash = FlashMode.auto;

  @override
  void initState() {
    super.initState();
    animCtr =
        AnimationController(duration: Duration(milliseconds: 300), vsync: this);
    anim = CurvedAnimation(parent: animCtr, curve: Curves.easeInCubic);
  }

  @override
  Widget build(BuildContext context) {
    double top = ScreenUtil().statusBarHeight;
    return FContainer(
      padding: EdgeInsets.fromLTRB(0, top, 0, 0),
      color: Colors.black,
      child: Row(children: [buttonFlash(), flashList(), Spacer(), buttonDir()]),
    );
  }

  @override
  void dispose() {
    animCtr.dispose();
    super.dispose();
  }

  Widget buttonFlash() {
    return IconButton(
      onPressed: onTapFlash,
      icon: Icon(getFlashIcon(curFlash), color: Colors.blue),
    );
  }

  Widget flashList() {
    List<Widget> children = List.generate(
        FlashMode.values.length, (index) => flashCell(FlashMode.values[index]));
    return SizeTransition(
      sizeFactor: anim,
      child: Row(children: children),
    );
  }

  Widget flashCell(FlashMode mode) {
    return IconButton(
      onPressed: () => onTapCell(mode),
      icon: Icon(getFlashIcon(mode),
          color: mode == curFlash ? Colors.blue : Colors.white),
    );
  }

  Widget buttonDir() {
    return IconButton(
        onPressed: widget.setDir,
        icon: LoadAssetImage("photo/direction", width: 45.w));
  }

  void onTapFlash() {
    if (animCtr.value == 1) {
      animCtr.reverse();
    } else {
      animCtr.forward();
    }
  }

  void onTapCell(FlashMode mode) async {
    bool success = await widget.setFlash(mode);
    if (!success || !mounted) return;
    curFlash = mode;
    setState(() {});
  }

  IconData getFlashIcon(FlashMode mode) {
    switch (mode) {
      case FlashMode.off:
        return Icons.flash_off;
      case FlashMode.auto:
        return Icons.flash_auto;
      case FlashMode.always:
        return Icons.flash_on;
      case FlashMode.torch:
        return Icons.highlight;
    }
  }
}
