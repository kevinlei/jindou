import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class PhotoPreview extends StatefulWidget {
  final List images;
  final int index;
  const PhotoPreview(this.images, {Key? key, this.index = 0}) : super(key: key);

  @override
  _PhotoPreviewState createState() => _PhotoPreviewState();
}

class _PhotoPreviewState extends State<PhotoPreview> {
  late PageController controller;

  @override
  void initState() {
    super.initState();
    controller = PageController(initialPage: widget.index);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: SafeArea(child: Stack(children: [photoContent(), backButton()])),
    );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  Widget backButton() {
    return Positioned(
      child: GestureDetector(
        onTap: onPressBack,
        child: CircleAvatar(
          radius: 50.r,
          backgroundColor: Colors.black45,
          child: const Icon(Icons.arrow_back_ios, color: Colors.white),
        ),
      ),
    );
  }

  Widget photoContent() {
    return PageView.builder(
      itemCount: widget.images.length,
      physics: const ClampingScrollPhysics(),
      controller: controller,
      itemBuilder: (_, index) {
        String photoUrl = widget.images[index];
        return photoCard(photoUrl);
      },
    );
  }

  Widget photoCard(String photoUrl) {
    photoUrl = NetConstants.ossPath + photoUrl;
    return SizedBox.fromSize(
      size: MediaQuery.of(context).size,
      child: PhotoView(
        imageProvider: ImageUtils().getImageProvider(photoUrl),
        minScale: 0.3,
        maxScale: 0.8,
        enableRotation: false,
        gaplessPlayback: true,
      ),
    );
  }

  void onPressBack() {
    WidgetUtils().popPage();
  }
}
