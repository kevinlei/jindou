import 'package:echo/import.dart';
import 'package:echo/pages/Login/widgets/photos/widgets/VideoProgress.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class VideoPreview extends StatefulWidget {
  final String videoUrl;

  const VideoPreview(this.videoUrl, {Key? key}) : super(key: key);

  @override
  _VideoPreviewState createState() => _VideoPreviewState();
}

class _VideoPreviewState extends State<VideoPreview> {
  late VideoPlayerController playerController;

  @override
  void initState() {
    super.initState();
    initVideoPlayer();
  }

  @override
  void dispose() {
    playerController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Widget videoCard = LoadNetImage(
        NetConstants.ossPath + widget.videoUrl + OssThumb.OSS_VIDEO.path);

    if (playerController.value.isInitialized) {
      videoCard = GestureDetector(
          onTap: onPressPlay,
          child: AspectRatio(
            aspectRatio: playerController.value.aspectRatio,
            child: VideoPlayer(playerController),
          ));
    }

    int maxDuration = playerController.value.duration.inSeconds;
    int curDuration = playerController.value.position.inSeconds;

    Widget playButton = LoadAssetImage("dynamic/dAudioOn");

    return Stack(children: [
      videoCard,
      Visibility(
        child: IconButton(onPressed: onPressPlay, icon: playButton),
        visible: (curDuration > 0) &&
            (curDuration == maxDuration || !playerController.value.isPlaying),
      ),
      videoProgress(),
    ], alignment: Alignment.center);
  }

  Widget videoProgress() {
    Widget volume = SizedBox(
      width: 50.w,
      height: 50.w,
      child: IconButton(
        padding: EdgeInsets.zero,
        onPressed: onPressVolume,
        icon: Icon(
            playerController.value.volume < 1
                ? Icons.volume_off
                : Icons.volume_up,
            color: Colors.white),
      ),
    );

    int maxDuration = playerController.value.duration.inSeconds;
    int curDuration = playerController.value.position.inSeconds;
    double progress = maxDuration > 0 ? (curDuration / maxDuration) : 0;
    Widget content = Row(children: [
      SizedBox(width: 25.w),
      volume,
      SizedBox(width: 40.w),
      Expanded(child: VideoProgress(value: progress, updateValue: onSeekVideo)),
      SizedBox(width: 25.w)
    ]);
    return Positioned(child: content, bottom: 100.h, left: 0, right: 0);
  }

  void initVideoPlayer() async {
    String videoUrl = NetConstants.ossPath + widget.videoUrl;
    playerController = VideoPlayerController.network(videoUrl);
    playerController.addListener(() {
      if (!playerController.value.isInitialized) return;
      if (!mounted) return;
      setState(() {});
    });
    await playerController.initialize();
    if (!mounted) return;
    setState(() {});
    playerController.play();
  }

  void onPressPlay() {
    if (!playerController.value.isInitialized) return;
    if (playerController.value.isPlaying) {
      playerController.pause();
    } else {
      playerController.play();
    }
  }

  void onPressVolume() {
    if (!playerController.value.isInitialized) return;
    if (playerController.value.volume < 1) {
      playerController.setVolume(1);
    } else {
      playerController.setVolume(0);
    }
  }

  void onSeekVideo(double value) {
    if (!playerController.value.isInitialized) return;
    int maxDuration = playerController.value.duration.inSeconds;
    int seek = (maxDuration * value).toInt();
    seek = seek > maxDuration ? maxDuration : seek;
    seek = seek < 0 ? 0 : seek;
    playerController.seekTo(Duration(seconds: seek));
  }
}
