import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class AgePicker extends StatefulWidget {
  @override
  _AgePickerState createState() => _AgePickerState();
}

class _AgePickerState extends State<AgePicker> {
  int age = 0;
  String zodiac = "";
  int maxDays = 31;
  Map<String, int> monthDays = {
    "1": 31,
    "2": 28,
    "3": 31,
    "4": 30,
    "5": 31,
    "6": 30,
    "7": 31,
    "8": 31,
    "9": 30,
    "10": 31,
    "11": 30,
    "12": 31
  };

  int minYear = DateTime.now().year - 72;
  int maxYear = DateTime.now().year - 18;
  DateTime dateTime = DateTime.now();

  late FixedExtentScrollController yearCtr =
      FixedExtentScrollController(initialItem: maxYear - minYear);
  FixedExtentScrollController monthCtr =
      FixedExtentScrollController(initialItem: 0);
  FixedExtentScrollController dayCtr =
      FixedExtentScrollController(initialItem: 0);

  @override
  void initState() {
    super.initState();
    getAgeAndZodiac();
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      color: Color(0xFFFFFFFF),
      padding: EdgeInsets.only(
          top: 10.w, bottom: ScreenUtil().bottomBarHeight + 20.w),
      child: Column(children: [
        FSheetLine(),
        resultContent(),
        pickerContent(),
        Row(children: [btnCancel(), btnConfirm()])
      ], mainAxisSize: MainAxisSize.min),
    );
  }

  @override
  void dispose() {
    yearCtr.dispose();
    monthCtr.dispose();
    dayCtr.dispose();
    super.dispose();
  }

  Widget resultContent() {
    Widget ageWidget = Flexible(
      child: Align(
        alignment: Alignment.center,
        child: FText("$age岁", color: Colors.white,size: 32.sp),
      ),
    );

    Widget zodiacWidget = Flexible(
      child: Align(
        alignment: Alignment.center,
        child: FText(zodiac,color: Colors.white, size: 32.sp),
      ),
    );
    return FContainer(
      color: Color(0xFFFF753F),
      height: 80.h,
      radius: BorderRadius.circular(80.h),
      margin: EdgeInsets.symmetric(horizontal: 65.w, vertical: 20.h),
      child: Row(children: [
        ageWidget,
        SizedBox(
            height: 55.h,
            child: VerticalDivider(color: Color(0xFFD2D4DB), width: 2.w)),
        zodiacWidget
      ]),
    );
  }

  Widget pickerContent() {
    Widget content = Row(children: [
      PickerWheel(pickerItem(minYear, maxYear, "年", yearCtr), onSelect: onYearChange),
      PickerWheel(pickerItem(1, 12, "月", monthCtr), onSelect: onMonthChange),
      PickerWheel(pickerItem(1, maxDays, "日", dayCtr), onSelect: onDayChange),
    ]);
    return SizedBox(height: 360.h, child: content);
  }

  Widget btnCancel() {
    Widget btn = FContainer(
      height: 90.w,
      margin: EdgeInsets.symmetric(horizontal: 50.w),
      radius: BorderRadius.circular(90.w),
      border: Border.all(color: const Color(0xFFFF753F)),
      align: Alignment.center,
      child: FText("取消", size: 32.sp, weight: FontWeight.bold, color: Color(0xFFFF753F)),
    );
    return Flexible(child: GestureDetector(child: btn, onTap: onCancel));

  }

  Widget btnConfirm() {
    Widget btn = FContainer(
      height: 90.w,
      gradientBegin: Alignment.topLeft,
      gradientEnd: Alignment.bottomRight,
      gradient: [Color(0xFFFF753F),Color(0xFFFF753F),Color(0xFFFF753F)],
      margin: EdgeInsets.symmetric(horizontal: 50.w),
      radius: BorderRadius.circular(90.w),
      align: Alignment.center,
      child: FText("确定", size: 32.sp,color: Colors.white, weight: FontWeight.bold),
    );
    return Flexible(child: GestureDetector(child: btn, onTap: onConfirm));
  }

  Widget pickerItem(
      int min, int max, String suffix, FixedExtentScrollController controller) {
    BorderSide borderSide = BorderSide(color: Color(0xFFEDEEF3), width: 2.w);
    return CupertinoPicker.builder(
        childCount: max - min + 1,
        itemExtent: 100.h,
        // backgroundColor: Colors.amber,
        // selectionOverlay: FContainer(border: Border(left: borderSide,top: borderSide, bottom: borderSide)),
        scrollController: controller,
        onSelectedItemChanged: null,
        itemBuilder: (_, index) => ageItem("${index + min}" + suffix));
  }

  Widget ageItem(String text) {
    return Center(child: FText(text,color: Colors.black, size: 32.sp));
  }

  void getAgeAndZodiac() {
    int year = maxYear, month = 1, day = 1;
    if (yearCtr.hasClients) {
      year = yearCtr.selectedItem + minYear;
    }
    if (monthCtr.hasClients) {
      month = monthCtr.selectedItem + 1;
    }
    if (dayCtr.hasClients) {
      day = dayCtr.selectedItem + 1;
    }
    switch (month) {
      case DateTime.january:
        zodiac = day >= 20 ? "水瓶座" : "魔羯座";
        break;
      case DateTime.february:
        zodiac = day >= 19 ? "双鱼座" : "水瓶座";
        break;
      case DateTime.march:
        zodiac = day >= 21 ? "白羊座" : "双鱼座";
        break;
      case DateTime.april:
        zodiac = day >= 20 ? "金牛座" : "白羊座";
        break;
      case DateTime.may:
        zodiac = day >= 21 ? "双子座" : "金牛座 ";
        break;
      case DateTime.june:
        zodiac = day >= 22 ? "巨蟹座" : "双子座";
        break;
      case DateTime.july:
        zodiac = day >= 23 ? "狮子座" : "巨蟹座";
        break;
      case DateTime.august:
        zodiac = day >= 23 ? "处女座" : "狮子座";
        break;
      case DateTime.september:
        zodiac = day >= 23 ? "天秤座" : "处女座";
        break;
      case DateTime.october:
        zodiac = day >= 24 ? "天蝎座" : "天秤座";
        break;
      case DateTime.november:
        zodiac = day >= 23 ? "射手座" : "天蝎座";
        break;
      case DateTime.december:
        zodiac = day >= 22 ? "魔羯座" : "射手座";
        break;
    }
    // DateTime time = DateTime(year, month, day);
    // int different = dateTime.difference(time).inDays;
    // age = (different / 365).floor();
    age = dateTime.year - year;
    if (!mounted) return;
    setState(() {});
  }

  void onYearChange() {
    getAgeAndZodiac();
  }

  void onMonthChange() {
    int month = monthCtr.selectedItem + 1;
    int days = monthDays[month.toString()]!;
    if (maxDays != days) {
      maxDays = days;
      dayCtr.jumpTo(0);
    }
    getAgeAndZodiac();
  }

  void onDayChange() {
    getAgeAndZodiac();
  }

  void onCancel() {
    WidgetUtils().popPage();
  }

  void onConfirm() {
    int year = minYear, month = 1, day = 1;
    if (yearCtr.hasClients) {
      year = yearCtr.selectedItem + minYear;
    }
    if (monthCtr.hasClients) {
      month = monthCtr.selectedItem + 1;
    }
    if (dayCtr.hasClients) {
      day = dayCtr.selectedItem + 1;
    }
    DateTime time = DateTime(year, month, day);
    WidgetUtils().popPage(time.millisecondsSinceEpoch);
  }
}
