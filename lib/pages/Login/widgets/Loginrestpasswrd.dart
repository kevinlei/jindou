
import 'package:echo/import.dart';
import 'dart:io';
import 'package:echo/pages/Login/widgets/Loinputpassword.dart';
import 'package:flutter/material.dart';


class Loginrestpasswrd extends StatefulWidget {
  final String passwrdkey;
  final String phone;
  Loginrestpasswrd({Key? key, required this.passwrdkey,required this.phone}) : super(key: key);
  @override
  State<Loginrestpasswrd> createState() => _LoginrestpasswrdState();
}

class _LoginrestpasswrdState extends State<Loginrestpasswrd> with WidgetsBindingObserver{
  TextEditingController pwdCtr = TextEditingController();
  TextEditingController passpwdCtr = TextEditingController();

  LoginProvider provider = LoginProvider();
  ScrollController scroll = ScrollController();
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    provider.decodePwd();
    FocusManager.instance.primaryFocus?.unfocus();
  }
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => provider,
        builder: (_, __) {
          return Scaffold(
            backgroundColor: Color(0xFF0B0017),
            body: FContainer(
              padding: EdgeInsets.only(
                  left: 30.w,
                  right: 30.w,
                  bottom: 20.w),
              image: ImageUtils().getAssetsImage("public/huibg"),
              imageFit: BoxFit.fill,
              imageAlign: Alignment.topCenter,
              child: SafeArea(child: loginContent()),
            ),
          );
        });
  }

  Widget loginContent() {
    return Column(children: [
      loginAppbar(),
      Expanded(child: inputContent()),

    ]);
  }
  //提交数据修改密码
  Future<void> quickLogin() async {

    if (provider.pwd.length != provider.passpwd.length || provider.pwd.contains(provider.passpwd)==false || provider.passpwd.contains(provider.pwd)==false) {
      WidgetUtils().showToast("密码两次输入不一致");
      return;
    }
    if (!FuncUtils().isMobile(widget.phone)) {
      WidgetUtils().showToast("手机号错误");
      return;
    }
    DioUtils().asyncHttpRequest(
      NetConstants.resetPwd,
      method: DioMethod.POST,
      params: {
        "phone": widget.phone,
        "pwd_change": provider.pwd,
        "key": widget.passwrdkey,
        "platform": Platform.isAndroid ? "android" : "ios"
      },
      onSuccess: onReportSuccess,
    );
  }
  void onReportSuccess(response) {

    WidgetUtils().popPage();
    WidgetUtils().popPage();
    WidgetUtils().popPage();
  }

  Widget loginAppbar() {
    return Row(children: [
      btnSwitch(),
      Spacer(),

    ]);
  }

  Widget btnSwitch() {
    return GestureDetector(
      onTap: onPressSwitch,
      child: FContainer(
        height: 50.w,
        child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
          LoadAssetImage("login/blckback", width: 60.w, height: 60.w),
        ]),
      ),
    );
  }
  void onPressSwitch() {
    FocusManager.instance.primaryFocus?.unfocus();
    WidgetUtils().popPage();

  }



  Widget inputContent() {
    Widget nextInput = Consumer<LoginProvider>(builder: (_, pro, __) {
      return Loinputpassword(
        inputType: LoginInputType.INPUT_TPWD,

        onInput:onpasspwdInput,
        controller: passpwdCtr,
        // suffix: pro.onLoginCode ? CodeButton(pro.requestPhoneCode) : null,
      );
    });
    return ListView(controller: scroll, children: [
      SizedBox(height: 98.h),
      Align(
        child: Consumer<LoginProvider>(builder: (_, pro, __) {
          return Container(
            padding: EdgeInsets.only(left: 40.w),
            child: FText("设置密码",
                size: 48.sp,  color: Color(0xFF1E1D23), weight: FontWeight.bold),
          );
        }),
        alignment: Alignment.centerLeft,
      ),
      SizedBox(height: 40.h),
      //手机登陆
      Loinputpassword(
        key: const Key("INPUT_PWD"),
        controller: pwdCtr,
        onInput: onPwdInput,
        inputType: LoginInputType.INPUT_PWD,
      ),

      Consumer<LoginProvider>(builder: (_, pro, __) {
        return nextInput;
      }),
      SizedBox(height: 130.h),
      Container(
        child:Column(
          children: [
            Consumer<LoginProvider>(builder: (_, pro, __) {
              return FcationButton(
                "确 定",
                isActivate: pro.pwd.isNotEmpty && pro.passpwd.isNotEmpty ,
                padding: 40.w,
                onPress:  (pro.pwd.isNotEmpty && pro.passpwd.isNotEmpty)==true?quickLogin:null,
              );
            }),
          ],
        ),

      ),

    ]);
  }



  void onAccept(accept) => provider.hasAccept = accept;

  @override
  void didChangeMetrics() {
    super.didChangeMetrics();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      if (!mounted) return;
      double keyboardHeight = MediaQuery.of(context).viewInsets.bottom;
      if (keyboardHeight <= 0) return;
      double max = scroll.position.maxScrollExtent;
      scroll.animateTo(max,
          duration: Duration(milliseconds: 200), curve: Curves.linear);
      Future.delayed(Duration(milliseconds: 200), () {
        double max2 = scroll.position.maxScrollExtent;
        scroll.animateTo(max2,
            duration: Duration(milliseconds: 1), curve: Curves.linear);
      });
    });
  }
  @override
  void dispose() {
    pwdCtr.dispose();
    passpwdCtr.dispose();
    scroll.dispose();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }
  Future<void> onpasspwdInput(String value) async {
    value = value.replaceAll(" ", "").trim();
    provider.onpasspwdInput(value);
  }

  Future<void> onPwdInput(String value) async {
    value = value.replaceAll(" ", "").trim();
    provider.onPwdInput(value);
  }


}
