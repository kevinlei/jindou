import 'package:flutter/material.dart';

class PickerWheel extends StatelessWidget {
  final Widget child;
  final Function()? onSelect;

  const PickerWheel(this.child, {Key? key, this.onSelect}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Flexible(
        flex: 1,
        child: NotificationListener(
          onNotification: (ScrollEndNotification no) {
            if (onSelect != null) onSelect!();
            return false;
          },
          child: child,
        ));
  }
}
