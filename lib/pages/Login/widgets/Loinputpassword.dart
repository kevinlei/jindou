import 'package:echo/import.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Loinputpassword extends StatefulWidget {
  final LoginInputType inputType;
  final Widget? suffix;
  final Function(String)? onInput;
  final TextEditingController? controller;
  const Loinputpassword(
      {Key? key,
        required this.inputType,
        this.suffix,
        this.onInput,
        this.controller})
      : super(key: key);

  @override
  _LoinputpasswordState createState() => _LoinputpasswordState();
}

class _LoinputpasswordState extends State<Loinputpassword> {
  FocusNode focusNode = FocusNode();

  bool showPwd = false;

  @override
  void initState() {
    super.initState();
    focusNode.addListener(onFocus);
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> children = [Expanded(child: fieldContent())];
    if (widget.inputType == LoginInputType.INPUT_PHONE)
      children.insert(0, phoneLead());
    if (suffix() != null) children.add(suffix()!);

    BoxShadow shadow = BoxShadow(
        offset: Offset(3, 5),
        color: const Color(0xF3222222).withOpacity(0.06),
        blurRadius: 10.0);

    return FContainer(
      height: 88.h,
      color: Colors.white,
      shadow: [shadow],
      border: Border.all(
          color: focusNode.hasFocus ?  Colors.grey: Colors.grey,
          width: 1.w),
      radius: BorderRadius.circular(88.w),
      padding: EdgeInsets.only(left: 24.w, right: 20.w),
      margin: EdgeInsets.symmetric(horizontal: 40.w, vertical: 20.h),
      child: Row(children: children),
    );
  }

  @override
  void dispose() {
    focusNode.removeListener(onFocus);
    focusNode.dispose();
    super.dispose();
  }

  Widget fieldContent() {
    List<TextInputFormatter>? formatter;
    if (widget.inputType.formatter != null) {
      formatter = [widget.inputType.formatter!];
    }

    if (widget.inputType == LoginInputType.INPUT_PHONE) {
      formatter?.add(phoneInputFormatter());
    }

    InputBorder border =
    OutlineInputBorder(borderSide: BorderSide(color: Colors.transparent));

    return TextField(
      focusNode: focusNode,
      maxLength: widget.inputType.maxLength,
      inputFormatters: formatter,
      keyboardType: widget.inputType.keyboardType,
      onChanged: widget.onInput,
      controller: widget.controller,
      obscureText: widget.inputType == LoginInputType.INPUT_PWD && !showPwd || widget.inputType == LoginInputType.INPUT_TPWD && !showPwd,
      style: TextStyle(color: Colors.black, fontSize: 30.sp),
      decoration: InputDecoration(
        enabledBorder: border,
        focusedBorder: border,
        counterText: "",
        hintText: widget.inputType.hintText,
        contentPadding: EdgeInsets.only(top: 0, bottom: 0,left: 10.w),
        hintStyle: TextStyle(color: Colors.grey, fontSize: 30.sp),
      ),
    );
  }

  Widget phoneLead() {
    return FContainer(
      padding: EdgeInsets.only(right: 10.w),
      margin: EdgeInsets.only(right: 10.w),
      border: Border(right: BorderSide(color: Color(0xFFBBBBBB))),
      child:Container(
        child:Row(
          children: [
            // FText("+86",
            //     size: 34.sp, color: Colors.black, weight: FontWeight.bold),
            // SizedBox(width: 5.w),
            // LoadAssetImage(showPwd ? "login/triangle" : "login/triangle",
            //     width: 25.w),
          ],
        ),
      ),

    );
  }

  Widget? suffix() {
    if (widget.inputType == LoginInputType.INPUT_PWD  || widget.inputType == LoginInputType.INPUT_TPWD) {
      return GestureDetector(
        onTap: onHide,
        child: FContainer(
          width: 50.w,
          height: 50.w,
          align: Alignment.center,
          child: LoadAssetImage(showPwd ? "login/hide2" : "login/hide1",
              width: 30.w),
        ),
      );
    }
    return widget.suffix;
  }

  void onFocus() {
    if (!mounted) return;
    setState(() {});
  }

  void onHide() {
    showPwd = !showPwd;
    if (!mounted) return;
    setState(() {});
  }
}

TextInputFormatter phoneInputFormatter() {
  return TextInputFormatter.withFunction((oldValue, newValue) {
    String text = newValue.text;

    final positionStr = (text.substring(0, newValue.selection.baseOffset))
        .replaceAll(RegExp(r"\s+\b|\b\s"), "");

    int length = positionStr.length;
    var position = 0;
    if (length <= 3) {
      position = length;
    } else if (length <= 7) {
      position = length + 1;
    } else if (length <= 11) {
      position = length + 2;
    } else {
      position = 13;
    }

    text = text.replaceAll(RegExp(r"\s+\b|\b\s"), "");
    var string = "";
    for (int i = 0; i < text.length; i++) {
      if (i == 3 || i == 7) {
        if (text[i] != " ") {
          string = string + " ";
        }
      }
      string += text[i];
    }

    return TextEditingValue(
      text: string,
      selection: TextSelection.fromPosition(
          TextPosition(offset: position, affinity: TextAffinity.upstream)),
    );
  });
}
