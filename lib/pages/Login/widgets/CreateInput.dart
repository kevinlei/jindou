import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:flutter/services.dart';

class CreateInput extends StatefulWidget {
  final bool inputName;
  final String? userNick;
  final Function(String)? onInput;
  const CreateInput(this.inputName, {Key? key, this.userNick, this.onInput})
      : super(key: key);

  @override
  _CreateInputState createState() => _CreateInputState();
}

class _CreateInputState extends State<CreateInput> {
  FocusNode focusNode = FocusNode();
  TextEditingController controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    focusNode.addListener(onFocus);
    if (widget.inputName) {
      controller.text = widget.userNick ?? "";
    }
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> children = [Expanded(child: fieldContent())];
    BoxShadow shadow = BoxShadow(
      offset: const Offset(3, 5),
      color: const Color(0xFF3C3C3C).withOpacity(0.06),
      blurRadius: 10.0,
    );

    return FContainer(
      height: 90.w,
      color: const Color(0xFF222222).withOpacity(0.05),
      shadow: [shadow],
      border: Border.all(
          color: focusNode.hasFocus ? Colors.transparent : Colors.transparent,
          width: 2.w),
      radius: BorderRadius.circular(47.w),
      padding: EdgeInsets.only(left: 40.w, right: 20.w),
      margin: EdgeInsets.symmetric(vertical: 10.h),
      child: Row(children: children),
    );
  }

  @override
  void dispose() {
    focusNode.removeListener(onFocus);
    focusNode.dispose();
    controller.dispose();
    super.dispose();
  }

  Widget fieldContent() {
    List<TextInputFormatter>? formatter;
    if (!widget.inputName) {
      formatter = [FilteringTextInputFormatter.allow(RegExp(r'[0-9]'))];
    }
    InputBorder border = const OutlineInputBorder(
        borderSide: BorderSide(color: Colors.transparent));

    return TextField(
      focusNode: focusNode,
      maxLength: widget.inputName ? 10 : 8,
      inputFormatters: formatter,
      keyboardType: widget.inputName ? null : TextInputType.number,
      onChanged: widget.onInput,
      controller: controller,
      style: TextStyle(color: Colors.black, fontSize: 30.sp),
      decoration: InputDecoration(
        enabledBorder: border,
        focusedBorder: border,
        disabledBorder: border,
        counterText: "",
        hintText: "取个昵称吧~",
        contentPadding: const EdgeInsets.only(top: 0, bottom: 0),
        hintStyle: TextStyle(color: const Color(0xFFBBBBBB), fontSize: 30.sp),
      ),
    );
  }

  void onFocus() {
    if (!mounted) return;
    setState(() {});
  }
}
