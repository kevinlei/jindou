import 'dart:io';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class LoginResume extends StatefulWidget {
  const LoginResume({Key? key}) : super(key: key);

  @override
  State<LoginResume> createState() => _LoginResumeState();
}

class _LoginResumeState extends State<LoginResume> {
  // 是否同意隐私协议
  bool isOpenMain = false;

  @override
  void initState() {
    super.initState();
    init();
  }

  void init() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    isOpenMain = preferences.getBool(SharedKey.KEY_UNIQUE.value) ?? true;
    if (!isOpenMain) {
      WidgetUtils().pushReplace(const InitPage());
    }
    Future.delayed(const Duration(seconds: 1), () async {
      if (!mounted) return;
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: onWillPop,
      child: FContainer(
        imageFit: BoxFit.fill,
        imageAlign: Alignment.topCenter,
        image: ImageUtils().getAssetsImage("login/bgret"),
        width: double.infinity,
        height: double.infinity,
        child: Center(
          child: Visibility(
            visible: isOpenMain,
            child: FContainer(
              width: 570.w,
              radius: BorderRadius.circular(20.w),
              height: 765.w,
              color: const Color(0xFF272044),
              padding: EdgeInsets.fromLTRB(25.w, 40.h, 25.w, 0),
              child: Column(children: [
                FText(
                  '隐私政策与用户协议',
                  size: 36.sp,
                  color: const Color(0xFFE2E2E2),
                ),
                SizedBox(height: 40.h),
                Expanded(
                    child: SingleChildScrollView(
                  child: Text.rich(
                    TextSpan(
                        text: '欢迎来到金豆派对！\n金豆派对非常重视保障您的个人隐私，我们依据最新的监管要求更新了',
                        style: TextStyle(
                            color: const Color(0xFFE2E2E2), fontSize: 24.sp),
                        children: [
                          TextSpan(
                              text: "《用户协议》",
                              style: TextStyle(
                                  color: const Color(0xFFFFC80B),
                                  fontSize: 24.sp),
                              recognizer: TapGestureRecognizer()
                                ..onTap =
                                    () => onTapProtocol(ProtocolType.PRO_USER)),
                          const TextSpan(text: "及"),
                          TextSpan(
                              text: "《隐私政策》",
                              style: TextStyle(
                                  color: const Color(0xFFFFC80B),
                                  fontSize: 24.sp),
                              recognizer: TapGestureRecognizer()
                                ..onTap = () =>
                                    onTapProtocol(ProtocolType.PRO_PRIVACY)),
                          const TextSpan(text: '，在此特向您说明：\n\n'),
                          const TextSpan(
                              text: '1、未经您的同意，我们不会从第三方获取、共享或向其提供您的信息；\n\n'
                                  '2、您可以随时查询、更正、删除您的个人信息，我们提供账户注销渠道；\n\n'
                                  '我们非常重视您的个人信息隐私保护，关于个人信息收集及使用的详细信息，您可以点击'),
                          TextSpan(
                              text: "《用户协议》",
                              style: TextStyle(
                                  color: const Color(0xFFFFC80B),
                                  fontSize: 24.sp),
                              recognizer: TapGestureRecognizer()
                                ..onTap =
                                    () => onTapProtocol(ProtocolType.PRO_USER)),
                          const TextSpan(text: "及"),
                          TextSpan(
                              text: "《隐私政策》",
                              style: TextStyle(
                                  color: const Color(0xFFFFC80B),
                                  fontSize: 24.sp),
                              recognizer: TapGestureRecognizer()
                                ..onTap = () =>
                                    onTapProtocol(ProtocolType.PRO_PRIVACY)),
                          const TextSpan(
                              text:
                                  '进行了解。点击“同意”即代表您已阅读并同意以上协议，可以继续使用我们的产品和服务，点击“不同意并退出APP”则是不同意我们的协议并且退出APP！'),
                        ]),
                  ),
                )),
                FLoginButton("同意",
                    isActivate: true,
                    radius: 90.w,
                    padding: 0,
                    onPress: onTapConfirm),
                TextButton(
                    onPressed: onTapCancel,
                    child: FText("不同意并退出APP",
                        size: 28.sp, color: const Color(0xFFB6B6B6))),
                SizedBox(height: 20.h),
              ]),
            ),
          ),
        ),
      ),
    );
  }

  Future<bool> onWillPop() async {
    return false;
  }

  void onTapProtocol(ProtocolType type) {
    WidgetUtils().pushPage(ProtocolPage(type: type));
  }

  void onTapConfirm() async {
    // WidgetUtils().popPage(true);
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setBool(SharedKey.KEY_UNIQUE.value, false);
    WidgetUtils().pushReplace(const InitPage());
  }

  void onTapCancel() async {
    exit(0);
  }
}
