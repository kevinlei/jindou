import 'package:echo/pages/Login/widgets/Loginputst.dart';
import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:fluwx/fluwx.dart' as fluwx;

class FeedsetbackPage extends StatefulWidget {
  final String title;
  final String hint;
  final int value;
  final void Function(String, String, List<String>) setBack;

  const FeedsetbackPage(this.title, this.hint, this.value,
      {Key? key, required this.setBack})
      : super(key: key);

  @override
  _FeedsetbackPagePageState createState() => _FeedsetbackPagePageState();
}

class _FeedsetbackPagePageState extends State<FeedsetbackPage>
    with WidgetsBindingObserver {
  TextEditingController phoneCtr = TextEditingController();
  TextEditingController nameCtr = TextEditingController();

  final FocusNode focusNode = FocusNode();
  LoginProvider provider = LoginProvider();
  ScrollController scroll = ScrollController();

  // 输入理由
  String inputText = "";

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => provider,
        builder: (_, __) {
          return Scaffold(
            body: GestureDetector(
              onTap: () {
                FocusScope.of(context).requestFocus(FocusNode());
              },
              child: FContainer(
                padding: EdgeInsets.only(left: 30.w, right: 30.w, bottom: 20.w),
                image: ImageUtils().getAssetsImage("public/huibg"),
                imageFit: BoxFit.fill,
                imageAlign: Alignment.topCenter,
                child: SafeArea(child: loginContent()),
              ),
            ),
          );
        });
  }

  Widget loginContent() {
    return Column(children: [
      loginAppbar(),
      const Divider(),
      Expanded(child: inputContent()),
    ]);
  }

  Widget inputContent() {
    return ListView(
      controller: scroll,
      padding: EdgeInsets.fromLTRB(25.w, 20.h, 25.w, 0),
      children: [
        FText(
          "理由：${widget.title}",
          color: Colors.black,
          size: 32.sp,
        ),
        SizedBox(height: 20.h),
        FText(
          "内容描述（必填）",
          color: Colors.black,
          size: 28.sp,
        ),
        SizedBox(height: 20.h),
        inputField(),
        SizedBox(height: 40.h),
        FText(
          "请填写您的联系方式（必填）",
          color: Colors.black,
          size: 32.sp,
        ),
        Loginputst(
          key: const Key("INPUT_PHONE"),
          controller: phoneCtr,
          onInput: onPhoneInput,
          suffix: phoneSuffix(),
          inputType: LoginInputType.INPUT_PHONE,
        ),
        Row(
          children: [
            FText(
              "上传图片证据（${widget.value == -1 ? "选" : "必"}填）",
              color: Colors.black,
              size: 32.sp,
            ),
          ],
        ),
        SizedBox(height: 20.h),
        ImgContent(urlList: provider.urlList),
        SizedBox(height: 200.h),
        Consumer<LoginProvider>(builder: (_, pro, __) {
          return FcationButton("提交",
              isActivate: pro.neirongtext.isNotEmpty,
              txtColor: Colors.white,
              padding: 0,
              radius: 90.w,
              onPress: pro.neirongtext.isNotEmpty ? onTapConfirm : null);
        }),
        btngroup(),
      ],
    );
  }

  Widget loginAppbar() {
    return Row(children: [
      Expanded(
          child: Align(
        alignment: Alignment.centerLeft,
        child: IconButton(
            onPressed: onPressSwitch,
            icon: const Icon(Icons.arrow_back_ios, color: Colors.black)),
      )),
      Expanded(
        child: Text("反馈",
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Colors.black,
                fontSize: 36.sp,
                fontWeight: FontWeight.w600)),
      ),
      Expanded(child: Row())
    ]);
  }

  void onPressSwitch() {
    FocusManager.instance.primaryFocus?.unfocus();
    WidgetUtils().popPage();
  }

  Widget btngroup() {
    return GestureDetector(
      onTap: onPresbtngroup,
      child: FContainer(
        height: 60.w,
        child: Column(
          children: [
            SizedBox(height: 12.w),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              FText("联系客服",
                  size: 28.sp,
                  weight: FontWeight.bold,
                  color: const Color(0xFFFF753F)),
            ]),
          ],
        ),
      ),
    );
  }

  void onPresbtngroup() {
    // fluwx.openWeChatCustomerServiceChat(
    //     corpId: 'wwdef5ce0ec115ae2b',
    //     url: 'https://work.weixin.qq.com/kfid/kfc5e127a02eb7e8381');
    WidgetUtils().pushPage(const BanerWage(
      url:
          "https://chatbot.weixin.qq.com/webapp/auth/7bZcj7lwQ11RGGNqTtpxdudrkwQBC5",
    ));
  }

  @override
  void didChangeMetrics() {
    super.didChangeMetrics();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      if (!mounted) return;
      double keyboardHeight = MediaQuery.of(context).viewInsets.bottom;
      if (keyboardHeight <= 0) return;
      double max = scroll.position.maxScrollExtent;
      scroll.animateTo(max,
          duration: const Duration(milliseconds: 200), curve: Curves.linear);
      Future.delayed(const Duration(milliseconds: 200), () {
        double max2 = scroll.position.maxScrollExtent;
        scroll.animateTo(max2,
            duration: const Duration(milliseconds: 1), curve: Curves.linear);
      });
    });
  }

  Future<void> onPhoneInput(String value) async {
    value = value.replaceAll(" ", "").trim();
    provider.onPhoneInput(value);
  }

  Widget phoneSuffix() {
    return Consumer<LoginProvider>(builder: (_, pro, __) {
      Widget btn = GestureDetector(
        onTap: onClean,
        child: FContainer(
          width: 60.w,
          height: 60.w,
          child: const Icon(Icons.clear, color: Color(0xFFBBBBBB)),
        ),
      );
      return Visibility(visible: pro.phone.isNotEmpty, child: btn);
    });
  }

  void onClean() {
    phoneCtr.clear();
    provider.onPhoneInput("");
  }

  @override
  void dispose() {
    phoneCtr.dispose();
    nameCtr.dispose();
    scroll.dispose();
    super.dispose();
  }

  Widget inputField() {
    InputBorder border = const OutlineInputBorder(
        borderSide: BorderSide(color: Colors.transparent));
    return FContainer(
      radius: BorderRadius.circular(20.w),
      color: const Color(0xFF222222).withOpacity(0.05),
      height: 350.h,
      padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 10.h),
      child: TextField(
        focusNode: focusNode,
        style: TextStyle(color: Colors.black, fontSize: 28.sp),
        maxLines: 9,
        maxLength: 300,
        controller: nameCtr,
        onChanged: onneirongtextInput,
        decoration: InputDecoration(
          enabledBorder: border,
          focusedBorder: border,
          hintText: widget.hint,
          contentPadding: EdgeInsets.zero,
          hintStyle: TextStyle(color: const Color(0xFFBBBBBB), fontSize: 28.sp),
          counterStyle:
              TextStyle(color: const Color(0xFFBBBBBB), fontSize: 26.sp),
        ),
      ),
    );
  }

  void onTextInput(value) => inputText = value;

  Future<void> onneirongtextInput(String value) async {
    value = value.replaceAll(" ", "").trim();
    inputText = value;
    provider.onneirongtextInput(value);
  }

  void onTapConfirm() {
    if (inputText.trimRight().isEmpty) return WidgetUtils().showToast("请输入内容");
    if (phoneCtr.value.text.isEmpty) return WidgetUtils().showToast("请输入手机号");
    String value = phoneCtr.value.text.replaceAll(" ", "").trim();
    if (value.isNotEmpty) {
      if (value.length != 11) {
        WidgetUtils().showToast("请输入正确的手机号");
        return;
      }
    }
    if (widget.value != -1 && provider.urlList.isEmpty) {
      return WidgetUtils().showToast("请上传图片证据");
    }

    widget.setBack(inputText, value, provider.urlList);
  }
}
