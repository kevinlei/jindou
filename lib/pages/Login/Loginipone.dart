import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class Loginipone extends StatefulWidget {
  const Loginipone({super.key});

  @override
  State<Loginipone> createState() => _LoginiponeState();
}

class _LoginiponeState extends State<Loginipone> with WidgetsBindingObserver {
  TextEditingController phoneCtr = TextEditingController();
  LoginProvider provider = LoginProvider();
  ScrollController scroll = ScrollController();
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    provider.decodePwd();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    phoneCtr.dispose();
    scroll.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => provider,
        builder: (_, __) {
          return Scaffold(
            backgroundColor: const Color(0xFFF5F6F7),
            body: FContainer(
              padding: EdgeInsets.only(left: 30.w, right: 30.w, bottom: 20.w),
              image: ImageUtils().getAssetsImage("public/huibg"),
              imageFit: BoxFit.fill,
              imageAlign: Alignment.topCenter,
              child: SafeArea(child: loginContent()),
            ),
          );
        });
  }

  Widget loginContent() {
    return Column(children: [
      loginAppbar(),
      Expanded(child: inputContent()),
      Column(
        children: [
          btngroup(),
          Consumer<LoginProvider>(builder: (_, pro, __) {
            return FcationButton(
              "获取验证码",
              isActivate: pro.phone.isNotEmpty,
              padding: 40.w,
              onPress: pro.phone.isNotEmpty ? quickLogin : null,
            );
          }),
        ],
      ),
      SizedBox(height: 100.h),
      FProSAccept(
        const [
          ProtocolType.PRO_USER,
          ProtocolType.PRO_PRIVACY,
          ProtocolType.FUTKAN,
        ],
        onAccept: onAccept,
      ),
    ]);
  }

  //获取验证码
  Future<void> quickLogin() async {
    bool state = provider.hasAccept;
    if (!state) return WidgetUtils().showToast("请先同意用户协议");
    var value = phoneCtr.value.text.replaceAll(" ", "").trim();
    if (value.length != 11) {
      WidgetUtils().showToast("请输入正确的手机号");
      return;
    }
    if (provider.onLoginCode) {
      //验证码登陆
      bool result = await provider.requestPhoneCode(0);
      if (result) {
        WidgetUtils().pushPage(Logincode(
          phone: provider.phone,
        ));
      }
    }
  }

  Widget loginAppbar() {
    return Row(children: [
      btnSwitch(),
      const Spacer(),
      btnright(),
    ]);
  }

  Widget btnSwitch() {
    return GestureDetector(
      onTap: onPressSwitch,
      child: FContainer(
        height: 50.w,
        child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
          LoadAssetImage("login/blckback", width: 60.w, height: 60.w),
        ]),
      ),
    );
  }

  void onPressSwitch() {
    FocusManager.instance.primaryFocus?.unfocus();
    WidgetUtils().popPage();
  }

  Widget btnright() {
    return GestureDetector(
      onTap: onPrescript,
      child: FContainer(
        height: 60.w,
        child: Column(
          children: [
            SizedBox(height: 12.w),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              FText("登录遇到问题？", size: 28.sp, color: const Color(0xFFFFFFFF))
            ]),
          ],
        ),
      ),
    );
  }

  void onPrescript() {
    WidgetUtils().pushPage(FeedsetbackPage(
        FeedbackType.LOGIN_PROBLEM_FEEDBACK.title,
        FeedbackType.LOGIN_PROBLEM_FEEDBACK.hint,
        FeedbackType.LOGIN_PROBLEM_FEEDBACK.value,
        setBack: setScrip));
  }

  void setScrip(String path, String spath, List<String> rest) {
    DioUtils().asyncHttpRequest(
      NetConstants.roomFeedback,
      method: DioMethod.POST,
      params: {"advice": path, "type": 1, "phone": spath, "img_info": rest},
      onSuccess: onReportSuccess,
    );
  }

  void onReportSuccess(response) {
    WidgetUtils().showToast("反馈成功");
    WidgetUtils().popPage();
  }

  Widget inputContent() {
    return ListView(controller: scroll, children: [
      SizedBox(height: 98.h),
      Align(
        alignment: Alignment.centerLeft,
        child: Consumer<LoginProvider>(builder: (_, pro, __) {
          return Container(
            padding: EdgeInsets.only(left: 40.w),
            child: FText("手机登录",
                size: 48.sp,
                color: const Color(0xFF1E1D23),
                weight: FontWeight.bold),
          );
        }),
      ),
      SizedBox(height: 40.h),
      //手机登陆
      LoginInput(
        key: const Key("INPUT_PHONE"),
        controller: phoneCtr,
        onInput: onPhoneInput,
        suffix: phoneSuffix(),
        inputType: LoginInputType.INPUT_PHONE,
      ),
    ]);
  }

  Widget btngroup() {
    return GestureDetector(
      onTap: onPresbtngroup,
      child: FContainer(
        height: 60.w,
        child: Column(
          children: [
            SizedBox(height: 12.w),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              FText("账号登录 ",
                  size: 28.sp,
                  weight: FontWeight.bold,
                  color: const Color(0xFF222222)),
              LoadAssetImage("login/group1", width: 20.w, height: 20.w),
            ]),
          ],
        ),
      ),
    );
  }

  void onPresbtngroup() {
    WidgetUtils().pushPage(const Loginpassword(), fun: () {
      // setState(() {});
    });
  }

  void onAccept(accept) => provider.hasAccept = accept;

  @override
  void didChangeMetrics() {
    super.didChangeMetrics();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      if (!mounted) return;
      double keyboardHeight = MediaQuery.of(context).viewInsets.bottom;
      if (keyboardHeight <= 0) return;
      double max = scroll.position.maxScrollExtent;
      scroll.animateTo(max,
          duration: const Duration(milliseconds: 200), curve: Curves.linear);
      Future.delayed(const Duration(milliseconds: 200), () {
        double max2 = scroll.position.maxScrollExtent;
        scroll.animateTo(max2,
            duration: const Duration(milliseconds: 1), curve: Curves.linear);
      });
    });
  }

  Future<void> onPhoneInput(String value) async {
    value = value.replaceAll(" ", "").trim();
    provider.onPhoneInput(value);
  }

  Widget phoneSuffix() {
    return Consumer<LoginProvider>(builder: (_, pro, __) {
      Widget btn = GestureDetector(
        onTap: onClean,
        child: FContainer(
          width: 60.w,
          height: 60.w,
          child: const Icon(Icons.clear, color: Color(0xFFBBBBBB)),
        ),
      );
      return Visibility(visible: pro.phone.isNotEmpty, child: btn);
    });
  }

  void onClean() {
    phoneCtr.clear();
    provider.onPhoneInput("");
  }
}
