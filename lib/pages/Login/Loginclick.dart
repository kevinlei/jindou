import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class Loginclick extends StatefulWidget {
  const Loginclick({super.key});

  @override
  State<Loginclick> createState() => _LoginclickState();
}

class _LoginclickState extends State<Loginclick> with WidgetsBindingObserver {
  LoginProvider provider = LoginProvider();

  @override
  void initState() {
    super.initState();
    provider.phone = "15210506666";
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => provider,
        builder: (_, __) {
          return Scaffold(
            backgroundColor: const Color(0xFFFFFFFF),
            body: FContainer(
              padding: EdgeInsets.only(left: 30.w, right: 30.w, bottom: 20.w),
              image: ImageUtils().getAssetsImage("login/bg2"),
              imageFit: BoxFit.fill,
              imageAlign: Alignment.topCenter,
              child: SafeArea(child: loginContent()),
            ),
          );
        });
  }

  Widget loginContent() {
    return Column(children: [
      Column(
        children: [
          SizedBox(height: 10.w),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                btnSwitch(),
                const Spacer(),
                btnright(),
              ],
            ),
          ),
          SizedBox(height: 100.w),
          btnbgyin(),
          SizedBox(height: 30.h),
          FLoginButton(
            "一键登录/注册",
            iponeBg: false,
            txtSize: 35.sp,
            isActivate: true,
            txtColor: const Color(0xFFFF753F),
            padding: 40.w,
            onPress: quickLogin,
          ),
          btngroup(),
          btnAsset(),
          SizedBox(height: 60.h),
          FProAccept(const [
            ProtocolType.PRO_USER,
            ProtocolType.PRO_PRIVACY,
            ProtocolType.FUTKAN,
          ], onAccept: onAccept),
          SizedBox(height: 80.w),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                // provider.hasAccept?btnapple():btnapples(),
                btnapple(),
                SizedBox(width: 40.w),
                btnwx(),
                SizedBox(width: 40.w),
                btnqq(),
                SizedBox(width: 40.w),
                btndouyin(),
              ],
            ),
          ),
        ],
      ),
    ]);
  }

  Widget btnSwitch() {
    return GestureDetector(
      onTap: onPressSwitch,
      child: FContainer(
        height: 50.w,
        child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
          LoadAssetImage("public/back", width: 60.w, height: 60.w),
        ]),
      ),
    );
  }

  void onPressSwitch() {
    WidgetUtils().popPage();
  }

  Widget btngroup() {
    return GestureDetector(
      onTap: onPresbtngroup,
      child: FContainer(
        height: 60.w,
        child: Column(
          children: [
            SizedBox(height: 12.w),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              FText("其他号码登录 ",
                  size: 28.sp,
                  weight: FontWeight.bold,
                  color: const Color(0xFFFFFFFF)),
              LoadAssetImage("login/group", width: 20.w, height: 20.w),
            ]),
          ],
        ),
      ),
    );
  }

  void onPresbtngroup() {
    // WidgetUtils().pushPage(LoginPhonePage(), fun: () {
    //   setState(() {});
    // });
  }

  Widget btnAsset() {
    return GestureDetector(
      onTap: onruntroup,
      child: FContainer(
        height: 60.w,
        child: Column(
          children: [
            SizedBox(height: 12.w),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              FText("其它登录 ",
                  size: 28.sp,
                  weight: FontWeight.bold,
                  color: const Color(0xFFFFFFFF)),
              LoadAssetImage("login/group", width: 20.w, height: 20.w),
            ]),
          ],
        ),
      ),
    );
  }

  void onruntroup() {}

  Widget btnright() {
    return GestureDetector(
      onTap: onPrescript,
      child: FContainer(
        height: 60.w,
        child: Column(
          children: [
            SizedBox(height: 12.w),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              FText("登录遇到问题？", size: 28.sp, color: const Color(0xFFFFFFFF))
            ]),
          ],
        ),
      ),
    );
  }

  void onPrescript() {
    WidgetUtils().pushPage(FeedsetbackPage(
        FeedbackType.LOGIN_PROBLEM_FEEDBACK.title,
        FeedbackType.LOGIN_PROBLEM_FEEDBACK.hint,
        FeedbackType.LOGIN_PROBLEM_FEEDBACK.value,
        setBack: setScrip));
  }

  void setScrip(String path, String spath, List<String> rest) {
    DioUtils().asyncHttpRequest(
      NetConstants.roomFeedback,
      method: DioMethod.POST,
      params: {"advice": path, "type": 1, "phone": spath, "img_info": rest},
      onSuccess: onReportSuccess,
    );
  }

  void onReportSuccess(response) {
    WidgetUtils().showToast("反馈成功");
    WidgetUtils().popPage();
  }

  Widget btnbgyin() {
    return FContainer(
      child: Column(
        children: [
          LoadAssetImage("login/bg1", width: 500.w),
          FText(
            provider.phone.replaceRange(3, 7, "****"),
            size: 56.sp,
            weight: FontWeight.bold,
          ),
          SizedBox(height: 20.h),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              LoadAssetImage("login/authentication", width: 40.w),
              FText(
                '认证服务由中国移动提供',
                size: 25.sp,
                weight: FontWeight.bold,
              ),
            ],
          ),
        ],
      ),
    );
  }

  Future<void> quickLogin() async {
    bool state = provider.hasAccept;
    if (!state) return WidgetUtils().showToast("请先同意用户协议");
    WidgetUtils().showLoading();
    WidgetUtils().pushPage(Logincode(phone: '15210506666'));
  }

  void onAccept(accept) => provider.hasAccept = accept;
}

//苹果按钮
Widget btnapples() {
  return GestureDetector(
    child: const FContainer(),
  );
}

//苹果按钮
Widget btnapple() {
  return GestureDetector(
    onTap: onPresapple,
    child: FContainer(
      child: Column(
        children: [
          SizedBox(height: 12.w),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            LoadAssetImage("login/apple", width: 80.w, height: 80.w),
          ]),
        ],
      ),
    ),
  );
}

void onPresapple() {
  // WidgetUtils().pushPage(LoginPhonePage(), fun: () {
  //   setState(() {});
  // });
}
//wx按钮
Widget btnwx() {
  return GestureDetector(
    onTap: onPreswx,
    child: FContainer(
      child: Column(
        children: [
          SizedBox(height: 12.w),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            LoadAssetImage("login/wx", width: 80.w, height: 80.w),
          ]),
        ],
      ),
    ),
  );
}

void onPreswx() {
  // WidgetUtils().pushPage(LoginPhonePage(), fun: () {
  //   setState(() {});
  // });
}

//qq按钮
Widget btnqq() {
  return GestureDetector(
    onTap: onPresqq,
    child: FContainer(
      child: Column(
        children: [
          SizedBox(height: 12.w),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            LoadAssetImage("login/qq", width: 80.w, height: 80.w),
          ]),
        ],
      ),
    ),
  );
}

void onPresqq() {
  // WidgetUtils().pushPage(LoginPhonePage(), fun: () {
  //   setState(() {});
  // });
}

//抖音按钮
Widget btndouyin() {
  return GestureDetector(
    onTap: onPresdouyin,
    child: FContainer(
      child: Column(
        children: [
          SizedBox(height: 12.w),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            LoadAssetImage("login/douyin", width: 80.w, height: 80.w),
          ]),
        ],
      ),
    ),
  );
}

void onPresdouyin() {
  WidgetUtils().pushPage(const Loginipone(), fun: () {
    // setState(() {});
  });
}
