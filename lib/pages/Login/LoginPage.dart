import 'dart:io';

import 'package:dart_ping_ios/dart_ping_ios.dart';
import 'package:echo/import.dart';
import 'package:flutter/material.dart';
import '../../main.dart';
import 'Loginclick.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> with WidgetsBindingObserver {
  LoginProvider provider = LoginProvider();
  String preToken = "";

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => provider,
        builder: (_, __) {
          return Scaffold(
            backgroundColor: const Color(0xFF0B0017),
            body: Container(
              padding: EdgeInsets.only(left: 30.w, right: 30.w, bottom: 20.w),
              decoration: BoxDecoration(gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Color(0xFFFF7B69),
                  Color(0XFFFFAB7C),
                ],
              ),),
              child: SafeArea(child: loginContent()),
            ),
          );
        });
  }

  Widget loginContent() {
    return Column(children: [
      Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(height: 10.w),
            // btnSwitch(),
            const Spacer(),
            btnright(),
          ],
        ),
      ),
      Expanded(
        child: Column(
          children: [
            SizedBox(height: 100.w),
            btnbgyin(),
            SizedBox(height: ScreenUtil().screenHeight / 7),
            FLoginButton(
              "手机号一键登录/注册",
              iponeBg: true,
              txtSize: 35.sp,
              isActivate: true,
              txtColor: const Color(0xFFFF753F),
              padding: 40.w,
              onPress: quickLogin,
            ),
            btngroup(),
            // SizedBox(height: 300.w),

            //暂时屏蔽
            // btnAsset(),

            // Container(
            //   child: Row(
            //     mainAxisAlignment: MainAxisAlignment.center,
            //     children: [
            //       // provider.hasAccept?btnapple():btnapples(),
            //       btnapple(),
            //       SizedBox(width: 40.w),
            //       btnwx(),
            //       SizedBox(width: 40.w),
            //       btnqq(),
            //       SizedBox(width: 40.w),
            //       btndouyin(),
            //     ],
            //   ),
            // ),
          ],
        ),
      ),
      FProAccept(
        const [
          ProtocolType.PRO_USER,
          ProtocolType.PRO_PRIVACY,
          ProtocolType.FUTKAN,
        ],
        onAccept: onAccept,
      ),
    ]);
  }

  Widget btnSwitch() {
    return GestureDetector(
      onTap: onPressSwitch,
      child: FContainer(
        height: 50.w,
        child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
          LoadAssetImage("public/back", width: 60.w, height: 60.w),
        ]),
      ),
    );
  }

  void onPressSwitch() {
    // WidgetUtils().pushPage(LoginPhonePage(), fun: () {
    //   setState(() {});
    // });
  }
  Widget btngroup() {
    return GestureDetector(
      onTap: onPresbtngroup,
      child: FContainer(
        height: 60.w,
        child: Column(
          children: [
            SizedBox(height: 12.w),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              FText("其他手机号登录/注册 ",
                  size: 28.sp,
                  weight: FontWeight.bold,
                  color: const Color(0xFFFFFFFF)),
              LoadAssetImage("login/group", width: 20.w, height: 20.w),
            ]),
          ],
        ),
      ),
    );
  }

  bool validateInput(String? input) {
    if (input == null) {
      return false;
    }

    if (input.isEmpty) {
      return false;
    }

    return true;
  }

//其他登录
  Future<void> onPresbtngroup() async {
    WidgetUtils().pushPage(const Loginipone(), fun: () {
      setState(() {});
    });
  }

  Widget btnAsset() {
    return GestureDetector(
      onTap: onruntroup,
      child: FContainer(
        height: 60.w,
        child: Column(
          children: [
            SizedBox(height: 12.w),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              FText("其它登录 ",
                  size: 25.sp,
                  weight: FontWeight.bold,
                  color: const Color(0xFFFFFFFF)),
              LoadAssetImage("login/group", width: 20.w, height: 20.w),
            ]),
          ],
        ),
      ),
    );
  }

  void onruntroup() {
    WidgetUtils().pushPage(const Loginpassword(), fun: () {
      setState(() {});
    });
  }

  Widget btnright() {
    return GestureDetector(
      onTap: onPrescript,
      child: FContainer(
        height: 60.w,
        child: Column(
          children: [
            SizedBox(height: 12.w),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              FText("登录遇到问题？", size: 28.sp, color: const Color(0xFFFFFFFF))
            ]),
          ],
        ),
      ),
    );
  }

  void onPrescript() {
    WidgetUtils().pushPage(FeedsetbackPage(
        FeedbackType.LOGIN_PROBLEM_FEEDBACK.title,
        FeedbackType.LOGIN_PROBLEM_FEEDBACK.hint,
        FeedbackType.LOGIN_PROBLEM_FEEDBACK.value,
        setBack: setScrip));
  }

  void setScrip(String path, String spath, List<String> rest) {
    DioUtils().asyncHttpRequest(
      NetConstants.roomFeedback,
      method: DioMethod.POST,
      params: {"advice": path, "type": 1, "phone": spath, "img_info": rest},
      onSuccess: onReportSuccess,
    );
  }

  void onReportSuccess(response) {
    WidgetUtils().showToast("反馈成功");
    WidgetUtils().popPage();
  }

  Widget btnbgyin() {
    return FContainer(
      child: LoadAssetImage("login/bg1", width: 500.w),
    );
  }

  Future<void> quickLogin() async {
    if (Platform.isIOS) {
      if (validateInput(AppConstants.Launchl) == true) {
        AppManager().initAppEngines();
        PluginUtils().initLogin();
        AppSDK().initWXLogin();
        // 初始化ios网络插件
        if (Platform.isIOS) {
          DartPingIOS.register();
        }
        await AppConstants.initAppInfo();
      }
    }
    bool state = provider.hasAccept;
    if (!state) return WidgetUtils().showToast("请先同意用户协议");
    if (Platform.isAndroid) {
      // android 一键登录
      WidgetUtils().showLoading();
      Map? result = await PluginUtils().prefetchPhone();
      WidgetUtils().cancelLoading();
      if (result?["success"] == true) {
        onLoginAn();
      } else {
        WidgetUtils().pushPage(const Loginipone());
      }
    } else {
      // ios 一键登录
      PluginUtils().setListener("onQuickLoginSuccess", onQuickLoginSuccess);
      await PluginUtils().prefetchPhone();
    }
  }

  // android 一键登录
  Future<void> onLoginAn() async {
    Map? result = await PluginUtils().loginPass();
    if (result == null ||
        result["success"] != true ||
        result["token"] == null ||
        result["token"].isEmpty ||
        result["gyuid"] == null ||
        result["gyuid"].isEmpty) {
      return WidgetUtils().pushPage(const Loginipone());
    }
    preToken = result["gyuid"];
    requestLogin(preToken, result["token"]);
  }

  // ios一键登录
  Future<void> onQuickLoginSuccess(
      String key, String method, dynamic preResult) async {
    WidgetUtils().cancelLoading();
    if (method != "onQuickLoginSuccess") return;
    bool preSuccessa = preResult?["success"] ?? false;
    String loginToken = preResult?["token"] ?? "";
    if (!preSuccessa || loginToken.isEmpty) {
      return WidgetUtils().pushPage(const Loginipone());
    }
    preToken = loginToken;
    PluginUtils().setListener("onLoginPassSuccess", onLoginPassSuccess);
    await PluginUtils().loginPass();
    return setState(() {});
  }

  // ios一键登录
  Future<void> onLoginPassSuccess(
      String key, String method, dynamic result) async {
    WidgetUtils().cancelLoading();
    if (method != "onLoginPassSuccess" || result == null || result.isEmpty) {
      return;
    }
    bool success = result["success"] ?? false;
    if (success) {
      requestLogin(preToken, result?["message"] ?? "");
    }
  }

  void onAccept(accept) => provider.hasAccept = accept;
}

void requestLogin(String token, String authToken) async {
  await DioUtils().httpcodeRequest(
    NetConstants.loginUser,
    method: DioMethod.POST,
    isSuccess: true,
    params: {
      "login_type": "one_click",
      "platform": Platform.isAndroid ? "android" : "ios",
      "gyuid": token,
      "gy_token": authToken
    },
    onSuccess: (res, code, msg) => onLoginSuccess(res, code, msg),
  );
}

Future<void> onLoginSuccess(response, code, msg) async {
  //登录验证码code不同处理
  // 昵称
  String name = "";
  // 头像
  String HeadIcon = "";
  // 生日
  int birthday = 0;
  // 性别
  int sex = 0;
  if (int.parse(code.toString()) == 101000) {
    UserConstants.userginInfo = LoginUserModel.fromJson((response));
    AppConstants.accessToken = UserConstants.userginInfo?.token ?? "";
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString(
        SharedKey.KEY_TOKEN.value, UserConstants.userginInfo?.token ?? "");
    preferences.setString(SharedKey.KEY_CODE.value, code.toString());
    HeadIcon = UserConstants.userginInfo?.info.headIcon ?? "";
    name = UserConstants.userginInfo?.info.name ?? "";
    birthday = UserConstants.userginInfo?.info.birthday ?? 0;
    sex = UserConstants.userginInfo?.info.sex ?? 0;
    // 注册统计
    Future.delayed(const Duration(seconds: 0), () async {
      AppManager().xinstallFlutterPlugin.reportRegister();
      AppManager().xinstallFlutterPlugin.reportEventWhenOpenDetailInfo(
          "register", 1, "userId: ${UserConstants.userginInfo!.info.userId}");
    });
    WidgetUtils().pushPage(Loginpersonal(
      name,
      birthday,
      sex,
      HeadIcon,
    ));
  } else if (int.parse(code.toString()) == 1) {
    AppConstants.accessToken = response["token"].toString();
    UserConstants.userginInfo = LoginUserModel.fromJson((response));
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString(
        SharedKey.KEY_TOKEN.value, response["token"].toString());
    preferences.setString(SharedKey.KEY_CODE.value, code.toString());
    // AppManager().connectMq();
    AppManager().loginSDK();
    WidgetUtils().pushRemove(const NavigatorPage());
  } else {
    WidgetUtils().showToast(msg);
  }

  return code;
}

//苹果按钮
Widget btnapples() {
  return GestureDetector(
    child: const FContainer(),
  );
}

//苹果按钮
Widget btnapple() {
  return GestureDetector(
    onTap: onPresapple,
    child: FContainer(
      child: Column(
        children: [
          SizedBox(height: 12.w),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            LoadAssetImage("login/apple", width: 80.w, height: 80.w),
          ]),
        ],
      ),
    ),
  );
}

void onPresapple() {
  // WidgetUtils().pushPage(LoginPhonePage(), fun: () {
  //   setState(() {});
  // });
}

//wx按钮
Widget btnwx() {
  return GestureDetector(
    onTap: onPreswx,
    child: FContainer(
      child: Column(
        children: [
          SizedBox(height: 12.w),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            LoadAssetImage("login/wx", width: 80.w, height: 80.w),
          ]),
        ],
      ),
    ),
  );
}

void onPreswx() {
  // WidgetUtils().pushPage(LoginPhonePage(), fun: () {
  //   setState(() {});
  // });
}

//qq按钮
Widget btnqq() {
  return GestureDetector(
    onTap: onPresqq,
    child: FContainer(
      child: Column(
        children: [
          SizedBox(height: 12.w),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            LoadAssetImage("login/qq", width: 80.w, height: 80.w),
          ]),
        ],
      ),
    ),
  );
}

void onPresqq() {
  // WidgetUtils().pushPage(LoginPhonePage(), fun: () {
  //   setState(() {});
  // });
}

//抖音按钮
Widget btndouyin() {
  return GestureDetector(
    onTap: onPresdouyin,
    child: FContainer(
      child: Column(
        children: [
          SizedBox(height: 12.w),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            LoadAssetImage("login/douyin", width: 80.w, height: 80.w),
          ]),
        ],
      ),
    ),
  );
}

void onPresdouyin() {
  WidgetUtils().pushPage(const Loginipone(), fun: () {
    // setState(() {});
  });
}
