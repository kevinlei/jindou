import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class Loginforget extends StatefulWidget {
  const Loginforget({super.key});

  @override
  State<Loginforget> createState() => _LoginforgetState();
}

class _LoginforgetState extends State<Loginforget> with WidgetsBindingObserver {
  TextEditingController phoneCtr = TextEditingController();
  LoginProvider provider = LoginProvider();
  ScrollController scroll = ScrollController();
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    provider.decodePwd();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => provider,
        builder: (_, __) {
          return Scaffold(
            backgroundColor: const Color(0xFF0B0017),
            body: FContainer(
              padding: EdgeInsets.only(left: 30.w, right: 30.w, bottom: 20.w),
              image: ImageUtils().getAssetsImage("public/huibg"),
              imageFit: BoxFit.fill,
              imageAlign: Alignment.topCenter,
              child: SafeArea(child: loginContent()),
            ),
          );
        });
  }

  Widget loginContent() {
    return Column(children: [
      loginAppbar(),
      Expanded(child: inputContent()),
      Column(
        children: [
          Consumer<LoginProvider>(builder: (_, pro, __) {
            return FcationButton(
              "获取验证码",
              isActivate: pro.phone.isNotEmpty,
              padding: 40.w,
              onPress: pro.phone.isNotEmpty ? quickLogin : null,
            );
          }),
        ],
      ),
    ]);
  }

  //获取验证码
  Future<void> quickLogin() async {
    var value = phoneCtr.value.text.replaceAll(" ", "").trim();
    if (value.length != 11) {
      WidgetUtils().showToast("请输入正确的手机号");
      return;
    }
    if (provider.onLoginCode) {
      //验证码登陆
      bool result = await provider.requestPhoneCode(1);
      if (result) {
        WidgetUtils().pushPage(Loginpwdcode(
          phone: value,
        ));
      }
    }
  }

  Widget loginAppbar() {
    return Row(children: [
      btnSwitch(),
      const Spacer(),
      btnrbaright(),
    ]);
  }

  Widget btnrbaright() {
    return GestureDetector(
      onTap: onPresbarcript,
      child: FContainer(
        height: 60.w,
        child: Column(
          children: [
            SizedBox(height: 12.w),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              FText("登录遇到问题？", size: 28.sp, color: const Color(0xFF222222))
            ]),
          ],
        ),
      ),
    );
  }

  void onPresbarcript() {
    WidgetUtils().pushPage(FeedsetbackPage(
        FeedbackType.LOGIN_PROBLEM_FEEDBACK.title,
        FeedbackType.LOGIN_PROBLEM_FEEDBACK.hint,
        FeedbackType.LOGIN_PROBLEM_FEEDBACK.value,
        setBack: setScrip));
  }

  void setScrip(String path, String spath, List<String> rest) {
    DioUtils().asyncHttpRequest(
      NetConstants.roomFeedback,
      method: DioMethod.POST,
      params: {"advice": path, "type": 1, "phone": spath, "img_info": rest},
      onSuccess: onReportSuccess,
    );
  }

  void onReportSuccess(response) {
    WidgetUtils().showToast("反馈成功");
    WidgetUtils().popPage();
  }

  Widget btnSwitch() {
    return GestureDetector(
      onTap: onPressSwitch,
      child: FContainer(
        height: 50.w,
        child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
          LoadAssetImage("login/blckback", width: 60.w, height: 60.w),
        ]),
      ),
    );
  }

  void onPressSwitch() {
    FocusManager.instance.primaryFocus?.unfocus();
    WidgetUtils().popPage();
  }

  Widget inputContent() {
    return ListView(controller: scroll, children: [
      SizedBox(height: 98.h),
      Align(
        alignment: Alignment.centerLeft,
        child: Consumer<LoginProvider>(builder: (_, pro, __) {
          return Container(
            padding: EdgeInsets.only(left: 40.w),
            child: FText("忘记密码",
                size: 48.sp,
                color: const Color(0xFF1E1D23),
                weight: FontWeight.bold),
          );
        }),
      ),
      SizedBox(height: 40.h),
      //手机登陆
      LoginInput(
        key: const Key("INPUT_PHONE"),
        controller: phoneCtr,
        onInput: onPhoneInput,
        suffix: phoneSuffix(),
        inputType: LoginInputType.INPUT_PHONE,
      ),
    ]);
  }

  void onAccept(accept) => provider.hasAccept = accept;

  @override
  void didChangeMetrics() {
    super.didChangeMetrics();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      if (!mounted) return;
      double keyboardHeight = MediaQuery.of(context).viewInsets.bottom;
      if (keyboardHeight <= 0) return;
      double max = scroll.position.maxScrollExtent;
      scroll.animateTo(max,
          duration: const Duration(milliseconds: 200), curve: Curves.linear);
      Future.delayed(const Duration(milliseconds: 200), () {
        double max2 = scroll.position.maxScrollExtent;
        scroll.animateTo(max2,
            duration: const Duration(milliseconds: 1), curve: Curves.linear);
      });
    });
  }

  @override
  void dispose() {
    phoneCtr.dispose();
    scroll.dispose();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  Future<void> onPhoneInput(String value) async {
    value = value.replaceAll(" ", "").trim();
    provider.onPhoneInput(value);
  }

  Future<void> onPwdInput(String value) async {
    value = value.replaceAll(" ", "").trim();
    provider.onPwdInput(value);
  }

  Widget phoneSuffix() {
    return Consumer<LoginProvider>(builder: (_, pro, __) {
      Widget btn = GestureDetector(
        onTap: onClean,
        child: FContainer(
          width: 60.w,
          height: 60.w,
          child: const Icon(Icons.clear, color: Color(0xFFBBBBBB)),
        ),
      );
      return Visibility(visible: pro.phone.isNotEmpty, child: btn);
    });
  }

  void onClean() {
    phoneCtr.clear();
    provider.onPhoneInput("");
  }

  Widget phoneLead() {
    return FContainer(
      padding: EdgeInsets.only(right: 10.w),
      margin: EdgeInsets.only(right: 10.w),
      border: const Border(right: BorderSide(color: Color(0xFFBBBBBB))),
      child: FText("+86",
          size: 34.sp, color: Colors.white, weight: FontWeight.bold),
    );
  }
}
