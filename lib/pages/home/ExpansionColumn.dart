import 'dart:async';

import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class ExpansionColumn extends StatefulWidget {
  final TabController controller;
  const ExpansionColumn(this.controller, {super.key});

  @override
  State<ExpansionColumn> createState() => _ExpansionColumnState();
}

class _ExpansionColumnState extends State<ExpansionColumn>
    with TickerProviderStateMixin, AutomaticKeepAliveClientMixin {
  late TabController tabController;

  int page = 0;
  // 新人
  List<UserList> newList = [];
  bool isNewLoad = true;

  // 推荐
  List<UserList> recoList = [];
  bool isRecoLoad = true;

  // 延时操作
  Timer? timer;

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    tabController = TabController(length: 2, vsync: this, initialIndex: 0);
    initList(false);
  }

  Future<void> onRefresh() async {
    page = 0;
    initList(false);
  }

  Future<void> callLoad() async {
    if ((tabController.index == 0 && !isRecoLoad) ||
        (tabController.index == 1 && !isNewLoad)) return;
    page += 1;
    initList(true);
  }

  void initList(bool isOnLoad) async {
    await DioUtils().httpRequest(
      NetConstants.expansionColumn,
      params: {
        "per_page": 20,
        "page": page * 20,
        "recommend_type": tabController.index == 0 ? 1 : 0,
      },
      loading: false,
      onSuccess: (res) async {
        if (res == null) return;
        List<UserList> list = (res as List<dynamic>)
            .map((e) => UserList.fromJson(e as Map<String, dynamic>))
            .toList();
        if (tabController.index == 0) {
          isOnLoad ? recoList.addAll(list) : recoList = list;
          if (list.length < 20) {
            isRecoLoad = false;
          } else {
            isRecoLoad = true;
          }
        } else {
          isOnLoad ? newList.addAll(list) : newList = list;
          if (list.length < 20) {
            isNewLoad = false;
          } else {
            isNewLoad = true;
          }
        }
        if (!mounted) return;
        setState(() {});
      },
    );
  }

  @override
  void dispose() {
    widget.controller.dispose();
    tabController.dispose();
    timer?.cancel();
    timer = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return GestureDetector(
      onHorizontalDragUpdate: (DragUpdateDetails details) {
        if (timer != null) return;
        if (details.delta.dx < -10 || details.delta.dx > 10) {
          timer = Timer(const Duration(milliseconds: 200), () {
            timer!.cancel();
            timer = null;
          });
        }
        if (details.delta.dx < -10 && tabController.index == 1) {
          widget.controller.animateTo(1);
        } else if (details.delta.dx > 10 && tabController.index == 1) {
          tabController.animateTo(0);
          setState(() {});
          initList(false);
        } else if (details.delta.dx < -10 && tabController.index == 0) {
          tabController.animateTo(1);
          setState(() {});
          initList(false);
        }
      },
      child: Column(
        children: [
          SizedBox(height: 20.w),
          Row(
            children: [
              headerItem("推荐", 0),
              SizedBox(width: 10.w),
              headerItem("新人", 1),
            ],
          ),
          SizedBox(height: 10.w),
          Expanded(
            child: TabBarView(
              controller: tabController,
              physics: const NeverScrollableScrollPhysics(),
              children: [
                userListView(recoList, tabController.index == 0 && isRecoLoad),
                userListView(newList, tabController.index == 1 && isNewLoad),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget headerItem(String text, int index) {
    Widget child = FContainer(
      radius: BorderRadius.circular(30.w),
      border:
          tabController.index == index ? Border.all(color: Colors.white) : null,
      padding: EdgeInsets.symmetric(vertical: 8.w, horizontal: 16.w),
      child: FText(
        text,
        size: 30.sp,
        weight: tabController.index == index ? FontWeight.bold : null,
        wordSpacing: 1.5,
      ),
    );
    return GestureDetector(
      child: child,
      onTap: () {
        tabController.animateTo(index);
        setState(() {});
        initList(false);
      },
    );
  }

  Widget userListView(List<UserList> list, bool isLoad) {
    return FRefreshLayout(
      firstRefresh: false,
      onRefresh: onRefresh,
      onLoad: isLoad ? callLoad : null,
      child: ListView.builder(
        padding: EdgeInsets.only(top: 30.w),
        itemBuilder: (context, index) => userItem(list[index]),
        itemCount: list.length,
      ),
    );
  }

  Widget userItem(UserList userInfo) {
    Widget child = FContainer(
      padding: EdgeInsets.all(20.w),
      margin: EdgeInsets.only(bottom: 20.w),
      radius: BorderRadius.circular(30.w),
      color: Colors.white,
      child: Column(
        children: [
          Row(
            children: [
              FContainer(
                width: 130.w,
                height: 130.w,
                child: Stack(
                  children: [
                    FContainer(
                      radius: BorderRadius.circular(30.w),
                      imageFit: BoxFit.cover,
                      image: ImageUtils().getImageProvider(
                          NetConstants.ossPath + userInfo.headIcon),
                    ),
                    Visibility(
                      visible: userInfo.exteriorRoomId != 0,
                      child: const LoadAssetImage(
                          "home/expansionColumn/herderImaMask"),
                    ),
                  ],
                ),
              ),
              SizedBox(width: 10.w),
              FContainer(
                constraints: BoxConstraints(maxWidth: 320.w),
                height: 130.w,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Expanded(
                            child: FText(
                          userInfo.name,
                          color: Colors.black,
                          size: 30.sp,
                          weight: FontWeight.w500,
                          overflow: TextOverflow.ellipsis,
                        )),
                        SizedBox(width: 5.w),
                        GenderAge(userInfo.sex == 1, userInfo.birthday)
                      ],
                    ),
                    FText(
                      userInfo.sign,
                      size: 22.sp,
                      color: const Color(0xFFA7A7A7),
                      overflow: TextOverflow.ellipsis,
                    )
                  ],
                ),
              ),
              const Spacer(),
              SizedBox(
                height: 130.w,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    currentState(userInfo.online),
                    SizedBox(height: 10.w),
                    privateChat(userInfo),
                  ],
                ),
              )
            ],
          ),
          Visibility(
              visible: userInfo.photos != null && userInfo.photos!.isNotEmpty,
              child: SizedBox(height: 10.w)),
          userInfo.photos != null && userInfo.photos!.isNotEmpty
              ? Row(
                  children: [
                    SizedBox(width: 130.w),
                    ...photos(userInfo.photos!),
                  ],
                )
              : const SizedBox(),
        ],
      ),
    );
    return GestureDetector(
      onTap: () {
        if (userInfo.exteriorRoomId != 0) {
          // WidgetUtils().pushPage(RoomPage(userInfo.exteriorRoomId));
          AppManager().joinRoom(userInfo.exteriorRoomId);
        } else {
          WidgetUtils().pushPage(PersonHomepage(userId: userInfo.userId));
        }
      },
      child: child,
    );
  }

  List<Widget> photos(List<String> phs) {
    List<Widget> list = [];
    for (var i = 0; i < (phs.length > 3 ? 3 : phs.length); i++) {
      list.add(FContainer(
        radius: BorderRadius.circular(10.w),
        margin: EdgeInsets.only(left: 25.w),
        width: 140.w,
        height: 140.w,
        imageFit: BoxFit.cover,
        image: ImageUtils().getImageProvider(NetConstants.ossPath + phs[i]),
      ));
    }
    return list;
  }

  Widget currentState(int status) {
    Color clor = status == 20
        ? const Color(0xFFFF753F)
        : const Color(0xFF222222).withOpacity(0.4);
    String text = status == 20 ? "在线中" : "离线中";
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        FContainer(
          radius: BorderRadius.circular(50),
          width: 10.w,
          height: 10.w,
          color: clor,
        ),
        SizedBox(width: 5.w),
        FText(
          text,
          size: 20.sp,
          color: clor,
        )
      ],
    );
  }

  Widget privateChat(UserList userInfo) {
    return FButton(
      onTap: () {
        WidgetUtils().pushPage(MsgChat(userInfo.hxInfo.uid,
            HxMsgExt(userInfo.userId, userInfo.headIcon, userInfo.name)));
      },
      isTheme: true,
      padding: EdgeInsets.symmetric(vertical: 16.w, horizontal: 20.w),
      radius: BorderRadius.circular(40.w),
      child: Row(
        children: [
          LoadAssetImage(
            "home/expansionColumn/privateChat",
            width: 30.w,
            height: 30.w,
          ),
          SizedBox(width: 10.w),
          const FText("私聊")
        ],
      ),
    );
  }
}
