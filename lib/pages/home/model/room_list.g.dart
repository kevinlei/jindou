// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'room_list.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RoomList _$RoomListFromJson(Map<String, dynamic> json) => RoomList(
      count: json['count'] as int,
      rooms: (json['rooms'] as List<dynamic>?)
          ?.map((e) => Rooms.fromJson(e as Map<String, dynamic>))
          .toList(),
      topRooms: (json['top_rooms'] as List<dynamic>?)
          ?.map((e) => Rooms.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$RoomListToJson(RoomList instance) => <String, dynamic>{
      'count': instance.count,
      'rooms': instance.rooms,
      'top_rooms': instance.topRooms,
    };

Rooms _$RoomsFromJson(Map<String, dynamic> json) => Rooms(
      id: json['ex_room_id'] as int,
      name: json['name'] as String,
      topicName: json['topic_name'] as String,
      topicPicture: json['topic_picture'] as String,
      hotNum: json['hot_num'] as int,
      personNum: json['person_num'] as int,
      isFollow: json['is_follow'] as int,
      image: json['image'] as String,
      owner: Owner.fromJson(json['owner'] as Map<String, dynamic>),
      usersImage: (json['users_image'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
    );

Map<String, dynamic> _$RoomsToJson(Rooms instance) => <String, dynamic>{
      'ex_room_id': instance.id,
      'name': instance.name,
      'topic_name': instance.topicName,
      'topic_picture': instance.topicPicture,
      'hot_num': instance.hotNum,
      'person_num': instance.personNum,
      'is_follow': instance.isFollow,
      'image': instance.image,
      'owner': instance.owner,
      'users_image': instance.usersImage,
    };

Owner _$OwnerFromJson(Map<String, dynamic> json) => Owner(
      ownerName: json['owner_name'] as String,
      ownerId: json['owner_id'] as int,
      ownerImage: json['owner_image'] as String,
    );

Map<String, dynamic> _$OwnerToJson(Owner instance) => <String, dynamic>{
      'owner_name': instance.ownerName,
      'owner_id': instance.ownerId,
      'owner_image': instance.ownerImage,
    };
