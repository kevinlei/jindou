import 'package:json_annotation/json_annotation.dart';

part 'room_list.g.dart';

@JsonSerializable()
class RoomList {
  final int count;
  final List<Rooms>? rooms;
  @JsonKey(name: 'top_rooms')
  final List<Rooms>? topRooms;

  const RoomList({
    required this.count,
    required this.rooms,
    required this.topRooms,
  });

  factory RoomList.fromJson(Map<String, dynamic> json) =>
      _$RoomListFromJson(json);

  Map<String, dynamic> toJson() => _$RoomListToJson(this);
}

@JsonSerializable()
class Rooms {
  @JsonKey(name: 'ex_room_id')
  final int id;
  final String name;
  @JsonKey(name: 'topic_name')
  final String topicName;
  @JsonKey(name: 'topic_picture')
  final String topicPicture;
  @JsonKey(name: 'hot_num')
  final int hotNum;
  @JsonKey(name: 'person_num')
  final int personNum;
  @JsonKey(name: 'is_follow')
  final int isFollow;
  final String image;
  final Owner owner;
  @JsonKey(name: 'users_image')
  final List<String>? usersImage;

  const Rooms({
    required this.id,
    required this.name,
    required this.topicName,
    required this.topicPicture,
    required this.hotNum,
    required this.personNum,
    required this.isFollow,
    required this.image,
    required this.owner,
    required this.usersImage,
  });

  factory Rooms.fromJson(Map<String, dynamic> json) => _$RoomsFromJson(json);

  Map<String, dynamic> toJson() => _$RoomsToJson(this);
}

@JsonSerializable()
class Owner {
  @JsonKey(name: 'owner_name')
  final String ownerName;
  @JsonKey(name: 'owner_id')
  final int ownerId;
  @JsonKey(name: 'owner_image')
  final String ownerImage;

  const Owner({
    required this.ownerName,
    required this.ownerId,
    required this.ownerImage,
  });

  factory Owner.fromJson(Map<String, dynamic> json) => _$OwnerFromJson(json);

  Map<String, dynamic> toJson() => _$OwnerToJson(this);
}
