import 'package:echo/import.dart';
import 'package:echo/widgets/my_widgets.dart';
import 'package:echo/widgets/myfont_weight.dart';
import 'package:flutter/material.dart';

import '../../widgets/RoomListWidget.dart';

class HomeRecommendWidget extends StatefulWidget {
  //推荐，无上面广告
  final TabController controller;

  const HomeRecommendWidget(this.controller, {super.key});

  @override
  State<HomeRecommendWidget> createState() => _HomeRecommendWidgetState();
}

class _HomeRecommendWidgetState extends State<HomeRecommendWidget>
    with AutomaticKeepAliveClientMixin {
  // 房间列表
  List<Rooms> roomList = [];

  // 房间总条数
  int coust = 0;

  // 分页
  int page = 0;

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    widget.controller.addListener(() {
      if (widget.controller.indexIsChanging && widget.controller.index == 1) {
        initRoomList(false);
      }
    });
    initRoomList(false);
  }

  Future<void> onRefresh() async {
    initRoomList(false);
  }

  Future<void> callLoad() async {
    page += 1;
    initRoomList(true);
  }

  void initRoomList(bool isLoad) async {
    if (!isLoad) {
      page = 0;
    }
    await DioUtils().httpRequest(
      NetConstants.recoRoomList,
      params: {
        "page_num": 20,
        "page_offset": page * 20,
        "user_id": UserConstants.userginInfo!.info.userId
      },
      loading: false,
      onSuccess: (res) async {
        if (res == null) return;
        RoomList list = RoomList.fromJson(res);
        List<Rooms> listRoom = [];
        listRoom.addAll(list.topRooms ?? []);
        listRoom.addAll(list.rooms ?? []);
        if (isLoad) {
          roomList.addAll(listRoom);
        } else {
          roomList = listRoom;
        }
        coust = list.count;
        setState(() {});
      },
    );
  }

  @override
  void dispose() {
    widget.controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return GestureDetector(
      onHorizontalDragUpdate: (DragUpdateDetails details) {
        if (details.delta.dx < -10 && widget.controller.index == 1) {
          widget.controller.animateTo(2);
        } else if (details.delta.dx > 10 && widget.controller.index == 1) {
          widget.controller.animateTo(0);
        }
      },
      child: FRefreshLayout(
        firstRefresh: false,
        onRefresh: onRefresh,
        onLoad: (roomList.isEmpty ||
                roomList.length == coust ||
                roomList.length % 20 != 0)
            ? null
            : callLoad,
        child: Column(
          children: [
            SizedBox(height: 20.w),
            BannerSwipe(BanbarSwper.HOME_COM.brand), //房间顶部广告
            SizedBox(height: 46.w),
            Row(
              children: [
                mySizeIcon("home/recommend/hot", size: 44.w),
                FText(
                  "热门房间",
                  color: Color(0xFF1A1A1A),
                  size: 32.sp,
                  weight: MyFontWeight.medium,
                ),
                // const Spacer(),
                // FText(
                //   "更多",
                //   size: 22.sp,
                //   color: const Color(0xFF545F63),
                // ),
                // LoadAssetImage("public/RightNavigation", width: 15.w),
              ],
            ),
            SizedBox(height: 20.w),
            roomList.isNotEmpty
                ? RoomListWidget(
                    roomList,
                    hotRoomStyle: true,
                  )
                : Padding(
                    padding: EdgeInsets.only(top: 300.w),
                    child: const FEmptyWidget(text: "暂时没有热门房间推荐哦"),
                  ),
          ],
        ),
      ),
    );
  }
}
