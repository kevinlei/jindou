import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class MyRoomWidget extends StatefulWidget {
  /*final TabController homeController;
  final TabController tabController;
  final int index;*/

  const MyRoomWidget(/*this.homeController, this.tabController, this.index,*/
      {super.key});

  @override
  State<MyRoomWidget> createState() => _MyRoomWidgetState();
}

class _MyRoomWidgetState extends State<MyRoomWidget>
    with AutomaticKeepAliveClientMixin, TickerProviderStateMixin {
  late TabController tabController;

  // 创建房间列表
  List<Rooms> creatRoomList = [];

  // 管理房间列表
  List<Rooms> adminRoomList = [];

  // 关注房间列表
  List<Rooms> followRoomList = [];

  int count = 0;

  // 标签
  List<RoomTypes> titles = [
    const RoomTypes(topicName: "收藏"),
    const RoomTypes(topicName: "管理"),
    const RoomTypes(topicName: "创建"),
  ];

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    // if (!mounted) return;
    tabController = TabController(length: 3, vsync: this, initialIndex: 0);
   /* widget.homeController.addListener(() {
      if (widget.homeController.indexIsChanging &&
          widget.homeController.index == 2 &&
          widget.tabController.index == widget.index) {
        initRoomList(tabController.index);
      }
    });
    widget.tabController.addListener(() {
      if (widget.tabController.indexIsChanging &&
          widget.tabController.index == widget.index) {
        initRoomList(tabController.index);
      }
    });*/
    tabController.addListener(() {
      if (tabController.indexIsChanging) {
        initRoomList(tabController.index);
      }
    });
    initRoomList(tabController.index);
  }

  Future<void> onRefresh() async {
    initRoomList(tabController.index);
  }

  void initRoomList(int index) async {
    await DioUtils().httpRequest(
      NetConstants.roomPowerList,
      params: {
        "room_flag": index + 1,
        "page_num": 0,
        "page_offset": 20,
      },
      loading: false,
      onSuccess: (res) async {
        if (res == null) {
          setRoomList(index, []);
          count = 0;
        } else {
          RoomList list = RoomList.fromJson(res);
          List<Rooms> listRoom = [];
          listRoom.addAll(list.topRooms ?? []);
          listRoom.addAll(list.rooms ?? []);
          setRoomList(index, listRoom);
          count = list.count;
        }
        setState(() {});
      },
      onError: (code, msg) {
        setRoomList(index, []);
        setState(() {});
      },
    );
  }

  void setRoomList(int index, List<Rooms> list) {
    switch (index) {
      case 2:
        creatRoomList = list;
        break;
      case 1:
        adminRoomList = list;
        break;
      default:
        followRoomList = list;
    }
  }

  @override
  void dispose() {
    /*widget.homeController.dispose();
    widget.tabController.dispose();*/
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return FRefreshLayout(
      firstRefresh: false,
      onRefresh: onRefresh,
      child: Column(
        children: [
          SizedBox(height: 20.w),
          HomeTabListView(tabController, titles),
          tabList(),
          Visibility(
            visible: count == 0,
            child: MyRoomLove(/*widget.tabController, widget.index*/),
          ),
          if(false)
          Visibility(
            visible: count == 0,
            child: EveryoneLook(/*widget.tabController, widget.index*/),
          ),
        ],
      ),
    );
  }

  Widget tabList() {
    switch (tabController.index) {
      case 0:
        return creatRoomList.isNotEmpty
            ? HomeRoomListPiece(
                creatRoomList,
                key: const Key("creatRoomList"),
              )
            : Padding(
                padding: EdgeInsets.only(top: 100.w),
                child: const FEmptyWidget(),
              );
      case 1:
        return adminRoomList.isNotEmpty
            ? HomeRoomListPiece(
                adminRoomList,
                key: const Key("adminRoomList"),
              )
            : Padding(
                padding: EdgeInsets.only(top: 100.w),
                child: const FEmptyWidget(),
              );
      default:
        return followRoomList.isNotEmpty
            ? HomeRoomListPiece(
                followRoomList,
                key: const Key("followRoomList"),
              )
            : Padding(
                padding: EdgeInsets.only(top: 100.w),
                child: const FEmptyWidget(),
              );
    }
  }
}

// 猜你喜欢
class MyRoomLove extends StatefulWidget {
  /*final TabController tabController;
  final int index;*/

  const MyRoomLove(/*this.tabController, this.index,*/ {super.key});

  @override
  State<MyRoomLove> createState() => _MyRoomLoveState();
}

class _MyRoomLoveState extends State<MyRoomLove> {
  List<UserList> loveList = [];

  @override
  void initState() {
    super.initState();
   /* if (!mounted) return;
    widget.tabController.addListener(() {
      if (widget.tabController.index == widget.index) {
        initList();
      }
    });*/
    initList();
  }

  void initList() async {
    await DioUtils().httpRequest(
      NetConstants.expansionColumn,
      params: {
        "page_num": 20,
        "page_offset": 0,
        "recommend_type": 2,
      },
      loading: false,
      onSuccess: (res) async {
        if (res == null) return;
        List<UserList> list = (res as List<dynamic>)
            .map((e) => UserList.fromJson(e as Map<String, dynamic>))
            .toList();
        loveList = list;
        if (!mounted) return;
        setState(() {});
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: loveList.isNotEmpty,
      child: Column(
        children: [
          SizedBox(height: 30.w),
          title("home/homeRoom/love", "猜你喜欢"),
          guessYouLikIt(),
        ],
      ),
    );
  }

  Widget title(String ima, String text) {
    return Row(
      children: [
        LoadAssetImage(ima, width: 50.w),
        SizedBox(width: 10.w),
        FText(
          text,
          size: 30.sp,
          weight: FontWeight.w500,
          color: const Color(0xFFE959A5),
        )
      ],
    );
  }

  Widget guessYouLikIt() {
    return Column(
      children: [
        SizedBox(height: 20.w),
        SizedBox(
          width: double.infinity,
          height: 200.w,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            padding: const EdgeInsets.all(0),
            itemBuilder: (context, index) => guessYouLikItItem(loveList[index]),
            itemCount: loveList.length,
          ),
        )
      ],
    );
  }

  Widget guessYouLikItItem(UserList userInfo) {
    return GestureDetector(
      onTap: () {
        WidgetUtils().pushPage(PersonHomepage(userId: userInfo.userId));
      },
      child: FContainer(
        radius: BorderRadius.circular(20.w),
        margin: EdgeInsets.only(right: 20.w),
        width: 160.w,
        color: Colors.white,
        padding: EdgeInsets.all(10.w),
        child: Stack(
          alignment: AlignmentDirectional.topCenter,
          children: [
            FContainer(
              width: double.infinity,
              height: 128.w,
              radius: BorderRadius.circular(10.w),
              imageFit: BoxFit.cover,
              image: ImageUtils()
                  .getImageProvider(NetConstants.ossPath + userInfo.headIcon),
            ),
            Positioned(
              bottom: 0,
              child: Column(
                children: [
                  Visibility(
                    visible: userInfo.voice.isNotEmpty,
                    child: AudioPlay(userInfo.voice),
                  ),
                  SizedBox(height: 6.w),
                  FText(
                    userInfo.name,
                    size: 21.sp,
                    color: Colors.black,
                    weight: FontWeight.w200,
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class AudioPlay extends StatefulWidget {
  final String source;

  const AudioPlay(this.source, {super.key});

  @override
  State<AudioPlay> createState() => _AudioPlayState();
}

class _AudioPlayState extends State<AudioPlay> {
  bool isPlaying = false;

  @override
  Widget build(BuildContext context) {
    return FButton(
      onTap: tabAudio,
      isTheme: true,
      width: 90.w,
      height: 40.w,
      padding: EdgeInsets.all(5.w),
      radius: BorderRadius.circular(20.w),
      child: Row(
        children: [
          LoadAssetImage("home/homeRoom/${isPlaying ? "play" : "suspend"}"),
          SizedBox(width: 5.w),
          Text.rich(TextSpan(
            style: const TextStyle(
              color: Colors.white,
              letterSpacing: 3,
              fontWeight: FontWeight.w900,
            ),
            children: [
              TextSpan(
                text: '|',
                style: TextStyle(
                  fontSize: 22.sp,
                ),
              ),
              const TextSpan(
                text: '|',
              ),
              TextSpan(
                text: '|',
                style: TextStyle(
                  fontSize: 22.sp,
                ),
              ),
            ],
          ))
        ],
      ),
    );
  }

  void tabAudio() {
    isPlaying ? stopPlayer() : startPlayer();
  }

  void startPlayer() async {
    if (AppManager().soundPlayer.isPlaying) return;
    try {
      if (widget.source.isEmpty) {
        return WidgetUtils().showToast("播放路径错误");
      }
      isPlaying = true;
      setState(() {});
      String source = NetConstants.ossPath + widget.source;
      debugPrint("播放录音地址$source");
      bool state = source.contains('wav');
      await AppManager().soundPlayer.startPlayer(
            fromURI: source,
            codec: !state ? Codec.aacADTS : Codec.pcm16WAV,
            sampleRate: 44100,
          );
      isPlaying = false;
      setState(() {});
    } catch (err) {
      debugPrint("播放录音错误：$err");
      stopPlayer();
    }
  }

  void stopPlayer() async {
    if (!AppManager().soundPlayer.isPlaying) return;
    try {
      await AppManager().soundPlayer.stopPlayer();
      isPlaying = false;
      setState(() {});
    } catch (err) {
      debugPrint("停止播放出错：$err");
    }
  }
}

// 热门推荐
class EveryoneLook extends StatefulWidget {
 /* final TabController tabController;
  final int index;*/

  const EveryoneLook(/*this.tabController, this.index,*/ {super.key});

  @override
  State<EveryoneLook> createState() => _EveryoneLookState();
}

class _EveryoneLookState extends State<EveryoneLook> {
  // 房间列表
  List<Rooms> roomList = [];

  @override
  void initState() {
    super.initState();
    /*if (!mounted) return;
    widget.tabController.addListener(() {
      if (widget.tabController.index == widget.index) {
        initRoomList();
      }
    });*/
    initRoomList();
  }

  void initRoomList() async {
    await DioUtils().httpRequest(
      NetConstants.userLockRoomList,
      params: {"page_num": 50, "page_offset": 0},
      loading: false,
      onSuccess: (res) async {
        if (res == null || res.length == 0) {
          roomList = [];
        } else {
          RoomList list = RoomList.fromJson(res);
          List<Rooms> listRoom = [];
          listRoom.addAll(list.rooms ?? []);
          roomList = listRoom;
        }
        setState(() {});
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: 30.w),
        title("home/homeRoom/television", "大家都在看"),
        SizedBox(height: 20.w),
        roomList.isNotEmpty
            ? HomeRoomListPiece(roomList)
            : Padding(
                padding: EdgeInsets.only(top: 200.w),
                child: const FEmptyWidget(),
              ),
      ],
    );
  }

  Widget title(String ima, String text) {
    return Row(
      children: [
        LoadAssetImage(ima, width: 50.w),
        SizedBox(width: 10.w),
        FText(
          text,
          size: 30.sp,
          weight: FontWeight.w500,
          color: const Color(0xFFE959A5),
        )
      ],
    );
  }
}
