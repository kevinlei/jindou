import 'dart:async';

import 'package:echo/import.dart';
import 'package:echo/pages/home/v2/widget/home_room_chat_widget.dart';
import 'package:flutter/material.dart';

import 'my_room_widget.dart';

class HomeRoomWidget extends StatefulWidget {
  //房间
  final TabController controller;
  final List<RoomTypes> classificationList;

  const HomeRoomWidget(this.controller, this.classificationList, {super.key});

  @override
  State<HomeRoomWidget> createState() => _HomeRoomWidgetState();
}

class _HomeRoomWidgetState extends State<HomeRoomWidget>
    with AutomaticKeepAliveClientMixin, TickerProviderStateMixin {
  late TabController tabController;

  // 标签
  List<RoomTypes> titles = [];

  // 延时操作
  Timer? timer;

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    tabController =
        TabController(length: widget.classificationList.length, vsync: this);
    initRoomType();
  }

  //获取房间标签
  void initRoomType() async {
    await DioUtils().httpRequest(
      NetConstants.getelation,
      loading: false,
      params: {"pid": 30},
      onSuccess: (res) {
        if (res == null) return;
        List<RoomTypes> list = [];
        list.addAll(
          (res as List<dynamic>)
              .map((e) => RoomTypes.fromJson(e as Map<String, dynamic>))
              .toList(),
        );
        setState(() {
          titles = list;
        });
      },
    );
  }

  @override
  void dispose() {
    widget.controller.dispose();
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    List<Widget> childList = [];
    for (var i = 0; i < widget.classificationList.length - 2; i++) {
      if (widget.classificationList[i + 2].topicName.contains("闲聊")) {
        childList
            .add(HomeRoomChatWidget(tabController, i + 2, titles)); //titles只属于闲聊
      } else {
        childList.add(HomeRoomClass(widget.controller, tabController, i + 2,
            widget.classificationList[i + 2].topicId!));
      }
    }

    return GestureDetector(
      onHorizontalDragUpdate: (DragUpdateDetails details) {
        if (timer != null) return;
        if (details.delta.dx > 10 || details.delta.dx < -10) {
          timer = Timer(const Duration(milliseconds: 200), () {
            timer!.cancel();
            timer = null;
          });
        }
        if (details.delta.dx > 10 && tabController.index == 0) {
          widget.controller.animateTo(1);
        } else if (details.delta.dx < -10 &&
            widget.classificationList.length > 1 &&
            tabController.index != (widget.classificationList.length - 1)) {
          tabController.animateTo(tabController.index + 1);
        } else if (details.delta.dx > 10 && tabController.index > 0) {
          tabController.animateTo(tabController.index - 1);
        }
      },
      child: Column(
        children: [
          SizedBox(height: 24.w),
          HomeRoomClassification(tabController, widget.classificationList),
          //tab
          SizedBox(height: 10.w),
          Expanded(
              child: TabBarView(
            physics: const NeverScrollableScrollPhysics(),
            controller: tabController,
            children: [
              // MyRoomWidget(/*widget.controller, tabController, 0*/),
              HomeRoomClass(widget.controller, tabController, 1, 1),
              ...childList,
            ],
          ))
        ],
      ),
    );
  }
}

class MyWidget extends StatefulWidget {
  const MyWidget({super.key});

  @override
  State<MyWidget> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<MyWidget> {
  @override
  Widget build(BuildContext context) {
    return const Placeholder();
  }
}
