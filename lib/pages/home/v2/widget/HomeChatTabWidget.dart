import 'package:echo/import.dart';
import 'package:echo/widgets/my_widgets.dart';
import 'package:echo/widgets/myfont_weight.dart';
import 'package:flutter/material.dart';

class HomeChatTabWidget extends StatefulWidget {
  final TabController tabController;
  final List<RoomTypes> titles;

  const HomeChatTabWidget(this.tabController, this.titles, {super.key});

  @override
  State<HomeChatTabWidget> createState() => _HomeChatTabWidgetState();
}

class _HomeChatTabWidgetState extends State<HomeChatTabWidget> {
  @override
  void initState() {
    super.initState();
    widget.tabController.addListener(() {
      setState(() {});
    });
  }

  @override
  void dispose() {
    widget.tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        mySpaceHeight(30.h),
        SizedBox(
          height: 50.w,
          child: items(),
        ),
        SizedBox(height: 15.w),
      ],
    );
  }

  Widget items() {
    List<Widget> list = [];
    for (var i = 0; i < widget.titles.length; i++) {
      list.add(item(widget.titles[i].topicName, i));
    }
    return ListView(
      scrollDirection: Axis.horizontal,
      children: list,
    );
  }

  Widget item(
    String text,
    int index,
  ) {
    Widget child = Container(
      height: 48.h,
      decoration: myRoundCornerBoxDecoration(
          radius: 100,
          color: widget.tabController.index == index
              ? Color(0xFFFBC5C5)
              : Colors.transparent),
      margin: EdgeInsets.only(right: 46.w),
      padding: EdgeInsets.symmetric(
          horizontal: widget.tabController.index == index ? 24 : 0),
      alignment: Alignment.center,
      child: myCenterText(text,
          fontSize: widget.tabController.index == index ? 24.sp : 24.sp,
          color: widget.tabController.index == index
              ? Color(0xFFFFFFFF)
              : Color(0xFF666666),
          fontWeight: widget.tabController.index == index
              ? MyFontWeight.medium
              : MyFontWeight.normal),
    );

    return GestureDetector(
      onTap: () {
        widget.tabController.animateTo(index);
        setState(() {});
      },
      child: child,
    );
  }
}
