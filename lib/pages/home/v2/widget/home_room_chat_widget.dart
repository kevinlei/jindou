import 'package:echo/import.dart';
import 'package:flutter/material.dart';

import '../../widgets/RoomListWidget.dart';
import 'HomeChatTabWidget.dart';

class HomeRoomChatWidget extends StatefulWidget {//闲聊tab界面
  final TabController tabController;
  final int index;
  final List<RoomTypes> titles;
  const HomeRoomChatWidget(this.tabController, this.index, this.titles, {super.key});

  @override
  State<HomeRoomChatWidget> createState() => _HomeRoomChatWidgetState();
}

class _HomeRoomChatWidgetState extends State<HomeRoomChatWidget>
    with AutomaticKeepAliveClientMixin, TickerProviderStateMixin {
  late TabController tabController;

  // 房间列表
  List<Rooms> roomList = [];
  // 房间总条数
  int coust = 0;
  // 分页
  int page = 0;

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    if (!mounted) return;
    tabController = TabController(length: widget.titles.length, vsync: this);
    widget.tabController.addListener(() {
      if (widget.tabController.indexIsChanging &&
          widget.tabController.index == widget.index) {
        initRoomList(widget.titles[tabController.index].topicId!, false);
      }
    });
    tabController.addListener(() {
      if (tabController.indexIsChanging) {
        page = 0;
        initRoomList(widget.titles[tabController.index].topicId!, false);
      }
    });
    if (widget.titles.isNotEmpty) {
      initRoomList(widget.titles[0].topicId!, false);
    }
  }

  Future<void> callLoad() async {
    page += 1;
    initRoomList(tabController.index, true);
  }

  void initRoomList(int id, bool isLoad) async {
    if (!isLoad) {
      page = 0;
    }
    await DioUtils().httpRequest(
      NetConstants.roomList,
      params: {"page_num": 20, "page_offset": page * 20, "topic_id": id},
      loading: false,
      onSuccess: (res) async {
        if (res == null) {
          roomList = [];
          coust = 0;
        } else {
          RoomList list = RoomList.fromJson(res);
          List<Rooms> listRoom = [];
          listRoom.addAll(list.topRooms ?? []);
          listRoom.addAll(list.rooms ?? []);
          if (isLoad) {
            roomList.addAll(listRoom);
          } else {
            roomList = listRoom;
          }
          coust = list.count;
        }
        setState(() {});
      },
    );
  }

  @override
  void dispose() {
    widget.tabController.dispose();
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return FRefreshLayout(
      firstRefresh: false,
      onLoad: (roomList.isEmpty ||
              roomList.length == coust ||
              roomList.length % 20 != 0)
          ? null
          : callLoad,
      child: Column(
        children: [
          HomeChatTabWidget(tabController, widget.titles),
          roomList.isNotEmpty
              ? RoomListWidget(roomList)
              : Padding(
                  padding: EdgeInsets.only(top: 300.w),
                  child: const FEmptyWidget(),
                ),
        ],
      ),
    );
  }
}
