import 'package:echo/import.dart';
import 'package:echo/pages/home/v2/widget/home_recommend_widget.dart';
import 'package:echo/pages/home/v2/widget/home_room_widget.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  final PageController homePageCtr;

  HomePage(this.homePageCtr, {Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with TickerProviderStateMixin {
  late TabController tabController;

  // 是否开启青少年模式
  bool adolescentMode = false;

  // 标签
  List<RoomTypes> classificationList = [
    // const RoomTypes(topicName: "我的"),
    const RoomTypes(topicName: "推荐"),
  ];

  @override
  void initState() {
    super.initState();
    EventUtils().on(
      EventConstants.E_MQ_ADOMODE,
      EventCall("Home", (v) {
        adolescentMode = UserConstants.userginInfo?.info.teenmode == 1;
        setState(() {});
      }),
    );
    adolescentMode = UserConstants.userginInfo?.info.teenmode == 1;
    tabController = TabController(length: 2, vsync: this, initialIndex: 0);
    initRoomType();
  }

  @override
  void dispose() {
    EventUtils().off(EventConstants.E_MQ_ADOMODE, EventCall("Home", (v) {}));
    tabController.dispose();
    super.dispose();
  }

  //获取房间标签
  void initRoomType() async {
    await DioUtils().httpRequest(
      NetConstants.getelation,
      loading: false,
      params: {"pid": 21},
      onSuccess: (res) {
        if (res == null) return;
        List<RoomTypes> list = [
          // const RoomTypes(topicName: "我的"),
          const RoomTypes(topicName: "推荐"),
        ];
        list.addAll(
          (res as List<dynamic>)
              .map((e) => RoomTypes.fromJson(e as Map<String, dynamic>))
              .toList(),
        );
        setState(() {
          classificationList.clear();
          classificationList.addAll(list); //todo
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return !adolescentMode
        ? FContainer(
            padding: EdgeInsets.only(
                top: ScreenUtil().statusBarHeight,
                left: 30.w,
                right: 30.w,
                bottom: 20.w),
            decoration: BoxDecoration(
              color: Colors.white,
                image: DecorationImage(
                    image: ImageUtils().getAssetsImage("home/bg"),
                    fit: BoxFit.fitWidth,
                    alignment: Alignment.topCenter)),
            /*imageFit: BoxFit.fill,
            imageAlign: Alignment.topCenter,
            image: ImageUtils().getAssetsImage("home/bg"),*/
            child: FContainer(
              color: Colors.transparent,
              child: Column(
                children: [
                  SizedBox(height: 40.w),
                  HomeHerder(tabController),
                  SizedBox(height: 20.w),
                  Expanded(
                      child: TabBarView(
                    controller: tabController,
                    physics: const NeverScrollableScrollPhysics(),
                    children: [
                      // ExpansionColumn(tabController),
                      HomeRecommendWidget(tabController),
                      HomeRoomWidget(tabController, classificationList),
                    ],
                  ))
                ],
              ),
            ),
          )
        : const AdolescentMode();
  }
}
