import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class HomeRoomListStrip extends StatelessWidget {
  // 房间列表
  final List<Rooms> roomList;
  const HomeRoomListStrip(
    this.roomList, {
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      addAutomaticKeepAlives: false,
      addRepaintBoundaries: false,
      padding: const EdgeInsets.all(0),
      itemBuilder: (context, index) => item(roomList[index]),
      itemCount: roomList.length,
    );
  }

  Widget item(Rooms info) {
    Widget child = FContainer(
      margin: EdgeInsets.only(bottom: 20.w),
      padding: EdgeInsets.all(15.w),
      radius: BorderRadius.circular(20.w),
      color: Colors.white,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          FContainer(
            radius: BorderRadius.circular(20.w),
            width: 160.w,
            height: 165.w,
            image: ImageUtils()
                .getImageProvider(NetConstants.ossPath + info.image),
            imageFit: BoxFit.cover,
          ),
          SizedBox(width: 20.w),
          Expanded(
              child: SizedBox(
            height: 165.w,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                FText(
                  info.name,
                  size: 31.sp,
                  color: Colors.black,
                  weight: FontWeight.w600,
                ),
                SizedBox(
                  height: 50.w,
                  child: Row(
                    children: [
                      headerImgList(info.usersImage ?? []),
                      const Spacer(),
                      LoadAssetImage(
                        "home/homeRoom/itemHot",
                        height: 33.w,
                      ),
                      SizedBox(width: 4.w),
                      FContainer(
                        constraints: BoxConstraints(maxWidth: 180.w),
                        child: FText(
                          "${info.hotNum > 10000 ? "${(info.hotNum / 10000).toStringAsFixed(2)}w" : info.hotNum}",
                          size: 32.sp,
                          color: Colors.black,
                          weight: FontWeight.bold,
                          overflow: TextOverflow.ellipsis,
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ))
        ],
      ),
    );
    return GestureDetector(
      onTap: () {
        // WidgetUtils().pushPage(RoomPage(info.id));
        AppManager().joinRoom(info.id);
      },
      child: child,
    );
  }

  Widget headerImgList(List<String> users) {
    List<Widget> list = [];
    for (var i = 0; i < users.length; i++) {
      Widget child = Positioned(
          left: (7 - i - 1) * 40.w,
          child: FContainer(
            width: 50.w,
            height: 50.w,
            radius: BorderRadius.circular(50),
            image: ImageUtils().getImageProvider(
                NetConstants.ossPath + users[users.length - i - 1]),
            imageFit: BoxFit.cover,
          ));
      list.add(child);
    }

    return SizedBox(
      width: list.length * 40.w + 10.w,
      height: 50.w,
      child: Stack(
        children: list,
      ),
    );
  }
}
