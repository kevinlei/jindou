import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class HomeRoomListPiece extends StatefulWidget {//热门房间列表
  // 房间列表
  final List<Rooms> roomList;
  // 下拉刷新
  Future<void> Function()? onLoad;
  HomeRoomListPiece(this.roomList, {super.key, this.onLoad});

  @override
  State<HomeRoomListPiece> createState() => _HomeRoomListPieceState();
}

class _HomeRoomListPieceState extends State<HomeRoomListPiece> {
  final GlobalKey globalKey = GlobalKey();

  // 获取GradView高度
  double height = 0;

  // 房间数组长度
  int length = 0;

  // 单个组件高度
  double childWdgHeight = 0;

  // 是否已经初始化高度
  bool isInitHeight = false;

  @override
  void initState() {
    super.initState();
    length = widget.roomList.length;
    initHeigth(widget.roomList.length);
  }

  @override
  void didUpdateWidget(covariant HomeRoomListPiece oldWidget) {
    if (length != widget.roomList.length) {
      length = widget.roomList.length;
      initHeigth(widget.roomList.length);
    }
    super.didUpdateWidget(oldWidget);
  }

  void initHeigth(int length) {
    Future.delayed(const Duration(milliseconds: 200), () async {
      if (childWdgHeight == 0) {
        childWdgHeight = (globalKey.currentContext?.size?.height ?? 0.0);
      }
      int num = length != 0
          ? length % 2 == 0
          ? length
          : length + 1
          : 0;
      height = num > 0 ? (childWdgHeight * num / 2) + 28.w * (num / 2 - 1) : 0;
      if (!mounted) return;
      isInitHeight = true;
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      height: widget.roomList.isNotEmpty ? height : 500.w,
      child: GridView.builder(
        physics: const NeverScrollableScrollPhysics(), // 禁止滚动
        padding: const EdgeInsets.all(0),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          mainAxisSpacing: 28.w,
          crossAxisSpacing: 28.w,
        ), // 数据数量
        itemCount: widget.roomList.length,
        // 所有数据
        itemBuilder: (_, index) => HomeRoomItem(
          widget.roomList[index],
          true,
          key: index == 0
              ? !isInitHeight
              ? globalKey
              : Key(
              "${widget.roomList[index].name}_${widget.roomList[index].hotNum}")
              : Key(
              "${widget.roomList[index].name}_${widget.roomList[index].hotNum}"),
          widget.roomList[index].isFollow == 1,
        ),
      ),
    );
  }
}
