import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class HomeRoomItem extends StatefulWidget {
  //  房间信息
  final Rooms roomInfo;
  //  卡片式还是列表式
  final bool typesOf;
  // 是否是自己关注的房间
  final bool care;
  const HomeRoomItem(this.roomInfo, this.typesOf, this.care, {super.key});

  @override
  State<HomeRoomItem> createState() => _HomeRoomItemState();
}

class _HomeRoomItemState extends State<HomeRoomItem> {
  // 显示取消关注按钮
  bool unfollowOpen = false;
  // 房间信息
  late Rooms info;

  @override
  void initState() {
    super.initState();
    info = widget.roomInfo;
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      key: Key("${widget.roomInfo.hotNum}_${widget.roomInfo.name}"),
      children: [
        GestureDetector(
          onTap: () {
            // WidgetUtils().pushPage(RoomPage(info.id));
            AppManager().joinRoom(info.id);
          },
          child: widget.typesOf ? room() : chat(),
        ),
        Visibility(
            visible: widget.care && unfollowOpen,
            child: Positioned(top: 42.w, right: -60.w, child: unfollow()))
      ],
    );
  }

  Widget room() {
    return FContainer(
      radius: BorderRadius.circular(20.w),
      image: ImageUtils().getImageProvider(NetConstants.ossPath + info.image),
      imageFit: BoxFit.cover,
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              FContainer(
                width: 82.w,
                radius: BorderRadius.only(
                    topLeft: Radius.circular(20.w),
                    bottomRight: Radius.circular(20.w)),
                height: 34.w,
                image: info.topicPicture.isNotEmpty
                    ? ImageUtils().getImageProvider(
                        NetConstants.ossPath + info.topicPicture)
                    : null,
                imageFit: BoxFit.fill,
              ),
              Visibility(
                  visible: widget.care,
                  child: GestureDetector(
                    onTap: () => setState(() {
                      unfollowOpen = !unfollowOpen;
                    }),
                    child: LoadAssetImage(
                      "public/more",
                      width: 80.w,
                      height: 60.w,
                    ),
                  )),
            ],
          ),
          const Spacer(),
          FContainer(
            padding: EdgeInsets.all(10.w),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                FText(
                  "ID：${info.id}",
                  size: 22.sp,
                  color: Colors.white.withOpacity(0.9),
                ),
                SizedBox(height: 5.w),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    FContainer(
                      constraints: BoxConstraints(maxWidth: 120.w),
                      child: FText(
                        info.name,
                        size: 26.sp,
                        weight: FontWeight.w600,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    const Spacer(),
                    LoadAssetImage(
                      "home/homeRoom/itemHot",
                      height: 33.w,
                    ),
                    SizedBox(width: 3.w),
                    FContainer(
                      constraints: BoxConstraints(maxWidth: 180.w),
                      child: FText(
                        "${info.hotNum > 10000 ? "${(info.hotNum / 10000).toStringAsFixed(2)}w" : info.hotNum}",
                        size: 30.sp,
                        weight: FontWeight.bold,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
          SizedBox(height: 10.w)
        ],
      ),
    );
  }

  Widget chat() {
    return FContainer(
      color: const Color(0xFFDA77F2),
      radius: BorderRadius.circular(20.w),
      padding: EdgeInsets.only(left: 10.w, right: 10.w, bottom: 10.w),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              FContainer(
                width: 80.w,
                padding: EdgeInsets.symmetric(vertical: 7.w, horizontal: 12.w),
                radius: BorderRadius.circular(30.w),
                color: const Color(0xFF000000).withOpacity(0.15),
                align: Alignment.center,
                child: FText(info.topicName),
              ),
              Visibility(
                  visible: widget.care,
                  child: GestureDetector(
                    onTap: () => setState(() {
                      unfollowOpen = !unfollowOpen;
                    }),
                    child: LoadAssetImage(
                      "public/more",
                      width: 80.w,
                      height: 60.w,
                    ),
                  )),
            ],
          ),
          SizedBox(height: 20.w),
          FContainer(
            width: double.infinity,
            child:
                FText(info.name, size: 28.sp, overflow: TextOverflow.ellipsis),
          ),
          SizedBox(height: 13.w),
          Row(
            children: [
              LoadAssetImage(
                "home/homeRoom/love",
                width: 40.w,
              ),
              SizedBox(width: 8.w),
              FText(
                  "${info.hotNum > 10000 ? "${info.hotNum.toStringAsFixed(2)}w" : info.hotNum}")
            ],
          ),
          const Spacer(),
          Row(
            children: [
              SizedBox(
                width: 60.w,
                child: CircleAvatar(
                  backgroundImage:
                      ImageUtils().getAssetsImage("home/homeRoom/love"),
                ),
              ),
              SizedBox(width: 8.w),
              FText(
                "CH-秦淮男友",
                size: 22.sp,
              )
            ],
          )
        ],
      ),
    );
  }

  Widget unfollow() {
    return GestureDetector(
      onTap: () => roomCollect(),
      child: FContainer(
        width: 330.w,
        height: 110.w,
        padding: EdgeInsets.only(bottom: 10.w, left: 10.w),
        image: ImageUtils().getAssetsImage("home/homeRoom/unfollow"),
        align: Alignment.center,
        child: FText(
          "取消关注",
          color: Colors.black,
          size: 26.sp,
        ),
      ),
    );
  }

  // 取消关注房间
  void roomCollect() async {
    await DioUtils().httpRequest(
      NetConstants.collectRoom,
      method: DioMethod.POST,
      params: {
        "exterior_room_id": info.id,
        "follow_type": 2,
      },
      loading: false,
      onSuccess: (data) {
        setState(() {});
      },
    );
  }
}
