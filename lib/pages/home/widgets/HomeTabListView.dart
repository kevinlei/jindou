import 'package:echo/import.dart';
import 'package:echo/widgets/my_widgets.dart';
import 'package:echo/widgets/myfont_weight.dart';
import 'package:flutter/material.dart';

class HomeTabListView extends StatefulWidget {
  final TabController tabController;
  final List<RoomTypes> titles;
  const HomeTabListView(this.tabController, this.titles, {super.key});

  @override
  State<HomeTabListView> createState() => _HomeTabListViewState();
}

class _HomeTabListViewState extends State<HomeTabListView> {
  @override
  void initState() {
    super.initState();
    widget.tabController.addListener(() {
      if (!mounted) return;
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        SizedBox(
          height: 50.w,
          child: items(),
        ),
        SizedBox(height: 15.w),
      ],
    );
  }

  Widget items() {
    List<Widget> list = [];
    for (var i = 0; i < widget.titles.length; i++) {
      list.add(item(widget.titles[i].topicName, i));
    }
    return ListView(
      scrollDirection: Axis.horizontal,
      children: list,
    );
  }

  Widget item(String text, int index) {
    Widget child = FContainer(
      radius: BorderRadius.circular(40.w),
      padding: EdgeInsets.symmetric(horizontal: 16.w),
      margin: EdgeInsets.symmetric(horizontal: 5.w),
      align: Alignment.center,
      color: widget.tabController.index == index
          ? Colors.white.withOpacity(0.2)
          : null,
      child: myCenterText(
          text,
          fontSize:widget.tabController.index == index? 32.sp:28.sp,
          color: widget.tabController.index == index
              ? Color(0xFF222222)
              : Color(0xFF222222),
          fontWeight: widget.tabController.index == index?MyFontWeight.medium:MyFontWeight.normal
      ),
    );

    return GestureDetector(
      onTap: () {
        widget.tabController.animateTo(index);
        setState(() {});
      },
      child: child,
    );
  }
}
