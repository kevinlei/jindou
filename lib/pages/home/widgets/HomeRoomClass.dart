import 'package:echo/import.dart';
import 'package:flutter/material.dart';

import 'RoomListWidget.dart';

class HomeRoomClass extends StatefulWidget {//房间tab下界面
  final TabController homeController;
  final TabController tabController;
  final int index;
  final int topicId;
  const HomeRoomClass(
      this.homeController, this.tabController, this.index, this.topicId,
      {super.key});

  @override
  State<HomeRoomClass> createState() => _HomeRoomClassState();
}

class _HomeRoomClassState extends State<HomeRoomClass>
    with AutomaticKeepAliveClientMixin {
  // 房间列表
  List<Rooms> roomList = [];
  // 房间总条数
  int coust = 0;
  // 分页
  int page = 0;

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    if (!mounted) return;
    widget.homeController.addListener(() {
      if (widget.homeController.indexIsChanging &&
          widget.homeController.index == 2 &&
          widget.tabController.index == widget.index) {
        initRoomList(false);
      }
    });
    widget.tabController.addListener(() {
      if (widget.tabController.indexIsChanging &&
          widget.tabController.index == widget.index) {
        initRoomList(false);
      }
    });
    initRoomList(false);
  }

  Future<void> onRefresh() async {
    initRoomList(false);
  }

  Future<void> callLoad() async {
    page += 1;
    initRoomList(true);
  }

  void initRoomList(bool isLoad) async {
    if (!isLoad) {
      page = 0;
    }
    await DioUtils().httpRequest(
      widget.index == 1 ? NetConstants.hotRoomList : NetConstants.roomList,
      params: {
        "page_num": 20,
        "page_offset": page * 20,
        (widget.index == 1 ? "user_id" : "topic_id"): (widget.index == 1
            ? UserConstants.userginInfo!.info.userId
            : widget.topicId),
      },
      loading: false,
      onSuccess: (res) async {
        if (res == null) {
          coust = 0;
          roomList = [];
        } else {
          RoomList list = RoomList.fromJson(res);
          List<Rooms> listRoom = [];
          listRoom.addAll(list.topRooms ?? []);
          listRoom.addAll(list.rooms ?? []);
          if (isLoad) {
            roomList.addAll(listRoom);
          } else {
            roomList = listRoom;
          }
          coust = list.count;
        }
        setState(() {});
      },
    );
  }

  @override
  void dispose() {
    widget.homeController.dispose();
    widget.tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return FRefreshLayout(
      firstRefresh: false,
      onRefresh: onRefresh,
      onLoad: (roomList.isEmpty ||
          roomList.length == coust ||
          roomList.length % 20 != 0)
          ? null
          : callLoad,
      child: Padding(
        padding: EdgeInsets.only(top: 20.w),
        child: roomList.isNotEmpty
        // ? widget.index > 1
        //     ? HomeRoomListStrip(roomList)
        //     : HomeRoomListPiece(roomList)
            ? RoomListWidget(roomList)
            : Padding(
          padding: EdgeInsets.only(top: 300.w),
          child: const FEmptyWidget(),
        ),
      ),
    );
  }
}
