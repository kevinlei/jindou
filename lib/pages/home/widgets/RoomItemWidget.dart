import 'package:echo/import.dart';
import 'package:echo/utils/ext/string_ext.dart';
import 'package:echo/widgets/my_widgets.dart';
import 'package:echo/widgets/myfont_weight.dart';
import 'package:flutter/material.dart';

class RoomItemWidget extends StatefulWidget {
  //  房间信息
  final Rooms roomInfo;

  //  卡片式还是列表式
  final bool typesOf;

  // 是否是自己关注的房间
  final bool care;

  final bool isSmallItem;

  const RoomItemWidget(this.roomInfo, this.typesOf, this.care,
      {super.key, this.isSmallItem = false});

  @override
  State<RoomItemWidget> createState() => _RoomItemWidgetState();
}

class _RoomItemWidgetState extends State<RoomItemWidget> {
  // 显示取消关注按钮
  bool unfollowOpen = false;

  // 房间信息
  late Rooms info;

  @override
  void initState() {
    super.initState();
    info = widget.roomInfo;
  }

  @override
  Widget build(BuildContext context) {
    return myGestureDetector(
        child: itemRoom(),
        onTap: () {
          AppManager().joinRoom(info.id);
        });
  }

  Widget topic() {
    if (info.topicPicture.isNotEmpty) {
      return FContainer(
        width: 144.w,
        radius: BorderRadius.only(
            topLeft: Radius.circular(20.w), bottomRight: Radius.circular(20.w)),
        height: 38.w,
        image: ImageUtils()
            .getImageProvider(NetConstants.ossPath + info.topicPicture),
        imageFit: BoxFit.fill,
      );
    }
    return myEmptyWidget();
  }

  Widget hot() {
    if (null == info.hotNum) {
      return myEmptyWidget();
    }

    return myRow(
        children: [
          mySizeIcon("home/homeRoom/itemHot", size: 24.w),
          mySpaceWidth(4.w),
          myLeftText(
              "${info.hotNum > 10000 ? "${(info.hotNum / 10000).toStringAsFixed(2)}w" : info.hotNum}",
              color: Colors.white,
              fontSize: 22.sp)
        ],
        mainAxisSize: MainAxisSize.min,
        decoration: myRoundCornerBoxDecoration(
            radius: 100, color: '#FF1A1A1A'.toColor().withOpacity(0.2)),
        padding:
            EdgeInsets.only(left: 20.w, right: 20.w, top: 8.w, bottom: 8.w));
  }

  Widget idAndName() {
    return myColumn(children: [
      myLeftText("ID：${info.id ?? '--'}",
          fontSize: 24.sp,
          color: Colors.white,
          maxLines: 1,
          overflow: TextOverflow.ellipsis),
      mySpaceHeight(8.w),
      myLeftText(info.name ?? '--',
          fontSize: 28.sp,
          color: Colors.white,
          maxLines: 1,
          fontWeight: MyFontWeight.medium,
          overflow: TextOverflow.ellipsis),
    ], mainAxisSize: MainAxisSize.min);
  }

  Widget itemRoom() {
    return FContainer(
      radius: BorderRadius.circular(20.w),
      image: ImageUtils().getImageProvider(NetConstants.ossPath + info.image),
      imageFit: BoxFit.cover,
      child: widget.isSmallItem
          ? myColumn(
              children: [
                  myColumn(children: [
                    topic(),
                    mySpaceHeight(8.w),
                    hot(),
                  ], mainAxisSize: MainAxisSize.min),
                  idAndName(),
                ],
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              padding: EdgeInsets.all(16.w))
          : myColumn(
              children: [
                  myRow(children: [
                    topic(),
                    hot(),
                  ], mainAxisAlignment: MainAxisAlignment.spaceBetween),
                  idAndName(),
                ],
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              padding: EdgeInsets.all(16.w)),
    );
  }

  Widget chat() {
    return FContainer(
      color: const Color(0xFFDA77F2),
      radius: BorderRadius.circular(20.w),
      padding: EdgeInsets.only(left: 10.w, right: 10.w, bottom: 10.w),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              FContainer(
                width: 80.w,
                padding: EdgeInsets.symmetric(vertical: 7.w, horizontal: 12.w),
                radius: BorderRadius.circular(30.w),
                color: const Color(0xFF000000).withOpacity(0.15),
                align: Alignment.center,
                child: FText(info.topicName),
              ),
              Visibility(
                  visible: widget.care,
                  child: GestureDetector(
                    onTap: () => setState(() {
                      unfollowOpen = !unfollowOpen;
                    }),
                    child: LoadAssetImage(
                      "public/more",
                      width: 80.w,
                      height: 60.w,
                    ),
                  )),
            ],
          ),
          SizedBox(height: 20.w),
          FContainer(
            width: double.infinity,
            child:
                FText(info.name, size: 28.sp, overflow: TextOverflow.ellipsis),
          ),
          SizedBox(height: 13.w),
          Row(
            children: [
              LoadAssetImage(
                "home/homeRoom/love",
                width: 40.w,
              ),
              SizedBox(width: 8.w),
              FText(
                  "${info.hotNum > 10000 ? "${info.hotNum.toStringAsFixed(2)}w" : info.hotNum}")
            ],
          ),
          const Spacer(),
          Row(
            children: [
              SizedBox(
                width: 60.w,
                child: CircleAvatar(
                  backgroundImage:
                      ImageUtils().getAssetsImage("home/homeRoom/love"),
                ),
              ),
              SizedBox(width: 8.w),
              FText(
                "CH-秦淮男友",
                size: 22.sp,
              )
            ],
          )
        ],
      ),
    );
  }

  Widget unfollow() {
    return GestureDetector(
      onTap: () => roomCollect(),
      child: FContainer(
        width: 330.w,
        height: 110.w,
        padding: EdgeInsets.only(bottom: 10.w, left: 10.w),
        image: ImageUtils().getAssetsImage("home/homeRoom/unfollow"),
        align: Alignment.center,
        child: FText(
          "取消关注",
          color: Colors.black,
          size: 26.sp,
        ),
      ),
    );
  }

  // 取消关注房间
  void roomCollect() async {
    await DioUtils().httpRequest(
      NetConstants.collectRoom,
      method: DioMethod.POST,
      params: {
        "exterior_room_id": info.id,
        "follow_type": 2,
      },
      loading: false,
      onSuccess: (data) {
        setState(() {});
      },
    );
  }
}
