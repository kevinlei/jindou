import 'package:echo/import.dart';
import 'package:flutter/material.dart';

import '../../person/romhome/MyRoomPage.dart';

class HomeHerder extends StatefulWidget {
  final TabController controller;

  const HomeHerder(this.controller, {super.key});

  @override
  State<HomeHerder> createState() => _HomeHerderState();
}

class _HomeHerderState extends State<HomeHerder> {
  // 索引
  int page = 0;

  @override
  void initState() {
    super.initState();
    if (!mounted) return;
    widget.controller.addListener(() {
      if (widget.controller.index != page) {
        setState(() {
          page = widget.controller.index;
        });
      }
    });
  }

  @override
  void dispose() {
    widget.controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          children: [
            /* homeHerderItem("扩列", 0),
            SizedBox(width: 45.w),*/
            homeHerderItem("推荐", 0),
            SizedBox(width: 48.w),
            homeHerderItem("房间", 1),
          ],
        ),
        Row(
          children: [
            GestureDetector(
              onTap: () => WidgetUtils().pushPage(const searchPage()),
              child: LoadAssetImage("home/search", width: 48.w),
            ),
            SizedBox(width: 40.w),
            GestureDetector(
              onTap: () => WidgetUtils().pushPage(const TheCharts()),
              child: LoadAssetImage("home/ranking", width: 48.w),
            ),
            SizedBox(width: 40.w),
            GestureDetector(
              onTap: () {
                WidgetUtils().pushPage(
                  MyRoomPage(),
                );
              },
              child: LoadAssetImage("home/myroom", width: 48.w),
            )
          ],
        )
      ],
    );
  }

  Widget homeHerderItem(String text, int index) {
    Widget child = FContainer(
      color: Colors.transparent,
      width: page == index ? 110.w : null,
      child: Stack(
        alignment: AlignmentDirectional.topEnd,
        children: [
          Visibility(
              visible: page == index,
              child: Positioned(
                  top: 8.h,
                  right: 0,
                  child: LoadAssetImage(
                    "public/pageViewBot",
                    width: 36.w,
                  ))),
          Container(
            alignment: Alignment.center,
            height: page == index ? 72.h : null,
            child: FText(
              text,
              color: Color(page == index ? 0xFF1A1A1A : 0xFF333333),
              size: page == index ? 44.sp : 36.sp,
              weight: page == index ? FontWeight.bold : FontWeight.normal,
            ),
          ),
        ],
      ),
    );

    return GestureDetector(
      onTap: () {
        widget.controller.animateTo(index);
        setState(() {
          page = index;
        });
      },
      child: child,
    );
  }
}
