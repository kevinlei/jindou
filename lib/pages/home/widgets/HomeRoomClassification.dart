import 'package:echo/import.dart';
import 'package:echo/widgets/my_widgets.dart';
import 'package:flutter/material.dart';

class HomeRoomClassification extends StatefulWidget {
  final TabController tabController;
  final List<RoomTypes> classificationList;

  const HomeRoomClassification(this.tabController, this.classificationList,
      {super.key});

  @override
  State<HomeRoomClassification> createState() => _HomeRoomClassificationState();
}

class _HomeRoomClassificationState extends State<HomeRoomClassification> {
  // 索引
  int page = 0;

  @override
  void initState() {
    super.initState();
    if (!mounted) return;
    widget.tabController.addListener(() {
      if (widget.tabController.index != page) {
        setState(() {
          page = widget.tabController.index;
        });
      }
    });
  }

  @override
  void dispose() {
    widget.tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      height: 60.w,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        padding: const EdgeInsets.all(0),
        itemBuilder: (context, index) =>
            item(widget.classificationList[index].topicName, index),
        itemCount: widget.classificationList.length,
      ),
    );
  }

  Widget item(String text, int index) {
    // 当前选择项
    int selectIndex = page;
    Widget child = Container(
      height: 55.h,
      decoration: myRoundCornerBoxDecoration(
        radius: 28,
        color: selectIndex == index
            ? Color(0xFFFF753F).withOpacity(0.2)
            : Colors.white,
      ),
      padding: EdgeInsets.symmetric(/*vertical: 12.w,*/ horizontal: 32.w),
      margin: EdgeInsets.only(right: 24.w),
      alignment: Alignment.center,
      child: myCenterText(
        text,
        fontSize: 28.sp,
        color: selectIndex == index
            ? const Color(0xFFFF753F)
            : const Color(0xFF646566),
      ),
    );

    return GestureDetector(
      onTap: () {
        widget.tabController.animateTo(index);
        setState(() {
          page = index;
        });
      },
      child: child,
    );
  }
}
