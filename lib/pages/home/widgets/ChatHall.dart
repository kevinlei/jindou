import 'dart:convert';

import 'package:echo/import.dart';
import 'package:flutter/material.dart';
import 'package:im_flutter_sdk/im_flutter_sdk.dart';

class ChatHall extends StatefulWidget {
  const ChatHall({super.key});

  @override
  State<ChatHall> createState() => _ChatHallState();
}

// 聊天大厅
class _ChatHallState extends State<ChatHall> {
  ScrollController scroll = ScrollController();
  List<EMMessage> worldChatList = [];

  @override
  void initState() {
    super.initState();
    EventUtils().on(
        EventConstants.E_ROOM_MSG_UPDATE,
        EventCall("ChatHall", (v) {
          worldChatList = AppManager().roomInfo.wordRoomMsg;
          setState(() {});
          scrollToEnd();
        }));
    worldChatList = AppManager().roomInfo.wordRoomMsg;
  }

  @override
  void dispose() {
    EventUtils()
        .off(EventConstants.E_ROOM_MSG_UPDATE, EventCall("ChatHall", (v) {}));
    scroll.dispose();
    super.dispose();
  }

  void scrollToEnd() async {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      if (scroll.hasClients) {
        double max = scroll.position.maxScrollExtent;
        scroll.animateTo(max,
            duration: const Duration(milliseconds: 200), curve: Curves.linear);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget child = FContainer(
      padding: EdgeInsets.only(top: 6.w, left: 170.w, right: 40.w),
      height: 180.w,
      image: ImageUtils().getAssetsImage("home/recommend/chatHallBg"),
      imageFit: BoxFit.fill,
      child: worldChatList.isNotEmpty
          ? ListView.builder(
              padding: const EdgeInsets.all(0),
              controller: scroll,
              itemExtent: 49.w,
              itemBuilder: (_, index) {
                EMTextMessageBody msg =
                    worldChatList[index].body as EMTextMessageBody;
                var data = json.decode(msg.content);
                ExtUser user = ExtUser.fromJson(data["info"]);
                return FContainer(
                  padding: EdgeInsets.only(bottom: 10.w),
                  child: FContainer(
                    child: Row(
                      children: [
                        ClipOval(
                          child: LoadAssetImage(
                            "home/recommend/love",
                            width: 40.w,
                            height: 40.w,
                          ),
                        ),
                        SizedBox(width: 5.w),
                        Expanded(
                            child: FText(
                          "${user.name}：${data["content"]}",
                          size: 21.sp,
                          overflow: TextOverflow.ellipsis,
                          color: Colors.black,
                          weight: FontWeight.w500,
                        ))
                      ],
                    ),
                  ),
                );
              },
              itemCount: worldChatList.length,
            )
          : Center(
              child: ShaderMask(
                shaderCallback: (Rect bounds) {
                  return const LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      colors: [
                        Color(0xFF46F5F6),
                        Color(0xFFFFA7FB),
                        Color(0xFFFF82B2)
                      ],
                      stops: [
                        0,
                        0.4,
                        1
                      ]).createShader(Offset.zero & bounds.size);
                },
                child: FText(
                  "暂无消息，快来抢头条吧!",
                  size: 28.sp,
                  weight: FontWeight.w500,
                ),
              ),
            ),
    );

    return GestureDetector(
      onTap: () {
        WidgetUtils().pushPage(const MsgChatHall());
      },
      child: child,
    );
  }
}
