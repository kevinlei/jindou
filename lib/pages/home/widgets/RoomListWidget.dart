import 'package:echo/import.dart';
import 'package:echo/utils/list_util.dart';
import 'package:echo/widgets/my_widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import 'RoomItemWidget.dart';

class RoomListWidget extends StatefulWidget {
  //热门房间列表
  // 房间列表
  final List<Rooms> roomList;

  // 下拉刷新
  Future<void> Function()? onLoad;

  bool hotRoomStyle;

  RoomListWidget(this.roomList,
      {super.key, this.onLoad, this.hotRoomStyle = false});

  @override
  State<RoomListWidget> createState() => _RoomListWidgetState();
}

class _RoomListWidgetState extends State<RoomListWidget> {
  final GlobalKey globalKey = GlobalKey();

  // 获取GradView高度
  double height = 0;

  // 房间数组长度
  int length = 0;

  // 单个组件高度
  double childWdgHeight = 0;

  // 是否已经初始化高度
  bool isInitHeight = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didUpdateWidget(covariant RoomListWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    if (ListUtil.isListHasData(widget.roomList)) {
     /* widget.roomList[0].owner!.ownerName!='在这里了';
      widget.roomList[0].name!='在这里了';
      widget.roomList.add(widget.roomList[0]);*/

      int index = 0;
      List<Widget> listWidget = widget.roomList.map((e) {
        int crossAxisCellCount = 3;
        int mainAxisCellCount = 3;
        bool isSmallItem = false;
        if (widget.hotRoomStyle) {
          if (0 == index) {
            crossAxisCellCount = 4;
            mainAxisCellCount = 4;
          } else if (1 == index || 2 == index) {
            isSmallItem = true;
            crossAxisCellCount = 2;
            mainAxisCellCount = 2;
          }
        }
        Widget itemWidget = StaggeredGridTile.count(
          crossAxisCellCount: crossAxisCellCount,
          mainAxisCellCount: mainAxisCellCount,
          child: RoomItemWidget(
            widget.roomList[index],
            true,
            widget.roomList[index].isFollow == 1,
            isSmallItem: isSmallItem,
          ),
        );
        index++;

        return itemWidget;
      }).toList();
      return StaggeredGrid.count(
        crossAxisCount: 6,
        mainAxisSpacing: 16.w,
        crossAxisSpacing: 16.w,
        children: listWidget,
      );
    }

    return myEmptyWidget();
  }
}
