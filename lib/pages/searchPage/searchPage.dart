import 'package:echo/import.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../home/v2/widget/my_room_widget.dart';
import '../person/homepage/PersonHomepage.dart';

class searchPage extends StatefulWidget {
  const searchPage({super.key});

  @override
  State<searchPage> createState() => _searchPageState();
}

class _searchPageState extends State<searchPage> with TickerProviderStateMixin {
  TextEditingController textController = TextEditingController();
  late TabController tabController;
  // 是否已搜索
  bool isSearch = false;
  // 本地搜索记录
  List<String> searchRecordList = [];
  // 搜索列表
  SearchList searchList = const SearchList(roomList: [], userList: []);

  @override
  void initState() {
    super.initState();
    tabController = TabController(length: 3, vsync: this);
    initSearchRecordList();
  }

  @override
  void deactivate() {
    textController.dispose();
    tabController.dispose();
    super.deactivate();
  }

  void initSearchRecordList() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    searchRecordList =
        preferences.getStringList(SharedKey.KEY_SEARCH_RECORD.value) ?? [];
    setState(() {});
  }

  // 搜索
  void getSearchList() async {
    if (textController.text.trim().isEmpty) {
      return WidgetUtils().showToast("请输入搜索内容");
    }
    await DioUtils().httpRequest(
      NetConstants.search,
      loading: false,
      method: DioMethod.POST,
      params: {"search_word": textController.text.trim(), "search_type": 0},
      onSuccess: (res) async {
        addText(textController.text.trim());
        SystemChannels.textInput.invokeMethod('TextInput.hide');
        if (res == null) return;
        setState(() {
          isSearch = true;
          searchList = SearchList.fromJson(res);
        });
      },
    );
  }

  // 新增搜索记录
  void addText(String text) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    for (var i = 0; i < searchRecordList.length; i++) {
      if (text == searchRecordList[i]) {
        searchRecordList.removeAt(i);
      }
    }
    if (searchRecordList.length >= 15) {
      searchRecordList.removeAt(0);
    }
    searchRecordList.insert(0, text);
    preferences.setStringList(
        SharedKey.KEY_SEARCH_RECORD.value, searchRecordList);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      padding: EdgeInsets.only(
        top: ScreenUtil().statusBarHeight + 10.w,
        left: 30.w,
        right: 30.w,
      ),
      image: ImageUtils().getAssetsImage("public/huibg"),
      imageFit: BoxFit.fill,
      child: FContainer(
        color: Colors.transparent,
        child: isSearch
            ? Column(
                children: [
                  searchHeader(),
                  searchUSerRoom(),
                ],
              )
            : ListView(
                padding: const EdgeInsets.all(0),
                children: [searchHeader(), recommend()],
              ),
      ),
    );
  }

  Widget searchHeader() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        IconButton(
          icon: const Icon(
            Icons.arrow_back_ios,
            color: Colors.black,
          ),
          onPressed: () {
            SystemChannels.textInput.invokeMethod('TextInput.hide');
            WidgetUtils().popPage();
          },
        ),
        Expanded(
          child: FContainer(
            padding: EdgeInsets.symmetric(horizontal: 20.w),
            radius: BorderRadius.circular(50),
            color: Colors.white,
            child: RawKeyboardListener(
              // 输入框
              focusNode: FocusNode(),
              onKey: (event) {
                if (event.runtimeType == RawKeyDownEvent) {
                  if (event.logicalKey.keyId == 4294967309) {
                    getSearchList();
                  }
                }
              },
              child: TextField(
                onSubmitted: (v) => getSearchList(),
                style: TextStyle(color: Colors.black, fontSize: 32.sp),
                maxLines: 1,
                textInputAction: TextInputAction.done,
                controller: textController,
                decoration: InputDecoration(
                  isDense: true,
                  contentPadding:
                      EdgeInsets.symmetric(vertical: 20.w, horizontal: 12.w),
                  border: InputBorder.none,
                  hintText: "请输入昵称/ID/房间",
                  hintStyle: TextStyle(
                      color: const Color(0xFF222222).withOpacity(0.4),
                      fontSize: 32.sp),
                  counterText: "",
                ),
              ),
            ),
          ),
        ),
        GestureDetector(
          onTap: getSearchList,
          child: FContainer(
            color: Colors.transparent,
            padding: EdgeInsets.only(left: 30.w),
            child: FText(
              "搜索",
              size: 32.sp,
              color: Colors.black,
            ),
          ),
        )
      ],
    );
  }

  // 推荐
  Widget recommend() {
    return Column(
      children: [
        history(),
        MyRoomLove(),
        if(false)
        EveryoneLook(),
      ],
    );
  }

  // 历史记录
  Widget history() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        searchTitle(),
        historyContent(),
        SizedBox(height: 40.w),
      ],
    );
  }

  Widget searchTitle() {
    return Row(children: [
      SizedBox(width: 15.w),
      FText("历史记录", size: 32.sp, color: Colors.black, weight: FontWeight.bold),
      const Spacer(),
      IconButton(
        onPressed: clearSearch,
        icon: LoadAssetImage("search/del", width: 32.w),
      )
    ]);
  }

  Widget historyContent() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 25.w),
      child: Wrap(
        children: List.generate(searchRecordList.length,
            (index) => historyCell(searchRecordList[index])),
      ),
    );
  }

  Widget historyCell(String text) {
    Widget btn = Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        FContainer(
          color: Colors.white.withOpacity(0.1),
          align: Alignment.center,
          radius: BorderRadius.circular(30.w),
          padding: EdgeInsets.symmetric(vertical: 8.w, horizontal: 15.w),
          margin: EdgeInsets.all(10.w),
          border: Border.all(color: const Color(0xFFFF753F)),
          child: FText(text,
              size: 24.sp,
              color: const Color(0xFFFF753F),
              align: TextAlign.center,
              overflow: TextOverflow.ellipsis),
        )
      ],
    );

    return GestureDetector(
      onTap: () {
        textController.text = text;
        setState(() {});
        getSearchList();
      },
      child: btn,
    );
  }

  // 搜索页面
  Widget searchUSerRoom() {
    return Expanded(
        child: Column(
      children: [
        SizedBox(height: 10.w),
        Row(
          children: [
            searchTitleItem("综合", 0),
            searchTitleItem("用户", 1),
            searchTitleItem("房间", 2),
          ],
        ),
        Expanded(
            child: TabBarView(
          controller: tabController,
          physics: const NeverScrollableScrollPhysics(),
          children: [
            allList(),
            userList(searchList.userList),
            searchList.roomList.isEmpty
                ? FContainer(
                    padding: EdgeInsets.symmetric(vertical: 100.w),
                    child: const FEmptyWidget(text: "未找到记录"),
                  )
                : HomeRoomListPiece(searchList.roomList),
          ],
        )),
        Container(),
      ],
    ));
  }

  Widget searchTitleItem(String text, int index) {
    Widget child = FContainer(
      margin: EdgeInsets.symmetric(horizontal: 15.w),
      padding: EdgeInsets.symmetric(vertical: 8.w, horizontal: 16.w),
      child: Column(
        children: [
          FText(
            text,
            color: tabController.index == index
                ? Colors.black
                : const Color(0xFF222222).withOpacity(0.8),
            size: tabController.index == index ? 32.sp : 28.sp,
            weight: tabController.index == index ? FontWeight.bold : null,
          ),
          SizedBox(height: 10.w),
          FContainer(
            width: 35.w,
            height: 5.w,
            radius: BorderRadius.circular(50),
            color: tabController.index == index
                ? const Color(0xFFFF753F)
                : Colors.transparent,
          )
        ],
      ),
    );
    return GestureDetector(
      child: child,
      onTap: () {
        tabController.animateTo(index);
        setState(() {});
      },
    );
  }

  // 综合
  Widget allList() {
    int userLength =
        searchList.userList.length >= 3 ? 3 : searchList.userList.length;
    List<Widget> users = [];
    for (var i = 0; i < userLength; i++) {
      users.add(userItem(searchList.userList[i]));
    }

    return (userLength == 0 && searchList.roomList.isEmpty)
        ? const FEmptyWidget(text: "未找到记录")
        : Padding(
            padding: EdgeInsets.symmetric(horizontal: 20.w),
            child: Column(
              children: [
                SizedBox(height: 20.w),
                allListChild("相关用户"),
                SizedBox(height: 10.w),
                userLength == 0
                    ? FContainer(
                        padding: EdgeInsets.symmetric(vertical: 100.w),
                        child: const FEmptyWidget(text: "未找到记录"),
                      )
                    : Column(
                        children: users,
                      ),
                SizedBox(height: 20.w),
                allListChild("相关房间"),
                SizedBox(height: 10.w),
                searchList.roomList.isEmpty
                    ? FContainer(
                        padding: EdgeInsets.symmetric(vertical: 100.w),
                        child: const FEmptyWidget(text: "未找到记录"),
                      )
                    : HomeRoomListPiece(searchList.roomList),
              ],
            ),
          );
  }

  Widget allListChild(String text) {
    return Row(
      children: [
        FText(
          text,
          size: 28.sp,
          color: Colors.black,
          weight: FontWeight.w500,
        ),
        const Spacer(),
        FText(
          "更多",
          size: 22.sp,
          color: const Color(0xFF545F63),
        ),
        LoadAssetImage("public/RightNavigation", width: 15.w),
      ],
    );
  }

  // 用户列表
  Widget userList(List<UserList> list) {
    return list.length != 0
        ? ListView.builder(
            padding: const EdgeInsets.all(0),
            itemBuilder: (context, index) => userItem(list[index]),
            itemCount: list.length,
          )
        : const FEmptyWidget(text: "未找到记录");
  }

  Widget userItem(UserList userinfo) {
    Widget child = FContainer(
      padding: EdgeInsets.all(20.w),
      margin: EdgeInsets.only(bottom: 20.w),
      radius: BorderRadius.circular(30.w),
      color: Colors.white,
      child: Column(
        children: [
          Row(
            children: [
              FContainer(
                width: 130.w,
                height: 130.w,
                child: Stack(
                  children: [
                    FContainer(
                      radius: BorderRadius.circular(30.w),
                      imageFit: BoxFit.cover,
                      image: ImageUtils().getImageProvider(
                          NetConstants.ossPath + userinfo.headIcon),
                    ),
                  ],
                ),
              ),
              SizedBox(width: 10.w),
              FContainer(
                constraints: BoxConstraints(maxWidth: 320.w),
                height: 130.w,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        SizedBox(
                          width: 260.w,
                          child: FText(
                            userinfo.name.toString(),
                            color: Colors.black,
                            size: 30.sp,
                            weight: FontWeight.w500,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                        SizedBox(width: 5.w),
                        GenderAge(userinfo.sex == 1, userinfo.birthday)
                      ],
                    ),
                    FText(
                      userinfo.sign,
                      size: 22.sp,
                      color: const Color(0xFFA7A7A7),
                      overflow: TextOverflow.ellipsis,
                    )
                  ],
                ),
              ),
              const Spacer(),
              SizedBox(
                height: 130.w,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    currentState(),
                    SizedBox(height: 10.w),
                    privateChat(userinfo),
                  ],
                ),
              )
            ],
          ),
        ],
      ),
    );

    return GestureDetector(
      onTap: () {
        WidgetUtils().pushPage(PersonHomepage(userId: userinfo.userId));
      },
      child: child,
    );
  }

  Widget currentState() {
    Color clor = const Color(0xFFFF753F);
    String text = "在线中";
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        FContainer(
          radius: BorderRadius.circular(50),
          width: 10.w,
          height: 10.w,
          color: clor,
        ),
        SizedBox(width: 5.w),
        FText(
          text,
          size: 20.sp,
          color: clor,
        )
      ],
    );
  }

  Widget privateChat(UserList userInfo) {
    Widget child = FButton(
      onTap: () async {
        await WidgetUtils().pushPage(MsgChat(userInfo.hxInfo.uid,
            HxMsgExt(userInfo.userId, userInfo.headIcon, userInfo.name)));
      },
      isTheme: true,
      padding: EdgeInsets.symmetric(vertical: 16.w, horizontal: 20.w),
      radius: BorderRadius.circular(40.w),
      child: Row(
        children: [
          LoadAssetImage(
            "home/expansionColumn/privateChat",
            width: 30.w,
            height: 30.w,
          ),
          SizedBox(width: 10.w),
          const FText("私聊")
        ],
      ),
    );
    return GestureDetector(
      child: child,
    );
  }

  // 清空搜索记录
  void clearSearch() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setStringList(SharedKey.KEY_SEARCH_RECORD.value, []);
    searchRecordList = [];
    setState(() {});
  }
}
