// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'search_list.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SearchList _$SearchListFromJson(Map<String, dynamic> json) => SearchList(
      userList: (json['user_list'] as List<dynamic>)
          .map((e) => UserList.fromJson(e as Map<String, dynamic>))
          .toList(),
      roomList: (json['room_list'] as List<dynamic>)
          .map((e) => Rooms.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$SearchListToJson(SearchList instance) =>
    <String, dynamic>{
      'user_list': instance.userList,
      'room_list': instance.roomList,
    };

UserList _$UserListFromJson(Map<String, dynamic> json) => UserList(
      userId: json['user_id'] as String,
      displayId: json['display_id'] as int,
      sex: json['sex'] as int,
      name: json['name'] as String,
      headIcon: json['head_icon'] as String,
      status: json['status'] as int,
      birthday: json['birthday'] as int,
      city: json['city'] as String,
      sign: json['sign'] as String,
      hxInfo: HxInfo.fromJson(json['hx_info'] as Map<String, dynamic>),
      photos:
          (json['photos'] as List<dynamic>?)?.map((e) => e as String).toList(),
      userRole: json['user_role'] as int,
      online: json['online'] as int,
      voice: json['voice'] as String,
      exteriorRoomId: json['exterior_room_id'] as int,
    );

Map<String, dynamic> _$UserListToJson(UserList instance) => <String, dynamic>{
      'user_id': instance.userId,
      'display_id': instance.displayId,
      'sex': instance.sex,
      'name': instance.name,
      'head_icon': instance.headIcon,
      'status': instance.status,
      'birthday': instance.birthday,
      'city': instance.city,
      'sign': instance.sign,
      'hx_info': instance.hxInfo,
      'photos': instance.photos,
      'user_role': instance.userRole,
      'online': instance.online,
      'voice': instance.voice,
      'exterior_room_id': instance.exteriorRoomId,
    };
