import 'package:echo/import.dart';
import 'package:json_annotation/json_annotation.dart';

part 'search_list.g.dart';

@JsonSerializable()
class SearchList {
  @JsonKey(name: 'user_list')
  final List<UserList> userList;
  @JsonKey(name: 'room_list')
  final List<Rooms> roomList;

  const SearchList({
    required this.userList,
    required this.roomList,
  });

  factory SearchList.fromJson(Map<String, dynamic> json) =>
      _$SearchListFromJson(json);

  Map<String, dynamic> toJson() => _$SearchListToJson(this);
}

@JsonSerializable()
class UserList {
  @JsonKey(name: 'user_id')
  final String userId;
  @JsonKey(name: 'display_id')
  final int displayId;
  final int sex;
  final String name;
  @JsonKey(name: 'head_icon')
  final String headIcon;
  final int status;
  final int birthday;
  final String city;
  final String sign;
  @JsonKey(name: 'hx_info')
  final HxInfo hxInfo;
  final List<String>? photos;
  @JsonKey(name: 'user_role')
  final int userRole;
  final int online;
  final String voice;
  @JsonKey(name: 'exterior_room_id')
  final int exteriorRoomId;

  const UserList({
    required this.userId,
    required this.displayId,
    required this.sex,
    required this.name,
    required this.headIcon,
    required this.status,
    required this.birthday,
    required this.city,
    required this.sign,
    required this.hxInfo,
    required this.photos,
    required this.userRole,
    required this.online,
    required this.voice,
    required this.exteriorRoomId,
  });

  factory UserList.fromJson(Map<String, dynamic> json) =>
      _$UserListFromJson(json);

  Map<String, dynamic> toJson() => _$UserListToJson(this);
}
