// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ext_user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ExtUser _$ExtUserFromJson(Map<String, dynamic> json) => ExtUser()
  ..id = json['id'] as String
  ..displayId = json['displayId'] as int
  ..name = json['name'] as String
  ..head = json['headIconUrl'] as String
  ..role = json['role'] as int
  ..wealthBadge = json['wealth_badge'] as String
  ..wealthGrade = json['wealth_grade'] as int
  ..charmBadge = json['charm_badge'] as String
  ..charmGrade = json['charm_grade'] as int;

Map<String, dynamic> _$ExtUserToJson(ExtUser instance) => <String, dynamic>{
      'id': instance.id,
      'displayId': instance.displayId,
      'name': instance.name,
      'headIconUrl': instance.head,
      'role': instance.role,
      'wealth_badge': instance.wealthBadge,
      'wealth_grade': instance.wealthGrade,
      'charm_badge': instance.charmBadge,
      'charm_grade': instance.charmGrade,
    };
