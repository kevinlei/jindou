// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_user_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginUserModel _$LoginUserModelFromJson(Map<String, dynamic> json) =>
    LoginUserModel(
      info: Info.fromJson(json['info'] as Map<String, dynamic>),
      goldInfo: GoldInfo.fromJson(json['gold_info'] as Map<String, dynamic>),
      gradeInfo: GradeInfo.fromJson(json['grade_info'] as Map<String, dynamic>),
      online: json['online'] as int?,
      visitorCount: json['visitor_count'] as int?,
      fansInfo: json['fans_info'] == null
          ? null
          : FansInfo.fromJson(json['fans_info'] as Map<String, dynamic>),
      roomInfo: json['room_info'] == null
          ? null
          : RoomInfo.fromJson(json['room_info'] as Map<String, dynamic>),
      dynamicInfo: json['dynamic_info'] == null
          ? null
          : DynamicInfo.fromJson(json['dynamic_info'] as Map<String, dynamic>),
      giftInfo: json['user_gift_info'] == null
          ? null
          : UserGiftInfo.fromJson(
              json['user_gift_info'] as Map<String, dynamic>),
      token: json['token'] as String?,
    );

Map<String, dynamic> _$LoginUserModelToJson(LoginUserModel instance) =>
    <String, dynamic>{
      'info': instance.info,
      'gold_info': instance.goldInfo,
      'grade_info': instance.gradeInfo,
      'online': instance.online,
      'visitor_count': instance.visitorCount,
      'fans_info': instance.fansInfo,
      'room_info': instance.roomInfo,
      'dynamic_info': instance.dynamicInfo,
      'user_gift_info': instance.giftInfo,
      'token': instance.token,
    };

Info _$InfoFromJson(Map<String, dynamic> json) => Info(
      userId: json['user_id'] as String,
      passwd: json['passwd'] as String?,
      displayId: json['display_id'] as int,
      sex: json['sex'] as int,
      name: json['name'] as String,
      headIcon: json['head_icon'] as String,
      status: json['status'] as int,
      birthday: json['birthday'] as int,
      city: json['city'] as String?,
      registerCity: json['register_city'] as String?,
      sign: json['sign'] as String,
      voice: json['voice'] as String,
      createTime: json['create_time'] as int?,
      updateTime: json['update_time'] as int?,
      phone: json['phone'] as String,
      registerIp: json['register_ip'] as String?,
      idcardId: json['idcard_id'] as String?,
      hxInfo: HxInfo.fromJson(json['hx_info'] as Map<String, dynamic>),
      job: json['job'] as String,
      userRole: json['user_role'] as int,
      transferAccountPower: json['transfer_account_power'] as int,
      secondaryPasswd: json['secondary_passwd'] as String?,
      agoraUid: json['agora_uid'] as int?,
      headIconReviewId: json['head_icon_review_id'] as String?,
      photos:
          (json['photos'] as List<dynamic>?)?.map((e) => e as String).toList(),
      displayIdLevel: json['display_id_type'] as int?,
      teenmode: json['teen_mode'] as int?,
      online: json['online'] as int?,
    );

Map<String, dynamic> _$InfoToJson(Info instance) => <String, dynamic>{
      'user_id': instance.userId,
      'passwd': instance.passwd,
      'display_id': instance.displayId,
      'sex': instance.sex,
      'name': instance.name,
      'head_icon': instance.headIcon,
      'status': instance.status,
      'birthday': instance.birthday,
      'city': instance.city,
      'register_city': instance.registerCity,
      'sign': instance.sign,
      'voice': instance.voice,
      'create_time': instance.createTime,
      'update_time': instance.updateTime,
      'phone': instance.phone,
      'register_ip': instance.registerIp,
      'idcard_id': instance.idcardId,
      'hx_info': instance.hxInfo,
      'job': instance.job,
      'user_role': instance.userRole,
      'transfer_account_power': instance.transferAccountPower,
      'secondary_passwd': instance.secondaryPasswd,
      'agora_uid': instance.agoraUid,
      'display_id_type': instance.displayIdLevel,
      'head_icon_review_id': instance.headIconReviewId,
      'photos': instance.photos,
      'teen_mode': instance.teenmode,
      'online': instance.online,
    };

HxInfo _$HxInfoFromJson(Map<String, dynamic> json) => HxInfo(
      uid: json['uid'] as String,
      password: json['password'] as String?,
      nickName: json['nick_name'] as String,
    );

Map<String, dynamic> _$HxInfoToJson(HxInfo instance) => <String, dynamic>{
      'uid': instance.uid,
      'password': instance.password,
      'nick_name': instance.nickName,
    };

GoldInfo _$GoldInfoFromJson(Map<String, dynamic> json) => GoldInfo(
      gains: (json['gains'] as num?)?.toDouble(),
      gainsLock: (json['gains_lock'] as num?)?.toDouble(),
      gold: (json['gold'] as num?)?.toDouble(),
      goldLock: (json['gold_lock'] as num?)?.toDouble(),
      goldable: (json['gold_able'] as num?)?.toDouble(),
      gainsable: (json['gains_able'] as num?)?.toDouble(),
    );

Map<String, dynamic> _$GoldInfoToJson(GoldInfo instance) => <String, dynamic>{
      'gains': instance.gains,
      'gains_lock': instance.gainsLock,
      'gold': instance.gold,
      'gold_lock': instance.goldLock,
      'gains_able': instance.gainsable,
      'gold_able': instance.goldable,
    };

GradeInfo _$GradeInfoFromJson(Map<String, dynamic> json) => GradeInfo(
      userGrade: UserGrade.fromJson(json['user_grade'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$GradeInfoToJson(GradeInfo instance) => <String, dynamic>{
      'user_grade': instance.userGrade,
    };

UserGrade _$UserGradeFromJson(Map<String, dynamic> json) => UserGrade(
      wealth: Wealth.fromJson(json['wealth'] as Map<String, dynamic>),
      charm: Charm.fromJson(json['charm'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$UserGradeToJson(UserGrade instance) => <String, dynamic>{
      'wealth': instance.wealth,
      'charm': instance.charm,
    };

Wealth _$WealthFromJson(Map<String, dynamic> json) => Wealth(
      picture: json['picture'] as String,
      badge: json['badge'] as String,
      headPicture: json['head_picture'] as String,
      nowExperience: json['now_experience'] as int?,
      experience: json['experience'] as int?,
      grade: json['grade'] as int,
      nextgrade: json['next_grade'] as int,
      nowgradeexperience: json['now_grade_experience'] as int,
      nowneedexperience: json['now_need_experience'] as int,
    );

Map<String, dynamic> _$WealthToJson(Wealth instance) => <String, dynamic>{
      'picture': instance.picture,
      'badge': instance.badge,
      'head_picture': instance.headPicture,
      'now_experience': instance.nowExperience,
      'experience': instance.experience,
      'grade': instance.grade,
      'next_grade': instance.nextgrade,
      'now_grade_experience': instance.nowgradeexperience,
      'now_need_experience': instance.nowneedexperience,
    };

Charm _$CharmFromJson(Map<String, dynamic> json) => Charm(
      picture: json['picture'] as String,
      badge: json['badge'] as String,
      headPicture: json['head_picture'] as String,
      nowExperience: json['now_experience'] as int?,
      experience: json['experience'] as int?,
      grade: json['grade'] as int,
      nextgrade: json['next_grade'] as int?,
      nowgradeexperience: json['now_grade_experience'] as int,
      nowneedexperience: json['now_need_experience'] as int,
    );

Map<String, dynamic> _$CharmToJson(Charm instance) => <String, dynamic>{
      'picture': instance.picture,
      'badge': instance.badge,
      'head_picture': instance.headPicture,
      'now_experience': instance.nowExperience,
      'experience': instance.experience,
      'grade': instance.grade,
      'next_grade': instance.nextgrade,
      'now_grade_experience': instance.nowgradeexperience,
      'now_need_experience': instance.nowneedexperience,
    };

FansInfo _$FansInfoFromJson(Map<String, dynamic> json) => FansInfo(
      flow: json['flow'] as int?,
      fans: json['fans'] as int?,
      friend: json['friend'] as int?,
    );

Map<String, dynamic> _$FansInfoToJson(FansInfo instance) => <String, dynamic>{
      'flow': instance.flow,
      'fans': instance.fans,
      'friend': instance.friend,
    };

RoomInfo _$RoomInfoFromJson(Map<String, dynamic> json) => RoomInfo(
      exteriorRoomId: json['exterior_room_id'] as int,
      roomId: json['room_id'] as int,
      roomName: json['room_name'] as String,
      roomPersonNum: json['room_person_num'] as int,
      roomImage: json['room_image'] as String,
      roomRole: json['room_role'] as int,
      isBanChat: json['is_ban_chat'] as int,
      isBanMic: json['is_ban_mic'] as int,
    );

Map<String, dynamic> _$RoomInfoToJson(RoomInfo instance) => <String, dynamic>{
      'exterior_room_id': instance.exteriorRoomId,
      'room_id': instance.roomId,
      'room_name': instance.roomName,
      'room_person_num': instance.roomPersonNum,
      'room_image': instance.roomImage,
      'room_role': instance.roomRole,
      'is_ban_chat': instance.isBanChat,
      'is_ban_mic': instance.isBanMic,
    };

DynamicInfo _$DynamicInfoFromJson(Map<String, dynamic> json) => DynamicInfo(
      count: json['count'] as int?,
      post: (json['post'] as List<dynamic>)
          .map((e) => Post.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$DynamicInfoToJson(DynamicInfo instance) =>
    <String, dynamic>{
      'count': instance.count,
      'post': instance.post,
    };

Post _$PostFromJson(Map<String, dynamic> json) => Post(
      contentText: json['content_text'] as String?,
      contentType: json['content_type'] as int?,
      id: json['id'] as int?,
      resourceurl: json['resource_url'] as List<dynamic>?,
    );

Map<String, dynamic> _$PostToJson(Post instance) => <String, dynamic>{
      'content_text': instance.contentText,
      'content_type': instance.contentType,
      'id': instance.id,
      'resource_url': instance.resourceurl,
    };

UserGiftInfo _$UserGiftInfoFromJson(Map<String, dynamic> json) => UserGiftInfo(
      giftCount: json['gift_count'] as int,
      giftList: (json['gift_list'] as List<dynamic>?)
          ?.map((e) => GiftList.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$UserGiftInfoToJson(UserGiftInfo instance) =>
    <String, dynamic>{
      'gift_count': instance.giftCount,
      'gift_list': instance.giftList,
    };

GiftList _$GiftListFromJson(Map<String, dynamic> json) => GiftList(
      giftId: json['gift_id'] as int?,
      giftNum: json['gift_num'] as int?,
      giftName: json['gift_name'] as String?,
      giftVfx: json['gift_vfx'] as String?,
      giftPicture: json['gift_picture'] as String?,
      giftPrice: json['gift_price'] as int?,
    );

Map<String, dynamic> _$GiftListToJson(GiftList instance) => <String, dynamic>{
      'gift_id': instance.giftId,
      'gift_num': instance.giftNum,
      'gift_name': instance.giftName,
      'gift_vfx': instance.giftVfx,
      'gift_picture': instance.giftPicture,
      'gift_price': instance.giftPrice,
    };
