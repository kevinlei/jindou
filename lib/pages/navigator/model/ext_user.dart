import 'package:json_annotation/json_annotation.dart';
part 'ext_user.g.dart';

@JsonSerializable()
class ExtUser extends Object {
  @JsonKey(name: 'id')
  late String id;

  @JsonKey(name: 'displayId')
  late int displayId;

  @JsonKey(name: 'name')
  late String name;

  @JsonKey(name: 'headIconUrl')
  late String head;

  @JsonKey(name: 'role')
  late int role;

  @JsonKey(name: 'wealth_badge')
  late String wealthBadge;

  @JsonKey(name: 'wealth_grade')
  late int wealthGrade;

  @JsonKey(name: 'charm_badge')
  late String charmBadge;

  @JsonKey(name: 'charm_grade')
  late int charmGrade;

  ExtUser();

  factory ExtUser.fromJson(Map<String, dynamic> srcJson) =>
      _$ExtUserFromJson(srcJson);

  Map<String, dynamic> toJson() => _$ExtUserToJson(this);
}
