import 'dart:ffi';

import 'package:json_annotation/json_annotation.dart';

part 'login_user_model.g.dart';

@JsonSerializable()
class LoginUserModel {
  final Info info;
  @JsonKey(name: 'gold_info')
  final GoldInfo goldInfo;
  @JsonKey(name: 'grade_info')
  final GradeInfo gradeInfo;
  final int? online;
  @JsonKey(name: 'visitor_count')
  final int? visitorCount;
  @JsonKey(name: 'fans_info')
  final FansInfo? fansInfo;
  @JsonKey(name: 'room_info')
  final RoomInfo? roomInfo;
  @JsonKey(name: 'dynamic_info')
  final DynamicInfo? dynamicInfo;
  @JsonKey(name: 'user_gift_info')
  final UserGiftInfo? giftInfo;
  final String? token;

  LoginUserModel({
    required this.info,
    required this.goldInfo,
    required this.gradeInfo,
    this.online,
    this.visitorCount,
    this.fansInfo,
    this.roomInfo,
    this.dynamicInfo,
    this.giftInfo,
    this.token,
  });

  factory LoginUserModel.fromJson(Map<String, dynamic> json) =>
      _$LoginUserModelFromJson(json);

  Map<String, dynamic> toJson() => _$LoginUserModelToJson(this);
}

@JsonSerializable()
class Info {
  @JsonKey(name: 'user_id')
  final String userId;
  final String? passwd;
  @JsonKey(name: 'display_id')
  final int displayId;
  late int sex;
  late String name;
  @JsonKey(name: 'head_icon')
  late String headIcon;
  final int status;
  late int birthday;
  late String? city;
  @JsonKey(name: 'register_city')
  final String? registerCity;
  late String sign;
  final String voice;
  @JsonKey(name: 'create_time')
  final int? createTime;
  @JsonKey(name: 'update_time')
  final int? updateTime;
  late String phone;
  @JsonKey(name: 'register_ip')
  final String? registerIp;
  @JsonKey(name: 'idcard_id')
  final String? idcardId;
  @JsonKey(name: 'hx_info')
  final HxInfo hxInfo;
  late String job;
  @JsonKey(name: 'user_role')
  final int userRole;
  @JsonKey(name: 'transfer_account_power')
  final int transferAccountPower;
  @JsonKey(name: 'secondary_passwd')
  final String? secondaryPasswd;
  @JsonKey(name: 'agora_uid')
  final int? agoraUid;
  @JsonKey(name: 'display_id_type')
  final int? displayIdLevel;
  @JsonKey(name: 'head_icon_review_id')
  final String? headIconReviewId;
  final List<String>? photos;
  @JsonKey(name: 'teen_mode')
  late int? teenmode;
  @JsonKey(name: 'online')
  late int? online;

  Info({
    required this.userId,
    this.passwd,
    required this.displayId,
    required this.sex,
    required this.name,
    required this.headIcon,
    required this.status,
    required this.birthday,
    this.city,
    this.registerCity,
    required this.sign,
    required this.voice,
    this.createTime,
    this.updateTime,
    required this.phone,
    this.registerIp,
    this.idcardId,
    required this.hxInfo,
    required this.job,
    required this.userRole,
    required this.transferAccountPower,
    this.secondaryPasswd,
    this.agoraUid,
    this.headIconReviewId,
    this.photos,
    this.displayIdLevel,
    required this.teenmode,
    this.online,
  });

  factory Info.fromJson(Map<String, dynamic> json) => _$InfoFromJson(json);

  Map<String, dynamic> toJson() => _$InfoToJson(this);
}

@JsonSerializable()
class HxInfo {
  final String uid;
  final String? password;
  @JsonKey(name: 'nick_name')
  final String nickName;

  const HxInfo({
    required this.uid,
    this.password,
    required this.nickName,
  });

  factory HxInfo.fromJson(Map<String, dynamic> json) => _$HxInfoFromJson(json);

  Map<String, dynamic> toJson() => _$HxInfoToJson(this);
}

@JsonSerializable()
class GoldInfo {
  late double? gains;
  @JsonKey(name: 'gains_lock')
  late double? gainsLock;
  late double? gold;
  @JsonKey(name: 'gold_lock')
  late double? goldLock;
  @JsonKey(name: 'gains_able')
  late double? gainsable;
  @JsonKey(name: 'gold_able')
  late double? goldable;

  GoldInfo({
    this.gains,
    this.gainsLock,
    this.gold,
    this.goldLock,
    this.goldable,
    this.gainsable,
  });

  factory GoldInfo.fromJson(Map<String, dynamic> json) =>
      _$GoldInfoFromJson(json);

  Map<String, dynamic> toJson() => _$GoldInfoToJson(this);
}

@JsonSerializable()
class GradeInfo {
  @JsonKey(name: 'user_grade')
  final UserGrade userGrade;

  const GradeInfo({
    required this.userGrade,
  });

  factory GradeInfo.fromJson(Map<String, dynamic> json) =>
      _$GradeInfoFromJson(json);

  Map<String, dynamic> toJson() => _$GradeInfoToJson(this);
}

@JsonSerializable()
class UserGrade {
  final Wealth wealth;
  final Charm charm;

  const UserGrade({
    required this.wealth,
    required this.charm,
  });

  factory UserGrade.fromJson(Map<String, dynamic> json) =>
      _$UserGradeFromJson(json);

  Map<String, dynamic> toJson() => _$UserGradeToJson(this);
}

@JsonSerializable()
class Wealth {
  final String picture;
  late String badge;
  @JsonKey(name: 'head_picture')
  final String headPicture;
  @JsonKey(name: 'now_experience')
  late int? nowExperience;
  late int? experience;
  late int grade;
  @JsonKey(name: 'next_grade')
  final int nextgrade;
  @JsonKey(name: 'now_grade_experience')
  final int nowgradeexperience;
  @JsonKey(name: 'now_need_experience')
  final int nowneedexperience;

  Wealth({
    required this.picture,
    required this.badge,
    required this.headPicture,
    this.nowExperience,
    this.experience,
    required this.grade,
    required this.nextgrade,
    required this.nowgradeexperience,
    required this.nowneedexperience,
  });

  factory Wealth.fromJson(Map<String, dynamic> json) => _$WealthFromJson(json);

  Map<String, dynamic> toJson() => _$WealthToJson(this);
}

@JsonSerializable()
class Charm {
  final String picture;
  late String badge;
  @JsonKey(name: 'head_picture')
  final String headPicture;
  @JsonKey(name: 'now_experience')
  late int? nowExperience;
  late int? experience;
  late int grade;
  @JsonKey(name: 'next_grade')
  final int? nextgrade;
  @JsonKey(name: 'now_grade_experience')
  final int nowgradeexperience;
  @JsonKey(name: 'now_need_experience')
  final int nowneedexperience;

  Charm({
    required this.picture,
    required this.badge,
    required this.headPicture,
    this.nowExperience,
    this.experience,
    required this.grade,
    required this.nextgrade,
    required this.nowgradeexperience,
    required this.nowneedexperience,
  });

  factory Charm.fromJson(Map<String, dynamic> json) => _$CharmFromJson(json);

  Map<String, dynamic> toJson() => _$CharmToJson(this);
}

@JsonSerializable()
class FansInfo {
  final int? flow;
  final int? fans;
  final int? friend;

  const FansInfo({
    this.flow,
    this.fans,
    this.friend,
  });

  factory FansInfo.fromJson(Map<String, dynamic> json) =>
      _$FansInfoFromJson(json);

  Map<String, dynamic> toJson() => _$FansInfoToJson(this);
}

@JsonSerializable()
class RoomInfo {
  @JsonKey(name: 'exterior_room_id')
  final int exteriorRoomId;
  @JsonKey(name: 'room_id')
  final int roomId;
  @JsonKey(name: 'room_name')
  final String roomName;
  @JsonKey(name: 'room_person_num')
  final int roomPersonNum;
  @JsonKey(name: 'room_image')
  final String roomImage;
  @JsonKey(name: 'room_role')
  final int roomRole;
  @JsonKey(name: 'is_ban_chat')
  final int isBanChat;
  @JsonKey(name: 'is_ban_mic')
  final int isBanMic;

  const RoomInfo({
    required this.exteriorRoomId,
    required this.roomId,
    required this.roomName,
    required this.roomPersonNum,
    required this.roomImage,
    required this.roomRole,
    required this.isBanChat,
    required this.isBanMic,
  });

  factory RoomInfo.fromJson(Map<String, dynamic> json) =>
      _$RoomInfoFromJson(json);

  Map<String, dynamic> toJson() => _$RoomInfoToJson(this);
}

@JsonSerializable()
class DynamicInfo {
  final int? count;
  final List<Post> post;

  DynamicInfo({
    this.count,
    required this.post,
  });

  factory DynamicInfo.fromJson(Map<String, dynamic> json) =>
      _$DynamicInfoFromJson(json);

  Map<String, dynamic> toJson() => _$DynamicInfoToJson(this);
}

@JsonSerializable()
class Post {
  @JsonKey(name: 'content_text')
  final String? contentText;
  @JsonKey(name: 'content_type')
  final int? contentType;
  final int? id;
  @JsonKey(name: 'resource_url')
  final List? resourceurl;

  const Post({
    this.contentText,
    this.contentType,
    this.id,
    this.resourceurl,
  });

  factory Post.fromJson(Map<String, dynamic> json) => _$PostFromJson(json);

  Map<String, dynamic> toJson() => _$PostToJson(this);
}

@JsonSerializable()
class UserGiftInfo {
  @JsonKey(name: 'gift_count')
  final int giftCount;
  @JsonKey(name: 'gift_list')
  final List<GiftList>? giftList;

  UserGiftInfo({
    required this.giftCount,
    required this.giftList,
  });

  factory UserGiftInfo.fromJson(Map<String, dynamic> json) =>
      _$UserGiftInfoFromJson(json);

  Map<String, dynamic> toJson() => _$UserGiftInfoToJson(this);
}

@JsonSerializable()
class GiftList {
  @JsonKey(name: 'gift_id')
  final int? giftId;
  @JsonKey(name: 'gift_num')
  final int? giftNum;
  @JsonKey(name: 'gift_name')
  final String? giftName;
  @JsonKey(name: 'gift_vfx')
  final String? giftVfx;
  @JsonKey(name: 'gift_picture')
  final String? giftPicture;
  @JsonKey(name: 'gift_price')
  final int? giftPrice;

  const GiftList({
    required this.giftId,
    required this.giftNum,
    required this.giftName,
    required this.giftVfx,
    required this.giftPicture,
    required this.giftPrice,
  });

  factory GiftList.fromJson(Map<String, dynamic> json) =>
      _$GiftListFromJson(json);

  Map<String, dynamic> toJson() => _$GiftListToJson(this);
}
