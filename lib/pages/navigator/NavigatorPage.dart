import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:echo/import.dart';
import 'package:echo/pages/home/v2/home_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:im_flutter_sdk/im_flutter_sdk.dart';

class NavigatorPage extends StatefulWidget {
  const NavigatorPage({Key? key}) : super(key: key);

  @override
  _NavigatorPageState createState() => _NavigatorPageState();
}

class _NavigatorPageState extends State<NavigatorPage>
    with WidgetsBindingObserver {
  late final PageController _pageController = PageController();

  Timer? timer;
  DateTime? lastExitTime;

  // 网络状态
  bool wifi = false;

  // 显示公告栏
  bool bulletinBoardState = true;

  @override
  void initState() {
    super.initState();
    // app事件监听
    WidgetsBinding.instance.addObserver(this);

    // 初始化一些数据/商品/礼物等配置
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      initApp();
    });

    // 刷新环信消息列表
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      Future.delayed(const Duration(milliseconds: 300), () async {
        AppManager().refreshUnread();
      });
    });

    // 监听页面切换
    _pageController.addListener(() {
      if (_pageController.page == 2) {
        AppManager().refreshUnread();
      }
    });

    // 环信消息监听
    EMClient.getInstance.addConnectionEventHandler(
        'NavigatorPage',
        EMConnectionEventHandler(
          onConnected: _onConnected,
          onDisconnected: _onDisconnected,
          onTokenDidExpire: _onTokenDidExpire,
        ));
    EMClient.getInstance.chatManager.addEventHandler(
        'NavigatorPage',
        EMChatEventHandler(
          onCmdMessagesReceived: _onCmdMessagesReceived,
          onConversationsUpdate: _onConversationsUpdate,
        ));

    // 关闭公告
    Future.delayed(const Duration(seconds: 30), () {
      bulletinBoardState = false;
      setState(() {});
    });
  }

  void initApp() async {
    // 初始化加载资源
    initNaturalResources();

    AppConstants.hasShowLogin = false;

    // 世界频道id
    AppConstants.initWordCan();

    // 监听网络连接
    AppManager().initPing();

    // 初始化麦克风/扬声器音量
    AppManager().setAudioCaptureVolume(AppManager().roomInfo.micVolume);
    AppManager().setAudioPlayoutVolume(AppManager().roomInfo.speakerVolume);

    // 清掉房间遗留数据
    AppManager().roomInfo.wordRoomMsg.clear();
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setInt(SharedKey.KEY_ROOM_ID.value, 0);
    AppManager().roomInfo.roomMode = null;
    AppManager().initRoom();
    AppManager().initWordMsgList();

    // 加入环信世界房间
    AppManager().hxWordRoom(true);

    // 日活统计
    AppConstants.channelStatistics("Login", extra: AppConstants.deviceId);

    // app表情配置
    requestVersion();

    // 初始化系统通知等
    AppManager().initSys();

    Future.delayed(const Duration(milliseconds: 200), () async {
      // 监听环信消息
      AppManager().joinHx();
    });

    Future.delayed(const Duration(milliseconds: 600), () async {
      // 进入活动页
      onEnterActivity();
    });

    Future.delayed(const Duration(seconds: 3), () async {
      // 加入系统mq
      AppManager().joinSysMq();
    });
  }

  // 初始化礼物/房间背景图片/资源
  void initNaturalResources() {
    DioUtils().asyncHttpRequest(
      NetConstants.config,
      params: {"conf_key": "background_list"},
      loading: false,
      onSuccess: (res) {
        List bgList = [];
        if (res == null) {
          bgList = [];
        } else {
          bgList = jsonDecode(res);
        }
        for (var i = 0; i < bgList.length; i++) {
          precacheImage(
            ImageUtils()
                .getImageProvider(NetConstants.ossPath + bgList[i]["image"]),
            context,
          );
        }
      },
    );
  }

  void requestVersion() {
    ConfigManager().requestGlobalConfig();
  }

  // 进入活动页
  void onEnterActivity() {
    if (AppManager().homeActivity.linkUrl.isNotEmpty) {
      showModalBottomSheet(
          context: context,
          backgroundColor: Colors.transparent,
          isScrollControlled: true,
          isDismissible: false,
          enableDrag: false,
          builder: (_) => RoomGame(url: AppManager().homeActivity.linkUrl));
    } else if (AppManager().homeActivity.linkImg.isNotEmpty) {
      WidgetUtils().pushPage(
        BannerPage(
          title: AppManager().homeActivity.title,
          image: AppManager().homeActivity.linkImg,
        ),
      );
    }
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    super.didChangeAppLifecycleState(state);
    switch (state) {
      case AppLifecycleState.resumed:
        print("触发了resumed");
        break;
      case AppLifecycleState.inactive:
        print("触发了inactive");
        break;
      case AppLifecycleState.paused:
        print("触发了paused");
        break;
      case AppLifecycleState.detached:
        print("触发了detached");
        break;
      case AppLifecycleState.hidden:
        break;
    }
  }

  @override
  void deactivate() {
    AppManager().homeActivity.title = "";
    AppManager().homeActivity.linkUrl = "";
    AppManager().homeActivity.linkImg = "";
    WidgetsBinding.instance.removeObserver(this);
    EMClient.getInstance.chatManager.removeEventHandler('NavigatorPage');
    EMClient.getInstance.removeConnectionEventHandler('NavigatorPage');
    if (AppManager().roomInfo.roomMode != null) {
      AppManager().leaveRoomMq();
      AppManager().leaveEngineManager();
    }
    AppManager().leaveSysMq();
    AppManager().leaveAppEngines();
    AppManager().roomInfo.roomMode = null;
    AppManager().ping.stop();
    _pageController.dispose();
    timer?.cancel();
    timer = null;
    super.deactivate();
  }

  @override
  Widget build(BuildContext context) {
    return ColorFiltered(
      colorFilter: const ColorFilter.mode(Colors.transparent, BlendMode.color),
      child: body(),
    );
  }

  Widget body() {
    if (Platform.isIOS) {
      return GestureDetector(
        onHorizontalDragUpdate: (DragUpdateDetails details) {
          if (details.delta.dx > 10) onWillPop();
        },
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: const Color(0xFF0B0C17),
          body: Stack(
            alignment: Alignment.bottomCenter,
            children: [
              PageView(
                physics: const NeverScrollableScrollPhysics(),
                controller: _pageController,
                children: [
                  HomePage(_pageController), // 首页
                  DynamicPage(_pageController), // 动态
                  const MsgPrivateChat(), // 消息
                  PersonPage(_pageController), // 我的
                ],
              ),
              const RoomFloat(),
              const TopFloatingScreen(eventKey: 'HomeTopFloatingScreen',),
            ],
          ),
          bottomNavigationBar: BottomNavigation(_pageController),
        ),
      );
    } else {
      return WillPopScope(
        onWillPop: onWillPop,
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: const Color(0xFF0B0C17),
          body: Stack(
            alignment: Alignment.bottomCenter,
            children: [
              PageView(
                physics: const NeverScrollableScrollPhysics(),
                controller: _pageController,
                children: [
                  HomePage(_pageController), // 首页
                  DynamicPage(_pageController), // 动态
                  const MsgPrivateChat(), // 消息
                  PersonPage(_pageController), // 我的
                ],
              ),
              const RoomFloat(),
              const TopFloatingScreen(eventKey: 'HomeTopFloatingScreen',),
            ],
          ),
          bottomNavigationBar: BottomNavigation(_pageController),
        ),
      );
    }
  }

  Widget bulletinBoard() {
    return IgnorePointer(
      child: Container(
        alignment: Alignment.topCenter,
        margin: EdgeInsets.only(top: 135.w),
        child: FContainer(
          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
          gradient: [
            const Color(0xFFFFFFFF).withOpacity(0),
            const Color(0xFFFF753F).withOpacity(0.2),
            const Color(0xFFFFFFFF).withOpacity(0),
          ],
          gradientBegin: Alignment.centerLeft,
          gradientEnd: Alignment.centerRight,
          stops: const [0, 0.2, 0.8],
          child: ScrollText(
            maxWidth: 800.w,
            const TextSpan(
              children: [
                TextSpan(
                  text: '绿色健康倡议：平台禁止产生违法、低俗、暴力、涉黄等行为，禁止买卖礼物、色情交易等!',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  // 滑动退出app
  Future<bool> onWillPop() async {
    if (lastExitTime == null ||
        DateTime.now().difference(lastExitTime!).inSeconds > 1) {
      WidgetUtils().showToast("再按一次将会退出APP");
      lastExitTime = DateTime.now();
      return false;
    } else {
      exit(0);
    }
  }

  // 环信连接状态
  void _onConnected() {
    print("环信连接成功");
  }

  void _onDisconnected() async {
    print('环信SDK与聊天服务器断开连接时发生');
  }

  void _onTokenDidExpire() async {
    // 环信token过期
    quketi();
  }

  // 退出登录
  void quketi() async {
    // 关闭监听系统消息
    AppConstants.accessToken = "";
    AppConstants.refreshToken = "";
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.clear();
    preferences.setBool(SharedKey.KEY_UNIQUE.value, false);
    UserConstants.userginInfo = null;
    Future.delayed(const Duration(seconds: 1), () async {
      WidgetUtils().cancelLoading();
      WidgetUtils().pushRemove(const LoginPage());
    });
  }

  void _onCmdMessagesReceived(List<EMMessage> messages) {
    // 收到cmd消息回调
    if (messages.isEmpty) return;
    if (messages[0].body.type == MessageType.CMD) {
      try {
        EMCmdMessageBody messageBody = messages[0].body as EMCmdMessageBody;
        String action = messageBody.action;
        Map? data = jsonDecode(action);
        int actionCode = data?["msgId"] ?? 0;
        String actionMsg = data?["msg"] ?? "";
        if (actionCode == 10001) {
          if (!AppConstants.hasShowLogin) {
            WidgetUtils().pushRemove(const LoginPage());
          }
          WidgetUtils().showNotification(
              "温馨提示", actionMsg.isEmpty ? "您的账号已被封禁" : actionMsg);
        }
      } catch (e) {}
    }
  }

  void _onConversationsUpdate() {
    // 会话列表数量变更
    print("| ----------环信会话列表变更---------- |");
    // unreadProvider.refreshUnread();
  }
}
