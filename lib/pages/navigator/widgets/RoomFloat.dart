import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class RoomFloat extends StatefulWidget {//悬浮房间
  const RoomFloat({Key? key}) : super(key: key);

  @override
  _RoomFloatState createState() => _RoomFloatState();
}

class _RoomFloatState extends State<RoomFloat>
    with SingleTickerProviderStateMixin {
  /// 会重复播放的控制器
  late final AnimationController _repeatController;

  /// 线性动画
  late final Animation<double> _animation;

  bool showFloat = false;
  bool isDragging = false;

  double topOffset = 0, rightOffset = 0;

  // 是否开启青少年模式
  bool adolescentMode = false;

  @override
  void initState() {
    super.initState();
    EventUtils().on(
        EventConstants.E_MQ_MESSAGE,
        EventCall("RoomFloat", (data) {
          if (data[1]?["event"] != MqMsgType.SELF_JOIN.value) return;
          int type = data[1]?["info"]?["type"] ?? 0;
          if (type == 1) {
            showFloat = true;
            _repeatController.repeat();
          } else if (type == 3 && showFloat) {
            showFloat = false;
            _repeatController.stop();
          }
          if (!mounted) return;
          setState(() {});
        }));
    EventUtils().on(
      EventConstants.E_ROOM_BGIMG,
      EventCall("RoomFloat", (v) {
        initFloat();
      }),
    );
    EventUtils().on(
      EventConstants.E_MQ_ADOMODE,
      EventCall("RoomFloat", (v) {
        adolescentMode = UserConstants.userginInfo?.info.teenmode == 1;
        setState(() {});
      }),
    );
    adolescentMode = UserConstants.userginInfo?.info.teenmode == 1;

    _repeatController = AnimationController(
      duration: const Duration(seconds: 5),
      vsync: this,
    );
    _animation = Tween<double>(begin: 0, end: 1).animate(_repeatController);

    initFloat();

    rightOffset = 0;
    topOffset = ScreenUtil().screenHeight / 2 - kToolbarHeight;
  }

  @override
  void deactivate() {
    EventUtils()
        .off(EventConstants.E_ROOM_BGIMG, EventCall("RoomFloat", (v) {}));
    EventUtils()
        .off(EventConstants.E_MQ_MESSAGE, EventCall("RoomFloat", (v) {}));
    EventUtils()
        .off(EventConstants.E_MQ_ADOMODE, EventCall("RoomFloat", (v) {}));
    super.deactivate();
  }

  void initFloat() async {
    if (AppManager().roomInfo.roomMode != null) {
      showFloat = true;
      _repeatController.repeat();
    } else {
      showFloat = false;
      _repeatController.stop();
    }
    if (!mounted) return;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Positioned(
      right: rightOffset,
      top: topOffset,
      child: Visibility(
        visible: !adolescentMode,
        child: showFloat && AppManager().roomInfo.roomMode != null
            ? float()
            : const SizedBox(),
      ),
    );
  }

  Widget float() {
    String cover = AppManager().roomInfo.roomMode!.headIcon;
    return Column(crossAxisAlignment: CrossAxisAlignment.end, children: [
      GestureDetector(
        onTap: onTapRoom,
        onPanUpdate: onDragUpdate,
        onPanEnd: onDragEnd,
        child: RotationTransition(
          turns: _animation,
          child: FContainer(
            width: 125.w,
            height: 125.w,
            color: Colors.white12,
            radius: BorderRadius.circular(125.w),
            margin: EdgeInsets.symmetric(horizontal: 20.w),
            border: Border.all(color: Colors.white, width: 6.w),
            align: Alignment.center,
            imageFit: BoxFit.cover,
            image: ImageUtils().getImageProvider(NetConstants.ossPath + cover),
          ),
        ),
      ),
    ]);
  }

  void onDragUpdate(DragUpdateDetails d) {
    isDragging = true;
    topOffset += d.delta.dy;
    rightOffset -= d.delta.dx;
    if (topOffset <= kToolbarHeight) {
      topOffset = kToolbarHeight;
    }
    if (topOffset >= ScreenUtil().screenHeight - 2 * kToolbarHeight - 100.h) {
      topOffset = ScreenUtil().screenHeight - 2 * kToolbarHeight - 100.h;
    }
    if (rightOffset <= 0) {
      rightOffset = 0;
    }
    if (rightOffset >= ScreenUtil().screenWidth - 150.w) {
      rightOffset = ScreenUtil().screenWidth - 150.w;
    }

    if (!mounted) return;
    setState(() {});
  }

  void onDragEnd(DragEndDetails d) {
    isDragging = false;
    if (rightOffset >= (ScreenUtil().screenWidth - 150.w) / 2) {
      rightOffset = ScreenUtil().screenWidth - 150.w;
    } else {
      rightOffset = 0;
    }
    if (!mounted) return;
    setState(() {});
  }

  void onTapRoom() {
    if (AppManager().roomInfo.roomMode == null) return;
    // WidgetUtils()
    //     .pushPage(RoomPage(AppManager().roomInfo.roomMode!.exteriorRoomId));
    AppManager().joinRoom(AppManager().roomInfo.roomMode!.exteriorRoomId);
  }
}
