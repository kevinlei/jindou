import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class BottomNavigation extends StatefulWidget {
  final PageController controller;
  // final onPressPage;
  // final int? pageNum;

  const BottomNavigation(this.controller, {Key? key}) : super(key: key);

  @override
  _BottomNavigationState createState() => _BottomNavigationState();
}

class _BottomNavigationState extends State<BottomNavigation> {
  // 未读消息
  int unread = 0;

  final bottomText = ["派对", "圈子", "聊天", "我的"];

  int curPage = 0;

  @override
  void initState() {
    super.initState();
    widget.controller.addListener(onPageChanged);
    EventUtils().on(
      EventConstants.E_MSG_UPDATE,
      EventCall(
        "BottomNavigation",
        (data) {
          Future.delayed(const Duration(milliseconds: 200), () async {
            unread = AppManager().unread + AppManager().sysunread;
            if (!mounted) return;
            setState(() {});
          });
        },
      ),
    );
  }

  @override
  void dispose() {
    EventUtils().off(
        EventConstants.E_MSG_UPDATE, EventCall("BottomNavigation", (data) {}));
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double bottom = ScreenUtil().bottomBarHeight;
    return FContainer(
      color: const Color(0xFFFFFFFF),
      padding: EdgeInsets.fromLTRB(10.w, 10.w, 10.w, bottom + 10.w),
      height: 100.w + bottom,
      child: Row(
        children:
            List.generate(bottomText.length, (index) => navigationBar(index)),
      ),
    );
  }

  Widget navigationBar(int page) {
    Widget bar;
    String icon = "navigation/na13";
    Color color =
        page == curPage ? const Color(0xFFFF763F) : const Color(0xFFADB2BE);

    icon = page == curPage
        ? "navigation/na1${page + 1}"
        : "navigation/na0${page + 1}";
    bar = FContainer(
      color: Colors.transparent,
      align: Alignment.center,
      margin: EdgeInsets.symmetric(horizontal: 5.r),
      child: Column(mainAxisSize: MainAxisSize.min, children: [
        Expanded(child: LoadAssetImage(icon)),
        FText(bottomText[page], color: color, size: 20.sp)
      ]),
    );
    if (page == 2) {
      bar =
          Stack(clipBehavior: Clip.none, children: [bar, unreadWidget(unread)]);
    }

    return Flexible(
      child: GestureDetector(onTap: () => onPressPage(page), child: bar),
    );
  }

  Widget unreadWidget(int count) {
    String unreadText = count > 99 ? "..." : "$count";
    return Positioned(
      top: -5.w,
      right: 55.w,
      child: Visibility(
        visible: count > 0,
        child: FContainer(
          radius: BorderRadius.circular(50),
          padding: EdgeInsets.symmetric(vertical: 2.w, horizontal: 8.w),
          color: const Color(0xFFED5353),
          align: Alignment.center,
          child: FText(unreadText, color: Colors.white, size: 20.sp),
        ),
      ),
    );
  }

  void onPressPage(int index) {
    widget.controller.jumpToPage(index);
    // widget.onPressPage(index);
    // curPage = index;
  }

  @override
  void deactivate() {
    super.deactivate();
    widget.controller.removeListener(onPageChanged);
  }

  void onPageChanged() {
    curPage = (widget.controller.page ?? 0).toInt();
    if (!mounted) return;
    setState(() {});
  }
}
