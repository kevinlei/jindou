import 'dart:developer';

import 'package:echo/import.dart';
import 'package:echo/utils/ext/state_ext.dart';
import 'package:echo/utils/string_util.dart';
import 'package:flutter/material.dart';

class TopFloatingScreen extends StatefulWidget {
  //送礼物悬浮
  static bool isInRoomPage = false;

  final String eventKey;

  const TopFloatingScreen({super.key,this.eventKey = 'TopFloatingScreen'});

  @override
  State<TopFloatingScreen> createState() => _TopFloatingScreenState();
}

class _TopFloatingScreenState extends State<TopFloatingScreen>
    with SingleTickerProviderStateMixin {
  /// 平移动画控制器
  late AnimationController moveCtr;

  /// 平移动画
  Animation<Offset>? moveAnim;

  // 房间送礼列表
  List bulletList = [];

  @override
  void initState() {
    super.initState();
    if (isInRoomPage()) {
      TopFloatingScreen.isInRoomPage = true;
    }
    EventUtils().on(
      EventConstants.E_ROOM_GIFT_TOP,
      EventCall(widget.eventKey, (v) {
        if (TopFloatingScreen.isInRoomPage && !isInRoomPage()) {
          return;
        }
        log('------>>${widget.eventKey} receive $v');
        refresh(() {
        bulletList.add(v);
        if ((moveCtr.status != AnimationStatus.forward ||
                moveCtr.status == AnimationStatus.completed) &&
            bulletList.isNotEmpty) {
          moveCtr.reset();
          moveCtr.forward();
        }
        if (!mounted) return;
        setState(() {});});
      }),
    );

    init();
  }

  void init() {
    moveCtr = AnimationController(
      duration: const Duration(seconds: 5),
      vsync: this,
    );
    _initAnim();
  }

  @override
  void dispose() {
    if (isInRoomPage()) {
      TopFloatingScreen.isInRoomPage = false;
    }
    log('------>>${widget.eventKey} receive dispose');
    EventUtils().off(
        EventConstants.E_ROOM_GIFT_TOP, EventCall(widget.eventKey, (v) {}));
    moveCtr.dispose();
    super.dispose();
  }

  bool isInRoomPage() {
    return StringUtil.isEqual('TopFloatingScreen', widget.eventKey);
  }

  void _initAnim() {
    Future.delayed(const Duration(seconds: 1), () async {
      if (!mounted) return;
      double md = MediaQuery.of(context).size.width;
      double giftw = 650.w;
      double offW = (md - giftw) / 4000 - 0.015;

      moveAnim = TweenSequence<Offset>([
        TweenSequenceItem(
            tween:
                Tween<Offset>(begin: const Offset(1, 0), end: Offset(offW, 0))
                    .chain(CurveTween(curve: Curves.easeInOut)),
            weight: 15),
        TweenSequenceItem(tween: ConstantTween(Offset(offW, 0)), weight: 80),
        TweenSequenceItem(
            tween: Tween(begin: const Offset(0, 0), end: const Offset(-1, 0)),
            weight: 15)
      ]).animate(moveCtr);
      moveCtr.addStatusListener(onAnimListener);
      if (!mounted) return;
      setState(() {});
    });
  }

  void onAnimListener(status) {
    if (status == AnimationStatus.completed) {
      if (bulletList.isEmpty) return;
      bulletList.removeAt(0);
      moveCtr.reset();
      moveCtr.forward();
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return bulletList.isNotEmpty
        ? FContainer(
            margin: EdgeInsets.only(top: 150.w),
            height: 60.w,
            child: moveAnim != null
                ? SlideTransition(
                    position: moveAnim!,
                    child: Align(
                      alignment: Alignment.center,
                      child: giftAnim(),
                    ),
                  )
                : null,
          )
        : const IgnorePointer(child: SizedBox());
  }

  Widget giftAnim() {
    bool msgType = AppManager().getMqMsgType(bulletList[0]["event"]) ==
        MqMsgType.ROOM_GIFT;
    return Stack(
      alignment: AlignmentDirectional.centerStart,
      clipBehavior: Clip.none,
      children: msgType ? giftListWdg() : gameListWdg(),
    );
  }

  // 礼物
  List<Widget> giftListWdg() {
    GiftMq info = GiftMq.fromJson(bulletList[0]["info"]);
    return [
      FContainer(
        width: 650.w,
        height: 50.w,
        radius: BorderRadius.circular(50),
        padding: EdgeInsets.symmetric(horizontal: 10.w),
        gradient: const [
          Color(0xFF6034DF),
          Color(0xFFE196E5),
        ],
        gradientBegin: Alignment.centerLeft,
        gradientEnd: Alignment.centerRight,
        child: Row(
          children: [
            SizedBox(width: 70.w),
            Expanded(
              child: ScrollText(
                maxWidth: 490.w,
                TextSpan(
                  style: TextStyle(
                    fontSize: 26.h,
                    color: Colors.white,
                  ),
                  text: "哇塞~ ",
                  children: [
                    TextSpan(
                      style: const TextStyle(
                        color: Color(0xFF8ECFFF),
                      ),
                      text: info.operateUserInfo.name,
                    ),
                    const TextSpan(
                      text: " 送给了 ",
                    ),
                    TextSpan(
                      style: const TextStyle(
                        color: Color(0xFF8ECFFF),
                      ),
                      text: "${info.obtainUserInfo.name} ",
                    ),
                    TextSpan(
                      style: const TextStyle(
                        color: Color(0xFFFFD65D),
                      ),
                      text:
                          "${info.obtainGiftInfo.giftName} x${info.obtainGiftInfo.giftNum}",
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(width: 10.w),
            Visibility(
              visible: info.roomId != AppManager().roomInfo.roomMode!.roomId,
              child: go(info.exRoomId),
            ),
          ],
        ),
      ),
      Positioned(
        left: 10.w,
        child: FContainer(
          width: 60.w,
          height: 60.w,
          imageFit: BoxFit.cover,
          image: ImageUtils().getImageProvider(
              NetConstants.ossPath + info.obtainGiftInfo.giftPicture),
        ),
      )
    ];
  }

  // 游戏
  List<Widget> gameListWdg() {
    GameInfo info = GameInfo.fromJson(bulletList[0]["info"]);
    return [
      FContainer(
        width: 650.w,
        height: 50.w,
        radius: BorderRadius.circular(50),
        padding: EdgeInsets.symmetric(horizontal: 10.w),
        gradient: info.topPicture!.isEmpty
            ? const [
                Color(0xFFEF5C88),
                Color(0xFFAE49EC),
              ]
            : null,
        gradientBegin: Alignment.centerLeft,
        gradientEnd: Alignment.centerRight,
        image: info.topPicture!.isNotEmpty
            ? ImageUtils()
                .getImageProvider(NetConstants.ossPath + info.topPicture!)
            : null,
        imageFit: BoxFit.cover,
        child: Row(
          children: [
            SizedBox(width: 70.w),
            Expanded(
              child: ScrollText(
                maxWidth: 490.w,
                TextSpan(
                  style: TextStyle(
                    fontSize: 26.h,
                    color: Colors.white,
                  ),
                  text: "哇塞~ ",
                  children: [
                    TextSpan(
                      style: const TextStyle(
                        color: Color(0xFF8ECFFF),
                      ),
                      text: info.userName,
                    ),
                    const TextSpan(
                      text: " 在游戏 ",
                    ),
                    TextSpan(
                      style: const TextStyle(
                        color: Color(0xFF8ECFFF),
                      ),
                      text: "${info.gameName} ",
                    ),
                    const TextSpan(
                      text: " 中获得 ",
                    ),
                    TextSpan(
                      style: const TextStyle(
                        color: Color(0xFFFFD65D),
                      ),
                      text:
                          "${info.giftInfo!.giftName} x${info.giftInfo!.giftNum}",
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      Positioned(
        left: 10.w,
        child: FContainer(
          width: 60.w,
          height: 60.w,
          imageFit: BoxFit.cover,
          image: ImageUtils().getImageProvider(
              NetConstants.ossPath + info.giftInfo!.giftPicture),
        ),
      )
    ];
  }

  Widget go(int roomId) {
    return FButton(
      onTap: () async {
        bool? accept = await WidgetUtils().showAlert("确定前往该房间吗");
        if (accept != true) return;
        AppManager().joinRoom(roomId);
      },
      height: 35.w,
      padding: EdgeInsets.symmetric(vertical: 0, horizontal: 12.w),
      gradient: const [
        Color(0xFFFFECA8),
        Color(0xFFFFECA8),
      ],
      child: Row(
        children: [
          FText(
            "Go",
            size: 22.sp,
            color: Colors.red,
          ),
          LoadAssetImage(
            "public/RightNavigation",
            width: 16.w,
            color: Colors.red,
          ),
        ],
      ),
    );
  }
}
