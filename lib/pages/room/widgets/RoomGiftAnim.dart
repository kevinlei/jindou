import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class RoomGiftAnim extends StatefulWidget {
  const RoomGiftAnim({super.key});

  @override
  State<RoomGiftAnim> createState() => _RoomGiftAnimState();
}

class _RoomGiftAnimState extends State<RoomGiftAnim> {
  // 房间送礼列表
  List<GiftMq> bulletList1 = [];
  List<GiftMq> bulletList2 = [];

  // 进入哪条道路
  int channel = 0;

  @override
  void initState() {
    super.initState();
    EventUtils().on(
      EventConstants.E_ROOM_GIFT,
      EventCall("RoomGiftAnim", (v) {
        if (channel == 0) {
          channel = 1;
          bulletList1.add(v);
        } else {
          channel = 0;
          bulletList2.add(v);
        }
        if (!mounted) return;
        setState(() {});
      }),
    );
  }

  @override
  void dispose() {
    EventUtils()
        .off(EventConstants.E_ROOM_GIFT, EventCall("RoomGiftAnim", (v) {}));
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: kToolbarHeight + 480.h,
      left: 20.w,
      right: 0,
      child: IgnorePointer(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GiftAnim(0, bulletList1),
            GiftAnim(1, bulletList2),
          ],
        ),
      ),
    );
  }
}

// 礼物动画
class GiftAnim extends StatefulWidget {
  final int index;
  final List<GiftMq> bulletList;
  const GiftAnim(this.index, this.bulletList, {super.key});

  @override
  State<GiftAnim> createState() => _GiftAnimState();
}

class _GiftAnimState extends State<GiftAnim>
    with SingleTickerProviderStateMixin {
  /// 平移动画
  late final AnimationController _controller;
  late Animation<Offset> moveAnim;

  // 房间送礼列表
  List<GiftMq> bulletList = [];

  // 进入哪条道路
  int channel = 0;

  @override
  void initState() {
    super.initState();
    if (!mounted) return;
    // 初始化执行动画列表
    bulletList = widget.bulletList;
    init();
  }

  void init() {
    // 初始化平移动画
    _controller = AnimationController(
      duration: const Duration(seconds: 3),
      vsync: this,
    );
    moveAnim = TweenSequence<Offset>([
      TweenSequenceItem(
          tween:
              Tween<Offset>(begin: const Offset(2, 0), end: const Offset(0, 0))
                  .chain(CurveTween(curve: Curves.easeInOut)),
          weight: 20),
      TweenSequenceItem(tween: ConstantTween(const Offset(0, 0)), weight: 60),
      TweenSequenceItem(
          tween: Tween(begin: const Offset(0, 0), end: const Offset(-2, 0)),
          weight: 20)
    ]).animate(_controller);
    _controller.addStatusListener(onAnimListener);

    if (bulletList.isNotEmpty) {
      _controller.forward();
    }
  }

  void onAnimListener(status) {
    if (status == AnimationStatus.completed) {
      if (bulletList.isEmpty) return;
      bulletList.removeAt(0);
      _controller.reset();
      _controller.forward();
      if (!mounted) return;
      setState(() {});
    }
  }

  @override
  void didUpdateWidget(covariant GiftAnim oldWidget) {
    bulletList = oldWidget.bulletList;
    if (!mounted) return;
    setState(() {});
    if ((_controller.status != AnimationStatus.forward ||
            _controller.status == AnimationStatus.completed) &&
        bulletList.isNotEmpty) {
      _controller.reset();
      _controller.forward();
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    EventUtils().off(EventConstants.E_ROOM_GIFT,
        EventCall("GiftAnim${widget.index}", (v) {}));
    _controller.removeStatusListener(onAnimListener);
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SlideTransition(
      position: moveAnim,
      child: Align(
        alignment: Alignment.centerLeft,
        child: bulletList.isNotEmpty ? giftAnim(bulletList.first) : null,
      ),
    );
  }

  Widget giftAnim(GiftMq info) {
    return FContainer(
      margin: EdgeInsets.only(bottom: 40.w),
      width: 400.w,
      padding: EdgeInsets.all(10.w),
      gradient: [
        const Color(0xFFFF367B),
        const Color(0xFFFF95BB).withOpacity(0.6),
        const Color(0xFFFFFFFF).withOpacity(0),
      ],
      gradientBegin: Alignment.centerLeft,
      gradientEnd: Alignment.centerRight,
      stops: const [0, 0.7, 0.9],
      radius: const BorderRadius.only(
          topLeft: Radius.circular(50), bottomLeft: Radius.circular(50)),
      child: Stack(
        clipBehavior: Clip.none,
        children: [
          SizedBox(
            height: 80.w,
            child: Row(
              children: [
                FContainer(
                  width: 80.w,
                  height: 80.w,
                  radius: BorderRadius.circular(50),
                  imageFit: BoxFit.cover,
                  image: ImageUtils().getImageProvider(
                      NetConstants.ossPath + info.operateUserInfo.headIcon),
                ),
                SizedBox(width: 10.w),
                SizedBox(
                  height: 80.w,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      FText(info.operateUserInfo.name),
                      FText(
                          "送给${info.obtainUserInfo.userId == UserConstants.userginInfo!.info.userId ? "你" : info.obtainUserInfo.name}"),
                    ],
                  ),
                )
              ],
            ),
          ),
          Positioned(
            right: -40.h,
            bottom: -10.h,
            child: FContainer(
              width: 120.h,
              height: 120.h,
              imageFit: BoxFit.cover,
              image: ImageUtils().getImageProvider(
                  NetConstants.ossPath + info.obtainGiftInfo.giftPicture),
            ),
          ),
          Positioned(
            left: 350.w,
            top: -30.h,
            child: FContainer(
              child: Row(
                children: [
                  LoadAssetImage("room/animation/x", width: 35.w),
                  ...nums(info.obtainGiftInfo.giftNum),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  List<Widget> nums(int num) {
    List<String> list = num.toString().split("");
    List<Widget> childList = [];
    for (var i = 0; i < list.length; i++) {
      childList.add(LoadAssetImage("room/animation/${list[i]}", width: 35.w));
    }
    return childList;
  }
}
