import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class RoomGameGroup extends StatefulWidget {
  const RoomGameGroup({Key? key}) : super(key: key);

  @override
  _RoomGameGroupState createState() => _RoomGameGroupState();
}

class _RoomGameGroupState extends State<RoomGameGroup> {
  List listTop = [];
  List listCen = [];
  List listEnd = [];

  @override
  void initState() {
    super.initState();
    EventUtils().on(
      EventConstants.E_MY_WEALTH,
      EventCall("RoomGameGroup", (v) {
        if (!mounted) return;
        setState(() {});
      }),
    );

    requestGameList();
  }

  @override
  void dispose() {
    EventUtils()
        .off(EventConstants.E_MY_WEALTH, EventCall("RoomGameGroup", (v) {}));
    super.dispose();
  }

  void requestGameList() async {
    DioUtils().asyncHttpRequest(
      NetConstants.banner,
      loading: false,
      params: {
        "type": BanbarSwper.ROOM_GAME_COM.brand,
        "position": 1,
      },
      onSuccess: (res) {
        if (res == null) return;
        updateGameList(res);
      },
    );
  }

  void updateGameList(response) {
    response ??= [];
    listTop.clear();
    listCen.clear();
    listEnd.clear();
    for (var i = 0; i < response.length; i++) {
      if (response[i]["sort"].toString() == "1") {
        listTop.add(response[i]);
      } else if (response[i]["sort"].toString() == "2") {
        listCen.add(response[i]);
      } else {
        listEnd.add(response[i]);
      }
    }
    if (!mounted) return;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: (UserConstants
                  .userginInfo!.gradeInfo.userGrade.wealth.nowExperience ??
              0) >=
          AppManager().roomInfo.isWealthRestrict,
      child: Align(
        alignment: Alignment.bottomCenter,
        child: FContainer(
          height: 130.w * 3,
          margin: EdgeInsets.only(right: 10.w),
          child: SingleChildScrollView(
            child: Column(children: [
              Visibility(visible: listTop.isNotEmpty, child: listCell(listTop)),
              Visibility(visible: listCen.isNotEmpty, child: listCell(listCen)),
              Visibility(visible: listEnd.isNotEmpty, child: listCell(listEnd)),
            ]),
          ),
        ),
      ),
    );
  }

  Widget listCell(List list) {
    return FContainer(
      width: 120.w,
      height: 120.w,
      margin: EdgeInsets.only(bottom: 10.w),
      child: Swiper(
        pagination: list.length > 1
            ? const SwiperPagination(
                builder: DotSwiperPaginationBuilder(
                color: Colors.white,
                activeColor: Color(0xFFFF7CE2),
                size: 5,
                activeSize: 5,
              ))
            : null,
        itemBuilder: (_, index) => gameCell(list[index]),
        itemCount: list.length,
        autoplay: list.length > 1,
        loop: list.length > 1,
        onTap: (index) => onTapGame(list[index]),
      ),
    );
  }

  Widget gameCell(game) {
    String image = game["img_url"];
    return Center(
      child: LoadNetImage(NetConstants.ossPath + image.trim(),
          width: 100.w, height: 100.w),
    );
  }

  void onTapGame(game) async {
    if (AppManager().time == 999) return WidgetUtils().showToast("网络异常");
    String linkUrl = game["link_url"];
    if (linkUrl.isEmpty) return;
    await showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      isScrollControlled: true,
      isDismissible: false,
      enableDrag: false,
      builder: (_) => RoomGame(url: linkUrl),
    );
  }
}
