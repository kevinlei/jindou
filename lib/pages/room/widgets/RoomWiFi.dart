import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class RoomWiFi extends StatefulWidget {
  const RoomWiFi({super.key});

  @override
  State<RoomWiFi> createState() => _RoomWiFiState();
}

class _RoomWiFiState extends State<RoomWiFi> {
  int time = 0;

  @override
  void initState() {
    super.initState();
    time = AppManager().time;
    EventUtils().on(
      EventConstants.E_ROOM_WIFI,
      EventCall("RoomWiFi", (v) {
        if (v == null) return;
        time = v;
        if (!mounted) return;
        setState(() {});
      }),
    );
  }

  @override
  void deactivate() {
    EventUtils().off(EventConstants.E_ROOM_WIFI, EventCall("RoomWiFi", (v) {}));
    super.deactivate();
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      margin: EdgeInsets.only(left: 10.w, top: 5.w),
      padding: EdgeInsets.symmetric(horizontal: 15.w, vertical: 10.w),
      color: bgColor(),
      radius: BorderRadius.circular(40.w),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          LoadAssetImage(
            "room/wifi",
            height: 25.w,
            color: textColor(),
          ),
          SizedBox(width: 10.w),
          FText(
            "$time ms",
            size: 22.sp,
            color: textColor(),
          )
        ],
      ),
    );
  }

  Color textColor() {
    Color color = const Color(0xFF90FF9D);
    if (time >= 120 && time <= 300) {
      color = const Color(0xFFFFC079);
    } else if (time > 300) {
      color = const Color(0xFFFF6565);
    }

    return color;
  }

  Color bgColor() {
    Color color = const Color(0xFF034A17).withOpacity(0.3);
    if (time >= 120 && time <= 300) {
      color = const Color(0xFF583004).withOpacity(0.3);
    } else if (time > 300) {
      color = const Color(0xFF580404).withOpacity(0.3);
    }

    return color;
  }
}
