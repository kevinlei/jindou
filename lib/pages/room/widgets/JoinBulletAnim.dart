import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class JoinBulletAnim extends StatefulWidget {
  const JoinBulletAnim({super.key});

  @override
  State<JoinBulletAnim> createState() => _JoinBulletAnimState();
}

class _JoinBulletAnimState extends State<JoinBulletAnim>
    with SingleTickerProviderStateMixin {
  /// 平移动画
  late final AnimationController _controller;
  late Animation<Offset> moveAnim;

  // 进房间用户列表
  List<ExtUser> bulletList = [];

  @override
  void initState() {
    super.initState();
    EventUtils().on(
      EventConstants.E_ROOM_JOIN,
      EventCall("JoinBulletAnim", (v) {
        int userId = v["displayId"];
        late ExtUser user;
        if (userId == UserConstants.userginInfo!.info.displayId) {
          user = UserConstants.getExtUser()!;
        } else {
          user = ExtUser.fromJson(v);
        }
        bulletList.add(user);
        if (!mounted) return;
        setState(() {});
        if (_controller.status != AnimationStatus.forward ||
            _controller.status == AnimationStatus.completed) {
          _controller.reset();
          _controller.forward();
        }
      }),
    );

    init();
  }

  void init() {
    // 初始化平移动画
    _controller = AnimationController(
      duration: const Duration(seconds: 3),
      vsync: this,
    );
    moveAnim = TweenSequence<Offset>([
      TweenSequenceItem(
          tween:
              Tween<Offset>(begin: const Offset(1, 0), end: const Offset(0, 0))
                  .chain(CurveTween(curve: Curves.easeInOut)),
          weight: 20),
      TweenSequenceItem(tween: ConstantTween(const Offset(0, 0)), weight: 60),
      TweenSequenceItem(
          tween: Tween(begin: const Offset(0, 0), end: const Offset(-1, 0)),
          weight: 20)
    ]).animate(_controller);
    _controller.addStatusListener(onAnimListener);
  }

  @override
  void dispose() {
    EventUtils()
        .off(EventConstants.E_ROOM_JOIN, EventCall("JoinBulletAnim", (v) {}));
    _controller.removeStatusListener(onAnimListener);
    _controller.dispose();
    super.dispose();
  }

  void onAnimListener(status) {
    if (status == AnimationStatus.completed) {
      if (bulletList.isEmpty) return;
      bulletList.removeAt(0);
      _controller.reset();
      _controller.forward();
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: kToolbarHeight + 800.h,
      left: 20.w,
      right: 0,
      child: IgnorePointer(
        child: SizedBox(
          height: 100.w,
          child: SlideTransition(
            position: moveAnim,
            child: Align(
              alignment: Alignment.centerLeft,
              child: bulletList.isNotEmpty ? joinAnim() : null,
            ),
          ),
        ),
      ),
    );
  }

  Widget joinAnim() {
    return FContainer(
      width: 370.w,
      padding: EdgeInsets.all(10.w),
      imageFit: BoxFit.fill,
      image: ImageUtils().getAssetsImage("room/animation/join"),
      radius: const BorderRadius.only(
          topLeft: Radius.circular(50), bottomLeft: Radius.circular(50)),
      child: Row(
        children: [
          FContainer(
            width: 80.w,
            height: 80.w,
            radius: BorderRadius.circular(50),
            imageFit: BoxFit.cover,
            image: ImageUtils()
                .getImageProvider(NetConstants.ossPath + bulletList[0].head),
          ),
          SizedBox(width: 10.w),
          SizedBox(
            height: 80.w,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                FText(bulletList[0].name),
                const FText("进入了房间"),
              ],
            ),
          )
        ],
      ),
    );
  }
}
