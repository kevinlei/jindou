import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class RoomTheCharts extends StatefulWidget {
  // 初始化页面
  final int index;
  // 麦位
  final int? micNum;
  const RoomTheCharts({super.key, this.index = 0, this.micNum});

  @override
  State<RoomTheCharts> createState() => _RoomTheChartsState();
}

class _RoomTheChartsState extends State<RoomTheCharts>
    with TickerProviderStateMixin {
  late TabController tabController;

  // 榜单范围
  List<int> types = [0, 1, 2];
  // 当前选择什么榜
  int typeId = 0;

  // 房间在线人数
  List userList = [];
  int count = 0;

  // 今日/今周/今月总流水
  int allBarley = 0;
  // 财富榜
  List wealthList = [];
  // 魅力榜
  List charmList = [];
  // 自己的信息
  var wealtCharmhUser;

  @override
  void initState() {
    super.initState();
    tabController =
        TabController(length: 3, vsync: this, initialIndex: widget.index);
    initList(widget.index);
  }

  // 初始化
  void initList(int index) {
    switch (index) {
      case 0:
        wealthCharmList(0);
        break;
      case 1:
        wealthCharmList(1);
        break;
      default:
        onlineUsers();
    }
  }

  // 获取房间财富/魅力榜
  void wealthCharmList(int type) async {
    await DioUtils().httpRequest(
      NetConstants.wealthCharmList,
      method: DioMethod.POST,
      loading: false,
      params: {
        "room_id": AppManager().roomInfo.roomMode!.roomId,
        "billboard_type": type,
        "time_type": typeId,
        "user_id": UserConstants.userginInfo!.info.userId,
      },
      onSuccess: (res) {
        if (res == null || res["billboard_user_infos"] == null) {
          if (type == 0) {
            wealthList = [];
          } else {
            charmList = [];
          }
          wealtCharmhUser = null;
          allBarley = 0;
        } else {
          if (type == 0) {
            wealthList = res["billboard_user_infos"];
          } else {
            charmList = res["billboard_user_infos"];
          }
          wealtCharmhUser = res["user_info"];
          allBarley = res["experience_count"];
        }
        setState(() {});
      },
    );
  }

  // 获取房间在线人数列表
  void onlineUsers() async {
    await DioUtils().httpRequest(
      NetConstants.roomUserList,
      method: DioMethod.POST,
      loading: false,
      params: {
        "exterior_room_id": AppManager().roomInfo.roomMode!.exteriorRoomId
      },
      onSuccess: (res) {
        if (res == null) return;
        userList = res["userInfos"];
        count = res["count"];
        setState(() {});
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      padding: EdgeInsets.only(
        top: 50.w,
        left: 30.w,
        right: 30.w,
        bottom: ScreenUtil().bottomBarHeight,
      ),
      height: 1300.w,
      radius: BorderRadius.only(
          topLeft: Radius.circular(20.w), topRight: Radius.circular(20.w)),
      image: tabController.index != 2
          ? ImageUtils().getAssetsImage(tabController.index == 0
              ? "theCharts/wealthBillboard"
              : "theCharts/charmBillboard")
          : null,
      imageFit: BoxFit.fill,
      gradient: tabController.index == 2
          ? [
              const Color(0xffFF9348),
              const Color(0xffFF9348),
            ]
          : [
              Colors.transparent,
              Colors.transparent,
            ],
      gradientBegin: Alignment.topRight,
      gradientEnd: Alignment.bottomLeft,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          header(),
          SizedBox(height: 30.w),
          option(),
          Expanded(
            child: TabBarView(
              controller: tabController,
              physics: const NeverScrollableScrollPhysics(),
              children: [
                theChartList(wealthList),
                theChartList(charmList),
                onlineUserList(),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget header() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        headerItem("财富榜", 0),
        headerItem("魅力榜", 1),
        headerItem("在线列表", 2),
      ],
    );
  }

  Widget headerItem(String text, int index) {
    return GestureDetector(
      onTap: () {
        tabController.animateTo(index);
        setState(() {});
        initList(index);
      },
      child: FContainer(
        child: Column(
          children: [
            FText(
              text,
              size: 34.sp,
              color: tabController.index == index
                  ? Colors.white
                  : Colors.white.withOpacity(0.7),
            ),
            SizedBox(height: 8.w),
            FContainer(
              width: 26.w,
              height: 5.w,
              radius: BorderRadius.circular(50),
              color: tabController.index == index
                  ? Colors.white
                  : Colors.transparent,
            )
          ],
        ),
      ),
    );
  }

  Widget option() {
    if (tabController.index != 2) {
      return chartsType();
    } else {
      return FText(
        "在线总人数：$count人",
        size: 26.sp,
      );
    }
  }

  Widget chartsType() {
    String text = "";
    switch (typeId) {
      case 0:
        text = "日";
        break;
      case 1:
        text = "周";
        break;
      default:
        text = "月";
    }
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 40.w),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          chartsTypeItem("日榜", 0),
          chartsTypeItem("周榜", 1),
          chartsTypeItem("月榜", 2),
          const Spacer(),
          FText("今$text总值：$allBarley")
        ],
      ),
    );
  }

  Widget chartsTypeItem(String text, int index) {
    return GestureDetector(
      onTap: () {
        typeId = index;
        initList(tabController.index);
        setState(() {});
      },
      child: FContainer(
        radius: BorderRadius.circular(50),
        padding: EdgeInsets.symmetric(vertical: 10.w, horizontal: 22.w),
        color: index == typeId ? Colors.white.withOpacity(0.4) : null,
        child: FText(
          text,
          size: 28.sp,
          color: index == typeId ? Colors.white : Colors.white.withOpacity(0.7),
        ),
      ),
    );
  }

  Widget theChartList(List list) {
    return Column(
      children: [
        theChartsHeader(list),
        SizedBox(height: 80.w),
        Expanded(child: theChartsUserList(list))
      ],
    );
  }

  Widget theChartsHeader(List list) {
    return FContainer(
      padding: EdgeInsets.symmetric(horizontal: 30.w),
      height: 330.w,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          theChartsHeaderItem(1, list.length > 1 ? list[1] : null),
          theChartsHeaderItem(2, list.isNotEmpty ? list[0] : null),
          theChartsHeaderItem(3, list.length > 2 ? list[2] : null),
        ],
      ),
    );
  }

  Widget theChartsHeaderItem(int index, data) {
    String img = "one";
    switch (index) {
      case 1:
        img = "two";
        break;
      case 3:
        img = "three";
        break;
    }
    double marginTop = 20.w;
    switch (index) {
      case 1:
        marginTop = 70.w;
        break;
      case 3:
        marginTop = 80.w;
        break;
    }
    return FContainer(
      width: 180.w,
      margin: EdgeInsets.only(top: marginTop, right: index == 3 ? 10.w : 0),
      child: Column(
        children: [
          Stack(
            children: [
              Positioned(
                  top: index == 2 ? 30.w : 20.w,
                  left: 6.w,
                  child: FContainer(
                    width: 111.w,
                    height: 111.w,
                    radius: BorderRadius.circular(50),
                    color: Colors.transparent,
                    imageFit: BoxFit.cover,
                    image:
                        data?["user_image"] != null && data["user_image"] != ""
                            ? ImageUtils().getImageProvider(
                                NetConstants.ossPath + data["user_image"])
                            : null,
                  )),
              LoadAssetImage(
                "theCharts/$img",
                width: 120.w,
              )
            ],
          ),
          SizedBox(height: 8.w),
          FText(
            data?["user_name"] ?? "",
            size: 28.sp,
            color: Colors.black,
            weight: FontWeight.w500,
            overflow: TextOverflow.ellipsis,
          ),
          SizedBox(height: 4.w),
          Expanded(
              child: FText(
            "${data?["experience"] ?? ""}",
            overflow: TextOverflow.ellipsis,
            align: TextAlign.center,
            color: Colors.black,
          ))
        ],
      ),
    );
  }

  // 在线列表
  Widget theChartsUserList(List list) {
    return list.length > 3
        ? FContainer(
            margin: EdgeInsets.symmetric(horizontal: 40.w),
            border: Border.all(color: Colors.white),
            color: Colors.white.withOpacity(0.6),
            radius: const BorderRadius.only(
                topLeft: Radius.circular(50), topRight: Radius.circular(50)),
            child: ListView.builder(
              addAutomaticKeepAlives: false,
              addRepaintBoundaries: false,
              padding: EdgeInsets.only(top: 20.w),
              itemBuilder: (context, index) =>
                  userItem(index + 3, list[index + 3]),
              itemCount: list.length - 3,
            ),
          )
        : const SizedBox();
  }

  Widget userItem(int index, data) {
    int num = index + 1;
    return FContainer(
      margin: EdgeInsets.only(bottom: 20.w),
      padding: EdgeInsets.symmetric(horizontal: 22.w),
      height: 120.w,
      child: Row(
        children: [
          FText("${num > 9 ? num : "0$num"}",
              size: 26.sp, color: const Color(0xFFBBBABA)),
          SizedBox(width: 20.w),
          FContainer(
            width: 90.w,
            height: 90.w,
            radius: BorderRadius.circular(50),
            imageFit: BoxFit.cover,
            image: ImageUtils().getImageProvider(
              NetConstants.ossPath + data["user_image"],
            ),
          ),
          SizedBox(width: 10.w),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              FText(
                data["user_name"],
                size: 28.sp,
                color: Colors.black,
                weight: FontWeight.w500,
              ),
              GenderAge(index % 2 == 0, data["birthday"]),
            ],
          ),
          Expanded(
            child: FText(
              "${data["experience"]}",
              size: 26.sp,
              color: const Color(0xFFBBBABA),
              align: TextAlign.right,
              maxLines: 3,
            ),
          )
        ],
      ),
    );
  }

  Widget onlineUserList() {
    return ListView.builder(
      addAutomaticKeepAlives: false,
      addRepaintBoundaries: false,
      padding: EdgeInsets.only(top: 20.w),
      itemBuilder: (context, index) => onlineUserListItem(index),
      itemCount: userList.length,
    );
  }

  Widget onlineUserListItem(int index) {
    var data = userList[index];
    int num = index + 1;

    Widget child = FContainer(
      margin: EdgeInsets.only(bottom: 20.w),
      padding: EdgeInsets.symmetric(horizontal: 22.w),
      height: 120.w,
      child: Row(
        children: [
          FText("${num > 9 ? num : "0$num"}",
              size: 26.sp, color: Colors.white.withOpacity(0.7)),
          SizedBox(width: 20.w),
          FContainer(
            width: 90.w,
            height: 90.w,
            color: Colors.white,
            radius: BorderRadius.circular(50),
            imageFit: BoxFit.cover,
            image: ImageUtils()
                .getImageProvider(NetConstants.ossPath + data["user_image"]),
          ),
          SizedBox(width: 10.w),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              FText(
                data["user_name"],
                size: 28.sp,
                color: Colors.white,
                weight: FontWeight.w500,
              ),
              GenderAge(data["user_sex"] == 1, data["user_birthday"]),
            ],
          ),
          const Spacer(),
          Visibility(
              visible:
                  data["user_id"] != UserConstants.userginInfo!.info.userId,
              child: FButton(
                onTap: () =>
                    followWithInterest(data["user_id"], data["relationship"]),
                bgColor: Colors.white.withOpacity(0.3),
                child: Row(
                  children: [
                    Icon(data["relationship"] == 0 ? Icons.add : Icons.check,
                        size: 32.sp, color: Colors.white),
                    SizedBox(width: 5.w),
                    FText(
                      data["relationship"] == 0 ? "关注" : "已关注",
                      size: 28.sp,
                    ),
                  ],
                ),
              ))
        ],
      ),
    );

    return GestureDetector(
      onTap: () {
        WidgetUtils().showBottomSheet(
            RoomUserInfo(
              data["user_id"],
              micNum: widget.micNum,
            ),
            color: Colors.transparent);
      },
      child: child,
    );
  }

  // 关注/取消关注
  void followWithInterest(String userId, int isFlow) async {
    debugPrint("好友关系$isFlow");
    await DioUtils().httpRequest(
      NetConstants.followUser,
      method: DioMethod.POST,
      params: {
        "flow_user_id": userId,
        "flow_action": isFlow == 0 ? 10 : 20,
      },
      onSuccess: (res) {
        initList(2);
      },
    );
  }
}
