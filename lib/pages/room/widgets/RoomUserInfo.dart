import 'package:echo/import.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:im_flutter_sdk/im_flutter_sdk.dart';

class RoomUserInfo extends StatefulWidget {
  // 用户id
  final String userId;
  // 页面层级
  final int popNums;
  // 麦位
  final int? micNum;
  const RoomUserInfo(this.userId, {super.key, this.popNums = 1, this.micNum});

  @override
  State<RoomUserInfo> createState() => _RoomUserInfoState();
}

class _RoomUserInfoState extends State<RoomUserInfo> {
  UserRoomInfo? userInfo;

  // 是否在麦上
  bool isSeat = false;

  // 是否管理
  bool isAdmin = false;

  // 是否房主
  bool isHomeowner = false;

  // 是否展现底部按钮列表
  bool isFooter = false;

  @override
  void initState() {
    super.initState();
    initUser();
  }

  void initUser() async {
    await DioUtils().httpRequest(
      NetConstants.userRoomInfo,
      loading: false,
      params: {"user_id": widget.userId},
      onSuccess: (res) {
        if (res == null) return;
        userInfo = UserRoomInfo.fromJson(res);
        print("用户权限${userInfo?.roomInfo.roomRole}");
        for (var i = 0; i < AppManager().roomInfo.seatLists.length; i++) {
          if (widget.userId ==
              AppManager().roomInfo.seatLists[i].micUserInfo.userId) {
            isSeat = true;
            continue;
          }
        }
        isAdmin = userInfo != null
            ? UserConstants.userginInfo!.info.userRole > UserRole.VIP.value ||
                AppManager().roomInfo.roomMode!.personalPermission < 4
            : false;
        isFooter = (widget.userId == UserConstants.userginInfo!.info.userId &&
                isSeat) ||
            (widget.userId != UserConstants.userginInfo!.info.userId &&
                isAdmin);
        setState(() {});
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      height: (isFooter ? 950.w : 830.w) + ScreenUtil().bottomBarHeight,
      child: Stack(
        children: [
          infoBody(),
          infoHeader(),
        ],
      ),
    );
  }

  Widget infoHeader() {
    return FContainer(
      padding: EdgeInsets.symmetric(horizontal: 40.w),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          GestureDetector(
            onTap: () {
              WidgetUtils().pushPage(PersonHomepage(userId: widget.userId));
            },
            child: FContainer(
              width: 180.w,
              height: 180.w,
              radius: BorderRadius.circular(50),
              color: Colors.white,
              imageFit: BoxFit.cover,
              image: userInfo != null
                  ? ImageUtils().getImageProvider(
                      NetConstants.ossPath + userInfo!.info.headIcon)
                  : null,
            ),
          ),
          Visibility(
            visible: widget.userId != UserConstants.userginInfo!.info.userId,
            child: FContainer(
              padding: EdgeInsets.only(top: 70.w),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  child(
                    Padding(
                      padding: EdgeInsets.only(right: 5.w),
                      child: FText("@", size: 26.sp, weight: FontWeight.w500),
                    ),
                    "ta",
                    atUser,
                  ),
                  SizedBox(width: 20.w),
                  child(
                    Icon(userInfo?.relationship != 0 ? Icons.check : Icons.add,
                        size: 34.sp, color: Colors.white),
                    userInfo?.relationship != 0 ? "已关注" : "关注",
                    followWithInterest,
                  ),
                  SizedBox(width: 10.w),
                  Visibility(
                    visible: isAdmin && userInfo?.roomInfo.roomRole != 1,
                    child: FContainer(
                      constraints: BoxConstraints(minWidth: 100.w),
                      child: PopupMenuButton(
                        icon: FText("更多", size: 28.sp),
                        onSelected: onUserOperate,
                        offset: const Offset(0, kToolbarHeight),
                        elevation: 0,
                        itemBuilder: (_) => <PopupMenuEntry<String>>[
                          PopupMenuItem<String>(
                            value: "踢出",
                            child: Center(
                              child: FText(
                                "踢出",
                                size: 28.sp,
                                weight: FontWeight.w500,
                                color: const Color(0xFF666666),
                              ),
                            ),
                          ),
                          PopupMenuDivider(height: 1.h),
                          PopupMenuItem<String>(
                            value: "拉黑",
                            child: Center(
                              child: FText(
                                "拉黑",
                                size: 28.sp,
                                weight: FontWeight.w500,
                                color: const Color(0xFF666666),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: reportUser,
                    child: Padding(
                      padding: EdgeInsets.only(left: 10.w),
                      child: LoadAssetImage("room/reportRoom", width: 50.w),
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget child(Widget icon, String text, void Function() onTap) {
    return GestureDetector(
      onTap: onTap,
      child: FContainer(
        padding: EdgeInsets.symmetric(vertical: 16.w, horizontal: 22.w),
        radius: BorderRadius.circular(50),
        color: Colors.white.withOpacity(0.1),
        child: Row(
          children: [icon, FText(text, size: 28.sp, weight: FontWeight.bold)],
        ),
      ),
    );
  }

  Widget infoBody() {
    return FContainer(
      margin: EdgeInsets.only(top: 70.w),
      padding: EdgeInsets.only(top: 110.w, left: 40.w, right: 40.w),
      width: double.infinity,
      height: (isFooter ? 920.w : 800.w) + ScreenUtil().bottomBarHeight,
      radius: BorderRadius.only(
          topLeft: Radius.circular(40.w), topRight: Radius.circular(40.w)),
      color: const Color(0xFF1D1928),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        SizedBox(height: 15.w),
        Row(
          children: [
            FText(
              userInfo != null ? userInfo!.info.name : "",
              size: 42.sp,
              weight: FontWeight.bold,
            ),
            SizedBox(width: 10.w),
            Visibility(
              visible: userInfo != null,
              child: GenderAge(
                userInfo != null ? userInfo!.info.sex == 1 : false,
                userInfo != null ? userInfo!.info.birthday : 0,
                width: 60.w,
                height: 50.w,
              ),
            ),
            const Spacer(),
            Visibility(
              visible:
                  UserConstants.userginInfo!.info.transferAccountPower == 1 &&
                      widget.userId != UserConstants.userginInfo!.info.userId,
              child: GestureDetector(
                onTap: () => WidgetUtils()
                    .showFDialog(SendHbao(userInfo!.info.userId, onSendGold)),
                child: LoadAssetImage("msgPrivateChat/hbao/hbaoEmo_1",
                    width: 60.w),
              ),
            )
          ],
        ),
        SizedBox(height: 15.w),
        Row(
          children: [
            FContainer(
              color: Colors.white.withOpacity(0.6),
              radius: BorderRadius.circular(50),
              child: LoadAssetImage("person/id", width: 42.w),
            ),
            SizedBox(width: 8.w),
            FText("${userInfo != null ? userInfo!.info.displayId : ""}",
                color: Colors.white.withOpacity(0.6)),
            GestureDetector(
              onTap: () {
                Clipboard.setData(ClipboardData(
                    text:
                        "${userInfo != null ? userInfo!.info.displayId : ""}"));
                WidgetUtils().showToast("ID复制成功");
              },
              child: LoadAssetImage(
                "person/rootzi",
                width: 42.w,
                color: Colors.white.withOpacity(0.6),
              ),
            ),
            FText("|", color: Colors.white.withOpacity(0.6)),
            SizedBox(width: 10.w),
            FText("被${userInfo != null ? userInfo!.fans : 0}人关注",
                color: Colors.white.withOpacity(0.6)),
          ],
        ),
        SizedBox(height: 15.w),
        Row(
          children: [
            ConfigManager().userIcon(
              AppManager().getUserRole(
                  userInfo != null ? userInfo!.info.userRole : 10000),
              paddingRight: 5.w,
            ),
            ConfigManager().roomIcon(
              AppManager().getRoomRole(
                  userInfo?.roomInfo != null ? userInfo!.roomInfo.roomRole : 4),
              paddingRight: 5.w,
            ),
            ConfigManager().userWealthCharmIcon(
              userInfo != null ? userInfo!.gradeInfo.userGrade.charm.badge : "",
              userInfo != null ? userInfo!.gradeInfo.userGrade.charm.grade : 0,
              marginRight: 5.w,
            ),
            ConfigManager().userWealthCharmIcon(
              userInfo != null
                  ? userInfo!.gradeInfo.userGrade.wealth.badge
                  : "",
              userInfo != null ? userInfo!.gradeInfo.userGrade.wealth.grade : 0,
              isWealth: true,
              marginRight: 5.w,
            ),
          ],
        ),
        SizedBox(height: 20.w),
        Stack(
          children: [
            LoadAssetImage(
              "person/pusts",
              width: 30.w,
              color: Colors.white,
            ),
            FContainer(
              padding: EdgeInsets.only(top: 15.w),
              height: 140.w,
              child: ListView(
                children: [
                  FText(
                    userInfo != null ? userInfo!.info.sign : "这个人很懒,没有签名...",
                    maxLines: 100,
                    size: 28.sp,
                  )
                ],
              ),
            )
          ],
        ),
        SizedBox(height: 20.w),
        GestureDetector(
          onTap: () {
            WidgetUtils().pushPage(HomepageGifts(
              UserConstants.userginInfo!.info.userId,
              UserConstants.userginInfo!.info.headIcon,
              UserConstants.userginInfo!.info.name,
            ));
          },
          child: FContainer(
            width: double.infinity,
            padding: EdgeInsets.symmetric(horizontal: 30.w, vertical: 20.w),
            radius: BorderRadius.circular(20.w),
            color: Colors.white.withOpacity(0.1),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text.rich(
                  TextSpan(
                    style: TextStyle(
                      fontSize: 33.sp,
                    ),
                    children: [
                      const TextSpan(
                          text: "本周礼物: ",
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w500)),
                      TextSpan(
                          text:
                              "收到${userInfo?.giftInfo.giftCount != null ? userInfo!.giftInfo.giftCount : 0}款",
                          style: TextStyle(
                            color: Colors.white.withOpacity(0.4),
                            fontWeight: FontWeight.w500,
                          )),
                    ],
                  ),
                ),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SizedBox(height: 90.w),
                    ...giftList(userInfo?.giftInfo.giftList != null
                        ? userInfo!.giftInfo.giftList
                        : []),
                    LoadAssetImage(
                      "public/RightNavigation",
                      width: 20.w,
                      color: Colors.white.withOpacity(0.6),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        SizedBox(height: 40.w),
        Visibility(
            visible:
                !(widget.userId == UserConstants.userginInfo!.info.userId &&
                    UserConstants.userginInfo?.info.teenmode == 1),
            child: Row(
              mainAxisAlignment:
                  widget.userId == UserConstants.userginInfo!.info.userId ||
                          UserConstants.userginInfo?.info.teenmode == 1
                      ? MainAxisAlignment.center
                      : MainAxisAlignment.spaceBetween,
              children: [
                Visibility(
                    visible:
                        widget.userId != UserConstants.userginInfo!.info.userId,
                    child: buttons(false)),
                Visibility(
                    visible: !(UserConstants.userginInfo?.info.teenmode == 1),
                    child: buttons(true)),
              ],
            )),
        Visibility(visible: isFooter, child: footerBtns()),
      ]),
    );
  }

  // 收礼列表
  List<Widget> giftList(List<GiftInfoList> list) {
    int length = list.length >= 3 ? 3 : list.length;
    List<Widget> childList = [];
    for (var i = 0; i < length; i++) {
      childList.add(FContainer(
        margin: EdgeInsets.only(right: 10.w),
        radius: BorderRadius.circular(10.w),
        width: 90.w,
        height: 90.w,
        image: ImageUtils()
            .getImageProvider(NetConstants.ossPath + list[i].giftPicture),
        imageFit: BoxFit.cover,
      ));
    }
    return childList;
  }

  // 打招呼/收礼物
  Widget buttons(bool isTheme) {
    return FButton(
      onTap: !isTheme ? sayHello : givingGifts,
      isTheme: isTheme,
      bgColor: isTheme ? null : const Color(0xFFFF753F),
      width: 320.w,
      height: 80.w,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          LoadAssetImage(
            isTheme ? "room/sendGift" : "room/sayHello",
            width: 40.w,
            height: 40.w,
          ),
          SizedBox(width: 10.w),
          FText(
            isTheme ? "送礼物" : "打招呼",
            size: 28.sp,
            weight: FontWeight.w500,
          )
        ],
      ),
    );
  }

  // 底部按钮列表
  Widget footerBtns() {
    return FContainer(
      margin: EdgeInsets.only(top: 40.w),
      padding: EdgeInsets.symmetric(vertical: 28.w),
      border: Border(top: BorderSide(color: Colors.white.withOpacity(0.15))),
      child: Row(
        children: footerBtn(),
      ),
    );
  }

  // 底部按钮
  List<Widget> footerBtn() {
    List<Widget> rows = [];
    if (widget.userId == UserConstants.userginInfo!.info.userId) {
      if (isAdmin) {
        if (userInfo!.roomInfo.roomRole != RoomRole.OWNER.value) {
          rows.addAll([
            footerItem("下麦", () => downWheat(true)),
            FContainer(
              height: 50.w,
              width: 3.w,
              color: Colors.white.withOpacity(0.15),
            )
          ]);
        }
        rows.add(footerItem("清空", clearToLove));
      } else if (userInfo?.roomInfo.roomRole != RoomRole.OWNER.value) {
        rows.addAll([
          footerItem("下麦旁听", () => downWheat(true), size: 36.sp),
        ]);
      }
    } else if (isAdmin) {
      if (!isSeat) {
        rows.addAll([
          footerItem("抱上麦", upWheat),
          FContainer(
            height: 50.w,
            width: 3.w,
            color: Colors.white.withOpacity(0.15),
          )
        ]);
      } else if (userInfo?.roomInfo.roomRole != RoomRole.OWNER.value) {
        rows.addAll([
          footerItem("抱下麦", () => downWheat(false)),
          FContainer(
            height: 50.w,
            width: 3.w,
            color: Colors.white.withOpacity(0.15),
          )
        ]);
      }
      if (isSeat) {
        rows.add(footerItem("清空", clearToLove));
      } else {
        rows.addAll([
          userInfo!.roomInfo.isBanChat == 2
              ? footerItem("禁言", () => noSpeakingChat(true, false))
              : footerItem("取消禁言", () => noSpeakingChat(true, true)),
        ]);
      }
    }
    return rows;
  }

  // 底部按钮
  Widget footerItem(String text, void Function() onTap, {double? size}) {
    return Expanded(
        child: GestureDetector(
      onTap: onTap,
      child: Center(
        child: FText(text, size: size ?? 32.sp),
      ),
    ));
  }

  // @用户
  void atUser() {
    WidgetUtils().popPage();
    WidgetUtils().showBottomSheet(
      RoomInput(
        onTapSend: onTapSend,
        initText: "@ ${userInfo!.info.name}",
      ),
      enableDrag: false,
    );
  }

  // 发送群消息
  void onTapSend(String inputText) async {
    if (AppManager().roomInfo.roomMode!.isBanMic == 1) {
      return WidgetUtils().showToast("您已被禁止发言");
    }
    if (inputText.trimRight().isEmpty) {
      return WidgetUtils().showToast("输入内容不能为空");
    }

    inputText = inputText.replaceAll("\n", "");
    await AppManager().sendRoomMsg(inputText.trimRight());
  }

  // 关注/取消关注
  void followWithInterest() async {
    await DioUtils().httpRequest(
      NetConstants.followUser,
      method: DioMethod.POST,
      loading: false,
      params: {
        "flow_user_id": widget.userId,
        "flow_action": userInfo!.relationship == 0 ? 10 : 20,
      },
      onSuccess: (res) {
        initUser();
      },
    );
  }

  // 抱上麦
  void upWheat() async {
    AppManager().upWheat(widget.micNum ?? -1, id: userInfo!.info.displayId,
        onSuccess: (res) {
      popPage();
    });
  }

  // 抱下麦
  void downWheat(bool isMy) async {
    AppManager().downWheat(
        id: isMy ? null : userInfo!.info.displayId,
        onSuccess: (res) {
          popPage();
        });
  }

  // 举报用户
  void reportUser() {
    WidgetUtils().pushPage(ReportType(widget.userId, isRoom: true));
  }

  // 禁止发言
  void noSpeakingChat(bool isChat, bool isOpen) async {
    await DioUtils().httpRequest(
      NetConstants.roomBanUser,
      method: DioMethod.POST,
      loading: false,
      params: {
        "exterior_room_id": AppManager().roomInfo.roomMode!.exteriorRoomId,
        "exterior_user_id": userInfo!.info.displayId,
        (isChat ? "is_ban_chat" : "is_ban_mic"): isOpen ? 2 : 1,
      },
      onSuccess: (res) {
        popPage();
      },
    );
  }

  // 清空心动值
  void clearToLove() async {
    await DioUtils().httpRequest(
      NetConstants.seatList,
      loading: false,
      params: {
        "room_id": AppManager().roomInfo.roomMode!.roomId,
        "room_mic_num": widget.micNum
      },
      onSuccess: (res) {
        WidgetUtils().popPage();
      },
    );
  }

  // 对用户操作
  void onUserOperate(String value) {
    switch (value) {
      case "踢出":
        outRoom();
        break;
      default:
        blockUser();
    }
  }

  // 踢出房间
  void outRoom() async {
    bool? accept = await WidgetUtils().showAlert("确定要将该用户踢出房间吗？");
    if (accept != true) return;
    await DioUtils().httpRequest(
      NetConstants.outRoom,
      method: DioMethod.POST,
      loading: false,
      params: {
        "exterior_room_id": AppManager().roomInfo.roomMode!.exteriorRoomId,
        "exterior_user_id": userInfo!.info.displayId,
      },
      onSuccess: (res) async {
        WidgetUtils().popPage();
      },
    );
  }

  // 拉黑用户
  void blockUser() async {
    bool? accept = await WidgetUtils().showAlert("确定要拉黑该用户吗？");
    if (accept != true) return;
    await DioUtils().httpRequest(
      NetConstants.roomBlockUser,
      method: DioMethod.POST,
      loading: false,
      params: {
        "room_id": AppManager().roomInfo.roomMode!.roomId,
        "user_id": userInfo!.info.userId,
      },
      onSuccess: (res) async {
        WidgetUtils().popPage();
      },
    );
  }

  // 聊天
  void sayHello() {
    if (userInfo == null) return;
    HxMsgExt msgExt = HxMsgExt(
        userInfo!.info.userId, userInfo!.info.headIcon, userInfo!.info.name);
    WidgetUtils().pushPage(MsgChat(
      userInfo!.info.hxInfo.uid,
      msgExt,
      isRoom: true,
    ));
  }

  // 送礼
  void givingGifts() async {
    await WidgetUtils().showBottomSheet(
      GiveDialog(
        true,
        isSeat: isSeat,
        userId: userInfo!.info.userId,
      ),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
        topRight: Radius.circular(10.w),
        topLeft: Radius.circular(10.w),
      )),
      color: Colors.transparent,
    );
  }

  void popPage() {
    for (var i = 0; i < widget.popNums; i++) {
      WidgetUtils().popPage();
    }
  }

  // 赠送金币
  void onSendGold(int gold) async {
    HxMsgExt msgExt = HxMsgExt(
        userInfo!.info.userId, userInfo!.info.headIcon, userInfo!.info.name);
    HXHbaoMsg msgBody = HXHbaoMsg(gold);
    await HXManager().sendHxChatMsg(userInfo!.info.hxInfo.uid, msgExt, msgBody);
  }
}
