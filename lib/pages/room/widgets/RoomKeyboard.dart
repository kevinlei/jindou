import 'dart:async';

import 'package:echo/import.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class RoomKeyboard extends StatefulWidget {
  const RoomKeyboard({Key? key}) : super(key: key);

  @override
  _RoomKeyboardState createState() => _RoomKeyboardState();
}

class _RoomKeyboardState extends State<RoomKeyboard> {
  OverlayEntry? inputField;

  // 是否有新消息提示
  int hasUnreadMsg = 0;

  // 是否在麦上
  late bool isMaishang;

  // 是否开麦;
  late bool isMico;

  // 是否排麦模式
  bool isMicApply = false;

  // 节流
  bool isOpen = false;

  @override
  void initState() {
    super.initState();
    EventUtils().on(
      EventConstants.E_MIC_OPEN,
      EventCall("RoomKeyboard", (v) {
        isMaishang = AppManager().roomInfo.isMaishang;
        isMico = AppManager().roomInfo.isMico;
        if (!mounted) return;
        setState(() {});
      }),
    );
    EventUtils().on(
      EventConstants.E_MIC_APPLY_OPEN,
      EventCall("RoomKeyboard", (v) {
        if (!mounted) return;
        isMicApply = v;
        setState(() {});
      }),
    );
    EventUtils().on(
      EventConstants.E_MSG_UPDATE,
      EventCall("RoomKeyboard", (v) {
        Future.delayed(const Duration(milliseconds: 200), () async {
          hasUnreadMsg = AppManager().unread;
          if (!mounted) return;
          setState(() {});
        });
      }),
    );

    isMaishang = AppManager().roomInfo.isMaishang;
    isMico = AppManager().roomInfo.isMico;
    hasUnreadMsg = AppManager().unread;
  }

  @override
  void dispose() {
    EventUtils()
        .off(EventConstants.E_MIC_OPEN, EventCall("RoomKeyboard", (v) {}));
    EventUtils().off(
        EventConstants.E_MIC_APPLY_OPEN, EventCall("RoomKeyboard", (v) {}));
    EventUtils()
        .off(EventConstants.E_MSG_UPDATE, EventCall("RoomKeyboard", (v) {}));
    inputField?.remove();
    inputField = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<RoomProvider>(builder: (_, pro, __) {
      return FContainer(
        padding: EdgeInsets.fromLTRB(20.w, 10.w, 20.w, 20.w),
        child: operateContent(pro),
      );
    });
  }

  Widget operateContent(RoomProvider pro) {
    return Row(children: [
      Expanded(child: inputButton()),
      mySeat(pro),
      microButton(pro),
      Visibility(
          visible: !(UserConstants.userginInfo?.info.teenmode == 1),
          child: giftButton()),
      msgButton(),
      moreButton(),
    ]);
  }

  Widget inputButton() {
    Widget input = FContainer(
      height: 70.w,
      width: 200.w,
      margin: EdgeInsets.only(right: 15.w),
      padding: EdgeInsets.symmetric(horizontal: 10.w),
      color: Colors.black.withOpacity(0.3),
      radius: BorderRadius.circular(70.w),
      align: Alignment.centerLeft,
      child: Row(children: [
        FText("说点什么...", color: Colors.white.withOpacity(0.4), size: 24.sp),
        const Spacer(),
        GestureDetector(
          onTap: onBnInput,
          child: LoadAssetImage("room/em", height: 40.w),
        ),
      ]),
    );
    return GestureDetector(onTap: onPressInput, child: input);
  }

  //图标
  void onBnInput() {
    WidgetUtils().showBottomSheet(RoomsEmo(), enableDrag: false);
  }

  Widget mySeat(RoomProvider pro) {
    bool isApply = (pro.roomModel?.personalPermission ?? 4) < 4 &&
        pro.roomModel?.isMicApply == 2;

    Widget child = FContainer(
      height: 70.w,
      radius: BorderRadius.circular(60.w),
      margin: EdgeInsets.only(right: 15.w),
      padding: EdgeInsets.symmetric(horizontal: 20.w),
      color: Colors.black.withOpacity(0.3),
      align: Alignment.center,
      child: FText(
        isApply
            ? "排麦"
            : !isMaishang
                ? pro.roomModel?.isMicApply == 2
                    ? "排麦"
                    : "上麦"
                : "下麦",
        weight: FontWeight.w500,
        wordSpacing: 1.2,
      ),
    );

    return Visibility(
        visible: !(!isApply &&
            (AppManager().roomInfo.roomMode?.personalPermission ?? 4) ==
                RoomRole.OWNER.value),
        child: GestureDetector(
          onTap: isApply
              ? onTapBarley
              : !isMaishang
                  ? pro.roomModel?.isMicApply == 2
                      ? onTapBarley
                      : onTapUpDownSeat
                  : onTapUpDownSeat,
          child: child,
        ));
  }

  Widget giftButton() {
    Widget btn = FContainer(
      width: 70.w,
      height: 70.w,
      radius: BorderRadius.circular(70.w),
      margin: EdgeInsets.only(right: 15.w),
      image: ImageUtils().getAssetsImage("room/gift"),
    );

    return GestureDetector(onTap: onTapGift, child: btn);
  }

  Widget microButton(RoomProvider pro) {
    String image = isMico ? "room/micro1" : "room/micro2";
    Widget btn = FContainer(
      width: 70.w,
      height: 70.w,
      radius: BorderRadius.circular(70.w),
      margin: EdgeInsets.only(right: 15.w),
      color: Colors.black.withOpacity(0.3),
      image: ImageUtils().getAssetsImage(image),
    );

    return Visibility(
      visible: AppManager().roomInfo.isMaishang,
      child: GestureDetector(
        onTap: () => microphone(pro),
        child: btn,
      ),
    );
  }

  Widget msgButton() {
    late Widget btn;
    if (hasUnreadMsg == 0) {
      btn = FContainer(
        width: 70.w,
        height: 70.w,
        radius: BorderRadius.circular(70.w),
        margin: EdgeInsets.only(right: 15.w),
        color: Colors.black.withOpacity(0.3),
        image: ImageUtils().getAssetsImage("room/msg"),
      );
    } else {
      btn = Stack(
        clipBehavior: Clip.none,
        children: [
          FContainer(
            width: 70.w,
            height: 70.w,
            radius: BorderRadius.circular(70.w),
            margin: EdgeInsets.only(right: 15.w),
            color: Colors.black.withOpacity(0.3),
            image: ImageUtils().getAssetsImage("room/msg"),
          ),
          Positioned(
              top: -6.w,
              left: 38.w,
              child: FContainer(
                radius: BorderRadius.circular(50),
                padding: EdgeInsets.symmetric(vertical: 2.w, horizontal: 6.w),
                color: Colors.red,
                align: Alignment.center,
                child: FText(
                  hasUnreadMsg > 99 ? "99+" : hasUnreadMsg.toString(),
                  size: 16.sp,
                  weight: FontWeight.bold,
                ),
              ))
        ],
      );
    }
    return GestureDetector(onTap: onTapMsg, child: btn);
  }

  Widget moreButton() {
    Widget btn = FContainer(
      width: 70.w,
      height: 70.w,
      radius: BorderRadius.circular(70.w),
      color: Colors.black.withOpacity(0.3),
      image: ImageUtils().getAssetsImage("room/set"),
    );
    return GestureDetector(onTap: onTapMore, child: btn);
  }

  // 输入群消息
  void onPressInput() {
    int? isShut = AppManager().roomInfo.roomMode?.isBanChat;
    if (isShut == 1) return WidgetUtils().showToast("您已被禁言");
    WidgetUtils()
        .showBottomSheet(RoomInput(onTapSend: onTapSend), enableDrag: false);
  }

  // 上下麦
  void onTapUpDownSeat() {
    if (isOpen) return;
    isOpen = true;
    Future.delayed(const Duration(milliseconds: 300), () async {
      isOpen = false;
    });
    if (!isMaishang) {
      AppManager().upWheat(
        -1,
        onSuccess: (res) {
          isMaishang = true;
          setState(() {});
        },
      );
    } else {
      AppManager().downWheat(
        onSuccess: (res) {
          isMaishang = false;
          setState(() {});
        },
      );
    }
  }

  // 排麦
  void onTapBarley() {
    WidgetUtils().showBottomSheet(
      const RoomBarley(),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(20.w),
          topLeft: Radius.circular(20.w),
        ),
      ),
    );
  }

  // 开关麦
  void microphone(RoomProvider pro) async {
    if (pro.roomModel!.isBanMic == 1) {
      return WidgetUtils().showToast("您已被禁麦");
    }
    bool accoct = await AppManager().microphone(!isMico);
    if (!accoct) return;
    setState(() {
      isMico = !isMico;
    });
  }

  // 送礼
  void onTapGift() {
    WidgetUtils().showBottomSheet(
      const GiveDialog(true),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
        topRight: Radius.circular(10.w),
        topLeft: Radius.circular(10.w),
      )),
      color: Colors.transparent,
    );
  }

  // 私信消息
  void onTapMsg() async {
    AppManager().refreshUnread();
    await WidgetUtils().showBottomSheet(
        const MsgPrivateChat(
          isRoom: true,
        ),
        color: Colors.transparent);
    // getUnreadMsg();
  }

  // 房间工具
  void onTapMore() {
    WidgetUtils()
        .showBottomSheet(const RoomMore(), color: const Color(0xFF1D1928));
  }

  // 发送群消息
  void onTapSend(String inputText) async {
    if (AppManager().roomInfo.roomMode!.isBanMic == 1) {
      return WidgetUtils().showToast("您已被禁止发言");
    }
    if (inputText.trimRight().isEmpty) {
      return WidgetUtils().showToast("输入内容不能为空");
    }

    inputText = inputText.replaceAll("\n", "");
    await AppManager().sendRoomMsg(inputText.trimRight());
  }
}

// 输入群消息
class RoomInput extends StatefulWidget {
  final Function(String) onTapSend;
  final String initText;
  const RoomInput({Key? key, required this.onTapSend, this.initText = ""})
      : super(key: key);

  @override
  _RoomInputState createState() => _RoomInputState();
}

class _RoomInputState extends State<RoomInput> {
  TextEditingController controller = TextEditingController();
  FocusNode focusNode = FocusNode();

  // 是否打开键盘
  bool isEmo = false;

  // 节流
  Timer? timer;
  bool sendOpen = false;

  @override
  void initState() {
    super.initState();
    if (widget.initText.isNotEmpty) {
      controller.text = widget.initText;
    }
  }

  @override
  void dispose() {
    controller.dispose();
    focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return RawKeyboardListener(
      focusNode: FocusNode(),
      onKey: (event) {
        if (event.runtimeType == RawKeyDownEvent) {
          if (event.logicalKey.keyId == 4294967309) {
            onTapSend();
          }
        }
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        mainAxisSize: MainAxisSize.min,
        children: [
          FContainer(
            color: const Color(0xFF0B0C17),
            padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 10.w),
            child: inputField(),
          ),
          operationPanel(),
        ],
      ),
    );
  }

  Widget inputField() {
    InputBorder border = OutlineInputBorder(
      borderSide: const BorderSide(color: Colors.transparent),
      borderRadius: BorderRadius.circular(20.w),
      gapPadding: 0,
    );

    Widget child = TextField(
      maxLines: null,
      autofocus: true,
      controller: controller,
      focusNode: focusNode,
      inputFormatters: [FilteringTextInputFormatter.deny(RegExp(r"\n"))],
      onTap: () {
        if (isEmo) {
          setState(() {
            isEmo = false;
          });
        }
      },
      style: TextStyle(color: Colors.white, fontSize: 28.h),
      decoration: InputDecoration(
          isDense: true,
          contentPadding:
              EdgeInsets.symmetric(vertical: 14.h, horizontal: 20.w),
          border: InputBorder.none,
          focusedBorder: border,
          enabledBorder: border,
          filled: true,
          fillColor: const Color(0xFFFFFFFF).withOpacity(0.05),
          counterText: "",
          hintText: "",
          hintStyle: TextStyle(color: const Color(0xFFBBBBBB), fontSize: 28.h),
          suffix: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              GestureDetector(
                  onTap: onTapEmo,
                  child: LoadAssetImage(
                    "room/em",
                    width: 45.w,
                    color: Colors.white,
                  )),
              SizedBox(width: 10.w),
              buttonSend()
            ],
          )),
    );
    return SizedBox(height: 70.w, child: child);
  }

  Widget buttonSend() {
    Widget btn = FButton(
      width: 105.w,
      height: 50.w,
      padding: EdgeInsets.symmetric(vertical: 0, horizontal: 20.w),
      radius: BorderRadius.circular(50),
      isTheme: true,
      align: Alignment.center,
      child: FText(
        "发送",
        size: 26.sp,
      ),
    );
    return GestureDetector(onTap: onTapSend, child: btn);
  }

  Widget operationPanel() {
    if (isEmo) {
      return SizedBox(
        height: 500.w,
        child: EmotionFooter(
          controller,
          color: const Color(0xFF34303D),
        ),
      );
    } else {
      return SizedBox(height: MediaQuery.of(context).viewInsets.bottom);
    }
  }

  void onTapEmo() {
    if (!isEmo) {
      SystemChannels.textInput.invokeMethod('TextInput.hide');
      isEmo = true;
      setState(() {});
    }
  }

  void onTapSend() async {
    if (sendOpen || timer != null) return;
    sendOpen = true;
    timer = Timer(const Duration(seconds: 1), () {
      sendOpen = false;
      timer = null;
    });

    String text = controller.text;
    if (text.isEmpty) return WidgetUtils().showToast("请输入要发送的内容");

    String atText = widget.initText;
    if (atText.isNotEmpty && text.startsWith(atText)) {
      text = text.substring(atText.length);
      text = "#[$atText]#$text";
    }

    controller.clear();
    widget.onTapSend(text);
    WidgetUtils().popPage();
  }
}

// 排麦模式
class RoomBarley extends StatefulWidget {
  const RoomBarley({super.key});

  @override
  State<RoomBarley> createState() => _RoomBarleyState();
}

class _RoomBarleyState extends State<RoomBarley> {
  // 排麦列表
  List barleyList = [];

  // 是否申请排麦中
  int isApply = 0;

  @override
  void initState() {
    super.initState();
    initBarleyList();
  }

  void initBarleyList() async {
    await DioUtils().httpRequest(
      NetConstants.barleyList,
      loading: false,
      params: {
        "exterior_room_id": AppManager().roomInfo.roomMode!.exteriorRoomId,
      },
      onSuccess: (res) {
        if (res != null) {
          debugPrint("排麦列表${res["applys"]}");
          setState(() {
            barleyList = res["applys"] ?? [];
            isApply = res["is_apply"];
          });
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      padding: EdgeInsets.only(bottom: ScreenUtil().bottomBarHeight + 40.w),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          FContainer(
            padding: EdgeInsets.symmetric(vertical: 20.w),
            width: double.infinity,
            border: Border(
                bottom: BorderSide(color: Colors.white.withOpacity(0.1))),
            align: Alignment.center,
            child: FText("等待连麦人数：${barleyList.length}", size: 34.sp),
          ),
          SizedBox(
            height: 600.w,
            child: ListView.builder(
              padding: EdgeInsets.symmetric(vertical: 0, horizontal: 40.w),
              itemBuilder: (_, index) => barleyItem(index),
              itemCount: barleyList.length,
            ),
          ),
          SizedBox(height: 20.w),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 40.w),
            child: btns(),
          ),
        ],
      ),
    );
  }

  Widget barleyItem(int index) {
    return FContainer(
      padding: EdgeInsets.symmetric(vertical: 25.w),
      border: Border(
        bottom: BorderSide(
          color: index + 1 == 8
              ? Colors.transparent
              : Colors.white.withOpacity(0.1),
        ),
      ),
      child: Row(
        children: [
          FText("${index + 1}"),
          SizedBox(width: 20.w),
          FContainer(
            width: 80.w,
            height: 80.w,
            radius: BorderRadius.circular(50),
            imageFit: BoxFit.cover,
            image: ImageUtils().getImageProvider(
                NetConstants.ossPath + barleyList[index]["user_image"]),
          ),
          SizedBox(width: 10.w),
          FText(barleyList[index]["user_name"]),
          const Spacer(),
          Visibility(
              visible: AppManager().roomInfo.roomMode!.personalPermission < 4,
              child: manageOperate(index)),
        ],
      ),
    );
  }

  Widget manageOperate(int index) {
    // {id: 1, ex_user_id: 70634664, user_name: 温柔的小卡拉米4646,
    // user_image: head_icon/xoxbAqu3ye79W3I15RqbWjuxArLt7fVM.png, mic_num: 2, apply_time: 1689407012432}
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        FButton(
          onTap: () => handleApplyMic(false, barleyList[index]["id"]),
          border: Border.all(color: const Color(0xFFFF753F)),
          child: const FText(
            "拒绝",
            color: Color(0xFFFF753F),
          ),
        ),
        SizedBox(width: 20.w),
        FButton(
          onTap: () => handleApplyMic(true, barleyList[index]["id"]),
          border: Border.all(color: Colors.transparent),
          bgColor: const Color(0xFFFF753F),
          child: const FText("同意"),
        ),
      ],
    );
  }

  Widget btns() {
    if (AppManager().roomInfo.roomMode!.personalPermission < 4) {
      return Row(
        children: [
          btnItem(false, "一键移除", () => allHandleApplyMic(false)),
          SizedBox(width: 20.w),
          btnItem(true, "一键同意", () => allHandleApplyMic(true)),
        ],
      );
    }
    return Row(
      children: [
        isApply == 1
            ? btnItem(false, "取消排麦", cellApplyMic)
            : btnItem(true, "申请上麦", applyMic),
      ],
    );
  }

  Widget btnItem(bool isBg, String text, void Function() onTap) {
    return Expanded(
        child: FButton(
      onTap: onTap,
      height: 80.w,
      bgColor: isBg ? const Color(0xFFFF753F) : Colors.transparent,
      border: Border.all(
          color: !isBg ? const Color(0xFFFF753F) : Colors.transparent),
      align: Alignment.center,
      child: FText(
        text,
        size: 28.sp,
        color: Color(isBg ? 0xFFFFFFFF : 0xFFFF753F),
      ),
    ));
  }

  // 申请上麦
  void applyMic() async {
    await DioUtils().httpRequest(
      NetConstants.applyBarley,
      method: DioMethod.POST,
      loading: false,
      params: {
        "exterior_room_id": AppManager().roomInfo.roomMode!.exteriorRoomId,
        "room_mic_num": -1
      },
      onSuccess: (res) {
        initBarleyList();
      },
    );
  }

  // 取消申请
  void cellApplyMic() async {
    await DioUtils().httpRequest(
      NetConstants.cellBarley,
      method: DioMethod.POST,
      loading: false,
      params: {
        "exterior_room_id": AppManager().roomInfo.roomMode!.exteriorRoomId,
      },
      onSuccess: (res) {
        initBarleyList();
      },
    );
  }

  // 同意/拒绝排麦申请
  void handleApplyMic(bool isagree, int id) async {
    await DioUtils().httpRequest(
      NetConstants.acceptBarley,
      method: DioMethod.POST,
      loading: false,
      params: {
        "exterior_room_id": AppManager().roomInfo.roomMode!.exteriorRoomId,
        "apply_id": id,
        "apply_state": isagree ? 3 : 2,
      },
      onSuccess: (res) {
        initBarleyList();
      },
    );
  }

  // 一键同意/拒绝排麦申请
  void allHandleApplyMic(bool isagree) async {
    await DioUtils().httpRequest(
      NetConstants.allBarley,
      method: DioMethod.POST,
      loading: false,
      params: {
        "exterior_room_id": AppManager().roomInfo.roomMode!.exteriorRoomId,
        "apply_state": isagree ? 3 : 2
      },
      onSuccess: (res) {
        initBarleyList();
      },
    );
  }
}
