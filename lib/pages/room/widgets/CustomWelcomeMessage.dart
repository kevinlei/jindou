import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class CustomWelcomeMessage extends StatefulWidget {
  const CustomWelcomeMessage({super.key});

  @override
  State<CustomWelcomeMessage> createState() => _CustomWelcomeMessageState();
}

class _CustomWelcomeMessageState extends State<CustomWelcomeMessage> {
  @override
  Widget build(BuildContext context) {
    return FContainer(
      padding: EdgeInsets.only(top: 20.w),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          FContainer(
            width: double.infinity,
            padding: EdgeInsets.symmetric(vertical: 20.w),
            align: Alignment.center,
            border: Border(
                bottom: BorderSide(color: Colors.white.withOpacity(0.08))),
            child: FText("自定义欢迎语", size: 34.sp),
          ),
          SizedBox(height: 10.w),
          SizedBox(
            height: 500.w,
            child: ListView.builder(
              padding: EdgeInsets.symmetric(horizontal: 40.w),
              itemBuilder: (context, index) => welcomMessageItem(index),
              itemCount: 10,
            ),
          ),
          newBtn(),
        ],
      ),
    );
  }

  Widget welcomMessageItem(int index) {
    return FContainer(
      padding: EdgeInsets.symmetric(vertical: 20.w),
      border: Border(bottom: BorderSide(color: Colors.white.withOpacity(0.08))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          FText(
            "被你的声音苏到了!",
            size: 28.sp,
            // color: index == 0 ? const Color(0xFFFF753F) : Colors.white,
          ),
          GestureDetector(
            child: LoadAssetImage(
              "room/welcomeMessageDelete",
              width: 45.w,
            ),
          )
        ],
      ),
    );
  }

  Widget newBtn() {
    return FContainer(
      padding: EdgeInsets.only(
          top: 20.w,
          left: 20.w,
          right: 20.w,
          bottom: ScreenUtil().bottomBarHeight + 60.w),
      child: FButton(
        onTap: () async {
          await WidgetUtils().showBottomSheet(
            const WelcomMessage(),
            color: const Color(0xFF1D1928),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(20.w),
                topLeft: Radius.circular(20.w),
              ),
            ),
          );
        },
        width: double.infinity,
        height: 100.w,
        radius: BorderRadius.circular(50),
        bgColor: const Color(0xFFFF753F),
        align: Alignment.center,
        child: FText(
          "新增",
          size: 26.sp,
        ),
      ),
    );
  }
}

class WelcomMessage extends StatefulWidget {
  const WelcomMessage({super.key});

  @override
  State<WelcomMessage> createState() => _WelcomMessageState();
}

class _WelcomMessageState extends State<WelcomMessage> {
  String textValue = "";
  @override
  Widget build(BuildContext context) {
    InputBorder border = OutlineInputBorder(
        borderRadius: BorderRadius.circular(20.w),
        borderSide: const BorderSide(color: Colors.transparent));

    return FContainer(
      padding: EdgeInsets.only(
          top: 40.w,
          left: 40.w,
          right: 40.w,
          bottom: ScreenUtil().bottomBarHeight + 40.w),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          FText("自定义欢迎语", size: 34.sp),
          SizedBox(height: 40.w),
          TextField(
            style: TextStyle(
                color: Colors.white, fontSize: 30.sp, letterSpacing: 1.2),
            maxLength: 30,
            maxLines: 10,
            autofocus: true,
            onChanged: (value) {
              if (textValue.isEmpty || value.isEmpty) {
                setState(() {
                  textValue = value;
                });
              }
            },
            decoration: InputDecoration(
              enabledBorder: border,
              focusedBorder: border,
              disabledBorder: border,
              filled: true,
              fillColor: Colors.white.withOpacity(0.1),
              counterStyle:
                  TextStyle(color: const Color(0xFFBBBBBB), fontSize: 30.sp),
              hintText: "请输入您的欢迎语~",
              contentPadding: const EdgeInsets.fromLTRB(12, 8, 12, 8),
              hintStyle: TextStyle(
                color: const Color(0xFFBBBBBB),
                fontSize: 30.sp,
                letterSpacing: 1,
              ),
            ),
          ),
          SizedBox(height: 40.w),
          FButton(
            onTap: () {
              if (textValue.isEmpty) {
                return WidgetUtils().showToast("请输入您的欢迎语");
              }
              WidgetUtils().popPage();
            },
            bgColor: const Color(0xFFFF753F),
            width: double.infinity,
            height: 90.w,
            align: Alignment.center,
            child: FText("保存", size: 30.sp),
          ),
          SizedBox(height: MediaQuery.of(context).viewInsets.bottom),
        ],
      ),
    );
  }
}
