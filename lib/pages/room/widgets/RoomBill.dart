import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class RoomBill extends StatefulWidget {
  @override
  _RoomBillState createState() => _RoomBillState();
}

class _RoomBillState extends State<RoomBill> {
  DateTime dateS =
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day);
  DateTime dateE = DateTime.now();

  List billData = [];
  int all = 0;

  @override
  void initState() {
    super.initState();
    requestBill();
  }

  void initTime() {
    dateS =
        DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day);
    dateE = DateTime.now();
    if (!mounted) return;
    requestBill();
    setState(() {});
  }

  // 查询流水
  void requestBill() async {
    int roomId = AppManager().roomInfo.roomMode!.exteriorRoomId;
    await DioUtils().httpRequest(
      NetConstants.roomBill,
      method: DioMethod.POST,
      loading: false,
      params: {
        "exterior_room_id": roomId,
        "start_time": dateS.millisecondsSinceEpoch,
        "end_time": dateE.millisecondsSinceEpoch
      },
      onSuccess: onGetBill,
    );
  }

  void onGetBill(response) {
    billData = response?["mic_gold_info"] ?? [];
    all = response?["room_gold_summary"] ?? 0;
    if (!mounted) return;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: FContainer(
        width: 640.w,
        radius: BorderRadius.circular(28.w),
        color: Colors.white,
        padding: EdgeInsets.symmetric(horizontal: 50.w, vertical: 40.w),
        child: Column(mainAxisSize: MainAxisSize.min, children: [
          FText("麦位流水",
              color: Colors.black, size: 36.sp, weight: FontWeight.bold),
          SizedBox(height: 25.w),
          timeTextContent(true),
          SizedBox(height: 20.w),
          timeTextContent(false),
          SizedBox(height: 20.w),
          FText("总流水: $all",
              color: const Color(0xFFFF753F),
              size: 28.sp,
              weight: FontWeight.bold),
          SizedBox(height: 20.w),
          pitBillList(),
        ]),
      ),
    );
  }

  Widget timeTextContent(bool isFirst) {
    String timeText = "";
    if (isFirst) {
      timeText = dateS.toString().substring(0, 19);
    } else {
      timeText = dateE.toString().substring(0, 19);
    }

    return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
      FText(
        isFirst ? "开始时间" : "结束时间",
        size: 26.sp,
        color: Colors.black,
        weight: FontWeight.w500,
      ),
      FContainer(
        padding: EdgeInsets.symmetric(vertical: 8.w, horizontal: 15.w),
        radius: BorderRadius.circular(10.w),
        border: Border.all(color: const Color(0xFF222222).withOpacity(0.1)),
        color: const Color(0xFF222222).withOpacity(0.07),
        child: Row(
          children: [
            FText(timeText, color: Colors.black),
            SizedBox(width: 12.w),
            GestureDetector(
                onTap: () => onTapScreen(isFirst),
                child: Icon(Icons.event_note, size: 30.sp))
          ],
        ),
      ),
      buttonScreen(isFirst),
    ]);
  }

  Widget buttonScreen(bool isFirst) {
    Widget btn = FButton(
      bgColor: isFirst ? const Color(0xFFFF753F) : Colors.white,
      border:
          Border.all(color: !isFirst ? const Color(0xFFFF753F) : Colors.white),
      child: FText(
        isFirst ? "查询" : "重置",
        color: !isFirst ? const Color(0xFFFF753F) : Colors.white,
      ),
    );
    return GestureDetector(onTap: isFirst ? requestBill : initTime, child: btn);
  }

  Widget pitBillList() {
    return FContainer(
      border: Border.all(color: const Color(0xFF222222).withOpacity(0.1)),
      radius: BorderRadius.circular(20.w),
      child: Column(
        children: List.generate(10, (index) => billCell(index)),
      ),
    );
  }

  Widget billCell(int index) {
    String start = "", end = "";
    Color textColor = Colors.black;
    if (index == 0) {
      start = "麦位";
      end = "礼物流水";
    } else {
      start = RoomProvider().getSeatName(index);
      textColor = const Color(0xFF999999);
      end =
          "${billData.length > (index - 1) ? billData[index - 1]["mic_gold_summary"] ?? 0 : 0}";
    }
    return FContainer(
      border: Border(
          top: const BorderSide(color: Colors.transparent),
          bottom: BorderSide(
              color: index != 9
                  ? const Color(0xFF222222).withOpacity(0.1)
                  : Colors.transparent)),
      child: Row(
        children: [
          Flexible(
            child: FContainer(
                height: index != 0 ? 65.w : 80.w,
                align: Alignment.center,
                child: FText(start,
                    size: index != 0 ? 26.sp : 32.sp, color: textColor)),
          ),
          FContainer(
            width: 1.w,
            height: 60.w,
            color: const Color(0xFF222222).withOpacity(0.1),
          ),
          Flexible(
            child: FContainer(
                height: index != 0 ? 65.w : 80.w,
                align: Alignment.center,
                child: FText(end,
                    size: index != 0 ? 26.sp : 32.sp, color: textColor)),
          ),
        ],
      ),
    );
  }

  void onTapScreen(bool isFirst) async {
    DateTime? time = await WidgetUtils().showBottomSheet(
        FTimePicker(initTime: isFirst ? dateS : dateE),
        color: Colors.white);
    if (time == null) return;
    if (isFirst) {
      if (time.isAfter(dateE)) return WidgetUtils().showToast("开始时间不得大于结束时间");
      dateS = time;
    } else {
      if (time.isBefore(dateS)) return WidgetUtils().showToast("结束时间不得小于开始时间");
      dateE = time;
    }
    if (!mounted) return;
    setState(() {});
  }
}
