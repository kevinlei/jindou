import 'package:echo/import.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class RoomsInfo extends StatefulWidget {
  final RoomProvider pro;
  const RoomsInfo(this.pro, {super.key});

  @override
  State<RoomsInfo> createState() => _RoomsInfoState();
}

class _RoomsInfoState extends State<RoomsInfo> {
  @override
  Widget build(BuildContext context) {
    return FContainer(
      child: Stack(
        children: [
          infoBody(),
          infoHeader(),
        ],
      ),
    );
  }

  Widget infoHeader() {
    return FContainer(
      padding: EdgeInsets.symmetric(horizontal: 40.w),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          FContainer(
            margin: EdgeInsets.only(right: 20.w),
            width: 180.w,
            height: 180.w,
            radius: BorderRadius.circular(40.w),
            image: ImageUtils().getImageProvider(
                NetConstants.ossPath + (widget.pro.roomModel!.headIcon)),
            imageFit: BoxFit.cover,
          ),
          Expanded(
              child: FContainer(
            padding: EdgeInsets.only(top: 80.w),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    FText(
                      widget.pro.roomModel!.roomName,
                      size: 32.sp,
                      weight: FontWeight.w500,
                    ),
                    SizedBox(height: 5.w),
                    Row(
                      children: [
                        FContainer(
                          color: Colors.white.withOpacity(0.6),
                          radius: BorderRadius.circular(50),
                          child: LoadAssetImage("person/id", width: 42.w),
                        ),
                        SizedBox(width: 8.w),
                        FText("${widget.pro.roomModel!.exteriorRoomId}",
                            color: Colors.white.withOpacity(0.6)),
                        GestureDetector(
                          onTap: () {
                            Clipboard.setData(ClipboardData(
                                text:
                                    "${widget.pro.roomModel!.exteriorRoomId}"));
                            WidgetUtils().showToast("ID复制成功");
                          },
                          child: LoadAssetImage(
                            "person/rootzi",
                            width: 42.w,
                            color: Colors.white.withOpacity(0.6),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                headerBtn((widget.pro.roomModel!.personalPermission) < 4 ||
                    (UserConstants.userginInfo!.info.userRole >
                        UserRole.VIP.value))
              ],
            ),
          ))
        ],
      ),
    );
  }

  Widget headerBtn(bool isAdmin) {
    return FButton(
      onTap: isAdmin ? setRoom : reportRoom,
      bgColor: Colors.white.withOpacity(0.1),
      child: Row(
        children: [
          LoadAssetImage(
            isAdmin ? "room/setRoom" : "room/reportRoom",
            width: 30.w,
            height: 30.w,
          ),
          SizedBox(width: 5.w),
          FText(isAdmin ? "编辑" : "举报")
        ],
      ),
    );
  }

  Widget infoBody() {
    return FContainer(
      margin: EdgeInsets.only(top: 70.w),
      padding: EdgeInsets.only(
          top: 110.w,
          left: 40.w,
          right: 40.w,
          bottom: ScreenUtil().bottomBarHeight + 50.w),
      width: double.infinity,
      radius: BorderRadius.only(
          topLeft: Radius.circular(40.w), topRight: Radius.circular(40.w)),
      color: const Color(0xFF1D1928),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(vertical: 40.w),
            child: SizedBox(
              height: 350.w,
              child: ListView(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      FText(
                        widget.pro.roomModel!.roomBulletin,
                        maxLines: 100,
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
          Visibility(visible: true, child: infoFooter()),
        ],
      ),
    );
  }

  Widget infoFooter() {
    return Padding(
      padding: EdgeInsets.only(top: 50.w),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          buttons(true),
        ],
      ),
    );
  }

  Widget buttons(bool isTheme) {
    return FButton(
      onTap: isTheme
          ? () => roomCollect(widget.pro.roomModel!.isFollow == 2)
          : shareRoom,
      isTheme: !isTheme,
      bgColor: !isTheme ? null : const Color(0xFFFF753F),
      width: 320.w,
      height: 80.w,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          LoadAssetImage(
            isTheme ? "room/collectRoom" : "room/shareRoom",
            width: 40.w,
            height: 40.w,
          ),
          SizedBox(width: 10.w),
          FText(
            isTheme
                ? widget.pro.roomModel!.isFollow == 2
                    ? "收藏房间"
                    : "已收藏"
                : "分享房间",
            size: 28.sp,
            weight: FontWeight.w500,
          )
        ],
      ),
    );
  }

  // 设置房间
  void setRoom() async {
    await WidgetUtils()
        .showBottomSheet(SetRoom(widget.pro), color: Colors.transparent);
    setState(() {});
  }

  // 分享房间
  void shareRoom() {}

  // 收藏/取消收藏房间
  void roomCollect(bool isCollect) async {
    if (!isCollect) {
      bool? accept = await WidgetUtils().showAlert("确定取消关注房间吗？");
      if (accept == null || !accept) return;
    }
    await DioUtils().httpRequest(
      NetConstants.collectRoom,
      method: DioMethod.POST,
      params: {
        "exterior_room_id": widget.pro.roomModel!.exteriorRoomId,
        "follow_type": isCollect ? 1 : 2
      },
      loading: false,
      onSuccess: (data) {
        if (isCollect) {
          widget.pro.roomModel!.isFollow = 1;
        } else {
          widget.pro.roomModel!.isFollow = 2;
        }
        widget.pro.updateRoom();
        setState(() {});
      },
    );
  }

  // 举报房间
  void reportRoom() {
    WidgetUtils().showBottomSheet(
      const ReportRoom(),
      color: const Color(0xFF1D1928),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(20.w),
          topLeft: Radius.circular(20.w),
        ),
      ),
    );
  }
}

// 举报房间
class ReportRoom extends StatefulWidget {
  const ReportRoom({super.key});

  @override
  State<ReportRoom> createState() => _ReportRoomState();
}

class _ReportRoomState extends State<ReportRoom> {
  String textValue = "";

  @override
  Widget build(BuildContext context) {
    InputBorder border = OutlineInputBorder(
        borderRadius: BorderRadius.circular(20.w),
        borderSide: const BorderSide(color: Colors.transparent));

    return FContainer(
      padding: EdgeInsets.only(
          top: 40.w,
          left: 40.w,
          right: 40.w,
          bottom: ScreenUtil().bottomBarHeight + 40.w),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          FText("举报房间", size: 34.sp),
          SizedBox(height: 40.w),
          TextField(
            style: TextStyle(
                color: Colors.white, fontSize: 30.sp, letterSpacing: 1.2),
            maxLength: 300,
            maxLines: 10,
            autofocus: true,
            onChanged: (value) {
              if (textValue.isEmpty || value.isEmpty) {
                setState(() {
                  textValue = value;
                });
              }
            },
            decoration: InputDecoration(
              enabledBorder: border,
              focusedBorder: border,
              disabledBorder: border,
              filled: true,
              fillColor: Colors.white.withOpacity(0.1),
              counterStyle:
                  TextStyle(color: const Color(0xFFBBBBBB), fontSize: 30.sp),
              hintText: "举报理由",
              contentPadding: const EdgeInsets.fromLTRB(12, 8, 12, 8),
              hintStyle: TextStyle(
                color: const Color(0xFFBBBBBB),
                fontSize: 30.sp,
                letterSpacing: 1,
              ),
            ),
          ),
          SizedBox(height: 40.w),
          FButton(
            onTap: () {
              if (textValue.isEmpty) {
                return WidgetUtils().showToast("请输入举报理由");
              }
              WidgetUtils().popPage();
            },
            bgColor: const Color(0xFFFF753F),
            width: double.infinity,
            height: 90.w,
            align: Alignment.center,
            child: FText("提交", size: 30.sp),
          ),
          SizedBox(height: MediaQuery.of(context).viewInsets.bottom),
        ],
      ),
    );
  }
}
