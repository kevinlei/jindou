import 'dart:convert';
import 'package:echo/import.dart';
import 'package:flutter/material.dart';
import 'package:im_flutter_sdk/im_flutter_sdk.dart';

import '../../../widgets/myfont_weight.dart';

class RoomChatGroup extends StatefulWidget {
  final int roomId;
  const RoomChatGroup(this.roomId, {Key? key}) : super(key: key);

  @override
  _RoomChatGroupState createState() => _RoomChatGroupState();
}

class _RoomChatGroupState extends State<RoomChatGroup>
    with TickerProviderStateMixin {
  ScrollController scroll = ScrollController();
  late PageController pageController = PageController();
  // 当前频道
  int tabIndex = 0;

  EMConversation? con;

  late AnimationController animCtr;

  // 房间消息
  List<EMMessage> roomMsg = [];
  // 世界消息
  List<EMMessage> wordRoomMsg = [];

  // 是否有新消息
  bool hasNewMsg = false;

  // 是否开启极简模式
  bool miniMode = false;

  // 头条消息
  Map? headlines;

  // 是否开启房间聊天
  bool isChat = true;

  @override
  void initState() {
    super.initState();
    EventUtils().on(
        EventConstants.E_ROOM_MSG_UPDATE,
        EventCall("RoomChatGroup", (v) {
          roomMsg = AppManager().roomInfo.roomMsg;
          wordRoomMsg = AppManager().roomInfo.wordRoomMsg;
          update();
          decodeHasNewMsg();
        }));
    EventUtils().on(
      EventConstants.E_ROOM_CHAT_OPEN,
      EventCall("RoomChatGroup", (v) {
        isChat = v;
        if (!mounted) return;
        setState(() {});
      }),
    );

    init();
  }

  void init() {
    animCtr = AnimationController(
        duration: const Duration(milliseconds: 200), vsync: this);
    initChat();
    scrollToEnd();
  }

  // 初始化
  void initChat() {
    if (widget.roomId == AppManager().roomInfo.roomMode?.exteriorRoomId) {
      roomMsg = AppManager().roomInfo.roomMsg;
      wordRoomMsg = AppManager().roomInfo.wordRoomMsg;
      isChat = AppManager().roomInfo.roomMode!.roomIsChat == 2;
    } else {
      isChat = AppManager().roomInfo.roomMode?.roomIsChat == 2;
      tabIndex = 0;
      initMiniMode();
    }
    if (!mounted) return;
    setState(() {});
  }

  void initMiniMode() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    bool open = preferences.getBool(SharedKey.KEY_MINI_MODE.value) ?? false;
    miniMode = open;
  }

  @override
  void dispose() {
    EventUtils().off(
        EventConstants.E_ROOM_MSG_UPDATE, EventCall("RoomChatGroup", (v) {}));
    EventUtils().off(
        EventConstants.E_ROOM_CHAT_OPEN, EventCall("RoomChatGroup", (v) {}));
    pageController.dispose();
    scroll.dispose();
    animCtr.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          FContainer(
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                appBar(0, "全部"),
                appBar(1, "房间"),
                appBar(2, "礼物"),
                Opacity(
                  opacity: 0,
                  child: appBar(4, "幸运"),
                ),
              ],
            ),
          ),
          SizedBox(height: 10.w),
          isChat
              ? Expanded(
                  child: PageView(
                    physics: const NeverScrollableScrollPhysics(),
                    controller: pageController,
                    children: [
                      chatMsgCells(1),
                      chatMsgCells(2),
                      chatMsgCells(3),
                      chatMsgCells(4),
                    ],
                  ),
                )
              : Expanded(
                  child: FContainer(
                  align: Alignment.center,
                  child: FText(
                    "房间已关闭聊天!",
                    size: 32.sp,
                  ),
                ))
        ],
      ),
      buttonNewMsg(),
    ]);
  }

  Widget appBar(int index, String text) {
    return GestureDetector(
        onTap: () async {
          if (pageController.page == index) return;
          pageController.jumpToPage(index);
          tabIndex = index;
          if (!mounted) return;
          setState(() {});
          scrollToEnd();
        },
        child: FContainer(
          margin: EdgeInsets.symmetric(horizontal: 30.w),
          width: 60.w,
          height: 52.w,
          color: Colors.white.withOpacity(0),
          child: Column(
            children: [
              ShaderMask(
                shaderCallback: (Rect bounds) {
                  return LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: tabIndex == index
                        ? [
                            const Color(0xFFFF753F),
                            const Color(0xFFFF753F),
                            const Color(0xFFFF753F)
                          ]
                        : [
                            Colors.white.withOpacity(1),
                            Colors.white.withOpacity(1),
                            Colors.white.withOpacity(1)
                          ],
                  ).createShader(Offset.zero & bounds.size);
                },
                child: FText(
                  text,
                  size: tabIndex == index ? 28.sp : 23.sp,
                  weight:
                      tabIndex == index ?  MyFontWeight.medium : MyFontWeight.normal,
                ),
              ),
              const Spacer(),
              Visibility(
                visible: tabIndex == index,
                child: FContainer(
                  margin: EdgeInsets.symmetric(horizontal: 19.w),
                  width: double.infinity,
                  radius: BorderRadius.circular(6.h),
                  height: 6.h,
                  color: const Color(0xFFFF753F),
                ),
              )
            ],
          ),
        ));
  }

  Widget chatMsgCells(int idx) {
    List<EMMessage> list = roomMsg;
    int len = list.length;

    List<EMMessage> lists1 = [];
    List<EMMessage> lists2 = [];
    List<EMMessage> lists3 = [];
    List<EMMessage> lists4 = [];

    for (var i = 0; i < list.length; i++) {
      EMTextMessageBody msg = list[i].body as EMTextMessageBody;
      var content = json.decode(msg.content);
      int code = content?["event"] ?? 0;
      MqMsgType msgType = AppManager().getMqMsgType(code);

      if (list[i].to != AppManager().roomInfo.roomMode?.hxId.toString()) {
        continue;
      }

      if (msgType != MqMsgType.ROOM_GAME_GIFT) {
        lists1.add(list[i]);
      }

      switch (msgType) {
        case MqMsgType.SELF_JOIN:
          lists2.add(list[i]);
          break;
        case MqMsgType.USER_ROOM_ACCESS:
          int type = content["info"]["type"];
          if (type == 1) {
            lists2.add(list[i]);
          }
          break;
        case MqMsgType.ROOM_SEND_CHAT:
          lists2.add(list[i]);
          break;
        case MqMsgType.ROOM_GIFT:
          lists3.add(list[i]);
          break;
        case MqMsgType.ROOM_GAME_GIFT:
          if ((UserConstants
                      .userginInfo!.gradeInfo.userGrade.wealth.nowExperience ??
                  0) >=
              AppManager().roomInfo.isWealthRestrict) {
            lists4.add(list[i]);
          }
          break;
        default:
      }
    }

    if (idx == 1) len = lists1.length;
    if (idx == 2) len = lists2.length;
    if (idx == 3) len = lists3.length;
    if (idx == 4) len = lists4.length;

    return ListView.builder(
      controller: scroll,
      padding: EdgeInsets.symmetric(horizontal: 25.w),
      itemBuilder: (_, index) {
        if (idx == 1) {
          return RoomChatCell(lists1[index], miniMode);
        } else if (idx == 2) {
          return RoomChatCell(lists2[index], miniMode);
        } else if (idx == 3) {
          return RoomChatCell(lists3[index], miniMode);
        } else {
          return RoomChatCell(lists4[index], miniMode);
        }
      },
      itemCount: len,
    );
  }

  Widget buttonNewMsg() {
    return Positioned(
      left: 20.w,
      bottom: 0,
      child: AnimatedBuilder(
        animation: animCtr.view,
        builder: (_, __) => !hasNewMsg
            ? const SizedBox()
            : GestureDetector(
                onTap: onTapNewMsg,
                child: FContainer(
                  height: 100.w + ScreenUtil().bottomBarHeight,
                  align: Alignment.centerLeft,
                  child: LoadAssetImage("room/new", width: 140.w),
                ),
              ),
      ),
    );
  }

  Future<EMConversation?> initConversation(String chatId) async {
    return await EMClient.getInstance.chatManager
        .getConversation(chatId, type: EMConversationType.ChatRoom);
  }

  void scrollToEnd() async {
    Future.delayed(const Duration(milliseconds: 100), () async {
      if (!mounted) return;
      WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
        if (scroll.hasClients) {
          double max = scroll.position.maxScrollExtent;
          scroll.animateTo(max,
              duration: const Duration(milliseconds: 200),
              curve: Curves.linear);
          Future.delayed(const Duration(milliseconds: 200), () {
            double max2 = scroll.position.maxScrollExtent;
            if (max == max2) return;
            scroll.animateTo(max2,
                duration: const Duration(milliseconds: 1),
                curve: Curves.linear);
          });
          hasNewMsg = false;
          if (!mounted) return;
          setState(() {});
        }
      });
    });
  }

  void onTapNewMsg() {
    scrollToEnd();
    hasNewMsg = false;
    animCtr.reverse();
  }

  void decodeHasNewMsg() async {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      if (!scroll.hasClients) return;
      double max = scroll.position.maxScrollExtent;
      if (scroll.offset < (max - 200)) {
        hasNewMsg = true;
        animCtr.forward();
      } else {
        hasNewMsg = false;
        animCtr.reverse();
        scrollToEnd();
      }
    });
  }

  void update() {
    if (!mounted) return;
    setState(() {});
  }

  // 发世界头条
  void onSendHeadlines() {
    // WidgetUtils().showFDialog(const Headlines());
  }
}
