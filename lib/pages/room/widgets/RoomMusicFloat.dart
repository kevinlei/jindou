import 'dart:async';

import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class RoomMusicFloat extends StatefulWidget {
  final int roomId;
  const RoomMusicFloat(this.roomId, {super.key});

  @override
  State<RoomMusicFloat> createState() => _RoomMusicFloatState();
}

class _RoomMusicFloatState extends State<RoomMusicFloat>
    with SingleTickerProviderStateMixin {
  // 显示音乐播放器
  bool showMusic = false;

  // 拖动
  double topOffset = 70;

  // 是否打开音量控制
  bool isVolume = false;

  // 音量
  double length = 250.w, proVolume = 0;

  /// 会重复播放的控制器
  late final AnimationController _repeatController;

  /// 线性动画
  late final Animation<double> _animation;

  // 播放音乐信息
  var musicInfo;
  // 当前音乐暂停或者播放
  int isMusicPlay = 0;
  // 当前音乐播放模式
  int musicMode = 0;

  @override
  void initState() {
    super.initState();
    EventUtils().on(
      EventConstants.E_ROOM_MUSIC,
      EventCall("RoomMusicFloat", (data) {
        musicInfo = AppManager().roomInfo.musicInfo;
        isMusicPlay = AppManager().roomInfo.playMusicMode;
        musicMode = AppManager().roomInfo.musicMode;
        if (musicInfo != null &&
            (_repeatController.status == AnimationStatus.completed ||
                _repeatController.status == AnimationStatus.dismissed)) {
          _repeatController.repeat();
        }
        if (!mounted) return;
        setState(() {});
      }),
    );

    init();
  }

  void init() {
    _repeatController = AnimationController(
      duration: const Duration(seconds: 5),
      vsync: this,
    );
    _animation = Tween<double>(begin: 0, end: 1).animate(_repeatController);

    if (widget.roomId == AppManager().roomInfo.roomMode?.exteriorRoomId) {
      musicInfo = AppManager().roomInfo.musicInfo;
      isMusicPlay = AppManager().roomInfo.playMusicMode;
      musicMode = AppManager().roomInfo.musicMode;

      if (musicInfo != null) {
        _repeatController.repeat();
      } else {
        _repeatController.stop();
      }
    }
  }

  @override
  void dispose() {
    EventUtils().off(
        EventConstants.E_ROOM_MUSIC, EventCall("RoomMusicFloat", (data) {}));
    _repeatController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: musicInfo != null,
      child: AnimatedPositioned(
        left: 10.w,
        top: kToolbarHeight + topOffset,
        duration: const Duration(milliseconds: 50),
        child: GestureDetector(
          onPanUpdate: onDragUpdate,
          child: AnimatedSwitcher(
            duration: const Duration(milliseconds: 150),
            child: showMusic ? player() : disc(),
          ),
        ),
      ),
    );
  }

  Widget disc() {
    Widget child = FContainer(
      width: 160.w,
      height: 160.w,
      radius: BorderRadius.circular(20.w),
      color: const Color(0xFF2C2850),
      padding: EdgeInsets.all(25.w),
      child: Stack(
        alignment: Alignment.topRight,
        clipBehavior: Clip.none,
        children: [
          RotationTransition(
            turns: _animation,
            child: FContainer(
              width: double.infinity,
              height: double.infinity,
              border: Border.all(
                  width: 2.w, color: const Color(0xFF000000).withOpacity(0.25)),
              radius: BorderRadius.circular(50),
              image: ImageUtils().getImageProvider(
                  musicInfo?["album"]["blurPicUrl"] ?? "",
                  format: ImageFormat.JPG),
              imageFit: BoxFit.cover,
            ),
          ),
          Positioned(
            top: -7.w,
            right: -7.w,
            child: LoadAssetImage(
              "room/RoomMusic/playerpointer",
              height: 80.w,
            ),
          )
        ],
      ),
    );
    return GestureDetector(
      onTap: () {
        setState(() {
          showMusic = true;
        });
      },
      child: child,
    );
  }

  Widget player() {
    // {name: 空待(独唱版), id: 34923178, artists: [{name: 王朝, id: 1039296}],
    // album: {name: 空待, id: 169585808, type: Single, size: 3, picId: 109951168737278943,
    // blurPicUrl: http://p1.music.126.net/Cz0Bor-8XWlR0h6y5iRk8g==/109951168737278943.jpg, companyId: 0,
    // pic: 109951168737278943, picUrl: http://p1.music.126.net/Cz0Bor-8XWlR0h6y5iRk8g==/109951168737278943.jpg,
    // publishTime: 1364918400000}, hMusic: {playTime: 324414},
    // playUrl: http://m701.music.1o3DlMOGwrbDjj7DisKw/29453631120/e7b6b13a3792d94705ef74370c.mp3}
    String playImg = "";
    switch (musicMode) {
      case 1:
        playImg = "random";
        break;
      case 2:
        playImg = "circulate";
        break;
      default:
        playImg = "order";
    }

    String openImg = "play";
    if (isMusicPlay == 2) {
      openImg = "suspend";
    }

    return FContainer(
      width: 450.w,
      padding: EdgeInsets.symmetric(vertical: 25.w, horizontal: 40.w),
      radius: BorderRadius.circular(20.w),
      color: const Color(0xFF2C2850),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              playerItem("room/RoomMusic/reduce", onTapMiniMode),
              Expanded(
                  child: Align(
                child: ScrollText(
                  maxWidth: 250.w,
                  TextSpan(
                    style: TextStyle(
                      fontSize: 27.sp,
                      color: Colors.white,
                    ),
                    text:
                        "${musicInfo?["name"] ?? ""} - ${musicInfo?["album"]["name"] ?? ""}",
                  ),
                ),
              )),
              playerItem("room/RoomMusic/end", AppManager().enMusic),
            ],
          ),
          SizedBox(height: 25.w),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              playerItem(
                  "room/RoomMusic/$playImg",
                  () => AppManager()
                      .switchMusicMode(musicMode == 2 ? 0 : musicMode + 1)),
              playerItem("room/RoomMusic/previousSong",
                  () => AppManager().upDownMusic(true)),
              playerItem(
                  "room/RoomMusic/$openImg", AppManager().roomAudioMixing,
                  width: 60.w),
              playerItem("room/RoomMusic/nextSong",
                  () => AppManager().upDownMusic(false)),
              playerItem("room/RoomMusic/volume", onTapVolume),
            ],
          ),
          Visibility(
              visible: isVolume,
              child: Padding(
                padding: EdgeInsets.only(top: 25.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    FText(
                      "0",
                      size: 22.sp,
                      color: Colors.white.withOpacity(0.5),
                    ),
                    volume(),
                    FText(
                      "100",
                      size: 22.sp,
                      color: Colors.white.withOpacity(0.5),
                    )
                  ],
                ),
              ))
        ],
      ),
    );
  }

  Widget playerItem(String img, void Function() onTap, {double? width}) {
    return GestureDetector(
      onTap: onTap,
      child: LoadAssetImage(
        img,
        width: width ?? 40.w,
      ),
    );
  }

  Widget volume() {
    return GestureDetector(
      onHorizontalDragUpdate: (detalis) => onDragVolume(detalis),
      // onHorizontalDragEnd: (detalis) => onDragEnd(detalis),
      child: FContainer(
        height: 35.w,
        width: length,
        child: Stack(
            alignment: Alignment.center,
            clipBehavior: Clip.none,
            children: [progress(), bar()]),
      ),
    );
  }

  Widget progress() {
    return FContainer(
      height: 6.w,
      radius: BorderRadius.circular(6.w),
      color: const Color(0xFFEFF1F5),
      align: Alignment.centerLeft,
      child: FContainer(
        height: 6.w,
        width: proVolume,
        radius: BorderRadius.circular(6.w),
        gradient: const [
          Color(0xFF96FCFC),
          Color(0xFFFFA7FB),
          Color(0xFFFF82B2),
        ],
        gradientBegin: Alignment.topLeft,
        gradientEnd: Alignment.bottomRight,
        stops: const [0, 0.4, 0.8],
      ),
    );
  }

  Widget bar() {
    return Positioned(
      left: proVolume,
      child: FContainer(
        width: 17.w,
        height: 17.w,
        radius: BorderRadius.circular(50),
        color: Colors.white,
      ),
    );
  }

  void onDragUpdate(DragUpdateDetails d) {
    topOffset += d.delta.dy;
    if (topOffset <= kToolbarHeight) {
      topOffset = kToolbarHeight;
    }
    if (topOffset >= ScreenUtil().screenHeight - 2 * kToolbarHeight - 100.h) {
      topOffset = ScreenUtil().screenHeight - 2 * kToolbarHeight - 100.h;
    }

    if (!mounted) return;
    setState(() {});
  }

  void onTapMiniMode() {
    setState(() {
      showMusic = false;
    });
  }

  void onDragVolume(DragUpdateDetails details) {
    proVolume += details.delta.dx;

    if (proVolume < 0) {
      proVolume = 0;
    }

    if (proVolume >= (length - 15.w)) {
      proVolume = length - 15.w;
    }

    double vv = double.parse((proVolume / (length - 15.w)).toStringAsFixed(2));
    AppManager().set5MusicVolume((vv * 100).toInt());
    if (!mounted) return;
    setState(() {});
  }

  void onTapVolume() async {
    if (!isVolume) {
      int volume = await AppManager().getVolume();
      proVolume = length * (volume / 100) - 15.w;
    }
    isVolume = !isVolume;
    setState(() {});
  }
}
