import 'dart:async';
import 'dart:math';
import 'package:echo/import.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class RoomSeatList extends StatefulWidget {
  final int roomId;
  const RoomSeatList(this.roomId, {super.key});

  @override
  State<RoomSeatList> createState() => _RoomSeatListState();
}

class _RoomSeatListState extends State<RoomSeatList> {
  // 麦位列表
  List<Mic> seatLists = [];

  @override
  void initState() {
    super.initState();
    EventUtils().on(
      EventConstants.E_ROOM_SEAT,
      EventCall("RoomSeatList", (data) {
        seatLists = AppManager().roomInfo.seatLists;
        if (!mounted) return;
        setState(() {});
      }),
    );

    if (widget.roomId == AppManager().roomInfo.roomMode!.exteriorRoomId) {
      seatLists = AppManager().roomInfo.seatLists;
    }
  }

  @override
  void dispose() {
    EventUtils()
        .off(EventConstants.E_ROOM_SEAT, EventCall("RoomSeatList", (data) {}));
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        RoomSeatItem(seatLists.isNotEmpty ? seatLists[0] : null, 0, one: true),
        SizedBox(height: 30.w),
        seatList(),
      ],
    );
  }

  Widget seatList() {
    return GridView.count(
      crossAxisCount: 4,
      shrinkWrap: true,
      crossAxisSpacing: 20.w,
      padding: EdgeInsets.symmetric(horizontal: 20.w),
      physics: const NeverScrollableScrollPhysics(),
      children: List.generate(8, (i) {
        Mic? seat = seatLists.length >= i + 2 ? seatLists[i + 1] : null;
        return RoomSeatItem(seat, i + 1);
      }),
    );
  }
}

// 麦位用户
class RoomSeatItem extends StatefulWidget {
  final Mic? seat;
  final int index;
  final bool one;
  const RoomSeatItem(this.seat, this.index, {super.key, this.one = false});

  @override
  State<RoomSeatItem> createState() => _RoomSeatItemState();
}

class _RoomSeatItemState extends State<RoomSeatItem>
    with TickerProviderStateMixin {
  late AnimationController _linController;

  // 是否在说话
  bool micState = false;

  // 播放表情开关
  Timer? timer;
  // 表情图片
  String emo = "";

  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(seconds: 1), () async {
      EventUtils().off(EventConstants.E_ROOM_SEAT_VOICE,
          EventCall("RoomSeatItem${widget.index}", (uids) {}));
      EventUtils().off(EventConstants.E_ROOM_MY_VOICE,
          EventCall("RoomSeatItem${widget.index}", (vouim) {}));
      EventUtils().off(EventConstants.E_ROOM_EMO,
          EventCall("RoomSeatItem${widget.index}", (emo) {}));
      EventUtils().on(
          EventConstants.E_ROOM_SEAT_VOICE,
          EventCall("RoomSeatItem${widget.index}", (uids) {
            if (widget.seat == null) return;
            bool containSelf = false;
            for (var i = 0; i < uids.length; i++) {
              if ((uids[i] == widget.seat!.micUserInfo.agoraUid &&
                      uids[i] != 0) ||
                  (uids[i] == 0 &&
                      widget.seat!.micUserInfo.userId ==
                          UserConstants.userginInfo!.info.userId)) {
                containSelf = true;
                break;
              }
            }
            if (containSelf && !micState) {
              startSpeakAnim();
            } else if (!containSelf && micState) {
              stopSpeakAnim();
            }
          }));
      EventUtils().on(
          EventConstants.E_ROOM_MY_VOICE,
          EventCall("RoomSeatItem${widget.index}", (vouim) {
            if (widget.seat == null) return;
            bool containSelf = widget.seat!.micUserInfo.userId ==
                UserConstants.userginInfo!.info.userId;
            if (!containSelf) return;
            if (vouim > 12 && !micState) {
              startSpeakAnim();
            } else if (vouim <= 12 && micState) {
              stopSpeakAnim();
            }
          }));
      EventUtils().on(
          EventConstants.E_ROOM_EMO,
          EventCall("RoomSeatItem${widget.index}", (v) {
            if (widget.seat != null &&
                widget.seat?.micUserInfo.userId == v[0]) {
              timer?.cancel();
              emo = v[1];
              if (!mounted) return;
              setState(() {});
              timer = Timer(const Duration(seconds: 3), () {
                emo = "";
                if (!mounted) return;
                setState(() {});
                timer!.cancel();
              });
            }
          }));
    });

    EventUtils().on(
        EventConstants.E_ROOM_SEAT_VOICE,
        EventCall("RoomSeatItem${widget.index}", (uids) {
          if (widget.seat == null) return;
          bool containSelf = false;
          for (var i = 0; i < uids.length; i++) {
            if ((uids[i] == widget.seat!.micUserInfo.agoraUid &&
                    uids[i] != 0) ||
                (uids[i] == 0 &&
                    widget.seat!.micUserInfo.userId ==
                        UserConstants.userginInfo!.info.userId)) {
              containSelf = true;
              break;
            }
          }
          if (containSelf && !micState) {
            startSpeakAnim();
          } else if (!containSelf && micState) {
            stopSpeakAnim();
          }
        }));
    EventUtils().on(
        EventConstants.E_ROOM_MY_VOICE,
        EventCall("RoomSeatItem${widget.index}", (vouim) {
          if (widget.seat == null) return;
          bool containSelf = widget.seat!.micUserInfo.userId ==
              UserConstants.userginInfo!.info.userId;
          if (!containSelf) return;
          if (vouim > 12 && !micState) {
            startSpeakAnim();
          } else if (vouim <= 12 && micState) {
            stopSpeakAnim();
          }
        }));
    EventUtils().on(
        EventConstants.E_ROOM_EMO,
        EventCall("RoomSeatItem${widget.index}", (v) {
          if (widget.seat != null && widget.seat?.micUserInfo.userId == v[0]) {
            timer?.cancel();
            emo = v[1];
            if (!mounted) return;
            setState(() {});
            timer = Timer(const Duration(seconds: 3), () {
              emo = "";
              if (!mounted) return;
              setState(() {});
              timer!.cancel();
            });
          }
        }));

    init();
  }

  void init() {
    _linController = AnimationController(
        duration: const Duration(milliseconds: 700), vsync: this);
  }

  @override
  void dispose() {
    EventUtils().off(EventConstants.E_ROOM_SEAT_VOICE,
        EventCall("RoomSeatItem${widget.index}", (uids) {}));
    EventUtils().off(EventConstants.E_ROOM_MY_VOICE,
        EventCall("RoomSeatItem${widget.index}", (vouim) {}));
    EventUtils().off(EventConstants.E_ROOM_EMO,
        EventCall("RoomSeatItem${widget.index}", (emo) {}));
    _linController.dispose();
    timer?.cancel();
    timer = null;
    super.dispose();
  }

  void startSpeakAnim() async {
    _linController.repeat();
    micState = true;
    if (!mounted) return;
    setState(() {});
  }

  void stopSpeakAnim() {
    _linController.stop();
    micState = false;
    if (!mounted) return;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return roomSeatItem(widget.index, one: widget.one);
  }

  Widget roomSeatItem(int index, {bool one = false}) {
    // 座位信息
    Mic? seat = widget.seat;

    // 主色
    int color = 0xFFFF753F;
    if (seat?.micUserInfo.sex == 1) {
      color = 0xFF648FFF;
    }
    if (index == 8) {
      color = 0xFFFFD81D;
    }

    // 座位名称
    String title = "$index";
    if (index == 1) {
      title = "主持";
    } else if (index == 0) {
      title = "房主";
    }

    // 尺寸
    double size = (index == 0) ? 110.w : 100.w;

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        GestureDetector(
          onTap: () => onTapTrueLoveChart(index + 1),
          child: FContainer(
            color: Colors.transparent,
            width: size,
            height: 15.w,
          ),
        ),
        Stack(
          alignment: Alignment.center,
          clipBehavior: Clip.none,
          children: [
            Visibility(visible: micState, child: speakerAnim(size)), // 声浪动画
            seatNode(seat, index, size),
            headBlock(size), // 头像框
            // seatShut(), // 麦克风
            // timeCount(), // 倒计时
            Visibility(visible: emo.isNotEmpty, child: emoWdg(size)), // 表情
            Visibility(
              visible: (AppManager().roomInfo.roomMode?.isHeart ?? 2) == 2,
              child: seatCharm(seat?.heart ?? 0, index + 1),
            ), // 心动值
          ],
        ),
        SizedBox(height: 5.w),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            FContainer(
              color: Color(color),
              padding: EdgeInsets.symmetric(horizontal: 5.w),
              radius: BorderRadius.circular(50),
              align: Alignment.center,
              child: FText(
                title,
                size: 16.sp,
              ),
            ),
            SizedBox(width: 3.w),
            Container(
              constraints: BoxConstraints(
                  maxWidth: title == "房主" ? double.infinity : 110.w),
              child: FText(
                (seat?.micUserInfo.userName != null &&
                        seat!.micUserInfo.userName.isNotEmpty)
                    ? AppConstants().flutterText(seat.micUserInfo.userName)
                    : index == 8
                        ? "嘉宾位"
                        : "上麦",
                size: 18.sp,
                color: (seat?.micUserInfo.userName != null &&
                        seat!.micUserInfo.userName.isNotEmpty)
                    ? Colors.white
                    : index == 8
                        ? const Color(0xFFFFD81D)
                        : Colors.white,
                overflow: TextOverflow.ellipsis,
              ),
            )
          ],
        )
      ],
    );
  }

  // 个人头像
  Widget seatNode(Mic? seat, int index, double size) {
    String img = "room/seat";
    if (index == 9) {
      img = "room/guest";
    }
    if (seat?.roomMicLock == 2) {
      img = "room/lock";
    }
    return GestureDetector(
      onTap: () => onTapUser(index + 1, seat),
      child: FContainer(
        width: size,
        height: size,
        radius: (seat?.micUserInfo.userImage != null &&
                seat!.micUserInfo.userImage.isNotEmpty)
            ? BorderRadius.circular(50)
            : null,
        imageFit: BoxFit.cover,
        image: (seat?.micUserInfo.userImage != null &&
                seat!.micUserInfo.userImage.isNotEmpty)
            ? ImageUtils().getImageProvider(
                NetConstants.ossPath + seat.micUserInfo.userImage)
            : ImageUtils().getAssetsImage(img),
      ),
    );
  }

  // 头像框
  Widget headBlock(double size) {
    double cell = 50.w;
    // String blockImage = getHeadBlock();
    String blockImage = "";
    Widget? blockWidget;
    if (blockImage.isNotEmpty) {
      if (blockImage.endsWith(".webp")) {
        blockWidget = LoadNetImage(NetConstants.ossPath + blockImage);
      } else if (blockImage.endsWith(".svga")) {
        blockWidget =
            SVGASimpleImage(resUrl: NetConstants.ossPath + blockImage);
      }
    }
    return Positioned(
      left: -cell / 2,
      top: -cell / 2,
      child: Visibility(
        visible: blockImage.isNotEmpty,
        child: SizedBox.fromSize(
            size: Size(size + cell, size + cell), child: blockWidget),
      ),
    );
  }

  // 表情
  Widget emoWdg(double size) {
    return Center(
      child: LoadNetImage(
        NetConstants.ossPath + emo,
        width: size,
        height: size,
      ),
    );
  }

  // 心动值
  Widget seatCharm(int loveValue, int pit) {
    Widget charm = FContainer(
      height: 26.w,
      constraints: BoxConstraints(minWidth: 55.w),
      padding: EdgeInsets.symmetric(horizontal: 5.w),
      gradient: [
        const Color(0xFFFF753F).withOpacity(0.9),
        const Color(0xFFFF753F).withOpacity(0.9),
        const Color(0xFFFF753F).withOpacity(0.9),
      ],
      stops: const [0, 0.4, 0.8],
      gradientBegin: Alignment.topLeft,
      gradientEnd: Alignment.bottomRight,
      radius: BorderRadius.only(
        topLeft: Radius.circular(20.w),
        topRight: Radius.circular(20.w),
        bottomRight: Radius.circular(20.w),
      ),
      align: Alignment.center,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(Icons.favorite, size: 16.sp, color: Colors.white),
          SizedBox(
            width: 3.w,
          ),
          FText(
              loveValue > 10000
                  ? "${(loveValue / 10000).toStringAsFixed(2)}w"
                  : "$loveValue",
              size: 16.sp,
              color: Colors.white)
        ],
      ),
    );
    return Positioned(
      left: 50.w,
      top: -15.w,
      child: GestureDetector(
        onTap: () => onTapTrueLoveChart(pit),
        child: charm,
      ),
    );
  }

  Widget speakerAnim(double size) {
    double cell = 80.w;
    return Positioned(
      left: -cell / 2,
      top: -cell / 2,
      child: SizedBox.fromSize(
        size: Size(size + cell, size + cell),
        child: AnimatedBuilder(
          animation: _linController,
          builder: (context, child) {
            return CustomPaint(
              painter: WaterRipplePainter(
                _linController.value,
                count: 4,
              ),
            );
          },
        ),
      ),
    );
  }

  // 点击麦位
  void onTapUser(int micNum, Mic? user) {
    if (user == null) return;
    if (AppManager().roomInfo.roomMode!.personalPermission == 4 &&
        (UserConstants.userginInfo!.info.userRole == UserRole.DEF.value) &&
        user.micUserInfo.exUserId != 0) {
      WidgetUtils().showBottomSheet(
        RoomUserInfo(
          user.micUserInfo.userId,
          micNum: micNum,
        ),
        color: Colors.transparent,
      );
    } else if (AppManager().roomInfo.roomMode!.personalPermission == 4 &&
        (UserConstants.userginInfo!.info.userRole == UserRole.DEF.value) &&
        user.micUserInfo.exUserId == 0) {
      // 上麦
      AppManager().upWheat(micNum);
    } else {
      WidgetUtils()
          .showBottomSheet(SeatOperate(micNum, user), color: Colors.black);
    }
  }

  // 麦位心动榜
  void onTapTrueLoveChart(int pit) {
    WidgetUtils().showFDialog(TrueLoveChart(pit));
  }
}

class WaterRipplePainter extends CustomPainter {
  final double progress;
  final int count;
  final Color color;

  WaterRipplePainter(this.progress,
      {this.count = 1, this.color = const Color(0xFF9fbaff)});

  @override
  void paint(Canvas canvas, Size size) {
    double radius = min(size.width / 2, size.height / 2);

    for (int i = count; i >= 0; i--) {
      final double opacity = (1.0 - ((i + progress) / (count + 1.2)));
      final Color _color = color.withOpacity(opacity);

      double _radius = radius * ((i + progress) / (count + 1));

      //水波纹
      Paint _paint = Paint()
        ..style = PaintingStyle.fill
        ..color = _color;
      canvas.drawCircle(
          Offset(size.width / 2, size.height / 2), _radius, _paint);
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}

// 麦位操作
class SeatOperate extends StatelessWidget {
  final int micNum;
  final Mic user;
  const SeatOperate(this.micNum, this.user, {super.key});

  @override
  Widget build(BuildContext context) {
    return FContainer(
      radius: BorderRadius.only(
          topLeft: Radius.circular(20.w), topRight: Radius.circular(20.w)),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          ...operateList(),
          SizedBox(height: ScreenUtil().bottomBarHeight),
        ],
      ),
    );
  }

  List<Widget> operateList() {
    List<Widget> list = [];
    if (user.micUserInfo.exUserId != 0) {
      list.add(item("用户信息", lockUser));
    }
    if (micNum != 1) {
      if (user.roomMicLock == 1) {
        if (user.micUserInfo.exUserId == 0 &&
            AppManager().roomInfo.roomMode!.personalPermission !=
                RoomRole.OWNER.value) {
          if (AppManager().roomInfo.roomMode!.isMicApply == 2 &&
              AppManager().roomInfo.roomMode!.personalPermission == 4) {
            list.add(item("申请上麦", applyMic));
          } else {
            list.add(item("上麦", upWheat));
          }
        }
        if (AppManager().roomInfo.roomMode!.personalPermission < 4 ||
            (UserConstants.userginInfo!.info.userRole > UserRole.VIP.value)) {
          if (user.micUserInfo.exUserId == 0) {
            list.add(item("抱上麦", grabTheWheat));
          } else if (user.micUserInfo.exUserId != 0) {
            if (user.micUserInfo.exUserId ==
                UserConstants.userginInfo!.info.displayId) {
              list.add(item("下麦", () => downWheat(true)));
            } else {
              if (AppManager().roomInfo.roomMode!.personalPermission < 4) {
                list.add(item("抱下麦", () => downWheat(false)));
              }
            }
          }
          list.add(item(user.roomMicBan == 2 ? "取消禁麦" : "禁麦", noSpeakingMic));
          list.add(item("锁麦", lockSeat));
        }
      } else if (AppManager().roomInfo.roomMode!.personalPermission < 4 ||
          (UserConstants.userginInfo!.info.userRole > UserRole.VIP.value)) {
        list.add(item("解锁", lockSeat));
      }
    }
    if ((AppManager().roomInfo.roomMode!.personalPermission < 4 ||
            (UserConstants.userginInfo!.info.userRole > UserRole.VIP.value)) &&
        user.roomMicLock == 1) {
      list.add(item("清空心动值", clearToLove));
    }
    list.add(item("取消", () => WidgetUtils().popPage()));
    return list;
  }

  Widget item(String text, void Function() onTap) {
    return GestureDetector(
      onTap: onTap,
      child: FContainer(
        padding: EdgeInsets.symmetric(vertical: 30.w),
        border: text == "取消"
            ? null
            : Border(bottom: BorderSide(color: Colors.white.withOpacity(0.2))),
        align: Alignment.center,
        child: FText(
          text,
          size: 32.sp,
        ),
      ),
    );
  }

  // 更新视图
  void update() {
    WidgetUtils().popPage();
  }

  // 申请上麦
  void applyMic() async {
    await DioUtils().httpRequest(
      NetConstants.applyBarley,
      method: DioMethod.POST,
      loading: false,
      params: {
        "exterior_room_id": AppManager().roomInfo.roomMode!.exteriorRoomId,
        "room_mic_num": micNum
      },
      onSuccess: (res) {
        update();
      },
    );
  }

  // 上麦
  void upWheat() async {
    AppManager().upWheat(micNum, onSuccess: (res) => update());
  }

  // 下麦/抱下麦
  void downWheat(bool isMy) async {
    AppManager().downWheat(
        id: isMy ? null : user.micUserInfo.exUserId,
        onSuccess: (res) => update());
  }

  // 抱上麦
  void grabTheWheat() async {
    await WidgetUtils().showBottomSheet(
        RoomTheCharts(
          index: 2,
          micNum: micNum,
        ),
        color: Colors.transparent);
    update();
  }

  // 清空心动值
  void clearToLove() async {
    await DioUtils().httpRequest(
      NetConstants.clearToLove,
      loading: false,
      params: {
        "room_id": AppManager().roomInfo.roomMode!.roomId,
        "room_mic_num": micNum
      },
      onSuccess: (res) {
        update();
      },
    );
  }

  // 锁麦/取消锁麦
  void lockSeat() async {
    await DioUtils().httpRequest(
      NetConstants.lockSeat,
      method: DioMethod.POST,
      loading: false,
      params: {
        "exterior_room_id": AppManager().roomInfo.roomMode!.exteriorRoomId,
        "room_mic_num": micNum,
        "room_mic_lock": user.roomMicLock == 1 ? 2 : 1
      },
      onSuccess: (res) {
        update();
      },
    );
  }

  // 禁麦/取消禁麦
  void noSpeakingMic() async {
    await DioUtils().httpRequest(
      NetConstants.roomMicBan,
      method: DioMethod.POST,
      loading: false,
      params: {
        "exterior_room_id": AppManager().roomInfo.roomMode!.exteriorRoomId,
        "room_mic_num": micNum,
        "room_mic_ban": user.roomMicBan == 1 ? 2 : 1,
      },
      onSuccess: (res) {
        update();
      },
    );
  }

  // 查看用户信息
  void lockUser() {
    update();
    WidgetUtils().showBottomSheet(
      RoomUserInfo(
        user.micUserInfo.userId,
        micNum: micNum,
      ),
      color: Colors.transparent,
    );
  }
}

// 真爱榜
class TrueLoveChart extends StatefulWidget {
  final int pit;
  const TrueLoveChart(this.pit, {super.key});

  @override
  State<TrueLoveChart> createState() => _TrueLoveChartState();
}

class _TrueLoveChartState extends State<TrueLoveChart> {
  List userList = [];

  @override
  void initState() {
    super.initState();
    initToLoveList();
  }

  void initToLoveList() async {
    await DioUtils().httpRequest(
      NetConstants.toLoveList,
      loading: false,
      params: {
        "room_id": AppManager().roomInfo.roomMode!.roomId,
        "room_mic_num": widget.pit,
      },
      onSuccess: (res) {
        if (res == null || !mounted) return;
        userList = res["list"];
        setState(() {});
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: GestureDetector(
        onTap: () => WidgetUtils().popPage(),
        child: FContainer(
          width: 600.w,
          height: 1100.w,
          margin: EdgeInsets.only(bottom: 200.w),
          padding: EdgeInsets.only(top: 320.w, bottom: 120.w),
          image: ImageUtils().getAssetsImage("room/TrueLoveChart/bg"),
          child: ListView.builder(
            addAutomaticKeepAlives: false,
            addRepaintBoundaries: false,
            padding: EdgeInsets.only(top: 0, left: 50.w, right: 50.w),
            itemBuilder: (context, index) => userItem(index, userList[index]),
            itemCount: userList.length,
          ),
        ),
      ),
    );
  }

  Widget userItem(int index, data) {
    Widget head = FContainer(
      width: 80.w,
      height: 80.w,
      radius: BorderRadius.circular(50),
      imageFit: BoxFit.cover,
      image: ImageUtils()
          .getImageProvider(NetConstants.ossPath + data["user_image"]),
    );
    if (index < 3) {
      head = Stack(
        clipBehavior: Clip.none,
        children: [
          FContainer(
            width: 80.w,
            height: 80.w,
            radius: BorderRadius.circular(50),
            imageFit: BoxFit.cover,
            image: ImageUtils()
                .getImageProvider(NetConstants.ossPath + data["user_image"]),
          ),
          Positioned(
              top: -28.w,
              left: -12.w,
              child: LoadAssetImage("room/TrueLoveChart/NO.${index + 1}",
                  width: 50.w)),
        ],
      );
    }

    return GestureDetector(
      onTap: () {},
      child: FContainer(
        padding: EdgeInsets.only(top: 30.w, bottom: 20.w),
        border: Border(
            bottom: BorderSide(
                color: index + 1 == 10
                    ? Colors.transparent
                    : const Color(0xFF222222).withOpacity(0.05))),
        child: Row(
          children: [
            head,
            SizedBox(width: 12.w),
            SizedBox(
              height: 80.w,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  FText("${data["user_name"]}", color: Colors.black),
                  Row(
                    children: [
                      GenderAge(data["user_sex"] == 1, data["user_birthday"]),
                      SizedBox(width: 5.w),
                      ConfigManager().userWealthCharmIcon(
                          data["user_grade"]["wealth"]["badge"],
                          data["user_grade"]["wealth"]["grade"],
                          marginRight: 5.w,
                          isWealth: true),
                      ConfigManager().userWealthCharmIcon(
                          data["user_grade"]["charm"]["badge"],
                          data["user_grade"]["charm"]["grade"],
                          marginRight: 5.w),
                    ],
                  )
                ],
              ),
            ),
            const Spacer(),
            Row(
              children: [
                LoadAssetImage("person/myGold", width: 26.w),
                FText(
                  NumberFormat("#,##0", "en_US").format(data["mic_heart_num"]),
                  color: const Color(0xFFFF753F),
                  size: 26.sp,
                  weight: FontWeight.bold,
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
