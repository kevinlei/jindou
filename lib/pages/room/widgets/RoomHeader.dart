import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class RoomHeader extends StatefulWidget {
  final int roomId;
  const RoomHeader(this.roomId, {super.key});

  @override
  State<RoomHeader> createState() => _RoomHeaderState();
}

class _RoomHeaderState extends State<RoomHeader> {
  RoomProvider roomProvider = RoomProvider();

  // 热度
  int charm = 0;
  // 人数
  int peopleNum = 1;

  @override
  void initState() {
    super.initState();
    EventUtils().on(
      EventConstants.E_ROOM_PEP_CHART,
      EventCall("RoomHeader", (data) {
        if (AppManager().roomInfo.roomMode == null) return;
        charm = AppManager().roomInfo.roomMode!.roomHot;
        peopleNum = AppManager().roomInfo.roomMode!.peopleNum;
        setState(() {});
      }),
    );

    if (widget.roomId == AppManager().roomInfo.roomMode?.exteriorRoomId) {
      charm = AppManager().roomInfo.roomMode?.roomHot ?? 0;
      peopleNum = AppManager().roomInfo.roomMode?.peopleNum ?? 1;
    }
  }

  @override
  void dispose() {
    super.dispose();
    EventUtils().off(
        EventConstants.E_ROOM_PEP_CHART, EventCall("RoomHeader", (data) {}));
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<RoomProvider>(builder: (_, pro, __) {
      return FContainer(
        padding: EdgeInsets.only(right: 20.w),
        height: 100.w,
        color: Colors.transparent,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            headerLeft(pro),
            headerRight(pro),
          ],
        ),
      );
    });
  }

  Widget headerLeft(RoomProvider pro) {
    String cover = pro.roomModel?.headIcon ?? "";
    return FContainer(
      padding:
          EdgeInsets.only(left: 20.w, top: 10.w, bottom: 10.w, right: 10.w),
      radius: const BorderRadius.only(
          topRight: Radius.circular(50), bottomRight: Radius.circular(50)),
      color: Colors.black.withOpacity(0.7),
      child: Row(
        children: [
          GestureDetector(
            onTap: () => onTapRoomInfo(pro),
            child: Row(
              children: [
                FContainer(
                  width: 65.w,
                  height: 65.w,
                  color: Colors.transparent,
                  radius: BorderRadius.circular(50),
                  image: cover.isNotEmpty
                      ? ImageUtils()
                          .getImageProvider(NetConstants.ossPath + cover)
                      : null,
                  imageFit: BoxFit.cover,
                ),
                SizedBox(width: 10.w),
                FContainer(
                  height: 80.w,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      FContainer(
                        width: 140.w,
                        child: ScrollText(
                          maxWidth: 140.w,
                          TextSpan(
                            style: TextStyle(
                              fontSize: 26.h,
                              color: Colors.white,
                            ),
                            text: pro.roomModel?.roomName ?? "",
                          ),
                        ),
                      ),
                      SizedBox(height: 2.w),
                      headerLeftSwiper(pro),
                    ],
                  ),
                )
              ],
            ),
          ),
          SizedBox(width: 10.w),
          Visibility(
            visible: (pro.roomModel?.personalPermission ?? 4) < 4 ||
                (AppManager().roomInfo.roomMode?.isFollow ?? 2) == 2,
            child: GestureDetector(
              onTap: () => setRoom(
                  ((pro.roomModel?.personalPermission ?? 4) < 4) ||
                      (UserConstants.userginInfo!.info.userRole >
                          UserRole.VIP.value),
                  pro),
              child: FContainer(
                color: Colors.transparent,
                child: LoadAssetImage(
                  ((pro.roomModel?.personalPermission ?? 4) < 4) ||
                          (UserConstants.userginInfo!.info.userRole >
                              UserRole.VIP.value)
                      ? "room/adminSetUpp"
                      : "room/collect",
                  width: 55.w,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget headerLeftSwiper(RoomProvider pro) {
    return FContainer(
      width: 120.w,
      height: 30.w,
      padding: EdgeInsets.only(left: 3.w),
      child: Swiper(
        scrollDirection: Axis.vertical,
        itemBuilder: (_, index) {
          if (index == 0) {
            return roomIdWdg(pro);
          } else {
            return roomHeatWdg(pro);
          }
        },
        itemCount: 2,
        autoplay: true,
        loop: true,
      ),
    );
  }

  Widget roomIdWdg(RoomProvider pro) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        LoadAssetImage(
          "room/beautiful",
          width: 28.w,
        ),
        SizedBox(width: 5.w),
        Padding(
          padding: EdgeInsets.only(top: 2.w),
          child: FText(
            "${pro.roomModel?.exteriorRoomId ?? 0}",
            size: 16.sp,
            color: const Color(0xFFDDDDDD).withOpacity(0.8),
            overflow: TextOverflow.ellipsis,
          ),
        )
      ],
    );
  }

  Widget roomHeatWdg(RoomProvider pro) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        LoadAssetImage("room/hot", width: 20.w, height: 20.w),
        Padding(
          padding: EdgeInsets.only(top: 2.w),
          child: FText(
            "$charm",
            size: 16.sp,
            color: const Color(0xFFDDDDDD).withOpacity(0.8),
            overflow: TextOverflow.ellipsis,
          ),
        )
      ],
    );
  }

  Widget headerRight(RoomProvider pro) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        GestureDetector(
          onTap: onTapTheCharts,
          child: userList(pro),
        ),
        SizedBox(width: 20.w),
        GestureDetector(
          onTap: onTapSetUp,
          child: FContainer(
            color: Colors.transparent,
            padding: EdgeInsets.only(top: 9.w),
            child: LoadAssetImage(
              "room/setUp",
              width: 50.w,
            ),
          ),
        ),
      ],
    );
  }

  Widget userList(RoomProvider pro) {
    List<Top3UserInfo> userList = pro.roomModel?.top3UserInfo ?? [];
    List<Widget> list = [];
    for (var i = 0; i < 4; i++) {
      late Widget child;
      if (i != 0) {
        String img = "room/one";
        if (i == 2) {
          img = "room/two";
        }
        if (i == 1) {
          img = "room/three";
        }
        child = Positioned(
          left: (4 - i - 1) * 50.w,
          child: Stack(
            children: [
              Positioned(
                top: 9.w,
                left: 4.w,
                child: (4 - i) > userList.length
                    ? LoadAssetImage(
                        "room/seat",
                        width: 60.w,
                        height: 60.w,
                        fit: BoxFit.cover,
                      )
                    : FContainer(
                        width: 60.w,
                        height: 60.w,
                        radius: BorderRadius.circular(50),
                        imageFit: BoxFit.cover,
                        image: ImageUtils().getImageProvider(
                            NetConstants.ossPath +
                                userList[4 - i - 1].userImage),
                      ),
              ),
              LoadAssetImage(
                img,
                width: 69.w,
                height: 69.w,
              ),
            ],
          ),
        );
      } else {
        child = Positioned(
          top: 9.w,
          left: (4 - i - 1) * 53.w,
          child: FContainer(
            width: 60.w,
            height: 60.w,
            radius: BorderRadius.circular(50),
            border: Border.all(color: const Color(0xFFB9B9BE)),
            align: Alignment.center,
            child: FText(
              "${peopleNum > 99 ? "99+" : peopleNum}",
              size: 18.sp,
            ),
          ),
        );
      }
      list.add(child);
    }

    return SizedBox(
      width: list.length * 53.w + 10.w,
      height: 70.w,
      child: Stack(
        alignment: AlignmentDirectional.center,
        children: list,
      ),
    );
  }

  Widget setUp() {
    return Stack(
      children: [
        FContainer(
          padding: EdgeInsets.only(
              top: ScreenUtil().statusBarHeight + 40.w,
              left: 40.w,
              right: 40.w,
              bottom: 40.w),
          radius: BorderRadius.only(
              bottomLeft: Radius.circular(30.w),
              bottomRight: Radius.circular(30.w)),
          width: double.infinity,
          height: ScreenUtil().statusBarHeight + 250.w,
          color: Colors.black,
          align: Alignment.center,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              // btnCell("share", "分享", onTapShare),
              btnCell("zoom", "最小化", onTapZoom),
              btnCell("exit", "退出", onTapExit),
            ],
          ),
        )
      ],
    );
  }

  Widget btnCell(String img, String text, Function() onTap) {
    return Column(mainAxisAlignment: MainAxisAlignment.center, children: [
      GestureDetector(
          onTap: onTap,
          child: FContainer(
            radius: BorderRadius.circular(50),
            color: Colors.white.withOpacity(0.1),
            child: LoadAssetImage("room/$img", width: 110.w, height: 110.w),
          )),
      SizedBox(height: 10.w),
      FText(text, color: Colors.white.withOpacity(0.6), size: 24.sp)
    ]);
  }

  void onTapSetUp() async {
    await WidgetUtils().showFDialog(setUp(), useSafeArea: false);
  }

  // 最小化
  void onTapZoom() {
    WidgetUtils().popPage();
    WidgetUtils().popPage();
  }

  void onTapExit() async {
    bool? accept = await WidgetUtils().showAlert("确定退出房间吗？");
    if (accept == null || !accept) return;
    AppManager().quiteRoom();
  }

  // 魅力值/财富值
  void onTapTheCharts() {
    WidgetUtils()
        .showBottomSheet(const RoomTheCharts(), color: Colors.transparent);
  }

  // 房间信息
  void onTapRoomInfo(RoomProvider pro) {
    WidgetUtils().showBottomSheet(RoomsInfo(pro), color: Colors.transparent);
  }

  // 房间设置
  void setRoom(bool isAdmin, RoomProvider pro) {
    if (isAdmin) {
      WidgetUtils().showBottomSheet(SetRoom(pro), color: Colors.transparent);
    } else {
      roomCollect(pro.roomModel!.isFollow == 2, pro);
    }
  }

  // 收藏/取消收藏房间
  void roomCollect(bool isCollect, RoomProvider pro) async {
    if (!isCollect) {
      bool? accept = await WidgetUtils().showAlert("确定取消关注房间吗？");
      if (accept == null || !accept) return;
    }
    await DioUtils().httpRequest(
      NetConstants.collectRoom,
      method: DioMethod.POST,
      params: {
        "exterior_room_id": pro.roomModel!.exteriorRoomId,
        "follow_type": isCollect ? 1 : 2
      },
      loading: false,
      onSuccess: (data) {
        if (isCollect) {
          pro.roomModel!.isFollow = 1;
        } else {
          pro.roomModel!.isFollow = 2;
        }
      },
    );
  }
}
