import 'dart:convert';
import 'package:im_flutter_sdk/im_flutter_sdk.dart';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class RoomsEmo extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => RoomsEmoState();
}

class RoomsEmoState extends State<RoomsEmo> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      mainAxisSize: MainAxisSize.min,
      children: [
        SizedBox(
          height: 370,
          child: EmoDialog(),
        )
      ],
    );
  }
}

/// -------------- 表情列表
class EmoDialog extends StatefulWidget {
  @override
  _EmoDialogState createState() => _EmoDialogState();
}

class _EmoDialogState extends State<EmoDialog> {
  final tables = ["默认", "工具"];
  int curPage = 0;
  PageController controller = PageController();
  Random random = Random();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        left: 20.w,
        right: 20.w,
        top: 20.w,
        bottom: ScreenUtil().bottomBarHeight + 20.w,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          tableContent(),
          SizedBox(height: 20.w),
          Expanded(
            child: PageView.builder(
              itemBuilder: (_, index) => listContent(index),
              itemCount: 2,
              onPageChanged: onPageChanged,
              controller: controller,
            ),
          )
        ],
      ),
    );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  Widget tableContent() {
    return Row(
      children: List.generate(tables.length, (index) => tabCell(index)),
    );
  }

  Widget tabCell(int page) {
    Color color =
        curPage == page ? Colors.white : Colors.white.withOpacity(0.5);
    FontWeight weight = curPage == page ? FontWeight.bold : FontWeight.normal;
    Widget btn = FContainer(
      width: 120.w,
      height: 60.w,
      child: FText(tables[page], color: color, size: 32.sp, weight: weight),
    );
    return GestureDetector(child: btn, onTap: () => onTapPage(page));
  }

  Widget searchLine() {
    return FContainer(
      width: 65.w,
      height: 8.w,
      radius: BorderRadius.circular(8.w),
      color: const Color(0xFFA852FF),
    );
  }

  Widget listContent(int page) {
    int length = page == 0 ? (ConfigManager().emotionConfig.length - 1) : 1;
    return GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 5,
          mainAxisExtent: 150.w,
          mainAxisSpacing: 20.w,
          crossAxisSpacing: 20.w,
        ),
        itemCount: length,
        itemBuilder: (_, index) => emoCell(page, index));
  }

  Widget emoCell(int page, int index) {
    Widget imageWidget;
    String name = "", image = "";
    if (page == 0) {
      image = ConfigManager().emotionConfig[index + 1].picture;
      imageWidget =
          LoadNetImage(NetConstants.ossPath + image, fit: BoxFit.contain);
      name = ConfigManager().emotionConfig[index + 1].name;
    } else {
      if (index == 0) {
        image = ConfigManager().emotionConfig[0].picture;
        imageWidget =
            LoadNetImage(NetConstants.ossPath + image, fit: BoxFit.contain);
        name = ConfigManager().emotionConfig[0].name;
      } else {
        imageWidget = const LoadAssetImage("none");
        name = "抽签";
      }
    }
    return GestureDetector(
      onTap: () => onTapEmo(image),
      child: FContainer(
        child: Column(children: [
          Expanded(
            child: AspectRatio(aspectRatio: 1.0, child: imageWidget),
          ),
          SizedBox(height: 10.w),
          FText(name, size: 24.sp, color: const Color(0xFF999999))
        ]),
      ),
    );
  }

  void onTapPage(int page) {
    if (page == curPage) return;
    controller.jumpToPage(page);
  }

  void onPageChanged(int page) {
    curPage = page;
    if (!mounted) return;
    setState(() {});
  }

  void onTapEmo(String image) async {
    if (AppManager().roomInfo.roomMode == null) return;

    int? isShut = AppManager().roomInfo.roomMode?.isBanChat;
    if (isShut == 1) return WidgetUtils().showToast("您已被禁言");

    int roomRole = AppManager().roomInfo.roomMode?.personalPermission ?? 4;
    int roomId = AppManager().roomInfo.roomMode!.exteriorRoomId;
    String content = jsonEncode({
      "content": image,
      "event": MqMsgType.ROOM_SEND_EMO.value,
      "info": UserConstants.getExtUser()!.toJson(),
      "roomRole": roomRole,
      "hxinfo": UserConstants.userginInfo!.info.hxInfo.toJson(),
      "roomId": roomId.toString(),
    });

    EMMessage message = await AppManager().hxEngine.sendHxRoomMsg(
        AppManager().roomInfo.roomMode!.hxId.toString(), content);
    AppManager().roomInfo.roomMsg.add(message);

    WidgetUtils().popPage();

    EventUtils().emit(EventConstants.E_ROOM_MSG_UPDATE, null);
    EventUtils().emit(EventConstants.E_ROOM_EMO,
        [UserConstants.userginInfo!.info.userId, image]);
  }
}
