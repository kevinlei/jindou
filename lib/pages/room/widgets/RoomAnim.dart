import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class RoomAnim extends StatefulWidget {
  const RoomAnim({super.key});

  @override
  State<RoomAnim> createState() => _RoomAnimState();
}

class _RoomAnimState extends State<RoomAnim>
    with SingleTickerProviderStateMixin {
  late SVGAAnimationController animCtr;

  // 礼物特效列表
  List<GiftMq> animList = [];

  // 动画是否执行中
  bool isAnim = false;

  String curAnim = "";

  @override
  void initState() {
    super.initState();
    if (!mounted) return;
    EventUtils().on(
      EventConstants.E_ROOM_GIFT,
      EventCall("RoomAnim", (v) {
        if (v.obtainGiftInfo.giftVfx != null &&
            v.obtainGiftInfo.giftVfx != "") {
          animList.add(v);
          if (!mounted) return;
          setState(() {});
          if (animCtr.status != AnimationStatus.forward ||
              animCtr.status == AnimationStatus.completed) {
            startShowAnim();
          }
        }
      }),
    );
    animCtr = SVGAAnimationController(vsync: this);
  }

  @override
  void dispose() {
    EventUtils().off(EventConstants.E_ROOM_GIFT, EventCall("RoomAnim", (v) {}));
    animCtr.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
        child: Center(
            child: Visibility(visible: isAnim, child: SVGAImage(animCtr))));
  }

  void startShowAnim() async {
    if (!AppManager().openEffect) {
      animList.clear();
      return setState(() {});
    }
    if (animList.isEmpty || animCtr.isAnimating == true || isAnim) return;
    isAnim = true;
    setState(() {});
    curAnim = animList.removeAt(0).obtainGiftInfo.giftVfx!;
    if (curAnim.endsWith(".svga")) {
      onStartSvgAnim(curAnim);
    }
  }

  void onStartSvgAnim(String anim) async {
    MovieEntity? videoItem;
    try {
      videoItem =
          await SVGAParser.shared.decodeFromURL(NetConstants.ossPath + anim);
      videoItem.autorelease = false;
    } catch (e) {
      videoItem = null;
    }
    if (videoItem == null) return;
    animCtr.videoItem = videoItem;
    animCtr.reset();
    animCtr.forward().whenComplete(() {
      isAnim = false;
      animCtr.videoItem = null;
      setState(() {});
      startShowAnim();
    });
  }
}
