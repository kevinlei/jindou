import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class RoomMore extends StatefulWidget {
  const RoomMore({super.key});

  @override
  State<RoomMore> createState() => _RoomMoreState();
}

class _RoomMoreState extends State<RoomMore> {
  // 是否开启心动值
  bool isToLove = true;
  // 是否开启声音
  bool isSound = true;
  // 是否开启礼物特效
  bool isGiftEffects = true;
  // 游戏列表
  List gameList = [];

  @override
  void initState() {
    super.initState();
    isSound = AppManager().roomInfo.isSound;
    isGiftEffects = AppManager().openEffect;
    isToLove = AppManager().roomInfo.roomMode!.isHeart == 2;
    requestGameList();
  }

  void requestGameList() async {
    DioUtils().asyncHttpRequest(
      NetConstants.banner,
      loading: false,
      params: {
        "type": BanbarSwper.ROOM_GAME_COM.brand,
        "position": 2,
      },
      onSuccess: (res) {
        if (res == null || !mounted) return;
        gameList = res;
        setState(() {});
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> list = [];
    if (AppManager().roomInfo.roomMode!.personalPermission < 4 ||
        UserConstants.userginInfo!.info.userRole > UserRole.DEF.value) {
      list.addAll([
        btnCell("room/cardiacValue", "${isToLove ? "关闭" : "开启"}心动值",
            onTapLoveValue),
        // btnCell("room/roomPk", "房间PK", () {}),
        btnCell("room/seatPrice", "麦位流水", onTapBill),
      ]);
    }
    if (AppManager().roomInfo.isMaishang) {
      list.addAll([
        btnCell("room/bgMusic", "背景音乐", onTapBgMusic),
      ]);
    }
    list.addAll([
      btnCell("room/mixer", "调音台", onTapMixer),
      btnCell("room/setSound", "${isSound ? "关闭" : "开启"}声音", onTapSound),
      btnCell("room/specialEffects", "${isGiftEffects ? "关闭" : "开启"}特效",
          onTapGiftSpecialEffects),
      // btnCell(
      //     "room/customWelcomeMessage", "自定义欢迎语", onTapWelcomeMessage),
      // btnCell("room/InviteFriends", "邀请好友", () {}),
    ]);
    return FContainer(
      padding: EdgeInsets.only(
          top: 50.w,
          left: 30.w,
          right: 30.w,
          bottom: ScreenUtil().bottomBarHeight + 50.w),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          FText("房间工具", size: 34.sp),
          SizedBox(height: 20.w),
          GridView.count(
            crossAxisCount: 5,
            mainAxisSpacing: 20.w,
            crossAxisSpacing: 8.w,
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            children: list,
          ),
          Visibility(
            visible: (UserConstants.userginInfo!.gradeInfo.userGrade.wealth
                        .nowExperience ??
                    0) >=
                AppManager().roomInfo.isWealthRestrict,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 40.w),
                FText("房间玩法", size: 34.sp),
                SizedBox(height: 20.w),
                GridView.count(
                  crossAxisCount: 5,
                  mainAxisSpacing: 20.w,
                  crossAxisSpacing: 8.w,
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  children: games(),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget btnCell(String img, String text, void Function() onTap,
      {bool net = false}) {
    return GestureDetector(
      onTap: onTap,
      child: Column(
        children: [
          Expanded(
              child: net
                  ? CircleAvatar(
                      radius: 50,
                      backgroundColor: Colors.transparent,
                      backgroundImage: NetworkImage(NetConstants.ossPath + img),
                    )
                  : LoadAssetImage(img)),
          SizedBox(height: 10.h),
          FText(text, size: 20.sp, color: const Color(0xFF999999))
        ],
      ),
    );
  }

  Widget gameCell(String img, String text, void Function() onTap,
      {bool net = false}) {
    return GestureDetector(
      onTap: onTap,
      child: Column(
        children: [
          Expanded(
              child: net
                  ? CircleAvatar(
                      radius: 50,
                      backgroundColor: Colors.transparent,
                      backgroundImage: NetworkImage(NetConstants.ossPath + img),
                    )
                  : LoadNetImage(NetConstants.ossPath + img)),
          SizedBox(height: 10.h),
          FText(text, size: 20.sp, color: const Color(0xFF999999))
        ],
      ),
    );
  }

  // 游戏列表
  List<Widget> games() {
    List<Widget> list = [];
    for (var game in gameList) {
      list.add(gameCell(game["img_url"], game["title"], () => onTapGame(game)));
    }
    return list;
  }

  void onTapGame(game) {
    if (AppManager().time == 999) return WidgetUtils().showToast("网络异常");
    String linkUrl = game["link_url"];
    if (linkUrl.isEmpty) return;
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        isDismissible: false,
        enableDrag: false,
        builder: (_) => RoomGame(url: linkUrl));
  }

  //  背景音乐
  void onTapBgMusic() {
    WidgetUtils().showBottomSheet(const RoomBgMusicSet(),
        color: const Color(0xFF1D1928));
  }

  // 调音台
  void onTapMixer() {
    WidgetUtils().showBottomSheet(
      const RoomMixer(),
      color: const Color(0xFF1D1928),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(20.w),
          topLeft: Radius.circular(20.w),
        ),
      ),
    );
  }

  // 开启关闭心动值
  void onTapLoveValue() async {
    if (isToLove) {
      bool? accept = await WidgetUtils().showAlert("确定要清空并关闭心动值吗?");
      if (accept != true) return;
    }
    await DioUtils().httpRequest(
      NetConstants.setRoomMode,
      method: DioMethod.POST,
      loading: false,
      params: {
        "room_id": AppManager().roomInfo.roomMode!.roomId,
        "is_heart": isToLove ? 1 : 2,
      },
      onSuccess: (data) async {
        isToLove = !isToLove;
        setState(() {});
      },
    );
  }

  // 麦位流水
  void onTapBill() {
    WidgetUtils().showFDialog(RoomBill());
  }

  // 开启关闭声音
  void onTapSound() {
    AppManager().switchSound(!isSound);
    setState(() {
      isSound = !isSound;
    });
  }

  // 开启关闭礼物特效
  void onTapGiftSpecialEffects() {
    AppManager().openEffect = !isGiftEffects;
    setState(() {
      isGiftEffects = !isGiftEffects;
    });
  }

  // 自定义欢迎语
  void onTapWelcomeMessage() {
    WidgetUtils().showBottomSheet(
      const CustomWelcomeMessage(),
      color: const Color(0xFF1D1928),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(20.w),
          topLeft: Radius.circular(20.w),
        ),
      ),
    );
  }
}
