import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:echo/import.dart';
import 'package:im_flutter_sdk/im_flutter_sdk.dart';

class RoomChatCell extends StatefulWidget {
  final EMMessage message;
  final bool label;
  final bool miniMode;

  const RoomChatCell(this.message, this.miniMode, {Key? key, this.label = true})
      : super(key: key);

  @override
  State<RoomChatCell> createState() => _RoomChatCellState();
}

class _RoomChatCellState extends State<RoomChatCell> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    widget.message.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    EMTextMessageBody msg = widget.message.body as EMTextMessageBody;
    var data = json.decode(msg.content);
    int code = data?["event"] ?? 0;
    late Widget child;
    MqMsgType msgType = AppManager().getMqMsgType(code);
    switch (msgType) {
      case MqMsgType.SELF_JOIN:
        child = selfJoinCell(data);
        break;
      case MqMsgType.ROOM_SEND_CHAT:
        child = userChatCell(data);
        break;
      case MqMsgType.ROOM_SEND_EMO:
        child = emoChatCell(data);
        break;
      case MqMsgType.USER_ROOM_ACCESS:
        child = userJoinCell(data);
        break;
      case MqMsgType.ROOM_GIFT:
        child = userGiftCell(data);
        break;
      case MqMsgType.ROOM_GAME_GIFT:
        child = gameGiftCell(data);
        break;
      default:
        child = const SizedBox();
    }
    return FContainer(
      padding: EdgeInsets.symmetric(vertical: 10.w),
      child: child,
    );
  }

  Widget selfJoinCell(data) {
    int roomRole = AppManager().roomInfo.roomMode!.personalPermission;
    ExtUser? user = UserConstants.getExtUser();
    if (user == null) return const SizedBox();
    return GestureDetector(
      onTap: () => WidgetUtils()
          .showBottomSheet(RoomUserInfo(user.id), color: Colors.transparent),
      child: Wrap(crossAxisAlignment: WrapCrossAlignment.center, children: [
        userInfo(roomRole, user, false),
        FText(" 加入直播间", size: 22.sp, weight: FontWeight.w600),
      ]),
    );
  }

  // {"info": ExtUser,"roomRole":60,"type":10000}
  Widget userJoinCell(data) {
    ExtUser user = ExtUser.fromJson(data["info"]);
    int roomRole = data["info"]["room_role"] ?? RoomRole.DEF.value;
    RoomRole.DEF.value;
    return GestureDetector(
      onTap: () => WidgetUtils()
          .showBottomSheet(RoomUserInfo(user.id), color: Colors.transparent),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          userInfo(roomRole, user, false),
          FText(" 加入直播间", size: 22.sp, weight: FontWeight.w600),
        ],
      ),
    );
  }

  // {"info": ExtUser,"roomRole":60,"type":10000, "content": "", "chatType": 1}
  Widget userChatCell(data) {
    int roomRole = data["roomRole"] ?? RoomRole.DEF.value;
    ExtUser user = ExtUser.fromJson(data["info"]);
    String content = data["content"] ?? "";

    Widget? child;
    if (content.startsWith("#[@")) {
      int endIndex = content.indexOf("]#");
      String startText = content.substring(2, endIndex);
      String endText = content.substring(endIndex + 2);

      child = FRichText([
        FRichTextItem()
          ..text = "${AppConstants().flutterText(startText)} "
          ..weight = FontWeight.w600
          ..color = const Color(0xFF50B5FF),
        FRichTextItem()
          ..text = AppConstants().flutterText(endText)
          ..weight = FontWeight.w600
          ..color = Colors.white,
      ], fontSize: 22.sp, lines: 100);
    } else {
      child = FText(
        AppConstants().flutterText(content),
        size: 22.sp,
        weight: FontWeight.w600,
        maxLines: 100,
      );
    }

    Widget bubbleChatBg = FContainer(
      color: Colors.white.withOpacity(0.3),
      padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 10.w),
      radius: BorderRadius.circular(40.w),
      child: child,
    );

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GestureDetector(
          onTap: () => WidgetUtils().showBottomSheet(RoomUserInfo(user.id),
              color: Colors.transparent),
          child: userInfo(roomRole, user, false),
        ),
        SizedBox(height: 10.w),
        bubbleChatBg,
      ],
    );
  }

  Widget userGiftCell(data) {
    GiftMq giftMqInfo = GiftMq.fromJson(data["info"]);
    List<FRichTextItem> textList = [
      FRichTextItem()
        ..text = AppConstants().flutterText(giftMqInfo.operateUserInfo.name)
        ..weight = FontWeight.w600
        ..color = const Color(0xFFFFA254),
      FRichTextItem()
        ..text = " 送给了 "
        ..color = Colors.white.withOpacity(0.8)
        ..weight = FontWeight.w600,
      FRichTextItem()
        ..text =
            AppConstants().flutterText("${giftMqInfo.obtainUserInfo.name} ")
        ..weight = FontWeight.w600
        ..color = const Color(0xFFFFA254),
      FRichTextItem()
        ..text =
            AppConstants().flutterText("${giftMqInfo.obtainGiftInfo.giftName} ")
        ..weight = FontWeight.w600
        ..color = const Color(0xFFFF367B),
    ];

    return FContainer(
      color: const Color(0xFF492CFF).withOpacity(0.3),
      padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 10.w),
      radius: BorderRadius.circular(40.w),
      child: Wrap(
        crossAxisAlignment: WrapCrossAlignment.center,
        children: [
          FRichText(textList, fontSize: 22.sp, color: Colors.white),
          FContainer(
            width: 40.w,
            height: 40.w,
            imageFit: BoxFit.cover,
            image: ImageUtils().getImageProvider(
                NetConstants.ossPath + giftMqInfo.obtainGiftInfo.giftPicture),
          ),
          FText(
            " x ${giftMqInfo.obtainGiftInfo.giftNum}",
            size: 22.sp,
            color: const Color(0xFFFF367B),
          )
        ],
      ),
    );
  }

  Widget emoChatCell(data) {
    int roomRole = data["roomRole"] ?? RoomRole.DEF.value;
    ExtUser user = ExtUser.fromJson(data["info"]);
    String content = data["content"] ?? "";

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GestureDetector(
          onTap: () => WidgetUtils().showBottomSheet(RoomUserInfo(user.id),
              color: Colors.transparent),
          child: userInfo(roomRole, user, false),
        ),
        SizedBox(height: 10.w),
        LoadNetImage(
          NetConstants.ossPath + content,
          width: 100.w,
          height: 100.w,
        ),
      ],
    );
  }

  Widget gameGiftCell(data) {
    GameInfo gameMqInfo = GameInfo.fromJson(data["info"]);
    List<FRichTextItem> giftTexts = [];
    for (var i = 0; i < gameMqInfo.giftList!.length; i++) {
      giftTexts.add(FRichTextItem()
        ..text =
            "${AppConstants().flutterText(gameMqInfo.giftList![i].giftName)}(${gameMqInfo.giftList![i].giftPrice}) x${gameMqInfo.giftList![i].giftNum}"
        ..weight = FontWeight.w600
        ..color = const Color(0xFFFF367B));
    }
    List<FRichTextItem> textList = [
      FRichTextItem()
        ..text = "${AppConstants().flutterText(gameMqInfo.userName)} 在 "
        ..color = Colors.white.withOpacity(0.8)
        ..weight = FontWeight.w600,
      FRichTextItem()
        ..text = AppConstants().flutterText(gameMqInfo.gameName)
        ..weight = FontWeight.w600
        ..color = const Color(0xFFFFD300),
      FRichTextItem()
        ..text = " 获得 "
        ..color = Colors.white.withOpacity(0.8)
        ..weight = FontWeight.w600,
      ...giftTexts,
      FRichTextItem()
        ..text = " 欧气爆棚~"
        ..color = Colors.white.withOpacity(0.8)
        ..weight = FontWeight.w600,
    ];

    return FContainer(
      color: const Color(0xFFFF367B).withOpacity(0.3),
      padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 10.w),
      radius: BorderRadius.circular(40.w),
      child: Wrap(
        crossAxisAlignment: WrapCrossAlignment.center,
        children: [FRichText(textList, fontSize: 22.sp, color: Colors.white)],
      ),
    );
  }

  Widget userInfo(int roomRole, ExtUser? user, bool isSelf) {
    String userName = "";

    String charmBadge = isSelf
        ? (UserConstants.userginInfo!.gradeInfo.userGrade.charm.badge)
        : user?.charmBadge ?? "";

    String wealthBadge = isSelf
        ? (UserConstants.userginInfo!.gradeInfo.userGrade.wealth.badge)
        : user?.wealthBadge ?? "";

    int charmGrade = isSelf
        ? (UserConstants.userginInfo!.gradeInfo.userGrade.charm.grade)
        : user?.charmGrade ?? 1;

    int wealthGrade = isSelf
        ? (UserConstants.userginInfo!.gradeInfo.userGrade.wealth.grade)
        : user?.wealthGrade ?? 1;

    if (isSelf) {
      userName = UserConstants.userginInfo?.info.name ?? "";
    } else {
      userName = user?.name ?? "";
    }
    return Row(mainAxisSize: MainAxisSize.min, children: [
      ConfigManager().userIcon(
          AppManager().getUserRole(
              isSelf ? UserConstants.userginInfo?.info.userRole : user?.role),
          paddingRight: 5.w),
      Visibility(
          visible: widget.label,
          child: ConfigManager()
              .roomIcon(AppManager().getRoomRole(roomRole), paddingRight: 5.w)),
      ConfigManager().userWealthCharmIcon(wealthBadge, wealthGrade,
          marginRight: 5.w, isWealth: true),
      ConfigManager()
          .userWealthCharmIcon(charmBadge, charmGrade, marginRight: 5.w),
      FText(
        AppConstants().flutterText(userName),
        overflow: TextOverflow.ellipsis,
        weight: FontWeight.w600,
        color: Colors.white.withOpacity(0.5),
        size: 22.sp,
      ),
    ]);
  }
}
