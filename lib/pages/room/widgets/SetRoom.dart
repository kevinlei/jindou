import 'dart:convert';

import 'package:echo/import.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';

// 房间设置
class SetRoom extends StatefulWidget {
  final RoomProvider pro;
  const SetRoom(this.pro, {super.key});

  @override
  State<SetRoom> createState() => _SetRoomState();
}

class _SetRoomState extends State<SetRoom> {
  // 房间头像
  String roomhead = "";
  // 房间名称
  String roomName = "";
  // 房间公告
  String notion = "";
  // 房间背景图片
  String roomBg = "";
  // 是否打开公屏
  bool isChat = true;

  @override
  void initState() {
    super.initState();
    roomhead = widget.pro.roomModel?.headIcon ?? "";
    roomName = widget.pro.roomModel?.roomName ?? "";
    notion = widget.pro.roomModel?.roomBulletin ?? "";
    roomBg = widget.pro.roomModel?.bgImg ?? "";
    isChat = (widget.pro.roomModel?.roomIsChat ?? 2) == 2 ? true : false;
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      child: Stack(
        alignment: AlignmentDirectional.topCenter,
        children: [
          infoBody(),
          infoHeader(),
        ],
      ),
    );
  }

  Widget infoHeader() {
    Widget child = FContainer(
      width: 180.w,
      height: 180.w,
      radius: BorderRadius.circular(40.w),
      image: ImageUtils().getImageProvider(NetConstants.ossPath + roomhead),
      imageFit: BoxFit.cover,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          FContainer(
            constraints: BoxConstraints(maxHeight: 40.w),
            height: 40.w,
            color: Colors.black.withOpacity(0.4),
            radius: BorderRadius.only(
                bottomLeft: Radius.circular(40.w),
                bottomRight: Radius.circular(40.w)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(Icons.party_mode, size: 22.sp, color: Colors.white),
                SizedBox(width: 5.w),
                FText(
                  "更换封面",
                  size: 18.sp,
                )
              ],
            ),
          )
        ],
      ),
    );

    return GestureDetector(
      onTap: onTapHead,
      child: child,
    );
  }

  Widget infoBody() {
    return FContainer(
      margin: EdgeInsets.only(top: 90.w),
      padding: EdgeInsets.only(
          top: 90.w,
          left: 40.w,
          right: 40.w,
          bottom: ScreenUtil().bottomBarHeight + 50.w),
      width: double.infinity,
      radius: BorderRadius.only(
          topLeft: Radius.circular(40.w), topRight: Radius.circular(40.w)),
      color: const Color(0xFF1D1928),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          item("房间名称", roomName, 10, onTapName),
          // item("欢迎语", "陪伴", 30),
          item("房间公告", notion, 300, onTapNotice, heigth: 200.w),
          itemSetRoomBg(),
          setList(),
        ],
      ),
    );
  }

  Widget item(
      String text, String span, int maxLength, void Function(String) onTap,
      {double? heigth}) {
    return Padding(
      padding: EdgeInsets.only(top: 30.w),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          FText(
            text,
            size: 26.sp,
            weight: FontWeight.w300,
          ),
          SizedBox(height: 20.w),
          GestureDetector(
            onTap: () async {
              await WidgetUtils().showBottomSheet(
                  setRoomInput(text, maxLength, onTap),
                  color: const Color(0xFF1D1928));
            },
            child: FContainer(
              constraints:
                  heigth != null ? BoxConstraints(minHeight: heigth) : null,
              width: double.infinity,
              height: heigth,
              padding: EdgeInsets.symmetric(vertical: 20.w, horizontal: 40.w),
              radius: BorderRadius.circular(50.w),
              color: Colors.white.withOpacity(0.1),
              child: heigth != null
                  ? ListView(
                      children: [
                        FText(
                          span,
                          size: 28.sp,
                          weight: FontWeight.w300,
                          maxLines: 100,
                        )
                      ],
                    )
                  : FText(
                      span,
                      size: 28.sp,
                      weight: FontWeight.w300,
                    ),
            ),
          )
        ],
      ),
    );
  }

  Widget itemSetRoomBg() {
    return GestureDetector(
      onTap: () {
        WidgetUtils().showBottomSheet(RoomBgList(roomBg, onTapBgImg),
            color: const Color(0xFF1D1928));
      },
      child: FContainer(
        padding: EdgeInsets.symmetric(vertical: 30.w),
        border:
            Border(bottom: BorderSide(color: Colors.white.withOpacity(0.15))),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            FText(
              "切换背景",
              size: 26.sp,
              weight: FontWeight.w300,
            ),
            Row(
              children: [
                FContainer(
                  width: 50.w,
                  height: 80.w,
                  image: ImageUtils()
                      .getImageProvider(NetConstants.ossPath + roomBg),
                  imageFit: BoxFit.fill,
                ),
                SizedBox(width: 5.w),
                LoadAssetImage(
                  "public/RightNavigation",
                  width: 25.w,
                  color: Colors.white,
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget setList() {
    return Padding(
      padding: EdgeInsets.only(top: 30.w),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          setItem("room/roomAdmin", "管理员", onTapAdmin),
          setItem("room/setUser", "用户管理", onTapUser),
          setItem("room/openChat", isChat ? "关闭公屏" : "打开公屏", onTapChat),
          setItem("room/seatMode", "上麦模式", onTapMode),
          setItem("room/roomLock", "房间加锁", onTapLock),
        ],
      ),
    );
  }

  Widget setItem(String img, String text, void Function() onTap) {
    return GestureDetector(
      onTap: onTap,
      child: Column(
        children: [
          LoadAssetImage(img, width: 100.w),
          SizedBox(height: 10.w),
          FText(
            text,
            color: Colors.white.withOpacity(0.6),
          )
        ],
      ),
    );
  }

  // 房间头像
  void onTapHead() async {
    final ImagePicker picker = ImagePicker();
    bool accept = await FuncUtils().requestPermission(Permission.photos);
    if (!accept) return;
    XFile? file = await picker.pickImage(source: ImageSource.gallery);
    if (file == null || file.path.isEmpty) return;

    WidgetUtils().showLoading();
    String? imagePath =
        await FuncUtils().requestUpload(file.path, UploadType.CHAT_IMAGE);
    if (imagePath == null) return WidgetUtils().cancelLoading();
    if (imagePath.isEmpty) {
      WidgetUtils().cancelLoading();
      return WidgetUtils().showToast("上传图片失败");
    }
    setRoomInfo(head: imagePath);
  }

  // 房间名称
  void onTapName(String name) async {
    setRoomInfo(name: name);
  }

  // 房间公告
  void onTapNotice(String roomNotion) async {
    setRoomInfo(roomNotion: roomNotion);
  }

  // 设置房间背景图片
  void onTapBgImg(String roomBgImg) async {
    setRoomInfo(roomBgImg: roomBgImg);
  }

  // 设置房间基本信息
  void setRoomInfo({
    String head = "",
    String name = "",
    String roomNotion = "",
    String roomBgImg = "",
  }) async {
    await DioUtils().httpRequest(
      NetConstants.setRoomInfo,
      method: DioMethod.POST,
      loading: false,
      params: {
        "exterior_room_id": AppManager().roomInfo.roomMode!.exteriorRoomId,
        "room_image": head,
        "room_name": name,
        "room_bulletin": roomNotion,
        "room_description": "",
        "room_background": roomBgImg,
      },
      onSuccess: (res) async {
        if (head.isNotEmpty) {
          roomhead = head;
        }
        if (name.isNotEmpty) {
          roomName = name;
          WidgetUtils().popPage();
        }
        if (roomNotion.isNotEmpty) {
          notion = roomNotion;
          WidgetUtils().popPage();
        }
        if (roomBgImg.isNotEmpty) {
          roomBg = roomBgImg;
          WidgetUtils().popPage();
        }
        setState(() {});
      },
    );
  }

  // 管理员设置
  void onTapAdmin() {
    WidgetUtils()
        .showBottomSheet(const SetRoomAdmin(), color: const Color(0xFF1D1928));
  }

  // 房间成员设置
  void onTapUser() {
    WidgetUtils()
        .showBottomSheet(const SetUser(), color: const Color(0xFF1D1928));
  }

  // 打开关闭公屏
  void onTapChat() async {
    bool? accept =
        await WidgetUtils().showAlert("是否确定${isChat ? "关闭" : "打开"}公屏");
    if (accept != true) return;
    await DioUtils().httpRequest(
      NetConstants.setRoomMode,
      method: DioMethod.POST,
      loading: false,
      params: {
        "room_id": AppManager().roomInfo.roomMode!.roomId,
        "is_chat": isChat ? 1 : 2,
      },
      onSuccess: (data) async {
        isChat = !isChat;
        setState(() {});
      },
    );
  }

  // 房间上麦模式
  void onTapMode() async {
    await WidgetUtils().showBottomSheet(SetRoomMode(widget.pro),
        color: const Color(0xFF1D1928));
  }

  // 房间锁
  void onTapLock() async {
    await WidgetUtils()
        .showBottomSheet(const SetRoomLock(), color: const Color(0xFF1D1928));
  }
}

// 设置房间名称/欢迎语/房间公告
class setRoomInput extends StatefulWidget {
  final String title;
  final int maxLength;
  final void Function(String) onTap;
  const setRoomInput(this.title, this.maxLength, this.onTap, {super.key});

  @override
  State<setRoomInput> createState() => _setRoomInputState();
}

class _setRoomInputState extends State<setRoomInput> {
  String textValue = "";

  @override
  Widget build(BuildContext context) {
    return FContainer(
      padding: EdgeInsets.only(
          top: 40.w,
          left: 40.w,
          right: 40.w,
          bottom: ScreenUtil().bottomBarHeight + 40.w),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          FText(
            widget.title,
            size: 30.sp,
          ),
          SizedBox(height: 40.w),
          input(),
          SizedBox(height: 20.w),
          btn(),
          SizedBox(height: MediaQuery.of(context).viewInsets.bottom),
        ],
      ),
    );
  }

  Widget input() {
    InputBorder border = OutlineInputBorder(
        borderRadius:
            BorderRadius.circular(widget.maxLength == 300 ? 20.w : 50),
        borderSide: const BorderSide(color: Colors.transparent));

    return TextField(
      style:
          TextStyle(color: Colors.white, fontSize: 30.sp, letterSpacing: 1.2),
      maxLength: widget.maxLength,
      maxLines: widget.maxLength == 300 ? 10 : 1,
      autofocus: true,
      onChanged: (value) {
        setState(() {
          textValue = value;
        });
      },
      decoration: InputDecoration(
        enabledBorder: border,
        focusedBorder: border,
        disabledBorder: border,
        filled: true,
        fillColor: Colors.white.withOpacity(0.1),
        counterStyle:
            TextStyle(color: const Color(0xFFBBBBBB), fontSize: 30.sp),
        hintText: "请输入新的${widget.title}~",
        contentPadding: const EdgeInsets.fromLTRB(12, 8, 12, 8),
        hintStyle: TextStyle(
            color: const Color(0xFFBBBBBB), fontSize: 30.sp, letterSpacing: 1),
      ),
    );
  }

  Widget btn() {
    return FButton(
      onTap: textValue.isNotEmpty ? () => widget.onTap(textValue) : null,
      width: double.infinity,
      height: 80.w,
      bgColor: textValue.isNotEmpty
          ? const Color(0xFFFF753F)
          : Colors.white.withOpacity(0.1),
      align: Alignment.center,
      child: FText(
        "保存修改",
        size: 32.sp,
      ),
    );
  }
}

// 背景列表
class RoomBgList extends StatefulWidget {
  final String img;
  final void Function(String) onTap;
  const RoomBgList(this.img, this.onTap, {super.key});

  @override
  State<RoomBgList> createState() => _RoomBgListState();
}

class _RoomBgListState extends State<RoomBgList> {
  // 当前选择背景图片
  String img = "";
  // 背景图片列表
  List bgList = [];

  @override
  void initState() {
    super.initState();
    img = widget.img;
    initBgList();
  }

  void initBgList() async {
    await DioUtils().httpRequest(
      NetConstants.config,
      params: {"conf_key": "background_list"},
      loading: false,
      onSuccess: (res) {
        if (res == null) {
          bgList = [];
        } else {
          bgList = jsonDecode(res);
        }
        setState(() {});
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      padding: EdgeInsets.only(
          top: 40.w,
          left: 40.w,
          right: 40.w,
          bottom: ScreenUtil().bottomBarHeight + 40.w),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          FText(
            "房间背景",
            size: 30.sp,
          ),
          SizedBox(height: 30.w),
          SizedBox(
            height: 600.w,
            child: GridView.builder(
              padding: const EdgeInsets.all(0),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 4,
                mainAxisSpacing: 0.w,
                crossAxisSpacing: 0.w,
                childAspectRatio: 0.55,
              ), // 数据数量
              itemCount: bgList.length,
              // 所有数据
              itemBuilder: (_, index) => items(index),
            ),
          )
        ],
      ),
    );
  }

  Widget items(int index) {
    return GestureDetector(
      onTap: () => onTapBg(bgList[index]["image"]),
      child: FContainer(
        child: Column(
          children: [
            FContainer(
              width: 140.w,
              height: 230.w,
              imageFit: BoxFit.fill,
              image: ImageUtils().getImageProvider(
                  NetConstants.ossPath + bgList[index]["image"]),
              border: img == bgList[index]["image"]
                  ? Border.all(width: 4.w, color: Colors.red)
                  : null,
              radius: BorderRadius.circular(10.w),
            ),
            SizedBox(height: 10.w),
            FText(
              bgList[index]["name"],
              color: Colors.white.withOpacity(0.6),
            )
          ],
        ),
      ),
    );
  }

  void onTapBg(String index) {
    setState(() {
      img = index;
    });
    widget.onTap(index);
  }
}

// 房间管理设置
class SetRoomAdmin extends StatefulWidget {
  const SetRoomAdmin({super.key});

  @override
  State<SetRoomAdmin> createState() => _SetRoomAdminState();
}

class _SetRoomAdminState extends State<SetRoomAdmin> {
  // 房间管理员列表
  List adminList = [];

  @override
  void initState() {
    super.initState();
    if (!mounted) return;
    initAdmin();
  }

  void initAdmin() async {
    await DioUtils().httpRequest(
      NetConstants.roomAdminList,
      loading: false,
      params: {
        "exterior_room_id": AppManager().roomInfo.roomMode!.exteriorRoomId
      },
      onSuccess: (res) async {
        adminList.clear();
        List? superManagers = res["super_manager"];
        List? managers = res["manager"];
        var owners = res["owner"];
        if (owners != null) {
          adminList.add(owners);
        }
        if (superManagers != null) {
          adminList.addAll(superManagers);
        }
        if (managers != null) {
          adminList.addAll(managers);
        }
        debugPrint("房间管理员列表$adminList");
        setState(() {});
      },
    );
  }

  Future<void> callRefresh() async {
    initAdmin();
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      padding: EdgeInsets.only(
          top: 30.w,
          left: 40.w,
          right: 40.w,
          bottom: ScreenUtil().bottomBarHeight + 40.w),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          FText("管理员", size: 34.sp),
          SizedBox(height: 10.w),
          SizedBox(
            height: 600.w,
            child: FRefreshLayout(
              onRefresh: callRefresh,
              // onLoad: callLoad,
              firstRefresh: false,
              emptyWidget: adminList.isEmpty
                  ? const FEmptyWidget(
                      text: "暂未设置管理员",
                    )
                  : null,
              child: ListView.builder(
                itemBuilder: (_, index) => items(adminList[index]),
                itemExtent: 120.w,
                itemCount: adminList.length,
              ),
            ),
          ),
          SizedBox(height: 30.w),
          btns()
        ],
      ),
    );
  }

  Widget items(data) {
    return Row(
      children: [
        FContainer(
          width: 90.w,
          height: 90.w,
          radius: BorderRadius.circular(50),
          image: ImageUtils()
              .getImageProvider(NetConstants.ossPath + data["image"]),
          imageFit: BoxFit.cover,
        ),
        SizedBox(width: 15.w),
        Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            FText(
              data["user_name"],
              size: 26.sp,
            ),
            // SizedBox(height: 15.w),
            // FText(
            //   "管理时长：永久",
            //   size: 22.sp,
            //   color: Colors.white.withOpacity(0.6),
            // )
          ],
        ),
        const Spacer(),
        FButton(
          onTap: () => onTapRemoveAdmin(data["ex_user_id"]),
          bgColor: const Color(0xFFFF753F),
          child: FText(
            "移除",
            size: 22.sp,
          ),
        )
      ],
    );
  }

  Widget btns() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        // FButton(
        //   onTap: () async {
        //     await WidgetUtils().showBottomSheet(
        //         const SetRoomUser("管理员", false, 0),
        //         color: const Color(0xFF1D1928));
        //     initAdmin();
        //   },
        //   width: 310.w,
        //   height: 90.w,
        //   border: Border.all(color: const Color(0xFFFF753F)),
        //   align: Alignment.center,
        //   child: FText(
        //     "搜索ID",
        //     size: 32.sp,
        //     color: const Color(0xFFFF753F),
        //     weight: FontWeight.w500,
        //   ),
        // ),
        FButton(
          onTap: () async {
            await WidgetUtils().showBottomSheet(
                const SetRoomUser("管理员", true, 0),
                color: const Color(0xFF1D1928));
            initAdmin();
          },
          width: 310.w,
          height: 90.w,
          bgColor: const Color(0xFFFF753F),
          align: Alignment.center,
          child: FText(
            "选择麦上用户",
            size: 32.sp,
            weight: FontWeight.w500,
          ),
        )
      ],
    );
  }

  // 移除管理员
  void onTapRemoveAdmin(int userId) async {
    await DioUtils().httpRequest(
      NetConstants.roomAdminSet,
      method: DioMethod.POST,
      loading: false,
      params: {
        "exterior_room_id": AppManager().roomInfo.roomMode!.exteriorRoomId,
        "user_role": RoomRole.DEF.value,
        "exterior_user_id": userId,
      },
      onSuccess: (res) async {
        initAdmin();
      },
    );
  }
}

// 设置用户
class SetUser extends StatefulWidget {
  const SetUser({super.key});

  @override
  State<SetUser> createState() => _SetUserState();
}

class _SetUserState extends State<SetUser> with SingleTickerProviderStateMixin {
  late TabController tabController;

  // 拉黑列表
  List blockList = [];
  // 禁言列表
  List banChatList = [];

  @override
  void initState() {
    super.initState();
    tabController = TabController(length: 2, vsync: this);
    initList(0);
  }

  Future<void> callRefresh() async {
    initList(tabController.index);
  }

  // 初始化拉黑/禁言列表
  void initList(int index) async {
    // await DioUtils().httpRequest(
    //   NetConstants.roomBanList,
    //   loading: false,
    //   params: {
    //     "exterior_room_id": AppManager().roomInfo.roomMode!.exteriorRoomId,
    //     "ban_type": 2,
    //   },
    //   onSuccess: (res) async {
    //     debugPrint("禁言列表$res");
    //     if (res == null || res["userInfos"] == null) {
    //       banChatList = [];
    //     } else {
    //       banChatList = res["userInfos"];
    //     }
    //   },
    // );
    if (index == 0) {
      await DioUtils().httpRequest(
        NetConstants.roomBlockList,
        loading: false,
        params: {
          "room_id": AppManager().roomInfo.roomMode!.roomId,
        },
        onSuccess: (res) async {
          debugPrint("拉黑列表$res");
          if (res == null || res["black_infos"] == null) {
            blockList = [];
          } else {
            blockList = res["black_infos"];
          }
        },
      );
    } else {
      await DioUtils().httpRequest(
        NetConstants.roomBanList,
        loading: false,
        params: {
          "exterior_room_id": AppManager().roomInfo.roomMode!.exteriorRoomId,
          "ban_type": 2,
        },
        onSuccess: (res) async {
          debugPrint("禁言列表$res");
          if (res == null || res["userInfos"] == null) {
            banChatList = [];
          } else {
            banChatList = res["userInfos"];
          }
        },
      );
    }
    setState(() {});
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      padding: EdgeInsets.only(
          top: 10.w, bottom: ScreenUtil().bottomBarHeight + 40.w),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          title(),
          SizedBox(height: 30.w),
          FContainer(
            padding: EdgeInsets.symmetric(horizontal: 40.w),
            height: 600.w,
            child: FRefreshLayout(
              onRefresh: callRefresh,
              firstRefresh: false,
              emptyWidget: ((tabController.index == 0 && blockList.isEmpty) ||
                      (tabController.index == 1 && banChatList.isEmpty))
                  ? const FEmptyWidget()
                  : null,
              child: SizedBox(
                height: 600.w,
                child: TabBarView(
                  physics: const NeverScrollableScrollPhysics(),
                  controller: tabController,
                  children: [
                    userList(0, blockList),
                    userList(1, banChatList),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget title() {
    return FContainer(
      padding: EdgeInsets.symmetric(horizontal: 40.w),
      border: Border(bottom: BorderSide(color: Colors.white.withOpacity(0.08))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          const SizedBox(),
          GestureDetector(
            onTap: () {
              tabController.animateTo(0);
              setState(() {});
              initList(0);
            },
            child: FContainer(
              border: tabController.index == 0
                  ? const Border(bottom: BorderSide(color: Color(0xFFFF753F)))
                  : null,
              padding: EdgeInsets.symmetric(vertical: 22.w, horizontal: 28.w),
              child: FText(
                "拉黑",
                size: tabController.index == 0 ? 34.sp : 32.sp,
                color: tabController.index == 0
                    ? Colors.white
                    : Colors.white.withOpacity(0.6),
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              tabController.animateTo(1);
              setState(() {});
              initList(1);
            },
            child: FContainer(
              border: tabController.index == 1
                  ? const Border(bottom: BorderSide(color: Color(0xFFFF753F)))
                  : null,
              padding: EdgeInsets.symmetric(vertical: 18.w, horizontal: 28.w),
              child: FText(
                "禁言",
                size: tabController.index == 0 ? 34.sp : 32.sp,
                color: tabController.index == 1
                    ? Colors.white
                    : Colors.white.withOpacity(0.6),
              ),
            ),
          ),
          const SizedBox(),
        ],
      ),
    );
  }

  Widget userList(int tabIndex, List list) {
    return SizedBox(
      height: 500.w,
      child: ListView.builder(
        itemBuilder: (_, index) => items(tabIndex, list[index]),
        itemExtent: 120.w,
        itemCount: list.length,
      ),
    );
  }

  Widget items(int tabIndex, data) {
    late void Function() onTap;
    switch (tabIndex) {
      case 0:
        onTap = () => removeBolck(data["user_id"]);
        break;
      default:
        onTap = () => removeBanChat(data["ex_user_id"]);
    }
    return Row(
      children: [
        FContainer(
          width: 90.w,
          height: 90.w,
          radius: BorderRadius.circular(50),
          imageFit: BoxFit.cover,
          image: ImageUtils()
              .getImageProvider(NetConstants.ossPath + data["user_image"]),
        ),
        SizedBox(width: 15.w),
        Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            FText(
              data["user_name"],
              size: 26.sp,
            ),
          ],
        ),
        const Spacer(),
        FButton(
          onTap: onTap,
          border: Border.all(color: const Color(0xFFFF753F)),
          child: FText(
            "取消${tabIndex == 0 ? "拉黑" : "禁言"}",
            color: const Color(0xFFFF753F),
            size: 22.sp,
          ),
        )
      ],
    );
  }

  // 移除拉黑
  void removeBolck(String userId) async {
    await DioUtils().httpRequest(
      NetConstants.removeRoomBlockUser,
      method: DioMethod.POST,
      loading: false,
      params: {
        "room_id": AppManager().roomInfo.roomMode!.roomId,
        "user_id": userId,
      },
      onSuccess: (res) async {
        initList(0);
      },
    );
  }

  // 取消禁言
  void removeBanChat(int userId) async {
    await DioUtils().httpRequest(
      NetConstants.roomBanUser,
      method: DioMethod.POST,
      loading: false,
      params: {
        "exterior_room_id": AppManager().roomInfo.roomMode!.exteriorRoomId,
        "exterior_user_id": userId,
        "is_ban_chat": 2,
      },
      onSuccess: (res) async {
        initList(1);
      },
    );
  }

  // 取消禁麦
  void removeBanMic(int userId) async {
    await DioUtils().httpRequest(
      NetConstants.roomBanUser,
      method: DioMethod.POST,
      loading: false,
      params: {
        "exterior_room_id": AppManager().roomInfo.roomMode!.exteriorRoomId,
        "exterior_user_id": userId,
        "is_ban_mic": 2,
      },
      onSuccess: (res) async {
        initList(2);
      },
    );
  }
}

// 设置管理员/拉黑/禁言
class SetRoomUser extends StatefulWidget {
  final String title;
  // 是否选择麦上用户
  final bool isSeat;
  // 0管理员 / 1拉黑 / 2禁言
  final int type;
  const SetRoomUser(this.title, this.isSeat, this.type, {super.key});

  @override
  State<SetRoomUser> createState() => _SetRoomUserState();
}

class _SetRoomUserState extends State<SetRoomUser> {
  // 搜索内容
  String textValue = "";

  // 麦位列表
  List<Mic> seatLists = [];

  @override
  void initState() {
    super.initState();
    if (!mounted) return;
    initList();
  }

  void initList() {
    if (widget.isSeat) {
      seatLists = AppManager().roomInfo.seatLists;
      List<Mic> list = [];
      for (var i = 0; i < AppManager().roomInfo.seatLists.length; i++) {
        if (AppManager().roomInfo.seatLists[i].micUserInfo.exUserId != 0) {
          list.add(AppManager().roomInfo.seatLists[i]);
        }
      }
      setState(() {
        seatLists = list;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      padding: EdgeInsets.only(
          top: 40.w,
          left: 40.w,
          right: 40.w,
          bottom: ScreenUtil().bottomBarHeight + 40.w),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          FText(
            "添加${widget.title}",
            size: 30.sp,
          ),
          SizedBox(height: 30.w),
          Visibility(visible: !widget.isSeat, child: input()),
          userList(),
          SizedBox(height: 30.w),
        ],
      ),
    );
  }

  Widget input() {
    InputBorder border = OutlineInputBorder(
        borderRadius: BorderRadius.circular(50),
        borderSide: const BorderSide(color: Colors.transparent));

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        FText("输入ID添加${widget.title}列表"),
        SizedBox(height: 20.w),
        SizedBox(
          height: 90.w,
          child: TextField(
            style: TextStyle(color: Colors.white, fontSize: 30.sp),
            maxLength: 15,
            maxLines: 1,
            onChanged: (value) {
              if (textValue.isEmpty || value.isEmpty) {
                setState(() {
                  textValue = value;
                });
              }
            },
            decoration: InputDecoration(
              enabledBorder: border,
              focusedBorder: border,
              disabledBorder: border,
              filled: true,
              fillColor: Colors.white.withOpacity(0.1),
              counterText: "",
              hintText: "请输入ID",
              contentPadding: EdgeInsets.only(
                  top: 0, left: 28.w, right: 28.w, bottom: 50.w),
              hintStyle: TextStyle(
                  color: const Color(0xFFBBBBBB),
                  fontSize: 30.sp,
                  letterSpacing: 1),
              suffix: Visibility(
                visible: textValue.isNotEmpty,
                child: FButton(
                  onTap: onSearch,
                  bgColor: const Color(0xFFFF753F),
                  text: "完成",
                ),
              ),
            ),
          ),
        ),
        SizedBox(height: 30.w),
      ],
    );
  }

  Widget userList() {
    List list = widget.isSeat ? seatLists : [];
    return SizedBox(
      height: 500.w,
      child: ListView.builder(
        itemBuilder: (_, index) => items(index),
        itemExtent: 120.w,
        itemCount: list.length,
      ),
    );
  }

  Widget items(int index) {
    if (widget.isSeat) {
      Mic data = seatLists[index];
      return Row(
        children: [
          FContainer(
            width: 90.w,
            height: 90.w,
            radius: BorderRadius.circular(50),
            imageFit: BoxFit.cover,
            image: ImageUtils().getImageProvider(
                NetConstants.ossPath + data.micUserInfo.userImage),
          ),
          SizedBox(width: 15.w),
          Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              FText(
                data.micUserInfo.userName,
                size: 26.sp,
              ),
            ],
          ),
          const Spacer(),
          seatBtn(data),
        ],
      );
    } else {
      return Row(
        children: [
          FContainer(
            width: 90.w,
            height: 90.w,
            radius: BorderRadius.circular(50),
            color: Colors.red,
          ),
          SizedBox(width: 15.w),
          Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              FText(
                "野猪姐姐",
                size: 26.sp,
              ),
            ],
          ),
          const Spacer(),
          // seatBtn(null),
        ],
      );
    }
  }

  // 返回按钮
  Widget seatBtn(Mic data) {
    switch (widget.type) {
      case 1:
        return FButton(
          onTap: () => addBolck(data.micUserInfo.exUserId),
          border: Border.all(color: const Color(0xFFFF753F)),
          child: FText(
            "拉黑",
            color: const Color(0xFFFF753F),
            size: 22.sp,
          ),
        );
      case 2:
        return FButton(
          onTap: () => removeBanChat(
              data.micUserInfo.exUserId, data.micUserInfo.isBanChat),
          border: Border.all(color: const Color(0xFFFF753F)),
          child: FText(
            "${data.micUserInfo.isBanChat == 1 ? "取消" : ""}禁言",
            color: const Color(0xFFFF753F),
            size: 22.sp,
          ),
        );
      default:
        return FButton(
          onTap: () => onTapRemoveAdmin(
            data.micUserInfo.exUserId,
            data.micUserInfo.personalPermission == 4,
          ),
          bgColor: const Color(0xFFFF753F),
          child: FText(
            data.micUserInfo.personalPermission < 4 ? "移除" : "添加",
            size: 22.sp,
          ),
        );
    }
  }

  // 搜索用户
  void onSearch() {
    FocusScope.of(context).requestFocus(FocusNode());
    debugPrint(textValue);
  }

  // 移除/添加管理员
  void onTapRemoveAdmin(int userId, bool isOpen) async {
    await DioUtils().httpRequest(
      NetConstants.roomAdminSet,
      method: DioMethod.POST,
      loading: false,
      params: {
        "exterior_room_id": AppManager().roomInfo.roomMode!.exteriorRoomId,
        "user_role": isOpen ? RoomRole.MANAGER.value : RoomRole.DEF.value,
        "exterior_user_id": userId,
      },
      onSuccess: (res) async {
        WidgetUtils().popPage();
      },
    );
  }

  // 拉黑
  void addBolck(int userId) async {
    await DioUtils().httpRequest(
      NetConstants.roomBlockUser,
      method: DioMethod.POST,
      loading: false,
      params: {
        "room_id": AppManager().roomInfo.roomMode!.roomId,
        "user_id": userId,
      },
      onSuccess: (res) async {
        WidgetUtils().popPage();
      },
    );
  }

  // 禁言/取消禁言
  void removeBanChat(int userId, int isOpen) async {
    await DioUtils().httpRequest(
      NetConstants.roomBanUser,
      method: DioMethod.POST,
      loading: false,
      params: {
        "exterior_room_id": AppManager().roomInfo.roomMode!.exteriorRoomId,
        "exterior_user_id": userId,
        "is_ban_chat": isOpen == 1 ? 2 : 1,
      },
      onSuccess: (res) async {
        WidgetUtils().popPage();
      },
    );
  }

  // 禁麦/取消禁麦
  void removeBanMic(int userId, int isOpen) async {
    await DioUtils().httpRequest(
      NetConstants.roomBanUser,
      method: DioMethod.POST,
      loading: false,
      params: {
        "exterior_room_id": AppManager().roomInfo.roomMode!.exteriorRoomId,
        "exterior_user_id": userId,
        "is_ban_mic": isOpen == 1 ? 2 : 1,
      },
      onSuccess: (res) async {
        WidgetUtils().popPage();
      },
    );
  }
}

// 房间上麦模式
class SetRoomMode extends StatefulWidget {
  final RoomProvider pro;
  const SetRoomMode(this.pro, {super.key});

  @override
  State<SetRoomMode> createState() => _SetRoomModeState();
}

class _SetRoomModeState extends State<SetRoomMode> {
  // 当前上麦模式
  int id = AppManager().roomInfo.roomMode!.isMicApply;

  @override
  Widget build(BuildContext context) {
    return FContainer(
      padding: EdgeInsets.only(
          top: 40.w,
          left: 40.w,
          right: 40.w,
          bottom: ScreenUtil().bottomBarHeight + 40.w),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          FText("上麦模式", size: 30.sp),
          SizedBox(height: 40.w),
          item("自由上麦", "无需房主同意即可上麦", 1),
          SizedBox(height: 40.w),
          item("上麦需房主同意", "申请上麦后需要房主同意", 2),
          SizedBox(height: 40.w),
          btn()
        ],
      ),
    );
  }

  Widget item(String text, String span, int index) {
    return GestureDetector(
      onTap: () {
        setState(() {
          id = index;
        });
      },
      child: FContainer(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                FText(
                  text,
                  size: 30.sp,
                ),
                SizedBox(height: 10.w),
                FText(
                  span,
                  color: Colors.white.withOpacity(0.6),
                ),
              ],
            ),
            LoadAssetImage(id == index ? "login/chekbanbtn" : "login/chekbtn",
                width: 34.w, height: 34.w)
          ],
        ),
      ),
    );
  }

  Widget btn() {
    return FButton(
      onTap: onTapMic,
      width: double.infinity,
      height: 80.w,
      bgColor: const Color(0xFFFF753F),
      align: Alignment.center,
      child: FText(
        "保存修改",
        size: 32.sp,
      ),
    );
  }

  // 修改排麦模式
  void onTapMic() async {
    await DioUtils().httpRequest(
      NetConstants.setRoomMode,
      method: DioMethod.POST,
      loading: false,
      params: {
        "room_id": AppManager().roomInfo.roomMode!.roomId,
        "is_mic_apply": id
      },
      onSuccess: (data) async {
        widget.pro.roomModel!.isMicApply = id;
        WidgetUtils().popPage();
      },
    );
  }
}

// 房间加锁
class SetRoomLock extends StatefulWidget {
  const SetRoomLock({super.key});

  @override
  State<SetRoomLock> createState() => _SetRoomLockState();
}

class _SetRoomLockState extends State<SetRoomLock> {
  String textValue = "";

  @override
  Widget build(BuildContext context) {
    return FContainer(
      padding: EdgeInsets.only(
          top: 40.w,
          left: 40.w,
          right: 40.w,
          bottom: ScreenUtil().bottomBarHeight + 40.w),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          FText("房间加锁", size: 30.sp),
          SizedBox(height: 30.w),
          input(),
          SizedBox(height: 30.w),
          btn()
        ],
      ),
    );
  }

  Widget input() {
    InputBorder border = OutlineInputBorder(
        borderRadius: BorderRadius.circular(50),
        borderSide: const BorderSide(color: Colors.transparent));

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const FText("请设置密码"),
        SizedBox(height: 20.w),
        SizedBox(
          height: 120.w,
          child: TextField(
            style: TextStyle(color: Colors.white, fontSize: 30.sp),
            maxLength: 4,
            maxLines: 1,
            keyboardType: TextInputType.number,
            inputFormatters: [
              FilteringTextInputFormatter.allow(RegExp("[0-9]")),
            ],
            onChanged: (value) {
              textValue = value;
              setState(() {});
            },
            decoration: InputDecoration(
              enabledBorder: border,
              focusedBorder: border,
              disabledBorder: border,
              filled: true,
              fillColor: Colors.white.withOpacity(0.1),
              counterStyle: TextStyle(
                  color: const Color(0xFFBBBBBB),
                  fontSize: 30.sp,
                  letterSpacing: 1),
              hintText: "请输入密码",
              contentPadding: EdgeInsets.only(
                  top: 0, left: 28.w, right: 28.w, bottom: 50.w),
              hintStyle: TextStyle(
                  color: const Color(0xFFBBBBBB),
                  fontSize: 30.sp,
                  letterSpacing: 1),
              suffix: Visibility(
                visible: textValue.isNotEmpty,
                child: FButton(
                  onTap: () {
                    FocusScope.of(context).requestFocus(FocusNode());
                  },
                  bgColor: const Color(0xFFFF753F),
                  text: "完成",
                ),
              ),
            ),
          ),
        ),
        SizedBox(
            height: MediaQuery.of(context).viewInsets.bottom - 110.w > 0
                ? MediaQuery.of(context).viewInsets.bottom - 110.w
                : 0),
      ],
    );
  }

  Widget btn() {
    return FButton(
      onTap: onSetLock,
      width: double.infinity,
      height: 80.w,
      bgColor: const Color(0xFFFF753F),
      align: Alignment.center,
      child: FText(
        "保存修改",
        size: 32.sp,
      ),
    );
  }

  void onSetLock() async {
    await DioUtils().httpRequest(
      NetConstants.setRoomLock,
      method: DioMethod.POST,
      params: {
        "exterior_room_id": AppManager().roomInfo.roomMode!.exteriorRoomId,
        "room_password": textValue,
      },
      loading: false,
      onSuccess: (data) {
        WidgetUtils().popPage();
      },
    );
  }
}
