import 'dart:async';
import 'dart:convert';

import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class RoomBgMusicSet extends StatefulWidget {
  const RoomBgMusicSet({super.key});

  @override
  State<RoomBgMusicSet> createState() => _RoomBgMusicSetState();
}

class _RoomBgMusicSetState extends State<RoomBgMusicSet>
    with SingleTickerProviderStateMixin {
  late TabController tabController;

  // 分页
  int page = 1;

  // 音乐列表
  List musicList = [];
  // 我的音乐列表
  List myMusicList = [];
  // 播放音乐信息
  var musicInfo;
  // 当前音乐暂停或者播放
  int isMusicPlay = 0;

  @override
  void initState() {
    super.initState();
    EventUtils().on(
      EventConstants.E_ROOM_MUSIC,
      EventCall("RoomBgMusicSet", (data) {
        musicInfo = AppManager().roomInfo.musicInfo;
        isMusicPlay = AppManager().roomInfo.playMusicMode;
        setState(() {});
      }),
    );
    tabController = TabController(length: 2, vsync: this, initialIndex: 0);
    musicInfo = AppManager().roomInfo.musicInfo;
    isMusicPlay = AppManager().roomInfo.playMusicMode;
    initMusicList(false);
  }

  @override
  void dispose() {
    EventUtils().off(
        EventConstants.E_ROOM_MUSIC, EventCall("RoomBgMusicSet", (data) {}));
    tabController.dispose();
    super.dispose();
  }

  Future<void> callLoad() async {
    if (musicList.isEmpty || musicList.length % 30 != 0) return;
    page += 1;
    initMusicList(true);
  }

  void initMusicList(bool isLoad) async {
    if (tabController.index == 0) {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      List musics =
          preferences.getStringList(SharedKey.KEY_MY_MUSIC.value) ?? [];
      List lists = [];
      for (var i = 0; i < musics.length; i++) {
        lists.add(jsonDecode(musics[i]));
      }
      myMusicList = lists;
    } else {
      if (!isLoad) {
        page = 1;
      }
      await DioUtils().httpRequest(
        NetConstants.musicList,
        method: DioMethod.POST,
        loading: false,
        params: {
          "type": "search",
          "search": "热门",
          "page": page,
          "limit": 30,
        },
        onSuccess: (res) {
          if (isLoad) {
            if (res != null) {
              musicList.addAll(res);
            }
          } else {
            if (res == null) {
              musicList = [];
            } else {
              musicList = res;
            }
          }
        },
      );
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      padding: EdgeInsets.only(
          top: 20.w, bottom: ScreenUtil().bottomBarHeight + 40.w),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          header(),
          SizedBox(
            height: 800.w,
            child: Column(
              children: [
                Expanded(
                    child: TabBarView(
                  physics: const NeverScrollableScrollPhysics(),
                  controller: tabController,
                  children: [
                    Column(
                      children: [
                        musicSort(true),
                        SizedBox(height: 12.w),
                        Expanded(
                            child: FRefreshLayout(
                          firstRefresh: false,
                          onLoad: callLoad,
                          emptyWidget:
                              myMusicList.isEmpty ? const FEmptyWidget() : null,
                          child: ListView.builder(
                            padding: EdgeInsets.symmetric(
                                vertical: 0, horizontal: 30.w),
                            itemBuilder: (_, index) =>
                                musicItems(true, myMusicList[index]),
                            itemCount: myMusicList.length,
                          ),
                        ))
                      ],
                    ),
                    Column(
                      children: [
                        musicSort(false),
                        Expanded(
                            child: FRefreshLayout(
                          firstRefresh: false,
                          onLoad: callLoad,
                          emptyWidget:
                              musicList.isEmpty ? const FEmptyWidget() : null,
                          child: ListView.builder(
                            padding: EdgeInsets.symmetric(
                                vertical: 0, horizontal: 30.w),
                            itemBuilder: (_, index) =>
                                musicItems(false, musicList[index]),
                            itemCount: musicList.length,
                          ),
                        ))
                      ],
                    ),
                  ],
                )),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget header() {
    return FContainer(
      padding: EdgeInsets.symmetric(horizontal: 30.w),
      border: Border(bottom: BorderSide(color: Colors.white.withOpacity(0.08))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          // Row(
          //   children: [
          //     LoadAssetImage(
          //       "room/RoomMusic/upload",
          //       width: 35.w,
          //       color: const Color(0xFFA5A3A9),
          //     ),
          //     SizedBox(width: 3.w),
          //     const FText(
          //       "上传",
          //       color: Color(0xFFA5A3A9),
          //     )
          //   ],
          // ),
          SizedBox(width: 55.w),
          headerItem("我的曲库", 0),
          headerItem("大家都听", 1),
          SizedBox(width: 55.w),
        ],
      ),
    );
  }

  Widget headerItem(String text, int index) {
    Widget child = FContainer(
      height: 80.w,
      align: Alignment.center,
      border: Border(
          bottom: BorderSide(
              color: tabController.index == index
                  ? const Color(0xFFFF753F)
                  : Colors.transparent)),
      child: FText(
        text,
        size: 34.sp,
        color: tabController.index == index
            ? Colors.white
            : const Color(0xFFA5A3A9),
        weight: FontWeight.w700,
      ),
    );
    return GestureDetector(
      onTap: () => switchtab(index),
      child: child,
    );
  }

  Widget musicSort(bool isMy) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 20.w, horizontal: 30.w),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text.rich(
            TextSpan(
              style: TextStyle(
                color: const Color(0xFFA5A3A9),
                fontSize: 32.sp,
              ),
              text: "全部歌曲 - ",
              children: [
                TextSpan(
                  style: const TextStyle(
                    color: Color(0xFFFF753F),
                  ),
                  text: "${isMy ? myMusicList.length : musicList.length}",
                ),
                const TextSpan(text: "首"),
              ],
            ),
          ),
          // Row(
          //   children: [
          //     LoadAssetImage(
          //       "room/RoomMusic/downSort",
          //       width: 30.w,
          //     ),
          //     SizedBox(width: 5.w),
          //     FText("排序", size: 26.sp, color: const Color(0xFFA5A3A9))
          //   ],
          // )
        ],
      ),
    );
  }

  Widget musicItems(bool isPlay, data) {
    bool isMusic = musicInfo != null &&
        musicInfo["id"] == data["songId"] &&
        isMusicPlay == 1;
    return FContainer(
      padding: EdgeInsets.symmetric(vertical: 25.w),
      border: Border(bottom: BorderSide(color: Colors.white.withOpacity(0.2))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
              child: Padding(
            padding: EdgeInsets.symmetric(vertical: 10.w),
            child: Text.rich(
              TextSpan(
                style: TextStyle(
                  fontSize: 33.sp,
                  color: Colors.white,
                ),
                text: data["title"],
                children: [
                  TextSpan(
                    style: TextStyle(
                      fontSize: 26.sp,
                      color: Colors.white.withOpacity(0.6),
                    ),
                    text: " - ${data["author"]}",
                  ),
                ],
              ),
            ),
          )),
          isPlay
              ? GestureDetector(
                  onTap: () => onPlay(data),
                  child: LoadAssetImage(
                    isMusic
                        ? "room/RoomMusic/listplay"
                        : "room/RoomMusic/listsuspend",
                    width: 55.w,
                  ),
                )
              : Visibility(
                  visible: !(myMusicList.isNotEmpty &&
                      myMusicList
                          .any((muscic) => muscic["songId"] == data["songId"])),
                  child: GestureDetector(
                    onTap: () => addMyMusic(data),
                    child: LoadAssetImage(
                      "room/RoomMusic/add",
                      width: 55.w,
                    ),
                  ),
                )
        ],
      ),
    );
  }

  void switchtab(int index) {
    tabController.animateTo(index);
    initMusicList(false);
    setState(() {});
  }

  // 添加到我的曲库
  void addMyMusic(data) async {
    myMusicList.add(data);
    setState(() {});
    SharedPreferences preferences = await SharedPreferences.getInstance();
    List<String> musics = [];
    for (var i = 0; i < myMusicList.length; i++) {
      musics.add(jsonEncode(myMusicList[i]));
    }
    preferences.setStringList(SharedKey.KEY_MY_MUSIC.value, musics);
  }

  // 播放音乐
  void onPlay(data) async {
    if (AppManager().roomInfo.roomMode!.isBanMic == 1) {
      return WidgetUtils().showToast("您已被禁麦!");
    }
    if (musicInfo != null && data["songId"] != musicInfo["id"]) {
      AppManager().roomInfo.musicPlaySchedule = 0;
    }
    AppManager().roomInfo.musicList = myMusicList;
    AppManager().roomAudioMixing(id: data["songId"]);
    Future.delayed(const Duration(milliseconds: 500), () async {
      WidgetUtils().showBottomSheet(const Mixer(), color: Colors.transparent);
    });
  }
}

class Mixer extends StatefulWidget {
  const Mixer({super.key});

  @override
  State<Mixer> createState() => _MixerState();
}

class _MixerState extends State<Mixer> {
  // {name: 空待(独唱版), id: 34923178, artists: [{name: 王朝, id: 1039296}],
  // album: {name: 空待, id: 169585808, type: Single, size: 3, picId: 109951168737278943,
  // blurPicUrl: http://p1.music.126.net/Cz0Bor-8XWlR0h6y5iRk8g==/109951168737278943.jpg, companyId: 0,
  // pic: 109951168737278943, picUrl: http://p1.music.126.net/Cz0Bor-8XWlR0h6y5iRk8g==/109951168737278943.jpg,
  // publishTime: 1364918400000}, hMusic: {playTime: 324414},
  // playUrl: http://m701.music.1o3DlMOGwrbDjj7DisKw/29453631120/e7b6b13a3792d94705ef74370c.mp3}
  // 播放音乐信息
  var musicInfo;
  // 当前音乐暂停或者播放
  int isMusicPlay = 0;
  // 当前音乐播放模式
  int musicMode = 0;
  // 当前音乐播放进度
  int musicPlaySchedule = 0;
  // 播放进度
  Timer? timer;

  @override
  void initState() {
    super.initState();
    EventUtils().on(
      EventConstants.E_ROOM_MUSIC,
      EventCall("Mixer", (data) {
        musicInfo = AppManager().roomInfo.musicInfo;
        isMusicPlay = AppManager().roomInfo.playMusicMode;
        musicMode = AppManager().roomInfo.musicMode;
        musicPlaySchedule = AppManager().roomInfo.musicPlaySchedule;
        if (musicInfo != null) {
          timer ??= Timer.periodic(
            const Duration(milliseconds: 1),
            (timer) {
              setState(() {
                musicPlaySchedule = AppManager().roomInfo.musicPlaySchedule;
              });
            },
          );
        } else {
          timer?.cancel();
          timer = null;
        }
        setState(() {});
      }),
    );
    musicInfo = AppManager().roomInfo.musicInfo;
    isMusicPlay = AppManager().roomInfo.playMusicMode;
    musicMode = AppManager().roomInfo.musicMode;
    musicPlaySchedule = AppManager().roomInfo.musicPlaySchedule;
    timer = Timer.periodic(const Duration(milliseconds: 1), (timer) {
      setState(() {
        musicPlaySchedule = AppManager().roomInfo.musicPlaySchedule;
      });
    });
  }

  @override
  void dispose() {
    EventUtils()
        .off(EventConstants.E_ROOM_MUSIC, EventCall("Mixer", (data) {}));
    timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    String playImg = "";
    switch (musicMode) {
      case 1:
        playImg = "random";
        break;
      case 2:
        playImg = "circulate";
        break;
      default:
        playImg = "order";
    }
    // 播放顺序
    Widget playbackMode = GestureDetector(
      onTap: () =>
          AppManager().switchMusicMode(musicMode == 2 ? 0 : musicMode + 1),
      child: LoadAssetImage("room/RoomMusic/$playImg", width: 50.w),
    );

    // 上一曲
    Widget previousSong = GestureDetector(
      onTap: () => AppManager().upDownMusic(true),
      child: LoadAssetImage("room/RoomMusic/previousSong", width: 50.w),
    );

    // 下一曲
    Widget nextSong = GestureDetector(
      onTap: () => AppManager().upDownMusic(false),
      child: LoadAssetImage("room/RoomMusic/nextSong", width: 50.w),
    );

    String openImg = "play";
    if (isMusicPlay == 2) {
      openImg = "suspend";
    }
    // 开始/结束播放
    Widget play = GestureDetector(
      onTap: AppManager().roomAudioMixing,
      child: LoadAssetImage("room/RoomMusic/$openImg", width: 60.w),
    );

    // 分享
    Widget share = GestureDetector(
      // child: LoadAssetImage("room/RoomMusic/share", width: 50.w),
      child: SizedBox(width: 60.w),
    );

    return FContainer(
      color: const Color(0xFF34303D),
      radius: BorderRadius.only(
          topLeft: Radius.circular(60.w), topRight: Radius.circular(60.w)),
      padding: EdgeInsets.only(
          top: 40.w,
          left: 30.w,
          right: 30.w,
          bottom: ScreenUtil().bottomBarHeight + 40.w),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              FText(
                WidgetUtils.formatSeconds(musicPlaySchedule),
                size: 20.sp,
                color: const Color(0xFF8E8C93),
              ),
              SizedBox(
                width: 500.w,
                child: Stack(
                  children: [
                    FContainer(
                      height: 6.w,
                      radius: BorderRadius.circular(50),
                      color: const Color(0xFF8E8C93),
                    ),
                    FContainer(
                      width: (musicPlaySchedule /
                              musicInfo["hMusic"]["playTime"]) *
                          500.w,
                      height: 6.w,
                      radius: BorderRadius.circular(50),
                      color: const Color(0xFFFF753F),
                    )
                  ],
                ),
              ),
              FText(
                WidgetUtils.formatSeconds(musicInfo["hMusic"]["playTime"]),
                size: 20.sp,
                color: const Color(0xFF8E8C93),
              ),
            ],
          ),
          SizedBox(height: 35.w),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              playbackMode,
              previousSong,
              play,
              nextSong,
              share,
            ],
          )
        ],
      ),
    );
  }
}
