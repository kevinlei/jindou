import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class RoomActivity extends StatefulWidget {
  const RoomActivity({Key? key}) : super(key: key);

  @override
  _RoomActivityState createState() => _RoomActivityState();
}

class _RoomActivityState extends State<RoomActivity> {
  List activityList = [];

  @override
  void initState() {
    super.initState();
    getBannerList();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void getBannerList() async {
    DioUtils().asyncHttpRequest(
      NetConstants.banner,
      loading: false,
      params: {
        "type": BanbarSwper.ROOM_ACTIVITY_COM.brand,
        "position": 1,
      },
      onSuccess: (res) {
        if (res == null) return;
        activityList.clear();
        activityList = res;
        if (!mounted) return;
        setState(() {});
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: FContainer(
        height: 130.w,
        margin: EdgeInsets.only(right: 10.w),
        child: SingleChildScrollView(
          child: listCell(),
        ),
      ),
    );
  }

  Widget listCell() {
    return FContainer(
      width: 120.w,
      height: 120.w,
      margin: EdgeInsets.only(bottom: 10.w),
      child: Swiper(
        pagination: activityList.length > 1
            ? const SwiperPagination(
                builder: DotSwiperPaginationBuilder(
                color: Colors.white,
                activeColor: Color(0xFFFF7CE2),
                size: 5,
                activeSize: 5,
              ))
            : null,
        itemBuilder: (_, index) => bannerCell(activityList[index]),
        itemCount: activityList.length,
        autoplay: activityList.length > 1,
        loop: activityList.length > 1,
        onTap: (index) => onTapAcitive(activityList[index]),
      ),
    );
  }

  Widget bannerCell(banner) {
    String image = banner["img_url"];
    return Center(
      child: LoadNetImage(NetConstants.ossPath + image.trim(),
          width: 100.w, height: 100.w),
    );
  }

  void onTapAcitive(data) async {
    if (AppManager().time == 999) return WidgetUtils().showToast("网络异常");
    String title = data["title"] ?? "";
    String linkImg = data["img_url"] ?? "";
    String linkUrl = data["link_url"] ?? "";
    if (linkUrl.isNotEmpty) {
      showModalBottomSheet(
          context: context,
          backgroundColor: Colors.transparent,
          isScrollControlled: true,
          isDismissible: false,
          enableDrag: false,
          builder: (_) => RoomGame(url: linkUrl));
    } else if (linkImg.isNotEmpty) {
      WidgetUtils().pushPage(BannerPage(title: title, image: linkImg));
    }
  }
}
