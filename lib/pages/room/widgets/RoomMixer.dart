import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class RoomMixer extends StatefulWidget {
  const RoomMixer({super.key});

  @override
  State<RoomMixer> createState() => _RoomMixerState();
}

class _RoomMixerState extends State<RoomMixer> {
  // 取值范围
  int volumeLength = 100;

  // 麦克风
  bool isMicrophoneOpen = true;
  double length = 510.w, micVolume = 0;

  // 扬声器
  bool isSpeakerOpen = true;
  double speakerVolume = 0;

  // 耳返开关
  bool isAuricular = false;

  @override
  void initState() {
    super.initState();
    if (!mounted) return;
    volumeLength = AppConstants.trtcAgoraSwitchFlag == 0 ? 100 : 400;
    isAuricular = AppManager().roomInfo.isAuricular;
    micVolume =
        AppManager().roomInfo.micVolume / volumeLength * length >= length
            ? length - 30.w
            : AppManager().roomInfo.micVolume / volumeLength * length;
    speakerVolume =
        AppManager().roomInfo.speakerVolume / volumeLength * length >= length
            ? length - 30.w
            : AppManager().roomInfo.speakerVolume / volumeLength * length;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FContainer(
      padding: EdgeInsets.only(
          top: 30.w,
          left: 20.w,
          right: 20.w,
          bottom: ScreenUtil().bottomBarHeight + 60.w),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          FText("调音台", size: 34.sp),
          SizedBox(height: 60.w),
          volumeControl(),
          SizedBox(height: 60.w),
          reverberationMode(),
          // SizedBox(height: 60.w),
          // audioMode(),
        ],
      ),
    );
  }

  Widget volumeControl() {
    return Column(
      children: [
        proContent("麦克风", 0),
        SizedBox(height: 40.w),
        proContent("扬声器", 1),
      ],
    );
  }

  Widget proContent(String text, int id) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        FText(
          text,
          size: 28.sp,
        ),
        GestureDetector(
          onHorizontalDragUpdate: (detalis) => onDragUpdate(detalis, id),
          // onHorizontalDragEnd: (detalis) => onDragEnd(detalis, id),
          child: FContainer(
            height: 35.w,
            width: length,
            child: Stack(
                alignment: Alignment.center, children: [progress(id), bar(id)]),
          ),
        ),
        souldOpen(id),
      ],
    );
  }

  Widget progress(int id) {
    return FContainer(
      height: 6.w,
      radius: BorderRadius.circular(6.w),
      color: const Color(0xFFEFF1F5),
      align: Alignment.centerLeft,
      child: FContainer(
        height: 6.w,
        width: id == 0 ? micVolume : speakerVolume,
        radius: BorderRadius.circular(6.w),
        gradient: const [
          Color(0xFF96FCFC),
          Color(0xFFFFA7FB),
          Color(0xFFFF82B2),
        ],
        gradientBegin: Alignment.topLeft,
        gradientEnd: Alignment.bottomRight,
        stops: const [0, 0.4, 0.8],
      ),
    );
  }

  Widget bar(int id) {
    return Positioned(
      left: id == 0 ? micVolume : speakerVolume,
      child: FContainer(
        width: 30.w,
        height: 30.w,
        radius: BorderRadius.circular(50),
        color: Colors.white,
      ),
    );
  }

  Widget souldOpen(int id) {
    return GestureDetector(
      child: FButton(
        bgColor: Colors.white.withOpacity(0.1),
        text: id == 0
            ? isMicrophoneOpen
                ? "静音"
                : "打开"
            : isSpeakerOpen
                ? "静音"
                : "打开",
      ),
    );
  }

  Widget reverberationMode() {
    return Row(
      children: [
        FText("混响模式", size: 38.sp),
        const Spacer(),
        FText(
          "耳返效果",
          size: 28.sp,
          color: Colors.white.withOpacity(0.6),
        ),
        SizedBox(width: 10.w),
        FBtnOpen(
          setAuricular,
          initOpen: isAuricular,
          width: 100.w,
        )
      ],
    );
  }

  Widget audioMode() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        audioModeItem("", 0),
        audioModeItem("room/RoomMusic/recordingStudio", 9),
        audioModeItem("room/RoomMusic/ktv", 1),
        audioModeItem("room/RoomMusic/theater", 3),
        audioModeItem("room/RoomMusic/ethereal", 8),
      ],
    );
  }

  Widget audioModeItem(String image, int type) {
    Widget child = image.isNotEmpty
        ? LoadAssetImage(
            image,
            width: 120.w,
          )
        : FContainer(
            width: 120.w,
            height: 120.w,
            color: Colors.white.withOpacity(0.1),
            radius: BorderRadius.circular(50),
            align: Alignment.center,
            child: FText(
              "无",
              size: 28.sp,
            ),
          );
    return GestureDetector(
      onTap: () => AppManager().setVoiceReverbType(type),
      child: child,
    );
  }

  void onDragUpdate(DragUpdateDetails details, int id) {
    if (id == 0) {
      micVolume += details.delta.dx;
      if (micVolume < 0) {
        micVolume = 0;
      }
      if (micVolume > length - 30.w) {
        micVolume = length - 30.w;
      }
      AppManager()
          .setAudioCaptureVolume(micVolume * volumeLength ~/ (length - 30.w));
    } else {
      speakerVolume += details.delta.dx;
      if (speakerVolume < 0) {
        speakerVolume = 0;
      }
      if (speakerVolume > length - 30.w) {
        speakerVolume = length - 30.w;
      }
      AppManager().setAudioPlayoutVolume(
          speakerVolume * volumeLength ~/ (length - 30.w));
    }
    if (!mounted) return;
    setState(() {});
  }

  // 设置耳返开关
  void setAuricular() {
    AppManager().roomInfo.isAuricular = !isAuricular;
    AppManager().setAuricularReturn(!isAuricular);
    setState(() {
      isAuricular = !isAuricular;
    });
  }
}
