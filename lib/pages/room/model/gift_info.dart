import 'package:json_annotation/json_annotation.dart';

part 'gift_info.g.dart';

@JsonSerializable()
class GiftInfo {
  final int id;
  @JsonKey(name: 'gift_num')
  final int? giftNum;
  @JsonKey(name: 'gift_name')
  final String giftName;
  @JsonKey(name: 'gift_picture')
  final String giftPicture;
  @JsonKey(name: 'gift_price')
  final int giftPrice;
  @JsonKey(name: 'is_star')
  final int isStar;
  @JsonKey(name: 'is_hot')
  final int isHot;
  final String label;
  @JsonKey(name: 'is_activity')
  final int isActivity;

  const GiftInfo({
    required this.id,
    this.giftNum,
    required this.giftName,
    required this.giftPicture,
    required this.giftPrice,
    required this.isStar,
    required this.isHot,
    required this.label,
    required this.isActivity,
  });

  factory GiftInfo.fromJson(Map<String, dynamic> json) =>
      _$GiftInfoFromJson(json);

  Map<String, dynamic> toJson() => _$GiftInfoToJson(this);
}
