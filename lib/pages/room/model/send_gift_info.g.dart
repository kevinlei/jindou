// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'send_gift_info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SendGiftInfo _$SendGiftInfoFromJson(Map<String, dynamic> json) => SendGiftInfo(
      sendGiftInfos: (json['send_gift_infos'] as List<dynamic>)
          .map((e) => SendGiftInfos.fromJson(e as Map<String, dynamic>))
          .toList(),
      userId: json['user_id'] as String,
    );

Map<String, dynamic> _$SendGiftInfoToJson(SendGiftInfo instance) =>
    <String, dynamic>{
      'send_gift_infos': instance.sendGiftInfos,
      'user_id': instance.userId,
    };

SendGiftInfos _$SendGiftInfosFromJson(Map<String, dynamic> json) =>
    SendGiftInfos(
      id: json['id'] as int,
      giftName: json['gift_name'] as String,
      giftPicture: json['gift_picture'] as String,
      giftPrice: json['gift_price'] as int,
      giftNum: json['gift_num'] as int,
    );

Map<String, dynamic> _$SendGiftInfosToJson(SendGiftInfos instance) =>
    <String, dynamic>{
      'id': instance.id,
      'gift_name': instance.giftName,
      'gift_picture': instance.giftPicture,
      'gift_price': instance.giftPrice,
      'gift_num': instance.giftNum,
    };
