import 'package:json_annotation/json_annotation.dart';

part 'room_model.g.dart';

@JsonSerializable()
class RoomModel {
  @JsonKey(name: 'exterior_room_id')
  late int exteriorRoomId;
  @JsonKey(name: 'room_id')
  final int roomId;
  @JsonKey(name: 'chat_id')
  final String hxId;
  @JsonKey(name: 'is_ban_chat')
  late int isBanChat;
  @JsonKey(name: 'is_ban_mic')
  late int isBanMic;
  @JsonKey(name: 'room_name')
  late String roomName;
  @JsonKey(name: 'room_bulletin')
  late String roomBulletin;
  @JsonKey(name: 'room_hot')
  late int roomHot;
  @JsonKey(name: 'image')
  late String headIcon;
  @JsonKey(name: 'room_background_image')
  late String bgImg;
  @JsonKey(name: 'people_num')
  late int peopleNum;
  @JsonKey(name: 'personal_permission')
  late int personalPermission;
  @JsonKey(name: 'top_3_user_info')
  late List<Top3UserInfo> top3UserInfo;
  @JsonKey(name: 'is_follow')
  late int isFollow;
  @JsonKey(name: 'is_mic_apply')
  late int isMicApply;
  @JsonKey(name: 'room_is_chat')
  late int roomIsChat;
  @JsonKey(name: 'is_heart')
  late int isHeart;

  RoomModel({
    required this.exteriorRoomId,
    required this.roomId,
    required this.hxId,
    required this.roomName,
    required this.isBanChat,
    required this.isBanMic,
    required this.roomBulletin,
    required this.roomHot,
    required this.headIcon,
    required this.peopleNum,
    required this.personalPermission,
    required this.top3UserInfo,
    required this.isFollow,
    required this.isMicApply,
    required this.bgImg,
    required this.roomIsChat,
    required this.isHeart,
  });

  factory RoomModel.fromJson(Map<String, dynamic> json) =>
      _$RoomModelFromJson(json);

  Map<String, dynamic> toJson() => _$RoomModelToJson(this);
}

@JsonSerializable()
class Top3UserInfo {
  @JsonKey(name: 'exterior_user_id')
  final int exteriorUserId;
  @JsonKey(name: 'user_image')
  final String userImage;

  const Top3UserInfo({
    required this.exteriorUserId,
    required this.userImage,
  });

  factory Top3UserInfo.fromJson(Map<String, dynamic> json) =>
      _$Top3UserInfoFromJson(json);

  Map<String, dynamic> toJson() => _$Top3UserInfoToJson(this);
}
