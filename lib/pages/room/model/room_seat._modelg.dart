// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'room_seat_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RoomSeat _$RoomSeatFromJson(Map<String, dynamic> json) => RoomSeat(
      mic: (json['mic'] as List<dynamic>)
          .map((e) => Mic.fromJson(e as Map<String, dynamic>))
          .toList(),
      count: json['count'] as int,
    );

Map<String, dynamic> _$RoomSeatToJson(RoomSeat instance) => <String, dynamic>{
      'mic': instance.mic,
      'count': instance.count,
    };

Mic _$MicFromJson(Map<String, dynamic> json) => Mic(
      micUserInfo:
          MicUserInfo.fromJson(json['mic_user_info'] as Map<String, dynamic>),
      micNum: json['mic_num'] as int,
      heart: json['heart'] as int,
      roomMicLock: json['room_mic_lock'] as int,
      roomMicBan: json['room_mic_ban'] as int,
    );

Map<String, dynamic> _$MicToJson(Mic instance) => <String, dynamic>{
      'mic_user_info': instance.micUserInfo,
      'mic_num': instance.micNum,
      'heart': instance.heart,
      'room_mic_lock': instance.roomMicLock,
      'room_mic_ban': instance.roomMicBan,
    };

MicUserInfo _$MicUserInfoFromJson(Map<String, dynamic> json) => MicUserInfo(
      exUserId: json['ex_user_id'] as int,
      userId: json['user_id'] as String,
      sex: json['sex'] as int,
      isBanChat: json['is_ban_chat'] as int,
      userName: json['user_name'] as String,
      userImage: json['user_image'] as String,
      personalPermission: json['personal_permission'] as int,
      goods: json['goods'] == null
          ? null
          : Goods.fromJson(json['goods'] as Map<String, dynamic>),
      agoraUid: json['agora_uid'] as int,
    );

Map<String, dynamic> _$MicUserInfoToJson(MicUserInfo instance) =>
    <String, dynamic>{
      'ex_user_id': instance.exUserId,
      'user_id': instance.userId,
      'sex': instance.sex,
      'is_ban_chat': instance.isBanChat,
      'user_name': instance.userName,
      'user_image': instance.userImage,
      'personal_permission': instance.personalPermission,
      'goods': instance.goods,
      'agora_uid': instance.agoraUid,
    };

Goods _$GoodsFromJson(Map<String, dynamic> json) => Goods(
      wear: json['mic_user_info'] == null
          ? null
          : (json['wear'] as List<dynamic>)
              .map((e) => Wear.fromJson(e as Map<String, dynamic>))
              .toList(),
    );

Map<String, dynamic> _$GoodsToJson(Goods instance) => <String, dynamic>{
      'wear': instance.wear,
    };

Wear _$WearFromJson(Map<String, dynamic> json) => Wear(
      micGoodsId: json['mic_goods_id'] as int,
      imageFrameId: json['image_frame_id'] as int,
    );

Map<String, dynamic> _$WearToJson(Wear instance) => <String, dynamic>{
      'mic_goods_id': instance.micGoodsId,
      'image_frame_id': instance.imageFrameId,
    };
