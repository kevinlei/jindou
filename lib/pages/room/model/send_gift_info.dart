import 'package:json_annotation/json_annotation.dart';

part 'send_gift_info.g.dart';

@JsonSerializable()
class SendGiftInfo {
  @JsonKey(name: 'send_gift_infos')
  final List<SendGiftInfos> sendGiftInfos;
  @JsonKey(name: 'user_id')
  final String userId;

  const SendGiftInfo({
    required this.sendGiftInfos,
    required this.userId,
  });

  factory SendGiftInfo.fromJson(Map<String, dynamic> json) =>
      _$SendGiftInfoFromJson(json);

  Map<String, dynamic> toJson() => _$SendGiftInfoToJson(this);
}

@JsonSerializable()
class SendGiftInfos {
  final int id;
  @JsonKey(name: 'gift_name')
  final String giftName;
  @JsonKey(name: 'gift_picture')
  final String giftPicture;
  @JsonKey(name: 'gift_price')
  final int giftPrice;
  @JsonKey(name: 'gift_num')
  final int giftNum;

  const SendGiftInfos({
    required this.id,
    required this.giftName,
    required this.giftPicture,
    required this.giftPrice,
    required this.giftNum,
  });

  factory SendGiftInfos.fromJson(Map<String, dynamic> json) =>
      _$SendGiftInfosFromJson(json);

  Map<String, dynamic> toJson() => _$SendGiftInfosToJson(this);
}
