import 'package:json_annotation/json_annotation.dart';

part 'gift_mq.g.dart';

@JsonSerializable()
class GiftMq {
  @JsonKey(name: 'obtain_user_info')
  final ObtainUserGiftInfo obtainUserInfo;
  @JsonKey(name: 'obtain_gift_info')
  final ObtainGiftInfos obtainGiftInfo;
  @JsonKey(name: 'operate_user_info')
  final OperateUserInfo operateUserInfo;
  @JsonKey(name: 'ex_room_id')
  final int exRoomId;
  @JsonKey(name: 'room_id')
  final int roomId;

  const GiftMq({
    required this.obtainUserInfo,
    required this.obtainGiftInfo,
    required this.operateUserInfo,
    required this.exRoomId,
    required this.roomId,
  });

  factory GiftMq.fromJson(Map<String, dynamic> json) => _$GiftMqFromJson(json);

  Map<String, dynamic> toJson() => _$GiftMqToJson(this);
}

@JsonSerializable()
class ObtainUserGiftInfo {
  @JsonKey(name: 'user_id')
  final String userId;
  @JsonKey(name: 'display_id')
  final int displayId;
  final String name;
  @JsonKey(name: 'head_icon')
  final String headIcon;

  const ObtainUserGiftInfo({
    required this.userId,
    required this.displayId,
    required this.name,
    required this.headIcon,
  });

  factory ObtainUserGiftInfo.fromJson(Map<String, dynamic> json) =>
      _$ObtainUserGiftInfoFromJson(json);

  Map<String, dynamic> toJson() => _$ObtainUserGiftInfoToJson(this);
}

@JsonSerializable()
class ObtainGiftInfos {
  final int id;
  @JsonKey(name: 'gift_name')
  final String giftName;
  @JsonKey(name: 'gift_vfx')
  final String? giftVfx;
  @JsonKey(name: 'gift_picture')
  final String giftPicture;
  @JsonKey(name: 'gift_num')
  final int giftNum;
  @JsonKey(name: 'gift_level')
  final int giftLevel;

  const ObtainGiftInfos({
    required this.id,
    required this.giftName,
    required this.giftVfx,
    required this.giftPicture,
    required this.giftNum,
    required this.giftLevel,
  });

  factory ObtainGiftInfos.fromJson(Map<String, dynamic> json) =>
      _$ObtainGiftInfosFromJson(json);

  Map<String, dynamic> toJson() => _$ObtainGiftInfosToJson(this);
}

@JsonSerializable()
class OperateUserInfo {
  @JsonKey(name: 'user_id')
  final String userId;
  @JsonKey(name: 'display_id')
  final int displayId;
  final String name;
  @JsonKey(name: 'head_icon')
  final String headIcon;

  const OperateUserInfo({
    required this.userId,
    required this.displayId,
    required this.name,
    required this.headIcon,
  });

  factory OperateUserInfo.fromJson(Map<String, dynamic> json) =>
      _$OperateUserInfoFromJson(json);

  Map<String, dynamic> toJson() => _$OperateUserInfoToJson(this);
}
