// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'magic_box.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MagicBox _$MagicBoxFromJson(Map<String, dynamic> json) => MagicBox(
      id: json['id'] as int,
      gameName: json['game_name'] as String,
      gameId: json['game_id'] as int,
      gameScale: json['game_scale'] as int,
      gameState: json['game_state'] as int,
      wealthLimit: json['wealth_limit'] as int,
      coinName: json['coin_name'] as String,
      gameLogo: json['game_logo'] as String,
      gameTheme: json['game_theme'] as int,
    );

Map<String, dynamic> _$MagicBoxToJson(MagicBox instance) => <String, dynamic>{
      'id': instance.id,
      'game_name': instance.gameName,
      'game_id': instance.gameId,
      'game_scale': instance.gameScale,
      'game_state': instance.gameState,
      'wealth_limit': instance.wealthLimit,
      'coin_name': instance.coinName,
      'game_logo': instance.gameLogo,
      'game_theme': instance.gameTheme,
    };
