import 'package:json_annotation/json_annotation.dart';

part 'room_seat._modelg.dart';

@JsonSerializable()
class RoomSeat {
  final List<Mic> mic;
  final int count;

  const RoomSeat({
    required this.mic,
    required this.count,
  });

  factory RoomSeat.fromJson(Map<String, dynamic> json) =>
      _$RoomSeatFromJson(json);

  Map<String, dynamic> toJson() => _$RoomSeatToJson(this);
}

@JsonSerializable()
class Mic {
  @JsonKey(name: 'mic_user_info')
  late MicUserInfo micUserInfo;
  @JsonKey(name: 'mic_num')
  final int micNum;
  late int heart;
  @JsonKey(name: 'room_mic_lock')
  late int roomMicLock;
  @JsonKey(name: 'room_mic_ban')
  late int roomMicBan;

  Mic({
    required this.micUserInfo,
    required this.micNum,
    required this.heart,
    required this.roomMicLock,
    required this.roomMicBan,
  });

  factory Mic.fromJson(Map<String, dynamic> json) => _$MicFromJson(json);

  Map<String, dynamic> toJson() => _$MicToJson(this);
}

@JsonSerializable()
class MicUserInfo {
  @JsonKey(name: 'ex_user_id')
  final int exUserId;
  @JsonKey(name: 'user_id')
  final String userId;
  final int sex;
  @JsonKey(name: 'is_ban_chat')
  final int isBanChat;
  @JsonKey(name: 'user_name')
  final String userName;
  @JsonKey(name: 'user_image')
  final String userImage;
  @JsonKey(name: 'personal_permission')
  final int personalPermission;
  final Goods? goods;
  @JsonKey(name: 'agora_uid')
  final int agoraUid;

  const MicUserInfo({
    required this.exUserId,
    required this.userId,
    required this.sex,
    required this.isBanChat,
    required this.userName,
    required this.userImage,
    required this.personalPermission,
    required this.goods,
    required this.agoraUid,
  });

  factory MicUserInfo.fromJson(Map<String, dynamic> json) =>
      _$MicUserInfoFromJson(json);

  Map<String, dynamic> toJson() => _$MicUserInfoToJson(this);
}

@JsonSerializable()
class Goods {
  @JsonKey(name: 'wear')
  final List<Wear>? wear;

  const Goods({
    required this.wear,
  });

  factory Goods.fromJson(Map<String, dynamic> json) => _$GoodsFromJson(json);

  Map<String, dynamic> toJson() => _$GoodsToJson(this);
}

@JsonSerializable()
class Wear {
  @JsonKey(name: 'mic_goods_id')
  final int micGoodsId;
  @JsonKey(name: 'image_frame_id')
  final int imageFrameId;

  const Wear({
    required this.micGoodsId,
    required this.imageFrameId,
  });

  factory Wear.fromJson(Map<String, dynamic> json) => _$WearFromJson(json);

  Map<String, dynamic> toJson() => _$WearToJson(this);
}
