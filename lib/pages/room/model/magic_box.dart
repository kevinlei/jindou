import 'package:json_annotation/json_annotation.dart';

part 'magic_box.g.dart';

@JsonSerializable()
class MagicBox {
  final int id;
  @JsonKey(name: 'game_name')
  final String gameName;
  @JsonKey(name: 'game_id')
  final int gameId;
  @JsonKey(name: 'game_scale')
  final int gameScale;
  @JsonKey(name: 'game_state')
  final int gameState;
  @JsonKey(name: 'wealth_limit')
  final int wealthLimit;
  @JsonKey(name: 'coin_name')
  final String coinName;
  @JsonKey(name: 'game_logo')
  final String gameLogo;
  @JsonKey(name: 'game_theme')
  final int gameTheme;

  const MagicBox({
    required this.id,
    required this.gameName,
    required this.gameId,
    required this.gameScale,
    required this.gameState,
    required this.wealthLimit,
    required this.coinName,
    required this.gameLogo,
    required this.gameTheme,
  });

  factory MagicBox.fromJson(Map<String, dynamic> json) =>
      _$MagicBoxFromJson(json);

  Map<String, dynamic> toJson() => _$MagicBoxToJson(this);
}
