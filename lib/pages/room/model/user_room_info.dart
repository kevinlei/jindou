import 'package:json_annotation/json_annotation.dart';

import '../../navigator/model/login_user_model.dart';

part 'user_room_info.g.dart';

@JsonSerializable()
class UserRoomInfo {
  final Info info;
  @JsonKey(name: 'gold_info')
  final GoldInfo goldInfo;
  @JsonKey(name: 'grade_info')
  final GradeInfo gradeInfo;
  final int fans;
  final int relationship;
  @JsonKey(name: 'gift_info')
  final GiftInfoUser giftInfo;
  @JsonKey(name: 'room_info')
  final RoomInfoUser roomInfo;

  const UserRoomInfo({
    required this.info,
    required this.goldInfo,
    required this.gradeInfo,
    required this.fans,
    required this.relationship,
    required this.giftInfo,
    required this.roomInfo,
  });

  factory UserRoomInfo.fromJson(Map<String, dynamic> json) =>
      _$UserRoomInfoFromJson(json);

  Map<String, dynamic> toJson() => _$UserRoomInfoToJson(this);
}

@JsonSerializable()
class RoomInfoUser {
  @JsonKey(name: 'exterior_room_id')
  final int exteriorRoomId;
  @JsonKey(name: 'room_id')
  final int roomId;
  @JsonKey(name: 'room_name')
  final String roomName;
  @JsonKey(name: 'room_person_num')
  final int roomPersonNum;
  @JsonKey(name: 'room_image')
  final String roomImage;
  @JsonKey(name: 'room_role')
  final int roomRole;
  @JsonKey(name: 'is_ban_chat')
  final int isBanChat;
  @JsonKey(name: 'is_ban_mic')
  final int isBanMic;

  const RoomInfoUser({
    required this.exteriorRoomId,
    required this.roomId,
    required this.roomName,
    required this.roomPersonNum,
    required this.roomImage,
    required this.roomRole,
    required this.isBanChat,
    required this.isBanMic,
  });

  factory RoomInfoUser.fromJson(Map<String, dynamic> json) =>
      _$RoomInfoUserFromJson(json);

  Map<String, dynamic> toJson() => _$RoomInfoUserToJson(this);
}

@JsonSerializable()
class GiftInfoUser {
  @JsonKey(name: 'gift_count')
  final int giftCount;
  @JsonKey(name: 'gift_list')
  final List<GiftInfoList> giftList;

  const GiftInfoUser({
    required this.giftCount,
    required this.giftList,
  });

  factory GiftInfoUser.fromJson(Map<String, dynamic> json) =>
      _$GiftInfoUserFromJson(json);

  Map<String, dynamic> toJson() => _$GiftInfoUserToJson(this);
}

@JsonSerializable()
class GiftInfoList {
  @JsonKey(name: 'gift_id')
  final int giftId;
  @JsonKey(name: 'gift_num')
  final int giftNum;
  @JsonKey(name: 'gift_name')
  final String giftName;
  @JsonKey(name: 'gift_vfx')
  final String giftVfx;
  @JsonKey(name: 'gift_picture')
  final String giftPicture;
  @JsonKey(name: 'gift_price')
  final int giftPrice;

  const GiftInfoList({
    required this.giftId,
    required this.giftNum,
    required this.giftName,
    required this.giftVfx,
    required this.giftPicture,
    required this.giftPrice,
  });

  factory GiftInfoList.fromJson(Map<String, dynamic> json) =>
      _$GiftInfoListFromJson(json);

  Map<String, dynamic> toJson() => _$GiftInfoListToJson(this);
}
