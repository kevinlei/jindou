// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'gift_mq.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GiftMq _$GiftMqFromJson(Map<String, dynamic> json) => GiftMq(
      obtainUserInfo: ObtainUserGiftInfo.fromJson(
          json['obtain_user_info'] as Map<String, dynamic>),
      obtainGiftInfo: ObtainGiftInfos.fromJson(
          json['obtain_gift_info'] as Map<String, dynamic>),
      operateUserInfo: OperateUserInfo.fromJson(
          json['operate_user_info'] as Map<String, dynamic>),
      exRoomId: json['ex_room_id'] as int,
      roomId: json['room_id'] as int,
    );

Map<String, dynamic> _$GiftMqToJson(GiftMq instance) => <String, dynamic>{
      'obtain_user_info': instance.obtainUserInfo,
      'obtain_gift_info': instance.obtainGiftInfo,
      'operate_user_info': instance.operateUserInfo,
      'ex_room_id': instance.exRoomId,
      'room_id': instance.roomId,
    };

ObtainUserGiftInfo _$ObtainUserGiftInfoFromJson(Map<String, dynamic> json) =>
    ObtainUserGiftInfo(
      userId: json['user_id'] as String,
      displayId: json['display_id'] as int,
      name: json['name'] as String,
      headIcon: json['head_icon'] as String,
    );

Map<String, dynamic> _$ObtainUserGiftInfoToJson(ObtainUserGiftInfo instance) =>
    <String, dynamic>{
      'user_id': instance.userId,
      'display_id': instance.displayId,
      'name': instance.name,
      'head_icon': instance.headIcon,
    };

ObtainGiftInfos _$ObtainGiftInfosFromJson(Map<String, dynamic> json) =>
    ObtainGiftInfos(
      id: json['id'] as int,
      giftName: json['gift_name'] as String,
      giftVfx: json['gift_vfx'] as String?,
      giftPicture: json['gift_picture'] as String,
      giftNum: json['gift_num'] as int,
      giftLevel: json['gift_level'] as int,
    );

Map<String, dynamic> _$ObtainGiftInfosToJson(ObtainGiftInfos instance) =>
    <String, dynamic>{
      'id': instance.id,
      'gift_name': instance.giftName,
      'gift_vfx': instance.giftVfx,
      'gift_picture': instance.giftPicture,
      'gift_num': instance.giftNum,
      'gift_level': instance.giftLevel,
    };

OperateUserInfo _$OperateUserInfoFromJson(Map<String, dynamic> json) =>
    OperateUserInfo(
      userId: json['user_id'] as String,
      displayId: json['display_id'] as int,
      name: json['name'] as String,
      headIcon: json['head_icon'] as String,
    );

Map<String, dynamic> _$OperateUserInfoToJson(OperateUserInfo instance) =>
    <String, dynamic>{
      'user_id': instance.userId,
      'display_id': instance.displayId,
      'name': instance.name,
      'head_icon': instance.headIcon,
    };
