// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'room_types.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RoomTypes _$RoomTypesFromJson(Map<String, dynamic> json) => RoomTypes(
      topicId: json['topic_id'] as int?,
      topicName: json['topic_name'] as String,
      topicType: json['topic_type'] as int?,
      categoryId: json['category_id'] as int?,
      subCategoryId: json['sub_category_id'] as int?,
      topicColor: json['topic_color'] as String?,
      topicBackground: json['topic_background'] as String?,
      topicIcon: json['topic_icon'] as String?,
      topicPicture: json['topic_picture'] as String?,
      topicState: json['topic_state'] as int?,
      sort: json['sort'] as int?,
    );

Map<String, dynamic> _$RoomTypesToJson(RoomTypes instance) => <String, dynamic>{
      'topic_id': instance.topicId,
      'topic_name': instance.topicName,
      'topic_type': instance.topicType,
      'category_id': instance.categoryId,
      'sub_category_id': instance.subCategoryId,
      'topic_color': instance.topicColor,
      'topic_background': instance.topicBackground,
      'topic_icon': instance.topicIcon,
      'topic_picture': instance.topicPicture,
      'topic_state': instance.topicState,
      'sort': instance.sort,
    };
