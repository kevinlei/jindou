// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_room_info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserRoomInfo _$UserRoomInfoFromJson(Map<String, dynamic> json) => UserRoomInfo(
      info: Info.fromJson(json['info'] as Map<String, dynamic>),
      goldInfo: GoldInfo.fromJson(json['gold_info'] as Map<String, dynamic>),
      gradeInfo: GradeInfo.fromJson(json['grade_info'] as Map<String, dynamic>),
      fans: json['fans'] as int,
      relationship: json['relationship'] as int,
      giftInfo:
          GiftInfoUser.fromJson(json['gift_info'] as Map<String, dynamic>),
      roomInfo:
          RoomInfoUser.fromJson(json['room_info'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$UserRoomInfoToJson(UserRoomInfo instance) =>
    <String, dynamic>{
      'info': instance.info,
      'gold_info': instance.goldInfo,
      'grade_info': instance.gradeInfo,
      'fans': instance.fans,
      'relationship': instance.relationship,
      'gift_info': instance.giftInfo,
      'room_info': instance.roomInfo,
    };

RoomInfoUser _$RoomInfoUserFromJson(Map<String, dynamic> json) => RoomInfoUser(
      exteriorRoomId: json['exterior_room_id'] as int,
      roomId: json['room_id'] as int,
      roomName: json['room_name'] as String,
      roomPersonNum: json['room_person_num'] as int,
      roomImage: json['room_image'] as String,
      roomRole: json['room_role'] as int,
      isBanChat: json['is_ban_chat'] as int,
      isBanMic: json['is_ban_mic'] as int,
    );

Map<String, dynamic> _$RoomInfoUserToJson(RoomInfoUser instance) =>
    <String, dynamic>{
      'exterior_room_id': instance.exteriorRoomId,
      'room_id': instance.roomId,
      'room_name': instance.roomName,
      'room_person_num': instance.roomPersonNum,
      'room_image': instance.roomImage,
      'room_role': instance.roomRole,
      'is_ban_chat': instance.isBanChat,
      'is_ban_mic': instance.isBanMic,
    };

GiftInfoUser _$GiftInfoUserFromJson(Map<String, dynamic> json) => GiftInfoUser(
      giftCount: json['gift_count'] as int,
      giftList: (json['gift_list'] as List<dynamic>)
          .map((e) => GiftInfoList.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$GiftInfoUserToJson(GiftInfoUser instance) =>
    <String, dynamic>{
      'gift_count': instance.giftCount,
      'gift_list': instance.giftList,
    };

GiftInfoList _$GiftInfoListFromJson(Map<String, dynamic> json) => GiftInfoList(
      giftId: json['gift_id'] as int,
      giftNum: json['gift_num'] as int,
      giftName: json['gift_name'] as String,
      giftVfx: json['gift_vfx'] as String,
      giftPicture: json['gift_picture'] as String,
      giftPrice: json['gift_price'] as int,
    );

Map<String, dynamic> _$GiftInfoListToJson(GiftInfoList instance) =>
    <String, dynamic>{
      'gift_id': instance.giftId,
      'gift_num': instance.giftNum,
      'gift_name': instance.giftName,
      'gift_vfx': instance.giftVfx,
      'gift_picture': instance.giftPicture,
      'gift_price': instance.giftPrice,
    };
