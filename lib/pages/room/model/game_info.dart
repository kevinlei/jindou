import 'package:json_annotation/json_annotation.dart';

part 'game_info.g.dart';

@JsonSerializable()
class GameInfo {
  @JsonKey(name: 'user_id')
  final String userId;
  @JsonKey(name: 'user_name')
  final String userName;
  @JsonKey(name: 'head_icon')
  final String headIcon;
  @JsonKey(name: 'top_picture')
  final String? topPicture;
  @JsonKey(name: 'game_name')
  final String gameName;
  @JsonKey(name: 'room_id')
  final int roomId;
  @JsonKey(name: 'gift_list')
  final List<GameGiftInfo>? giftList;
  @JsonKey(name: 'gift_info')
  final GameGiftInfo? giftInfo;

  const GameInfo({
    required this.userId,
    required this.userName,
    required this.headIcon,
    this.topPicture,
    required this.gameName,
    required this.roomId,
    this.giftList,
    this.giftInfo,
  });

  factory GameInfo.fromJson(Map<String, dynamic> json) =>
      _$GameInfoFromJson(json);

  Map<String, dynamic> toJson() => _$GameInfoToJson(this);
}

@JsonSerializable()
class GameGiftInfo {
  @JsonKey(name: 'gift_name')
  final String giftName;
  @JsonKey(name: 'gift_price')
  final int giftPrice;
  @JsonKey(name: 'gift_num')
  final int giftNum;
  @JsonKey(name: 'gift_picture')
  final String giftPicture;

  const GameGiftInfo({
    required this.giftName,
    required this.giftPrice,
    required this.giftNum,
    required this.giftPicture,
  });

  factory GameGiftInfo.fromJson(Map<String, dynamic> json) =>
      _$GameGiftInfoFromJson(json);

  Map<String, dynamic> toJson() => _$GameGiftInfoToJson(this);
}
