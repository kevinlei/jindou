// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'game_info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GameInfo _$GameInfoFromJson(Map<String, dynamic> json) => GameInfo(
      userId: json['user_id'] as String,
      userName: json['user_name'] as String,
      headIcon: json['head_icon'] as String,
      topPicture: json['top_picture'] as String?,
      gameName: json['game_name'] as String,
      roomId: json['room_id'] as int,
      giftList: (json['gift_list'] as List<dynamic>?)
          ?.map((e) => GameGiftInfo.fromJson(e as Map<String, dynamic>))
          .toList(),
      giftInfo: json['gift_info'] == null
          ? null
          : GameGiftInfo.fromJson(json['gift_info'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$GameInfoToJson(GameInfo instance) => <String, dynamic>{
      'user_id': instance.userId,
      'user_name': instance.userName,
      'head_icon': instance.headIcon,
      'top_picture': instance.topPicture,
      'game_name': instance.gameName,
      'room_id': instance.roomId,
      'gift_list': instance.giftList,
      'gift_info': instance.giftInfo,
    };

GameGiftInfo _$GameGiftInfoFromJson(Map<String, dynamic> json) => GameGiftInfo(
      giftName: json['gift_name'] as String,
      giftPrice: json['gift_price'] as int,
      giftNum: json['gift_num'] as int,
      giftPicture: json['gift_picture'] as String,
    );

Map<String, dynamic> _$GameGiftInfoToJson(GameGiftInfo instance) =>
    <String, dynamic>{
      'gift_name': instance.giftName,
      'gift_price': instance.giftPrice,
      'gift_num': instance.giftNum,
      'gift_picture': instance.giftPicture,
    };
