import 'package:json_annotation/json_annotation.dart';

part 'room_types.g.dart';

@JsonSerializable()
class RoomTypes {
  @JsonKey(name: 'topic_id')
  final int? topicId;
  @JsonKey(name: 'topic_name')
  final String topicName;
  @JsonKey(name: 'topic_type')
  final int? topicType;
  @JsonKey(name: 'category_id')
  final int? categoryId;
  @JsonKey(name: 'sub_category_id')
  final int? subCategoryId;
  @JsonKey(name: 'topic_color')
  final String? topicColor;
  @JsonKey(name: 'topic_background')
  final String? topicBackground;
  @JsonKey(name: 'topic_icon')
  final String? topicIcon;
  @JsonKey(name: 'topic_picture')
  final String? topicPicture;
  @JsonKey(name: 'topic_state')
  final int? topicState;
  final int? sort;

  const RoomTypes({
    this.topicId,
    required this.topicName,
    this.topicType,
    this.categoryId,
    this.subCategoryId,
    this.topicColor,
    this.topicBackground,
    this.topicIcon,
    this.topicPicture,
    this.topicState,
    this.sort,
  });

  factory RoomTypes.fromJson(Map<String, dynamic> json) =>
      _$RoomTypesFromJson(json);

  Map<String, dynamic> toJson() => _$RoomTypesToJson(this);
}
