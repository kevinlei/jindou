// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'room_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RoomModel _$RoomModelFromJson(Map<String, dynamic> json) => RoomModel(
      exteriorRoomId: json['exterior_room_id'] as int,
      roomId: json['room_id'] as int,
      hxId: json['chat_id'] as String,
      roomName: json['room_name'] as String,
      isBanChat: json['is_ban_chat'] as int,
      isBanMic: json['is_ban_mic'] as int,
      roomBulletin: json['room_bulletin'] as String,
      roomHot: json['room_hot'] as int,
      headIcon: json['image'] as String,
      peopleNum: json['people_num'] as int,
      personalPermission: json['personal_permission'] as int,
      top3UserInfo: (json['top_3_user_info'] as List<dynamic>)
          .map((e) => Top3UserInfo.fromJson(e as Map<String, dynamic>))
          .toList(),
      isFollow: json['is_follow'] as int,
      isMicApply: json['is_mic_apply'] as int,
      bgImg: json['room_background_image'] as String,
      roomIsChat: json['room_is_chat'] as int,
      isHeart: json['is_heart'] as int,
    );

Map<String, dynamic> _$RoomModelToJson(RoomModel instance) => <String, dynamic>{
      'exterior_room_id': instance.exteriorRoomId,
      'room_id': instance.roomId,
      'chat_id': instance.hxId,
      'is_ban_chat': instance.isBanChat,
      'is_ban_mic': instance.isBanMic,
      'room_name': instance.roomName,
      'room_bulletin': instance.roomBulletin,
      'room_hot': instance.roomHot,
      'image': instance.headIcon,
      'room_background_image': instance.bgImg,
      'people_num': instance.peopleNum,
      'personal_permission': instance.personalPermission,
      'top_3_user_info': instance.top3UserInfo,
      'is_follow': instance.isFollow,
      'is_mic_apply': instance.isMicApply,
      'room_is_chat': instance.roomIsChat,
      'is_heart': instance.isHeart,
    };

Top3UserInfo _$Top3UserInfoFromJson(Map<String, dynamic> json) => Top3UserInfo(
      exteriorUserId: json['exterior_user_id'] as int,
      userImage: json['user_image'] as String,
    );

Map<String, dynamic> _$Top3UserInfoToJson(Top3UserInfo instance) =>
    <String, dynamic>{
      'exterior_user_id': instance.exteriorUserId,
      'user_image': instance.userImage,
    };
