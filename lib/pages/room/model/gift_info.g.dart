// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'gift_info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GiftInfo _$GiftInfoFromJson(Map<String, dynamic> json) => GiftInfo(
      id: json['id'] as int,
      giftNum: json['gift_num'] as int?,
      giftName: json['gift_name'] as String,
      giftPicture: json['gift_picture'] as String,
      giftPrice: json['gift_price'] as int,
      isStar: json['is_star'] as int,
      isHot: json['is_hot'] as int,
      label: json['label'] as String,
      isActivity: json['is_activity'] as int,
    );

Map<String, dynamic> _$GiftInfoToJson(GiftInfo instance) => <String, dynamic>{
      'id': instance.id,
      'gift_num': instance.giftNum,
      'gift_name': instance.giftName,
      'gift_picture': instance.giftPicture,
      'gift_price': instance.giftPrice,
      'is_star': instance.isStar,
      'is_hot': instance.isHot,
      'label': instance.label,
      'is_activity': instance.isActivity,
    };
