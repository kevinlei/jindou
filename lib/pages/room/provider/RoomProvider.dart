import 'package:flutter/material.dart';
import 'package:echo/import.dart';

class RoomProvider extends ChangeNotifier {
  // 房间信息
  RoomModel? roomModel;

  // 房间内规定财富值
  double richExp = 0;

  // 更新房间信息
  void updateRoom({bool isAppmanager = false}) {
    isAppmanager
        ? roomModel = AppManager().roomInfo.roomMode
        : AppManager().roomInfo.roomMode = roomModel;
    notifyListeners();
  }

  // 返回麦位名称
  String getSeatName(int pitNumber) {
    if (pitNumber == 1) {
      return "房主";
    }
    if (pitNumber == 2) {
      return "主持";
    }
    return "${pitNumber - 1}号麦";
  }
}

// 房间密码
class RoomPassWord extends StatefulWidget {
  const RoomPassWord({super.key});

  @override
  State<RoomPassWord> createState() => _RoomPassWordState();
}

class _RoomPassWordState extends State<RoomPassWord> {
  String? textValue;
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: [
        FContainer(
          padding: EdgeInsets.all(40.w),
          width: 500.w,
          radius: BorderRadius.circular(20.w),
          color: Colors.white,
          child: Column(
            children: [
              FText(
                "请输入房间密码",
                size: 34.sp,
                color: Colors.black,
              ),
              SizedBox(height: 10.w),
              TextField(
                maxLength: 4,
                onChanged: (value) {
                  setState(() {
                    textValue = value;
                  });
                },
              ),
              SizedBox(height: 10.w),
              FButton(
                onTap: () => WidgetUtils().popPage(textValue),
                isTheme: true,
                padding: EdgeInsets.symmetric(vertical: 20.w, horizontal: 40.w),
                child: FText(
                  "确定",
                  size: 28.sp,
                ),
              )
            ],
          ),
        )
      ],
    );
  }
}
