import 'dart:convert';

import 'package:echo/import.dart';
import 'package:flutter/material.dart';

class RoomPage extends StatefulWidget {
  final int roomId;
  final RoomModel roomInfo;
  final List<Mic> micList;
  final bool isNew;
  const RoomPage(this.roomId, this.roomInfo, this.micList,
      {super.key, this.isNew = true});

  @override
  State<RoomPage> createState() => _RoomPageState();
}

class _RoomPageState extends State<RoomPage> {
  RoomProvider roomProvider = RoomProvider();

  // 房间背景图片
  String bgImg = "";

  @override
  void initState() {
    super.initState();
    bgImg = widget.roomInfo.bgImg;
    roomProvider.roomModel = widget.roomInfo;
    initGifts();
    getBoxMicGifts();

    EventUtils().on(
      EventConstants.E_ROOM_BGIMG,
      EventCall("RoomPage", (v) {
        bgImg = v;
        if (!mounted) return;
        setState(() {});
      }),
    );
    EventUtils().on(
      EventConstants.E_ROOM_INFO_UPDATE,
      EventCall("RoomPage", (v) {
        roomProvider.updateRoom(isAppmanager: true);
      }),
    );
    EventUtils().on(
      EventConstants.E_ROOM_MY_OUT,
      EventCall("RoomPage", (v) {
        WidgetUtils().popPage();
      }),
    );

    if (widget.isNew) {
      AppManager().joinEngineManager();
      AppManager().xinstallFlutterPlugin.reportEventWhenOpenDetailInfo(
          "getinto",
          AppManager().roomInfo.roomMode!.exteriorRoomId,
          UserConstants.userginInfo!.info.userId);
      Future.delayed(const Duration(milliseconds: 300), () async {
        EventUtils().emit(EventConstants.E_MQ_MESSAGE, [
          "",
          {
            "event": MqMsgType.SELF_JOIN.value,
            "info": {
              "type": 1,
              "displayId": UserConstants.userginInfo!.info.displayId
            }
          }
        ]);
      });
    }
  }

  void initGifts() {
    DioUtils().asyncHttpRequest(
      NetConstants.knapsackGift,
      loading: false,
      onSuccess: (res) {
        if (res == null) return;
        var data = jsonDecode(res);
        List<GiftInfo> gifts = [];
        for (var i = 0; i < data.length; i++) {
          GiftInfo info = GiftInfo.fromJson(data[i]);
          gifts.add(info);
        }
        AppManager().roomInfo.gifts = gifts;
        for (var i = 0; i < gifts.length; i++) {
          precacheImage(
            ImageUtils()
                .getImageProvider(NetConstants.ossPath + gifts[i].giftPicture),
            context,
          );
        }
      },
    );

    DioUtils().asyncHttpRequest(
      NetConstants.allGift,
      loading: false,
      params: {"extra_param": "magic_box"},
      onSuccess: (res) {
        if (res == null) return;
        var data = jsonDecode(res);
        List<GiftInfo> gifts = [];
        for (var i = 0; i < data.length; i++) {
          GiftInfo info = GiftInfo.fromJson(data[i]);
          gifts.add(info);
        }
        AppManager().roomInfo.gifts = gifts;
        for (var i = 0; i < gifts.length; i++) {
          precacheImage(
            ImageUtils()
                .getImageProvider(NetConstants.ossPath + gifts[i].giftPicture),
            context,
          );
        }
      },
    );
  }

  @override
  void dispose() {
    EventUtils()
        .off(EventConstants.E_ROOM_BGIMG, EventCall("RoomPage", (v) {}));
    EventUtils()
        .off(EventConstants.E_ROOM_INFO_UPDATE, EventCall("RoomPage", (v) {}));
    EventUtils()
        .off(EventConstants.E_ROOM_MY_OUT, EventCall("RoomPage", (v) {}));
    super.dispose();
  }

  void getBoxMicGifts() {
    DioUtils().asyncHttpRequest(
      NetConstants.boxGiftList,
      method: DioMethod.POST,
      loading: false,
      hastoast: false,
      onSuccess: (res) {
        AppManager().roomInfo.magicBoxGiftList = res["box_gift_info"];
        AppManager().roomInfo.magicBoxUrl = res["jmp_detail"];
      },
    );
    DioUtils().asyncHttpRequest(
      NetConstants.config,
      hastoast: false,
      params: {"conf_key": "rule"},
      loading: false,
      onSuccess: (res) {
        if (res == null || res.toString() == "1") {
          AppManager().roomInfo.magicBoxUrl = "";
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: roomProvider),
      ],
      child: FContainer(
        color: const Color(0xFF11181C),
        child: Stack(
          children: [
            roomBackgroundImage(),
            roomMainContent(),
            RoomMusicFloat(widget.roomId), // 音乐播放器
            const RoomGiftAnim(), // 房间礼物漂屏
            const RoomAnim(), // 房间礼物特效
            const TopFloatingScreen(), // 房间送礼/抽奖消息 顶部横幅
            const JoinBulletAnim(), // 进房间动画
          ],
        ),
      ),
    );
  }

  Widget roomMainContent() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: ScreenUtil().statusBarHeight + 20.w),
        RoomHeader(widget.roomId),
        const RoomWiFi(),
        RoomSeatList(widget.roomId),
        Expanded(child: roomFooter()),
        const RoomKeyboard(),
      ],
    );
  }

  Widget roomBackgroundImage() {
    return LoadNetImage(
      NetConstants.ossPath + bgImg,
      width: double.infinity,
      height: double.infinity,
      fit: BoxFit.fill,
      bgc: true,
    );
  }

  Widget roomFooter() {
    return Row(
      children: [
        Expanded(child: RoomChatGroup(widget.roomId)), // 聊天
        Column(
          children: const [
            RoomActivity(), // 公告
            RoomGameGroup(), // 游戏
          ],
        )
      ],
    );
  }
}
