import 'dart:convert';
import 'dart:io';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:echo/import.dart';
import 'package:flutter/cupertino.dart';
import 'package:open_settings/open_settings.dart';

class AppConstants {
  /// 运行环境
  static AppEnv appEnv = AppEnv.ENV_DEV;

  /// 开启打印
  static bool activeLog = true;

  /// 版本号
  static String version = '1.0.9';

  /// 设备号
  static String machineCode = '';

  /// 设备信息
  static String device = '';

  /// 设备唯一标识
  static String deviceId = '';

  /// 安卓版本
  static String androidVersion = '';

  /// 是否模拟器
  static bool isDevice = true;

  /// 鉴权token
  static String accessToken = '';

  /// 刷新token
  static String refreshToken = '';

  /// 代理地址
  static String proxyIP = "";

  /// 当前app构建版本
  static String appBuild = 'Build 2';

  /// 渠道
  static String channel = const String.fromEnvironment('APP_CHANNEL');

  /// 世界频道
  static String worldChannel = "";

  /// 首次启动
  static String Launchl = "";

  /// 启用声网还是TRTC
  // 0使用腾讯TRTC，1使用声网Agora
  static int trtcAgoraSwitchFlag = 1;

  /// 分页数据量
  static int pageSize = 20;

  static bool hasShowUpdate = false;
  static bool hasShowLogin = false;
  static int chatOnLevel = 0;

  // 睡眠模式
  String appSleep() {
    if (device.toLowerCase().contains("huawei")) {
      return BackstageActivation.HUAWEI.sleep;
    } else if (device.toLowerCase().contains("honor")) {
      return BackstageActivation.HONOR.sleep;
    } else if (device.toLowerCase().contains("xiaomi")) {
      return BackstageActivation.XIAOMI.sleep;
    } else if (device.toLowerCase().contains("vivo")) {
      return BackstageActivation.VIVO.sleep;
    } else if (device.toLowerCase().contains("oppo")) {
      return BackstageActivation.OPPO.sleep;
    } else {
      return BackstageActivation.NONE.sleep;
    }
  }

  // 后台保活
  String activation() {
    if (device.toLowerCase().contains("huawei")) {
      return BackstageActivation.HUAWEI.value;
    } else if (device.toLowerCase().contains("honor")) {
      return BackstageActivation.HONOR.value;
    } else if (device.toLowerCase().contains("xiaomi")) {
      return BackstageActivation.XIAOMI.value;
    } else if (device.toLowerCase().contains("vivo")) {
      return BackstageActivation.VIVO.value;
    } else if (device.toLowerCase().contains("oppo")) {
      return BackstageActivation.OPPO.value;
    } else {
      return BackstageActivation.NONE.value;
    }
  }

  // 设置睡眠模式
  void setAppSleep() async {
    if (device.toLowerCase().contains("xiaomi")) {
      await PluginUtils().sleepSettings("xiaomi");
    } else if (device.toLowerCase().contains("vivo")) {
      await PluginUtils().sleepSettings("vivo");
    } else if (device.toLowerCase().contains("oppo")) {
      await PluginUtils().sleepSettings("oppo");
    } else {
      await OpenSettings.openMainSetting();
    }
  }

  // 设置后台保活
  void setActivation() async {
    if (device.toLowerCase().contains("huawei")) {
      await OpenSettings.openMainSetting();
    } else if (device.toLowerCase().contains("honor")) {
      await OpenSettings.openMainSetting();
    } else if (device.toLowerCase().contains("xiaomi")) {
      await PluginUtils().systemSettings("xiaomi");
    } else if (device.toLowerCase().contains("vivo")) {
      await PluginUtils().systemSettings("vivo");
    } else if (device.toLowerCase().contains("oppo")) {
      await PluginUtils().systemSettings("oppo");
    } else {
      await OpenSettings.openMainSetting();
    }
  }

  // 模拟器筛选特殊字符
  String flutterText(String text) {
    if (!AppConstants.isDevice) {
      String und = "";
      for (var i = 0; i < text.length; i++) {
        und += text[i];
      }
      text = und;
    }
    return text;
  }

  /// 渠道
  static Future<void> channelStatistics(String action, {String? extra}) async {
    // DioUtils().asyncHttpRequest(
    //   NetConstants.channelStatistics,
    //   method: DioMethod.POST,
    //   loading: false,
    //   params: {
    //     "channel": channel,
    //     "uid": "",
    //     "action": action,
    //     "extra": extra ?? "",
    //     "os": Platform.isAndroid ? "android" : "ios",
    //     "os_ver": device,
    //     "app_ver": "${version}_$appBuild",
    //   },
    // );
  }

  static void initWordCan() {
    DioUtils().asyncHttpRequest(
      NetConstants.config,
      params: {"conf_key": "chat_world_channel"},
      loading: false,
      onSuccess: (res) {
        Map data = jsonDecode(res);
        if (data["chat_id"] != null) {
          worldChannel = data["chat_id"].toString();
        }
      },
    );
  }

  static Future<void> initAppInfo() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    accessToken = preferences.getString(SharedKey.KEY_TOKEN.value) ?? "";
    refreshToken = preferences.getString(SharedKey.KEY_REF_TOKEN.value) ?? "";
    machineCode = preferences.getString(SharedKey.KEY_MACHINE.value) ?? "";
    proxyIP = preferences.getString(SharedKey.KEY_PROXY.value) ?? "";

    // 初始化系统通知
    List<String> list =
        preferences.getStringList(SharedKey.KEY_NOTICE_SET.value) ?? [];
    UserConstants.getNoticeSet(list);

    if (Platform.isAndroid) {
      // 获取设备信息
      var androidInfo = await DeviceInfoPlugin().androidInfo;
      var release = androidInfo.version.release;
      var sdkInt = androidInfo.version.sdkInt;
      var manufacturer = androidInfo.manufacturer;
      var model = androidInfo.model;
      var androidId = androidInfo.androidId;
      deviceId = androidId ?? '';
      debugPrint(
          'Android ${androidInfo.id} $release (SDK $sdkInt), $manufacturer $model ${androidInfo.androidId}');
      device =
          'Android $release (SDK $sdkInt), $manufacturer $model $androidId';
      androidVersion = release ?? "";

      // 是否模拟器
      if (androidInfo.host == 'ubuntu' && androidInfo.device == 'aosp') {
        isDevice = false;
      }
      bool anyFolderExists(List<String> foldersPaths) {
        for (String folderPath in foldersPaths) {
          if (Directory(folderPath).existsSync()) {
            return true;
          }
        }
        return false;
      }

      List<String> harmfulFoldersPaths = [
        '/storage/emulated/0/storage/secure',
        '/storage/emulated/0/Android/data/com.android.ld.appstore',
      ];
      if (anyFolderExists(harmfulFoldersPaths)) {
        isDevice = false;
      }
    } else if (Platform.isIOS) {
      var iosInfo = await DeviceInfoPlugin().iosInfo;
      var systemName = iosInfo.systemName;
      var version = iosInfo.systemVersion;
      var name = iosInfo.name;
      var model = iosInfo.model;
      debugPrint(
          'IOS $systemName $version, $name $model ${iosInfo.identifierForVendor}');
      deviceId = iosInfo.identifierForVendor ?? '';
      device = '$systemName $version, $name $model';
    }

    debugPrint(
        'version = $version | machineCode = $machineCode  | accessToken = $accessToken');
  }
}
