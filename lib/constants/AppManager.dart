import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'package:agora_rtc_engine/agora_rtc_engine.dart';
import 'package:audio_session/audio_session.dart';
import 'package:dart_ping/dart_ping.dart';
import 'package:echo/import.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:im_flutter_sdk/im_flutter_sdk.dart';
import 'package:aliyun_face_plugin/aliyun_face_plugin.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:sm_fraud/sm_fraud.dart';
import 'package:xinstall_flutter_plugin/xinstall_flutter_plugin.dart';

// 首页活动
class HomeActivity {
  String title = "";
  String linkImg = "";
  String linkUrl = "";
}

// 房间信息
class RoomInfoMode {
  // 是否正在进房间
  bool isJoin = false;
  // 房间数据
  RoomModel? roomMode;
  // 是否开麦
  bool isMico = false;
  // 是否开声音
  bool isSound = false;
  // 是否在麦位上
  bool isMaishang = false;
  // 环信群消息列表
  List<EMMessage> roomMsg = [];
  // 环信世界群消息列表
  List<EMMessage> wordRoomMsg = [];
  // 麦位列表
  List<Mic> seatLists = [];
  // 房间内背景音乐播放列表
  List musicList = [];
  // 房间内背景音乐播放循环模式(0：循环播放，1：随机播放，2：单曲循环)
  int musicMode = 0;
  // 当前播放音乐信息
  var musicInfo;
  // 当前音乐播放暂停(0：未播放，1：正在播放，2：暂停播放，3：播放结束)
  int playMusicMode = 0;
  // 当前音乐列表索引
  int musicIndex = 0;
  // 当前音乐播放进度
  int musicPlaySchedule = 0;
  // 耳返开关
  bool isAuricular = false;
  // 麦克风音量
  int micVolume = 100;
  // 扬声器音量
  int speakerVolume = 100;
  // 声网token
  String agroraToken = "";
  // 财富值限制
  int isWealthRestrict = 0;
  // 盲盒礼物列表
  List magicBoxGiftList = [];
  // 盲盒礼物列表地址
  String magicBoxUrl = "";
  // 礼物列表
  List<GiftInfo> gifts = [];
}

/// app全局状态
class AppManager extends ChangeNotifier {
  AppManager._();
  static final AppManager _instance = AppManager._();
  factory AppManager() => _instance;

  // 首页活动点击
  HomeActivity homeActivity = HomeActivity();

  // 此变量单纯为了储存房间信息不被销毁以控制房间状态
  RoomInfoMode roomInfo = RoomInfoMode();

  // 是否打开特效
  bool openEffect = true;

  // 获取聊天大厅历史消息
  void initWordMsgList() async {
    await DioUtils().httpRequest(
      NetConstants.wordMsgList,
      loading: false,
      hastoast: false,
      params: {
        "page_size": 50,
        "page_index": 1,
      },
      onSuccess: (response) {
        // {"id":1,"birthday":1125648393369,"sex":0,"wealth_grade":40,"charm_grade":5,"user_id":"user1697883695551107072EP4ugb0u7zl",
        // "user_name":"懒惰的小卡拉米8545","content":"好的","head_icon":"head_icon/logo.png","create_time":0}
        if (response == null) return;
        List<EMMessage> msgList = [];

        for (var i = 0; i < response["chat_record"].length; i++) {
          EMMessage message = EMMessage.fromJson({
            "to": AppConstants.worldChannel,
            "from": "admin",
            "body": {
              "type": "txt",
              "content": json.encode(response["chat_record"][i])
            },
            "attributes": {},
            "direction": "rec",
            "hasRead": true,
            "hasReadAck": false,
            "needGroupAck": false,
            "groupAckCount": 0,
            "conversationId": AppConstants.worldChannel,
            "chatType": 2,
            "localTime": DateTime.now().millisecondsSinceEpoch,
            "serverTime": DateTime.now().millisecondsSinceEpoch,
            "status": 2
          });
          msgList.add(message);
        }

        roomInfo.wordRoomMsg.addAll(msgList.reversed);
        EventUtils().emit(EventConstants.E_ROOM_MSG_UPDATE, null);
      },
    );
  }

  // 初始化房间信息
  void initRoom() {
    roomInfo.isMaishang = false;
    roomInfo.isMico = false;
    roomInfo.isSound = true;
    roomInfo.roomMsg.clear();
    roomInfo.seatLists.clear();
    initRoomMusic();
  }

  // 初始化房间音乐
  void initRoomMusic() {
    roomInfo.musicInfo = null;
    roomInfo.musicList = [];
    roomInfo.musicMode = 0;
    roomInfo.playMusicMode = 0;
    roomInfo.musicIndex = 0;
    roomInfo.musicPlaySchedule = 0;
  }

  // 播放音乐节流
  Timer? timer;

  // 环信实例
  final HXManager _hxEngine = HXManager();
  get hxEngine => _hxEngine;
  // 环信是否开启订阅
  bool isHxsubscribe = false;

  // 声网/TRTC实例
  final EngineManager _engineManager = EngineManager();
  get engineManager => _engineManager;

  // mq实例
  final MqEngine _mqEngine = MqEngine();
  MqEngine get mqEngine => _mqEngine;
  // mq定时查询
  Timer? mqTimer;

  // 网络连接
  late Ping ping;
  int time = 0;

  // 监听网络连接
  void initPing() {
    ping = Ping(
      "baidu.com",
      timeout: 5,
    );

    ping.stream.listen((PingData event) {
      int times = event.response?.time != null
          ? event.response!.time!.inMilliseconds
          : const Duration(milliseconds: 0).inMilliseconds;
      time = times > 999 ? 999 : times;
      if (event.error != null) {
        time = 999;
      }
      EventUtils().emit(EventConstants.E_ROOM_WIFI, time);
    });
  }

  // 录音
  FlutterSoundRecorder soundRecorder = FlutterSoundRecorder();
  FlutterSoundPlayer soundPlayer = FlutterSoundPlayer();
  StreamSubscription? recorderStream, playerStream;
  String recordPath = "";
  // 销毁录音数据
  void cancelStream() {
    playerStream?.cancel();
    playerStream = null;
    recorderStream?.cancel();
    recorderStream = null;
  }

  // 初始化录音
  void initAudio() async {
    await soundRecorder.openRecorder();
    //设置订阅计时器
    await soundRecorder
        .setSubscriptionDuration(const Duration(milliseconds: 50));
    //设置音频
    final session = await AudioSession.instance;
    await session.configure(AudioSessionConfiguration(
      avAudioSessionCategory: AVAudioSessionCategory.playAndRecord,
      avAudioSessionCategoryOptions:
          AVAudioSessionCategoryOptions.allowBluetooth |
              AVAudioSessionCategoryOptions.defaultToSpeaker,
      avAudioSessionMode: AVAudioSessionMode.spokenAudio,
      avAudioSessionRouteSharingPolicy:
          AVAudioSessionRouteSharingPolicy.defaultPolicy,
      avAudioSessionSetActiveOptions: AVAudioSessionSetActiveOptions.none,
      androidAudioAttributes: const AndroidAudioAttributes(
        contentType: AndroidAudioContentType.speech,
        flags: AndroidAudioFlags.none,
        usage: AndroidAudioUsage.voiceCommunication,
      ),
      androidAudioFocusGainType: AndroidAudioFocusGainType.gain,
      androidWillPauseWhenDucked: true,
    ));
    await soundPlayer.closePlayer();
    await soundPlayer.openPlayer();
    await soundPlayer.setSubscriptionDuration(const Duration(milliseconds: 50));
    Directory tempDir = await getTemporaryDirectory();
    int timeStamp = DateTime.now().millisecondsSinceEpoch ~/ 1000;
    recordPath =
        '${tempDir.path}/audioRecord-$timeStamp${ext[Platform.isAndroid ? Codec.aacADTS.index : Codec.pcm16WAV.index]}';
  }

  // Xinstall
  late XinstallFlutterPlugin _xinstallFlutterPlugin;
  XinstallFlutterPlugin get xinstallFlutterPlugin => _xinstallFlutterPlugin;

  // 阿里云实名认证
  late AliyunFacePlugin _aliyunFacePlugin = AliyunFacePlugin();
  AliyunFacePlugin get aliyunFacePlugin => _aliyunFacePlugin;

  /// 进入app初始化SDK
  void initAppEngines() {
    // 初始化数美
    _initSmFraud();

    // 初始化环信
    _hxEngine.createEmEngine();

    // 初始化声网
    _engineManager.initRtcEngine();

    // 初始化Xinstall
    _xinstallFlutterPlugin = XinstallFlutterPlugin.getInstance();
    _xinstallFlutterPlugin.init();
    Future.delayed(const Duration(seconds: 0), () async {
      _xinstallFlutterPlugin.reportPoint("open", 1);
    });

    // 初始化实名认证
    _aliyunFacePlugin = AliyunFacePlugin();
    _aliyunFacePlugin.init();

    // 初始化录音插件
    initAudio();

    // 初始化房间数据
    roomInfo.roomMode = null;
    roomInfo.isMaishang = false;
    roomInfo.isMico = false;
  }

  //初始化数美
  Future<void> _initSmFraud() async {
    SmFraud.create(optionDic: {
      SmFraud.OPTION_ORG: "JY6bfIQSmbNHdvvwrovQ",
      SmFraud.OPTION_APPID: "comcodyparty",
      SmFraud.OPTION_PUBLICKEY_ANDROID:
          "MIIDLzCCAhegAwIBAgIBMDANBgkqhkiG9w0BAQUFADAyMQswCQYDVQQGEwJDTjELMAkGA1UECwwCU00xFjAUBgNVBAMMDWUuaXNodW1laS5jb20wHhcNMjMwNzExMDgzMDA4WhcNNDMwNzA2MDgzMDA4WjAyMQswCQYDVQQGEwJDTjELMAkGA1UECwwCU00xFjAUBgNVBAMMDWUuaXNodW1laS5jb20wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCMkDUJe4TtNlb2CbmuvMpufqgKficH25/GyYHC3l0wpACZD2yEQV/nYQdx/W8psv51Efkeprn97Tt/8G+7EuWsWaJiVlcmLNfvQqq74KqKhWREHPhN/mZ/+hhibW8KGPQYaEM0Y1p64P44KJvCLJdKnmLbiOq5vlLpDNplA1uPrV2pCNSOyhpaZeRAl/hg+kiCRhMjFE09rRQBrCZX1sLcENN1Kwwg/mXPqKOKpppRKHAvDr3VNUGezHpEvZ/ttGj6Lb3tenJXv2liBLTY0cN/0kZGoSJ56IUb396dU4250wnMNSqDsq6Un0R1DVbpVvQOkquSdwrsTYVRfDrNMNflAgMBAAGjUDBOMB0GA1UdDgQWBBTcQYyjpWREiSHvQ0rVVCEeD8eZezAfBgNVHSMEGDAWgBTcQYyjpWREiSHvQ0rVVCEeD8eZezAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBBQUAA4IBAQAZ1hNroMNl4AmKAMImVs3QoaWTzwe24ZXBMLh45BDM9G34lVkqRArVnOCmPE+099xk0nuaFnZgB/IubfArw4KaGHS7A8u+ri1Vio484nPm8/lauA360GK28p2BV64c1+n3VnuB196ktB7VlgtJz7pj3dFo9w1K0l8mZSQ3AuXVp0j6R8fhnC/qr7W0hTIuXIL+eUQBGQHhvTN4euD1uoLY1vXK9h3IMMzxK7Zu4ck3WCNrQ02ow0anY/nhiG/yu/PkyR5D5LjA95VZ660CZ1s2cjTsS2/iPEwqQxmLWuogYDxTGW2jORI2cOFJOeqIRwXDXnhqpFiCgvEFgGQghj6q",
      SmFraud.OPTION_PUBLICKEY_IOS:
          "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCkxq/k5hNahGzRgpYJGjZng69C3szrbvKQ+HJA9ihu8Mr+rU1XdxbLbcBBefFQnUc76DsxXNj34q43hAdqQGf415KMlXSLvLKRcVx6cM75DnQXrkLZAUE4aeDjHU9XL/wTWoFvasGYwT6LWY2fCaHUNf5QUdU2cGFfORk72YAIUQIDAQAB",
      SmFraud.OPTION_USE_HTTPS: true,
    });
    // 获取用户设备码
    Future.delayed(const Duration(seconds: 2), () async {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      AppConstants.machineCode =
          preferences.getString(SharedKey.KEY_MACHINE.value) ?? "";
      if (AppConstants.machineCode.isEmpty) {
        String? token = await SmFraud.getDeviceId();
        if (token != null && token.isNotEmpty) {
          AppConstants.machineCode = await getMachineCode(token);
          if (AppConstants.machineCode.isNotEmpty) {
            preferences.setString(
                SharedKey.KEY_MACHINE.value, AppConstants.machineCode);
          }
        }
      }
    });
  }

  Future<String> getMachineCode(String token) async {
    String device = "";
    await DioUtils().httpRequest(NetConstants.getShumeiDevicId,
        method: DioMethod.POST,
        loading: false,
        needMachine: false,
        hastoast: false,
        params: {"device_id": token, "type": 1}, onSuccess: (res) {
      device = res["device_unique_id"] ?? "";
    });
    return device;
  }

  // 登录第三方sdk
  void loginSDK() {
    try {
      _hxEngine.loginEmClient();
      // mq
      connectMq();
    } catch (e) {
      debugPrint("登录第三方sdk失败$e");
    }
  }

  /// 返回登录或释放app释放三方
  void leaveAppEngines() {
    try {
      _hxEngine.loginOutEmClient();
      disconnectMq();
    } catch (e) {
      debugPrint("释放第三方sdk数据失败$e");
    }
  }

  // 返回个人权限
  UserRole getUserRole(int? index) {
    for (UserRole value in UserRole.values) {
      if (value.value == index) return value;
    }
    return UserRole.DEF;
  }

  // 返回用户房间权限
  RoomRole getRoomRole(int? index) {
    for (RoomRole value in RoomRole.values) {
      if (value.value == index) return value;
    }
    return RoomRole.DEF;
  }

  // 获取财富值限制
  void getWealthRestrict() {
    DioUtils().httpRequest(
      NetConstants.config,
      params: {"conf_key": "show_game_push"},
      loading: false,
      onSuccess: (res) {
        if (res == null || res == "") return;
        roomInfo.isWealthRestrict = int.parse(res);
        EventUtils().emit(EventConstants.E_ROOM_WIFI, null);
      },
    );
  }

  // 更新用户财富信息
  void requestDynamicInfo() async {
    await DioUtils().httpRequest(
      NetConstants.gradeUserMoney,
      loading: false,
      method: DioMethod.GET,
      onSuccess: updateInfo,
    );
  }

  void updateInfo(response) {
    if (response == null) return;
    UserConstants.userginInfo?.goldInfo.gold = response['gold'];
    UserConstants.userginInfo?.goldInfo.goldLock = response['gold_lock'];
    UserConstants.userginInfo?.goldInfo.gains = response['gains'];
    UserConstants.userginInfo?.goldInfo.gainsLock = response['gains_lock'];
    UserConstants.userginInfo?.goldInfo.goldable = response['gold_able'];
    UserConstants.userginInfo?.goldInfo.gainsable = response['gains_able'];
    notifyListeners();
  }

  ///--------------------------------------声网/TRTC--------------------------------------///
  // 获取声网token
  Future<String> enToken() async {
    String engineToken = "";
    await DioUtils().httpcodeRequest(
      NetConstants.enToken,
      loading: false,
      method: DioMethod.POST,
      isSuccess: true,
      params: {
        "channelName": roomInfo.roomMode!.roomId.toString(),
        "uid": UserConstants.userginInfo!.info.agoraUid.toString(),
        "tokenExpireTs": 86400,
        "privilegeExpireTs": 86400,
        "serviceRtc": {"enable": true, "role": 1},
        "serviceRtm": {"enable": true}
      },
      onSuccess: (data, code, msg) {
        if (data == null || data["token"] == null || data["token"] == "") {
          WidgetUtils().showToast("网络异常，已退出房间");
          quiteRoom(hastoast: false);
          WidgetUtils().popPage();
        } else {
          engineToken = data["token"];
        }
      },
      onError: (code, msg) {
        WidgetUtils().showToast("网络异常，已退出房间");
        quiteRoom(hastoast: false);
        WidgetUtils().popPage();
      },
    );
    return engineToken;
  }

  // 音频流识别开启
  void voiceIdentifyOpen(String token, {int isRefresh = 0}) {
    DioUtils().asyncHttpRequest(
      NetConstants.voiceIdentifyOpen,
      method: DioMethod.POST,
      loading: false,
      hastoast: false,
      params: {
        "stream_type": "AGORA",
        "event_id": "voiceroom",
        "user_id": UserConstants.userginInfo!.info.userId,
        "room_id": roomInfo.roomMode!.exteriorRoomId,
        "role": "USER",
        "channel": roomInfo.roomMode!.roomId.toString(),
        "token": token,
        "uid": UserConstants.userginInfo!.info.agoraUid,
        "token_refresh": isRefresh,
      },
    );
  }

  // 加入声网/TRTC房间
  void joinEngineManager() async {
    String token = await enToken();
    roomInfo.agroraToken = token;
    if (token.isEmpty) return;
    _engineManager.joinRtcEngine(
      UserConstants.userginInfo!.info.agoraUid!,
      roomInfo.roomMode!.roomId.toString(),
      token,
    );
    _engineManager.statusMonitoring();
  }

  // 退出声网/TRTC房间
  void leaveEngineManager() {
    _engineManager.leaveRtcEngine();
    microphone(false);
    _engineManager.unregisterEventHandler();
  }

  // 设置用户权限
  void setClientRole(ClientRoleType role) {
    _engineManager.setClientRole(role);
  }

  // 开麦/关麦
  Future<bool> microphone(bool enabled) async {
    if (enabled) {
      bool accept = await FuncUtils().requestPermission(Permission.microphone);
      if (!accept) return false;
      voiceIdentifyOpen(roomInfo.agroraToken);
    }
    roomInfo.isMico = enabled;
    _engineManager.enableLocalAudio(enabled);
    return true;
  }

  // 播放/随机播放/暂停播放背景音乐
  void roomAudioMixing({var id}) {
    if (roomInfo.roomMode!.isBanMic == 1) {
      return WidgetUtils().showToast("您已被禁麦!");
    }
    if (roomInfo.playMusicMode == 1 &&
        roomInfo.musicInfo != null &&
        (id == roomInfo.musicInfo["id"] || id == null)) {
      _engineManager.audioMixing(stop: true);
    } else if (roomInfo.playMusicMode == 2 &&
        roomInfo.musicInfo != null &&
        (id == roomInfo.musicInfo["id"] || id == null)) {
      _engineManager.resumeAudioMixing();
    } else if (id != null) {
      late int index;
      for (var i = 0; i < roomInfo.musicList.length; i++) {
        if (id == roomInfo.musicList[i]["songId"]) {
          index = i;
          continue;
        }
      }
      onPalyMusic(index);
    } else {
      if (roomInfo.musicMode == 1) {
        int index = Random().nextInt(roomInfo.musicList.length);
        onPalyMusic(index);
      } else {
        _engineManager.audioMixing(filePath: roomInfo.musicInfo["playUrl"]);
      }
    }
  }

  // 获取播放音乐
  void onPalyMusic(int index) async {
    var data = roomInfo.musicList[index];
    await DioUtils().httpRequest(
      NetConstants.musicInfo,
      method: DioMethod.POST,
      loading: false,
      params: {
        "ids": [data["songId"]]
      },
      onSuccess: (res) {
        if (res == null) return;
        timer = Timer(const Duration(seconds: 10), () {
          timer = null;
        });
        debugPrint("音乐详情$res");
        _engineManager.audioMixing(filePath: res["playUrl"]);
        roomInfo.musicInfo = res;
        roomInfo.musicIndex = index;
      },
    );
  }

  // 切换播放模式
  void switchMusicMode(int index) {
    roomInfo.musicMode = index;
    EventUtils().emit(EventConstants.E_ROOM_MUSIC, null);
  }

  // 上/下一曲
  void upDownMusic(bool isUp) {
    late int index;
    for (var i = 0; i < roomInfo.musicList.length; i++) {
      if (roomInfo.musicInfo["id"] == roomInfo.musicList[i]["songId"]) {
        index = i;
        continue;
      }
    }
    if (index == 0 && isUp) {
      return WidgetUtils().showToast("暂无上一曲");
    }
    isUp ? index -= 1 : index += 1;
    onPalyMusic(index);
  }

  // 获取音乐音量
  Future<int> getVolume() async {
    return await _engineManager.getAudioMixingPlayoutVolume();
  }

  // 设置音乐音量
  void set5MusicVolume(int pitch) {
    _engineManager.setAudioMixingPitch(pitch);
  }

  // 设置耳返功能
  void setAuricularReturn(bool isOpen) {
    roomInfo.isMaishang = isOpen;
    _engineManager.enableInEarMonitoring(isOpen);
  }

  // 设置麦克风音量
  void setAudioCaptureVolume(int volume) {
    roomInfo.micVolume = volume;
    _engineManager.setAudioCaptureVolume(volume);
  }

  // 设置扬声器音量
  void setAudioPlayoutVolume(int volume) {
    roomInfo.speakerVolume = volume;
    _engineManager.setAudioPlayoutVolume(volume);
  }

  // 设置混响模式
  void setVoiceReverbType(int type) {
    _engineManager.setVoiceReverbType(type);
  }

  // 结束背景音乐
  void enMusic() async {
    bool? accept = await WidgetUtils().showAlert("确定要结束播放背景音乐吗?");
    if (accept != true) return;
    _engineManager.audioMixing(stop: true);
    initRoomMusic();
    EventUtils().emit(EventConstants.E_ROOM_MUSIC, null);
  }

  // 开关声音
  void switchSound(bool pull) {
    roomInfo.isSound = pull;
    _engineManager.pullLocalAudio(pull);
  }

  ///--------------------------------------mq--------------------------------------///
  // 创建mq
  void connectMq() {
    _mqEngine.createMqEngine();
    Future.delayed(const Duration(seconds: 1), () async {
      _mqEngine.connectMq();
    });
  }

  // 销毁mq
  void disconnectMq() {
    _mqEngine.disConnectMq();
  }

  // 定时查询是否断开连接
  void scheduledQuery(bool isOpen) {
    if (isOpen) {
      mqTimer = Timer.periodic(const Duration(seconds: 1), (Timer timer) {
        if (!_mqEngine.isConnected) {
          _mqEngine.connectMq();
          onReconnectRoom();
        }
      });
    } else {
      mqTimer?.cancel();
      mqTimer = null;
    }
  }

  // 加入系统mq
  void joinSysMq() {
    _mqEngine.subscribeTopic(["message_notice_topic"]);
    _mqEngine.subscribeTopic(["message_official_topic"]);
    EventUtils()
        .on(EventConstants.E_SYS_MESSAGE, EventCall("AppManager", _sysListMq));
  }

  // 退出系统mq
  void leaveSysMq() {
    _mqEngine.unSubscribeTopic(["message_notice_topic"]);
    _mqEngine.unSubscribeTopic(["message_official_topic"]);
    EventUtils()
        .off(EventConstants.E_SYS_MESSAGE, EventCall("AppManager", _sysListMq));
  }

  // 系统mq
  void _sysListMq(data) {
    if (data == null) {
      return;
    }

    int code = data["type"] ?? 0;
    SysMqMsgType msgType = getSysMqMsgType(code);

    switch (msgType) {
      case SysMqMsgType.SYS:
        if (data["info"]["user_display_id"] != null &&
            data["info"]["user_display_id"]
                    .indexOf(UserConstants.userginInfo!.info.displayId) !=
                -1) {
          sysPageIndex = 1;
          sysMessageList();
        }
        break;
      case SysMqMsgType.ADMIN:
        adminPageIndex = 1;
        mqAdminList(data["info"]["operate"] != 2);
        break;
      case SysMqMsgType.ACTIVITY:
        break;
      case SysMqMsgType.INTERACTION:
        break;
      default:
    }
  }

  // 加入房间mq
  void joinRoomMq() async {
     _mqEngine.subscribeTopic([
       "roomInsideMsg/${roomInfo.roomMode!.roomId}",
      "AppGiftSendGlobalMsg/${roomInfo.roomMode!.roomId}",
       "roomGlobalMsg"
     ]);
    scheduledQuery(true);
    EventUtils().emit(EventConstants.E_ROOM_MUSIC, null);
    EventUtils().on(
      EventConstants.E_MQ_MESSAGE,
      EventCall("AppManager", _onMqListener),
    );
    EventUtils().on(
      EventConstants.E_MQ_RECONNECT,
      EventCall("AppManager", (v) {
        initSeatList(roomInfo.roomMode!.exteriorRoomId);
        onReconnectRoom();
      }),
    );
  }

  // 离开房间mq
  Future<void> leaveRoomMq() async {
     _mqEngine.unSubscribeTopic([
       "roomInsideMsg/${roomInfo.roomMode!.roomId}",
       "AppGiftSendGlobalMsg/${roomInfo.roomMode!.roomId}",
       "roomGlobalMsg"
     ]);
    scheduledQuery(false);
    EventUtils().off(
        EventConstants.E_MQ_MESSAGE, EventCall("AppManager", _onMqListener));
    EventUtils().off(
      EventConstants.E_MQ_RECONNECT,
      EventCall("AppManager", (v) {}),
    );
  }

  // 重连房间
  Future<void> onReconnectRoom() async {
    await DioUtils().httpRequest(
      NetConstants.reconnectRoom,
      loading: false,
      method: DioMethod.POST,
      params: {
        "exterior_room_id": roomInfo.roomMode!.exteriorRoomId,
      },
      onSuccess: (res) async {
        if (res == null) return;
        roomInfo.roomMode = RoomModel.fromJson(res);
        leaveHx();
        joinHx();
      },
    );
  }

  // 预加载房间数据
  void joinRoom(int roomId, {String? passwords}) async {
    if (roomInfo.isJoin) return;
    roomInfo.isJoin = true;
    if (roomId == roomInfo.roomMode?.exteriorRoomId) {
      roomInfo.isJoin = false;
      return WidgetUtils().pushPage(RoomPage(
          roomId, roomInfo.roomMode!, roomInfo.seatLists,
          isNew: false));
    }
    initRoom();
    await DioUtils().httpRequest(
      NetConstants.joinRoom,
      method: DioMethod.POST,
      params: {"exterior_room_id": roomId, "room_password": passwords},
      onSuccess: (res) async {
        try {
          leaveRoomMq();
          leaveEngineManager();
          // ignore: empty_catches
        } catch (e) {
          debugPrint("调用切房间清除mq和声网失败$e");
        }

        if (res == null) {
          WidgetUtils().showToast("进入房间失败，请重新尝试");
          return WidgetUtils().popPage();
        }

        roomInfo.roomMode = RoomModel.fromJson(res);
        joinRoomMq();

        if (!await initSeatList(roomId)) return WidgetUtils().popPage();
        WidgetUtils()
            .pushPage(RoomPage(roomId, roomInfo.roomMode!, roomInfo.seatLists));
        roomInfo.isJoin = false;
      },
      onError: (code, msg) async {
        if (code == 102006) {
          roomInfo.isJoin = false;
          String? inputpassword =
              await WidgetUtils().showFDialog(const RoomPassWord());
          if (inputpassword is String) {
            joinRoom(roomId, passwords: inputpassword);
          } else {
            try {
              leaveRoomMq();
              leaveEngineManager();
              // ignore: empty_catches
            } catch (e) {
              debugPrint("调用切房间清除mq和声网失败$e");
            }
            initRoom();
            roomInfo.isJoin = false;
          }
        } else {
          try {
            leaveRoomMq();
            leaveEngineManager();
            // ignore: empty_catches
          } catch (e) {
            debugPrint("调用切房间清除mq和声网失败$e");
          }
          initRoom();
          roomInfo.isJoin = false;
        }
      },
    );
  }

  // 退出房间
  void quiteRoom({bool hastoast = true}) async {
    await DioUtils().httpRequest(
      NetConstants.quiteRoom,
      loading: false,
      hastoast: hastoast,
      onSuccess: (data) async {
        EventUtils().emit(EventConstants.E_MQ_MESSAGE, [
          "",
          {
            "event": MqMsgType.SELF_JOIN.value,
            "info": {
              "type": 3,
              "displayId": UserConstants.userginInfo!.info.displayId
            }
          }
        ]);
        leaveHx();
        leaveRoomMq();
        leaveEngineManager();
        SharedPreferences preferences = await SharedPreferences.getInstance();
        preferences.setInt(SharedKey.KEY_ROOM_ID.value, 0);
        roomInfo.roomMode = null;
        initRoom();
        if (hastoast) {
          WidgetUtils().popPage();
          WidgetUtils().popPage();
        }
      },
    );
  }

  // 初始化麦位列表
  Future<bool> initSeatList(int roomId) async {
    bool isMic = false;
    await DioUtils().httpRequest(
      NetConstants.seatList,
      loading: false,
      params: {"exterior_room_id": roomId},
      onSuccess: (res) {
        isMic = true;
        List<Mic> list = RoomSeat.fromJson(res).mic;
        List<Mic> list2 = [];
        bool isMyBanMic = false;
        bool isAnchor = false;
        for (var i = 1; i <= list.length; i++) {
          if (list[i - 1].micUserInfo.userId ==
              UserConstants.userginInfo!.info.userId) {
            if (list[i - 1].roomMicBan == 2) {
              isMyBanMic = true;
            }
            isAnchor = true;
          }
          for (var j = 0; j < list.length; j++) {
            if (list[j].micNum == i) {
              list2.add(list[j]);
              break;
            }
          }
        }
        roomInfo.seatLists = list2;
        EventUtils().emit(EventConstants.E_ROOM_SEAT, null);
        if (!isAnchor) {
          microphone(false);
          _engineManager.audioMixing(stop: true);
          initRoomMusic();
          EventUtils().emit(EventConstants.E_ROOM_MUSIC, null);
        }
        if (isMyBanMic) {
          if (roomInfo.roomMode!.isBanMic == 2) {
            roomInfo.isMico = false;
            _engineManager.enableLocalAudio(false);
            roomInfo.roomMode!.isBanMic = 1;
            WidgetUtils().showToast("该麦位已被禁麦");
          }
        } else {
          roomInfo.roomMode!.isBanMic = 2;
        }
        roomInfo.isMaishang = isAnchor;
        EventUtils().emit(EventConstants.E_MIC_OPEN, null);
      },
    );
    return isMic;
  }

  // 房间基本信息修改（此处为房间全部逻辑根本，切勿随意更改）
  void _onMqListener(data) async {
    if (data == null || data[1] == null) return;

    int code = data[1]["event"] ?? 0;
    MqMsgType msgType = getMqMsgType(code);

    if (msgType == MqMsgType.BAN_MICRO_CHAT) {
      int userId = data[1]["info"]["ex_user_id"] ?? 0;
      bool open = data[1]["info"]["func_int"] == 1 ? true : false;
      if (userId == UserConstants.userginInfo!.info.displayId) {
        // 禁言
        if (open) {
          roomInfo.roomMode!.isBanChat = 1;
          WidgetUtils().showToast("您已被管理员禁言!");
        } else {
          roomInfo.roomMode!.isBanChat = 2;
        }
      }
    } else if (msgType == MqMsgType.ROOM_SET_CONF) {
      int type = data[1]["info"]["type"];
      int open = data[1]["info"]["func_int"];
      String newRoomInfo = data[1]["info"]["func_string"];
      switch (type) {
        case 1:
          roomInfo.roomMode!.roomIsChat = (open == 1 ? 2 : 1);
          EventUtils()
              .emit(EventConstants.E_ROOM_CHAT_OPEN, open == 1 ? true : false);
          break;
        case 2:
          roomInfo.roomMode!.isMicApply = (open == 1 ? 2 : 1);
          EventUtils()
              .emit(EventConstants.E_MIC_APPLY_OPEN, open == 1 ? true : false);
          break;
        case 3:
          roomInfo.roomMode!.bgImg = newRoomInfo;
          EventUtils().emit(EventConstants.E_ROOM_BGIMG, newRoomInfo);
          break;
        case 4:
          roomInfo.roomMode!.roomBulletin = newRoomInfo;
          break;
        case 5:
          roomInfo.roomMode!.headIcon = newRoomInfo;
          break;
        case 6:
          roomInfo.roomMode!.roomName = newRoomInfo;
          break;
        case 7:
          // 心动值开关
          roomInfo.roomMode!.isHeart = open == 1 ? 2 : 1;
          initSeatList(roomInfo.roomMode!.exteriorRoomId);
          break;
        case 8:
          roomInfo.roomMode!.exteriorRoomId = int.parse(newRoomInfo);
          break;
      }
      EventUtils().emit(EventConstants.E_ROOM_INFO_UPDATE, null);
    } else if (msgType == MqMsgType.HEAT_NUMPEOPLE) {
      roomInfo.roomMode!.roomHot = data[1]["info"]["room_hot"];
      roomInfo.roomMode!.peopleNum = data[1]["info"]["people_num"];
      EventUtils().emit(EventConstants.E_ROOM_PEP_CHART, null);
    } else if (msgType == MqMsgType.SEAT_UP_DOWN) {
      if (data[1]["info"]["mic_user_info"]["ex_user_id"] ==
          UserConstants.userginInfo!.info.displayId) {
        roomInfo.isMaishang = (data[1]["info"]["func_int"] == 1);
        if (data[1]["info"]["func_int"] == 2) {
          roomInfo.isMico = false;
          _engineManager.audioMixing(stop: true);
          _engineManager.enableLocalAudio(false);
          initRoomMusic();
          EventUtils().emit(EventConstants.E_ROOM_MUSIC, null);
        } else {
          if (data[1]["info"]["room_mic_ban"] == 2) {
            roomInfo.isMico = false;
            _engineManager.enableLocalAudio(false);
            roomInfo.roomMode!.isBanMic = 1;
            WidgetUtils().showToast("该麦位已被禁麦");
          } else {
            roomInfo.roomMode!.isBanMic = 2;
          }
        }
        EventUtils().emit(EventConstants.E_MIC_OPEN, null);
      }
      bool isUp = data[1]["info"]["func_int"] == 1;
      int micNum = data[1]["info"]["mic_num"];
      List<Mic> list = roomInfo.seatLists;
      for (int i = 0; i < list.length; i++) {
        if (list[i].micNum == micNum) {
          if (isUp) {
            list[i].micUserInfo =
                MicUserInfo.fromJson(data[1]["info"]["mic_user_info"]);
          } else {
            list[i].micUserInfo = const MicUserInfo(
              exUserId: 0,
              userId: "",
              sex: 0,
              userName: "",
              userImage: "",
              personalPermission: 0,
              agoraUid: 0,
              goods: null,
              isBanChat: 0,
            );
          }
          if (UserConstants.userginInfo!.info.userId ==
              list[i].micUserInfo.userId) {
            if (isUp) {
              roomInfo.isMaishang = true;
            } else {
              roomInfo.isMaishang = false;
            }
            EventUtils().emit(EventConstants.E_MIC_OPEN, null);
          }
          break;
        }
      }
      roomInfo.seatLists = list;
      EventUtils().emit(EventConstants.E_ROOM_SEAT, null);
    } else if (msgType == MqMsgType.CLEAN_SEAT_CHARM) {
      int heart = data[1]["info"]["heart_num"];
      int micNum = data[1]["info"]["mic_num"];
      List<Mic> list = roomInfo.seatLists;
      for (int i = 0; i < list.length; i++) {
        if (list[i].micNum == micNum) {
          list[i].heart = heart;
        }
      }
      roomInfo.seatLists = list;
      EventUtils().emit(EventConstants.E_ROOM_SEAT, null);
    } else if (msgType == MqMsgType.USER_ROOM_ACCESS ||
        msgType == MqMsgType.SELF_JOIN) {
      int type = data[1]["info"]["type"];
      int userId = data[1]["info"]["displayId"];
      if (type == 1) {
        if (userId == UserConstants.userginInfo!.info.displayId &&
            msgType == MqMsgType.USER_ROOM_ACCESS) return;
        _insertMsgToSql(data[1]);
        EventUtils().emit(EventConstants.E_ROOM_JOIN, data[1]["info"]);
      } else if (type == 2) {
        if (userId == UserConstants.userginInfo!.info.displayId) {
          EventUtils().emit(EventConstants.E_MQ_MESSAGE, [
            "",
            {
              "event": MqMsgType.SELF_JOIN.value,
              "info": {
                "type": 3,
                "displayId": UserConstants.userginInfo!.info.displayId
              }
            }
          ]);
          leaveRoomMq();
          leaveEngineManager();
          roomInfo.roomMode = null;
          initRoom();
          SharedPreferences preferences = await SharedPreferences.getInstance();
          preferences.setInt(SharedKey.KEY_ROOM_ID.value, 0);
          EventUtils().emit(EventConstants.E_ROOM_MY_OUT, null);
          WidgetUtils().showToast("您已被踢出房间");
        }
      }
    } else if (msgType == MqMsgType.ROOM_GIFT) {
      GiftMq giftMqInfo = GiftMq.fromJson(data[1]["info"]);
      if (giftMqInfo.obtainGiftInfo.giftLevel > 4) {
        EventUtils().emit(EventConstants.E_ROOM_GIFT_TOP, data[1]);
      }
      if (giftMqInfo.roomId != roomInfo.roomMode!.roomId) return;
      EventUtils().emit(EventConstants.E_ROOM_GIFT, giftMqInfo);
      _insertMsgToSql(data[1]);
      EventUtils().emit(EventConstants.E_ROOM_MSG_UPDATE, null);
    } else if (msgType == MqMsgType.ROOM_GAME_GIFT) {
      if ((UserConstants
                  .userginInfo!.gradeInfo.userGrade.wealth.nowExperience ??
              0) <
          roomInfo.isWealthRestrict) return;
      GameInfo gameMqInfo = GameInfo.fromJson(data[1]["info"]);
      if (gameMqInfo.giftList == null) return;
      _insertMsgToSql(data[1]);
      EventUtils().emit(EventConstants.E_ROOM_MSG_UPDATE, null);
    } else if (msgType == MqMsgType.GAME_GIFT) {
      GameInfo gameMqInfo = GameInfo.fromJson(data[1]["info"]);
      if (gameMqInfo.giftInfo == null || gameMqInfo.topPicture == null) return;
      EventUtils().emit(EventConstants.E_ROOM_GIFT_TOP, data[1]);
    } else if (msgType == MqMsgType.LOCK_ONLY_SEAT) {
      int roomMicLock = data[1]["info"]["func_int"];
      int micNum = data[1]["info"]["mic_num"];
      List<Mic> list = roomInfo.seatLists;
      for (int i = 0; i < list.length; i++) {
        if (list[i].micNum == micNum) {
          list[i].roomMicLock = roomMicLock == 1 ? 2 : 1;
        }
      }
      roomInfo.seatLists = list;
      EventUtils().emit(EventConstants.E_ROOM_SEAT, null);
    } else if (msgType == MqMsgType.MIC_BAN) {
      int roomMicBan = data[1]["info"]["func_int"];
      int micNum = data[1]["info"]["mic_num"];
      List<Mic> list = roomInfo.seatLists;
      for (int i = 0; i < list.length; i++) {
        if (list[i].micNum == micNum) {
          list[i].roomMicBan = roomMicBan == 1 ? 2 : 1;
          if (list[i].micUserInfo.userId ==
              UserConstants.userginInfo!.info.userId) {
            if (roomMicBan == 1) {
              roomInfo.isMico = false;
              roomInfo.roomMode!.isBanMic = 1;
              _engineManager.enableLocalAudio(false);
              _engineManager.audioMixing(stop: true);
              WidgetUtils().showToast("当前麦位已被禁麦!");
            } else {
              roomInfo.roomMode!.isBanMic = 2;
            }
            EventUtils().emit(EventConstants.E_MIC_OPEN, null);
          }
        }
      }
      roomInfo.seatLists = list;
      EventUtils().emit(EventConstants.E_ROOM_SEAT, null);
    }
  }

  // 插入环信消息
  void _insertMsgToSql(data) {
    EMMessage message = EMMessage.fromJson({
      "to": roomInfo.roomMode!.hxId,
      "from": "admin",
      "body": {"type": "txt", "content": json.encode(data)},
      "attributes": {},
      "direction": "rec",
      "hasRead": true,
      "hasReadAck": false,
      "needGroupAck": false,
      "groupAckCount": 0,
      "conversationId": roomInfo.roomMode!.hxId,
      "chatType": 2,
      "localTime": DateTime.now().millisecondsSinceEpoch,
      "serverTime": DateTime.now().millisecondsSinceEpoch,
      "status": 2
    });
    if (roomInfo.roomMsg.length >= 300) {
      roomInfo.roomMsg.removeAt(0);
    }
    roomInfo.roomMsg.add(message);
    EventUtils().emit(EventConstants.E_ROOM_MSG_UPDATE, null);
  }

  // 房间mq类型
  MqMsgType getMqMsgType(int code) {
    for (MqMsgType type in MqMsgType.values) {
      if (type.value == code) return type;
    }
    return MqMsgType.NULL;
  }

  // 系统mq类型
  SysMqMsgType getSysMqMsgType(int code) {
    for (SysMqMsgType type in SysMqMsgType.values) {
      if (type.value == code) return type;
    }
    return SysMqMsgType.NULL;
  }

  ///--------------------------------------环信--------------------------------------///
  // 登录环信
  void loginEmClient() async {
    await _hxEngine.loginEmClient();
  }

  // 退出登录环信
  void loginOutEmClient() {
    _hxEngine.loginOutEmClient();
  }

  // 加入退出环信世界房间
  void hxWordRoom(bool isJoin) {
    DioUtils().asyncHttpRequest(
      NetConstants.hxWordRoom,
      method: DioMethod.POST,
      loading: false,
      params: {
        "operate_type": isJoin ? 1 : 2,
      },
    );
  }

  // 加入环信消息监听
  void joinHx() {
    isHxsubscribe = true;
    EMClient.getInstance.chatManager.addEventHandler('RoomChatGroup',
        EMChatEventHandler(onMessagesReceived: _onMessagesReceived));
  }

  void _onMessagesReceived(List<EMMessage> messages) {
    debugPrint(
        "| ----------收到环信消息---------- |${messages[0].toJson()}, ${messages[0].chatType}");
    if (messages[0].chatType == ChatType.Chat) {
      debugPrint("| ----------收到环信私信消息---------- |${messages[0].toJson()}");
      if (UserConstants.openAudio) {
        HapticFeedback.vibrate();
      }
      if (UserConstants.openShock) {
        HapticFeedback.mediumImpact();
      }
      refreshUnread();
    } else if (messages[0].chatType == ChatType.ChatRoom) {
      debugPrint(
          "| ----------收到环信房间消息---------- |${messages[0].to}, ${AppConstants.worldChannel} ${messages[0].to == AppConstants.worldChannel.toString()}");
      if (messages[0].to == AppConstants.worldChannel.toString()) {
        if (roomInfo.wordRoomMsg.length >= 100) {
          roomInfo.wordRoomMsg.removeAt(0);
        }
        roomInfo.wordRoomMsg.add(messages[0]);
      } else if (messages[0].to == roomInfo.roomMode?.hxId) {
        if (roomInfo.roomMsg.length >= 300) {
          roomInfo.roomMsg.removeAt(0);
        }
        roomInfo.roomMsg.add(messages[0]);
      }
      Future.delayed(const Duration(seconds: 0), () async {
        EMTextMessageBody msg = messages[0].body as EMTextMessageBody;
        var content = json.decode(msg.content);
        int code = content?["event"] ?? 0;
        MqMsgType msgType = getMqMsgType(code);
        if (msgType == MqMsgType.ROOM_SEND_EMO) {
          EventUtils().emit(EventConstants.E_ROOM_EMO,
              [content["info"]["id"], content["content"]]);
        }
      });
      EventUtils().emit(EventConstants.E_ROOM_MSG_UPDATE, null);
    }
  }

  // 离开环信消息监听
  void leaveHx() {
    isHxsubscribe = false;
    EMClient.getInstance.chatManager.removeEventHandler('RoomChatGroup');
  }

  // 处理环信群消息
  Future<EMMessage?> sendRoomMsg(String msgText, {bool isWord = false}) async {
    bool lawful =
        await FuncUtils().checkTextLawful(msgText, CheckTxtType.groupChat);
    if (!lawful) return null;

    SharedPreferences preferences = await SharedPreferences.getInstance();
    int roomId = preferences.getInt(SharedKey.KEY_ROOM_ID.value) ?? 0;
    int roomRole = roomInfo.roomMode?.personalPermission ?? 4;
    String content = json.encode({
      "content": msgText,
      "event": MqMsgType.ROOM_SEND_CHAT.value,
      "info": UserConstants.getExtUser()?.toJson(),
      "roomRole": roomRole,
      "hxinfo": UserConstants.userginInfo?.info.hxInfo.toJson(),
      "roomId": roomId.toString(),
    });

    late EMMessage message;
    if (isWord) {
      if (!await canSendChat(msgText)) return null;
      message =
          await _hxEngine.sendHxRoomMsg(AppConstants.worldChannel, content);
      roomInfo.wordRoomMsg.add(message);
    } else {
      message = await _hxEngine.sendHxRoomMsg(
          roomInfo.roomMode!.hxId.toString(), content);
      roomInfo.roomMsg.add(message);
    }
    EventUtils().emit(EventConstants.E_ROOM_MSG_UPDATE, null);
    return message;
  }

  // 聊天大厅消息记录
  Future<bool> canSendChat(String text) async {
    bool isOpen = false;
    await DioUtils().httpRequest(
      NetConstants.sendWordMsg,
      method: DioMethod.POST,
      loading: false,
      params: {
        "birthday": UserConstants.userginInfo!.info.birthday,
        "sex": UserConstants.userginInfo!.info.sex,
        "wealth_grade":
            UserConstants.userginInfo!.gradeInfo.userGrade.wealth.grade,
        "charm_grade":
            UserConstants.userginInfo!.gradeInfo.userGrade.charm.grade,
        "user_id": UserConstants.userginInfo!.info.userId,
        "user_name": UserConstants.userginInfo!.info.name,
        "content": text,
        "head_icon": UserConstants.userginInfo!.info.headIcon,
        "content_type": 1,
      },
      onSuccess: (data) => isOpen = true,
    );
    return isOpen;
  }

  // 上麦/抱上麦
  void upWheat(int micNum,
      {int? id, dynamic Function(dynamic)? onSuccess}) async {
    id ??= UserConstants.userginInfo!.info.displayId;

    await DioUtils().httpRequest(
      NetConstants.upWheat,
      loading: false,
      method: DioMethod.POST,
      params: {
        "room_mic_num": micNum,
        "exterior_user_id": id,
        "exterior_room_id": roomInfo.roomMode!.exteriorRoomId
      },
      onSuccess: (res) {
        roomInfo.isMaishang = true;
        onSuccess != null ? onSuccess(res) : null;
      },
    );
  }

  // 下麦/抱下麦
  void downWheat({int? id, dynamic Function(dynamic)? onSuccess}) async {
    id ??= UserConstants.userginInfo!.info.displayId;

    await DioUtils().httpRequest(
      NetConstants.downWheat,
      loading: false,
      method: DioMethod.POST,
      params: {
        "exterior_user_id": id,
        "exterior_room_id": roomInfo.roomMode!.exteriorRoomId
      },
      onSuccess: (res) {
        roomInfo.isMaishang = false;
        onSuccess != null ? onSuccess(res) : null;
      },
    );
  }

  ///--------------------------------------------------私信----------------------------------------------///
  // 未读消息数量
  int unread = 0;

  // 私信消息列表
  List<EMConversation> conList = [];

  // 未读消息通知
  void updateUnreadCount(int count, List<EMConversation> lists) {
    unread = count;
    conList = lists;
    EventUtils().emit(EventConstants.E_MSG_UPDATE, null);
  }

  // 环信私信列表
  Future<void> refreshUnread() async {
    try {
      int count = 0;
      List<EMConversation> lists = [];
      List<EMConversation> lists2 = [];
      lists = await EMClient.getInstance.chatManager.loadAllConversations();

      for (var e in lists) {
        EMMessage? data = await e.latestMessage();
        if (data == null ||
            data.from == 'admin' ||
            data.attributes == null ||
            data.chatType != ChatType.Chat) continue;
        lists2.add(e);
      }

      for (EMConversation con in lists) {
        if (con.type != EMConversationType.Chat || con.id == 'admin') continue;
        count += await con.unreadCount();
      }

      debugPrint('未读消息数量$count');
      debugPrint('当前用户聊天人数${lists.length} ${lists2.length}');
      updateUnreadCount(count, lists2);
    } on EMError catch (e) {
      debugPrint('获取会话列表失败: $e');
    }
  }

  // 清除所有已读消息
  void markAllConversation() async {
    await EMClient.getInstance.chatManager.markAllConversationsAsRead();
    refreshUnread();
  }

  // 删除某个会话
  void deleteConversation(EMConversation con) async {
    bool? accept = await WidgetUtils().showAlert("确定删除与该用户的聊天记录吗？");
    if (accept == null || !accept) return;
    await EMClient.getInstance.chatManager.deleteConversation(con.id);
    refreshUnread();
  }

  // 删除会话中的消息
  void cleanConMsg(EMConversation con) async {
    bool? accept = await WidgetUtils().showAlert("确定清空聊天记录吗？");
    if (accept == null || !accept) return;
    try {
      await con.deleteAllMessages();
      WidgetUtils().showToast("清空聊天记录成功");
      WidgetUtils().popPage();
      WidgetUtils().popPage();
    } on EMError catch (e) {
      debugPrint("环信删除会话中的消息失败$e");
    }
  }

  // 用户是否被拉黑
  Future<bool> blockList(String userId) async {
    try {
      List<String> list =
          await EMClient.getInstance.contactManager.getBlockListFromServer();
      for (var i = 0; i < list.length; i++) {
        debugPrint("环信拉黑列表${list[i]}, $userId");
        if (list[i] == userId) {
          return true;
        }
      }
      return false;
    } on EMError catch (e) {
      debugPrint("环信获取拉黑列表失败$e");
      return false;
    }
  }

  // 拉黑用户
  void userToBlock(String userId) async {
    bool? accept = await WidgetUtils().showAlert("确定拉黑该用户吗？");
    if (accept == null || !accept) return;
    try {
      await EMClient.getInstance.contactManager.addUserToBlockList(userId);
      WidgetUtils().showToast("拉黑用户成功");
    } on EMError catch (e) {
      debugPrint("环信拉黑用户失败$e");
    }
  }

  // 取消拉黑
  void userFromBlock(String userId) async {
    bool? accept = await WidgetUtils().showAlert("用户已被拉黑，是否将该用户移出黑名单");
    if (accept == null || !accept) return;
    try {
      await EMClient.getInstance.contactManager.removeUserFromBlockList(userId);
      WidgetUtils().showToast("已将用户移出黑名单");
    } on EMError catch (e) {
      debugPrint("环信取消拉黑用户失败$e");
    }
  }

  ///--------------------------------------------------系统/官方/活动/互动----------------------------------------------///
  // 系统通知未读消息数
  int sysunread = 0;
  // 系统通知总条数
  int sysListNum = 0;
  // 系统通知
  List sysMsgList = [];
  // 页码
  int sysPageIndex = 1;

  // 官方消息未读消息数
  int adminunread = 0;
  // 官方消息总条数
  int adminListNum = 0;
  // 官方消息
  List adminMsgList = [];
  // 页码
  int adminPageIndex = 1;

  // 活动公告未读消息数
  int activityunread = 0;
  // 官方消息总条数
  int activityListNum = 0;
  // 官方消息
  List activityMsgList = [];
  // 页码
  int activityPageIndex = 1;

  // 初始化
  void initSys() {
    sysMessageList();
    adminMessageList();
  }

  // 消息已读
  void messageRead(int type) {
    DioUtils().asyncHttpRequest(
      NetConstants.messageRead,
      method: DioMethod.POST,
      loading: false,
      hastoast: false,
      params: {
        "message_type": type,
      },
    );
  }

  // 系统通知
  Future<List> sysMessageList({bool isInit = true, bool isLoad = false}) async {
    await DioUtils().httpRequest(
      NetConstants.systemNotifications,
      method: DioMethod.POST,
      loading: false,
      hastoast: false,
      params: {
        "type": 0,
        "page_size": 20,
        "page_index": sysPageIndex,
      },
      onSuccess: (res) {
        if (res == null) return;
        sysListNum = res["total"];
        if (isLoad) {
          sysMsgList += res["detail"] ?? [];
        } else {
          sysMsgList = res["detail"] ?? [];
        }
        if (!isInit) {
          sysunread = 0;
          messageRead(1);
        } else {
          sysunread = res["unread_count"];
          EventUtils().emit(EventConstants.E_MSG_UPDATE, null);
        }
      },
    );
    return sysMsgList;
  }

  // mq下发官方消息
  void mqAdminList(bool isAdd) {
    if (isAdd) {
      adminunread += 1;
    } else {
      adminunread -= 1;
    }
    EventUtils().emit(EventConstants.E_MSG_UPDATE, null);
  }

  // 官方消息
  Future<List> adminMessageList(
      {bool isInit = true, bool isLoad = false}) async {
    await DioUtils().httpRequest(
      NetConstants.adminMessage,
      method: DioMethod.POST,
      loading: false,
      hastoast: false,
      params: {
        "page_size": 20,
        "page_index": adminPageIndex,
      },
      onSuccess: (res) {
        if (res == null) return;
        adminListNum = res["total"];
        if (isLoad) {
          adminMsgList += res["detail"] ?? [];
        } else {
          adminMsgList = res["detail"] ?? [];
        }
        if (!isInit) {
          adminunread = 0;
          messageRead(2);
        } else {
          adminunread = res["unread_count"];
          EventUtils().emit(EventConstants.E_MSG_UPDATE, null);
        }
      },
    );
    return adminMsgList;
  }

  // 活动公告
  void activityMessageList() {
    DioUtils().httpRequest(
      NetConstants.activityMessage,
      method: DioMethod.POST,
      loading: false,
      hastoast: false,
      params: {
        "is_read": 0,
        "status": 0,
        "page_size": 20,
        "page_index": activityPageIndex,
      },
      onSuccess: (res) {
        if (res == null) return;
        activityunread = res["unread_count"];
        activityListNum = res["total"];
        activityMsgList = res["detail"] ?? [];
        EventUtils().emit(EventConstants.E_MSG_UPDATE, null);
      },
    );
  }
}
