import 'dart:async';
import 'dart:io';
import 'dart:math';
import 'package:echo/import.dart';
import 'package:path_provider/path_provider.dart';

import 'package:tencent_trtc_cloud/trtc_cloud.dart';
import 'package:tencent_trtc_cloud/trtc_cloud_def.dart';
import 'package:tencent_trtc_cloud/trtc_cloud_listener.dart';
import 'package:tencent_trtc_cloud/tx_audio_effect_manager.dart';

import 'DebugUserSig.dart';

class TRTCManager {
  //
  final int APPID = 1400826023;

  // TRTC实例
  late TRTCCloud _trtcCloud;

  late TXAudioEffectManager _txAudioManager;

  // 音乐音量
  int musicVolume = 100;

  // 初始化声网
  void initRtcEngine() async {
    print("初始化TRTC开始");
    try {
      _trtcCloud = (await TRTCCloud.sharedInstance())!;
      _txAudioManager = _trtcCloud.getAudioEffectManager();
      _trtcCloud.registerListener(onTrtcListener);
    } catch (e) {
      print("初始化TRTC失败$e");
    }
  }

  onTrtcListener(type, params) async {
    switch (type) {
      case TRTCCloudListener.onError:
        print("TRTCSDK异常 ${params["errCode"]} ${params["errMsg"]}");
        break;
      case TRTCCloudListener.onWarning:
        break;
      case TRTCCloudListener.onEnterRoom:
        print("用户加入成功");
        break;
      case TRTCCloudListener.onExitRoom:
        // 用户退出TRTC房间
        break;
      case TRTCCloudListener.onSwitchRole:
        break;
      case TRTCCloudListener.onRemoteUserEnterRoom:
        break;
      case TRTCCloudListener.onRemoteUserLeaveRoom:
        break;
      case TRTCCloudListener.onConnectOtherRoom:
        break;
      case TRTCCloudListener.onDisConnectOtherRoom:
        break;
      case TRTCCloudListener.onSwitchRoom:
        break;
      case TRTCCloudListener.onUserVideoAvailable:
        break;
      case TRTCCloudListener.onUserSubStreamAvailable:
        break;
      case TRTCCloudListener.onUserAudioAvailable:
        print("其他用户 ${params["userId"]} ${params['available'] ? "加入" : "离开"}");
        break;
      case TRTCCloudListener.onFirstVideoFrame:
        break;
      case TRTCCloudListener.onFirstAudioFrame:
        break;
      case TRTCCloudListener.onSendFirstLocalVideoFrame:
        break;
      case TRTCCloudListener.onSendFirstLocalAudioFrame:
        break;
      case TRTCCloudListener.onNetworkQuality:
        break;
      case TRTCCloudListener.onStatistics:
        break;
      case TRTCCloudListener.onConnectionLost:
        break;
      case TRTCCloudListener.onTryToReconnect:
        break;
      case TRTCCloudListener.onConnectionRecovery:
        break;
      case TRTCCloudListener.onSpeedTest:
        break;
      case TRTCCloudListener.onCameraDidReady:
        break;
      case TRTCCloudListener.onMicDidReady:
        break;
      case TRTCCloudListener.onUserVoiceVolume:
        break;
      case TRTCCloudListener.onRecvCustomCmdMsg:
        break;
      case TRTCCloudListener.onMissCustomCmdMsg:
        break;
      case TRTCCloudListener.onRecvSEIMsg:
        break;
      case TRTCCloudListener.onStartPublishing:
        break;
      case TRTCCloudListener.onStopPublishing:
        break;
      case TRTCCloudListener.onStartPublishCDNStream:
        break;
      case TRTCCloudListener.onStopPublishCDNStream:
        break;
      case TRTCCloudListener.onSetMixTranscodingConfig:
        break;
      case TRTCCloudListener.onMusicObserverStart:
        break;
      case TRTCCloudListener.onMusicObserverPlayProgress:
        AppManager().roomInfo.musicPlaySchedule = params["curPtsMS"];
        break;
      case TRTCCloudListener.onMusicObserverComplete:
        print("本地音效文件播放已结束");
        _txAudioManager.stopPlayMusic(0);
        playNextMusic();
        break;
      case TRTCCloudListener.onSnapshotComplete:
        break;
      case TRTCCloudListener.onScreenCaptureStarted:
        break;
      case TRTCCloudListener.onScreenCapturePaused:
        break;
      case TRTCCloudListener.onScreenCaptureResumed:
        break;
      case TRTCCloudListener.onScreenCaptureStoped:
        break;
      case TRTCCloudListener.onDeviceChange:
        break;
      case TRTCCloudListener.onTestMicVolume:
        break;
      case TRTCCloudListener.onTestSpeakerVolume:
        break;
    }
  }

  void playNextMusic() {
    AppManager().roomInfo.playMusicMode = 3;
    int index = 0;
    if (AppManager().roomInfo.musicMode == 0) {
      index = (AppManager().roomInfo.musicIndex + 1) ==
              AppManager().roomInfo.musicList.length
          ? 0
          : AppManager().roomInfo.musicIndex + 1;
    } else if (AppManager().roomInfo.musicMode == 1) {
      index = Random().nextInt(AppManager().roomInfo.musicList.length);
    }

    var id = AppManager().roomInfo.musicMode != 2
        ? AppManager().roomInfo.musicList[index]["songId"]
        : null;

    AppManager().roomAudioMixing(id: id);
  }

  // 状态监听
  void statusMonitoring() {
    try {
      _trtcCloud.registerListener(onTrtcListener);
    } catch (e) {
      print("TRTC监听失败$e");
    }
  }

  // 取消声网监听
  void unregisterEventHandler() {
    try {
      _trtcCloud.unRegisterListener(onTrtcListener);
    } catch (e) {
      print("取消TRTC监听失败$e");
    }
  }

  // 用户加入TRTC房间
  void joinRtcEngine(int uid, String channelId, String token) async {
    print("用户加入TRTC房间$uid, $channelId, $token");
    try {
      TRTCParams params = TRTCParams();
      params.sdkAppId = APPID;
      // params.sdkAppId = GenerateTestUserSig.sdkAppId;
      params.strRoomId = channelId;
      params.userId = "$uid";
      params.role = TRTCCloudDef.TRTCRoleAnchor;
      // params.userSig = token;
      params.userSig = GenerateTestUserSig.genTestSig(params.userId);
      await _trtcCloud.enterRoom(
          params, TRTCCloudDef.TRTC_APP_SCENE_VOICE_CHATROOM);
    } catch (e) {
      print("用户加入TRTC房间失败$e");
    }
  }

  // 用户退出声网房间
  void leaveRtcEngine() async {
    try {
      await _trtcCloud.exitRoom();
    } catch (e) {
      print("用户退出TRTC房间失败$e");
    }
  }

  // 设置用户角色和级别
  void setClientRole(int role) async {
    try {
      await _trtcCloud.switchRole(role);
    } catch (e) {
      print("设置用户角色和级别失败$e");
    }
  }

  // 推流/暂停推流
  void enableLocalAudio(bool enabled) async {
    try {
      if (enabled) {
        await _trtcCloud.startLocalAudio(TRTCCloudDef.TRTC_AUDIO_QUALITY_MUSIC);
      } else {
        await _trtcCloud.stopLocalAudio();
      }
    } catch (e) {
      print("${enabled ? "推流" : "暂停推流失败"}$e");
    }
  }

  // 拉流/暂停拉流
  void pullLocalAudio(bool pull) {
    try {
      if (pull) {
        _trtcCloud.startLocalAudio(TRTCCloudDef.TRTC_AUDIO_QUALITY_MUSIC);
        _trtcCloud.muteLocalAudio(false);
        _trtcCloud.muteAllRemoteAudio(false);
      } else {
        _trtcCloud.stopLocalAudio();
        _trtcCloud.muteLocalAudio(true);
        _trtcCloud.muteAllRemoteAudio(true);
      }
    } catch (e) {
      print("${pull ? "拉流" : "暂停拉流失败"}$e");
    }
  }

  // 播放/暂停播放音乐
  void audioMixing(
      {String filePath = "", int cycle = 1, bool stop = false}) async {
    // cycle 音乐文件的播放次数 ≥ 0: 播放次数。例如，0 表示不播放；1 表示播放 1 次 -1: 无限循环播放;
    try {
      if (stop) {
        await _txAudioManager.pausePlayMusic(0);
        AppManager().roomInfo.playMusicMode = 2;
      } else if (filePath.isNotEmpty) {
        AudioMusicParam musicParam = AudioMusicParam(path: filePath, id: 0);
        if (cycle > 1) {
          musicParam.loopCount = cycle - 1;
        } else if (cycle == 0) {
          return;
        } else if (cycle < 0) {
          musicParam.loopCount = 9007199254740991; // 2^53-1
        }
        musicParam.publish = true;
        await _txAudioManager.startPlayMusic(musicParam);
        AppManager().roomInfo.playMusicMode = 1;
      }
      EventUtils().emit(EventConstants.E_ROOM_MUSIC, null);
    } catch (e) {
      print("${stop ? "暂停播放" : "播放"}音乐失败$e");
    }
  }

  // 恢复播放音乐
  void resumeAudioMixing() async {
    try {
      _txAudioManager.resumePlayMusic(0);
      AppManager().roomInfo.playMusicMode = 1;
      EventUtils().emit(EventConstants.E_ROOM_MUSIC, null);
    } catch (e) {
      print("恢复播放音乐失败$e");
    }
  }

  // 设置耳返功能
  void enableInEarMonitoring(bool isOpen) async {
    try {
      await _txAudioManager.enableVoiceEarMonitor(isOpen);
    } catch (e) {
      print("设置耳返功能$e");
    }
  }

  // 获取音乐音量
  Future<int> getAudioMixingPlayoutVolume() async {
    try {
      return musicVolume;
    } catch (e) {
      print("获取音乐音量失败$e");
      return 0;
    }
  }

  // 设置音乐音量
  void setAudioMixingPitch(int pitch) async {
    try {
      if (pitch < 0) {
        musicVolume = 0;
      } else if (pitch > 100) {
        musicVolume = 100;
      } else {
        musicVolume = pitch;
      }
      await _txAudioManager.setAllMusicVolume(musicVolume);
    } catch (e) {
      print("设置音乐音量失败$e");
    }
  }

  // 设置麦克风采集音量
  void setAudioCaptureVolume(int volume) async {
    try {
      await _trtcCloud.setAudioCaptureVolume(volume);
    } catch (e) {
      print("设置SDK采集音量失败$e");
    }
  }

  // 获取麦克风采集音量
  Future<int> getAudioCaptureVolume() async {
    try {
      return (await _trtcCloud.getAudioCaptureVolume())!;
    } catch (e) {
      print("获取音乐音量失败$e");
      return 0;
    }
  }

  // 设置扬声器播放音量
  void setAudioPlayoutVolume(int volume) async {
    try {
      await _trtcCloud.setAudioPlayoutVolume(volume);
    } catch (e) {
      print("设置SDK播放音量失败$e");
    }
  }

  // 获取扬声器播放音量
  Future<int> getAudioPlayoutVolume() async {
    try {
      return (await _trtcCloud.getAudioPlayoutVolume())!;
    } catch (e) {
      print("获取SDK播放音量失败$e");
      return 0;
    }
  }

  // 录音配置
  void startAudioRecording() async {
    try {
      Directory tempDir = await getTemporaryDirectory();
      int timeStamp = DateTime.now().millisecondsSinceEpoch ~/ 1000;
      String filePath = '${tempDir.path}/audioRecord-${timeStamp}2.14';
      await _trtcCloud
          .startAudioRecording(TRTCAudioRecordingParams(filePath: filePath));
    } catch (e) {
      print("录音配置失败${e.toString()}");
    }
  }

  // 停止录音
  void stopAudioRecording() {
    try {
      _trtcCloud.stopAudioRecording();
    } catch (e) {
      print("停止录音失败${e.toString()}");
    }
  }

  // 设置变声特效
  // 0：关闭；1：熊孩子；2：萝莉；3：大叔；4：重金属；5：感冒；6：外语腔；7：困兽；8：肥宅；9：强电流；10：重机械；11：空灵
  void setVoiceChangerType(int type) {
    _txAudioManager.setVoiceChangerType(type);
  }

  // 设置混响
  // 0：关闭；1：KTV；2：小房间；3：大会堂；4：低沉；5：洪亮；6：金属声；7：磁性；8：空灵；9：录音棚；10：悠扬 11：录音棚2
  void setVoiceReverbType(int type) {
    try {
      _txAudioManager.setVoiceReverbType(type);
    } catch (e) {
      print("设置混响失败${e.toString()}");
    }
  }

  // 销毁TRTC实例
  void releaseRtcEngine() async {
    try {
      await TRTCCloud.destroySharedInstance();
    } catch (e) {
      print("销毁TRTC实例失败${e.toString()}");
    }
  }
}
