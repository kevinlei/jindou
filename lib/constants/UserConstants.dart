import 'package:echo/import.dart';

class UserConstants {
  static LoginUserModel? userginInfo;
  static LocationResult? location;

  // 开播通知
  static bool openBroad = true;
  // 声音通知
  static bool openAudio = true;
  // 震动通知
  static bool openShock = true;
  // 粉丝通知
  static bool openFans = true;
  //个性化开关
  static bool openBsts = true;

  //防止闪退
  static bool openflash = false;

  static void getNoticeSet(List<String> list) {
    if (list.isEmpty) return;
    openBroad = list.isNotEmpty && list[0] == "true";
    openAudio = list.length > 1 && list[1] == "true";
    openShock = list.length > 2 && list[2] == "true";
    openFans = list.length > 3 && list[3] == "true";
    openBsts = list.length > 4 && list[4] == "true";
    openflash = list.length > 5 && list[5] == "true";
  }

  static Future<void> setNoticeSet() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setStringList(SharedKey.KEY_NOTICE_SET.value, [
      openBroad.toString(),
      openAudio.toString(),
      openShock.toString(),
      openFans.toString(),
      openBsts.toString(),
      openflash.toString(),
    ]);
  }

  static ExtUser? getExtUser() {
    if (userginInfo == null) return null;
    ExtUser user = ExtUser()
      ..id = userginInfo!.info.userId
      ..displayId = userginInfo!.info.displayId
      ..name = userginInfo!.info.name
      ..head = userginInfo!.info.headIcon
      ..wealthBadge = userginInfo!.gradeInfo.userGrade.wealth.badge
      ..charmBadge = userginInfo!.gradeInfo.userGrade.charm.badge
      ..wealthGrade = userginInfo!.gradeInfo.userGrade.wealth.grade
      ..charmGrade = userginInfo!.gradeInfo.userGrade.charm.grade
      ..role = userginInfo!.info.userRole;
    return user;
  }

  static int getUserAge(int birthday) {
    if (birthday == 0) {
      return 0;
    }
    int year1 = DateTime.fromMillisecondsSinceEpoch(birthday).year;
    int year2 = DateTime.now().year;
    return year2 - year1;
  }

  static String getZodiacImg(int birthday) {
    if (birthday == 0) {
      return "none";
    }
    int month = DateTime.fromMillisecondsSinceEpoch(birthday).month;
    int day = DateTime.fromMillisecondsSinceEpoch(birthday).day;
    switch (month) {
      case DateTime.january:
        return day >= 20 ? "person/zodiac1" : "person/zodiac12";
      case DateTime.february:
        return day >= 19 ? "person/zodiac2" : "person/zodiac1";
      case DateTime.march:
        return day >= 21 ? "person/zodiac3" : "person/zodiac2";
      case DateTime.april:
        return day >= 20 ? "person/zodiac4" : "person/zodiac3";
      case DateTime.may:
        return day >= 21 ? "person/zodiac5" : "person/zodiac4";
      case DateTime.june:
        return day >= 22 ? "person/zodiac6" : "person/zodiac5";
      case DateTime.july:
        return day >= 23 ? "person/zodiac7" : "person/zodiac6";
      case DateTime.august:
        return day >= 23 ? "person/zodiac8" : "person/zodiac7";
      case DateTime.september:
        return day >= 23 ? "person/zodiac9" : "person/zodiac8";
      case DateTime.october:
        return day >= 24 ? "person/zodiac10" : "person/zodiac9";
      case DateTime.november:
        return day >= 23 ? "person/zodiac11" : "person/zodiac10";
      case DateTime.december:
        return day >= 22 ? "person/zodiac12" : "person/zodiac11";
    }
    return "none";
  }

  static String getZodiacName(int birthday) {
    if (birthday == 0) {
      return "";
    }
    int month = DateTime.fromMillisecondsSinceEpoch(birthday).month;
    int day = DateTime.fromMillisecondsSinceEpoch(birthday).day;
    switch (month) {
      case DateTime.january:
        return day >= 20 ? "水瓶座" : "魔羯座";
      case DateTime.february:
        return day >= 19 ? "双鱼座" : "水瓶座";
      case DateTime.march:
        return day >= 21 ? "白羊座" : "双鱼座";
      case DateTime.april:
        return day >= 20 ? "金牛座" : "白羊座";
      case DateTime.may:
        return day >= 21 ? "双子座" : "金牛座";
      case DateTime.june:
        return day >= 22 ? "巨蟹座" : "双子座";
      case DateTime.july:
        return day >= 23 ? "狮子座" : "巨蟹座";
      case DateTime.august:
        return day >= 23 ? "处女座" : "狮子座";
      case DateTime.september:
        return day >= 23 ? "天秤座" : "处女座";
      case DateTime.october:
        return day >= 24 ? "天蝎座" : "天秤座";
      case DateTime.november:
        return day >= 23 ? "射手座" : "天蝎座";
      case DateTime.december:
        return day >= 22 ? "魔羯座" : "射手座";
    }
    return "";
  }
}
