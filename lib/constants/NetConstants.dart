import 'package:echo/import.dart';

class NetConstants {
  static String shareUrl = "http://share.h5.ayuyin.com/#/";

  static Map host = {
    // AppEnv.ENV_DEV.value: "https://api-dev.chuncui.online",
    AppEnv.ENV_DEV.value: "http://api.chuncui.online", // 测试抓包
    AppEnv.ENV_TST.value: "https://api.chuncui.online",
    AppEnv.ENV_LIN.value: "https://api.chuncui.online",
    // AppEnv.ENV_LIN.value: "http://api.chuncui.online", // 正式抓包
  };

  /// ---- OSS ----
  static final Map _ossPath = {
    AppEnv.ENV_DEV.value: "https://jindou-prod.oss-cn-hangzhou.aliyuncs.com/",
    AppEnv.ENV_TST.value: "https://jindou-prod.oss-cn-hangzhou.aliyuncs.com/",
    AppEnv.ENV_LIN.value: "https://jindou-prod.oss-cn-hangzhou.aliyuncs.com/",
  };
  static String get ossPath => _ossPath[AppConstants.appEnv.value];

  /// ---- MQ ----
  static final Map _mqConfig = {
    AppEnv.ENV_DEV.value: {"host": "mqtt-dev.chuncui.online", "port": 1883},
    AppEnv.ENV_TST.value: {"host": "mqtt.chuncui.online", "port": 1883},
    AppEnv.ENV_LIN.value: {"host": "mqtt.chuncui.online", "port": 1883},
  };
  static Map get mqConfig => _mqConfig[AppConstants.appEnv.value];

  /// ---- 个推 ----
  static final Map _quickConfig = {
    AppEnv.ENV_DEV.value: "pVfyPR8Ytq5KXmxzWmZ1r4",
    AppEnv.ENV_TST.value: "X6E8EWC71t6N8vSDYkcesA",
    AppEnv.ENV_LIN.value: "X6E8EWC71t6N8vSDYkcesA",
  };
  static String get quickConfig => _quickConfig[AppConstants.appEnv.value];

  /// ---- 声网房间token ----
  static final Map _agoraToken ={
    AppEnv.ENV_DEV.value: "http://agora-dev.chuncui.online",
    AppEnv.ENV_TST.value: "http://agora.chuncui.online",
    AppEnv.ENV_LIN.value: "http://agora.chuncui.online",
  };
  static String get agoraToken => _agoraToken[AppConstants.appEnv.value];

  /// ----------------------------------登录---------------------------------- ///
  // 获取token
  static String getToken = '/connect/token';

  /// ----------------------------------渠道---------------------------------- ///
  // static String channelStatistics =
  //     host[AppConstants.appEnv.value] + '/data/elk/log';

  /// ----------------------------------房间---------------------------------- ///
  static String hostRoom =
      "${host[AppConstants.appEnv.value]}/room-api/api/v1/room";

  /// ----------------------------------记录消息---------------------------------- ///
  static String hostMsg =
      "${host[AppConstants.appEnv.value]}/notice-api/api/v1/message";

  /// ----------------------------------通用服---------------------------------- ///
  static String hostShumei =
      host[AppConstants.appEnv.value] + "/ishumei-api/api/v1";

  /// ----------------------------------系统服---------------------------------- ///
  static String hostSys =
      host[AppConstants.appEnv.value] + "/notice-api/api/v1/message";

  /// ----------------------------------配置服---------------------------------- ///
  /// ----全局配置-----
  static String hostDataConfig =
      host[AppConstants.appEnv.value] + "/data-api/api/v1";

  /// ----无需鉴权全局配置-----
  static String hostNoneDataConfig =
      host[AppConstants.appEnv.value] + "/data-private-api/api/v1";

  /// ----社区服动态 ----
  static String hostDynamic =
      host[AppConstants.appEnv.value] + "/dynamic-api/api/v1";

  /// ----公会服----
  static String hostClub =
      host[AppConstants.appEnv.value] + "/guild-api/api/v1/";

  /// ---登录反馈配置---
  static String hostLoginfig =
      host[AppConstants.appEnv.value] + "/notice-api/api/v1";

  /// ----账号服----
  static String hostAccount =
      host[AppConstants.appEnv.value] + "/user-api/api/v1";

  /// ----礼物----
  static String hostGift =
      host[AppConstants.appEnv.value] + "/bags-api/api/v1/gift";

  /// ----游戏----
  static String hostGame = host[AppConstants.appEnv.value] + "/game-api/api/v1";

  /// ----我的账户----
  static String hostbBags =
      host[AppConstants.appEnv.value] + "/bags-api/api/v1";

  /// ----音乐----
  static String hostMusic =
      host[AppConstants.appEnv.value] + "/music-api/api/v1/music";

  /// ----支付服----
  static String hostPay =
      host[AppConstants.appEnv.value] + "/unify-pay-api/api/v1/pay";

  /// ----------------------------------配置---------------------------------- ///
  // 无需鉴权APP配置
  static String noneConfig = "$hostDataConfig/conf/info";
  // APP配置
  static String config = "$hostDataConfig/conf/info";
  // APP各种协议
  static String protocol = "$hostDataConfig/un-token/article/info";
  // 各种配置banner列表
  static String banner = "$hostDataConfig/banner/list";
  // 开屏广告
  static String openScreenAdvertising = "$hostDataConfig/ad/info";
  // 数美解析设备id
  static String getShumeiDevicId = '$hostShumei/shumei/device-info';
  // 检测文本
  static String checkText = '$hostShumei/shumei/check-text';
  // 检测图片
  static String checkImage = '$hostShumei/shumei/check-image';
  // 检测音频
  static String checkVideo = '$hostShumei/shumei/check-audio';
  // 版本更新
  static String versionConfig = "$hostDataConfig/conf/getOne";
  // 礼物配置
  static String giftsConfig = "$hostDataConfig/conf/gift";
  //表情配置
  static String emojisConfig = "$hostDataConfig/emojis/info/list";

  // 房间礼物
  static String roomGifts = "$hostDataConfig/gift/list";
  // 录音配置和职业和关于
  static String recordAudio = "$hostDataConfig/conf/info";
  // 上传文件
  static String uploadFile =
      "${host[AppConstants.appEnv.value]}/oss-api/upload";
  // 登录问题反馈
  static String roomFeedback = "$hostLoginfig/message/feedback";
  // App意见反馈
  static String roomadvice = "$hostLoginfig/message/advice";
  // 提现绑定状态
  static String bindStatus = "$hostAccount/payment_account/list";
  // 绑定支付宝或银行卡
  static String bindCard = "$hostAccount/payment_account/bind";
  // 用户提现申请
  static String deposit = "$hostbBags/withdrawal/apply";
  //统一下单接口
  // static String payCreateOrder = "$hostPay/unify_order_d87d";
  static String payCreateOrder = "$hostPay/unify_order_f8ef";
  // 充值金额配置
  static String productAudio = "$hostDataConfig/product/list";
  // 收益提现记录
  static String depositRecord = "$hostbBags/withdrawal/owner/withdrawals/list";
  // 充值记录
  static String chargeRecord = "$hostAccount/product/list";

  // 获取明细类型
  static String finance = "$hostbBags/bags/grade/finance/type";

  // 用户个人明细表
  static String gradeRecord = "$hostbBags/bags/grade/finance/info";

  // 银行卡提现详情
  static String gradeRecordetail = "$hostbBags/bags/grade/finance/info/detail";

  // 微信对话开放api
  static String wxRecord =
      "https://chatbot.weixin.qq.com/openapi/sign/7bZcj7lwQ11RGGNqTtpxdudrkwQBC5";

  /// ----------------------------------用户---------------------------------- ///
  // 搜索用户房间
  static String search = "$hostAccount/user/search_user_and_room";
  // 获取用户信息
  static String getUserInfo = "$hostAccount/user/info";
  // 个人主页
  static String gethomeUserInfo = "$hostAccount/user/home";
  // 获取用户基础信息
  static String getbaseInfo = "$hostAccount/user/base_info";

  static String getchangeInfo = "$hostAccount/user/change_teen_mode";

  // 修改个人资料
  static String modifyUser = "$hostAccount/user/info_update";
  // 修改头像
  static String updatHeadUser = "$hostAccount/user/head_icon_update";

  // home获取用户关系
  static String relahomeship = "$hostAccount/user/relationship";
  // 获取当前图片状态
  static String modicoviewnUser = "$hostAccount/user/head_icon_review";
  // 取消审核
  static String modireviewUser = "$hostAccount/user/head_icon_review/cancel";
  // 添加照片墙
  static String photoWall = "$hostAccount/user/photos";
  // 获取用户当前余额（金币值、收益等）
  static String gradeUserMoney = "$hostbBags/grade/user/gold_info";
  // 收益兑换金币
  static String exchangeGift = "$hostbBags/grade/gains/exchange/gold";
  // 更新用户信息
  static String getUserupdate = "$hostAccount/user/register_update";
  // 获取关注。粉丝。访客数量
  static String getRelationCount = "$hostAccount/user/flow_and_visitor_count";
  // 关注
  static String followUser = "$hostAccount/user/flow";
  // 获取实名认证的状态/储存实名认证状态
  static String authStatus = "$hostAccount/idcard/is_validate_idcard";
  // 人工实名认证
  static String authManual = "$hostAccount/idcard/bind_idcard";
  //人脸认证获取certifyId
  static String aliFaceCheck = '$hostAccount/idcard/init_face_verify';
  // 关注列表
  static String followList = "$hostAccount/user/flow_list";
  // 取消关注
  static String unFollow = "$hostAccount/user/flow";
  // 访客列表
  static String visitorList = "$hostAccount/visitor/info";
  // 装扮类别
  static String dressMenu = "$hostDataConfig/category/info/list";
  // App动态反馈
  static String reportUser = "$hostMsg/dynamic-inform";
  // 搜索用户
  static String searchUser = "SearchUser";
  // 好友列表
  static String friendList = "/GetFriends";
  // 粉丝列表
  static String fansList = "/getFans";
  // 背包 ?needDetail=1 0：是要礼物 1：不要礼物
  static String getBag = "GetBag";
  // 购买装扮
  static String buyDress = "BuyAvatar";
  // 是否设置了二级密码
  static String secondaryStatus = "GetSecondPwdStatus";
  // 重置二级密码
  static String resetSecondPwd = "ChangeSecondPwd";
  // 自己的装扮
  static String selfDress = "GetAvatar";
  // 使用装扮
  static String useDress = "UseAvatar";
  // 取下装扮
  static String removeDress = "UnUseAvatar";
  // 自己的购买记录
  static String titleRecord = "GetPeerages";
  // 获取我的礼物墙
  static String giftWall = "$hostAccount/user/gift_wall";
  // 隐私设置
  static String getUserSet = "GetOtherPrivateConfig/";
  // 设置隐私配置
  static String setUserSet = "SetUserPrivateConfig";
  // 用户是否在房间
  static String inRoom = "$hostRoom/user/online/info";
  // 请求用户黑名单
  static String userBlack = "$hostAccount/user/flow_list";

  // 获取用户登录设备
  static String userdeviceBlack = "$hostAccount/user/device";

  // 获取用户登录设备
  static String delete_deviceBlack = "$hostAccount/user/delete_device";

  // 添加到黑名单
  static String addBlack = "addUserBlackList";
  // 帮助中心
  static String helpCenter = "$hostDataConfig/category/info/list";
  // 帮助中心里列表
  static String helpCenterSon = "$hostDataConfig/article/info/list";
  // 扩列用户列表
  static String expansionColumn = "$hostAccount/user/recommend_user";

  /// ----------------------------------账号---------------------------------- ///
  // 获取登录和注册短信验证码
  static String loginCode = "$hostAccount/user/send_code";
  // 获取获取除登录和注册短信验证码之外 其他的短信验证码
  static String loginUser = "$hostAccount/user/login";
  // 获取获取除登录和注册短信验证码之外 其他的短信验证码
  static String infoUser = "$hostAccount/getSmsCode";
  // 忘记密码验证code
  static String validateCode = "$hostAccount/user/validate_code";
  // 重设密码
  static String resetPwd = "$hostAccount/user/pwd_change";
  // 验证当前手机
  static String checkPhone = "$hostAccount/user/validate_code";
  // 绑定手机
  static String bindPhone = "$hostAccount/user/phone_change";

  /// ----------------------------------注销---------------------------------- ///
  // 注销账号
  static String quitAccount = "$hostAccount/CancelAccount";
  // 注销条件
  static String quiteCondition = "CheckCancelInfo";

  /// ----------------------------------动态---------------------------------- ///

  // 请求社区推荐列表
  static String dyNearbyList = "$hostDynamic/dynamic/posts/recommend";
  // 请求社区最新列表
  static String dyRecList = "$hostDynamic/dynamic/posts/latest";
  // 请求社区关注列表
  static String dyFollowList = "$hostDynamic/dynamic/posts/follow";
  // 社区请求投票/点赞/评论点赞/
  static String dynamicVote = "$hostDynamic/dynamic/posts/likes";
  // 发布社区动态
  static String publish = "$hostDynamic/dynamic/posts";
  // 动态详情
  static String dynamicInfo = "$hostDynamic/dynamic/posts/details";
  // 动态评论
  static String dyComment = "$hostDynamic/dynamic/posts/comments";
  // 获取用户关系
  static String getRelation = "$hostDynamic/dynamic/posts/relation";
  // 回复评论
  static String dyReplies = "$hostDynamic/dynamic/comments/replies";
  // 个人动态
  static String userDynamic = "$hostDynamic/dynamic/posts/user";
  // 动态删除
  static String dynamicDel = "$hostDynamic/dynamic/posts/delete";
  // 更改动态权限
  static String dynamicAuth = "$hostDynamic/dynamic/user/update";
  // 热门话题
  static String hotTopics = "$hostDynamic/dynamic/topics/hots";
  // 添加话题
  static String addTopic = "$hostDynamic/topic/add";
  // 动态搜索
  static String dySearch = "$hostDynamic/dynamic/search";
  // 官方操作动态
  static String officialHandle = "$hostDynamic/dynamic/user/handle";

  /// ----------------------------------公会---------------------------------- ///
  // 创建公会
  static String createClub = "${hostClub}guild/guilds";
  // 获取用户的公会
  static String userClub = "${hostClub}guild/guilds/myguild";
  // 公会房间列表
  static String clubRoom = "${hostClub}guild/guilds/rooms";
  // 获取公会列表
  static String clubList = "${hostClub}guild/guilds/reco";
  // 查看房间详情
  static String userdetails = "${hostClub}guild/guilds/room/details";
  // 获取公会详情
  static String clubInfo = "${hostClub}guild/guilds/details";
  // 搜索公会成员接口
  static String ownersearch = "${hostClub}guild/guilds/setroomowner/search";
  // 会长设置房间厅长
  static String ownerguild = "${hostClub}guild/guilds/room/owner";
  // 公会成员
  static String clubMember = "${hostClub}guild/guilds/members";
  // 修改公会
  static String modifyClub = "${hostClub}guild/UpdateGuild";
  // 入会申请列表
  static String joinList = "${hostClub}guild/guilds/apply/list";
  // 签约申请列表
  static String signList = "${hostClub}guildMember/GetGuildSigning";
  // 解约申请列表
  static String unSignList = "${hostClub}guild/guilds/quit/list";
  // 退出申请列表
  static String quitList = "${hostClub}guildMember/GetGuildExit";
  // 申请加入公会
  static String applyJoin = "${hostClub}guild/guilds/apply";
  // 同意或拒绝加入申请
  static String agreeJoin = "${hostClub}guild/guilds/apply/check";
  // 申请签约
  static String applySign = "${hostClub}guildMember/SetGuildInfo";
  // 同意或拒绝签约申请
  static String agreeSign = "${hostClub}guildMember/SetGuildSigning";
  // 申请解约
  static String applyUnSign = "${hostClub}guildMember/SetSigningStatus";
  // 同意或拒绝解约申请
  static String agreeUnSign = "${hostClub}guild/guilds/quit/check";
  // 申请退出公会
  static String applyQuit = "${hostClub}guildMember/ExitGuild";
  // 同意或拒绝退出申请
  static String agreeQuit = "${hostClub}guildMember/setExitGuild";
  // 公会新消息
  static String clubMsg = "${hostClub}guildMember/GetGuildIsMessage";
  // 踢出公会
  static String kickOutClub = "${hostClub}guildMember/KickUutGuild";
  // 签约人数
  static String signCount = "${hostClub}guildMember/GetQXGuildUserInfoCount";
  // 公会搜索
  static String clubSearch = "${hostClub}guild/guilds/search";
  // 设置厅主
  static String setRoomOwner = "${hostClub}guildMember/SetVicePresident";
  // 公会签约成员
  static String signMember = "${hostClub}guildMember/GetGuildRegular";
  // 所在公会的外显id
  static String clubDisplayId = "${hostClub}guild/GetGuildDisplayByGuild";
  // 公会时间段流水
  static String clubTimeBillList =
      "${hostClub}flowingWater/GetGuildRoomFinanceByTimestamp";
  // 我的工会首页日流水
  static String getGuildDataFW = "${hostClub}flowingWater/GetGuildFW";
  // 公会时间段流水值
  static String clubTimeBillValue =
      "${hostClub}flowingWater/GuildTimeRangeFinance";
  // 公会流水总值
  static String clubTotalBillValue = "${hostClub}flowingWater/GuildAllFinance";
  // 公会房间时间段流水
  static String clubRoomTimeBillList =
      "${hostClub}flowingWater/RoomFinanceByTimestamp";
  // 公会房间时间段流水值
  static String clubRoomTimeBillValue =
      "${hostClub}flowingWater/RoomTimeRangeFinance";
  // 公会房间流水总值
  static String clubRoomTotalBillValue =
      "${hostClub}flowingWater/GetFinanceByRoomId";

  /// -----------------------------------------------房间--------------------------------------------- ///
  // 创建房间
  static String roomCreate = "$hostRoom/info/create";
  // 首页获取房间Type
  static String getelationHome = "$hostDataConfig/room/topic/home/list";
  // 获取房间Type
  static String getelation = "$hostDataConfig/room/topic/info/list";
  // 随机获取一个房间
  static String randomRoom = "$hostRoom/random/join";
  // 获取房间列表
  static String roomList = "$hostRoom/info/list";
  // 获取推荐房间列表
  static String hotRoomList = "$hostRoom/info/mic/user/num";
  // 获取大家都在看房间列表
  static String userLockRoomList = "$hostRoom/info/finance/list";
  // 获取热门房间列表
  static String recoRoomList = "$hostRoom/info/list";
  // 获取自己关注/管理/创建的房间列表
  static String roomPowerList = "$hostRoom/info/user/s";
  // 加入房间
  static String joinRoom = "$hostRoom/join";
  // 重连房间
  static String reconnectRoom = "$hostRoom/retry/join";
  // 退出房间
  static String quiteRoom = "$hostRoom/exit";
  // 麦位列表
  static String seatList = "$hostRoom/mic/list";
  // 房间在线人数列表
  static String roomUserList = "$hostRoom/user/online/list";
  // 上麦
  static String upWheat = "$hostRoom/mic/set";
  // 下麦
  static String downWheat = "$hostRoom/mic/abdication";
  // 取消/收藏房间
  static String collectRoom = "$hostRoom/follow";
  // 修改房间上麦模式/开关聊天/心动值开关
  static String setRoomMode = "$hostRoom/info/update/function/state";
  // 排麦列表
  static String barleyList = "$hostRoom/mic/apply/list";
  // 申请排麦
  static String applyBarley = "$hostRoom/mic/apply";
  // 取消排麦
  static String cellBarley = "$hostRoom/mic/apply/cancel";
  // 一键同意/取消排麦
  static String allBarley = "$hostRoom/mic/apply/handle/all";
  // 锁麦/取消锁麦
  static String lockSeat = "$hostRoom/mic/update/lock";
  // 管理员操作排麦申请
  static String acceptBarley = "$hostRoom/mic/apply/handle";
  // 清空心动值
  static String clearToLove = "$hostRoom/mic/update/heart/0";
  // 获取房间管理列表
  static String roomAdminList = "$hostRoom/role/list";
  // 设置用户权限
  static String roomAdminSet = "$hostRoom/role/grant";
  // 设置房间基础信息
  static String setRoomInfo = "$hostRoom/info/update/detail";
  // 修改房间密码
  static String setRoomLock = "$hostRoom/info/update/password";
  // 获取用户在当前房间的信息
  static String userRoomInfo = "$hostAccount/user/small_home";
  // 获取房间禁言禁麦列表
  static String roomBanList = "$hostRoom/user/ban/list";
  // 获取房间拉黑列表
  static String roomBlockList = "$hostRoom/kick/black/list";
  // 房间用户禁言禁麦/解禁
  static String roomBanUser = "$hostRoom/user/set/ban";
  // 麦位禁麦/取消禁麦
  static String roomMicBan = "$hostRoom/mic/update/ban";
  // 房间拉黑用户
  static String roomBlockUser = "$hostRoom/kick/black/in";
  // 房间解除拉黑用户
  static String removeRoomBlockUser = "$hostRoom/kick/black/out";
  // 把用户踢出房间
  static String outRoom = "$hostRoom/kick/out";
  // 进入退出环信房间
  static String hxWordRoom = "$hostRoom/hx/world/room";
  // 麦位流水
  static String roomBill = "$hostRoom/mic/finance";
  // 获取声网房间token
  static String enToken = "$agoraToken/token/generate";
  // 音频流审核开启
  static String voiceIdentifyOpen = "$hostShumei/shumei/check-audio-stream";
  // 麦位真爱榜
  static String toLoveList = "$hostRoom/mic/heart/list";

  ///---------------------------------------------------礼物--------------------------------------------------------------------///
  // 获取全部礼物
  static String allGift = "$hostDataConfig/gift/shop/info/list";
  // 获取背包礼物
  static String knapsackGift = "$hostGift/user/gift-bags";
  // 获取魔盒礼物列表
  static String blindBoxGiftList = "$hostGame/magic_box/list";
  // 赠送礼物
  static String sendGift = "$hostGift/send";
  // 赠送魔盒
  static String sendBlindBoxGift = "$hostGame/magic_box/send_box";
  // 获取魔盒礼物
  static String boxGiftList = "$hostGame/magic_box/box_info";
  // 赠送金币
  static String sendGold = "$hostbBags/grade/user/send_gold";

  ///---------------------------------------------------音乐--------------------------------------------------------------------///
  // 音乐列表
  static String musicList = "$hostMusic/playlist";
  // 音乐详情
  static String musicInfo = "$hostMusic/info";

  ///---------------------------------------------------私信--------------------------------------------------------------------///
  // 私信举报用户
  static String msgReport = "$hostMsg/inform";
  // 发送私信
  static String sendMsg = "$hostMsg/chat-info";

  ///---------------------------------------------------聊天大厅---------------------------------------------------------------///
  // 发送聊天大厅消息
  static String sendWordMsg = "$hostMsg/hot-chat";
  // 热聊广场历史消息
  static String wordMsgList = "$hostMsg/hot-chat";
  // 这两个接口不同功能，但是是一个地址，以请求方式区分

  ///---------------------------------------------------流水-------------------------------------------------------------------///
  // 房间财富榜/魅力榜
  static String wealthCharmList = "$hostRoom/billboard";

  ///---------------------------------------------------系统-------------------------------------------------------------------///
  // 已读
  static String messageRead = "$hostSys/mark-message-read";
  // 系统通知
  static String systemNotifications = "$hostSys/system-list";
  // 官方消息
  static String adminMessage = "$hostSys/official-list";
  // 活动公告
  static String activityMessage = "$hostSys/activity-notice-list";
}
