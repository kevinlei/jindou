import 'package:echo/import.dart';
import 'package:flutter/material.dart';
import 'package:im_flutter_sdk/im_flutter_sdk.dart';

class HXManager extends ChangeNotifier {
  /// ---- 环信 ----
  static final Map _keyHxChat = {
    AppEnv.ENV_DEV.value: "1133230706175209#demo",
    AppEnv.ENV_TST.value: "1133230706175209#codyparty",
    AppEnv.ENV_LIN.value: "1133230706175209#codyparty",
  };
  static String get keyHxChat => _keyHxChat[AppConstants.appEnv.value];

  /// 初始化环信
  void createEmEngine() async {
    EMOptions options = EMOptions(
      appKey: keyHxChat,
      debugModel: true,
      usingHttpsOnly: false,
      requireDeliveryAck: true,
      requireAck: true,
    );
    await EMClient.getInstance.init(options);
    await EMClient.getInstance.startCallback();
    EMClient.getInstance.clearConnectionEventHandles();
    debugPrint("---------初始化环信");
  }

  /// 登录环信
  Future loginEmClient() async {
    if (UserConstants.userginInfo == null) return;
    try {
      String hxUserID = UserConstants.userginInfo?.info.hxInfo.uid ?? " ";
      String hxUserToken =
          UserConstants.userginInfo?.info.hxInfo.password ?? " ";
      debugPrint('环信登录信息：$hxUserID');
      await EMClient.getInstance.login(hxUserID, hxUserToken);
    } on EMError catch (e) {
      if (e.toString().contains("The user is already logged in")) return true;
      debugPrint('环信登录错误：$e');
      return false;
    }
  }

  /// 退出环信登录
  void loginOutEmClient() async {
    try {
      await EMClient.getInstance.logout(true);
      debugPrint("退出环信");
    } catch (e) {
      debugPrint("退出环信错误：$e");
    }
  }

  /// 发送环信私信消息
  Future<EMMessage> sendHxChatMsg(
      String conId, HxMsgExt msgExt, HXMsgBody msgBody) async {
    EMMessage msg = EMMessage.createTxtSendMessage(
        targetId: conId, content: msgBody.toJsonString());
    msg.chatType = ChatType.Chat;
    msg.hasRead = true;
    msg.attributes = msgExt.toMap();
    return await EMClient.getInstance.chatManager.sendMessage(msg);
  }

  /// 发送环信房间消息
  Future<EMMessage> sendHxRoomMsg(String conId, String msgText) async {
    EMMessage msg =
        EMMessage.createTxtSendMessage(targetId: conId, content: msgText);
    msg.chatType = ChatType.ChatRoom;
    msg.hasRead = true;
    return await EMClient.getInstance.chatManager.sendMessage(msg);
  }

  /// 加入环信房间
  void joinRoomChat(String chatId) async {
    try {
      await EMClient.getInstance.chatRoomManager.joinChatRoom(chatId);
      await EMClient.getInstance.chatRoomManager
          .joinChatRoom(AppConstants.worldChannel);
    } catch (err) {
      if (err.toString().contains("User is not logged in")) {
        loginEmClient();
      }
    }
  }

  /// 离开环信房间
  Future<void> leaveRoomChat(String chatId) async {
    AppConstants.channelStatistics("QuitIM", extra: chatId);
    await EMClient.getInstance.chatRoomManager.leaveChatRoom(chatId);
    await EMClient.getInstance.chatRoomManager
        .leaveChatRoom(AppConstants.worldChannel);
  }
}
