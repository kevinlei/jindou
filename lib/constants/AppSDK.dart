import 'package:flutter/cupertino.dart';
import 'package:fluwx/fluwx.dart' as fluwx;

class AppSDK extends ChangeNotifier {
  AppSDK._();

  static final AppSDK _instance = AppSDK._();

  factory AppSDK() => _instance;

  Future<void> initWXLogin() async {
    // wxd9dda2011ef6c386
    fluwx.registerWxApi(
      appId: 'wxb9a2a2ae454a6303', //查看微信开放平台
      doOnAndroid: true,
      doOnIOS: true,
      universalLink: 'https://com.cody.party/', //查看微信开放平台
    );
  }
}
