import 'dart:async';
import 'dart:io';
import 'dart:math';
import 'package:agora_rtc_engine/agora_rtc_engine.dart';
import 'package:echo/import.dart';
import 'package:path_provider/path_provider.dart';

class AgoraManager {
  /// ---- 声网 ----
  static final Map _APPID = {
    AppEnv.ENV_DEV.value: "eb7daf8ed12a43ed8d59914652892b17",
    AppEnv.ENV_TST.value: "9e0ee3e74bde47cfb968ce413fb4f2f3",
    AppEnv.ENV_LIN.value: "9e0ee3e74bde47cfb968ce413fb4f2f3",
  };
  static String get APPID => _APPID[AppConstants.appEnv.value];

  // 声网实例
  late RtcEngine _engine;

  // 节流
  int timer = 0;

  // 初始化声网
  void initRtcEngine() async {
    try {
      _engine = createAgoraRtcEngine();
      await _engine.initialize(RtcEngineContext(
        appId: APPID,
        channelProfile: ChannelProfileType.channelProfileLiveBroadcasting,
        audioScenario: AudioScenarioType.audioScenarioGameStreaming,
        logConfig: const LogConfig(
          level: LogLevel.logLevelError,
        ),
      ));
      _engine.setParameters("{\"che.audio.max_mixed_participants\":9}");
      _engine.enableAudioVolumeIndication(
          interval: 500, smooth: 3, reportVad: true);
      _engine.setAudioProfile(
          profile: AudioProfileType.audioProfileMusicHighQuality);
      _engine.setAudioScenario(AudioScenarioType.audioScenarioGameStreaming);
      // await _engine.setAINSMode(
      //     mode: AudioAinsMode.ainsModeBalanced, enabled: true);
      await _engine.enableInstantMediaRendering();
      enableLocalAudio(false);
    } catch (e) {
      print("初始化声网失败$e");
    }
  }

  // 更新token
  void renewToken(String newToken) async {
    try {
      await _engine.renewToken(newToken);
    } catch (e) {
      print("更新token失败$e");
    }
  }

  // 状态监听
  void statusMonitoring() {
    try {
      _engine.registerEventHandler(RtcEngineEventHandler(
        onError: (ErrorCodeType errorCode, String error) {
          print("声网SDK异常 $errorCode $error");
        },
        onTokenPrivilegeWillExpire:
            (RtcConnection connection, String token) async {
          // token还有30s过期
          String newToken = await AppManager().enToken();
          renewToken(newToken);
        },
        onJoinChannelSuccess: (RtcConnection connection, int elapsed) {
          print("用户 ${connection.localUid} $elapsed 加入成功");
        },
        onLeaveChannel: (RtcConnection connection, RtcStats stats) {
          // 用户退出声网房间
        },
        onRejoinChannelSuccess: (RtcConnection connection, int elapsed) {
          print("用户连接中断重新加入 ${connection.localUid} $elapsed 加入成功");
        },
        onConnectionStateChanged: (RtcConnection connection,
            ConnectionStateType stateType,
            ConnectionChangedReasonType reasonType) {
          print("网络连接状态更改 $stateType $reasonType");
        },
        onAudioVolumeIndication: (RtcConnection connection,
            List<AudioVolumeInfo> speakers,
            int speakerNumber,
            int totalVolume) {
          List<int> uids = [];
          for (var i = 0; i < speakers.length; i++) {
            if (speakers[i].uid == 0) {
              EventUtils()
                  .emit(EventConstants.E_ROOM_MY_VOICE, speakers[i].volume);
            } else if (speakers[i].volume != null &&
                speakers[i].volume! > 12 &&
                speakers[i].uid is int) {
              uids.add(speakers[i].uid as int);
            }
          }
          if (DateTime.now().millisecondsSinceEpoch - timer < 200) return;
          timer = DateTime.now().millisecondsSinceEpoch;
          EventUtils().emit(EventConstants.E_ROOM_SEAT_VOICE, uids);
        },
        onUserJoined: (RtcConnection connection, int remoteUid, int elapsed) {
          print("其他用户 $remoteUid 加入");
        },
        onUserOffline: (RtcConnection connection, int remoteUid,
            UserOfflineReasonType reason) {
          print("其他用户 $remoteUid 离开");
        },
        onAudioEffectFinished: (int soundId) {
          print("本地音效文件播放已结束 $soundId");
        },
        onAudioMixingPositionChanged: (int position) {
          // 每秒一次 音乐文件播放进度
          AppManager().roomInfo.musicPlaySchedule = position;
        },
        onAudioMixingStateChanged:
            (AudioMixingStateType state, AudioMixingReasonType reason) {
          if (state == AudioMixingStateType.audioMixingStateStopped &&
              AppManager().roomInfo.playMusicMode == 1 &&
              AppManager().timer == null &&
              AppManager().roomInfo.musicList.isNotEmpty) {
            print(
                "音乐文件的播放状态改变 $state $reason, ${AppManager().roomInfo.playMusicMode}, ${AppManager().timer == null}");
            AppManager().roomInfo.playMusicMode = 3;

            int index = 0;
            if (AppManager().roomInfo.musicMode == 0) {
              index = (AppManager().roomInfo.musicIndex + 1) ==
                      AppManager().roomInfo.musicList.length
                  ? 0
                  : AppManager().roomInfo.musicIndex + 1;
            } else if (AppManager().roomInfo.musicMode == 1) {
              index = Random().nextInt(AppManager().roomInfo.musicList.length);
            }

            var id = AppManager().roomInfo.musicMode != 2
                ? AppManager().roomInfo.musicList[index]["songId"]
                : null;

            AppManager().roomAudioMixing(id: id);
          }
        },
      ));
    } catch (e) {
      print("声网监听失败$e");
    }
  }

  // 取消声网监听
  void unregisterEventHandler() {
    try {
      _engine.unregisterEventHandler(RtcEngineEventHandler(
        onError: (ErrorCodeType errorCode, String error) {
          print("声网SDK异常 $errorCode $error");
        },
        onTokenPrivilegeWillExpire: (RtcConnection connection, String token) {
          // token还有30s过期
          renewToken(token);
        },
        onJoinChannelSuccess: (RtcConnection connection, int elapsed) {
          print("用户 ${connection.localUid} $elapsed 加入成功");
        },
        onLeaveChannel: (RtcConnection connection, RtcStats stats) {
          // 用户退出声网房间
        },
        onRejoinChannelSuccess: (RtcConnection connection, int elapsed) {
          print("用户连接中断重新加入 ${connection.localUid} $elapsed 加入成功");
        },
        onConnectionStateChanged: (RtcConnection connection,
            ConnectionStateType stateType,
            ConnectionChangedReasonType reasonType) {
          print("网络连接状态更改 $stateType $reasonType");
        },
        onAudioVolumeIndication: (RtcConnection connection,
            List<AudioVolumeInfo> speakers,
            int speakerNumber,
            int totalVolume) {
          List<int> uids = [];
          for (var i = 0; i < speakers.length; i++) {
            if (speakers[i].uid == 0) {
              EventUtils()
                  .emit(EventConstants.E_ROOM_MY_VOICE, speakers[i].volume);
            } else if (speakers[i].volume != null &&
                speakers[i].volume! > 12 &&
                speakers[i].uid is int) {
              uids.add(speakers[i].uid as int);
            }
          }
          if (DateTime.now().millisecondsSinceEpoch - timer < 200) return;
          timer = DateTime.now().millisecondsSinceEpoch;
          EventUtils().emit(EventConstants.E_ROOM_SEAT_VOICE, uids);
        },
        onUserJoined: (RtcConnection connection, int remoteUid, int elapsed) {
          print("其他用户 $remoteUid 加入");
        },
        onUserOffline: (RtcConnection connection, int remoteUid,
            UserOfflineReasonType reason) {
          print("其他用户 $remoteUid 离开");
        },
        onAudioEffectFinished: (int soundId) {
          print("本地音效文件播放已结束 $soundId");
        },
        onAudioMixingPositionChanged: (int position) {
          // 每秒一次 音乐文件播放进度
          AppManager().roomInfo.musicPlaySchedule = position;
        },
        onAudioMixingStateChanged:
            (AudioMixingStateType state, AudioMixingReasonType reason) {
          if (state == AudioMixingStateType.audioMixingStateStopped &&
              AppManager().roomInfo.playMusicMode == 1 &&
              AppManager().timer == null &&
              AppManager().roomInfo.musicList.isNotEmpty) {
            print(
                "音乐文件的播放状态改变 $state $reason, ${AppManager().roomInfo.playMusicMode}, ${AppManager().timer == null}");
            AppManager().roomInfo.playMusicMode = 3;

            int index = 0;
            if (AppManager().roomInfo.musicMode == 0) {
              index = (AppManager().roomInfo.musicIndex + 1) ==
                      AppManager().roomInfo.musicList.length
                  ? 0
                  : AppManager().roomInfo.musicIndex + 1;
            } else if (AppManager().roomInfo.musicMode == 1) {
              index = Random().nextInt(AppManager().roomInfo.musicList.length);
            }

            var id = AppManager().roomInfo.musicMode != 2
                ? AppManager().roomInfo.musicList[index]["songId"]
                : null;

            AppManager().roomAudioMixing(id: id);
          }
        },
      ));
    } catch (e) {
      print("取消声网监听失败$e");
    }
  }

  // 用户加入声网房间(是否加入声网成功以声网回调为准)
  void joinRtcEngine(int uid, String channelId, String token) async {
    try {
      await _engine.joinChannel(
        token: token,
        channelId: channelId,
        uid: uid,
        options: const ChannelMediaOptions(
          clientRoleType: ClientRoleType.clientRoleBroadcaster,
          publishCameraTrack: false,
          publishScreenTrack: false,
          publishScreenCaptureVideo: false,
          publishSecondaryScreenTrack: false,
          publishTranscodedVideoTrack: false,
          publishCustomVideoTrack: false,
          publishEncodedVideoTrack: false,
          publishMediaPlayerVideoTrack: false,
        ),
      );
    } catch (e) {
      print("用户加入声网房间失败$e");
    }
  }

  // 用户退出声网房间
  void leaveRtcEngine() async {
    try {
      await _engine.leaveChannel(options: const LeaveChannelOptions());
    } catch (e) {
      print("用户退出声网房间失败$e");
    }
  }

  // 取消或恢复发布本地音频流
  void muteLocalAudioStream(bool mute) async {
    try {
      await _engine.muteLocalAudioStream(mute);
    } catch (e) {
      print("${mute ? "恢复" : "取消"}发布本地音频流失败$e");
    }
  }

  // 设置用户角色和级别
  void setClientRole(ClientRoleType role) async {
    print("设置用户角色和级别了$role");
    try {
      await _engine.setClientRole(
          role: role, options: const ClientRoleOptions());
    } catch (e) {
      print("设置用户角色和级别失败$e");
    }
  }

  // 推流/暂停推流
  void enableLocalAudio(bool enabled) async {
    try {
      await _engine.enableLocalAudio(enabled);
    } catch (e) {
      print("${enabled ? "推流" : "暂停推流失败"}$e");
    }
  }

  // 拉流/暂停拉流
  void pullLocalAudio(bool pull) {
    try {
      _engine.muteAllRemoteAudioStreams(!pull);
    } catch (e) {
      print("${pull ? "拉流" : "暂停拉流失败"}$e");
    }
  }

  // 播放/暂停播放音乐
  void audioMixing(
      {String filePath = "", int cycle = 1, bool stop = false}) async {
    // cycle 音乐文件的播放次数 ≥ 0: 播放次数。例如，0 表示不播放；1 表示播放 1 次 -1: 无限循环播放;
    try {
      if (stop) {
        await _engine.pauseAudioMixing();
        AppManager().roomInfo.playMusicMode = 2;
      } else if (filePath.isNotEmpty) {
        await _engine.startAudioMixing(
            filePath: filePath, loopback: false, cycle: cycle);
        AppManager().roomInfo.playMusicMode = 1;
      }
      EventUtils().emit(EventConstants.E_ROOM_MUSIC, null);
    } catch (e) {
      print("${stop ? "暂停播放" : "播放"}音乐失败$e");
    }
  }

  // 恢复播放音乐
  void resumeAudioMixing() async {
    try {
      await _engine.resumeAudioMixing();
      AppManager().roomInfo.playMusicMode = 1;
      EventUtils().emit(EventConstants.E_ROOM_MUSIC, null);
    } catch (e) {
      print("恢复播放音乐失败$e");
    }
  }

  // 设置耳返功能
  void enableInEarMonitoring(bool isOpen) async {
    try {
      await _engine.enableInEarMonitoring(
        enabled: isOpen,
        includeAudioFilters:
            EarMonitoringFilterType.earMonitoringFilterNoiseSuppression,
      );
    } catch (e) {
      print("设置耳返功能$e");
    }
  }

  // 获取音乐音量
  Future<int> getAudioMixingPlayoutVolume() async {
    try {
      return await _engine.getAudioMixingPlayoutVolume();
    } catch (e) {
      print("获取音乐音量失败$e");
      return 0;
    }
  }

  // 设置音乐音量
  void setAudioMixingPitch(int pitch) async {
    try {
      await _engine.adjustAudioMixingVolume(pitch);
    } catch (e) {
      print("设置音乐音量失败$e");
    }
  }

  // 录音配置
  void startAudioRecording() async {
    try {
      Directory tempDir = await getTemporaryDirectory();
      int timeStamp = DateTime.now().millisecondsSinceEpoch ~/ 1000;
      String filePath = '${tempDir.path}/audioRecord-${timeStamp}2.14';
      _engine.startAudioRecording(AudioRecordingConfiguration(
        filePath: filePath,
        sampleRate: 44100,
        fileRecordingType: AudioFileRecordingType.audioFileRecordingMic,
        quality: AudioRecordingQualityType.audioRecordingQualityMedium,
      ));
    } catch (e) {
      print("录音配置失败${e.toString()}");
    }
  }

  // 停止录音
  void stopAudioRecording() {
    try {
      _engine.stopAudioRecording();
    } catch (e) {
      print("停止录音失败${e.toString()}");
    }
  }

  // 设置麦克风采集音量
  void setAudioCaptureVolume(int volume) async {
    try {
      await _engine.adjustRecordingSignalVolume(volume);
    } catch (e) {
      print("设置SDK采集音量失败$e");
    }
  }

  // // 获取麦克风采集音量
  // Future<int> getAudioCaptureVolume() async {
  //   try {} catch (e) {
  //     print("获取音乐音量失败$e");
  //     return 0;
  //   }
  // }

  // 设置扬声器播放音量
  void setAudioPlayoutVolume(int volume) async {
    try {
      await _engine.adjustPlaybackSignalVolume(volume);
    } catch (e) {
      print("设置SDK播放音量失败$e");
    }
  }

  // // 获取扬声器播放音量
  // Future<int> getAudioPlayoutVolume() async {
  //   try {} catch (e) {
  //     print("获取SDK播放音量失败$e");
  //     return 0;
  //   }
  // }

  // 设置混响
  // 0：关闭；1：KTV；3：大会堂；8：空灵；9：录音棚;
  void setVoiceReverbType(int type) async {
    try {
      print("设置混响");
      late AudioReverbType reverbKey;
      late int value;
      switch (type) {
        case 1:
          reverbKey = AudioReverbType.audioReverbRoomSize;
          value = 10;
          break;
        case 3:
          reverbKey = AudioReverbType.audioReverbStrength;
          value = 100;
          break;
        case 8:
          reverbKey = AudioReverbType.audioReverbWetDelay;
          value = 100;
          break;
        case 9:
          reverbKey = AudioReverbType.audioReverbWetLevel;
          value = 10;
          break;
        default:
          reverbKey = AudioReverbType.audioReverbDryLevel;
          value = 0;
      }
      await _engine.setLocalVoiceReverb(reverbKey: reverbKey, value: value);
    } catch (e) {
      print("设置混响失败${e.toString()}");
    }
  }

  // 销毁声网实例
  void releaseRtcEngine() async {
    try {
      await _engine.release();
    } catch (e) {
      print("销毁声网实例失败${e.toString()}");
    }
  }
}
