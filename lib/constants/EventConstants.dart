class EventConstants {
  // 点击屏幕
  static const String E_TAP_SCREEN = "E_TAP_SCREEN";
  // 键盘弹起/落下
  static const String E_KEYBOARD_OPEN = "E_KEYBOARD_OPEN";
  // 输入框点击
  static const String E_INPUT_ACTIVE = "E_INPUT_ACTIVE";
  // emotion点击
  static const String E_EMOTION_INPUT = "E_EMOTION_INPUT";
  // emotion删除
  static const String E_EMOTION_DEL = "E_EMOTION_DEL";
  // 房间声音采集
  static const String E_REMOTE_SOUND = "E_REMOTE_SOUND";
  // 房间声音采集
  static const String E_LOCAL_SOUND = "E_LOCAL_SOUND";
  // 清理投票输入框
  static const String E_CLEAN_VOTE = "E_CLEAN_VOTE";
  // 房间mq消息
  static const String E_MQ_MESSAGE = "E_MQ_MESSAGE";
  // mq重连reconnect
  static const String E_MQ_RECONNECT = "E_MQ_RECONNECT";
  // 开启关闭青少年模式
  static const String E_MQ_ADOMODE = "E_MQ_ADOMODE";
  // 系统mq消息
  static const String E_SYS_MESSAGE = "E_SYS_MESSAGE";

  ///-------------------------------------------------房间mq类型------------------------------------------------///
  // 自己上下麦
  static const String E_MIC_OPEN = "E_MIC_OPEN";
  // 排麦开关
  static const String E_MIC_APPLY_OPEN = "E_MIC_APPLY_OPEN";
  // 房间聊天开关
  static const String E_ROOM_CHAT_OPEN = "E_ROOM_CHAT_OPEN";
  // 修改房间背景图片
  static const String E_ROOM_BGIMG = "E_ROOM_BGIMG";
  // 房间基本信息修改
  static const String E_ROOM_INFO_UPDATE = "E_ROOM_INFO_UPDATE";
  // 房间人数/热度更新
  static const String E_ROOM_PEP_CHART = "E_ROOM_PEP_CHART";
  // 麦位列表更新
  static const String E_ROOM_SEAT = "E_ROOM_SEAT";
  // 房间消息更新(送礼/消息)
  static const String E_ROOM_MSG_UPDATE = "E_ROOM_MSG_UPDATE";
  // 用户进房间
  static const String E_ROOM_JOIN = "E_ROOM_JOIN";
  // 自己被踢出房间
  static const String E_ROOM_MY_OUT = "E_ROOM_MY_OUT";
  // 房间播放/暂停播放背景音乐
  static const String E_ROOM_MUSIC = "E_ROOM_MUSIC";
  // 房间送礼
  static const String E_ROOM_GIFT = "E_ROOM_GIFT";
  // 房间送礼顶部飘屏
  static const String E_ROOM_GIFT_TOP = "E_ROOM_GIFT_TOP";
  // 房间网速回调
  static const String E_ROOM_WIFI = "E_ROOM_WIFI";
  // 声浪回调
  static const String E_ROOM_SEAT_VOICE = "E_ROOM_SEAT_VOICE";
  // 自己的声浪回调
  static const String E_ROOM_MY_VOICE = "E_ROOM_MY_VOICE";
  // 自己发送表情
  static const String E_ROOM_EMO = "E_ROOM_EMO";
  // 财富更新
  static const String E_MY_WEALTH = "E_MY_WEALTH";

  ///-------------------------------------------------私信----------------------------------------------------///
  // 私信消息更新
  static const String E_MSG_UPDATE = "E_MSG_UPDATE";
  //青少年模式变更
  static const String E_HOME_VES = "E_HOME_VES";

  ///-------------------------------------------------系统----------------------------------------------------///
  // 系统消息
  static const String E_SYS_MSG = "E_SYS_MSG";
  // 活动公告
  static const String E_EVENT_ACT = "E_EVENT_ACT";
  // 互动消息
  static const String E_INTERACTION_MSG = "E_INTERACTION_MSG";
  // 官方消息
  static const String E_ADMIN_MSG = "E_ADMIN_MSG";
}
