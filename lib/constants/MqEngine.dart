import 'dart:async';
import 'dart:convert';
import 'package:echo/import.dart';
import 'package:flutter/foundation.dart';
import 'package:im_flutter_sdk/im_flutter_sdk.dart';
import 'package:mqtt_client/mqtt_client.dart';
import 'package:mqtt_client/mqtt_server_client.dart';

class MqEngine {
  late MqttServerClient _client;

  // 是否初始化
  bool _init = false;

  // 连接状态
  bool _isConnected = false;
  bool get isConnected => _isConnected;

  // 订阅回调
  SubscribeCallback? _subSuccess;

  // 取消回调
  UnsubscribeCallback? _unSubSuccess;

  StreamSubscription? subscription;

  Function()? onReconnected;

  void createMqEngine({Function()? onReconnected}) {
    if (UserConstants.userginInfo == null || _init) return;
    String server = NetConstants.mqConfig["host"];
    int port = NetConstants.mqConfig["port"];
    String identifier = UserConstants.userginInfo!.info.userId +
        DateTime.now().millisecondsSinceEpoch.toString();
    _client = MqttServerClient.withPort(server, identifier, port);
    this.onReconnected = onReconnected;
    _init = true;
  }

  void connectMq() async {
    _client.keepAlivePeriod = 60;
    _client.setProtocolV311();
    _client.logging(on: false);
    _client.autoReconnect = true;
    _client.resubscribeOnAutoReconnect = true;
    _client.onConnected = _onMqConnected;
    _client.onSubscribed = _onSubSuccess;
    _client.onSubscribeFail = _onSubFail;
    _client.onUnsubscribed = _onUnSubSuccess;
    _client.onAutoReconnected = _onMqReConnected;
    _client.onDisconnected = _onDisConnected;
    _client.pongCallback = _onPongCallback;
    await _client.connect();
  }

  /// 订阅主题
  void subscribeTopic(List<String> topics, {SubscribeCallback? callback}) {
    if (!_isConnected) return;
    if (callback != null) {
      _subSuccess = callback;
    }
    for (String topic in topics) {
      _client.subscribe(topic, MqttQos.atLeastOnce);
    }
  }

  /// 取消订阅
  void unSubscribeTopic(List<String> topics, {UnsubscribeCallback? callback}) {
    if (!_isConnected) return;
    if (callback != null) {
      _unSubSuccess = callback;
    }
    for (String topic in topics) {
      _client.unsubscribe(topic);
    }
  }

  /// 断开连接
  void disConnectMq() {
    _init = false;
    _client.disconnect();
  }

  /// 发布
  void publishMq(String topic, String content) {
    if (!_isConnected) return;
    var builder = MqttClientPayloadBuilder();
    builder.addString(content);
    _client.publishMessage(topic, MqttQos.exactlyOnce, builder.payload!);
  }

  // 连接成功
  void _onMqConnected() {
    try {
      _isConnected = true;
      subscription?.cancel();
      subscription = null;
      subscription = _client.updates!.listen(_onListener);
      if (kDebugMode) {
        debugPrint("------Mq连接成功${!_isConnected}");
      }
    } catch (e) {
      debugPrint("------Mq连接失败$e");
    }
  }

  // 消息
  void _onListener(List<MqttReceivedMessage<MqttMessage>> msgList) {
    MqttPublishMessage msg = msgList[0].payload as MqttPublishMessage;
    String payload = const Utf8Decoder().convert(msg.payload.message);
    debugPrint("收到Mq信息了${jsonDecode(payload)}");
    if (msgList[0].topic.contains('loginMqttTopic')) {
      if (jsonDecode(payload)["provide"] == SDKMQ.IM.value) {
        if (jsonDecode(payload)["action"] == "reconnect") {
          AppManager().joinHx();
        } else if (jsonDecode(payload)["action"] == "unsubscribe") {
          AppManager().leaveHx();
        }
      }
    } else if (msgList[0].topic.contains('message_notice') || msgList[0].topic.contains('message_official_topic')) {
      EventUtils().emit(EventConstants.E_SYS_MESSAGE, jsonDecode(payload));
    } else {
      EventUtils().emit(
          EventConstants.E_MQ_MESSAGE, [msgList[0].topic, jsonDecode(payload)]);
    }
  }

  // 订阅成功
  void _onSubSuccess(String topic) {
    debugPrint("------Mq订阅成功：$topic");
    if (_subSuccess == null) return;
    _subSuccess!(topic);
  }

  // 订阅失败
  void _onSubFail(String topic) {
    if (kDebugMode) {
      debugPrint("------Mq订阅失败：$topic");
    }
  }

  // 取消订阅
  void _onUnSubSuccess(String? topic) {
    if (kDebugMode) {
      debugPrint("------Mq取消订阅：$topic");
    }
    if (_unSubSuccess == null) return;
    _unSubSuccess!(topic);
  }

  // 重连
  void doReconnect() {
    if (_client.connectionStatus?.state != MqttConnectionState.connected) {
      _client.doAutoReconnect();
    }
  }

  // 重连成功
  void _onMqReConnected() {
    _isConnected = true;
    if (onReconnected != null) {
      onReconnected!();
    }
    debugPrint("------Mq重连成功");
    Future.delayed(const Duration(milliseconds: 300), () async {
      EventUtils().emit(EventConstants.E_MQ_RECONNECT, null);
    });
  }

  // 断开连接
  void _onDisConnected() {
    _isConnected = false;
    subscription?.cancel();
    subscription = null;
    debugPrint("------Mq断开连接");
  }

  // 心跳
  void _onPongCallback() async {
    if (UserConstants.userginInfo == null) return;
    bool isLogin = await EMClient.getInstance.isLoginBefore();
    if (!isLogin) {
      debugPrint('环信登录断开');
      HXManager().loginEmClient();
      AppManager().leaveHx();
      AppManager().joinHx();
    } else {
      bool isConnected = await EMClient.getInstance.isConnected();
      if (!isConnected || !AppManager().isHxsubscribe) {
        debugPrint('环信连接断开');
        AppManager().leaveHx();
        AppManager().joinHx();
      }
    }
  }
}
