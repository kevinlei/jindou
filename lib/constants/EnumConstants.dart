// ignore_for_file: constant_identifier_names
import 'package:flutter/services.dart';

/// 运行环境：内网 公测 线上
enum AppEnv { ENV_DEV, ENV_TST, ENV_LIN }

extension AppEnvExtension on AppEnv {
  String get value => ["dev", "test", "online"][index];
}

/// DIO请求方式
enum DioMethod { GET, POST, PUT, PATCH, DELETE }

extension DioMethodExtension on DioMethod {
  String get value => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE'][index];
}
// ----------------------------------------------------------------------------------------------------

/// oss图片/视频缩略图
enum OssThumb { OSS_VIDEO, OSS_IM_FILL, OSS_IM_SML }

extension OssThumbExtension on OssThumb {
  String get path => [
        "?x-oss-process=video/snapshot,t_100,f_jpg,m_fast",
        "?x-oss-process=image/resize,m_fill,w_400,h_400/quality,q_90",
        "?x-oss-process=image/resize,m_fill,w_300,h_300/quality,q_60"
      ][index];
}

enum CountListType { FOLLOW, FANS, FRIENDS, VISIT }

extension CountListTypeExtension on CountListType {
  String get text => ["关注", "粉丝", "好友", "访客"][index];
}

enum CountLiType { NIMOR, PRICE, LISU }

extension CountLiTypeExtension on CountLiType {
  String get text => ["默认排序", "礼物价格排序", "礼物数量排序"][index];
}

/// 房间大分类
enum EnterRoomType { ROOM_PRIVATE, ROOM_CLUB }

extension EnterRoomTypeExtension on EnterRoomType {
  int get value => [1, 2][index];
}

enum RoomManageType {
  DEFAULT,
  ROOM_SELF,
  ROOM_CLUB,
  ROOM_MANAGE,
  ROOM_FAVORITE,
}

// ----------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------
/// 读写存储key
enum SharedKey {
  KEY_TOKEN, // 用户token
  KEY_CODE,
  KEY_PWD_SAVE,
  KEY_NOTICE_SET,
  KEY_MINI_MODE,
  KEY_PROXY,
  KEY_UNIQUE,
  KEY_NO_LIKE,
  KEY_ROOM_ID, // 当前房间id
  KEY_REF_TOKEN, //刷新token
  KEY_MACHINE, //设备号
  KEY_SUB_MACHINE, //设备号
  KEY_ADOLESCENT, // 是否青少年模式
  KEY_SEARCH_RECORD, // 本地搜索记录
  KEY_MY_MUSIC, // 本地搜索记录
  KEY_REMOTHST, //偏好设置在房
}

extension ShareKeyExtension on SharedKey {
  String get value => [
        "KEY_TOKEN",
        "KEY_CODE",
        "KEY_PWD_SAVE",
        "KEY_NOTICE_SET",
        "KEY_MINI_MODE",
        "KEY_PROXY",
        "KEY_UNIQUE",
        "KEY_NO_LIKE",
        "KEY_ROOM_ID",
        "KEY_REF_TOKEN",
        "KEY_MACHINE",
        "KEY_SUB_MACHINE",
        "KEY_ADOLESCENT",
        "KEY_SEARCH_RECORD",
        "KEY_MY_MUSIC",
        "KEY_REMOTHST"
      ][index];
}

// ----------------------------------------------------------------------------------------------------

/// 交互方法
enum ChannelMethod { WE_CHAT_PAY, LOC_CHANGED, LOC_SEARCHED }

extension ChannelMethodExtension on ChannelMethod {
  String get method =>
      ["onWeChatPayResp", "onLocationChanged", "onLocationSearch"][index];
}

/// 提现绑定输入
enum DepositInputType { INPUT_NAME, INPUT_ALI, INPUT_BANK, INPUT_CODE }

extension DepositInputTypeExtension on DepositInputType {
  String get mainText => ["姓名：  ", "支付宝账号：", "银行卡账号：", ""][index];
  String get hintText => ["请输入真实姓名", "请输入支付宝账号", "请输入银行卡账号", "请输入验证码"][index];
  int get maxLength => [20, 20, 20, 6][index];
  TextInputType get keyboardType => [
        TextInputType.name,
        TextInputType.visiblePassword,
        TextInputType.visiblePassword,
        TextInputType.number
      ][index];
  TextInputFormatter? get formatter => [
        // FilteringTextInputFormatter.deny(RegExp(r"[\s]")),
        null,
        FilteringTextInputFormatter.allow(RegExp(r'[@0-9A-Za-z.]')),
        FilteringTextInputFormatter.allow(RegExp(r'[0-9A-Za-z]')),
        FilteringTextInputFormatter.digitsOnly,
      ][index];
}

/// 提现类型
enum DepositType { NULL, DEP_ALI, DEP_BANK }

extension DepositTypeExtension on DepositType {
  String get text => ["", "绑定支付宝", "绑定银行卡"][index];
}
// ----------------------------------------------------------------------------------------------------

/// 录音状态
enum SoundRecordStatus { DEF, RECORDING, READY, PLAYING }

enum LoginInputType {
  INPUT_PHONE,
  INPUT_CODE,
  INPUT_HAO,
  INPUT_PWD,
  INPUT_TPWD
}

extension LoginInputTypeExtension on LoginInputType {
  String get hintText => [
        "请输入您的手机号",
        "请输入验证码",
        "请输入您的账号",
        "请输入密码 (6-20位数字或字母)",
        "请再次输入您的密码"
      ][index];
  int get maxLength => [13, 6, 13, 18, 18][index];
  TextInputType get keyboardType => [
        TextInputType.number,
        TextInputType.number,
        TextInputType.number,
        TextInputType.name,
        TextInputType.name
      ][index];
  TextInputFormatter? get formatter => [
        FilteringTextInputFormatter.digitsOnly,
        FilteringTextInputFormatter.digitsOnly,
        FilteringTextInputFormatter.digitsOnly,
        FilteringTextInputFormatter.allow(
          RegExp(r'[0-9A-Za-z._-]'),
        ),
        FilteringTextInputFormatter.allow(
          RegExp(r'[0-9A-Za-z._-]'),
        )
      ][index];
}
// ----------------------------------------------------------------------------------------------------

/// 一键登录运营商
enum LoginOperator { NULL1, TEL_COM, CHINA_MOBILE, UNI_COM, NULL2, UNKNOWN }

extension LoginOperatorExtension on LoginOperator {
  String get brand =>
      ["", "中国电信提供认证服务", "中国移动提供认证服务", "中国联通提供认证服务", "", ""][index];
}

// ----------------------------------------------------------------------------------------------------

/// 协议
enum ProtocolType {
  // 相册权限使用目的 84
  PER_PHOTO,
  // 隐私政策 10
  PRO_PRIVACY,
  // 无线数据使用目的 73
  PER_WIFI,
  // 位置信息使用目的 77
  PER_LOC,
  // 相机权限使用目的 79
  PER_CAMERA,
  // 用户协议 11
  PRO_USER,
  // 通知权限使用目的 80
  PER_NOTICE,
  // 麦克风权限使用目的 67
  PER_MICRO,
  // 儿童隐私政策 7
  PRO_CHILD,
  // 电话权限使用目的 83
  PER_PHONE,
  // 充值协议 14
  PRO_CHARGE,
  // 账号注销须知 85
  PRO_QUIT,
  // 公会创建协议 13
  PRO_CLUB_CREATE,
  // 公会签约协议 87
  PRO_CLUB_SIGN,
  // 公会入驻规范 88
  PRO_CLUB_JOIN,
  // 邀请帮助
  PRO_INVITE_HELP,
  // 第三方SDK目录 9
  PRO_SDK_HELP,
  // 未成年人保护 8
  PRO_SCN_HELP,
  // 中国移动服务条款
  FUTKAN,
  // 应用权限说明
  PROUANXIAN,
  // 个人信息说明
  PROPRSON,
  // 防闪退消息送达攻略
  PROREST,
}

extension ProtocolTypeExtension on ProtocolType {
  String get text => [
        "相册权限使用目的",
        "隐私政策",
        "无线数据使用目的",
        "位置信息使用目的",
        "相机权限使用目的",
        "用户协议",
        "通知权限使用目的",
        "麦克风权限使用目的",
        "儿童隐私政策",
        "电话权限使用目的",
        "充值协议",
        "账号注销须知",
        "公会创建协议",
        "公会签约协议",
        "公会入驻规范",
        "邀请帮助",
        "第三方SDK目录",
        "未成年人保护",
        "中国移动服务条款",
        "应用权限说明",
        "个人信息说明",
        "防闪退消息送达攻略"
      ][index];

  int get proId => [
        84,
        10,
        73,
        77,
        79,
        11,
        83,
        67,
        7,
        83,
        14,
        85,
        13,
        87,
        88,
        99,
        9,
        8,
        12,
        1,
        2,
        3
      ][index];
}

/// 上传文件类型
enum UploadType {
  // 用户头像
  USER_IMAGE,
  // 房间
  ROOM_IMAGE,
  // 活动
  ACTIVITY_IMAGE,
  // 横幅
  BANNER,
  // 礼物
  GIFT_IMAGE,
  // 表情
  EMO,
  // 商品
  COMMODITY,
  // 徽章资源
  BADGE_RESOURCES,
  // 等级资源
  LEVEL_RESOURCES,
  // 照片墙(动态广场图)
  DYNAMIC_SQUARE,
  // 照片墙(动态广场语音)
  DYNAMIC_AUIDO,
  // 登录反馈(图片上传)
  LOGIN_FEEBACK,
  // 身份证实名
  REAL_ID_CARD,
  // 私聊发送语音
  CHAT_AUDIO,
  // 私聊发送图片
  CHAT_IMAGE,
  // 公会封面
  CLUB_COVER,
  // 公会头像
  CLUB_HEADIMG,
}

extension UploadTypeExtension on UploadType {
  String get value => [
        "0",
        "1",
        "2",
        "3",
        "4",
        "5",
        "6",
        "7",
        "8",
        "9",
        "10",
        "11",
        "12",
        "13",
        "14",
        "15",
        "16",
      ][index];
}

// ----------------------------------------------------------------------------------------------------

/// 文本检测类型
enum CheckTxtType {
  // 动态评论
  circle,
  // 动态
  dynamic,
  // 群聊
  groupChat,
  // 公会名称
  guildname,
  // 私聊
  message,
  // 昵称
  nickname,
  // 公会介绍
  notice,
  // 个人简介
  profile,
  // 房间名称
  roomName,
  // 公屏
  screen,
  // 广场
  square,
}

extension CheckTxtTypeExtension on CheckTxtType {
  String get value => [
        "circle",
        "dynamic",
        "groupChat",
        "guildname",
        "message",
        "nickname",
        "notice",
        "profile",
        "roomName",
        "screen",
        "square",
      ][index];
}

/// 图片检测类型
enum CheckImgType {
  // 不使用
  IMG_NULL,
  // 个人相册
  BACKGROUND,
  // 动态
  DYNAMIC,
  // 群聊
  GROUPMESSAGE,
  // 公会封面
  GUILDNAME,
  // 头像
  HEADIMAGE,
  // 私聊
  MESSAGE,
  // 个人房间
  PRTSONALROOM,
  // 房间封面
  ROOMCOVER,
  // 实名认证
  STATUS,
}

extension CheckImgTypeExtension on CheckImgType {
  String get value => [
        "",
        "background",
        "dynamic",
        "groupMessage",
        "guildname",
        "headImage",
        "message",
        "personalroom",
        "roomCover",
        "status",
      ][index];
}
// ----------------------------------------------------------------------------------------------------

/// 反馈类型
enum FeedbackType {
  // 登录问题反馈
  LOGIN_PROBLEM_FEEDBACK,
  // app问题反馈
  APP_PROBLEM_FEEDBACK,
  // 动态举报用户
  DYNAMIC_REPORTING_USER
}

extension FeedbackTypeExtension on FeedbackType {
  String get title => [
        "登录问题反馈",
        "APP问题反馈",
        "举报用户",
      ][index];

  String get hint => [
        "请描述您的意见反馈",
        "请描述您的意见反馈",
        "举报描述",
      ][index];

  int get value => [
        -1,
        -1,
        -1,
      ][index];
}

/// 环信消息类型
enum HXMsgType {
  TEXT,
  IMAGE,
  LOC,
  AUDIO,
  HBAO,
  VIDEO,
  DYNAMIC,
  GIFT,
  ROOM,
  GOLD,
  CLUB,
  DRESS,
}

extension HXMessageTypeExtension on HXMsgType {
  String get value => [
        "txt",
        "img",
        "loc",
        "audio",
        "hbao",
        "video",
        "dynamic",
        "gift",
        "room",
        "gold",
        "club",
        "dress",
      ][index];
}

/// 动态类型 // 纯文本  // 图文 // 语音 // 文本投票 // 图片投票
enum DynamicType {
  DEFAULT,
  TEXT_ONLY,
  TEXT_IMAGE,
  TEXT_AUDIO,
  VOTE_TEXT,
  VOTE_IMAGE
}
// ----------------------------------------------------------------------------------------------------

/// 动态列表类型
enum DynamicListType { DYNAMIC_FOLLOW, DYNAMIC_RECOMMEND, DYNAMIC_NEARBY }

/// 发布类型
enum PublishType { TEXT_IMG, TEXT_AUDIO, VOTE_TXT, VOTE_IMG }

// ----------------------------------------------------------------------------------------------------

// ----------------------------------------------------------------------------------------------------

/// 发布权限
enum DynamicAuthType { DEF, ALL_SEE, FRIEND }

extension DynamicAuthTypeExtension on DynamicAuthType {
  String get text => [
        "",
        "所有人可见",
        "仅好友可见",
      ][index];
}

// ----------------------------------------------------------------------------------------------------

/// 房间角色 会长 副会长 房主 超管 管理员 普通
enum RoomRole { CLUB, FIT_CLUB, OWNER, SM, MANAGER, DEF }

extension RoomRoleExtension on RoomRole {
  int get value => [-2, -1, 1, 2, 3, 4][index];
}

/// 用户角色 10000：普通用户 10010：vip用户 10030：官方用户 10020：管理员 10100：超级管理员
enum UserRole { DEF, VIP, OFFICIAL, MANAGER, SUPER }

extension UserRoleExtension on UserRole {
  int get value => [10000, 10010, 10030, 10020, 10100][index];
}

/// 注销条件
enum QuitAccountType {
  ACCOUNT_SAFE,
  WALLET_ASSETS,
  VIRTUAL_ASSETS,
  INFO_ASSETS,
  ACCOUNT_TITLE,
  PLATFORM_RIGHT
}

extension QuitAccountTypExtension on QuitAccountType {
  String get title => [
        "1、账号安全状态",
        "2、钱包资产",
        "3、虚拟资产",
        "4、信息资产",
        "5、爵位信息",
        "5、平台权限",
      ][index];
  String get des => [
        "需要在经常使用的手机上提交注销账号的申请,账号未被盗、被封等风险，且未在一个月注销成功过",
        "充值的金币、收益以及其他种类的可消费资产需清空后再注销",
        "礼物背包、商城背包等背包内的礼物或装扮将作废",
        "账号注销后，无法找回或处理该账号的任何内容，包括但不限于账号信息，已发布的动态，点赞、评论、转发、收藏、关注、私聊等互动信息",
        "购买的爵位以及不同爵位对应的权益将作废",
        "已解除平台内的主播、厅主或会长身份",
      ][index];
}

/// 实名认证审核状态
enum AuthStatus {
  NULL,
  AUTO_S,
  MANUAL_S,
  AUTO_F,
  AUTO_W,
  MANUAL_W,
  MANUAL_F,
}

extension AuthStatusExtension on AuthStatus {
  int get value => [0, 1, 1, 2, 3, 3, 2][index];
}

// 0.未知状态 1.认证完成 2.认证失败 3.等待审核中 4.未人脸识别
// 1已加入 2审核中 3未加入
/// 我在公会中的状态 0：已加入 1：申请签约中 2：已签约 3：申请解约中 4：申请退出中 5：申请加入中 6：默认状态
enum InClubStatus {
  IN_CLUB,
  SIGN_APPLY,
  SIGNED,
  DIS_APPLY,
  QUIT_APPLY,
  JOIN_APPLY,
  DEFAULT
}

/// 公会角色 0:非公会成员 1：公会普通成员 2：厅主 3：会长
///
/// 公会角色 0:非公会成员 1-普通成员 2-会长 3-副会长 4-管理员 5-厅长 6-财务
enum ClubRole { DEF, NORMAL, VICE, OWNER, FUWER, GUER, TINER, CNER }

// ----------------------------------------------------------------------------------------------------

/// 公会申请类型
enum ClubApplyType { APPLY_ENTER, APPLY_SIGN, APPLY_UN_SIGN, APPLY_QUIT }

/// ------------------------------------------服务端发送mq消息-------------------------------------------------------------

/// 房间飘屏动画类型
enum RoomBulletType { USER_JOIN, GIFT_PRIVATE, GIFT_GLOBAL }

/// 房间mq消息
enum MqMsgType {
  // 房间无效消息 0
  NULL,
  // 自己进退房间 1
  SELF_JOIN,
  // 用户发送消息 2
  ROOM_SEND_CHAT,
  // 表情消息 3
  ROOM_SEND_EMO,
  // 房间送礼消息 1001
  ROOM_GIFT,
  // 用户加入/踢出/退出房间 1002
  USER_ROOM_ACCESS,
  // 房间用户上下麦 1003
  SEAT_UP_DOWN,
  // 房间用户禁言/禁麦 1004
  BAN_MICRO_CHAT,
  // 清除麦位心动 1005
  CLEAN_SEAT_CHARM,
  // 房间麦位锁麦 1006
  LOCK_ONLY_SEAT,
  // 更新房间配置：公屏开关、背景图片更新、排麦开关、公告 1007
  ROOM_SET_CONF,
  // 飘屏推送 1008
  FLOATING_SCREEN,
  // 房间人数/热度更新 1009
  HEAT_NUMPEOPLE,
  // 麦位禁麦/取消禁麦 1010
  MIC_BAN,
  // 游戏抽奖消息 1011
  ROOM_GAME_GIFT,
  // 全服游戏抽奖消息 1012
  GAME_GIFT,
}

extension MqMsgTypeExtension on MqMsgType {
  int get value => [
        0,
        1, // 自己进退房间
        2, // 用户发送消息
        3, // 表情消息
        1001, // 房间送礼消息
        1002, // 用户加入/踢出/退出房间
        1003, // 房间用户上下麦
        1004, // 房间用户禁言/禁麦
        1005, // 清除麦位心动
        1006, // 房间麦位锁麦
        1007, // 更新房间配置：公屏开关、背景图片更新、排麦开关、公告
        1008, // 飘屏推送
        1009, // 房间人数/热度更新
        1010, // 麦位禁麦/取消禁麦
        1011, // 游戏抽奖消息
        1012, // 全服游戏抽奖消息
      ][index];
}

/// 系统mq消息
enum SysMqMsgType {
  NULL,
  SYS, // 系统通知
  ADMIN, // 官方消息
  ACTIVITY, // 活动公告
  INTERACTION, // 互动消息
}

extension SysMqMsgTypeExtension on SysMqMsgType {
  int get value => [
        0,
        1,
        2,
        3,
        4,
      ][index];
}

/// 支付方式
enum PayType {
  NULL,
  PAY_WX,
  PAY_ALI,
  PAY_SD_KJ,
}

extension PayTypeExtension on PayType {
  String get icon => [
        "",
        "set/wx",
        "set/al",
        "set/un",
      ][index];

  String get title => [
        "",
        "微信支付",
        "支付宝支付",
        "云闪付",
      ][index];

  String get msg => [
        "",
        "Weixin",
        "Alipay",
        "SandAlipay",
      ][index];
}

/// mq订阅sdk操作
enum SDKMQ { IM, TRTC, AGORA }

extension SDKMQExtension on SDKMQ {
  String get value => ["im", "tencent", "agora"][index];
}

//banner图 动态 个人中心 账户 充值 收益提现 公会 首页 房间游戏 房间活动
enum BanbarSwper {
  DYT_COM,
  PERSON_COM,
  ZHU_COM,
  PRICE_COM,
  TIXIAN_COM,
  GOHUI_COM,
  HOME_COM,
  ROOM_GAME_COM,
  ROOM_ACTIVITY_COM,
}

extension BanbarExtension on BanbarSwper {
  int get brand => [0, 1, 2, 3, 4, 5, 6, 8, 7][index];
}

/// 验证码接口类型参数
//   SMS_TEMPLATE_SENSITIVE: SMS_462575764         #敏感操作
//   SMS_TEMPLATE_LOGIN: SMS_462650718             #登陆操作
//   SMS_TEMPLATE_PASSWORD: SMS_462570776          #修改密码
//   SMS_TEMPLATE_LOG_OFF: SMS_462650720           #账号注销
//   SMS_TEMPLATE_SIGN: SMS_462570777              #实名认证
//   SMS_TEMPLATE_WITHDRAW: SMS_462550762          #提现账户
//   SMS_TEMPLATE_REGISTER: SMS_462595730          #账号注册
//   SMS_TEMPLATE_BIND: SMS_462555760              #账号绑定
///
enum SMSType {
  SMS_TEMPLATE_SENSITIVE,
  SMS_TEMPLATE_LOGIN,
  SMS_TEMPLATE_PASSWORD,
  SMS_TEMPLATE_LOG_OFF,
  SMS_TEMPLATE_SIGN,
  SMS_TEMPLATE_WITHDRAW,
  SMS_TEMPLATE_REGISTER,
  SMS_TEMPLATE_BIND,
}

extension UploadSMSType on SMSType {
  String get value => [
        "SMS_TEMPLATE_SENSITIVE",
        "SMS_TEMPLATE_LOGIN",
        "SMS_TEMPLATE_PASSWORD",
        "SMS_TEMPLATE_LOG_OFF",
        "SMS_TEMPLATE_SIGN",
        "SMS_TEMPLATE_WITHDRAW",
        "SMS_TEMPLATE_REGISTER",
        "SMS_TEMPLATE_BIND",
      ][index];
}

/// mq订阅sdk操作
enum BackstageActivation { HONOR, HUAWEI, XIAOMI, VIVO, OPPO, NONE }

extension BackstageActivationExtension on BackstageActivation {
  String get sleep => [
        "",
        "",
        "解决方法：设置-电池与性能-右上角设置入口-场景配置-睡眠模式-开关关闭",
        "解决方法：设置-电池-睡眠模式-开关关闭",
        "解决方法: 1.设置-电池-智能省电场景-睡眠待机优化-开关关闭；2.设置-电池-应用速冻-金豆派对-开关关闭",
        "解决方法：设置-电池-睡眠模式-开关关闭",
      ][index];

  String get value => [
        "解决方法：设置-应用-应用启动管理-金豆派对-切换为手动管理-开启【允许后台活动】、【允许自启动管理】、【允许关联启动】",
        "解决方法：设置-应用-应用启动管理-金豆派对-切换为手动管理-开启【允许后台活动】、【允许自启动管理】、【允许关联启动】",
        "解决方法：设置-电池与性能-右上角设置入口-应用智能省电-金豆派对-选择[无限制]，允许在后台运行",
        "解决方法：设置-电池-后台高耗电-金豆派对-开关打开，允许后台高耗电时继续运行",
        "解决方法：设置-应用管理-应用列表-金豆派对-耗电保护/耗电管理-选择【允许后台运行】/【允许完全后台行为】开启，【允许应用自启动】开启，【允许应用关联启动】开启",
        "解决方法：设置-应用管理-应用列表-金豆派对-允许后台活动开启",
      ][index];
}
