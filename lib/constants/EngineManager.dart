import 'dart:async';
import 'package:agora_rtc_engine/agora_rtc_engine.dart';
import 'AgoraManager.dart';
import 'AppConstants.dart';
import 'TRTCManager.dart';

class EngineManager {
  // 声网实例
  final AgoraManager agoraManager = AgoraManager();
  // TRTC实例
  final TRTCManager trtcManager = TRTCManager();

  // 初始化声网/TRTC
  void initRtcEngine() async {
    trtcManager.initRtcEngine();
    agoraManager.initRtcEngine();
  }

  // 状态监听
  void statusMonitoring() {
    if (AppConstants.trtcAgoraSwitchFlag == 0) {
      trtcManager.statusMonitoring();
    } else {
      agoraManager.statusMonitoring();
    }
  }

  // 取消声网/TRTC监听
  void unregisterEventHandler() {
    if (AppConstants.trtcAgoraSwitchFlag == 0) {
      trtcManager.unregisterEventHandler();
    } else {
      agoraManager.unregisterEventHandler();
    }
  }

  // 用户加入声网/TRTC房间
  void joinRtcEngine(int uid, String channelId, String token) async {
    if (AppConstants.trtcAgoraSwitchFlag == 0) {
      trtcManager.joinRtcEngine(uid, channelId, token);
    } else {
      agoraManager.joinRtcEngine(uid, channelId, token);
    }
  }

  // 用户退出声网/TRTC房间
  void leaveRtcEngine() async {
    if (AppConstants.trtcAgoraSwitchFlag == 0) {
      trtcManager.leaveRtcEngine();
    } else {
      agoraManager.leaveRtcEngine();
    }
  }

  // 设置用户角色和级别
  void setClientRole(ClientRoleType role) async {
    if (AppConstants.trtcAgoraSwitchFlag == 0) {
      trtcManager.setClientRole(
          (role == ClientRoleType.clientRoleBroadcaster) ? 20 : 21);
    } else {
      agoraManager.setClientRole(role);
    }
  }

  // 推流/暂停推流
  void enableLocalAudio(bool enabled) async {
    if (AppConstants.trtcAgoraSwitchFlag == 0) {
      trtcManager.enableLocalAudio(enabled);
    } else {
      agoraManager.enableLocalAudio(enabled);
    }
  }

  // 拉流/暂停拉流
  void pullLocalAudio(bool pull) {
    if (AppConstants.trtcAgoraSwitchFlag == 0) {
      trtcManager.pullLocalAudio(pull);
    } else {
      agoraManager.pullLocalAudio(pull);
    }
  }

  // 播放/暂停播放音乐
  void audioMixing(
      {String filePath = "", int cycle = 1, bool stop = false}) async {
    // cycle 音乐文件的播放次数 ≥ 0: 播放次数。例如，0 表示不播放；1 表示播放 1 次 -1: 无限循环播放;
    if (AppConstants.trtcAgoraSwitchFlag == 0) {
      trtcManager.audioMixing(filePath: filePath, cycle: cycle, stop: stop);
    } else {
      agoraManager.audioMixing(filePath: filePath, cycle: cycle, stop: stop);
    }
  }

  // 恢复播放音乐
  void resumeAudioMixing() async {
    if (AppConstants.trtcAgoraSwitchFlag == 0) {
      trtcManager.resumeAudioMixing();
    } else {
      agoraManager.resumeAudioMixing();
    }
  }

  // 设置耳返功能
  void enableInEarMonitoring(bool isOpen) async {
    if (AppConstants.trtcAgoraSwitchFlag == 0) {
      trtcManager.enableInEarMonitoring(isOpen);
    } else {
      agoraManager.enableInEarMonitoring(isOpen);
    }
  }

  // 获取音乐音量
  Future<int> getAudioMixingPlayoutVolume() async {
    if (AppConstants.trtcAgoraSwitchFlag == 0) {
      return trtcManager.getAudioMixingPlayoutVolume();
    } else {
      return agoraManager.getAudioMixingPlayoutVolume();
    }
  }

  // 设置音乐音量
  void setAudioMixingPitch(int pitch) async {
    if (AppConstants.trtcAgoraSwitchFlag == 0) {
      trtcManager.setAudioMixingPitch(pitch);
    } else {
      agoraManager.setAudioMixingPitch(pitch);
    }
  }

  // 设置麦克风音量
  void setAudioCaptureVolume(int volume) async {
    if (AppConstants.trtcAgoraSwitchFlag == 0) {
      trtcManager.setAudioCaptureVolume(volume);
    } else {
      agoraManager.setAudioCaptureVolume(volume);
    }
  }

  // 设置扬声器音量
  void setAudioPlayoutVolume(int volume) async {
    if (AppConstants.trtcAgoraSwitchFlag == 0) {
      trtcManager.setAudioPlayoutVolume(volume);
    } else {
      agoraManager.setAudioPlayoutVolume(volume);
    }
  }

  // 设置混响
  void setVoiceReverbType(int type) {
    if (AppConstants.trtcAgoraSwitchFlag == 0) {
      // 0：关闭；1：KTV；2：小房间；3：大会堂；4：低沉；5：洪亮；6：金属声；7：磁性；8：空灵；9：录音棚；10：悠扬 11：录音棚2
      trtcManager.setVoiceReverbType(type);
    } else {
      agoraManager.setVoiceReverbType(type);
    }
  }

  // 设置变声特效(仅TRTC支持)
  // 0：关闭；1：熊孩子；2：萝莉；3：大叔；4：重金属；5：感冒；6：外语腔；7：困兽；8：肥宅；9：强电流；10：重机械；11：空灵
  void setVoiceChangerType(int type) {
    if (AppConstants.trtcAgoraSwitchFlag == 0) {
      trtcManager.setVoiceChangerType(type);
    }
  }

  // 录音配置
  void startAudioRecording() async {
    if (AppConstants.trtcAgoraSwitchFlag == 0) {
      trtcManager.startAudioRecording();
    } else {
      agoraManager.startAudioRecording();
    }
  }

  // 停止录音
  void stopAudioRecording() {
    if (AppConstants.trtcAgoraSwitchFlag == 0) {
      trtcManager.stopAudioRecording();
    } else {
      agoraManager.stopAudioRecording();
    }
  }

  // 销毁声网/TRTC实例
  void releaseRtcEngine() async {
    trtcManager.releaseRtcEngine();
    agoraManager.releaseRtcEngine();
  }
}
