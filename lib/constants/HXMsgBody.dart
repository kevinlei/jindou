import 'dart:convert';
import 'package:echo/import.dart';
import 'package:flutter/cupertino.dart';

class HxMsgExt {
  late String toUser;
  late String toHead;
  late String toName;

  HxMsgExt(String user, String head, String name) {
    this.toUser = user;
    this.toHead = head;
    this.toName = name;
  }

  Map<String, String> toMap() {
    return {
      "sendUser": UserConstants.userginInfo?.info.userId ?? "",
      "sendHead": UserConstants.userginInfo?.info.headIcon ?? "",
      "sendName": UserConstants.userginInfo?.info.name ?? "",
      "toUser": toUser,
      "toHead": toHead,
      "toName": toName
    };
  }
}

class HxReceiveMsg {
  HXMsgType msgType = HXMsgType.TEXT;
  HXMsgBody msgBody = HXTextMsg("");

  late String _content;
  HxReceiveMsg(String content) {
    this._content = content;
    _decodeMsg();
  }

  void _decodeMsg() {
    try {
      Map map = jsonDecode(_content);
      String type = map["type"];
      debugPrint("消息类型$type, $map");
      switch (type) {
        case "txt":
          msgType = HXMsgType.TEXT;
          msgBody = HXTextMsg(map["text"]);
          break;
        case "img":
          msgType = HXMsgType.IMAGE;
          double width = map["width"] ?? 0;
          double height = map["height"] ?? 0;
          msgBody = HXImageMsg(map["path"], width: width, height: height);
          break;
        case "loc":
          msgType = HXMsgType.LOC;
          double lat = map["latitude"];
          double lot = map["longitude"];
          String address = map["address"];
          msgBody = HXLocMsg(lat, lot, address);
          break;
        case "audio":
          msgType = HXMsgType.AUDIO;
          msgBody = HXAudioMsg(map["path"], map["duration"]);
          break;
        case "hbao":
          msgType = HXMsgType.HBAO;
          msgBody = HXHbaoMsg(map["hbaoNum"]);
          break;
        case "video":
          msgType = HXMsgType.VIDEO;
          msgBody = HXVideoMsg(map["path"]);
          break;
        case "dynamic":
          msgType = HXMsgType.DYNAMIC;
          int dyId = map["dyId"], dyType = map["dyType"];
          String name = map["name"],
              head = map["head"],
              resource = map["resource"],
              describe = map["describe"];
          msgBody = HXDynamicMsg(dyId, dyType, name, head, resource, describe);
          break;
        case "gift":
          msgType = HXMsgType.GIFT;
          msgBody = HXGiftMsg(map["giftImg"], map["giftCount"],
              map["giftPrice"], map["giftName"]);
          break;
        case "room":
          msgType = HXMsgType.ROOM;
          int roomId = map["roomId"];
          String displayId = map["displayId"],
              name = map["name"],
              cover = map["cover"];
          msgBody = HXRoomMsg(roomId, displayId, name, cover);
          break;
        case "gold":
          msgType = HXMsgType.GOLD;
          msgBody = HXGoldMsg(map["number"]);
          break;
        case "club":
          msgType = HXMsgType.CLUB;
          msgBody = HXClubMsg(
              map["clubId"], map["displayId"], map["name"], map["cover"]);
          break;
        case "dress":
          msgType = HXMsgType.DRESS;
          msgBody = HXDressMsg(map["dressId"], map["day"]);
          break;
      }
    } catch (e) {
      debugPrint("分析消息类型错误$e");
    }
  }
}

/// 环信消息体
abstract class HXMsgBody {
  String toJsonString();
}

/// 纯文本消息
class HXTextMsg extends HXMsgBody {
  late String text;
  HXTextMsg(String text) {
    this.text = text;
  }

  @override
  String toJsonString() {
    return jsonEncode({
      "type": HXMsgType.TEXT.value,
      "text": text,
    });
  }
}

/// 环信图片消息
class HXImageMsg extends HXMsgBody {
  late String imagePath;
  double? width;
  double? height;

  HXImageMsg(String path, {double? width, double? height}) {
    this.imagePath = path;
    this.width = width;
    this.height = height;
  }

  @override
  String toJsonString() {
    return jsonEncode({
      "type": HXMsgType.IMAGE.value,
      "path": imagePath,
      "width": width,
      "height": height,
    });
  }
}

/// 环信音频消息
class HXAudioMsg extends HXMsgBody {
  late String audioPath;
  late int duration;

  HXAudioMsg(String path, [int duration = 0]) {
    this.audioPath = path;
    this.duration = duration;
  }

  @override
  String toJsonString() {
    return jsonEncode({
      "type": HXMsgType.AUDIO.value,
      "path": audioPath,
      "duration": duration,
    });
  }
}

/// 环信红包消息
class HXHbaoMsg extends HXMsgBody {
  late int hbaoNum;

  HXHbaoMsg(int hbaoNum) {
    this.hbaoNum = hbaoNum;
  }

  @override
  String toJsonString() {
    return jsonEncode({
      "type": HXMsgType.HBAO.value,
      "hbaoNum": hbaoNum,
    });
  }
}

/// 环信视频消息
class HXVideoMsg extends HXMsgBody {
  late String videoPath;

  HXVideoMsg(String path) {
    this.videoPath = path;
  }

  @override
  String toJsonString() {
    return jsonEncode({
      "type": HXMsgType.VIDEO.value,
      "path": videoPath,
    });
  }
}

/// 环信位置消息
class HXLocMsg extends HXMsgBody {
  late double latitude;
  late double longitude;
  late String address;

  HXLocMsg(double lat, double lot, [String address = ""]) {
    this.latitude = lat;
    this.longitude = lot;
    this.address = address;
  }

  @override
  String toJsonString() {
    return jsonEncode({
      "type": HXMsgType.LOC.value,
      "latitude": latitude,
      "longitude": longitude,
      "address": address,
    });
  }
}

/// 环信动态分享消息
class HXDynamicMsg extends HXMsgBody {
  // 动态ID
  late int dynamicId;
  late int dyType;
  late String name;
  late String head;
  late String resource;
  late String describe;

  HXDynamicMsg(int id, int dyType, String name, String head, String resource,
      String describe) {
    this.dynamicId = id;
    this.dyType = dyType;
    this.name = name;
    this.head = head;
    this.resource = resource;
    this.describe = describe;
  }

  @override
  String toJsonString() {
    return jsonEncode({
      "type": HXMsgType.DYNAMIC.value,
      "dyId": dynamicId,
      "dyType": dyType,
      "name": name,
      "head": head,
      "resource": resource,
      "describe": describe,
    });
  }
}

/// 环信礼物赠送消息
class HXGiftMsg extends HXMsgBody {
  late String giftImg;
  late int giftCount;
  late int giftPrice;
  late String giftName;

  HXGiftMsg(String giftImg, int giftCount, int giftPrice, String giftName) {
    this.giftImg = giftImg;
    this.giftCount = giftCount;
    this.giftPrice = giftPrice;
    this.giftName = giftName;
  }

  @override
  String toJsonString() {
    return jsonEncode({
      "type": HXMsgType.GIFT.value,
      "giftImg": giftImg,
      "giftCount": giftCount,
      "giftPrice": giftPrice,
      "giftName": giftName,
    });
  }
}

/// 环信房间分享消息
class HXRoomMsg extends HXMsgBody {
  late int roomId;
  late String displayId;
  late String roomName;
  late String roomCover;

  HXRoomMsg(int id, String displayId, String name, String cover) {
    this.roomId = id;
    this.displayId = displayId;
    this.roomName = name;
    this.roomCover = cover;
  }

  @override
  String toJsonString() {
    return jsonEncode({
      "type": HXMsgType.ROOM.value,
      "roomId": roomId,
      "displayId": displayId,
      "name": roomName,
      "cover": roomCover,
    });
  }
}

/// 环信金豆赠送消息
class HXGoldMsg extends HXMsgBody {
  late int goldNumber;

  HXGoldMsg(int num) {
    this.goldNumber = num;
  }

  @override
  String toJsonString() {
    return jsonEncode({
      "type": HXMsgType.GOLD.value,
      "number": goldNumber,
    });
  }
}

/// 环信公会分享消息
class HXClubMsg extends HXMsgBody {
  late String clubId;
  late String displayId;
  late String clubName;
  late String clubCover;

  HXClubMsg(String id, String displayId, String name, String cover) {
    this.clubId = id;
    this.displayId = displayId;
    this.clubName = name;
    this.clubCover = cover;
  }

  @override
  String toJsonString() {
    return jsonEncode({
      "type": HXMsgType.CLUB.value,
      "clubId": clubId,
      "displayId": displayId,
      "name": clubName,
      "cover": clubCover,
    });
  }
}

/// 环信装扮信息
class HXDressMsg extends HXMsgBody {
  late int dressId;
  late int day;

  HXDressMsg(int dressId, int day) {
    this.dressId = dressId;
    this.day = day;
  }

  @override
  String toJsonString() {
    return jsonEncode({
      "type": HXMsgType.DRESS.value,
      "dressId": dressId,
      "day": day,
    });
  }
}
