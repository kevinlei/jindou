import 'package:flutter/material.dart';
import 'package:echo/import.dart';

/// 配置
class ConfigManager {
  ConfigManager._();
  static final ConfigManager _instance = ConfigManager._();
  factory ConfigManager() => _instance;
  // 财富/魅力等级
  List<ConfigLevel> levelConfig = [];
  // 表情
  List<ConfigEmo> emotionConfig = [];

  ConfigLevel? getLevel(int exp) {
    ConfigLevel? level;
    for (ConfigLevel lv in levelConfig) {
      if (exp >= double.parse(lv.experience)) {
        level = lv;
        continue;
      }
    }
    return level;
  }

  ConfigLevel? getLevelByLv(int level) {
    for (ConfigLevel lv in levelConfig) {
      if (int.parse(lv.grade) == level) {
        return lv;
      }
    }
    return null;
  }

  void requestGlobalConfig() {
    DioUtils().httpRequest(
      NetConstants.emojisConfig,
      loading: false,
      method: DioMethod.GET,
      onSuccess: _onGlobalConfig,
    );
  }

  void _onGlobalConfig(response) {
    if (response == null) return;
    ConfigGlobal global = ConfigGlobal.fromJson(response);
    emotionConfig = global.emo ?? [];
  }

  // 用户权限勋章
  Widget userIcon(UserRole userRole, {double paddingRight = 0}) {
    switch (userRole) {
      case UserRole.OFFICIAL:
        return Padding(
            padding: EdgeInsets.only(right: paddingRight),
            child:
                LoadAssetImage("medal/i_official", width: 62.w, height: 30.w));
      default:
        return const SizedBox();
    }
  }

  // 个人勋章图片
  Widget userWealthCharmIcon(String image, int? grade,
      {double marginRight = 0,
      bool isWealth = false,
      double? width,
      double? height}) {
    Color color = Colors.white;
    if (isWealth) {
      color = wealthColor(grade??0);
    }
    bool showDefault=null==grade||0==grade;
    return Visibility(
      visible: true,
      child: FContainer(
        margin: EdgeInsets.only(right: marginRight),
        padding: EdgeInsets.only(right: 8.w),
        radius: BorderRadius.circular(50),
        imageFit: BoxFit.cover,
        image: ImageUtils().getImageProvider(showDefault?'':NetConstants.ossPath + image,holderImg: isWealth?'person/wealth_default':'person/charm_default'),
        width: width ?? 50.w,
        height: height ?? 25.w,
        align: Alignment.centerRight,
        child:
            FText("${grade??0}", color: color, size: 16.sp, weight: FontWeight.w600),
      ),
    );
  }

  Color wealthColor(int grade) {
    if (grade <= 10) {
      return const Color(0xFFB1BEA3);
    } else if (grade <= 20) {
      return const Color(0xFFB0B244);
    } else if (grade <= 30) {
      return const Color(0xFFBF9D23);
    } else if (grade <= 40) {
      return const Color(0xFFBE8900);
    } else if (grade <= 50) {
      return const Color(0xFFBF7116);
    } else if (grade <= 60) {
      return const Color(0xFFBD563F);
    } else if (grade <= 70) {
      return const Color(0xFFB83434);
    } else if (grade <= 80) {
      return const Color(0xFFB21658);
    } else if (grade <= 90) {
      return const Color(0xFFBB00A8);
    } else {
      return const Color(0xFF8200BF);
    }
  }

  // 房间勋章图片
  Widget roomIcon(RoomRole roomRole, {double paddingRight = 0}) {
    switch (roomRole) {
      case RoomRole.CLUB:
        return Padding(
            padding: EdgeInsets.only(right: paddingRight),
            child: LoadAssetImage("medal/i_club1", width: 80.w, height: 30.w));
      case RoomRole.FIT_CLUB:
        return Padding(
            padding: EdgeInsets.only(right: paddingRight),
            child: LoadAssetImage("medal/i_club2", width: 80.w, height: 30.w));
      case RoomRole.OWNER:
        return Padding(
            padding: EdgeInsets.only(right: paddingRight),
            child: LoadAssetImage("medal/i_room", width: 62.w, height: 30.w));
      case RoomRole.MANAGER:
        return Padding(
            padding: EdgeInsets.only(right: paddingRight),
            child: LoadAssetImage("medal/i_manage", width: 62.w, height: 30.w));
      case RoomRole.SM:
        return Padding(
            padding: EdgeInsets.only(right: paddingRight),
            child: LoadAssetImage("medal/i_sm", width: 62.w, height: 30.w));
      default:
    }
    return const SizedBox();
  }
}
