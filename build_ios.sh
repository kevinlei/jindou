#!/usr/bin/env bash
flutter clean
flutter build ios --release  --no-codesign --obfuscate --split-debug-info=./symbols
cd build/ios/iphoneos/Runner.app/Frameworks
cd App.framework
xcrun bitcode_strip -r app -o app
cd ..
cd Flutter.framework
xcrun bitcode_strip -r Flutter -o Flutter
cd ..
xcrun bitcode_strip -r libswiftCore.dylib -o libswiftCore.dylib  
xcrun bitcode_strip -r libswiftFoundation.dylib -o libswiftFoundation.dylib
xcrun bitcode_strip -r libswiftDispatch.dylib -o libswiftDispatch.dylib
cd Flutter.framework

cd ../../../
rm -rf Payload
mkdir Payload
mv Runner.app Payload/Runner.app
zip -9 -r app.zip Payload
rm -rf app.ipa
mv app.zip app.ipa

cd ../../../
exit 0